<?php

/**
 * <b>CalendarService</b>
 * Service to control all the dashboard of Info SKU;
 * @param INT $companyId
 * @param INT $skuId
 */
class CalendarService
{
    private $company_id;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    public function newCalendarID()
    {
        $query = DB::Select(
            'scm_cal_receiving',
            'max(ID_CALENDAR_PK) + 1 AS new_id',
            "WHERE ID_COMPANY_FK = {$this->company_id}"
        );

        if ($query) {
            return $query[0]['NEW_ID'];
        } else {
            return false;
        }

    }

    public function insertInfos($calendar_id, $description)
    {
        $avg_days_interval = $this->getAvgDaysInterval($calendar_id);

        $insert_data = [
            'ID_COMPANY_FK' => $this->company_id,
            'ID_CALENDAR_PK' => $calendar_id,
            'DESC_CALENDAR' => "'$description'",
            'AVG_DAYS_INTERVAL' => $avg_days_interval
        ];

        return DB::Insert('SCM_CAL_RECEIVING', $insert_data);
    }

    public function updateInfos($calendar_id, $description)
    {
        $avg_days_interval = $this->getAvgDaysInterval($calendar_id);

        $insert_update = [
            'DESC_CALENDAR' => "$description",
            'AVG_DAYS_INTERVAL' => $avg_days_interval
        ];

        return DB::Update(
            'SCM_CAL_RECEIVING', 
            $insert_update,
            "WHERE ID_COMPANY_FK = {$this->company_id} AND ID_CALENDAR_PK = :calendar_id",
            "calendar_id={$calendar_id}"
        );
    }

    public function insertDates($calendar_id, $dates)
    {
        $columns = [
            'ID_COMPANY_FK',
            'ID_CALENDAR_FK',
            'DT_RECEIVING_PK',
        ];

        $data = array();

        foreach ($dates as $date) {
            $data[] = [
                $this->company_id,
                $calendar_id,
                "to_date('{$date} 00:00:00', 'yyyy-mm-dd HH24:mi:ss')",
            ];
        }


        return DB::InsertAll('scm_cal_receiving_date', $columns, $data);
    }

    public function getCalendarsbyCompany()
    {
        return DB::Select(
            'scm_cal_receiving',
            "id_calendar_pk,id_calendar_pk || ' - ' || desc_calendar as desc_calendar",
            "WHERE id_company_fk = {$this->company_id}"
        );
    }

    public function getCalendarInfo($calendar_id)
    {
        $columns = "ID_COMPANY_FK,
        ID_CALENDAR_PK,
        DESC_CALENDAR,
        AVG_DAYS_INTERVAL";

        $terms = "WHERE ID_COMPANY_FK = {$this->company_id} AND ID_CALENDAR_PK = {$calendar_id}";

        $query_result = DB::Select(
            'SCM_CAL_RECEIVING',
            $columns,
            $terms
        );

        if ($query_result) {
            return $query_result[0];
        } else {
            return false;
        }
    }

    public function getCalendarDates($calendar_id)
    {
        $columns = "TO_CHAR(DT_RECEIVING_PK, 'yyyy-mm-dd') as calendar_date";

        $terms = "WHERE ID_COMPANY_FK = {$this->company_id} AND ID_CALENDAR_FK = {$calendar_id}";

        $query_result = DB::Select(
            'SCM_CAL_RECEIVING_DATE',
            $columns,
            $terms
        );

        if ($query_result) {
            return $this->prepareDates($query_result);
        } else {
            return false;
        }
    }

    public function deleteCalendar($calendar_id)
    {
        if (!$this->deleteCalendarInfo($calendar_id)) {
            return false;
        }

        if (!$this->deleteCalendarDates($calendar_id)) {
            return false;
        }

        return true;
    }

    public function deleteCalendarDates($calendar_id)
    {
        $terms = "WHERE ID_COMPANY_FK = {$this->company_id} AND ID_CALENDAR_FK = :id";

        return DB::Delete('SCM_CAL_RECEIVING_DATE', $terms, "id={$calendar_id}");
    }

    private function prepareDates($dates)
    {
        $prepared_dates = array();
        foreach($dates as $date){
           $prepared_dates[] = $date['CALENDAR_DATE'];
        }

        return $prepared_dates;
    }

    private function deleteCalendarInfo($calendar_id)
    {
        $terms = "WHERE ID_COMPANY_FK = {$this->company_id} AND ID_CALENDAR_PK = :id";

        return DB::Delete('SCM_CAL_RECEIVING', $terms, "id={$calendar_id}");
    }

    private function getAvgDaysInterval($calendar_id)
    {
        $query_result = DB::Select(
            'vw_scr_scm470f_days_interval',
            'days_interval',
            "WHERE id_company_fk = {$this->company_id} AND id_calendar_fk = {$calendar_id}"
        );

        if($query_result) {
            return $query_result[0]['DAYS_INTERVAL'];
        } else {
            return 0;
        }
    }

}
