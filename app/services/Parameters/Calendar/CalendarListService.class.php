<?php

/**
 * <b>CalendarListService</b>
 * Service responsible for managing Calendars List methods
 *
 * @copyright (c) 2018, Alef Alves | Bionexo
 */
class CalendarListService
{
    protected $user;
    protected $company_id;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * getData
     *
     * @param INT $page
     * @param INT $limit
     * @param STRING $terms
     * @param STRING $order
     * @return ARRAY/BOOLEAN
     */
    public function getData($page, $limit, $order, $search = null)
    {
        $search = $search ? " AND (ID_CALENDAR_PK LIKE '%{$search}%' OR DESC_CALENDAR LIKE '%{$search}%') " : null;

        $query_result = DB::Page(
            'VW_SCR_SCM471G_SCHEDULE',
            Utilities::setOffset($page, $limit),
            $limit,
            'ID_COMPANY_FK,
            ID_CALENDAR_PK,
            DESC_CALENDAR,
            DT_RECEIVING_PK,
            TOTAL_SKUS,
            AVG_DAYS_INTERVAL',
            "WHERE id_company_fk = {$this->company_id}" . $search . $order 
        );

        if ($query_result) {
            return $this->prepareData($query_result);
        }

        return false;
    }

    public function getTotalItems()
    {
        $query_result = DB::Select('SCM_CAL_RECEIVING', 'ID_CALENDAR_PK', "WHERE id_company_fk = {$this->company_id}");

        if ($query_result) {
            return count($query_result);
        } else {
            return 0;
        }
    }

    public function setOrder($orders)
    {
        return DB::prepareOrder($orders);
    }

    private function prepareData($query_result)
    {
        $data = array();
        foreach ($query_result as $row => $rowdata) {
            foreach ($rowdata as $col => $value) {
                $data[$row][strtolower($col)] = $this->formatData($col, $value);
            }
        }

        return $data;
    }

    private function formatData($column, $value)
    {
        if(is_null($value) || $value == 'NULL' || is_array($value)) {
            return null;
        }
        switch (strtolower($column)) {
            case 'dt_receiving_pk':
                return date('d/m/Y', strtotime($value));
                break;
            default:
                return Utilities::dataFormatter($column, $value);
                break;
        }
    }
    

}
