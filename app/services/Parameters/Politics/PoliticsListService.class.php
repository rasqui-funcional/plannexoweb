<?php

/**
 * <b>PoliticsListService</b>
 * Service responsible for managing Politics methods
 *
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class PoliticsListService
{
    protected $user;
    protected $data;

    public function __construct($user)
    {
        $this->user = $user;
    }
    
    /**
     * <b>paginate</b>
     * Retrieve the data paginated.
     * @param STRING $offset.
     * @param STRING $limit.
     * @param STRING $columns.
     * @param STRING $terms.
      * @return ARRAY
     */
    public function paginate($offset, $limit, $order, $search = '')
    {
        $columns = " /*+FIRST_ROWS*/ ID_PROFILE_PK, COD_PROFILE, DESC_PROFILE, QUANTIDADE, SIT_MIN_MAX_ADD_ERP, DESC_CALENDAR, next_date, last_date, lt_planner, lt_buyer, lt_supplier, lt_receiving, lt_others, min_limit_bottom, max_limit_bottom, service_level, days_min";
        $table = "VW_SCR_SCM400G_PROFILE_X_QTD";
        if ($order == "") {
            $order = "order by ID_PROFILE_PK DESC";
        }
        
        $terms = "WHERE ID_COMPANY_FK='{$this->user['id_company_pk']}' $search $order";

        $result['data'] = DB::Page($table, $offset, $limit, $columns, $terms);

        if (empty($result['data'])) {
            return [
                'data' => [],
                'columns'=> []
            ];
        }
        
        $result['columns'] = array_keys($result['data'][0]);

        return $result;
    }
        
    /**
     * <b>select</b>
     * Retrieve the data paginated.
     * @param STRING $offset.
     * @param STRING $limit.
     * @param STRING $columns.
     * @param STRING $terms.
     * @return ARRAY
     */
    public function select($order, $search)
    {
        $columns = " /*+FIRST_ROWS*/ ID_PROFILE_PK, COD_PROFILE, DESC_PROFILE, QUANTIDADE, SIT_MIN_MAX_ADD_ERP, desc_calendar, next_date, last_date, lt_planner, lt_buyer, lt_supplier, lt_receiving, lt_others, min_limit_bottom, max_limit_bottom, service_level, days_min";
        $table = "VW_SCR_SCM400G_PROFILE_X_QTD";

        if ($order == "") {
            $order = "order by ID_PROFILE_PK DESC";
        }

        $terms = "WHERE ID_COMPANY_FK='{$this->user['id_company_pk']}' $search $order";

        $result['data'] = DB::Select($table, $columns, $terms);

        if (empty($result)) {
            return [
                'data' => [],
                'columns'=> []
            ];
        }
        
        $result['columns'] = array_keys($result['data'][0]);

        return $result;
    }

    public function update($politicsData, $user)
    {
        $this->data = $politicsData;
        $this->updateLocalDisp($this->data, $user);

        unset($this->data['cod_local']);

        $table = "SCM_PROFILE";
        $terms = "WHERE ID_COMPANY_FK = :companyId AND ID_PROFILE_PK= :politicsId";
        $parse = "companyId={$user['id_company_pk']}&politicsId={$this->data['ID_PROFILE_PK']}";
        $result = DB::Update($table, $this->data, $terms, $parse);
        return $result;
    }

    public function save($politicsData, $user)
    {
        $this->data = $politicsData;
        $this->data['ID_PROFILE_PK'] = $this->newId();
        $this->data['ID_COMPANY_FK'] = $user['id_company_pk'];

        $this->updateLocalDisp($this->data, $user);
        unset($this->data['cod_local']);
        $table = "SCM_PROFILE";

        return DB::Insert($table, $this->data);
    }

    public function newId()
    {
        $newPoliticsId = DB::Select("SCM_PROFILE", "MAX (ID_PROFILE_PK) as lastId");
        return (int)$newPoliticsId[0]['LASTID'] + 1;
    }


    public function show($politicsId, $user)
    {
        $columns = "ID_COMPANY_FK, ID_PROFILE_PK, COD_PROFILE, DESC_PROFILE, MIN_LIMIT_BOTTOM, MAX_LIMIT_BOTTOM, DAYS_MIN, DAYS_MAX, LT_RECEIVING, LT_OTHERS, LT_SUPPLIER_EXCEPTION, LT_BUYER_EXCEPTION, LT_PLANNER_EXCEPTION, LT_RECEIVING_EXCEPTION, LT_OTHERS_EXCEPTION, LT_SUPPLIER, LT_BUYER, LT_PLANNER, SCHEDULE, BATCH_ORDER, SERVICE_LEVEL, DAYS_HORIZON, SIT_ONLY_FIRST_PARC, ID_CALENDAR_FK, SIT_PARC_IN_LT, SIT_MIN_MAX_ADD_ERP, M_CALENDAR, M_LT";
        $table = "SCM_PROFILE"; 
        $terms = "WHERE ID_PROFILE_PK='{$politicsId}' AND ID_COMPANY_FK='{$user['id_company_pk']}'";
        $data = DB::Select($table, $columns, $terms)[0];
        $result = [
            'data' => $data,
            'cod_local_selected' => $this->showLocalDisp($data),
            'receiving_days' => $this->retrieveReceivingDays($data),
            'AVG_DAYS_INTERVAL' => $this->retrieveReceiving($data),
            'count_skus' => $this->countSkus($data),
            'calendar_selected' =>$this->retrieveCalendar($data),
        ];

        return  $result; 
    }

    public function duplicate($rows, $user)
    {
        $result = [];
        foreach ($rows as $row) {
            array_push($result, $this->save($row, $user));
        }

        return $result;
    }

    public function delete($id, $user)
    {
        $deletedDisplLocals = $this->deleteDispLocal($id, $user);
        if(!$deletedDisplLocals) {
            return false;
        }

        $terms = "WHERE ID_COMPANY_FK = :companyId AND ID_PROFILE_PK= :politicsId";
        $parse = "companyId={$user['id_company_pk']}&politicsId={$id}";
        return DB::Delete("SCM_PROFILE", $terms, $parse);
    }

    public function deleteDispLocal($id, $user)
    {
        $terms = "WHERE ID_COMPANY_FK = :companyId AND ID_PROFILE_FK= :politicsId";
        $parse = "companyId={$user['id_company_pk']}&politicsId={$id}";
        return DB::Delete("SCM_PROFILE_LOCAL_DISP", $terms, $parse);
    }

    private function retrieveCalendar($data)
    {
        $terms = "WHERE ID_CALENDAR_PK='{$data['ID_CALENDAR_FK']}' AND ID_COMPANY_FK='{$data['ID_COMPANY_FK']}'";
        $result = DB::Select('scm_cal_receiving', " id_calendar_pk,id_calendar_pk || ' - ' || desc_calendar  as desc_calendar", $terms);
        return $result;
    }

    private function retrieveReceivingDays($data)
    {
        $terms = "WHERE ID_CALENDAR_FK='{$data['ID_CALENDAR_FK']}' AND ID_COMPANY_FK='{$data['ID_COMPANY_FK']}'";
        $result = DB::Select('SCM_CAL_RECEIVING_DATE', "to_char(DT_RECEIVING_PK,'dd/mm/yyyy') as DT_RECEIVING_PK", $terms);
        return $result;
    }

    private function countSkus($data)
    {
        $terms = "WHERE ID_PROFILE='{$data['ID_PROFILE_PK']}' AND ID_COMPANY_FK='{$data['ID_COMPANY_FK']}'";
        $result = DB::Select('vw_scm_aux_sku', "count(id_sku_pk) as count", $terms)[0]['COUNT'];
        return $result;
    }

    private function retrieveReceiving($data)
    {
        $terms = "WHERE ID_CALENDAR_PK='{$data['ID_CALENDAR_FK']}' AND ID_COMPANY_FK='{$data['ID_COMPANY_FK']}'";
        $result = DB::Select('SCM_CAL_RECEIVING', "AVG_DAYS_INTERVAL", $terms);
        return $result[0]['AVG_DAYS_INTERVAL'] == null ? 0 : $result[0]['AVG_DAYS_INTERVAL'];
    }

    private function showLocalDisp($data)
    {
        $terms = "WHERE ID_PROFILE_FK='{$data['ID_PROFILE_PK']}' AND ID_COMPANY_FK='{$data['ID_COMPANY_FK']}'";
        $result = DB::Select('SCM_PROFILE_LOCAL_DISP', "COD_LOCAL_FK", $terms);
        $ids = []; 
        foreach ($result as $value) {
            array_push($ids, $value['COD_LOCAL_FK']);
        }

        return $ids;
    }

    private function updateLocalDisp($data, $user)
    {
        $terms = "WHERE ID_COMPANY_FK = :companyId AND ID_PROFILE_FK= :politicsId";
        $parse = "companyId={$user['id_company_pk']}&politicsId={$data['ID_PROFILE_PK']}";
        $result = DB::Delete("SCM_PROFILE_LOCAL_DISP", $terms, $parse);
        if (!$result) {
            return false;
        }

        return $this->insertProfileLocalDisp($data, $user);
    }


    public function insertProfileLocalDisp($data, $user)
    {
        $result = [];
        foreach ($data['cod_local'] as $value) {
            $result[$value] = DB::Insert("SCM_PROFILE_LOCAL_DISP", [
                'ID_COMPANY_FK' => $user['id_company_pk'],
                'ID_PROFILE_FK' => $data['ID_PROFILE_PK'],
                'COD_LOCAL_FK' => "'{$value}'"
            ]);
        }
        return $data;
    }
}
