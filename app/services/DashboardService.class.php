<?php

/**
 * <b>DashboardService.class:</b>
 * Service to control all the dashboard filters;
 *
 */
class DashboardService
{
    CONST VALSUPPLIER = 1;

    public function __construct($user_session)
    {
        $this->company_id = $user_session['id_company_pk'];
        $this->user = $user_session['user_id'];
        $this->session = $user_session;
    }

    /**
     * <b>buildIndicators</b>
     * Build the Indicators filters.
     * get
     * Get the Indicators data.
     * @param $params
     * @param null $data
     * @return array
     */
    public function buildIndicators($params, $data = null, $manager_users)
    {
        $result = [
            'urgentes' => $this->getSitUrgent($params, $manager_users),
            'prox_parcel' => $this->getSuggestUpcomming7Days($params, $manager_users),
            'ordens' => $this->getDelayedOrders($params, $data, $manager_users),
            'parcel_apr' => $this->getSuggestLastApproved7Days($params, $data, $manager_users),
        ];

        return $result;
    }

    /**
     * get
     * Get the Details data.
     * @return ARRAY
     */
    public function buildDetails($Where, $manager_users)
    {
        $is_supplier = Gate::setSupplier('a.', self::VALSUPPLIER);
        $handled_manager_users = $this->handleManagerUsers('a.', $manager_users);
        $estabIn = $this->getEstabByUser('a');

        $Query = "SELECT
                        a.inv_level,
                        NVL(COUNT(id_sku_pk), 0) AS skus,
                        NVL(SUM(saldo), 0) AS saldo,
                        NVL(SUM(qty_excess), 0) AS qty_excess,
                        NVL(SUM(req_purch), 0) AS req_purch,
                        NVL(SUM(purch_order), 0) AS purch_order
                    FROM
                        (
                        SELECT
                            ROWNUM-1 inv_level
                        FROM
                            (
                            SELECT
                                1 a
                            FROM
                                dual
                            CONNECT BY
                                LEVEL <= 6 ) ) a
                    LEFT JOIN (
                        SELECT
                            a.id_company_fk,
                            a.cod_estab_fk,
                            a.id_sku_pk,
                            a.cod_item_pk,
                            a.desc_item,
                            a.id_profile,
                            a.inv_level,
                            a.curve_abc,
                            a.curve_pqr,
                            a.curve_xyz,
                            a.curve_123,
                            a.cod_group1_fk,
                            a.cod_group2_fk,
                            a.cod_group3_fk,
                            a.cod_group4_fk,
                            a.cod_group5_fk,
                            a.cod_group6_fk,
                            a.cod_group7_fk,
                            a.cod_group8_fk,
                            a.cod_group9_fk,
                            a.cod_group10_fk,
                            a.cod_group11_fk,
                            a.cod_group12_fk,
                            COUNT (b.id_sku_fk) AS nro_sku,
                            NVL (SUM (a.inv_available * a.val_unit_erp),
                            0) saldo,
                            NVL (SUM (a.purchase_req * a.val_unit_erp),
                            0) req_purch,
                            NVL (SUM (a.purchase_order * a.val_unit_erp),
                            0) purch_order,
                            NVL (SUM (a.qty_excess * a.val_unit_erp),
                            0) qty_excess
                        FROM
                            vw_scm_aux_sku a
                        LEFT JOIN scm_sku_out b ON
                            a.id_company_fk = b.id_company_fk
                            AND a.id_sku_pk = b.id_sku_fk
                        WHERE
                            sit_sku = 1
                            {$handled_manager_users}
                            {$is_supplier}
                            {$estabIn}
                        GROUP BY
                            a.id_company_fk,
                            a.cod_estab_fk,
                            a.id_sku_pk,
                            a.cod_item_pk,
                            a.desc_item,
                            a.id_profile,
                            a.inv_level,
                            a.curve_abc,
                            a.curve_pqr,
                            a.curve_xyz,
                            a.curve_123,
                            a.cod_group1_fk,
                            a.cod_group2_fk,
                            a.cod_group3_fk,
                            a.cod_group4_fk,
                            a.cod_group5_fk,
                            a.cod_group6_fk,
                            a.cod_group7_fk,
                            a.cod_group8_fk,
                            a.cod_group9_fk,
                            a.cod_group10_fk,
                            a.cod_group11_fk,
                            a.cod_group12_fk ) b ON
                        a.inv_level = b.inv_level
                        AND b.id_company_fk = {$this->company_id} " . $this->buildGroupByClauseManager($Where) . "
                    GROUP BY
                        a.inv_level
                    ORDER BY
                        a.inv_level";
        return DB::Procedure($Query, true);
    }

    /**
     * get
     * Get the total level service percentege.
     * @return INT
     */
    public function buildDetailsServicePercentage($Where, $manager_users)
    {
        $is_supplier = Gate::setSupplier('', self::VALSUPPLIER);
        $estabIn = $this->getEstabByUser('a');

        $fields = "NVL(AVG(SERVICE_LEVEL_REAL),0)";
        $table = "VW_SCM_AUX_SKU a";
        $handled_manager_users = $this->handleManagerUsers('a.', $manager_users);
        $terms = "WHERE SIT_SKU = 1 {$Where} AND ID_COMPANY_FK = {$this->company_id} {$estabIn} {$handled_manager_users} {$is_supplier}";

        $PercentageData = DB::Select($table, $fields, $terms);

        if ($PercentageData) {
            return number_format($PercentageData[0]['NVL(AVG(SERVICE_LEVEL_REAL),0)'], 2, ',', '');
        }

        return 0;
    }

    public function buildDetailsDataServicePercentage()
    {
        return date('d/m/Y');
    }
    
    public function detailStockBalancing($where, $manager_users)
    {
        $is_supplier = Gate::setSupplier('a.', self::VALSUPPLIER);
        $estabIn = $this->getEstabByUser('a');

        $columns = 'g.inv_level AS inv_level,
                    nvl(round(SUM(g.qty), 0), 0) AS saldo,
                    ceil(CASE
                        WHEN SUM(g.qty_forecast) = 0 THEN 0
                        ELSE SUM(g.qty) / SUM(g.qty_forecast)
                    END) AS cobertura';

        $table = 'vw_scr_scm456g_gauge_grid g
                    INNER JOIN
                        VW_SCM_AUX_SKU a
                    ON a.id_company_fk = g.id_company_fk
                        AND a.id_sku_pk = g.id_sku_fk
                        AND sit_sku = 1';

        $handled_manager_users = $this->handleManagerUsers('a.', $manager_users);

        $where = $this->defaultWhere($where);
        $terms = "WHERE g.id_company_fk = {$this->company_id} {$where}
                    {$handled_manager_users}
                    {$is_supplier}
                    {$estabIn}
                    GROUP BY g.inv_level, g.id_company_fk
                    ORDER BY inv_level ASC";

        $query_result = DB::Select($table, $columns, $terms);

        return $this->buildDetailStockBalancingResponse($query_result);
    }

    private function buildDetailStockBalancingResponse($query_result)
    {
        $response = [];
        $response['minimum'] = $query_result[0];
        $response['maximum'] = $query_result[1];
        return $response;
    }

    /**
     * <b>filters</b>
     * Retrieve all filters
     * @param INT $selectedFilter selected filter
     * @param INT|ARRAY $user id company | user data
     * @return ARRAY
     */
    public function filter($selectedFilter, $user = null)
    {
        if (!is_numeric($selectedFilter)) {
            return [];
        }

        $id_company_fk = (is_array($user)) ? $user['id_company_pk'] : $this->company_id;

        $columns = "filter_id, filter_nome, filter_perfil, dbms_lob.substr(filter_data, 4000, 1 ) as filter_data";
        $terms = "WHERE filter_id = '{$selectedFilter}' AND ID_COMPANY_FK = '{$id_company_fk}'";

        $result = DB::Select('uac_filters', $columns, $terms);
        if (!$result) {
            return [];
        }

        return $result[0];
    }

    /**
     * @param $where
     * @return mixed|string
     */
    public function defaultWhere($where) :string
    {
        if (strstr($where, 'inv_level')) {
            return str_replace('inv_level', 'g.inv_level', $where);
        }

        return $where.= ' AND g.inv_level in(2,4)';
    }

    public function handleManagerUsers($prefix, $manager_users)
    {
        return str_replace("AND {$prefix}user_id", 'AND user_id', $manager_users);
    }

    private function getEstabByUser($alias = '')
    {
        if (!empty($alias)) {
            $alias .= '.'; 
        }

        if(!empty($this->session['estab'])) {
            return " AND {$alias}COD_ESTAB_FK IN({$this->session['estab']}) ";
        }
        return '';
    }

    private function getSitUrgent($where = '', $manager_users)
    {
        $is_supplier = Gate::setSupplier('', self::VALSUPPLIER);
        
        $columns = 'count(*) as total, sum(qty_altered) as qty, sum(qty_altered * val_unit_erp) as val';
        $table = 'VW_SCR_SCM420G_SUG';
        $estabIn = $this->getEstabByUser();

        $terms = "WHERE id_company_fk = {$this->company_id} {$estabIn} {$manager_users} {$is_supplier} AND num_parc_pk = 1 AND sit_urgent = 1 {$where} AND sit_sku IN('1')";

        $query = DB::SelectRow($table, $columns, $terms);
        return [
            'TOTAL' => $query['TOTAL'],
            'TOTAL_VAL' => $query['VAL']
        ];
    }

    private function getSuggestUpcomming7Days($where = '', $manager_users)
    {
        $is_supplier = Gate::setSupplier('', self::VALSUPPLIER);

        $current = (new DateTime())->format('Y-m-d');
        $upcoming = (new DateTime('+7 days'))->format('Y-m-d');

        $columns = 'count(*) as total, sum(qty_altered) as qty, sum(qty_altered * val_unit_erp) as val';
        $table = 'VW_SCR_SCM420G_SUG';
        $estabIn = $this->getEstabByUser();
        $terms = "WHERE id_company_fk = {$this->company_id} {$estabIn} {$manager_users} {$is_supplier} AND num_parc_pk = 1 
        AND DATE_ARRIVAL_ALTERED between to_date('{$current} 00:00:00', 'YYYY/MM/DD HH24:MI:SS') AND to_date('{$upcoming} 23:59:59', 'YYYY/MM/DD HH24:MI:SS') 
        {$where}";

        $query = DB::SelectRow($table, $columns, $terms);
        return [
            'TOTAL' => $query['TOTAL'],
            'TOTAL_VAL' => $query['VAL']
        ];
    }

    private function getSuggestLastApproved7Days($where = '', $data, $manager_users)
    {
        $estabIn = $this->getEstabByUser('S');
        $where = !is_null($data) ? $this->mergeWhere($data, 'S') : $this->addAlias($where, 'S');
        $is_supplier = Gate::setSupplier('S.', self::VALSUPPLIER);
        $columns = 'COUNT(*) AS TOTAL, SUM(HI.QTY_ALTERED) AS QTY, SUM(HI.QTY_ALTERED * S.VAL_UNIT_ERP) AS QTY_VAL';
        $table = 'VW_SCM_AUX_SKU S';
        $terms = "INNER JOIN SCM_SKU_PURCHASE_SUG_HIST HI
                    ON S.ID_COMPANY_FK = HI.ID_COMPANY_FK AND S.ID_SKU_PK = HI.ID_SKU_FK
                  INNER JOIN SCM_SKU_PURCHASE_SUG_HEADER HE
                    ON HI.ID_COMPANY_FK = HE.ID_COMPANY_FK AND HI.ID_INTEGRATION_GTP = HE.COD_HEADER_PK
                  WHERE S.ID_COMPANY_FK = {$this->company_id} {$manager_users} {$is_supplier}
                    AND TRUNC(HI.DATE_INTEGRATION) BETWEEN TRUNC(CURRENT_DATE - 7) AND TRUNC(CURRENT_DATE)
                    AND HE.SIT_HEADER = 2
                    
        {$where} {$estabIn}";

        $query = DB::SelectRow($table, $columns, $terms);
        return [
            'TOTAL' => $query['TOTAL'],
            'TOTAL_VAL' => $query['QTY_VAL']
        ];
    }

    private function getDelayedOrders($where = '', $data, $manager_users)
    {
        $estabIn = $this->getEstabByUser('S');
        $where = !is_null($data) ? $this->mergeWhere($data, 'S') : $this->addAlias($where, 'S');
        $where = str_replace('cod_item_pk', 'cod_item', $where);

        $is_supplier = Gate::setSupplier('S.', self::VALSUPPLIER);

        $columns = '
            COUNT(S.id_order_pk)       AS TOTAL,
            ROUND(SUM(S.val_total), 2) AS TOTAL_VAL
        ';

        $table = '
            VW_SCR_SCM800G_FOLLOW_UP S
                INNER JOIN scm_estab b
                    ON S.id_company_fk = b.id_company_fk
                        AND b.cod_estab_pk = S.cod_estab_fk
        ';

        $terms = "
            WHERE S.ID_COMPANY_FK = {$this->company_id}
                {$manager_users}
                {$is_supplier}
                {$where}
                {$estabIn}
                AND S.SIT_SKU IN ('1')
                AND S.ind_ord_atraso = 1
        ";
        return DB::SelectRow($table, $columns, $terms);
    }

    private function mergeWhere($columns, $alias = null)
    {
        $places = '';
        if (!key_exists('FILTER_DATA', $columns)) {
            return $places;
        }

        $columns['FILTER_DATA'] = json_decode($columns['FILTER_DATA'], true);
        foreach ($columns['FILTER_DATA'] as $column => $values) {
            if ($this->notNull($column) && $this->notNull($values)) {
                $places .= ' AND ';
                $places .= is_array($values) ?
                    $this->prepareWhereIn($column, $values, $alias) : $this->prepareWhereLike($column, $values, $alias);
            }
        }

        return $places;
    }

    private function prepareWhereIn($col, $items, $extra)
    {
        $arr = [];
        foreach ($items as $key => $value) {
            array_push($arr, "'{$value}'");
        }

        $prepare_place = sprintf(
            "%s IN (%s)",
            is_null($extra) ? $col : $extra . '.' . $col,
            implode(', ', $arr)
        );

        return $prepare_place;
    }

    private function prepareWhereLike($column, $values, $alias)
    {
        return sprintf(
            "%s LIKE '%%%s%%'",
            is_null($alias) ? $column : $alias . '.' . $column,
            trim($values)
        );
    }

    private function addAlias($where, $alias)
    {
        $where = str_ireplace("cod_estab_fk", "{$alias}.cod_estab_fk", $where);
        $where = str_ireplace("cod_item_pk", "{$alias}.cod_item_pk", $where);
        $where = str_ireplace("user_id", "{$alias}.user_id", $where);
        return $where;
    }

    private function notNull($value)
    {
        if ($value == '' || is_null($value)) {
            return false;
        }

        return true;
    }

    private function buildGroupByClauseManager($clause)
    {
        $where = [];
        $strings = explode('AND', trim($clause));
        foreach ($strings as $string) {
            if (strpos($string, 'user_id') === false) {
                array_push($where, $string);
            }
        }

        $clause = implode(' AND', $where);
        return str_replace('AND ', 'AND b.', $clause);
    }
}
