<?php

/**
 * <b>LogService.class:</b>
 * Service to control all the log of an SKU;
 * @param INT $companyId
 * @param INT $sku
 */
class LogService
{
    private $company_id;
    private $sku_id;

    public function __construct($company_id, $sku_id)
    {
        $this->sku_id = $sku_id;
        $this->company_id = $company_id;
    }

    public function getTableData($offset, $order, $limit)
    {
        $columns = "/*+FIRST_ROWS*/ id, inserted_date, username, ip_user, application, action as sc_field_0, description";
        $table = "UAC_LOG";
        $terms = " where description like '%id_company_fk : $this->companyId||%' AND (description like '%id_sku_pk : $this->sku||%' OR description like '%id_sku_fk : $this->sku||%') $order";
        $result = DB::Page($table, $offset, $limit, $columns, $terms);

        return $result;
    }

    public function getTableFullDataBySkuId($skuId)
    {
        $terms = " WHERE (L.ID_COMPANY_FK = {$this->company_id} AND (L.DESCRIPTION_JSON LIKE '%\"id\":{$skuId}%') OR (L.DESCRIPTION_JSON LIKE '%\"id_sku\":{$skuId}%')) OR (description like '%id_company_fk : {$this->company_id}||%' AND (description like '%id_sku_pk : {$skuId}||%' OR description like '%id_sku_fk : {$skuId}||%'))";
        $columns = ' 
                    DISTINCT LOG.ID,
                    LOG.INSERTED_DATE,
                    LOG.IP_USER,
                    LOG.APPLICATION,
                    LOG.ACTION,
                    LOG.DESCRIPTION,
                    LOG.CREATOR,
                    UAC_USER.NAME AS USERNAME';
        $table = "({$this->getColumns()}  {$terms}) LOG INNER JOIN UAC_USER ON LOG.ID_USER = UAC_USER.ID_USER_PK WHERE ID_COMPANY_FK = {$this->company_id} {$order} ";
        $result = DB::Select($table, $columns);
        return $result;
    }

    public function fetch($offset, $order, $limit)
    {
        $order = empty(trim($order)) ? 'ORDER BY ID DESC' : $order;
        $terms = " WHERE (L.ID_COMPANY_FK = {$this->company_id} AND (L.DESCRIPTION_JSON LIKE '%\"id\":{$this->sku_id}%') OR (L.DESCRIPTION_JSON LIKE '%\"id_sku\":{$this->sku_id}%')) OR (description like '%id_company_fk : $this->company_id||%' AND (description like '%id_sku_pk : $this->sku_id||%' OR description like '%id_sku_fk : $this->sku_id||%'))";
        $columns = ' 
                    DISTINCT LOG.ID,
                    LOG.INSERTED_DATE,
                    LOG.IP_USER,
                    LOG.APPLICATION,
                    LOG.ACTION,
                    LOG.DESCRIPTION,
                    LOG.CREATOR,
                    UAC_USER.NAME AS USERNAME';
        $table = "({$this->getColumns()}  {$terms}) LOG INNER JOIN UAC_USER ON LOG.ID_USER = UAC_USER.ID_USER_PK WHERE ID_COMPANY_FK = {$this->company_id} {$order} ";

        return DB::Page($table, $offset, $limit, $columns);
    }

    public function countSkuLog()
    {
        $columns = 'COUNT(DISTINCT LOG.ID) AS ID';
        $terms = " WHERE (L.ID_COMPANY_FK = {$this->company_id} AND (L.DESCRIPTION_JSON LIKE '%\"id\":{$this->sku_id}%') OR (L.DESCRIPTION_JSON LIKE '%\"id_sku\":{$this->sku_id}%')) OR (description like '%id_company_fk : $this->company_id||%' AND (description like '%id_sku_pk : $this->sku_id||%' OR description like '%id_sku_fk : $this->sku_id||%')) ";
        $table = "({$this->getColumns()}  {$terms}) LOG INNER JOIN UAC_USER ON LOG.ID_USER = UAC_USER.ID_USER_PK WHERE ID_COMPANY_FK = {$this->company_id} ";
        $query_result = DB::SelectRow($table, $columns);

        return (int)$query_result['ID'];
    }

    public function listFooterPages($quantity_pages, $page_number)
    {
        $list_footer_pages = [];

        if ($page_number === 1) {
            $list_footer_pages['list'] = $this->buildStartList($quantity_pages);
            $list_footer_pages['condiction'] = 'start';
        } else {
            if ($page_number === $quantity_pages) {
                $list_footer_pages['list'] = $this->buildEndList($quantity_pages);
                $list_footer_pages['condiction'] = 'end';
            } else {
                if ($page_number > 1 && $page_number < $quantity_pages) {
                    $list_footer_pages['list'] = $this->buildMiddleList($page_number);
                    $list_footer_pages['condiction'] = 'middle';
                } else {
                    // nothing todo
                }
            }
        }

        return $list_footer_pages;
    }

    private function buildStartList($quantity_pages)
    {
        $init_list = [];
        $MAX = ($quantity_pages > 3 ? 3 : $quantity_pages);

        for ($index = 1; $index <= $MAX; $index++) {
            array_push($init_list, strval($index));
        }

        return $init_list;
    }

    private function buildEndList($quantity_pages)
    {
        $init_list = [];
        $init = ($quantity_pages >= 3 ? $quantity_pages - 2 : $quantity_pages - 1);

        for ($index = $init; $index <= $quantity_pages; $index++) {
            array_push($init_list, strval($index));
        }

        return $init_list;
    }

    private function buildMiddleList($page_number)
    {
        return [$page_number - 1, $page_number, $page_number + 1];
    }

    private function getColumns()
    {
        return "
                SELECT
                    /*+FIRST_ROWS*/ DISTINCT L.ID, 
                    TO_CHAR(L.INSERTED_DATE, 'DD/MM/YYYY HH24:MI:SS') INSERTED_DATE, 
                    L.IP_USER,
                    CASE WHEN CREATOR = 'Plannexo' THEN 
                        L.USERNAME
                    ELSE 
                        REGEXP_SUBSTR (L.USERNAME,'[^.]+',1,2)
                    END AS ID_USER,
                    L.APPLICATION, 
                    L.ACTION, 
                    CASE WHEN CREATOR = 'Plannexo' THEN
                        dbms_lob.substr(L.DESCRIPTION_JSON, 10000, 1) 
                    ELSE
                        dbms_lob.substr(L.DESCRIPTION, 10000, 1) 
                    END
                     AS DESCRIPTION,
                    CASE WHEN CREATOR = 'Plannexo' THEN
                        'Plannexo 2.0' 
                    ELSE
                        'Plannexo 1.0' 
                    END AS CREATOR
                FROM UAC_LOG L
               ";
    }
}
