<?php

/**
 * <b>DashboardInfoService.class:</b>
 * Service to control all the dashboard of Info SKU;
 * @param INT $companyId
 * @param INT $skuId
 */
class DashboardInfoService
{
    private $companyId;
    private $skuId;
    private $lang;

    public function __construct($companyId, $skuId)
    {
        global $lang;
        $this->lang = $lang;
        $this->skuId = $skuId;
        $this->companyId = $companyId;
    }

    /**
     * getHeader
     * Get data of the header, next installment and late orders on InfoSKU
     *
     * @return ARRAY
     */
    public function getHeader()
    {
        $result = $this->getHeaderData();

        $Data['header'] = [
            'item' => $result['COD_ITEM_PK'],
            'description' => $result['DESC_ITEM'],
            'estab' => $result['COD_ESTAB_PK'] . ' - ' . $result['DESC_ESTAB'],
            'cod_estab' => $result['COD_ESTAB_PK'],//usado na aba ERP
            'curve' => $result['CURVE_ABC'] . $result['CURVE_PQR'] . $result['CURVE_XYZ'] . $result['CURVE_123'],
            'unity' => $result['DESC_STD_UNIT'],
            'politics' => $result['COD_PROFILE'],
            'category' => $result['CATEGORY'],
            'sin' => $result['INV_LEVEL'],
            'status' => $this->checkStatus($result['SIT_SKU']),
            'status_erp' => $this->checkStatus($result['SIT_SKU_ERP']),
            'days_horizon' => $result['DAYS_HORIZON'],
            'val_unit_erp' => (float) $result['VAL_UNIT_ERP'],
            'manager_login' => $result['LOGIN'],
        ];

        $Data['next_installment'] = [
            'arrear_days' => $this->checkArrearDays($result['PP_ATRASO']),
            'quantity' => $result['PP_QTY_ALTERED'],
            'total' => $this->formatField($result['PP_VAL_QTY_ALTERED'], 'Currency'),
        ];

        $Data['orders'] = $this->getLateOrders($result);

        return $Data;
    }

    /**
     * getLateOrders
     * Prepare the LateOrders data
     * @param ARRAY $lateOrdersData
     * @return ARRAY
     */
    private function getLateOrders($lateOrdersData)
    {
        if (!$lateOrdersData) {
            return [
                'ordens_atrasadas' => 0,
                'fornecedor' => '',
                'qtde' => 0,
                'codigo' => '',
                'atraso' => 0,
            ];
        }

        return [
            'ordens_atrasadas' => $lateOrdersData['ORDENS_ATRASADAS'],
            'fornecedor' => $lateOrdersData['fornecedor'],
            'qtde' => $lateOrdersData['qtde'],
            'codigo' => $lateOrdersData['codigo'],
            'atraso' => round($lateOrdersData['atraso'], 1),
        ];
    }

    /**
     * <b>getHeaderData</b>
     * Get on database the data used on infosku header, next installment and late orders
     * @return ARRAY
     */
    public function getHeaderData()
    {
        $columns = ' id_company_fk,
                    cod_item_pk,
                    desc_item,
                    cod_estab_pk,
                    desc_estab,
                    curve_abc,
                    curve_pqr,
                    curve_xyz,
                    curve_123,
                    desc_std_unit,
                    cod_profile,
                    category,
                    inv_level,
                    sit_sku,
                    sit_sku_erp,
                    pp_qty_altered,
                    pp_val_qty_altered,
                    pp_atraso,
                    ordens_atrasadas,
                    oa_desc_supplier as "fornecedor",
                    oa_qty_pending as "qtde",
                    oa_num_order "codigo",
                    oa_atraso as "atraso",
                    days_horizon,
                    val_unit_erp,
                    login';
        $table = ' vw_scm_info_sku';
        $terms = "WHERE id_company_fk={$this->companyId} AND id_sku_pk={$this->skuId}";

        $result = DB::Select($table, $columns, $terms);
        return $result[0];
    }


    /**
     * <b>checkStatus</b>
     * Formata o campo de status do SKU, que no banco é boolean.
     * @param bool status
     * @return string
     */
    private function checkStatus($status)
    {
        if (!isset($status) || is_null($status)) {
            return '';
        }
        return $status == 0 ? $this->lang['info_sku']['status_inativo'] : $this->lang['info_sku']['status_ativo'];
    }


    /**
     * <b>checkArrearDays</b>
     *
     * Checa a validade do campo 'dias de atraso', e formata caso ele seja menor que zero
     * @param int value
     * @return int
     */
    public function checkArrearDays($value)
    {
        if (!is_numeric($value) || $value < 0) {
            return 0;
        }

        return $value;
    }


    /**
     * <b>formatField</b>
     * Formata o valor recebido com a ajuda da classe Utilities
     * @param int value
     * @param int method
     * @return string
     */
    public function formatField($value, $method)
    {
        return Utilities::$method($value);
    }
}
