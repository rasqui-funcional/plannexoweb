<?php

/**
 * <b>InformationService</b>
 * Service to control all the information tab on Info SKU.
 * @param INT $companyId
 */
class InformationService
{
    private $company_id;
    private $sku_id;
    public $sku_calendar_id;
    private $lang;

    public function __construct($company_id)
    {
        global $lang;
        $this->lang = $lang;
        $this->company_id = $company_id;
        $this->sku_id = (int) Request::get('skuID');
        $this->sku_calendar_id = null;
    }

    public function getInterdependenceData()
    {
        $fields = "
            I.COD_ITEM_PK,
            I.cod_estab_pk,
            I.desc_estab,
            S.V_ID_DEPEND_SKU,
            S.V_DEPEND_DATE_VALID,
            S.V_DEPEND_PERC_OUT,
            s.V_DEPEND_PROFILE_FLAG,
            S.V_ID_PROFILE,
            S.V_DEPEND_SUM_INV,
            I.COD_PROFILE
        ";

        $table = "
            VW_SCM_AUX_SKU S
            INNER JOIN
            VW_SCM_INFO_SKU I
            ON S.V_ID_DEPEND_SKU = I.ID_SKU_PK
            AND s.id_company_fk = i.id_company_fk
        ";

        $terms = "WHERE S.ID_COMPANY_FK = {$this->company_id} AND S.ID_SKU_PK = {$this->sku_id}";

        $query_result = DB::SelectRow($table, $fields, $terms);
        return $query_result;
    }

    public function getInheritProfile($idSku)
    {
        $terms = "
        ID_COMPANY_FK = {$this->company_id}
        AND COD_ITEM_PK = '{$idSku}'
        AND COD_ESTAB_FK = '1'";
        $fields = "ID_PROFILE_FK";
        $table = "SCM_SKU";
        $policy = DB::Select($table,$fields,$terms);

        return $policy;
    }

    public function updateInheritProfile($idSku)
    {
        $data = [
            "V_ID_PROFILE" => $this->getInheritProfile($idSku),
        ];
        $table = "SCM_SKU_IN";
        $terms = "WHERE ID_COMPANY_FK = {$this->company_id} AND ID_SKU_FK = {$this->sku_id}";
        DB::Update($table,$data,$terms);
    }

    public function buildBasicInformations()
    {
        $data = [];
        $info = $this->buildInfo();
        $first_element = $info[0];
        $formatedBasicInformation = $this->prepareCodGroupData($info[0]);

        $data['base'] = $formatedBasicInformation;
        $data['managers'] = $this->buildInfoManager();
        $data['standardDeposits'] = $this->buildInfoStandartDeposit($first_element);
        $data['observation'] = $this->buildInfoObservation($first_element);
        $data['groupings'] = $this->buildOptionsByGroupings($this->buildGroupings());

        return $data;
    }

    /**
     * Handler for validating policy values when updating or creating.
     * @param Array $data Array with values for validation
     * @return bool
     */
    protected function validatePolicyValues(array $data) : bool
    {
        if (!$this->validateMinimumLotValue($data)) {
            return false;
        }
        return true;
    }

    /**
     * Check if v_order_min (minimum lot) is an integer above 0
     * @param array Array $data
     * @return bool
     */
    protected function validateMinimumLotValue(array $data) : bool
    {
        return (isset($data['v_order_min']) && (empty($data['v_order_min']) || !filter_var($data['v_order_min'], FILTER_VALIDATE_INT))) !== true;
    }

    public function updatePolicy($post)
    {
        $data = $this->buildPolicyDataUpdate($post);
        $current_date = $data['v_date_cons_expiration'];

        if ($current_date === 'null') {
            $current_date = null;
            $data['v_date_cons_expiration'] = $current_date;
        }

        if (!$this->validatePolicyValues($data)) {
            return false;
        }

        $table = 'scm_sku_in';
        $terms = "WHERE ID_SKU_FK = :id AND id_company_fk = :company_id";
        $parse_string = "id={$this->sku_id}&company_id={$this->company_id}";

        if (!is_null($current_date)) {
            unset($data['v_date_cons_expiration']);
            $date = null;

            if (count($data) > 0) {
                $date .= ', ';
            }

            $date_time = new DateTime($current_date);
            $date .= "v_date_cons_expiration=to_date('{$date_time->format('d/m/Y h:i:s')}', 'DD/MM/YYYY HH:MI:SS')";
            $terms = $date . ' ' . $terms;
        }

        $query_result = DB::Update($table, $data, $terms, $parse_string);

        return $query_result;
    }

    private function buildPolicyDataUpdate($post)
    {
        $data = [];
        $policy_keys = array_keys($post);

        foreach ($policy_keys as $policy_key) {
            $data[$policy_key] = $post[$policy_key];
        }

        return $data;
    }

    private function buildInfo()
    {
        return DB::Procedure(
            "select * from VW_SCM_AUX_SKU where id_company_fk = {$this->company_id} and ID_SKU_PK = {$this->sku_id}",
            true
        );
    }

    private function buildInfoManager()
    {
        return DB::Procedure(
            "select id_user_pk, login from uac_user where id_company_fk={$this->company_id} and active = 1",
            true
        );
    }

    private function buildInfoStandartDeposit($info)
    {
        $result = DB::Select(
            'erp_local',
            "cod_local_pk, cod_local_pk || '-' || desc_local as desc_local",
            "where id_company_fk={$this->company_id} AND cod_estab_pk='{$info['COD_ESTAB_FK']}'"
        );

        return $result;
    }

    private function buildInfoObservation($info)
    {
        $result = DB::Select(
            'scm_sku',
            'notes',
            "where id_company_fk={$this->company_id} AND id_sku_pk={$info['ID_SKU_PK']}"
        );

        return $result[0]['NOTES'];
    }

    private function buildGroupings()
    {
        return DB::Procedure(
            "SELECT * FROM SCM_GROUP_NAME WHERE ID_COMPANY_FK = {$this->company_id} AND SIT_GROUP = 1",
            true
        );
    }

    private function buildOptionsByGroupings($groupings)
    {

        // $numGroup = implode(',', array_column($groupings, 'NUM_GROUP_PK'));
        // $options = DB::Select('scm_group_cont a
        //     inner join scm_group_name b
        //     on a.id_company_fk=b.id_company_fk
        //     and a.num_group_fk=b.num_group_pk', 'a.num_group_fk,
        //     a.cod_group_pk,
        //     a.desc_group_cont,
        //     b.auto_group', "where
        //     a.id_company_fk = {$this->company_id}
        //     and b.sit_group = 1
        //     and a.num_group_fk IN ($numGroup) ORDER BY a.num_group_fk");
        // dd($options);
        foreach ($groupings as $key => $group) {
            $groupings[$key]['options'] = DB::Select('scm_group_cont a
                inner join scm_group_name b
                on a.id_company_fk=b.id_company_fk
                and a.num_group_fk=b.num_group_pk', 'a.num_group_fk,
                a.cod_group_pk,
                a.desc_group_cont,
                b.auto_group', "where
                a.id_company_fk = {$this->company_id}
                and b.sit_group = 1
                and a.num_group_fk = {$group['NUM_GROUP_PK']}");
        }

        return $groupings;
    }

    /**
     * <b>getChildrenData</b>
     * Retrieve data that fills 'ITEM FILHO' card's .
     * @param STRING $codEstabPk.
     * @param STRING $idCompanyFk.
     * @param STRING $codItemPk.
     * @return ARRAY
     */
    public function getChildrenData($codItemPk = null, $idCompanyFk = null, $codEstabPk = null)
    {
        $columns = "QTY_INV,DESC_ITEM,COBERTURA,ESTOQUE_FINANCEIRO,SOLICITACOES,ORDENS";
        $table = "vw_scm_item_filho";
        $terms = "
            WHERE
            cod_estab_pk = '{$codEstabPk}'
            AND id_company_fk = {$idCompanyFk}
            AND cod_item_pk = '{$codItemPk}'
            AND ITEM_PAI = 0";

        $data = DB::Select($table, $columns, $terms);

        $parentData = $this->getParentData($codItemPk,$idCompanyFk,$codEstabPk);
        if((empty($data) || $data == NULL) && (empty($parentData) || $parentData == NULL)){
            return [
                'error' => $this->lang['global']['no_items_to_display']
            ];
        }

        $footerData = $this->getChildrenFooter($codItemPk,$idCompanyFk,$codEstabPk);

        return [
            'data' => $data,
            'footer' => $footerData,
            'parent' => $parentData
        ];
    }

    /**
     * <b>getParentData</b>
     * Retrieve data that fills 'ITEM PAI' row in the 'ITEM FILHO' table.
     * @param STRING $codEstabPk.
     * @param STRING $idCompanyFk.
     * @param STRING $codItemPk.
     * @return ARRAY
     */
    public function getParentData($codItemPk, $idCompanyFk, $codEstabPk){
        $columns = "QTY_INV,DESC_ITEM,COBERTURA,ESTOQUE_FINANCEIRO,SOLICITACOES,ORDENS";
        $table = "vw_scm_item_filho";
        $terms = "
            WHERE
            cod_estab_pk = '{$codEstabPk}'
            AND id_company_fk = {$idCompanyFk}
            AND cod_item_pk = '{$codItemPk}'
            AND ITEM_PAI = 1";

        $parentData = DB::Select($table, $columns, $terms);
        return $parentData;
    }

    /**
     * <b>getChildrenFooter</b>
     * Retrieve data that fills 'ITEM FILHO' table's footer.
     * @param STRING $codEstabPk.
     * @param STRING $idCompanyFk.
     * @param STRING $codItemPk.
     * @return ARRAY
     */
    public function getChildrenFooter($codItemPk, $idCompanyFk, $codEstabPk)
    {
        $columns = "sum(qty_inv) as tAvailable, sum(cobertura) as tCoverage, sum(estoque_financeiro) as tInvVal, sum(ordens) as tOrders, sum(solicitacoes) as tReq";
        $table = "vw_scm_item_filho";
        $terms = "
            WHERE
            cod_estab_pk = '{$codEstabPk}'
            AND id_company_fk = {$idCompanyFk}
            AND cod_item_pk = '{$codItemPk}'
            AND ITEM_PAI = 0";

        $footerData = DB::Select($table, $columns, $terms);
        return $footerData;
    }

    public function getCalendar()
    {
        $data = [];
        $data['calendarSkuID'] = $this->getCalendarID();

        if (isset($data['calendarSkuID'])) {
            $data['companyCalendars'] = $this->getCompanyCalendars();
            $data['calendarIntervalAvarage'] = $this->getCalendarIntervalAverage();
            $data['receivingDates'] = $this->getReceivingDates();
        }

        return $data;
    }

    public function getDates()
    {
        $data = [];

        $data['receivingDates'] = $this->getReceivingDates();
        $data['calendarIntervalAverage'] = $this->getCalendarIntervalAverage();

        return $data;
    }

    private function getCalendarID()
    {
        $columns = "id_calendar_fk";
        $table = 'vw_scm_aux_sku';
        $terms = "WHERE id_company_fk = {$this->company_id} AND id_sku_pk = {$this->sku_id}";

        $data = DB::Select($table, $columns, $terms);

        if($data){
            $this->sku_calendar_id = $data[0]['ID_CALENDAR_FK'];
            return $this->sku_calendar_id;
        }else{
            return 0;
        }
    }

    private function getCompanyCalendars()
    {
        $columns = "id_calendar_pk, id_calendar_pk || ' - ' || desc_calendar AS calendar_name";
        $table = 'scm_cal_receiving';
        $terms = "WHERE id_company_fk = {$this->company_id} ORDER BY id_calendar_pk ASC";

        $data = DB::Select($table, $columns, $terms);

        if ($data) {
            return $this->prepareData($data);
        } else {
            return [0,''];
        }
    }

    private function getCalendarIntervalAverage()
    {
        $columns = "AVG_DAYS_INTERVAL";
        $table = 'scm_cal_receiving';
        $terms = "WHERE id_company_fk = {$this->company_id} AND id_calendar_pk = {$this->sku_calendar_id}";

        $data = DB::Select($table, $columns, $terms);

        if ($data) {
            return $data[0]['AVG_DAYS_INTERVAL'];
        } else {
            return 0;
        }
    }

    private function getReceivingDates()
    {
        $columns = "TO_CHAR(dt_receiving_pk, 'YYYY/MM/DD') as dt_receiving_pk";
        $table = 'SCM_CAL_RECEIVING_DATE';
        $terms = "WHERE id_company_fk = {$this->company_id} AND ID_CALENDAR_FK = {$this->sku_calendar_id}";

        $query_result = DB::Select($table, $columns, $terms);
        $query_result = $this->buildCalendarDates($query_result);
        $query_result = $this->buildDatesByYearAndMonth($query_result);
        $query_result = $this->buildCalendarWithMonthAndYaear($query_result);

        return $query_result;
    }

    public function updateCalendar($post) {
        $data = [
            'v_calendar' => $post['vCalendar'],
        ];

        $query_result = DB::Update(
            'scm_sku_in',
            $data,
            "WHERE ID_SKU_FK = :id AND id_company_fk = :company_id",
            "id={$this->sku_id}&company_id={$this->company_id}"
        );

        return $query_result;
    }

    public function getPolicy($skuID = null)
    {
        $data = [];

        $data['policyCodes'] = $this->buildPolicyCodes();
        $data['defaultPolicyCode'] = $this->buildDefaultPolicyCode();
        $data['defaultConsumerProfile'] = $this->buildConsumerProfile($skuID);
        $data['consumerProfiles'] = $this->buildConsumerProfiles();
        $data['policyInfo'] = $this->buildPolicyInfo();
        $id_profile_pk = $data['policyInfo']['ID_PROFILE_PK'];

        $data['policyDeposits'] = $this->buildPolicyDeposits($id_profile_pk);
        $data['unavailableDeposits'] = $this->buildUnavailableDeposits($id_profile_pk);
        $data['manualDataUpdate'] = $this->buildManualDataUpdate();

        return $data;
    }

    public function getPolicyByID($policy_id)
    {
        $data = [];
        $data['policyInfo'] = $this->buildPolicyInfo($policy_id);

        $data['policyDeposits'] = $this->buildPolicyDeposits($policy_id);
        $data['unavailableDeposits'] = $this->buildUnavailableDeposits($policy_id);
        $data['manualDataUpdate'] = $this->buildManualDataUpdate();

        return $data;
    }

    public function establishmentOrigin($params)
    {
        $columns = "s.cod_estab_fk, s.cod_estab_fk || ' - ' || e.desc_estab as desc_estab";

        $table = 'scm_sku s
                      INNER JOIN
                          scm_estab e
                      ON e.cod_estab_pk = s.cod_estab_fk
                          AND e.id_company_fk = s.id_company_fk';

        $terms = "where
                      s.cod_item_pk = '{$params['codItemPK']}'
                      and s.id_sku_pk != {$this->sku_id}
                      and s.id_company_fk = {$this->company_id}";

        $query_result = DB::Select($table, $columns, $terms);

        return $query_result;
    }

    public function updateInterdependence($post)
    {
        if ($post['check'] === 1) {
            $this->updateInheritance();
        }

        $v_depend_profile_flag = ($post['check'] === 'true' ? true : false);
        $v_depend_sum_inv = ($post['check_stock'] === 'true' ? true : false);
        $data = [
            'v_depend_perc_out' => $post['V_DEPEND_PERC_OUT'],
            'v_depend_profile_flag' => ($v_depend_profile_flag ? 1 : 0),
            'v_depend_sum_inv' => ($v_depend_sum_inv ? 1 : 0),
            'v_id_depend_sku' => $this->idDependSku($post)
        ];

        $formated_date = str_replace('/', '-', $post['V_DEPEND_DATE_VALID']);
        $current_date = date('d/m/Y', strtotime($formated_date));
        $date = ", V_DEPEND_DATE_VALID = to_date('{$current_date}','DD/MM/YYYY HH:MI:SS')";

        $query_result = DB::Update(
            'scm_sku_in',
            $data,
            "{$date} WHERE ID_SKU_FK = :id AND id_company_fk = :company_id",
            "id={$this->sku_id}&company_id={$this->company_id}"
        );

        return $query_result;
    }

    public function updateInheritance()
    {
        $data = [
            'v_id_profile' => $this->idProfile()
        ];

        $query_result = DB::Update(
            'scm_sku_in',
            $data,
            "WHERE ID_SKU_FK = :id AND id_company_fk = :company_id",
            "id={$this->sku_id}&company_id={$this->company_id}"
        );

        return $query_result;
    }

    private function buildConsumerProfiles()
    {
        $columns = 'ID_CONS_PK, NAME_CONS';
        $table = 'SCM_CONS_PROFILE';
        $terms = "WHERE id_company_fk = {$this->company_id}";

        return DB::Select($table, $columns, $terms);
    }

    private function idDependSku($post)
    {
        $columns = 'ID_SKU_PK';
        $table = 'scm_sku';
        $terms = "
            WHERE id_company_fk = {$this->company_id}
                  AND cod_item_pk = '{$post['COD_ITEM_PK']}'
                  AND cod_estab_fk = '{$post['COD_ESTAB_PK']}'
        ";

        $query_result = DB::Select($table, $columns, $terms);

        return $query_result[0]['ID_SKU_PK'];
    }

    private function idProfile()
    {
        $columns = 'ID_SKU_PK';
        $table = 'SCM_SKU';
        $terms = "WHERE ID_COMPANY_FK = {$this->company_id} AND COD_ITEM_PK = 198914 AND COD_ESTAB_FK = '1'";

        $query_result = DB::Select($table, $columns, $terms);

        return $query_result;
    }

    public function deleteInterdependence()
    {
        $data = [
            'v_id_depend_sku' => null,
            'v_depend_date_valid' => null,
            'v_depend_perc_out' => null,
            'v_depend_sum_inv' => null,
            'v_depend_date_ref_sku' => null,
            'v_depend_out_flag' => null,
            'v_depend_profile_flag' => null
        ];

        $query_result = DB::Update(
            'SCM_SKU_IN',
            $data,
            "WHERE ID_SKU_FK = :id AND id_company_fk = :company_id",
            "id={$this->sku_id}&company_id={$this->company_id}"
        );

        return $query_result;
    }

    public function basicInformationsUpdate($post)
    {
        $updateResultGroupingsData = $this->updateGroupingsData($post);
        $updateResultScmSku = $this->updateScmSku($post);

        return $updateResultGroupingsData && $updateResultScmSku;
    }

    private function updateGroupingsData($post)
    {
        $data = $this->buildGroupingsData($post);

        $query_result = DB::Update(
            'SCM_SKU',
            $data,
            "WHERE ID_SKU_PK = :id AND id_company_fk = :company_id",
            "id={$this->sku_id}&company_id={$this->company_id}"
        );

        return $query_result;
    }

    private function buildGroupingsData($post)
    {
        $data = [];
        for ($i = 5; $i < 13; $i++) {
            $data['cod_group'. $i .'_fk'] = $post['codGroup'. $i .'FK'];
        }

        return $data;
    }

    private function updateScmSku($post)
    {
        $data = $this->buildScmSkuData($post);
        $current_date = date('d/m/Y h:i:s');
        $date = ", date_notes = to_date('{$current_date}','DD/MM/YYYY HH:MI:SS')";

        $query_result = DB::Update(
            'SCM_SKU',
            $data,
            "{$date} WHERE ID_SKU_PK = :id AND id_company_fk = :company_id",
            "id={$this->sku_id}&company_id={$this->company_id}"
        );

        return $query_result;
    }

    private function buildScmSkuData($post)
    {
        $user_id = $post['userId'];
        $default_local = $post['defaultLocal'];
        $notes = $post['notes'];
        $usu_notes = $this->usuNotes();
        $curve_xyz = $post['curveXYZ'];

        $data = [
            'user_id' => $user_id,
            'default_local' => $default_local === '' ? '#' : $default_local,
            'notes' => $notes,
            'usu_notes' => $usu_notes['ID_USER_PK'],
            'curve_xyz' => $curve_xyz,
            'sit_pending_calc' => 1
        ];

        return $data;
    }

    public function buildSkuData($post)
    {
        foreach ($this->buildGroupingsData($post) as $key => $value) {
            $arr1[strtoupper($key)] = $value;
        }

        foreach ($this->buildScmSkuData($post) as $key => $value) {
            $arr2[strtoupper($key)] = $value;
        }

        $arr2['OBSERVATION'] = $arr2['NOTES'];

        return  array_merge($arr1, $arr2);
    }

    public function buildDeleteInterdependence()
    {
        $arr = [];
        foreach (INTERDEPENDENCE_PARAMS as $param) {
            $arr[$param] = null;
        }

        return $arr;
    }

    public function buildPolicyData($post)
    {
        $arrayCopy = (new ArrayObject($post))->getArrayCopy();
        $arrayCopy['id_profile_fk'] = $post['v_id_profile'];
        unset($arrayCopy['v_id_profile']);

        $arr = [];
        foreach ($arrayCopy as $key => $value) {
            $arr[strtoupper($key)] = $value;
        }

        return $arr;
    }

    private function buildDataAudit($data)
    {
        $buildGroupingsData = $this->buildGroupingsData($data);
        $buildScmSkuData = $this->buildScmSkuData($data);

        return array_merge($buildScmSkuData, $buildGroupingsData);
    }

    private function getOldSkuValues($columns)
    {
        $columns = str_replace('usu_notes,', '', $columns);
        $columns = str_replace('notes,', '', $columns);

        $table = 'VW_SCR_SCM417G_SKU_SKUIN';
        $terms = "WHERE ID_COMPANY_FK = {$this->company_id} AND ID_SKU_PK = {$this->sku_id}";

        $queryResult = DB::Select($table, $columns, $terms);

        return $queryResult[0];
    }

    private function checkPermissionGroupingUpdate()
    {
        $columns = 'num_group_pk, sit_group, auto_group';
        $table = 'scm_group_name';
        $terms = "where id_company_fk = {$this->company_id}";

        $query_result = DB::Select($table, $columns, $terms);

        return $query_result;
    }

    private function usuNotes()
    {
        $columns = 'id_user_pk';
        $table = 'uac_user';
        $terms = "where login = 'sysadmin' and id_company_fk = {$this->company_id}";

        $query_result = DB::Select($table, $columns, $terms);

        return $query_result[0];
    }

    private function buildPolicyCodes()
    {
        $columns = "id_profile_pk, id_profile_pk || ' - ' || cod_profile AS DESC_PROFILE";
        $table = 'scm_profile';
        $terms = "
            WHERE id_company_fk = {$this->company_id}
            ORDER BY ID_PROFILE_PK DESC
        ";

        $query_result = DB::Select($table, $columns, $terms);

        return $query_result;
    }

    private function buildPolicyInfo($policy_id=null)
    {
        $columns = 'id_profile_pk,
            cod_profile,
            desc_profile,
            min_limit_bottom,
            max_limit_bottom,
            days_min,
            lt_receiving,
            lt_others,
            lt_supplier,
            lt_buyer,
            lt_planner,
            service_level,
            id_calendar_fk';

        $table = 'scm_profile';

        $id_profile_pk = $policy_id ?? $this->buildIDProfilePK();
        $terms = "WHERE
            id_profile_pk = ({$id_profile_pk})
            AND id_company_fk = {$this->company_id}
            ORDER BY id_profile_pk";

        $query_result = DB::Select($table, $columns, $terms);

        return $query_result[0];
    }

    private function buildDefaultPolicyCode()
    {
        $columns = 'id_profile_fk';
        $table = 'scm_sku';
        $terms = "where id_sku_pk = {$this->sku_id} and id_company_fk = {$this->company_id}";

        $query_result = DB::Select($table, $columns, $terms);

        return $query_result[0];
    }

    private function buildIDProfilePK()
    {
        $columns = 'id_profile';
        $table = 'vw_scm_aux_sku';
        $terms = "WHERE
            id_sku_pk = {$this->sku_id}
            AND id_company_fk = {$this->company_id}";

        $query_result = DB::Select($table, $columns, $terms);

        return $query_result[0]['ID_PROFILE'];
    }

    private function buildPolicyDeposits($id_profile_pk)
    {
        $query = "SELECT
            l.cod_local_pk,
            l.cod_local_pk || ' - ' || l.desc_local as available_deposits_desc
            FROM
            erp_local l
            INNER JOIN scm_profile_local_disp ld ON l.id_company_fk = ld.id_company_fk
            AND l.cod_local_pk = ld.cod_local_fk
            AND ld.id_profile_fk = {$id_profile_pk}
            WHERE
            l.id_company_fk = {$this->company_id}";

        $query_result = DB::Procedure($query, true);

        return $this->buildDepositsResponse($query_result);
    }

    private function buildUnavailableDeposits($id_profile_pk)
    {
        $query = "SELECT
            l.cod_local_pk,
            l.cod_local_pk || ' - ' || l.desc_local as unavailable_deposit_desc
            FROM
            erp_local l
            LEFT JOIN scm_profile_local_disp ld ON l.id_company_fk = ld.id_company_fk
            AND l.cod_local_pk = ld.cod_local_fk
            AND ld.id_profile_fk = {$id_profile_pk}
            WHERE
            l.id_company_fk = {$this->company_id}
            AND ld.id_profile_fk IS NULL";

        $query_result = DB::Procedure($query, true);

        return $this->buildDepositsResponse($query_result);
    }

    private function buildDepositsResponse($query_result)
    {
        $data = [];
        $data['items'] = $query_result;
        $data['qty'] = count($query_result);
        return $data;
    }

    private function buildManualDataUpdate()
    {
        $query = "SELECT
            id_company_fk,
            id_sku_fk,
            v_lt_receiving_in,
            v_lt_planner_in,
            v_lt_purchase_in,
            v_lt_supplier_in,
            v_lt_others_in,
            v_qty_min_in,
            v_calendar,
            v_qty_cons,
            v_date_cons_expiration,
            v_order_min,
            v_order_mult,
            v_type_out_forecast
            FROM
            scm_sku_in
            WHERE
            id_company_fk = {$this->company_id}
            AND id_sku_fk = {$this->sku_id}";

        $query_result = DB::Procedure($query, true);

        return $query_result[0];
    }

    private function buildConsumerProfile($skuID = null)
    {
        $query = "
            SELECT V_CONS_PROFILE
            FROM scm_sku_in
            WHERE ID_COMPANY_FK = {$this->company_id}
        ";

        if($skuID)
        {
            $query .= "AND ID_SKU_FK  = $skuID";
        }

        $query_result = DB::Procedure($query, true);

        return $query_result[0]['V_CONS_PROFILE'];
    }

    private function buildCalendarDates($dates)
    {
        $result = [];

        foreach ($dates as $date) {
            $data_object = [
                'value' => $date['DT_RECEIVING_PK'],
                'haveDay' => true,
                'month' => date('Y/m', strtotime($date['DT_RECEIVING_PK']))
            ];

            array_push($result, $data_object);
        }

        return $result;
    }

    private function buildDatesByYearAndMonth($dates)
    {
        usort($dates, function($a, $b)
        {
            return strcmp($a['value'], $b['value']);
        });

        $result = [];
        foreach ($dates as $date) {
            $result[$date['month']][] = $date;
        }

        return $this->filterDates($result);
    }

    private function filterDates($groups)
    {
        $result = [];
        $index = 0;
        $MAX_DATES = 6;

        foreach ($groups as $key => $group) {
            if ($index === $MAX_DATES) {
                break;
            }

            $result[$key] = $group;
            $index++;
        }

        return $result;
    }

    private function buildCalendarWithMonthAndYaear($dates)
    {
        $MAX_DATES = 6;
        $amount_dates = ($MAX_DATES - count($dates));
        $last_date = $this->lastDate($dates);

        for ($index = 0; $index < $amount_dates; $index++) {
            $last_date = date('Y/m/d', strtotime('+1 month' . $last_date));

            $dates[date('Y/m', strtotime($last_date))] = [
                [
                    'value' => $last_date,
                    'haveDay' => false,
                    'month' => date('Y/m', strtotime($last_date))
                ]
            ];
        }

        return $dates;
    }

    private function lastDate($dates)
    {
        $last_date = '';
        foreach ($dates as $key => $date) {
            $last_date = $key;
        }

        return date('Y/m/d', strtotime($last_date . '/01'));
    }

    private function prepareData($data)
    {
        foreach($data as $key => $value) {
            if(is_array($value)){
                foreach($value as $subkey => $subvalue) {
                    $result[$key][] = $this->formatData($subvalue, $subkey);
                }
            }else{
                $result[] = $this->formatData($value, $key);
            }
        }

        return $result;
    }

    private function formatData($value, $key)
    {
        switch ($key) {
            case 'DT_RECEIVING_PK':
                return date('d/m/Y', strtotime($value));
                break;
            default:
                return (is_numeric($value) ? (float) $value : $value);
                break;
        }
    }


    /**
     * @return bool
     */
    public function recalculateSku(): bool
    {
        try {
            if($this->updateSitRecalc()){
                $procedure = "BEGIN PRC_CALC_SCM_SKU_OUT_DAY({$this->company_id}, {$this->sku_id}); COMMIT; END;";
                return DB::Procedure($procedure);
            }
            return false;
        } catch (Exception $e) {
            Logger::error(__CLASS__ . ":infoSku:recalculate", [
                "Error when try recalculate SKU {$this->sku_id} with following response code:{$e->getCode()} message:{$e->getMessage()}",
                 $e->getFile(), $e->getTrace()
            ]);
            return false;
        }
    }

    /**
     * É necessário atualizar a coluna SIT_PENDING_CALC sempre que uma SKU for recalculada
     */
    public function updateSitRecalc(){
        try {

            $data = [
                "SIT_PENDING_CALC" => 1,
            ];
            $table = "SCM_SKU";
            $terms = "WHERE ID_SKU_PK = :id AND id_company_fk = :company_id";
            $parse_string = "id={$this->sku_id}&company_id={$this->company_id}";

            return DB::Update($table, $data, $terms, $parse_string);

        } catch (Exception $e) {
            Logger::error(__CLASS__ . ":infoSku:updateSitRecalc", [
                "Error when try update updateSitRecalc {$this->sku_id} with following response code:{$e->getCode()} message:{$e->getMessage()}",
                 $e->getFile(), $e->getTrace()
            ]);
            return false;
        }
    }


    /*
        Substitui código numerico dos campos Grupo e SubGrupo por sua descrição
    */
    public function prepareCodGroupData($select_result)
    {
        $data = [];

        foreach ($select_result as $key => $value) {
            $data[$key] = $this->formatCodGroupData($key, $select_result[$key]);
        }

        return $data;
    }

    /*
        Busca a descrição do Grupo no Redis através de sua "FK".
    */
    private function formatCodGroupData($column, $value)
    {
        if (is_null($value) || $value == 'NULL' || is_array($value)) {
            return null;
        }
        return $value;
    }
}
