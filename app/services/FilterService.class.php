<?php

/**
 * FilterService
 * Service to control all the queries of Filters;
 *
 */
class FilterService
{
    private $Filter;
    private $Table;
    private $user;

    /**
     * Set the table name.
     */
    public function __construct()
    {
        $this->Table = 'uac_filters';
        $this->user = Login::getUserSession();
    }

    /**
     * get
     * Get the filter by user.
     * @param INT $FilterID
     * @return ARRAY/BOOLEAN
     */
    public function get($FilterID)
    {
        if (!$FilterID) {
            return false;
        }

        $Terms = "WHERE filter_id = {$FilterID} AND id_company_fk = {$this->user['id_company_pk']} ORDER BY filter_id ASC";
        $Select = DB::Select($this->Table, 'filter_id, filter_nome, filter_perfil, filter_data', $Terms);

        if ($Select) {
            $Select[0]['FILTER_DATA'] = stream_get_contents($Select[0]['FILTER_DATA']);
            return $Select;
        }

        return false;
    }

    /**
    * save
    * Insert a new filter on database.
    * @param INT $FilterID
    * @return ARRAY/BOOLEAN
    */
    public function save($Data)
    {
        if (!$Data) {
            return false;
        }

        unset($Data['filter_data']['filter_temp']);
        $dataFilterJson = json_encode($Data['filter_data']);
        
        $Data = "'" . $this->user['user_id'] . "', '" . $this->user['id_company_pk'] . "', '" . $Data['filter_perfil'] . "', '" . $Data['filter_nome'] . "', '" . $dataFilterJson . "', '" . $Data['filter_tela_id'] . "'" ;

        $Procedure = "BEGIN PRC_INSERT_FILTER({$Data}); COMMIT; END;";

        return DB::Procedure($Procedure);
    }
    
    public function update($Data)
    {
        if (!$Data || array_search("", $Data)) {
            return false;
        }

        $dataFilterJson = json_encode($Data['filter_data']);
        
        $updateData = $Data['filter_id'] . ",'" . $dataFilterJson . "', '" . $Data['filter_nome'] . "', '" . $Data['filter_perfil'] . "'";
        return DB::Procedure("BEGIN PRC_UPDATE_FILTER({$updateData}); COMMIT;  END;");
    }

    /**
    * delete
    * Delete the filter by ID.
    * @param INT $FilterID
    * @return BOOLEAN
    */
    public function delete($FilterID)
    {
        if (!$FilterID) {
            return false;
        }

        return DB::Delete($this->Table, "WHERE filter_id = :id", "id={$FilterID}");
    }
}
