<?php

/**
 * SuggestAddService
 * Servers the Login controller with autenthic, validate and check user login.
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class EstabsService
{

    private $company_id;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    public function getCacheEstabs()
    {
        return Redis::HGet("company:{$this->company_id}:estabs");
    }

    public function updateEstabs()
    {
        $Select = DB::Select(
            'SCM_ESTAB', 
            "cod_estab_pk || '-' || desc_estab_short AS estab, cod_estab_pk", 
            "WHERE id_company_fk = {$this->company_id}"
        );
        
        foreach ($Select as $Result) {
            $Data[$Result['COD_ESTAB_PK']] = $Result['ESTAB'];
        }

        if (!$Data) {
            return false;
        }

        Redis::HSet("company:{$company_id}:estabs", $Data);

        return $Data;
    }
}
