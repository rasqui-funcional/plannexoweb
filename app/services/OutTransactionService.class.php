<?php

// SELECT /*+FIRST_ROWS*/ DATE_TRANS, COD_LOCAL, COD_LOCAL_DEST, COD_SECTOR, QTY_TRANS, TYPE_TRANS, YN_CONS from VW_SCR_ERP306G_TRANSACTION_OUT where id_company_fk = 3 AND cod_item_fk = '89249' AND cod_estab_fk = '1' order by date_trans desc

// OBS: Os campos cod_item_pk, cod_estab_pk e id_company_fk recebem variáveis de sessão.

class OutTransactionService
{
    private $lang;
    const TABLE = "VW_SCR_ERP306G_TRANSACTION_OUT";

    const COLUMNS = [
        'DATE_TRANS AS DATE_TRANS_ALT',
        'COD_LOCAL',
        'COD_LOCAL_DEST',
        'COD_SECTOR',
        'QTY_TRANS',
        'TYPE_TRANS',
        'YN_CONS'
    ];

    const CONDITION_STANDARD_QTY = "(QTY_TRANS > 0 AND QTY_TRANS IS NOT NULL)";

    /**
     * OutTransactionService constructor.
     * @param $lang
     */
    public function __construct()
    {
        global $lang;
        $this->lang = $lang;
    }

    /**
     * <b>buildIndicators</b>
     * Get the StockOverBalance data.
     * @param $post
     * @param $id_company_fk
     * @param $offset
     * @param INT $limit ,$Offset
     * @param string $order
     * @return array
     */

    public function getTableData($post, $id_company_fk, $offset, $limit, $order = '')
    {
        $order = str_ireplace('DATE_TRANS_ALT', 'DATE_TRANS', $order);
        $terms = "where " . self::CONDITION_STANDARD_QTY;
        $terms .= " AND cod_item_fk = '{$post['skuItem']}' AND cod_estab_fk = '{$post['codEstab']}' and id_company_fk = {$id_company_fk} {$order}";

        $queryResult = DB::Page(self::TABLE, $offset, $limit, implode(',', self::COLUMNS), $terms);

        if (!$queryResult || $queryResult == null) {
            return [
                'error' => $this->lang['global']['not_found']
            ];
        }

        $cols = array_keys($queryResult[0]);
        $titles = array();

        foreach ($cols as $col) {
            array_push($titles, $this->lang['info_sku']['erp'][$col]);
        }

        $raw = $this->select($post['skuItem'], $id_company_fk, $post['codEstab'], $order);

        return [
            'result' => $queryResult,
            'titles' => $titles,
            'raw_result' => $raw['data'],
            'rows' => $this->totalOutTransaction(self::TABLE, $terms),
            'countInfo' => $this->countInfo($raw['data'])
        ];
    }

    public function select($item, $idCompanyFk, $codEstabPk, $order = 'ORDER BY DATE_TRANS_ALT desc')
    {
        $terms = "where " . self::CONDITION_STANDARD_QTY;
        $terms .= " AND id_company_fk = {$idCompanyFk} AND cod_item_fk = '{$item}' AND cod_estab_fk = '{$codEstabPk}' {$order}";
        $result = DB::Select(self::TABLE, implode(',', self::COLUMNS), $terms);

        return [
            'count_desc_local' => $this->lang['info_sku']['erp'],
            'titles' => $this->lang['info_sku']['erp'],
            'data' => $result,
        ];
    }

    private function countInfo($result)
    {
        $totalQuantity = 0;
        foreach ($result as $array) {
            foreach ($array as $column => $value) {
                if ($column == 'QTY_TRANS') {
                    $totalQuantity = $totalQuantity + $value;
                }
            }
        }

        return [
            'QTY_TRANS' => Utilities::toFloatFormat($totalQuantity)
        ];
    }

    private function totalOutTransaction($table, $terms)
    {
        return DB::SelectRow($table, 'count(*) AS total', $terms);
    }
}
