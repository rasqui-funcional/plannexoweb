<?php

/**
 * <b>PurchaseOrderFiltersService</b>
 * Service to control all the dashboard of Info SKU;
 * @param INT $companyId
 * @param INT $skuId
 */
class PurchaseOrderFiltersService
{
    const SESS_NAME_DATA_FILTERS = USERSESSIONID . ':session:purchaseorder:list:filters';
    private $company_id;
    private $filter_temp;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
        $this->filter_temp = Redis::Get(self::SESS_NAME_DATA_FILTERS);
        $this->FieldsService = new Fields();
    }

    public function getFiltersFields()
    {
        return $this->FieldsService->getFields();
    }

    public function getSavedFilters()
    {
        return Fields::getSelectFilter('SCM800G');
    }

    public function getDinamicFieldsGrouping()
    {
        return $this->FieldsService->getNewDinamicFieldsGrouping();
    }

    public function getFilterTemp($referer)
    {
        return $this->filter_temp && $referer ? $this->filter_temp : 'false';
    }
}