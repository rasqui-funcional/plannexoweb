<?php

/**
 * <b>OrderCoverFilterService</b>
 * Service to control all the dashboard of Info SKU;
 * @param INT $companyId
 * @param INT $skuId
 */
class PurchaseOrderGroupedService
{
    private $user;
    private $company_id;
    private $post;
    private $query_cols;
    private $auxiliar_cols;
    private $header_cols;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
        $this->user = Login::getUserSession();
    }

    public function checkOrderCoverCache()
    {
        if (!Redis::HGet(USERSESSIONID . ':session:purchaseorder:grouped:header-cols')) {
            return false;
        }

        if (!Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:query-cols')) {
            return false;
        }

        if (!Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:group-by')) {
            return false;
        }

        if (!Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:query-basic')) {
            return false;
        }

        return true;
    }

    public function prepareHeaderCols($post)
    {
        $header_cols = (isset($post['cod_group_fk']) ? $post['cod_group_fk'] : array(0 => ''));
        Redis::HSet(USERSESSIONID . ':session:purchaseorder:grouped:header-cols', $header_cols);
        return $header_cols;
    }

    public function getHeaderColsFromCache()
    {
        return Redis::HGet(USERSESSIONID . ':session:purchaseorder:grouped:header-cols');
    }

    public function preparePagination($post)
    {
        $this->post = $post;
        $this->buildCols();
        $this->query_cols = $this->prepareQueryCols();
        $this->prepareQuery();
        $this->prepareGroupBy();
    }

    public function getGroupedOrders($post)
    {
        $offset = Utilities::setOffset($post['page'], $post['limit']);
        $columns = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:query-cols');
        $table = 'VW_SCR_SCM800G_FOLLOW_UP a 
                  INNER JOIN scm_estab b ON a.id_company_fk = b.id_company_fk AND b.cod_estab_pk = a.cod_estab_fk';
        $group_by = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:group-by');

        $order = (isset($post['order']) && is_array($post['order']) ? $this->prepareOrder($post['order']) : '');

        $estabIn = '';
        if(!empty($this->user['estab'])) {
            $estabIn = " AND a.COD_ESTAB_FK IN({$this->user['estab']}) ";
        }

        $terms = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:query-basic') . $estabIn . $group_by . $order;

        $orders_data = DB::Page(
            $table,
            $offset,
            $post['limit'],
            $columns,
            $terms
        );

        $this->footer_count = $this->setQtyTotalOrders($columns, $terms);

        if ($orders_data) {
            return $this->prepareData($orders_data);
        }

        return false;
    }

    public function getQtyTotalOrders()
    {
        return $this->footer_count;
    }

    public function getHeaderCols()
    {
        $this->header_cols = Redis::HGet(USERSESSIONID . ':session:purchaseorder:grouped:header-cols');
        return $this->unsetDefaultHeaderCols($this->header_cols);
    }

    private function prepareProcessWhere($suggest)
    {
        $where = "AND cod_estab_fk = '{$suggest['cod_estab_fk']}'" . DB::setFields($suggest['dynamicFields']);

        return $where;
    }

    private function unsetDefaultHeaderCols($header_cols)
    {
        $cols_to_unset = [
            'date_arrival',
            'sit_urgent',
            'cod_local_fk',
        ];

        foreach ($cols_to_unset as $col) {
            $col_key = array_search($col, $header_cols);
            if ($col_key !== false) {
                unset($header_cols[$col_key]);
            }
        }

        return $header_cols;
    }

    private function prepareDate($DateStr, $tableField)
    {
        $DateArray = explode('-', $DateStr);
        $DateArray = str_replace('/', '-', $DateArray);
        $DateFrom = date('d/m/Y 00:00:00', strtotime($DateArray[0]));
        $DateTo = date('d/m/Y 23:59:59', strtotime($DateArray[1]));
        $DateQuery = " AND ( a.{$tableField} between  to_date('{$DateFrom}', 'DD/MM/YYYY HH24:mi:ss') and to_date('{$DateTo}','DD/MM/YYYY HH24:mi:ss')) ";

        return $DateQuery;
    }

    public function csvData()
    {
        $group_by = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:group-by');
        $columns = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:query-cols');
        $table = 'VW_SCR_SCM800G_FOLLOW_UP a
        INNER JOIN scm_estab b ON a.id_company_fk = b.id_company_fk
        AND b.cod_estab_pk = a.cod_estab_fk';

        $estabIn = '';
        if(!empty($this->user['estab'])) {
            $estabIn = " AND a.COD_ESTAB_FK IN({$this->user['estab']}) ";
        }

        $terms = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:query-basic'). $estabIn . $group_by;

        $result['data'] = DB::Select($table, $columns, $terms);

        if (empty($result['data'])) {
            return [
                'data' => [],
                'columns' => [],
            ];
        }
        $result['columns'] = array_keys($result['data'][0]);

        return $result;
    }

    /**
     * Count the total of orders, used in pagination.
     */
    private function setQtyTotalOrders($columns, $terms)
    {
        $orders = DB::Select(
            'VW_SCR_SCM800G_FOLLOW_UP a
            INNER JOIN scm_estab b ON a.id_company_fk = b.id_company_fk
            AND b.cod_estab_pk = a.cod_estab_fk',
            $columns,
            $terms
        );

        return count($orders);
    }

    private function prepareData($select_result)
    {
        foreach ($select_result as $key => $result) {
            foreach ($result as $column => $value) {
                $data[$key][$column] = [
                    'value' => $value,
                    'name' => $this->formatData($column, $value),
                ];
            }
        }

        return $data;
    }

    private function formatData($column, $value)
    {
        if (is_null($value) || $value == 'NULL' || is_array($value)) {
            return null;
        }
        switch (strtoupper($column)) {
            case 'COD_GROUP2_FK':
            case 'COD_GROUP3_FK':
            case 'COD_GROUP4_FK':
            case 'COD_GROUP5_FK':
            case 'COD_GROUP6_FK':
            case 'COD_GROUP7_FK':
            case 'COD_GROUP8_FK':
            case 'COD_GROUP9_FK':
            case 'COD_GROUP10_FK':
            case 'COD_GROUP11_FK':
            case 'COD_GROUP12_FK':
                preg_match_all('!\d+!', $column, $matches);
                return Redis::HGet("company:{$this->company_id}:group-{$matches[0][0]}", (string)$value);
                break;
            default:
                return Utilities::dataFormatter($column, $value);
                break;
        }
    }

    private function buildDynamicData($result)
    {
        $header_cols = $this->getHeaderCols();
        $data = array();
        foreach ($header_cols as $col) {
            $data[$col] = Utilities::dataFormatter($col, $result[strtoupper($col)]);
        }

        return $data;
    }

    private function prepareQuery()
    {
        $post_filters = $this->unsetDefaults($this->post);
        $late_days = $this->prepareLateDays($this->post);
        $orders = $this->prepareOrders($this->post['ord_atraso']);
        $filters = $this->buildFilters($post_filters);
        $Date = (($this->post['date_exp']) != '' ? $this->prepareDate($this->post['date_exp'], 'date_exp') : null);
        $Date .= (($this->post['date_order']) != '' ? $this->prepareDate($this->post['date_order'], 'date_order') : null);
        $Date .= (($this->post['date_req']) != '' ? $this->prepareDate($this->post['date_req'], 'date_req') : null);
        Redis::Set(USERSESSIONID . ':session:purchaseorder:grouped:date', $Date);

        $estabIn = '';
        if(!empty($this->user['estab'])) {
            $estabIn = " AND a.COD_ESTAB_FK IN({$this->user['estab']}) ";
        }

        $basic_terms = "WHERE a.id_company_fk = {$this->company_id} {$estabIn} {$filters} {$orders} {$late_days} {$Date}";
        Redis::Set(USERSESSIONID . ':session:purchaseorder:grouped:query-basic', $basic_terms);
    }

    private function prepareLateDays(array $data) : string
    {
        $value = (int) $data['qtde_dias_atraso'];
        $minusOneDay = $value - 1;
        $plusOneDay = $value + 1;
        $query = 'AND dias_atraso';
        switch ($data['dias_atraso']) {
            case '>':
            case '<':
            case '=':
                $query .= " {$data['dias_atraso']} {$value}";
                break;
            case '>=':
                $query .= " > {$minusOneDay}";
                break;
            case '<=':
                $query .= " < {$plusOneDay}";
                break;
            
            default:
                return '';
                break;
        }

        return $query;
    }

    private function buildFilters($post_filters)
    {
        $post_filters['cod_item'] = ($post_filters['cod_item'] === '') ? '' : "%{$post_filters['cod_item']}%";
        $post_filters['desc_supplier'] = ($post_filters['desc_supplier'] === '') ? '' : "%{$post_filters['desc_supplier']}%";
        return DB::setFields($post_filters);
    }

    private function prepareGroupBy()
    {
        $columns = implode(",", $this->auxiliar_cols);
        $group_by = "GROUP BY a.id_company_fk, {$columns} ";
        Redis::Set(USERSESSIONID . ':session:purchaseorder:grouped:group-by', $group_by);

        return $group_by;
    }

    private function prepareOrder($orders)
    {
        end($orders);
        $last_column = key($orders);
        $order_query = "ORDER BY ";
        foreach ($orders as $column => $col_order) {
            $order_query .= "{$column} {$col_order}";
            if ($last_column != $column) {
                $order_query .= ", ";
            }
        }

        return $order_query;
    }

    private function prepareQueryCols()
    {
        $query_cols = DB::getTableCols('VW_SCR_SCM800G_FOLLOW_UP');

        if (!is_null($this->post['cod_group_fk'])) {
            $cols_set = ['count(id_order_pk) as total_orders',];
            $query_cols = DB::setColumns(array_merge($cols_set, $this->auxiliar_cols));
        }

        if (Redis::Set(USERSESSIONID . ':session:purchaseorder:grouped:query-cols', $query_cols)) {
            return true;
        }

        return false;
    }

    private function unsetDefaults($post)
    {
        unset(
            $post['cod_group_fk'],
            $post['filter_temp'],
            $post['date_order_in_cond'],
            $post['date_exp_in_cond'],
            $post['date_req_order_cond'],
            $post['ord_atraso'],
            $post['date_req'],
            $post['date_order'],
            $post['date_exp'],
            $post['qtde_dias_atraso'],
            $post['dias_atraso']
        );
        return $post;
    }

    private function buildCols()
    {
        $cols = (key_exists('cod_group_fk', $this->post) && $this->post['cod_group_fk'])
            ? $this->post['cod_group_fk'] : $this->getAuxArr();

        foreach ($cols as $col) {
            $this->auxiliar_cols[] = $col;
        }
    }

    private function getAuxArr()
    {
        return [
            'a.auxiliar1',
            'a.auxiliar2',
            'a.auxiliar3',
            'a.auxiliar4',
            'a.auxiliar5',
            'a.auxiliar6',
            'a.auxiliar7',
            'a.auxiliar8',
            'a.auxiliar9',
            'a.auxiliar10',
            'a.auxiliar11',
            'a.auxiliar12',
        ];
    }

    private function prepareOrders($in_orders)
    {
        $where = '';
        $order = strtolower($in_orders[0]);
        $values = ['com' => 1, 'sem' => 0];
        if ($order != 'todas') {
            $where .= " AND ind_ord_atraso = {$values[$order]}";
        }

        return $where;
    }
}
