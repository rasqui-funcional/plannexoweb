<?php

/**
 * <b>OrderCoverFooterService</b>
 * Service to get all footer data on Order Cover;
 */
class PurchaseOrderFooterService
{
    public function getOrdersFooterData()
    {
        $terms = Redis::Get(USERSESSIONID . ':session:purchaseorder:list:where');

        $columns = "SUM(QTY_PARC) AS qty_parc,
                    SUM(QTY_REC) AS qty_rec,
                    SUM(QTY_PENDING) AS qty_pending,
                    SUM(VAL_UNIT) AS val_unit,
                    SUM(VAL_TOTAL) AS val_total";

        $result_data = DB::Select('VW_SCR_SCM800G_FOLLOW_UP', $columns, $terms);

        if ($result_data) {
            return $this->prepareData($result_data);
        } else {
            return $this->mockNullData();
        }
    }

    private function prepareData($result_data)
    {
        $data = array();

        foreach ($result_data[0] as $column => $value) {
            $data[$column] = $this->formatData($column, $value);
        }

        return $data;
    }

    private function formatData($column, $value)
    {
        switch (strtolower($column)) {
            case 'qty_parc':
            case 'qty_pending':
            case 'qty_rec':
                return (int) $value;
                break;
            case 'val_unit':
            case 'val_total':
                return Utilities::Currency($value);
                break;
            default:
                return $value;
                break;
        }
    }

    private function mockNullData()
    {
        return [
            'QTY_PARC' => 0,
            'QTY_REC' => 0,
            'QTY_PENDING' => 0,
            'VAL_UNIT' => '0,00'
        ];
    }

}
