<?php

/**
 * <b>OrderCoverSuggestionsService</b>
 * Service to control suggestions of ordercover;
 * @param INT $companyId
 * @param INT $skuId
 */
class PurchaseOrderListService
{
    private $user;
    private $company_id;
    private $groupings;
    private $columns = [
        'ID_SKU_PK',
        'LT_TOTAL_OUT',
        'OUT6',
        'SIT_SKU',
        'INV_LEVEL',
        'COD_ESTAB_FK',
        'COD_ITEM',
        'DESC_ITEM',
        'USER_ID',
        'ID_SUPPLIER',
        'DESC_SUPPLIER',
        'COD_GROUP1_FK',
        'COD_GROUP2_FK',
        'COD_GROUP3_FK',
        'COD_GROUP4_FK',
        'COD_GROUP5_FK',
        'COD_GROUP6_FK',
        'COD_GROUP7_FK',
        'COD_GROUP8_FK',
        'COD_GROUP9_FK',
        'COD_GROUP10_FK',
        'COD_GROUP11_FK',
        'COD_GROUP12_FK',
        'NUM_REQ',
        'DATE_REQ',
        'NUM_ORDER',
        'DATE_ORDER',
        'DATE_EXP',
        'QTY_PARC',
        'QTY_REC',
        'QTY_PENDING',
        'VAL_UNIT',
        'VAL_TOTAL',
        'DIAS_ATRASO',
        'INV_AVAILABLE',
        'INV_TOTAL',
        'QTY_COVERAGE'
    ];

    public function __construct($company_id, $groupings)
    {
        $this->company_id = $company_id;
        $this->groupings = $groupings;
        $this->user = Login::getUserSession();
    }

    public function getHeaderCols()
    {
        return array_map('trim', explode(",", $this->getColumns()));
    }

    public function getOrdersData($post)
    {
        $offset = Utilities::setOffset($post['page'], $post['limit']);
        $columns = $this->getColumns();
        $order = (isset($post['order']) && is_array($post['order']) ? $this->prepareOrder($post['order']) : null);
        $date = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:date');
        $date = str_replace('a.', '', $date);

        $estabIn = '';
        if(!empty($this->user['estab'])) {
            $estabIn = " AND COD_ESTAB_FK IN({$this->user['estab']}) ";
        }
        
        $terms = Redis::Get(USERSESSIONID . ":session:purchaseorder:list:where") . $estabIn . $date . $order;
        $select_result = DB::Page(
            'VW_SCR_SCM800G_FOLLOW_UP',
            $offset,
            $post['limit'],
            $columns,
            $terms
        );

        if ($select_result) {
            return $this->prepareData($select_result);
        }

        return false;
    }

    /**
     * Count the total of suggestions, used in pagination.
     */
    public function getQtyTotalOrders($terms)
    {
        $qty_total_orders = 0;

        $date = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:date');
        $date = str_replace('a.', '', $date);
        
        $estabIn = '';
        if(!empty($this->user['estab'])) {
            $estabIn = " AND COD_ESTAB_FK IN({$this->user['estab']}) ";
        }

        $terms = $terms . $estabIn . $date;

        $result = DB::Select(
            'VW_SCR_SCM800G_FOLLOW_UP',
            $this->getColumns(),
            $terms
        );

        if ($result) {
            $qty_total_orders = count($result);
        }

        return $qty_total_orders;
    }

    private function prepareData($select_result)
    {
        $data = array();
        foreach ($select_result as $row => $rowdata) {
            foreach ($rowdata as $col => $value) {
                $data[$row][$col] = $this->formatData($col, $value);
            }
        }

        return $data;
    }

    private function formatData($column, $value)
    {
        if(is_null($value) || $value == 'NULL' || is_array($value)) {
            return null;
        }

        switch ($column) {
            case 'COD_GROUP2_FK':
            case 'COD_GROUP3_FK':
            case 'COD_GROUP4_FK':
            case 'COD_GROUP6_FK':
            case 'COD_GROUP7_FK':
            case 'COD_GROUP8_FK':
            case 'COD_GROUP9_FK':
            case 'COD_GROUP10_FK':
            case 'COD_GROUP11_FK':
            case 'COD_GROUP12_FK':
                preg_match_all('!\d+!', $column, $matches);
                return Redis::HGet("company:{$this->company_id}:group-{$matches[0][0]}", (string)$value);
                break;
            case 'COD_GROUP5_FK':
                return $value;
            case 'INV_LEVEL':
                return [
                    'name' => Utilities::dataFormatter($column, $value)
                    , 'value' => $value
                ];
            default:
                return Utilities::dataFormatter($column, $value);
        }
    }

    private function prepareOrder($orders)
    {
        end($orders);
        $last_column = key($orders);
        $order_query = "ORDER BY ";
        foreach ($orders as $column => $col_order) {
            $order_query .= "{$column} {$col_order}";
            if ($last_column != $column) {
                $order_query .= ", ";
            }
        }

        return $order_query;
    }

    private function getColumns()
    {
        $format = 'COD_GROUP%d_FK';
        foreach ($this->groupings as $key => $value) {
            $str = str_replace('%d', $key, $format);
            if ( $uid = array_search($str, $this->columns) ) {
                unset($this->columns[$uid]);
            }
        }

        return implode(',', $this->columns);
    }

    public function getPurchaseOrderCSV()
    {
        $date = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:date');
        $date = str_replace('a.', '', $date);

        $estabIn = '';
        if(!empty($this->user['estab'])) {
            $estabIn = " AND COD_ESTAB_FK IN({$this->user['estab']}) ";
        }

        $terms = Redis::Get(USERSESSIONID . ":session:purchaseorder:list:where") . $estabIn . $date;
        $result['data'] = DB::Select(
            'VW_SCR_SCM800G_FOLLOW_UP',
            $this->getColumns(),
            $terms
        );

        if (empty($result['data'])) {
            return [
                'data' => [],
                'columns' => [],
            ];
        }

        $result['columns'] = array_keys($result['data'][0]);

        $data = [];
        foreach ($result['data'] as $csvRecord) {
            foreach ($csvRecord as $key => $value) {
                $csvRecord[$key] = $this->formatCsvData($key, $value);
            }
            $data[] = $csvRecord;
        }

        $result['data'] = $data;

        return $result;
    }

    private function formatCsvData($column, $value)
    {
        if(is_null($value) || $value == 'NULL' || is_array($value)) {
            return null;
        }

        switch ($column) {
            case 'COD_GROUP2_FK':
            case 'COD_GROUP3_FK':
            case 'COD_GROUP4_FK':
            case 'COD_GROUP6_FK':
            case 'COD_GROUP7_FK':
            case 'COD_GROUP8_FK':
            case 'COD_GROUP9_FK':
            case 'COD_GROUP10_FK':
            case 'COD_GROUP11_FK':
            case 'COD_GROUP12_FK':
                preg_match_all('!\d+!', $column, $matches);
                return Redis::HGet("company:{$this->company_id}:group-{$matches[0][0]}", (string)$value);
            default:
                return $value;
        }
    }
}