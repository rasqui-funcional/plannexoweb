<?php

/**
 * <b>OrderCoverFooterService</b>
 * Service to get all footer data on Order Cover;
 */
class OrderCoverFooterService
{
    public function getGroupedFooterData()
    {
        $basic_terms = Redis::Get(USERSESSIONID . ':session:ordercover:list:query-basic');
        $terms = str_replace('a.', '', $basic_terms);
        $estabIn = '';
        $user = Login::getUserSession();
        if(!empty($user['estab'])) {
            $estabIn = " AND COD_ESTAB_FK IN({$user['estab']}) ";
        }

        $query = "SELECT
                        SUM(total_sugs) total_sugs,
                        SUM(total_qty) total_qty,
                        SUM(total_price) total_price
                    FROM(
                        SELECT
                            COUNT (ID_SUG_PK) AS total_sugs,
                            SUM (QTY_ALTERED) AS total_qty,
                            SUM (VAL_UNIT_ERP * QTY_ALTERED) AS total_price
                        FROM   VW_SCR_SCM532G_SUG_PUR_AGRUP
                        {$terms} {$estabIn}
                        )";

        $result_data = DB::Procedure($query, true);

        if ($result_data) {
            return $this->prepareData($result_data);
        } else {
            return $this->mockNullData();
        }
    }

    public function getSuggestionsFooterData()
    {
        $terms = Redis::Get(USERSESSIONID . ':session:ordercover:suggestions:where');

        $query = "SELECT
                        SUM(total_value) total_value
                    FROM(
                        SELECT
                            SUM (total_value) AS total_value
                        FROM  vw_scr_scm533g_sug_pur_detail
                        {$terms}
                        )";

        $result_data = DB::Procedure($query, true);

        if ($result_data) {
            return $this->prepareData($result_data);
        } else {
            return $this->mockNullData();
        }
    }

    private function prepareData($result_data)
    {
        $data = array();

        foreach ($result_data[0] as $column => $value) {
            $data[$column] = $this->formatData($column, $value);
        }

        return $data;
    }

    private function formatData($column, $value)
    {
        switch ($column) {
            case 'TOTAL_SUGS':
            case 'TOTAL_QTY':
                return (int) $value;
                break;
            case 'TOTAL_PRICE':
            case 'TOTAL_VALUE':
                return Utilities::Currency($value);
                break;
            default:
                return $value;
                break;
        }
    }

    private function mockNullData()
    {
        return [
            'TOTAL_SUGS' => 0,
            'TOTAL_QTY' => 0,
            'TOTAL_VALUE' => '0,00',
            'TOTAL_PRICE' => '0,00',
        ];
    }

}
