<?php

/**
 * <b>OrderCoverFilterService</b>
 * Service to control all the dashboard of Info SKU;
 * @param INT $companyId
 * @param INT $skuId
 */
class OrderCoverFilterService
{
    private $company_id;
    
    public function __construct($company_id)
    {
        $this->company_id = $company_id;
        $this->FieldsService = new Fields();
    }

    public function getFiltersFields()
    {
        return $this->FieldsService->getFields();
    }

    public function getSavedFilters()
    {
        return Fields::getSelectFilter('SCM532G');
    }
}
