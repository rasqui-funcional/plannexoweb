<?php

/**
 * <b>OrderCoverSuggestionsService</b>
 * Service to control suggestions of ordercover;
 * @param INT $companyId
 * @param INT $skuId
 */
class OrderCoverSuggestionsService
{
    private $company_id;
    private $post;
    private $query_cols;
    private $auxiliar_cols;
    private $footer_count;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    public function getQtyTotalSuggestions()
    {
        return $this->footer_count;
    }

    public function getSuggestionsData($post)
    {
        $offset = $this->prepareSetOffset($post['page'], $post['limit']);
        $columns = $this->getColumns();
        $post_order = $post['order'] ?? "";
        $order = $this->prepareOrder($post_order);
        Redis::Set(USERSESSIONID . ":session:ordercover:suggestions:order", json_encode($order));
        $terms = Redis::Get(USERSESSIONID . ":session:ordercover:suggestions:where") . $order;
        $select_result = DB::Page(
            'vw_scr_scm533g_sug_pur_detail',
            $offset,
            $post['limit'],
            $columns,
            $terms
        );

        $this->footer_count = $this->setQtyTotalSuggestions($columns, $terms);

        if ($select_result) {
            return $this->prepareData($select_result);
        }

        return false;
    }

    public function updateSuggestions($data, $suggestion_id, $delivery_date, $obs)
    {
        $where = ", OBS = '" . $obs . "'";
        $where .= ", DATE_ARRIVAL_ALTERED = TO_DATE('{$delivery_date}', 'DD/MM/YYYY')";
        $where .= " WHERE id_sug_pk = :id";

        return DB::Update('scm_sku_purchase_sug', $data, $where, "id={$suggestion_id}");
    }

    public function removeSuggestions($data, $id_psug, $id_sug)
    {
        $update = DB::Update('SCM_SKU_PURCHASE_SUG', $data, "WHERE id_psug_pk = :id_psug AND id_sug_pk = :id_sug AND ID_COMPANY_FK = :company_id", "id_psug={$id_psug}&id_sug={$id_sug}&company_id={$this->company_id}");

        if (!$update) {
            return false;
        }

        return $update;
    }

    public function deleteSuggestions($data)
    {
        $where = "WHERE PSUG_GROUPED_PK = :group_id AND NUM_PARC_PK = :num_parc AND ID_PSUG_PK = :psug_id AND ID_COMPANY_FK = :company_id";
        $parse = "group_id={$data['PSUG_GROUPED_PK']}&num_parc={$data['NUM_PARC']}&psug_id={$data['ID_PSUG_PK']}&company_id={$this->company_id}";

        return DB::Delete('SCM_SKU_PSUG_GROUPED_DET', $where, $parse);
    }

    private function prepareData($select_result)
    {
        $data = array();
        foreach ($select_result as $row => $rowdata) {
            foreach ($rowdata as $col => $value) {
                $data[$row][$col] = $this->formatData($col, $value);
            }
        }

        return $data;
    }

    private function prepareOrder($orders)
    {
        if(is_null($orders) || empty($orders)){
            $orders = [];
            $orders["cod_item_pk"] = "DESC"; 
        }
        $orders["num_parc_pk"] = "DESC";
        end($orders);
        $last_column = key($orders);
        $order_query = "ORDER BY ";
        foreach ($orders as $column => $col_order) {
            $order_query .= "{$column} {$col_order}";
            if ($last_column != $column) {
                $order_query .= ", ";
            }
        }
        return $order_query;
    }

    private function formatData($col, $value)
    {
        switch ($col) {
            case 'TOTAL_VALUE':
            case 'TOTAL_VALUE_ORIGINAL':
                return Utilities::Currency($value);
                break;
            default:
                return $value;
                break;
        }
    }

    private function getColumns()
    {
        return "num_parc_pk AS num_parc,
                cod_item_pk,
                desc_item,
                v_order_mult,
                v_order_min,
                qty_original,
                total_value_original,
                total_value,
                id_sug_pk,
                id_profile,
                cod_group1_fk,
                cod_group2_fk,
                cod_group3_fk,
                qty_altered,
                dbms_lob.substr(obs, 4000, 1 ) as obs,
                sit_urgent,
                TO_CHAR(date_arrival_altered,'YYYY-MM-DD') AS date_arrival_dmy,
                TO_CHAR(date_arrival_altered,'DD/MM/YYYY') AS date_arrival_altered,
                id_psug_pk,
                psug_grouped_pk,
                id_sku_fk";
    }

    private function prepareSetOffset($Page, $Limit)
    {
        return ($Page - 1) * $Limit;
    }

    /**
     * Count the total of suggestions, used in pagination.
     */
    private function setQtyTotalSuggestions($columns, $terms)
    {
        $suggestions = DB::Select(
            'vw_scr_scm533g_sug_pur_detail',
            $columns,
            $terms
        );

        return count($suggestions);
    }

    public function getSuggestions($suggestions)
    {
        $where = Redis::Get(USERSESSIONID . ":session:ordercover:suggestions:where");

        $select = DB::Select(
            'vw_scr_scm533g_sug_pur_detail',
            "{$columns}",
            $where
        );

        return $select;
    }

    public function getSuggestionsToCsv($terms, $userId, $companyId, $search)
    {
        $columns = "num_parc_pk AS num_parc,
        cod_item_pk,
        desc_item,
        v_order_mult,
        v_order_min,
        qty_original,
        REPLACE(ROUND(total_value_original, 2), '.', ',') total_value_original,
        REPLACE(ROUND(total_value, 2), '.', ',') total_value,
        qty_altered,
        dbms_lob.substr(obs, 4000, 1 ) AS obs,
        TO_CHAR(date_arrival_altered,'DD/MM/YYYY') AS date_arrival_dmy";
        $table = "vw_scr_scm533g_sug_pur_detail";

        $result['data'] = DB::Select($table, $columns, $terms);

        if (empty($result['data'])) {
            return [
                'data' => [],
                'columns' => []
            ];
        }

        $result['columns'] = array_keys($result['data'][0]);

        return $result;
    }

    public function buildOrderCoverSuggest($data)
    {
        $arrayCopy = (new ArrayObject($data))->getArrayCopy();
        $arrayCopy['ID_SUG_PK'] = $data['ID'];
        unset($arrayCopy['ID']);

        return $arrayCopy;
    }
}
