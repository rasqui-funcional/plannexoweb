<?php

/**
 * <b>OrderCoverFilterService</b>
 * Service to control all the dashboard of Info SKU;
 * @param INT $companyId
 * @param INT $skuId
 */
class OrderCoverListService
{
    private $company_id;
    private $user;
    private $estab;
    private $post;
    private $query_cols;
    private $query_basic_terms;
    private $auxiliar_cols;
    private $header_cols;
    private $has_date;
    private $lang;
    private $cod_groups = [];

    public function __construct($user_session)
    {
        global $lang;
        $this->company_id = $user_session['id_company_pk'];
        $this->user = $user_session['user_id'];
        $this->estab = $user_session['estab'];
        $this->has_date = false;
        $this->lang = $lang;
    }

    public function hasDate()
    {
        return $this->has_date;
    }

    public function checkOrderCoverCache()
    {
        if (!Redis::HGet(USERSESSIONID . ':session:ordercover:list:header-cols')) {
            return false;
        }

        if (!Redis::Get(USERSESSIONID . ':session:ordercover:list:query-cols')) {
            return false;
        }

        if (!Redis::Get(USERSESSIONID . ':session:ordercover:list:group-by')) {
            return false;
        }

        if (!Redis::Get(USERSESSIONID . ':session:ordercover:list:query-basic')) {
            return false;
        }

        return true;
    }

    public function validateSuggestions($suggest, $where)
    {
        $collumns = "
                    id_company_fk,
                    id_sug_pk,
                    num_parc_pk,
                    id_sku_fk,
                    date_arrival_altered,
                    qty_altered,
                    date_arrival_original,
                    qty_original
                ";

        $sug_items = DB::Select('VW_SCR_SCM533G_SUG_PUR_DETAIL', $collumns, "WHERE id_company_fk = {$this->company_id} {$where}");

        $sku_arr = [];
        $suggestError = [];
        foreach($sug_items as $item)
        {
            $sku_arr[$item['ID_SKU_FK']][] = $item['DATE_ARRIVAL_ALTERED'];
            if(floatval($item['QTY_ALTERED']) < 1){
                $suggestError['qty_zero'] = true;
            }
        }

        foreach($sku_arr as $sku){
            if(count(array_unique($sku)) < count($sku))
            {
                $suggestError['duplicated_date'] = true;
            }
        }

        if(count($sug_items)==0){
            $suggestError['items_empty'] = true;
        }

        return $suggestError;
    }

    public function processSuggestions($suggestions)
    {
        $query = "BEGIN ";
        $return_data = [];
        $allowProccess = false;
        $return_data['validate'] = [];
        foreach ($suggestions as $suggest) {

            $sug_header_id = $this->nextSugHeaderID();

            if (!$sug_header_id) {
                return false;
            }

            $where = $this->prepareProcessWhere($suggest);
            $validate = $this->validateSuggestions($suggest, $where);
            if(!$validate['duplicated_date'] && !$validate['qty_zero'] && !$validate['items_empty'])
            {
                $allowProccess = true;
                $query .= "INSERT INTO scm_sku_purchase_sug_header
                    (
                        id_company_fk,
                        COD_HEADER_PK,
                        COD_ESTAB_FK,
                        cod_local_fk,
                        sit_header,
                        cod_user_fk,
                        dt_exp_req,
                        cod_buyer,
                        cod_sector,
                        cod_req_reason,
                        yn_urgent,
                        obs_req,
                        date_approved
                    )
                    VALUES (
                        '{$this->company_id}',
                        '{$sug_header_id}',
                        '{$suggest['cod_estab_fk']}',
                        '{$suggest['cod_local_fk']}',
                        1,
                        " . USERID . ",
                        to_date('{$suggest['dt_exp_req']}','DD/MM/YYYY'),
                        '{$suggest['cod_buyer']}',
                        '{$suggest['cod_sector']}',
                        '{$suggest['cod_reason']}',
                        '{$suggest['yn_urgent']}',
                        '{$suggest['obs_req']}',
                        sysdate
                        );";

                $query .= "INSERT INTO scm_sku_purchase_sug_hist
                    (
                        id_company_fk,
                        id_sug_pk,
                        ID_INTEGRATION_GTP,
                        obs,
                        NUM_PARC_PK,
                        date_arrival_altered,
                        qty_altered,
                        date_arrival_original,
                        qty_original,
                        sit_altered_date,
                        sit_altered_qty,
                        id_user,
                        sit_psug,
                        id_sku_fk,
                        sit_auto_sug,
                        cod_order_unit,
                        date_created,
                        ID_PSUG_PK,
                        ID_USER_CREATED,
                        DATE_INTEGRATION,
                        date_approved
                    )
                        SELECT
                            id_company_fk,
                            id_sug_pk,
                            '{$sug_header_id}',
                            obs,
                            num_parc_pk,
                            date_arrival_altered,
                            qty_altered,
                            date_arrival_original,
                            qty_original,
                            1,
                            1,
                            " . USERID . ",
                            1,
                            id_sku_fk,
                            sit_auto_sug,
                            cod_order_unit,
                            current_date,
                            ID_PSUG_PK,
                            USER_ID,
                            SYSDATE,
                            SYSDATE
                        FROM
                            VW_SCR_SCM533G_SUG_PUR_DETAIL
                        WHERE id_company_fk = {$this->company_id} {$where}
                        AND qty_altered > 0;";

                $query .= "DELETE FROM scm_sku_purchase_sug
                    WHERE (id_company_fk,id_sug_pk)
                    IN (SELECT id_company_fk,
                            id_sug_pk
                            FROM VW_SCR_SCM533G_SUG_PUR_DETAIL
                            WHERE id_company_fk = {$this->company_id} {$where} );";

                $query .= "DELETE FROM SCM_SKU_PSUG_GROUPED_DET WHERE PSUG_GROUPED_PK = {$suggest['psug']};";
                $query .= "DELETE FROM SCM_SKU_PSUG_GROUPED WHERE PSUG_GROUPED_PK = {$suggest['psug']};";
                
            }else{
                $return_data['validate'][] = $validate;
            }
        }
        $query .= "END;";
        //$query = str_replace("\n", "", $query);
        if($allowProccess){
            $view_data['data'] = DB::Procedure($query);
            $view_data['inserted'] = DB::Select('scm_sku_purchase_sug_header', '*', "where COD_HEADER_PK = $sug_header_id ");
            $view_data['json'] = true;
        }
        $return_data['json'] = true;
        return $return_data;
    }

    public function agroupSuggestions($definitions, $suggestions)
    {
        $psug_grouped_id = $this->nextPsugGroupedID();

        if (!$psug_grouped_id) {
            return false;
        }

        $query = "BEGIN ";

        $query .= "INSERT INTO scm_sku_psug_grouped (
                    id_company_fk,
                    PSUG_GROUPED_PK,
                    COD_ESTAB_FK,
                    cod_local_fk,
                    dt_exp_req,
                    cod_buyer,
                    cod_sector,
                    cod_req_reason,
                    yn_urgent,
                    obs_req
                )
                VALUES (
                    '{$this->company_id}',
                    '{$psug_grouped_id}',
                    '{$definitions['cod_estab_fk']}',
                    '{$definitions['default_local']}',
                    to_date('{$definitions['dt_exp_req']}','dd/mm/yyyy'),
                    '{$definitions['cod_buyer']}',
                    '{$definitions['cod_sector']}',
                    '{$definitions['cod_req_reason']}',
                    '{$definitions['sit_urgent']}',
                    ''
                );";

        foreach ($suggestions as $suggest) {
            $suggest_psug_id = $suggest['psug'];
            $extras_filters = $this->getExtraFilters($suggest);
            $where = " id_company_fk = {$this->company_id} AND cod_estab_fk = '{$definitions['cod_estab_fk']}' {$extras_filters}";

            if ($suggest_psug_id !== '-1') {
                $where .= " AND psug_grouped_pk = $suggest_psug_id ";
            } else {
                $where .= $this->prepareDatetoQuery($suggest['DATE_ARRIVAL_ALTERED']);
                unset($suggest['psug'], $suggest['DATE_ARRIVAL_ALTERED']);
                $where .= DB::setFields($suggest);
                $where .= " AND psug_grouped_pk = -1 ";
            }

            $query .= "INSERT INTO scm_sku_psug_grouped_det (
                        id_company_fk,
                        psug_grouped_pk,
                        id_psug_pk,
                        num_parc_pk
                    )
                    SELECT
                        id_company_fk,
                        {$psug_grouped_id},
                        id_psug_pk,
                        num_parc_pk
                    FROM
                        vw_scr_scm533g_sug_pur_detail
                    WHERE
                        {$where}
                        ;";

            if ($suggest_psug_id != -1) {
                $query .= " DELETE FROM scm_sku_psug_grouped_det WHERE psug_grouped_pk = {$suggest_psug_id};";
                $query .= " DELETE FROM SCM_SKU_PSUG_GROUPED WHERE psug_grouped_pk = {$suggest_psug_id};";
            }
        }

        $query .= "END;";

        $query = str_replace("\n", "", $query);

        $view_data['data'] = DB::Procedure($query);
        $view_data['json'] = true;

        return $view_data;
    }

    private function prepareDatetoQuery($date)
    {
        if ($date) {
            return " AND date_arrival_altered = to_date('{$date}', 'DD/MM/YYYY') ";
        }

        return null;
    }

    public function prepareHeaderCols($post)
    {
        $header_cols = (isset($post['cod_group_fk']) ? $post['cod_group_fk'] : array(0 => ''));
        Redis::HSet(USERSESSIONID . ':session:ordercover:list:header-cols', $header_cols);

        return $header_cols;
    }

    public function getHeaderColsFromCache()
    {
        return Redis::HGet(USERSESSIONID . ':session:ordercover:list:header-cols');
    }

    public function preparePagination($post)
    {
        $this->post = $post;
        $this->buildCols();
        $this->query_cols = $this->prepareQueryCols();
        $this->prepareQuery();
        $this->prepareGroupBy();
    }

    public function getGroupedSuggestions($post)
    {
        $suggestions_data = $this->getSuggestionsData($post);
        $data_table = $this->getSelectOptions($suggestions_data);

        return $data_table;
    }

    public function getQtyTotalSuggestions()
    {
        return $this->footer_count;
    }

    public function getHeaderCols()
    {
        $this->header_cols = Redis::HGet(USERSESSIONID . ':session:ordercover:list:header-cols');
        $this->unsetDefaultHeaderCols();
        $this->setTitleonHeaderCols();

        return $this->header_cols;
    }

    private function setTitleonHeaderCols()
    {
        $header_cols_titles = [];
        $group_data = $this->getDynamicHeaderColsTitle();

        foreach ($this->header_cols as $col) {
            $header_cols_titles[$col] = $group_data[$col];
        }

        $this->header_cols = $header_cols_titles;
    }

    private function getDynamicHeaderColsTitle()
    {
        $data = DB::Select('SCM_GROUP_NAME', 'NUM_GROUP_PK, DESC_GROUP', "WHERE id_company_fk = {$this->company_id}");

        foreach ($data as $group) {
            $name = "cod_group{$group['NUM_GROUP_PK']}_fk";
            $header_titles[$name] = $group['DESC_GROUP'];
        }

        return $header_titles;
    }

    private function nextSugHeaderID()
    {
        $exec_next_val = DB::Procedure("SELECT seq_scm_purchase_sug_header.nextval from dual", true);
        if ($exec_next_val) {
            return $exec_next_val[0]['NEXTVAL'];
        }

        return false;
    }

    private function prepareProcessWhere($suggest)
    {
        $extras_filters = $this->getExtraFilters($suggest['dynamicFields']);
        $where = " AND cod_estab_fk = '{$suggest['cod_estab_fk']}' AND default_local = '{$suggest['original_cod_local_fk']}' {$extras_filters}";

        if ($suggest['psug'] !== '-1') {
            $where .= "AND psug_grouped_pk = {$suggest['psug']}";
        } else {
            $where .= $this->prepareDatetoQuery($suggest['DATE_ARRIVAL_ALTERED']);
            $where .= DB::setFields($suggest['dynamicFields']);
            $where .= " AND psug_grouped_pk = -1";
        }

        return $where;
    }

    private function nextPsugGroupedID()
    {
        $exec_next_val = DB::Procedure("SELECT seq_scm_sku_psug_grouped.NEXTVAL FROM dual", true);
        if ($exec_next_val) {
            return $exec_next_val[0]['NEXTVAL'];
        }

        return false;
    }

    private function unsetDefaultHeaderCols()
    {
        $cols_to_unset = [
            'cod_estab_fk',
            'date_arrival',
            'cod_local_fk',
            ' ',
        ];

        foreach ($cols_to_unset as $col) {
            if (in_array($col, $this->header_cols)) {
                $col_key = array_search($col, $this->header_cols);
                unset($this->header_cols[$col_key]);
            }
        }
    }

    private function getSuggestionsData($post)
    {
        $profile = null;
        if (can('supplier')) {
            $profile = $this->setProfile();
        }

        $offset = $this->prepareSetOffset($post['page'], $post['limit']);
        $columns = Redis::Get(USERSESSIONID . ':session:ordercover:list:query-cols');
        $group_by = Redis::Get(USERSESSIONID . ':session:ordercover:list:group-by');
        $order = (isset($post['order']) && is_array($post['order']) ? $this->prepareOrder($post['order']) : $this->prepareDefaultOrder($group_by));
        
        $estabIn = '';
        if(!empty($this->estab)) {
            $estabIn = " AND a.COD_ESTAB_FK IN({$this->estab}) ";
        }

        $this->terms = Redis::Get(USERSESSIONID . ':session:ordercover:list:query-basic') . $estabIn . $profile . $group_by . $order;

        // dd($this->terms);
        $select_result = DB::Page(
            'vw_scr_scm532g_sug_pur_agrup a
            INNER JOIN scm_estab b ON a.id_company_fk = b.id_company_fk
            AND b.cod_estab_pk = a.cod_estab_fk',
            $offset,
            $post['limit'],
            $columns,
            $this->terms
        );

        $this->footer_count = $this->setQtyTotalSuggestions($columns);

        if ($select_result) {
            return $this->prepareData($select_result);
        }

        return false;
    }

    private function setProfile()
    {
        //return "AND a.user_id = {$this->user} ";
        return "AND a.cod_group11_fk = '1' ";
    }

    private function prepareDefaultOrder($columns)
    {
        $col = 'a.auxiliar19';
        if (strpos($columns, 'date_arrival_altered')) {
            $col = 'date_arrival_altered';
        }

        return "ORDER BY {$col} ASC";
    }


    /**
     * Count the total of suggestions, used in pagination.
     */
    private function setQtyTotalSuggestions($columns)
    {
        $suggestions = DB::Select(
            'vw_scr_scm532g_sug_pur_agrup a
            INNER JOIN scm_estab b ON a.id_company_fk = b.id_company_fk
            AND b.cod_estab_pk = a.cod_estab_fk',
            $columns,
            $this->terms
        );

        if ($suggestions) {
            return count($suggestions);
        }

        return 0;
    }

    private function getSelectOptions($data_table)
    {
        foreach ($data_table as $key => $deposit) {
            $estabs_cods = $this->getParams($deposit['estab_cod']);
            $data_table[$key]['deposit'] = $this->getDepositByEstab($deposit['estab_cod']);
            $data_table[$key]['options'] = $estabs_cods;
            $data_table[$key]['opts'] = $this->prepareOptionsArray($estabs_cods);
        }        
        return $data_table;
    }

    private function getDepositByEstab($deposit_id)
    {
        $select = DB::Select('ERP_LOCAL', 'COD_LOCAL_PK, COD_LOCAL_PK || ' - ' || DESC_LOCAL as "DESC"', "WHERE ID_COMPANY_FK = {$this->company_id} AND COD_ESTAB_PK = '{$deposit_id}'");
        return $select;
    }

    private function getParams($deposit_id)
    {
        $select = DB::Select(
            'scm_purchase_sug_param',
            'cod_param, val_param, desc_param',
            "WHERE id_company_fk = {$this->company_id} AND cod_estab_fk = '{$deposit_id}' AND cod_param in ('cod_buyer', 'cod_sector', 'cod_reason') ORDER BY val_param"
        );

        return $select;
    }

    private function prepareOptionsArray($data)
    {
        $arr = [];
        foreach($data as $key => $value) {
            $arr[$value['COD_PARAM']][] = [
                'VAL_PARAM' => $value['VAL_PARAM'],
                'DESC_PARAM' => $value['DESC_PARAM'],
            ];
        }

        return $arr;
    }

    private function prepareData($select_result)
    {
        foreach ($select_result as $result) {
            $dt_exp_req = $result['DATE_ARRIVAL_ALTERED'] ?? $this->getMinDateArrival($result);

            $data_row = [
                'suggestions_qty' => $result['TOTAL_SUGS'],
                'total_qty' => $result['TOTAL_QTY'],
                'estab_name' => $result['DESC_ESTAB'],
                'estab_cod' => $result['COD_ESTAB_FK'],
                'total_value' => Utilities::Currency($result['TOTAL_PRICE']),
                'psug_pk' => $result['PSUG_GROUPED_PK'],
                'sit_urgent' => $result['SIT_URGENT'],
                'default_local' => $result['DEFAULT_LOCAL'],
                'cod_buyer' => $result['COD_BUYER'],
                'cod_sector' => $result['COD_SECTOR'],
                'cod_reason' => $result['COD_REQ_REASON'],
                'dt_exp_req' => $this->formatDate($dt_exp_req)
            ];

            $dynamicData = $this->buildDynamicData($result);
            $data[] = ($dynamicData) ? array_merge($data_row, $dynamicData) : $data_row;
        }

        return $data;
    }

    private function dataFormatter($column, $value)
    {
        if ($value == '-1' || is_array($value)) {
            return '-';
        }

        switch (strtoupper($column)) {
            case "COD_GROUP2_FK":
            case "COD_GROUP3_FK":
            case "COD_GROUP4_FK":
            case "COD_GROUP5_FK":
            case "COD_GROUP6_FK":
            case "COD_GROUP7_FK":
            case "COD_GROUP8_FK":
            case "COD_GROUP9_FK":
            case "COD_GROUP10_FK":
            case "COD_GROUP11_FK":
            case "COD_GROUP12_FK":
                preg_match_all('!\d+!', $column, $matches);
                return Redis::HGet("company:{$this->company_id}:group-{$matches[0][0]}", (string)$value);

            case "SIT_URGENT":
                return $value == '1'  ? $this->lang['global']['yes'] : $this->lang['global']['no'];

            default:
                return Utilities::dataFormatter(strtoupper($column), $value);
        }
    }

    private function formatDate($date)
    {
        if ($date) {
            $this->has_date = true;
            return date('Y-m-d', strtotime($date));
        }

        return null;
    }

    private function buildDynamicData($result)
    {
        $header_cols = $this->getHeaderCols();
        foreach ($header_cols as $col_id => $col_name) {
            $data[$col_id] = [
                'value' => $result[strtoupper($col_id)],
                'name' => $this->dataFormatter($col_id, $result[strtoupper($col_id)]),
            ];
        }

        return $data;
    }

    private function prepareQuery()
    {
        if ($this->post || !Redis::Get(USERSESSIONID . ':session:ordercover:list:query-basic')) {
            $post_filters = $this->unsetDefaults($this->post);
            $filters = $this->buildFilters($post_filters);
            $this->query_basic_terms = "WHERE a.id_company_fk = {$this->company_id} {$filters} ";
            Redis::Set(USERSESSIONID . ':session:ordercover:list:query-basic', $this->query_basic_terms);

            unset($post_filters['cod_estab_fk'], $post_filters['default_local']);
            $save_or_group['save_or_group'] = json_encode($post_filters);
            Redis::HSet(USERSESSIONID . ':session:ordercover:list:query-basic-to-save-or-group', $save_or_group);
        }
    }

    private function buildFilters($post_filters)
    {
        return DB::setFields($post_filters);
    }

    private function prepareGroupBy()
    {
        return (!isset($this->post['cod_group_fk']) && $this->hasGroupedColumns())
            ? $this->getGroupByFromCache()
            : $this->buildGroupBy();
    }

    private function prepareOrder($orders)
    {
        end($orders);
        $last_column = key($orders);
        $order_query = "ORDER BY ";
        foreach ($orders as $column => $col_order) {
            $order_query .= "{$column} {$col_order}";
            if ($last_column != $column) {
                $order_query .= ", ";
            }
        }

        return $order_query;
    }

    private function prepareQueryCols()
    {
        $this->query_cols = DB::getTableCols('vw_scr_scm532g_sug_pur_agrup');
        if (!isset($this->post['cod_group_fk']) || !is_null($this->post['cod_group_fk'])) {
            $cols_set = [
                'a.cod_estab_fk',
                'b.desc_estab',
                'count(id_sug_pk) as total_sugs',
                'a.cod_buyer',
                'a.cod_sector',
                'a.cod_req_reason',
                'a.cod_local_fk',
                'sum(qty_altered) as total_qty',
                'sum(val_unit_erp * qty_altered) as total_price',
                'a.id_company_fk',
                'a.default_local',
                'a.psug_grouped_pk'
            ];

            $this->query_cols = DB::setColumns(array_merge($cols_set, $this->auxiliar_cols));
        }

        if (Redis::Set(USERSESSIONID . ':session:ordercover:list:query-cols', $this->query_cols)) {
            return true;
        }

        return false;
    }

    private function unsetDefaults($post)
    {
        unset($post['cod_group_fk'], $post['filter_temp']);
        return $post;
    }

    private function buildCols()
    {
        $cols_relation = $this->getColsRelation();
        $post_cols = $this->post['cod_group_fk'];

        if (!isset($post_cols) && $this->hasGroupedColumns()) {
            $post_cols = $this->getHeaderColumnsFromCache();
        }

        foreach ($cols_relation as $col => $auxiliar) {
            $col_key = array_search($col, $post_cols);
            if ($post_cols && $col_key !== false) {
                $this->cod_groups[] = strtoupper($post_cols[$col_key]);
                $this->auxiliar_cols[] = $post_cols[$col_key];
            } else {
                $this->auxiliar_cols[] = $auxiliar;
            }
        }

        Redis::HSet(USERSESSIONID . ':session:ordercover:list:query-cod-groups', ['codgroups' => json_encode($this->cod_groups)]);
    }

    private function getColsRelation()
    {
        return [
            'cod_group1_fk' => 'a.auxiliar1',
            'cod_group2_fk' => 'a.auxiliar2',
            'cod_group3_fk' => 'a.auxiliar3',
            'cod_group4_fk' => 'a.auxiliar4',
            'cod_group5_fk' => 'a.auxiliar5',
            'cod_group6_fk' => 'a.auxiliar6',
            'cod_group7_fk' => 'a.auxiliar7',
            'cod_group8_fk' => 'a.auxiliar8',
            'cod_group9_fk' => 'a.auxiliar9',
            'cod_group11_fk' => 'a.auxiliar10',
            'cod_group10_fk' => 'a.auxiliar11',
            'cod_group12_fk' => 'a.auxiliar12',
            'id_profile' => 'a.auxiliar18',
            'date_arrival_altered' => 'a.auxiliar19',
            'id_calendar_fk' => 'a.auxiliar22',
            'user_id' => 'a.auxiliar20',
            'sit_urgent' => 'a.auxiliar21',
        ];
    }

    private function prepareSetOffset($Page, $Limit)
    {
        return ($Page - 1) * $Limit;
    }

    /**
     * @return string
     */
    private function buildGroupBy()
    {
        $columns = ($this->auxiliar_cols ? ", " . implode(",", $this->auxiliar_cols) : null);
        $group_by = "GROUP BY a.id_company_fk {$columns}, a.cod_estab_fk, b.desc_estab, a.default_local, a.cod_buyer, a.cod_sector, a.cod_req_reason, a.cod_local_fk, a.psug_grouped_pk ";
        Redis::Set(USERSESSIONID . ':session:ordercover:list:group-by', $group_by);

        return $group_by;
    }

    /**
     * @return bool|string
     */
    private function getGroupByFromCache()
    {
        return Redis::Get(USERSESSIONID . ':session:ordercover:list:group-by');
    }

    /**
     * @return string
     */
    private function getHeaderColumnsFromCache()
    {
        return Redis::HGet(USERSESSIONID . ':session:ordercover:list:header-cols');
    }

    /**
     * @return bool
     */
    private function hasGroupedColumns()
    {
        return $this->checkOrderCoverCache() && Request::verifyReferer('ordercover/suggestions/list');
    }

    /**
     * @param $items
     * @return string|null
     */
    private function getMinDateArrival($items)
    {
        $where = '';
        $cod_groups = Redis::HGet(USERSESSIONID . ':session:ordercover:list:query-cod-groups');
        $groups = json_decode($cod_groups['codgroups']);
        if (count($groups) > 0 && $items['PSUG_GROUPED_PK'] == '-1') {
            foreach ($groups as $cod_group) {
                $where .= sprintf(
                    " AND %s = '%s'",
                    $cod_group,
                    key_exists($cod_group, $items) ? $items[$cod_group] : ''
                );
            }
        }

        $stmt = DB::SelectRow(
            'VW_SCR_SCM533G_SUG_PUR_DETAIL p',
            'MIN(p.date_arrival_altered) AS date_arrival_altered',
            "WHERE p.id_company_fk = {$items['ID_COMPANY_FK']}
                    AND psug_grouped_pk LIKE '{$items['PSUG_GROUPED_PK']}'
                    AND cod_estab_fk LIKE '{$items['COD_ESTAB_FK']}'
                    AND default_local LIKE '{$items['DEFAULT_LOCAL']}'
                    {$where}"
        );

        return $stmt['DATE_ARRIVAL_ALTERED'];
    }

    /**
     * @return mixed
     */
    public function getConditionsFiltered()
    {
        $filters = Redis::HGet(USERSESSIONID . ':session:ordercover:list:query-basic-to-save-or-group')['save_or_group'];

        return json_decode($filters) ;
    }

    /**
     * @param array $dynamicFields
     * @return string
     */
    public function getExtraFilters($dynamicFields = [])
    {
        $columnsToUnset = array_keys($dynamicFields);
        $filters = $this->getConditionsFiltered();

        foreach ($filters as $fields => $val) {
            $fields = str_replace('_filtro', '', $fields);
            if (!in_array($fields, $columnsToUnset)) {
                $extra_filters[$fields] = $val;
            }
        }

        return $this->buildFilters($extra_filters);
    }
}
