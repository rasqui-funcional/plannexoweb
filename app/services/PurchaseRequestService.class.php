<?php
// SELECT /*+FIRST_ROWS*/ COD_LOCAL, DATE_REQ, QTY_REQ from VW_SCR_ERP303G_PURCHASE_REQ where cod_item = '89249' AND cod_estab = '1' and id_company_fk = 3 order by date_req desc

/**
 * <b>StockBalanceService</b>
 * Service responsible for managing the Stock Balance Graph data
 *
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class PurchaseRequestService
{
    private $columns;
    private $table;
    private $lang;

    public function __construct()
    {
        $this->columns = '/*+FIRST_ROWS*/
                          NUM_REQ,
                          ORIGEM,
                          u.NAME,
                          COD_LOCAL,
                          DESC_LOCAL,
                          DATE_REQ,
                          DATE_EXP,
                          QTY_REQ,
                          QTY_COVERAGE';
        $this->table = 'vw_scr_erp303g_purchase_req p
                        LEFT JOIN erp_local el ON (el.cod_local_pk = p.cod_local
                            AND el.cod_estab_pk = p.cod_estab
                            AND p.id_company_fk = el.id_company_fk)
                        LEFT JOIN UAC_USER u ON (p.id_user = u.ID_USER_PK
                            AND p.ID_COMPANY_FK = u.ID_COMPANY_FK)';
        global $lang;
        $this->lang = $lang;
    }

    /**
     * <b>getStockBalance</b>
     * Retrieve the Purchase Request data to fill the graph.
     *
     * @param STRING $idCompanyPk
     * @param where $where
     * @return ARRAY
     */
    public function getTableData($post, $id_company_fk, $offset, $limit, $search, $order = '')
    {
        $terms = "where p.cod_item = '{$post['skuItem']}' AND p.cod_estab = '{$post['codEstab']}' and p.id_company_fk = {$id_company_fk} {$search} {$order}";
        $query_result = DB::Page($this->table, $offset, $limit, $this->columns, $terms);

        if(!$query_result || is_null($query_result) || empty($query_result)) {
            return [
                'error' => 'not found'
            ];
        }

        $query_result = $this->formatData($query_result);
        $cols = array_keys($query_result[0]);
        $titles = array();

        foreach($cols as $key => $col){
            array_push($titles, $this->lang['info_sku']['erp'][$col]);
        }

        $raw = $this->select($post['skuItem'], $id_company_fk, $post['codEstab'], $search);

        return [
            'result' => $query_result,
            'titles' => $titles,
            'raw_result' => $raw['data'],
            'rows' => $this->totalPurchaseRequest($this->table, $terms),
            'countEstab' => $this->countEstab($raw['data']),
            'countInfo' => $this->countInfo($raw['data'])
        ];
    }

    public function select ($item, $idCompanyFk, $codEstabPk, $order = 'order by p.date_req desc', $search = '' )
    {
        $terms = "where p.cod_item = '{$item}' AND p.cod_estab = '$codEstabPk' and p.id_company_fk = $idCompanyFk {$search} $order";
        $result = DB::Select($this->table, $this->columns, $terms);

        $result = $this->formatData($result);

        return [
            'count_desc_local' => $this->lang['info_sku']['erp'],
            'titles' => $this->lang['info_sku']['erp'],
            'data' => $result,
        ];
    }

    private function countEstab($result)
    {
        $total = 0;
        foreach($result as $row => $array) {
            foreach($array as $column => $value) {
                if($column == 'COD_LOCAL') {
                    $total = $total + $value;
                }
            }
        }

        return $total;
    }

    private function countInfo($result)
    {
        $totalQuantity = 0;
        foreach($result as $row => $array) {
            foreach($array as $column => $value) {
                if($column == 'QTY_REQ') {
                    $totalQuantity = $totalQuantity + $value;
                }
            }
        }

        return [
            'QTY_REQ' => Utilities::Unity($totalQuantity)
        ];
    }

    /**
     * Format rows before sending to Controller.
     * @param rows
     * @return array
     */
    private function formatData(array $rows) : array
    {
        $result  = [];
        foreach ($rows as $key => $row) {
            $result[$key] = [
                'COD_REQ' => $row['NUM_REQ'],
                'ORIGEM' => $row['ORIGEM'],
                'NAME' => $row['NAME'],
                'DESC_LOCAL' => "{$row['COD_LOCAL']} - {$row['DESC_LOCAL']}",
                'DATE_REQ' => $row['DATE_REQ'],
                'DATE_EXP_REQ' => $row['DATE_EXP'],
                'QTY_REQ' => $row['QTY_REQ'],
                'QTY_COVERAGE' => $row['QTY_COVERAGE']
            ];
        }

        return $result;
    }

    private function totalPurchaseRequest($table, $terms)
    {
        return DB::SelectRow($table, 'count(*) AS total', $terms);
    }
}
