<?php

/**
 * LoginService
 * Servers the Login controller with autenthic, validate and check user login.
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class LoginService
{
    private $userData;

    public function setLogin($post)
    {
        $getUserData = false;
        $checkData = $this->preparelogin($post);

        try {
            $columns = "
                a.mail_login as mail_login,
                a.mail as email,
                a.name as name,
                a.id_user_pk as id_user_pk,
                a.date_added as created_at,
                a.id_company_fk as id_company_fk,
                a.type as type,
                case a.type
                    when 0 then 'buyer'
                    when 1 then 'buyer'
                    when 2 then 'supplier'
                    when 3 then 'sysadmin'
                end as user_type,
                c.desc_group as profile,
                d.company_name as company_name,
                d.company_reg  as company_reg,
                (CASE l.LANG_VALUE 
                    WHEN 'es;es_pe' THEN 'pe'
                    WHEN 'es;es_ar' THEN 'ar'
                    WHEN 'es;es_co' THEN 'co'
                    ELSE 'br'
                END) AS country_lang,
                (CASE l.LANG_VALUE 
                    WHEN 'es;es_pe' THEN 'PEN'
                    WHEN 'es;es_ar' THEN 'ARS'
                    WHEN 'es;es_co' THEN 'COP'
                    ELSE 'BRL'
                END) AS country_currency";

            $table = '
                uac_user a
                    left join uac_profile_user b
                        on a.id_company_fk=b.id_company_fk
                            and b.id_user_fk=a.id_user_pk
                    left join uac_profile c
                        on b.id_company_fk=c.id_company_fk
                            and c.id_profile_pk = b.id_profile_fk
                    inner join adm_company d
                        on d.id_company_pk=a.id_company_fk
                    LEFT JOIN ADM_LANGUAGE l 
                        ON l.ID_LANGUAGE_PK = d.ID_LANGUAGE_PK';

            $terms = "
                WHERE a.mail_login = '{$checkData['mail_login']}'
                AND a.active = 1
                AND a.password = '{$checkData['password']}'";

            $getUserData = DB::Select($table, $columns, $terms);
            Logger::debug(__CLASS__ . ":login:result", [$getUserData]);
        } catch (Exception $exception) {
            Logger::debug(__CLASS__ . ":login:resultset", [
                $exception->getCode(),
                $exception->getFile(),
                $exception->getMessage(),
                $exception->getTrace()
            ]);
        }

        if ($getUserData && $this->createSession($getUserData)) {
            return $this->userData;
        }

        return false;
    }

    public function logout()
    {
        if (Redis::Del(Login::getSessionID() . ":session")) {
            return true;
        }

        return false;
    }

    private function createSession($userData)
    {

        $userData = $this->buildUserData($userData);
        $this->prepareSessionData($userData);

        $saveSession = null;
        try {
            $saveSession = Redis::HSet(Login::getSessionID() . ":session", $this->userData, null, 86400);
            Logger::debug(__CLASS__ . ":redis:createsession", [$saveSession]);
        } catch (Exception $exception) {
            Logger::debug(__CLASS__ . ":redis:createsession", [
                $exception->getCode(),
                $exception->getFile(),
                $exception->getMessage(),
                $exception->getTrace()
            ]);
        }

        /**
         * Simulates the apache session($_SESSION) using data from the logged user
         */
        if ($saveSession) {
            return true;
        }

        return false;
    }

    private function prepareLogin($post)
    {
        $data['mail_login'] = $post['login'];
        $data['password'] = hash('sha256', $post['password']);

        return $data;
    }

    private function prepareSessionData($userData)
    {
        $this->userData = [
            'currency' => $userData['COUNTRY_CURRENCY'],
            'lang' => $userData['COUNTRY_LANG'],
            'id_company_pk' => $userData['ID_COMPANY_FK'],
            'company_name' => $userData['COMPANY_NAME'],
            'user_id' => $userData['ID_USER_PK'],
            'name' => $userData['NAME'],
            'initials' => $this->findInitialsName($userData['NAME']),
            'email' => $userData['MAIL_LOGIN'],
            'profile' => $userData['USER_TYPE'],
            'register' => MaskHelper::cnpjCpf($userData['COMPANY_REG']),
            'user_profiles' => $this->setProfiles($userData['PROFILES']),
            'created_at' => $userData['CREATED_AT'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'estab' =>  $this->prepareUserEstabs($userData),
        ];
    }

    private function prepareUserEstabs($userData)
    {
        $estabs = (new UserEstabsService($userData))->getEstabs();
        if (!empty($estabs)) {

            return "'" . implode("', '", (new UserEstabsService($userData))->getEstabs()) . "'";
        }

        return '';
    }

    private function buildUserData($users)
    {
        $first_user = $users[0];
        $first_user['PROFILES'] = [];
        if (!is_null($first_user['PROFILE'])) {
            $first_user['PROFILES'] = $this->buildProfiles($users);
        }

        unset($first_user['PROFILE']);

        return $first_user;
    }

    private function buildProfiles($users)
    {
        return array_map(function ($user) {
            return $user['PROFILE'];
        }, $users);
    }

    private function setProfiles($profiles)
    {
        $response = '';
        if (!empty($profiles)) {
            foreach ($profiles as $profile) {
                $response .= $profile . ', ';
            }
        }

        return $response;
    }

    private function findInitialsName($name, $length = 2)
    {
        $letters = explode(' ', $name);
        if (count($letters) == 1) {
            return substr(strtoupper($letters[0]), 0, $length);
        }

        $initial = '';
        for ($cont = 0; $cont < count($letters); $cont++) {
            $initial .= $letters[$cont]{0};
        }

        $initialName = strtoupper($initial);
        return substr($initialName, 0, $length);
    }
}
