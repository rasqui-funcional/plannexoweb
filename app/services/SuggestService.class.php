<?php

/**
 * <b>SuggestService</b>
 * Service responsible for managing Suggest methods
 *
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class SuggestService
{
    private $table = 'vw_scr_scm420g_sug';
    private $columns = '';
    private $groupings = null;
    private $id_company_fk = null;
    private $conn;

    public function __construct($groupings)
    {
        $this->columns = DB::getTableCols($this->table);
        $this->groupings = $groupings;
        $this->id_company_fk = Login::getUserSession()['id_company_pk'];
    }

    /**
     * <b>getTable</b>
     * Retrieve the data that fills the suggest table.
     * @param STRING $offset .
     * @param STRING $limit .
     * @param STRING $columns .
     * @param STRING $terms .
     * @return ARRAY
     */
    public function getTable($offset, $limit, $columns, $terms, $orderby = 'ORDER BY RISK_SUPPLIER DESC, id_sku_pk')
    {
        if (!is_array($columns)) {
            $columns = explode(',', $columns);
        }

        $columns = implode(', ', $columns);
        $columns = str_replace(', obs', ', dbms_lob.substr(obs, 4000, 1 ) as obs', $columns);
        $table = "vw_scr_scm420g_sug g";

        if ($offset != 0) {
            $offset++;
            $limit--;

            $limit = $offset + $limit;
        }

        // return DB::Page($table, $offset, $limit, $columns, $terms);
        $select = "( 
            SELECT 
                $columns, 
                ROW_NUMBER() OVER($orderby) R
            FROM
                $table 
                $terms
        ) WHERE R BETWEEN $offset AND $limit";

        return DB::Select($select, '', '');
    }

    public function avoidJoin($items) 
    {
        $cod_groups = [
            'COD_GROUP1_FK', 
            'COD_GROUP2_FK', 
            'COD_GROUP3_FK', 
            'COD_GROUP4_FK', 
            'COD_GROUP5_FK', 
            'COD_GROUP6_FK', 
            'COD_GROUP7_FK', 
            'COD_GROUP8_FK', 
            'COD_GROUP9_FK', 
            'COD_GROUP10_FK', 
            'COD_GROUP11_FK', 
            'COD_GROUP12_FK'
        ];
        
        foreach ($items as $key => $item) {
            /**
             * Join de supplier
             */
            $supplier = '';
            if ($item["LAST_PURCHASE_SUPPLIER"] > 0) {
                $supplier = DB::SelectRow("erp_supplier", "desc_supplier", "WHERE ID_COMPANY_FK={$this->id_company_fk} AND ID_SUPPLIER_PK = '{$item["LAST_PURCHASE_SUPPLIER"]}'");    
            }
            $item["LAST_PURCHASE_SUPPLIER"] = isset($supplier["DESC_SUPPLIER"]) ? $supplier["DESC_SUPPLIER"] : '';

            /**
             * Join de local
             */
            $local = DB::SelectRow(
                "erp_local", 
                "cod_local_pk || '-' || desc_local AS DEFAULT_LOCAL", 
                "WHERE ID_COMPANY_FK={$this->id_company_fk} AND cod_local_pk = '{$item["DEFAULT_LOCAL"]}' AND cod_estab_pk = '{$item["COD_ESTAB_FK"]}'"
            );
            $item["DEFAULT_LOCAL"] = isset($local["DEFAULT_LOCAL"]) ? $local["DEFAULT_LOCAL"] : '';

            $item["CAN_GROUP"] = false;

            if ($item['NUM_PARC_PK'] == 1 && $item['SIT_URGENT'] == 1 && $this->validSevenDayDiff($item['DATE_ARRIVAL_ALTERED'], $item['ID_PSUG_PK'])) {
                $item["CAN_GROUP"] = true;
            }

            /**
             * Join de cod_group
             */
            foreach ($cod_groups as $key_group => $group) {
                $cache = Redis::HGet( sprintf('company:%s:group-%s', $this->id_company_fk, $key_group+1) );
                $item[$group] = isset($cache[$item[$group]]) ? $cache[$item[$group]] : '';
                $items[$key] = $item;
            }
        }
        
        return $items;
    }

    public function getTableOci($offset, $limit, $columns, $terms, $orderby = 'ORDER BY RISK_SUPPLIER DESC, id_sku_pk')
    {
        if (!is_array($columns)) {
            $columns = explode(',', $columns);
        }

        $columns = implode(', ', $columns);
        $columns = str_replace(', obs', ', dbms_lob.substr(obs, 4000, 1 ) as obs', $columns);
        $columns = str_replace('date_arrival_altered', "TO_CHAR(date_arrival_altered, 'YYYY-MM-DD') date_arrival_altered", $columns);

        $table = "vw_scr_scm420g_sug g";

        if ($offset != 0) {
            $offset++;
            $limit--;
            $limit = $offset + $limit;
        }

        $select = "SELECT * FROM ( 
                        SELECT 
                            {$columns}, 
                            ROW_NUMBER() OVER({$orderby}) R
                        FROM
                            {$table} 
                            {$terms}
                    ) WHERE R BETWEEN {$offset} AND {$limit}
                    ";

        $this->conn = oci_pconnect(
            getenv("DBUSUARIO"),
            getenv("DBSENHA"),
            getenv("DBSERVIDORNAME"),
            'AL32UTF8'
        );

        if (!$this->conn) {
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        $stid = oci_parse($this->conn, "{$select}");
        oci_execute($stid,OCI_DEFAULT);
        $nrows = oci_fetch_all($stid, $result, null, null, OCI_FETCHSTATEMENT_BY_ROW);

        return $result;
    }

    public function avoidJoinOci($items)
    {
        $cod_groups = [
            'COD_GROUP1_FK',
            'COD_GROUP2_FK',
            'COD_GROUP3_FK',
            'COD_GROUP4_FK',
            'COD_GROUP5_FK',
            'COD_GROUP6_FK',
            'COD_GROUP7_FK',
            'COD_GROUP8_FK',
            'COD_GROUP9_FK',
            'COD_GROUP10_FK',
            'COD_GROUP11_FK',
            'COD_GROUP12_FK'
        ];
        foreach ($items as $key => $item) {
            /**
             * Join de supplier
             */
            $supplier = '';
            if ($item["LAST_PURCHASE_SUPPLIER"] > 0) {
                $select = "SELECT desc_supplier FROM erp_supplier WHERE ID_COMPANY_FK={$this->id_company_fk} AND ID_SUPPLIER_PK = '{$item["LAST_PURCHASE_SUPPLIER"]}'";
                $stid = oci_parse($this->conn, "{$select}");
                oci_execute($stid,OCI_DEFAULT);
                $supplier = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
            }
            $item["LAST_PURCHASE_SUPPLIER"] = isset($supplier["DESC_SUPPLIER"]) ? $supplier["DESC_SUPPLIER"] : '';
            /**
             * Join de local
             */
            $select = "SELECT cod_local_pk || '-' || desc_local AS DEFAULT_LOCAL FROM erp_local WHERE ID_COMPANY_FK={$this->id_company_fk} AND cod_local_pk = '{$item["DEFAULT_LOCAL"]}' AND cod_estab_pk = '{$item["COD_ESTAB_FK"]}'";
            $stid = oci_parse($this->conn, "{$select}");
            oci_execute($stid,OCI_DEFAULT);
            $local = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
            $item["DEFAULT_LOCAL"] = isset($local["DEFAULT_LOCAL"]) ? $local["DEFAULT_LOCAL"] : '';

            $item["CAN_GROUP"] = false;

            if ($item['NUM_PARC_PK'] == 1 && $item['SIT_URGENT'] == 1 && $this->validSevenDayDiff($item['DATE_ARRIVAL_ALTERED'], $item['ID_PSUG_PK'])) {
                $item["CAN_GROUP"] = true;
            }

            /**
             * Join de cod_group
             */
            foreach ($cod_groups as $key_group => $group) {
                $cache = Redis::HGet(sprintf('company:%s:group-%s', $this->id_company_fk, $key_group + 1));
                $item[$group] = isset($cache[$item[$group]]) ? $cache[$item[$group]] : '';
                $items[$key] = $item;
            }
        }
        return $items;
    }

    public function prepareData($data)
    {
        $result = [];
        foreach ($data as $key => $value) {
            $result[$key] = $this->formatData($key, $value);
        }

        return $result;
    }

    public function prepareDisplayColumns($columns)
    {
        if (!is_null($columns)) {
            return $columns;
        }

        $arr_columns = $this->transformStringToArray($this->getColumns());
        $this->unsetColumnsInactive($arr_columns);

        return $this->getColumns();
    }

    public function prepareQueryColumns($columns): string
    {
        $columns = DB::getTableCols($this->table);        
        $columns = str_replace(', obs', ', dbms_lob.substr(obs, 4000, 1 ) as obs', $columns);
        $columns .= ', id_sug_pk, total_out_last_half_month, total_out_last_month, grouped_installments';

        return $columns;
    }

    /**
     * @return string
     */
    public function getColumns()
    {
        return $this->columns;
    }
    
    public function unsetColumnsInactive($columns)
    {
        //formato pra coluna segundo a query
        $format = 'COD_GROUP%d_FK';
        //varre os grupos
        foreach ($this->groupings as $key => $value) {
            $str = str_replace('%d', $key, $format);
            if ($uid = array_search(strtolower($str), $columns)) {
                unset($columns[$uid]);
            }
        }
        //manda as colunas ativas
        $this->setColumns($columns);
    }

    public function validSevenDayDiff($start, $id_psug)
    {
        $columns = 'ID_COMPANY_FK, ID_PSUG_PK, NUM_PARC_PK, DATE_ARRIVAL_ALTERED';
        $table = 'VW_SCR_SCM420G_SUG';
        $terms = sprintf("WHERE ID_COMPANY_FK = '%s' AND ID_PSUG_PK = '%s' AND NUM_PARC_PK = 2"
            , Login::getUserSessionByKey('id_company_pk'), $id_psug);

        $stmt = DB::SelectRow($table, $columns, $terms);

        if(!$stmt){
          return false;
        }

        $dateStart = new DateTime($start);
        $dateNow = new DateTime($stmt['DATE_ARRIVAL_ALTERED']);
        $dateDiff = $dateStart->diff($dateNow);

        if ($dateDiff->m == 0 && $dateDiff->d <= 7) {
            return true;
        }

        return false;
    }

    private function formatData($column, $value)
    {
        switch (strtolower($column)) {
            case 'out5':
            case 'avg_3m':
            case 'avg_6m':
            case 'qty_pp':
            case 'avg_12m':
            case 'inv_total':
            case 'qty_excess':
            case 'qty_pp_erp':
            case 'qty_min_out':
            case 'qty_max_out':
            case 'qty_max_erp':
            case 'qty_min_erp':
            case 'qty_next_in':
            case 'qty_last_in':
            case 'qty_original':
            case 'out_last_30d':
            case 'qty_last_out':
            case 'inv_available':
            case 'qty_min_out_ori':
            case 'qty_target':
            case 'qty_max_out_ori':
            case 'qty_avg_cons_erp':
            case 'last_purchase_qty':
            case 'out_month_minus1':
            case 'out_month_minus2':
            case 'out_month_minus3':
            case 'out_month_minus4':
            case 'out_month_minus5':
            case 'out_month_minus6':
            case 'out_month_minus7':
            case 'out_month_minus8':
            case 'out_month_minus9':
            case 'out_month_minus10':
            case 'out_month_minus11':
            case 'out_month_minus12':
            case 'out_current_month':
                return Utilities::Unity($value);

            case 'vl_excess':
            case 'val_altered':
            case 'total_money':
            case 'qty_forecast':
            case 'available_money':
            case 'last_purchase_price':
                return Utilities::Currency($value);

            case 'out6':
                return number_format($value, 2, ',', '');

            case 'out_yesterday':
            case 'service_level_real':
            case 'service_level_target':
            case 'out_yesterday_num_dev':
                return number_format($value, 2, '.', '');
                break;

            case 'days_no_out':
                return '± ' .str_replace('.', '', Utilities::Unity($value));

            default:
                return $value;
        }
    }

    /**
     * @param string $columns
     */
    private function setColumns($columns): void
    {
        $this->columns = $columns;
    }

    private function transformStringToArray($str_columns)
    {
        $arr = explode(',', $str_columns);
        return array_map('trim', $arr);
    }

    private function transformArrayToString($arr_columns)
    {
        return implode(', ', $arr_columns);
    }

    /**
     * @param int $id_psug_pk
     * @return array
     */
    public function getSuggestToGroup(int $id_psug_pk)
    {
        $id_company_fk = (int)Login::getUserSession()['id_company_pk'];
        $columns = "qty_altered,
                    id_psug_pk,
                    TO_CHAR(date_arrival_altered, 'YYYY-MM-DD') as date_arrival_altered,
                    inv_level,
                    desc_item,
                    cod_item_pk";
        $table = " (SELECT
                        SUM(qty_altered)AS qty_altered
                    FROM
                        scm_sku_purchase_sug 
                    WHERE
                        id_psug_pk = {$id_psug_pk}
                        and (num_parc_pk = 1
                        or num_parc_pk = 2)
                        and id_company_fk = {$id_company_fk} 
                    ) 
                , (SELECT
                        id_psug_pk,
                        num_parc_pk,
                        date_arrival_altered,
                        nvl(inv_level, 0) as inv_level,
                        desc_item,
                        cod_item_pk
                    FROM
                        scm_sku_purchase_sug sug
                    inner join vw_scm_aux_sku aux on
                        sug.id_sku_fk = aux.id_sku_pk
                        and sug.id_company_fk = aux.id_company_fk
                    WHERE
                        sug.id_psug_pk = $id_psug_pk
                        and sug.num_parc_pk = 1
                        and sug.id_company_fk = {$id_company_fk} 
                ) ";

        try {
            if (!$result = DB::SelectRow($table, $columns)) {
                $result = [];
            }

            return [
                'id_psug_pk' => (int)$result['ID_PSUG_PK'],
                'qty_altered' => (int)$result['QTY_ALTERED'],
                'date_arrival_altered' => $result['DATE_ARRIVAL_ALTERED'] ?? '',
                'inv_level' => (int)$result['INV_LEVEL'],
                'desc_item' => $result['DESC_ITEM'] ?? '',
                'cod_item_pk' => (int)$result['COD_ITEM_PK']
            ];
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }


}
