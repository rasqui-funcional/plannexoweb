<?php

/**
 * <b>ResumeFiltersService</b>
 * Service to filter Resume page;
 * @param INT $companyId
 */
class ResumeFiltersService
{
    private $company_id;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
        $this->FieldsService = new Fields();
    }

    public function getFiltersFields()
    {
        return $this->FieldsService->getFields();
    }

    public function getSavedFilters()
    {
        return Fields::getSelectFilter('SCM429G');
    }

    public function getDinamicFieldsGrouping()
    {
        return $this->FieldsService->getNewDinamicFieldsGrouping();
    }
}
