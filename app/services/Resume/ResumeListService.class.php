<?php

class ResumeListService
{
    private $user;
    private $company_id;
    private $stock_coverage_columns = [];

    public function __construct($company_id)
    {
        $this->user = Login::getUserSession();
        $this->company_id = $company_id;
    }

    public function getCSVData($columns, $terms, $order, $header_cols)
    {
        $data = $this->prepareQuery($columns, $order, $header_cols);
        $order = $data['order'];
        $columns = $data['columns'];

        $query_result = DB::Select(
            'VW_SCR_SCM429G_AGROUPED_OUT',
            $columns,
            $terms . $order
        );

        if ($query_result) {
            return $this->prepareCSVData($query_result, $header_cols);
        }

        return false;
    }

    public function getData($page, $limit, $columns, $terms, $order, $header_cols)
    {
        $data = $this->prepareQuery($columns, $order, $header_cols);
        $order = $data['order'];
        $columns = $data['columns'];

        $query_result = DB::Page(
            'VW_SCR_SCM429G_AGROUPED_OUT',
            Utilities::setOffset($page, $limit),
            $limit,
            $columns,
            $terms . $order
        );

        if ($query_result) {
            return $this->prepareData($query_result);
        }

        return false;
    }

    public function getTotalItems($terms, $columns)
    {
        $query_result = DB::Select('VW_SCR_SCM429G_AGROUPED_OUT', $columns, $terms);

        if ($query_result) {
            return count($query_result);
        } else {
            return 0;
        }
    }

    private function codEstabByUser()
    {
        if(!empty($this->user['estab'])) {

            return " AND COD_ESTAB_FK IN({$this->user['estab']}) ";
        }

        return '';
    }

    public function setTerms($filters)
    {
        $estabIn = isset($filters['cod_estab_fk']) ? '' : $this->codEstabByUser();

        $group_by = $this->prepareGroupBy($filters['group_columns'], $filters['exibition_columns']);
        $filters = $this->clearFilters($filters);
        return "WHERE id_company_fk = {$this->company_id} {$estabIn} " . DB::setFields($filters) . $group_by;
    }

    public function setColumns($exitibion_columns, $group_columns)
    {
        $exitibion_columns = array_unique(array_merge($exitibion_columns, $group_columns), SORT_REGULAR);

        return [
            'query_columns' => $this->setQueryColumns($exitibion_columns),
            'redirect_columns' => $this->getRedirectColumns($exitibion_columns)
        ];
    }

    public function setExibitionsColumns($redirect_columns, $header_cols)
    {
        $exitibion_columns = array();
        foreach ($header_cols as $column => $value) {
            $exitibion_columns[] = $column;
        }

        return array_unique(array_merge($exitibion_columns, $redirect_columns), SORT_REGULAR);
    }

    private function setQueryColumns($columns)
    {
        $total_skus_key = array_search('total_skus', $columns);
        if ($total_skus_key !== false) {
            unset($columns[$total_skus_key]);
        }

        $query_columns = 'COUNT(*) as total_skus';

        $columns_type = $this->getColumnsType();
        foreach ($columns as $key => $column) {
            $column = strtolower($column);
            if (array_key_exists($column, $columns_type)) {
                $columns[$key] = "{$columns_type[$column]}({$column}) as $column";
            }
        }

        return $columns ? $query_columns . ', ' . DB::setColumns($columns) : null;
    }

    private function getRedirectColumns($columns)
    {
        $cols_to_unset = $this->getColumnsType();

        $total_skus_key = array_search('total_skus', $columns);
        if ($total_skus_key !== false) {
            unset($columns[$total_skus_key]);
        }

        foreach ($columns as $key => $col) {
            if (isset($cols_to_unset[strtolower($col)])) {
                unset($columns[$key]);
            }
        }

        return $columns;
    }

    private function getColumnsType()
    {
        return [
            'sku_life_cycle' => 'SUM',
            'inv_total_money' => 'SUM',
            'inv_available_money' => 'SUM',
            'qty_min_out_money' => 'SUM',
            'qty_target_money' => 'SUM',
            'qty_max_out_money' => 'SUM',
            'qty_excess' => 'SUM',
            'qty_min_out_ori_money' => 'SUM',
            'qty_max_out_ori_money' => 'SUM',
            'qty_min_erp_money' => 'SUM',
            'qty_max_erp_money' => 'SUM',
            'val_purchase_sugg' => 'SUM',
            'val_purchase_req' => 'SUM',
            'val_purchase_order' => 'SUM',
            'out_current_month' => 'SUM',
            'out_month_minus1' => 'SUM',
            'out_month_minus2' => 'SUM',
            'out_month_minus3' => 'SUM',
            'out_month_minus4' => 'SUM',
            'out_month_minus5' => 'SUM',
            'out_month_minus6' => 'SUM',
            'out_month_minus7' => 'SUM',
            'out_month_minus8' => 'SUM',
            'out_month_minus9' => 'SUM',
            'out_month_minus10' => 'SUM',
            'out_month_minus11' => 'SUM',
            'out_month_minus12' => 'SUM',
            'cod_item_pk' => 'COUNT',
        ];
    }

    public function setOrder($orders)
    {
        return DB::prepareOrder($orders);
    }

    public function setFooterData($terms, $columns)
    {
        $query_result = DB::Select('VW_SCR_SCM429G_AGROUPED_OUT', $this->prepareFooterDataColumns($columns), $terms);

        $data = [''];
        if ($query_result) {
            $data = $this->prepareFooterData($query_result);
        }

        return $data;
    }

    private function prepareFooterDataColumns($columns)
    {
        $columns_array = explode(', ', $columns);
        $cols_to_unset = $this->getFooterColstoUnset();

        $footer_columns = array();
        foreach($columns_array as $column) {
            if(!in_array($column, $cols_to_unset)) {
                $footer_columns[] = $column;
            }
        }

        return implode(', ', $footer_columns);
    }

    private function getFooterColstoUnset()
    {
        return [
            'cod_estab_fk',
            'curve_abc',
            'curve_xyz',
            'curve_pqr',
            'curve_123',
            'cod_group1_fk',
            'cod_group2_fk',
            'cod_group3_fk',
            'cod_group4_fk',
            'cod_group5_fk',
            'cod_group6_fk',
            'cod_group7_fk',
            'cod_group8_fk',
            'cod_group9_fk',
            'cod_group10_fk',
            'cod_group11_fk',
            'cod_group12_fk',
            'id_profile_fk',
            'user_id',
            'inv_level'
        ];
    }


    public function getDynamicHeaderColsTitle()
    {
        $data = DB::Select('SCM_GROUP_NAME', 'NUM_GROUP_PK, DESC_GROUP', "WHERE id_company_fk = {$this->company_id}");

        foreach ($data as $group) {
            $name = "cod_group{$group['NUM_GROUP_PK']}_fk";
            $header_titles[$name] = $group['DESC_GROUP'];
        }

        return $header_titles;
    }

    private function clearFilters($filters)
    {
        unset($filters['exibition_columns'], $filters['group_columns'], $filters['chk_check_all_exibicao'], $filters['order'], $filters['filter_temp']);

        return $filters;
    }

    private function prepareGroupBy($group_columns, $exitibion_columns)
    {
        $query_columns = $group_columns;
        $columns_to_ignore = $this->getColumnsType();

        $total_skus_key = array_search('total_skus', $exitibion_columns);
        if ($total_skus_key !== false) {
            unset($exitibion_columns[$total_skus_key]);
        }

        foreach ($exitibion_columns as $column) {
            if (!in_array($column, $group_columns) && !isset($columns_to_ignore[strtolower($column)])) {
                $query_columns[] = $column;
            }
        }

        if ($query_columns) {
            return " GROUP BY " . implode(', ', $query_columns);
        } else {
            return null;
        }
    }

    private function prepareCSVData($query_result, $header_cols)
    {
        $data = [];
        foreach ($query_result as $row => $rowdata) {
            foreach ($rowdata as $col => $value) {
                $data[$row][strtolower($col)] = $this->formatData($col, $value);
            }

            $data[$row] = $this->buildCSVWithHeaderColumnsOrder(
                $data[$row],
                $this->pushCoverageColumns($header_cols)
            );
        }

        return $data;
    }

    private function buildCSVWithHeaderColumnsOrder($data, $header_cols)
    {
        $ordered_data = [];
        foreach(array_keys($header_cols) as $column) {
            $ordered_data[$column] = $data[$column];
        }

        return $ordered_data;
    }

    private function pushCoverageColumns($header_cols)
    {
        $data = [];
        foreach (array_keys($header_cols) as $key) {
            $stock_coverage_keys = array_keys($this->stock_coverage_columns);

            if (in_array($key, $stock_coverage_keys)) {
                $data[$key] = '';
                $data[$this->stock_coverage_columns[$key]] = '';
            } else {
                $data[$key] = '';
            }
        }

        return $data;
    }

    private function prepareData($query_result)
    {
        $data = array();
        foreach ($query_result as $row => $row_data) {
            foreach ($row_data as $col => $value) {
                $column = strtolower($col);

                if (in_array($column, array_keys($this->stock_coverage_columns))) {
                    $stock_coverage_days = $this->pushStockCoverageDays($row_data, $column);

                    $data[$row][$column] = [
                        'name' => $this->formatData($col, $value) . $stock_coverage_days,
                        'value' => $value
                    ];
                } else {
                    $data[$row][$column] = [
                        'name' => $this->formatData($col, $value),
                        'value' => $value
                    ];
                }
            }
        }
        return $data;
    }

    private function prepareFooterData($query_result)
    {
        $data = array();

        foreach ($query_result as $row => $rowdata) {
            foreach ($rowdata as $col => $value) {
                $data['footer'][strtolower($col)] +=  $value;
                $data[$row][strtolower($col)] = $this->formatData($col, $value);
            }
        }

        return $this->formatFooterValues($data['footer']);
    }

    private function formatFooterValues($data)
    {
        foreach ($data as $key => $value) {
            $data[$key] = $this->formatData($key, $value);
        }

        return $data;
    }

    private function formatData($col, $value)
    {
        switch (strtolower($col)) {
            case 'inv_total_money':
            case 'available_money':
            case 'qty_min_out_money':
            case 'qty_target_money':
            case 'qty_max_out_money':
            case 'qty_excess':
            case 'inv_total_money':
            case 'out_current_month':
            case 'out_month_minus1':
            case 'out_month_minus2':
            case 'out_month_minus3':
            case 'out_month_minus4':
            case 'out_month_minus5':
            case 'out_month_minus6':
            case 'out_month_minus7':
            case 'out_month_minus8':
            case 'out_month_minus9':
            case 'out_month_minus10':
            case 'out_month_minus11':
            case 'out_month_minus12':
            case 'inv_available_money':
            case 'qty_min_out_ori_money':
            case 'qty_max_out_ori_money':
            case 'qty_min_erp_money':
            case 'qty_max_erp_money':
            case 'val_purchase_sugg':
            case 'val_purchase_req':
            case 'val_purchase_order':
                return Utilities::Currency($value);
                break;
            case 'cod_group2_fk':
            case 'cod_group3_fk':
            case 'cod_group4_fk':
            case 'cod_group5_fk':
            case 'cod_group6_fk':
            case 'cod_group7_fk':
            case 'cod_group8_fk':
            case 'cod_group9_fk':
            case 'cod_group10_fk':
            case 'cod_group11_fk':
            case 'cod_group12_fk':
                preg_match_all('!\d+!', $col, $matches);
                return Redis::HGet("company:{$this->company_id}:group-{$matches[0][0]}", (string) $value);
                break;
            case 'sku_life_cycle':
            case 'total_skus':
                return Utilities::Unity($value);
                break;
            default:
                return Utilities::dataFormatter(strtoupper($col), $value);
                break;
        }
    }

    private function pushCoverageDaysIntoColumns($columns)
    {
        foreach($this->stock_coverage_columns as $key => $value) {
            $columns .= ', ';
            $columns = $columns . "
                ROUND(
                   SUM({$key}) /
                     CASE WHEN SUM(qty_forecast * val_unit_erp) = 0 THEN 1 ELSE SUM(qty_forecast * val_unit_erp) END,
                   0) AS {$value}
            ";
        }

        return $columns;
    }

    private function pushStockCoverageDays($data, $column)
    {
        $stock_coverage_column = $this->stock_coverage_columns[$column];
        return " ({$data[strtoupper($stock_coverage_column)]} dias)";
    }

    private function buildStockCoverageColumns($columns)
    {
        $all_stock_coverage_columns = [
            'inv_total_money' => 'inv_total_coverage',
            'inv_available_money' => 'inv_available_coverage',
            'qty_min_out_money' => 'qty_min_out_coverage',
            'qty_target_money' => 'qty_target_coverage',
            'qty_max_out_money' => 'qty_max_out_coverage',
            'qty_excess' => 'qty_excess_coverage',
            'qty_min_out_ori_money' => 'qty_min_out_ori_coverage',
            'qty_max_out_ori_money' => 'qty_max_out_ori_coverage',
            'qty_min_erp_money' => 'qty_min_erp_coverage',
            'qty_max_erp_money' => 'qty_max_erp_coverage'
        ];

        foreach ($all_stock_coverage_columns as $key => $value) {
            if (strpos($columns, $key)) {
                $this->stock_coverage_columns[$key] = $value;
            }
        }
    }

    private function prepareQuery($columns, $order, $header_cols)
    {
        $data = [
            'order' => $order,
            'columns' => '',
        ];

        if ($order === '') {
            $first_grouping = array_keys($header_cols)[0];
            $data['order'] = " order by {$first_grouping} ASC";
        }

        $this->buildStockCoverageColumns($columns);
        $data['columns'] = $this->pushCoverageDaysIntoColumns($columns);

        return $data;
    }
}
