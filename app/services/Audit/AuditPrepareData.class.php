<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 26/02/19
 * Time: 15:35
 */

class AuditPrepareData
{
    public function basicInformations(array $data):array
    {
        $result = [];
        foreach (BASIC_INFORMATION_PARAMS as $param) {
            if (array_key_exists($param, $data['base'])) {
                $result[$param] = $data['base'][$param];
                continue;
            }

            if (array_key_exists(strtolower($param), $data)) {
                $result[$param] = $data['base'][strtolower($param)];
            }
        }

        return $result;
    }

    public function calendar(array $data):array
    {
        if (array_key_exists('calendarSkuID', $data)) {
            return [CALENDAR_SKU_PARAM => $data['calendarSkuID']];
        }

        return [];
    }

    public function policy(array $data):array
    {
        $result = [];
        foreach (POLICY_SKU_PARAMS as $param) {
            if (array_key_exists($param, $data['manualDataUpdate'])) {
                $result[$param] = $data['manualDataUpdate'][$param];
            }

            if (array_key_exists($param, $data['defaultPolicyCode'])) {
                $result[$param] = $data['defaultPolicyCode'][$param];
            }

            if (array_key_exists($param, $data['policyInfo'])) {
                $result[$param] = $data['policyInfo'][$param];
            }
        }

        return $result;
    }

    public function interdependence(array $data):array
    {
        $result = [];
        foreach (INTERDEPENDENCE_PARAMS as $param) {
            if (array_key_exists($param, $data['data'])) {
                $result[$param] = $data['data'][$param];
                continue;
            }

            $result[$param] = null;
        }

        return $result;
    }

    public function suggest(array $data) : array
    {
        $result = [];

        for ($i = 0; $i <= count($data); $i++) {
            foreach (SUGGEST_TABLE_PARAMS as $param) {
                if (array_key_exists($param, $data[$i])) {
                    $result[$i][$param] = $this->prepareData($param, $data[$i][$param]);
                }
            }
        }

        return $result;
    }

    public function orderCoverSuggestions(array $data):array
    {
        $result = [];
        for ($i = 0; $i <= count($data); $i++) {
            foreach (SUGGEST_TABLE_PARAMS as $param) {
                if (array_key_exists($param, $data[$i])) {
                    $result[$i][$param] = $data[$i][$param];
                }
            }
        }
        return $result;
    }

    protected function prepareData($item, $value)
    {
        switch (strtolower($item)) {
            case 'date_arrival_altered':
                $date = new DateTime($value);
                return $date->format('d-m-y');
                break;
            case 'obs':
                $data = ($value != null) ?: '';
                return $data;
                break;
            default:
                return $value;
                break;
        }
    }
}
