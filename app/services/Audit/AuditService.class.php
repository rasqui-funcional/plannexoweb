<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 06/03/19
 * Time: 09:45
 */

class AuditService
{
    private $id = null;
    private $str;
    private $type;
    private $save;
    private $extra = [];
    private $action;
    private $creator;
    private $application;
    private $params = [];
    private $dataOld = [];
    private $dataNew = [];
    private $arrayObject = [];

    public function __construct()
    {
        $this->save = false;
        $this->creator = 'Plannexo';
    }

    public function dataOld(array $dataOld)
    {
        $this->dataOld = $dataOld;
        return $this;
    }

    public function dataNew(array $dataNew)
    {
        $this->dataNew = $dataNew;
        return $this;
    }

    public function type($type)
    {
        $this->type = $type;
        return $this;
    }

    public function application($application)
    {
        $this->application = $application;
        return $this;
    }

    public function action($action)
    {
        $this->action = $action;
        return $this;
    }

    public function id($id)
    {
        $this->id = $id;
        return $this;
    }

    public function params(array $params)
    {
        $this->params = $params;
        return $this;
    }

    public function extra($extra = "")
    {
        if (!empty($extra)) {
            foreach ($extra as $key => $value) {
                $this->extra[$key] = $value;
            }
        }

        return $this;
    }

    public function generate()
    {
        $this->arrayObjectInitialize();

        $this->hasKeyData() ?
            $this->collectRows() :
            $this->collectRow($this->dataOld);

        $this->definedSave();
        return $this;
    }

    public function save()
    {
        $result = false;
        if ($this->save) {
            $query = "INSERT INTO UAC_LOG(ID, 
                                        INSERTED_DATE, 
                                        USERNAME, 
                                        APPLICATION, 
                                        CREATOR, 
                                        IP_USER, 
                                        ACTION, 
                                        DESCRIPTION_JSON,
                                        ID_COMPANY_FK) 
                    VALUES ({$this->maxId()}, 
                    CURRENT_DATE, 
                    '{$this->params['user_id']}',
                    '{$this->application}', 
                    '{$this->creator}', 
                    '{$this->params['ip']}', 
                    '{$this->action}', 
                    '{$this->descriptionJson()}',
                    '{$this->params['id_company_pk']}' 
                    )";

            Logger::info(strtolower(__CLASS__) . '_insert_query', [$query]);

            $result = !is_null(DB::Procedure($query)) ?: true;
        }

        return $result;
    }

    public function __toString()
    {
        return $this->str . PHP_EOL . $this->save;
    }

    private function arrayObjectInitialize()
    {
        $this->arrayObject['id'] = $this->id;
        $this->arrayObject['type'] = $this->type;

        foreach ($this->extra as $key => $value) {
            $this->arrayObject[$key] = $value;
        }

        $this->arrayObject['fields'] = [];
    }

    protected function definedSave()
    {
        $this->save = count($this->arrayObject['fields']) > 0;
    }

    protected function descriptionJson()
    {
        return json_encode($this->arrayObject);
    }

    protected function mountArrayObject($label, $valueOld, $valueNew)
    {
        array_push($this->arrayObject['fields'], [
            'label' => $label,
            'old' => $valueOld,
            'new' => $valueNew
        ]);
    }

    protected function hasKeyData()
    {
        return array_key_exists('data', $this->dataOld);
    }

    protected function collectRows()
    {
        $arr = unserialize($this->dataOld['data']);
        for ($i = 0; $i <= count($arr); $i++) {
            $this->collectRow($arr[$i]);
        }
    }

    protected function collectRow($data)
    {
        foreach ($data as $key => $value) {
            $this->hasKeyData() ?
                $this->mountArrayFieldWithData($key, $value, $data) :
                $this->mountArrayField($key, $value);
        }
    }

    protected function mountArrayField($key, $value)
    {
        if ($this->isValid($key, $value)) {
            $this->mountArrayObject($key, $value, $this->dataNew[$key]);
        }
    }

    protected function mountArrayFieldWithData($key, $value, $data)
    {
        if ($this->isValidSugPk($data) && $this->isValid($key, $value)) {
            $this->mountArrayObject($key, $value, $this->dataNew[$key]);
        }
    }

    protected function isValid($key, $value)
    {
        return array_key_exists($key, $this->dataNew) && $value != $this->dataNew[$key];
    }

    protected function isValidSugPk($item)
    {
        return $item['ID_SUG_PK'] == $this->dataNew['ID_SUG_PK'];
    }

    private function maxId()
    {
        //$result = DB::Select("UAC_LOG", "MAX(NVL(ID, 0)) AS MAX", "ORDER BY ID");
        //return $result[0];
        $result = DB::Procedure("SELECT SEQ_UAC_LOG.NEXTVAL FROM DUAL", true);
        if ($result) {
            return $result[0]['NEXTVAL'];
        }

        return false;
    }
}
