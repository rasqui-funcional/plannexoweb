<?php

class InTransactionService
{
    private $lang;

    /**
     * InTransactionService constructor.
     * @param $lang
     */
    public function __construct()
    {
        global $lang;
        $this->lang = $lang;
    }

    /**
     * <b>buildIndicators</b>
     * Get the StockOverBalance data.
     * @param INT $limit ,$Offset
     * @return array
     */
    public function getTableData($post, $id_company_fk, $offset, $limit, $search, $order = '')
    {
        $columns = "COD_TRANS_PK, QTY_TRANS, DATE_TRANS";
        $table = "VW_SCR_ERP305G_TRANSACTION_IN";
        $terms = "where cod_item_fk = '{$post['skuItem']}' AND cod_estab_fk = '{$post['codEstab']}' and id_company_fk = {$id_company_fk} {$search} {$order}";

        $queryResult = DB::Page($table, $offset, $limit, $columns, $terms);
        if (!$queryResult || $queryResult == null) {
            return [
                'error' => 'not found'
            ];
        }

        $cols = array_keys($queryResult[0]);
        $titles = array();

        foreach ($cols as $key => $col) {
            array_push($titles,  $this->lang['info_sku']['erp'][$col]);
        }

        $raw = $this->select($post['skuItem'], $id_company_fk, $post['codEstab'], '');

        return [
            'result' => $queryResult,
            'titles' => $titles,
            'raw_result' => $raw['data'],
            'rows' => $this->totalInTransaction($table, $terms),
            'countEstab' => $this->countEstab($raw['data']),
            'countInfo' => $this->countInfo($raw['data'])
        ];
    }

    public function select($item, $idCompanyFk, $codEstabPk, $search, $order = '')
    {
        $where = "";
        if ($search != "") {
            $where = " and COD_TRANS_PK LIKE '%{$search}%' ";
        }

        $columns = "COD_TRANS_PK, QTY_TRANS, DATE_TRANS";
        $table = "VW_SCR_ERP305G_TRANSACTION_IN";
        $Terms = "where cod_item_fk = '{$item}' AND cod_estab_fk = '{$codEstabPk}' and id_company_fk = '{$idCompanyFk}' {$where} {$order}";
        $result = DB::Select($table, $columns, $Terms);
        return [
            'count_desc_local' => $this->lang['info_sku']['erp'],
            'titles' => $this->lang['info_sku']['erp'],
            'data' => $result,
        ];
    }

    private function countEstab($result)
    {
        $total = 0;
        foreach ($result as $row => $array) {
            foreach ($array as $column => $value) {
                if ($column == 'QTY_TRANS') {
                    $total = $total + $value;
                }
            }
        }

        return $total;
    }

    private function countInfo($result)
    {
        $totalQuantity = 0;
        foreach($result as $row => $array) {
            foreach($array as $column => $value) {
                if($column == 'QTY_TRANS') {
                    $totalQuantity = $totalQuantity + $value;
                }
            }
        }

        return [
            'QTY_TRANS' => Utilities::toFloatFormat($totalQuantity)
        ];
    }

    private function totalInTransaction($table, $terms)
    {
        return DB::SelectRow($table, 'count(*) AS total', $terms);
    }
}
