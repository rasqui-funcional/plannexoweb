<?php

// SELECT /*+FIRST_ROWS*/id_company_fk, cod_item_pk, cod_estab_pk, month_trans_pk, qty_trans FROM sigah.    vw_scr_scm419g_month_cons WHERE cod_item_pk = '40581' AND cod_estab_pk = '1' AND id_company_fk = 2

// OBS: Os campos cod_item_pk, cod_estab_pk e id_company_fk recebem variáveis de sessão

class MonthlyConsumptionService 
{
    private $lang;

    /**
     * MonthlyConsumptionService constructor.
     */
    public function __construct()
    {
        global $lang;
        $this->lang = $lang;
    }

    /**
     * <b>buildIndicators</b>
     * Get the StockOverBalance data.
     * @param INT $limit,$Offset
     * @return array
     */
	public function getTableData($post, $id_company_fk, $offset, $limit, $order = '')
    {
        $Columns = "month_trans_pk, qty_trans";
        $Table = "vw_scr_scm419g_month_cons";
        $Terms = "where cod_item_pk = '{$post['skuItem']}' AND cod_estab_pk = '{$post['codEstab']}' AND id_company_fk = {$id_company_fk} {$order}";

        $queryResult = DB::Page($Table, $offset, $limit, $Columns, $Terms);
        if(!$queryResult || $queryResult == null || empty($queryResult)) {
            return [
                'error' => $this->lang['global']['not_found']
            ];
        }

        $result = $this->cleamDate($queryResult);

        $cols= array_keys($queryResult[0]);
        $titles = array();

        foreach($cols as $key => $col){
            array_push($titles, $this->lang['info_sku']['erp'][$col]);
        }
        
        $raw = $this->select($post['skuItem'], $id_company_fk, $post['codEstab']);

        return [
            'result' => $result,
            'titles' => $titles,
            'rows' => $this->totalMonthlyConsumption($Table, $Terms),
            'raw_result' => $raw['data'],
        ];
    }

    public function select($item, $idCompanyFk, $codEstabPk, $order = 'order by month_trans_pk desc')
    {       
        $Columns = "month_trans_pk, qty_trans";
        $Table = "vw_scr_scm419g_month_cons";
        $Terms = "where cod_item_pk = '{$item}' AND cod_estab_pk = '{$codEstabPk}' AND id_company_fk = {$idCompanyFk} {$order}";
       
        $result = DB::Select($Table, $Columns, $Terms);
        $result = $this->cleamDate($result)  ;  

        return [
            'titles' => $this->lang['info_sku']['erp'],
            'data' => $result,
        ];
    }    

    private function cleamDate ($result)
    {
        $i = 0 ;
        while ($i <  count($result)) {
            foreach ($result[$i] as $key => $value) {
                if ($key == "MONTH_TRANS_PK") { 
                    $d = Utilities::dataFormatter('DATE_MONTH_YEAR', $value);
                    $t[$i]['MONTH_TRANS_PK'] = $d;
                }

                if ($key == "QTY_TRANS") {
                    $t[$i]['QTY_TRANS'] = $value;
                }          
            }

            $i ++;
        }

        return $t;
    }

    private function totalMonthlyConsumption($table, $terms)
    {
        return DB::SelectRow($table, 'count(*) AS total', $terms);
    }
}
