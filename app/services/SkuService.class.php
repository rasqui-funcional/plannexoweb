<?php
/**
 * Created by PhpStorm.
 * User: Celso Lopes
 * Date: 25/02/19
 * Time: 09:33
 * Desc: Classe responsável pela busca e tratamento de dados
 */

class SkuService
{
    private $table = 'vw_scr_scm405g_sku_list';
    private $columns = '';
    private $groupings = null;
    private $id_company_fk = null;
    private $conn;

    public function __construct($groupings)
    {
        $this->columns = DB::getTableCols($this->table);
        $this->groupings = $groupings;
        $this->id_company_fk = Login::getUserSession()['id_company_pk'];
    }

    /**
     * @param $chkvetor
     * @return string
     */
    public function prepareQueryColumns(): string
    {
        $columns = $this->getColumnsQuery();
        // Código necessário para mostrar descrição dos 12 agrupamentos (exceto o cod_group1) em vez de ID
            for($i = 2; $i <= 12; $i++) {
                $key_group_column = array_search("cod_group{$i}_fk", $columns);
                if (is_numeric($key_group_column)) {
                    $columns[$key_group_column] = "scm_group{$i}.DESC_GROUP_CONT as cod_group{$i}_fk";
                }
            }

        return $columns;
    }

    /**
     * @param $chkvetor
     * @return array
     */
    public function prepareDisplayColumns($columns)
    {
        if (!is_null($columns)) {
            return $columns;
        }

        $arr_columns = $this->transformStringToArray($this->getColumns());
        $this->unsetColumnsInactive($arr_columns);

        return $this->getColumns();
    }

    public function getDataTable($offset, $limit, $columns, $terms, $orderby = "ORDER BY sit_sku DESC, id_sku_pk")
    {
        if ($offset != 0) {
            $offset++;
            $limit--;

            $limit = $offset + $limit;
        }

        $select = "( 
            SELECT 
                $columns, 
                ROW_NUMBER() OVER($orderby) R
            FROM
                {$this->getTable()}
                $terms
        ) WHERE R BETWEEN $offset AND $limit";

        return DB::Select($select, '', '');
    }

    public function getDataTableOci($offset, $limit, $columns, $terms, $orderby = "ORDER BY sit_sku DESC, id_sku_pk")
    {
        if ($offset != 0) {
            $offset++;
            $limit--;

            $limit = $offset + $limit;
        }

        $select = "SELECT * FROM ( 
            SELECT 
                $columns, 
                ROW_NUMBER() OVER($orderby) R
            FROM
                {$this->getTable()}
                $terms
        ) WHERE R BETWEEN $offset AND $limit";
            
        // charset-iso:  WE8ISO8859P1
        $this->conn = oci_pconnect(
            getenv("DBUSUARIO"),
            getenv("DBSENHA"),
            getenv("DBSERVIDORNAME"),
            'AL32UTF8'
        );

        if (!$this->conn) {
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        $stid = oci_parse($this->conn, "{$select}");
        oci_execute($stid,OCI_DEFAULT);
        $nrows = oci_fetch_all($stid, $result, null, null, OCI_FETCHSTATEMENT_BY_ROW);

        return $result;
    }

    public function prepareData($data)
    {
        $result = [];
        foreach ($data as $key => $value) {
            $result[$key] = $this->formatData($key, $value);
        }

        return $result;
    }

    private function formatData($column, $value)
    {
        switch (strtolower($column)) {
            case 'vl_excess':
            case 'inv_total_money':
            case 'inv_available_money':
            case 'last_purchase_price':
                return Utilities::Currency($value);
                break;
            case 'qty_pp':
            case 'avg_3m':
            case 'avg_6m':
            case 'avg_12m':
            case 'inv_total':
            case 'qty_excess':
            case 'qty_pp_erp':
            case 'qty_min_out':
            case 'qty_max_out':
            case 'qty_min_erp':
            case 'qty_max_erp':
            case 'qty_next_in':
            case 'qty_last_in':
            case 'out_last_30d':
            case 'qty_last_out':
            case 'purchase_req':
            case 'inv_available':
            case 'purchase_sugg':
            case 'purchase_order':
            case 'qty_forecast':
            case 'qty_forecast_r3':
            case 'qty_min_out_ori':
            case 'qty_max_out_ori':
            case 'qty_avg_cons_erp':
            case 'last_purchase_qty':
            case 'qty_excess_next_in':
                return Utilities::Unity($value);
                break;

            case 'out_month_minus1':
            case 'out_month_minus2':
            case 'out_month_minus3':
            case 'out_month_minus4':
            case 'out_month_minus5':
            case 'out_month_minus6':
            case 'out_month_minus7':
            case 'out_month_minus8':
            case 'out_month_minus9':
            case 'out_month_minus10':
            case 'out_month_minus11':
            case 'out_month_minus12':
            case 'out_current_month':
                return Utilities::toFloatFormat($value, 2);
                break;

            case 'qty_target':
                return round($value, 0);
                break;

            case 'service_level_real':
            case 'service_level_target':
            case 'out_yesterday_num_dev':
                return number_format($value, 2, '.', '');
                break;

            default:
                return $value;
                break;
        }
    }

    private function transformStringToArray($str_columns)
    {
        $arr = explode(',', $str_columns);
        return array_map('trim', $arr);
    }

    private function transformArrayToString($arr_columns)
    {
        return implode(', ', $arr_columns);
    }

    /**
     * @return string
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param string $columns
     */
    private function setColumns($columns)
    {
        $this->columns = $columns;
    }

    private function unsetColumnsInactive($columns)
    {
        $format = 'COD_GROUP%d_FK';
        foreach ($this->groupings as $key => $value) {
            $str = str_replace('%d', $key, $format);
            if ($uid = array_search(strtolower($str), $columns)) {
                unset($columns[$uid]);
            }
        }

        $this->setColumns($columns);
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getColumnsQuery()
    {
        $cols = "
            sit_sku,
            id_sku_pk,
            ind_excess,
            cod_item_pk,
            desc_item,
            cod_estab_fk,
            val_unit_erp,
            desc_std_unit,
            QTY_MIN_ORDER,
            QTY_MULT_ORDER,
            DEFAULT_LOCAL,
            curve_abc,
            curve_pqr,
            curve_xyz,
            curve_123,
            cod_profile,
            ID_CALENDAR_FK,
            inv_available,
            qty_inv_origin,
            inv_available_money,
            inv_total,
            inv_total_money,
            qty_forecast,
            QTY_FORECAST_R3,
            INV_DAYS,
            qty_min_out,
            QTY_MIN_OUT_ORI,
            qty_pp,
            qty_target,
            qty_max_out,
            QTY_MAX_OUT_ORI,
            qty_excess,
            VL_EXCESS,
            QTY_EXCESS_NEXT_IN,
            inv_level,
            avg_3m,
            avg_6m,
            avg_12m,
            qty_min_erp,
            qty_max_erp,
            qty_pp_erp,
            qty_avg_cons_erp,
            out_last_30d,
            (NVL(out_yesterday, 0)) AS out_yesterday,
            out_yesterday_num_dev,
            out_month_minus12,
            out_month_minus11,
            out_month_minus10,
            out_month_minus9,
            out_month_minus8,
            out_month_minus7,
            out_month_minus6,
            out_month_minus5,
            out_month_minus4,
            out_month_minus3,
            out_month_minus2,
            out_month_minus1,
            out_current_month,
            ORDERS_NEXT_IN,
            REQS_NEXT_IN,
            SUG_NEXT_IN,
            date_next_in,
            qty_next_in,
            date_last_in,
            qty_last_in,
            date_last_out,
            qty_last_out,
            days_no_out,
            last_purchase_price,
            last_purchase_supplier,
            last_purchase_date,
            last_purchase_qty,
            last_parc_date,
            purchase_req,
            purchase_order,
            purchase_sugg,
            purchase_sugg_arr_date,
            LT_PLANNER_OUT,
            lt_buyer_out,
            lt_supplier_out,
            LT_RECEIVING_OUT,
            lt_others_out,
            lt_total_out,
            LT_ORIGIN_DRP,
            COD_ESTAB_ORIGIN_DRP_FK,
            TP_ORIGIN_DRP,
            out1,
            out2,
            out3,
            out4,
            out5,
            out6,
            out7,
            out8,
            out9,
            out10,
            out11,
            out12,
            service_level_real,
            service_level_target,
            cod_group1_fk,
            cod_group2_fk,
            cod_group3_fk,
            cod_group4_fk,
            cod_group5_fk,
            cod_group6_fk,
            cod_group7_fk,
            cod_group8_fk,
            cod_group9_fk,
            cod_group10_fk,
            cod_group11_fk,
            cod_group12_fk,
            sit_sku_erp,
            user_id,
            sku_life_cycle,
            date_mature,
            date_last_calc,
            date_added,
            sit_pending_calc,
            V_QTY_CONS,
            V_DATE_CONS_EXPIRATION,
            V_LT_PLANNER_IN,
            v_lt_purchase_in,
            v_lt_supplier_in,
            V_LT_RECEIVING_IN,
            V_LT_OTHERS_IN,
            v_qty_min_in,
            v_qty_max_in,
            v_qty_max_restr,
            V_CONS_PROFILE,
            V_ID_DEPEND_SKU,
            V_ORDER_MIN,
            V_ORDER_MULT,
            v_in1,
            v_in2,
            v_in3,
            v_in4,
            v_in5,
            v_in6,
            v_in7,
            v_in8,
            v_in9,
            v_in10,
            v_in11,
            v_in12,
            out_week_month_percent_avg, 
            total_out_last_month_avg, 
            total_out_last_half_month_avg, 
            total_out_last_week_avg, 
            total_out_last_week";

        return $cols;
    }

    public function avoidJoin($items)
    {
        $cod_groups = [
            'COD_GROUP1_FK',
            'COD_GROUP2_FK',
            'COD_GROUP3_FK',
            'COD_GROUP4_FK',
            'COD_GROUP5_FK',
            'COD_GROUP6_FK',
            'COD_GROUP7_FK',
            'COD_GROUP8_FK',
            'COD_GROUP9_FK',
            'COD_GROUP10_FK',
            'COD_GROUP11_FK',
            'COD_GROUP12_FK'
        ];

        foreach ($items as $key => $item) {
            /**
             * Join de supplier
             */
            $supplier = '';
            if ($item["LAST_PURCHASE_SUPPLIER"] > 0) {
                $supplier = DB::SelectRow("erp_supplier", "desc_supplier",
                    "WHERE ID_COMPANY_FK={$this->id_company_fk} AND ID_SUPPLIER_PK = '{$item["LAST_PURCHASE_SUPPLIER"]}'");
            }
            $item["LAST_PURCHASE_SUPPLIER"] = isset($supplier["DESC_SUPPLIER"]) ? $supplier["DESC_SUPPLIER"] : '';

            /**
             * Join de local
             */
            $local = DB::SelectRow(
                "erp_local",
                "cod_local_pk || '-' || desc_local AS DEFAULT_LOCAL",
                "WHERE ID_COMPANY_FK={$this->id_company_fk} AND cod_local_pk = '{$item["DEFAULT_LOCAL"]}' AND cod_estab_pk = '{$item["COD_ESTAB_FK"]}'"
            );
            $item["DEFAULT_LOCAL"] = isset($local["DEFAULT_LOCAL"]) ? $local["DEFAULT_LOCAL"] : '';

            if (array_key_exists('OBR', $item)) {
                $item['OBS'] = stream_get_contents($item['OBS']);
            }

            /**
             * Join de cod_group
             */
            foreach ($cod_groups as $key_group => $group) {
                $cache = Redis::HGet( sprintf('company:%s:group-%s', $this->id_company_fk, $key_group+1) );
                $item[$group] = isset($cache[$item[$group]]) ? $cache[$item[$group]] : '';
                $items[$key] = $item;
            }
        }

        return $items;
    }

    public function avoidJoinOci($items)
    {
        $cod_groups = array_keys(Redis::HGet("company:{$this->id_company_fk}:group:active"));

        foreach ($items as $key => $item) {
            /**
             * Join de supplier
             */
            $supplier = '';
            if ($item["LAST_PURCHASE_SUPPLIER"] > 0) {
                $select = "SELECT desc_supplier FROM erp_supplier WHERE ID_COMPANY_FK={$this->id_company_fk} AND ID_SUPPLIER_PK = '{$item["LAST_PURCHASE_SUPPLIER"]}'";
                $stid = oci_parse($this->conn, "{$select}");
                oci_execute($stid,OCI_DEFAULT);
                $supplier = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
            }
            $item["LAST_PURCHASE_SUPPLIER"] = isset($supplier["DESC_SUPPLIER"]) ? $supplier["DESC_SUPPLIER"] : '';
            /**
             * Join de local
             */
            $select = "SELECT cod_local_pk || '-' || desc_local AS DEFAULT_LOCAL FROM erp_local WHERE ID_COMPANY_FK={$this->id_company_fk} AND cod_local_pk = '{$item["DEFAULT_LOCAL"]}' AND cod_estab_pk = '{$item["COD_ESTAB_FK"]}'";
            $stid = oci_parse($this->conn, "{$select}");
            oci_execute($stid,OCI_DEFAULT);
            $local = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
            $item["DEFAULT_LOCAL"] = isset($local["DEFAULT_LOCAL"]) ? $local["DEFAULT_LOCAL"] : '';
            if (array_key_exists('OBR', $item)) {
                $item['OBS'] = stream_get_contents($item['OBS']);
            }
            /**
             * Join de cod_group
             */
            foreach ($cod_groups as $key_group => $group) {
                $cache = Redis::HGet( sprintf('company:%s:group-%s', $this->id_company_fk, $group) );
                $item["COD_GROUP{$group}_FK"] = isset($cache[$item["COD_GROUP{$group}_FK"]]) ? $cache[$item["COD_GROUP{$group}_FK"]] : '';
                $items[$key] = $item;
            }
        }
        return $items;
    }
}
