<?php

class TagService
{
    /*
    * @var int
         */
    public static $company_id_fk = 0;
    private static $table = 'UAC_TAG';

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public static function hasTag($tag = ''): bool
    {
        self::$company_id_fk = Login::getUserSessionByKey('id_company_pk');
        $terms = "WHERE ID_COMPANY_FK = " . self::$company_id_fk . " AND TAG = '{$tag}'";
        $has_tag = DB::SelectRow(self::$table, null, $terms);

        return !!$has_tag;
    }
}
