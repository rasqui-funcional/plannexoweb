<?php

/**
 * UserEstabsService
 *
 */

class UserEstabsService
{

    private $userData;
    private $table = 'UAC_USER_ESTAB';

    public function __construct($userData)
    {
        $this->userData = $userData;
    }

    /*
     * @return array
     */
    public function getEstabs() 
    {
        $columns = "CAST(COD_ESTAB_PK AS varchar2(30)) AS COD_ESTAB_PK";
        $terms = "WHERE ID_COMPANY_PK = {$this->userData['ID_COMPANY_FK']} AND ID_USER_PK = {$this->userData['ID_USER_PK']}";
        $sel_estabs = DB::Select($this->table, $columns, $terms);
        $estabs = [];

        if ($sel_estabs) {
            foreach ($sel_estabs as $estab) {
                array_push($estabs, $estab['COD_ESTAB_PK']);
            }

            return $estabs;
        }

        return $estabs;
    }

}
