<?php

class PurchaseOrderService
{
    private $lang;

    /**
     * PurchaseOrderService constructor.
     * @param $lang
     */
    public function __construct()
    {
        global $lang;
        $this->lang = $lang;
    }

    /**
     * <b>buildIndicators</b>
     * Get the StockOverBalance data.
     * @param INT $limit,$Offset
     * @return array
     */
	public function getTableData($post, $id_company_fk, $offset, $limit, $search, $order = '')
    {
        $columns = 'NUM_ORDER, ID_SUPPLIER, es.DESC_SUPPLIER, DATE_ORDER, DATE_EXP, QTY_PENDING, QTY_REC, QTY_PARC, VAL_UNIT, (QTY_PARC * VAL_UNIT) AS VAL_TOTAL';
        $table = "VW_SCR_ERP302G_PURCHASE_ORDER vwpo
                    LEFT JOIN ERP_SUPPLIER es
                    ON es.ID_SUPPLIER_PK = vwpo.ID_SUPPLIER
                    AND es.id_company_fk = vwpo.id_company_fk";
        $terms = "WHERE cod_item_pk = '{$post['skuItem']}' AND cod_estab = '{$post['codEstab']}' AND vwpo.id_company_fk = {$id_company_fk} {$search} {$order}";

        $query_result = DB::Page($table, $offset, $limit, $columns, $terms);

        if(!$query_result || $query_result == null || empty($query_result)) {
            return [
                'error' => $this->lang['global']['not_found']
            ];
        }

        for($i = 0; $i < count($query_result); $i++ ){
            if(!empty($query_result[$i]['ID_SUPPLIER'])){
                $query_result[$i]['ID_SUPPLIER'] = $query_result[$i]['ID_SUPPLIER'] ." - ". $query_result[$i]['DESC_SUPPLIER'];
                unset($query_result[$i]['DESC_SUPPLIER']);
            }
        }

        $raw = $this->select($post['skuItem'], $id_company_fk, $post['codEstab'], $offset, $order, $limit );

        return [
            'result' => $query_result,
            'raw_result' => $raw['data'],
            'rows' => $this->totalPurchaseOrder($table, $terms),
            'countInfo' => $this->countInfo($raw['data'])
        ];
    }

    private function countInfo($result)
    {
        $qty_pending = 0;
        $qty_rec = 0;
        $qty_parc = 0;
        foreach($result as $rows) {
            foreach($rows as $column => $value) {
                if($column == 'QTY_PENDING') {
                    $qty_pending = $qty_pending + $value;
                }
                if($column == 'QTY_REC') {
                    $qty_rec = $qty_rec + $value;
                }
                if($column == 'QTY_PARC') {
                    $qty_parc = $qty_parc + $value;
                }
            }
        }

        return [
            'QTY_PENDING' => Utilities::Unity($qty_pending),
            'QTY_REC' => Utilities::Unity($qty_rec),
            'QTY_PARC' => Utilities::Unity($qty_parc)
        ];
    }

    public function getSupplierInfo($id_sup, $id_company_fk, $cod_estab_pk)
    {
        $columns = "ID_SUPPLIER_PK , DESC_SUPPLIER ";
        $table = "ERP_SUPPLIER";
        $terms = "WHERE id_company_fk = {$id_company_fk} and id_supplier_pk = '{$id_sup}'";

        $querySupplierResult = DB::Select($table, $columns, $terms);
        if (!$querySupplierResult || $querySupplierResult == null) {
            return [];
        }

        return $querySupplierResult;
    }

    public function select ($item, $id_company_fk, $cod_estab_pk, $offset, $order, $limit, $search = '')
    {
        $columns = 'NUM_ORDER, ID_SUPPLIER, es.DESC_SUPPLIER, DATE_ORDER, DATE_EXP, QTY_PENDING, QTY_REC, QTY_PARC, VAL_UNIT, (QTY_PARC * VAL_UNIT) AS VAL_TOTAL';
        $table = "VW_SCR_ERP302G_PURCHASE_ORDER vwpo
                    LEFT JOIN ERP_SUPPLIER es
                    ON es.ID_SUPPLIER_PK = vwpo.ID_SUPPLIER
                    AND es.id_company_fk = vwpo.id_company_fk";
        $terms = "WHERE cod_item_pk = '{$item}' AND cod_estab = '{$cod_estab_pk}' AND vwpo.id_company_fk = {$id_company_fk} {$search} {$order}";

        $result = DB::Select($table, $columns, $terms);

        if(!$result || $result == null) {
            return [
                'titles' => $this->lang['info_sku']['erp'],
                'data' => [],
            ];
        }

        for($i = 0; $i < count($result); $i++ ){
            if(!empty($result[$i]['ID_SUPPLIER'])){
                $result[$i]['ID_SUPPLIER'] = $result[$i]['ID_SUPPLIER'] ." - ". $result[$i]['DESC_SUPPLIER'];
                unset($result[$i]['DESC_SUPPLIER']);
            }
        }

        return [
            'titles' => $this->lang['info_sku']['erp'],
            'data' => $result,
        ];
    }

    private function totalPurchaseOrder($table, $terms)
    {
        return DB::SelectRow($table, 'count(*) AS total', $terms);
    }
}
