<?php

/**
 * Class AdhesionService
 */
class AdhesionService
{
    /**
     * @var
     */
    private $company_id;
    private $table = 'his_adhesion';

    /**
     * AdhesionService constructor.
     * @param $company_id
     */
    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @param string $type M|W
     * @return array
     */
    public function getAdhesion(string $type = 'M'): array
    {

        $columns = "
            qtd_plannexo as plannexo,
            qtd_erp as erp,
            (qtd_plannexo + qtd_erp) as total, 
            CASE WHEN type = 'M' 
                THEN TO_CHAR(date_related, 'Mon/YYYY') 
                ELSE TO_CHAR(date_related-5, 'DD/MM/YYYY') 
            END as dt_related";
        $terms = "WHERE 
          ROWNUM <= 13 AND
          id_company_fk = {$this->company_id} AND 
          type = '{$type}' ORDER BY date_related";

        return DB::Select($this->table, $columns, $terms);
    }

    /**
     * @return array
     */
    public function getAdhesionTotal(): array
    {
        $columns = '
            SUM(qtd_plannexo) AS plannexo,
            SUM(qtd_plannexo + qtd_erp) as total';
        $terms = "WHERE id_company_fk = {$this->company_id} AND type = 'M'";

        return DB::SelectRow($this->table, $columns, $terms);
    }
}
