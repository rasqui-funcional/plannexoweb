<?php

/**
 * <b>ShoppingForecastServiceDaily</b>
 * Service to control all the dashboard filters;
 * @param INT $company_id
 */
class ShoppingForecastServiceDaily
{

    private $using = 0;
    private $entry = 0;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
        $this->sku_id = Request::get('skuId');
    }

    /**
     * buildChartData
     * Build the data to the chart and targets
     * @return ARRAY
     */
    public function buildChartData()
    {

        $chart_data = $this->getChartData();
        $chart_data = $this->prepareChartData($chart_data); 

        $targets_data = $this->getTargetData();
        if(!$targets_data){
            $targets_data = false;
        }

        $data['chartData'] = $chart_data;
        $data['targetsData'] =  $targets_data;

        
        return $data;
    }

    /**
     * ******************************
     * ************ PRIVATE *********
     * ******************************
     */

    private function prepareChartData($chart_data){
        $date_from = strtotime(Request::get('dateFrom'));
        $date_to = strtotime(Request::get('dateTo'));

        while($date_from <= $date_to) {
            foreach($chart_data as $key => $day_data){
                if($day_data['PERIODO'] == date("Y-m-d",$date_from)){
                    foreach($day_data as $day_field_key => $day_field_value){
                        $data[$date_from][$day_field_key] = $this->formatChartFieldData($day_field_key, $day_field_value);
                    }
                    $data[$date_from]['ESTOQUE_TOTAL'] = $this->getEstoqueTotal($day_data, $date_from);
                    break;
                }

            }

            if(!key_exists($date_from, $data)){
                $next_estoque_total = $this->getNextDateWithValues($chart_data, $date_from, $date_to);
                $data[$date_from] = $this->mockDataChart($date_from, $next_estoque_total['ESTOQUE_TOTAL']);
            }
            $date_from = strtotime('+1 day',$date_from);
        }
        return array_values($data);
    }

    private function getEstoqueTotal($data, $date_from)
    {
        $this->using += $data['PREVISAO_DE_CONSUMO']+$data['OUTRAS_SAIDAS'];
        $this->entry += $data['SOLICITACOES']+$data['ORDENS']+$data['PARCELAS'];
        return $data['ESTOQUE_TOTAL']-$this->using+$this->entry;
    }

    private function getNextDateWithValues($data, $date_from, $date_to)
    {
        while($date_from <= $date_to) {
            $periodo = array_filter($data, function($dados) use ($date_from) {
                if ($dados['PERIODO'] == date("Y-m-d", $date_from)) {
                    return $dados;
                }
            });

            if(!is_null($periodo) && !empty($periodo)) {
                return $periodo[0];
            }
            $date_from = strtotime('+1 day',$date_from);
        }
    }

    private function formatChartFieldData($field, $value)
    {
        switch($field){
            case 'PREVISAO_ESTOQUE_DISPONIVEL':
                if($value <= 0) {
                    return 0;
                }else{
                    return (float) $value;
                }
                break;
            case 'PREVISAO_DE_CONSUMO':
            case 'OUTRAS_SAIDAS':
                return $value * -1;
                break;
            case 'PERIODO':
                return date('d/m/Y', strtotime($value));
                break;
            default:
                return (float) $value;            
        }
    }


    private function getChartData()
    {
        $date_from = Request::get('dateFrom');
        $date_to = Request::get('dateTo');

        $table = '
            aux_calendar c
                LEFT JOIN vw_scr_scm463c_inv_forecast f ON
                    c.date_calendar_pk = f.dt_inv_pk
                LEFT JOIN vw_scm_aux_sku s ON
                    s.id_sku_pk = f.id_sku_fk
                    AND s.id_company_fk = f.id_company_fk
                LEFT JOIN erp_purchase_order ord ON
                    ord.ID_COMPANY_FK = f.ID_COMPANY_FK
                    AND ord.COD_ITEM = s.COD_ITEM_PK
                LEFT JOIN erp_purchase_req sug ON
                    sug.ID_COMPANY_FK = f.ID_COMPANY_FK
                    AND s.COD_ITEM_PK = sug.COD_ITEM 
                    AND sug.COD_ESTAB = s.COD_ESTAB_FK
        ';

        $columns = "
            TO_CHAR(c.DATE_CALENDAR_PK, 'YYYY-MM-DD') AS periodo,
            NVL(f.QTY_OUT_FORECAST, 0) AS previsao_de_consumo,
            NVL(f.QTY_OUT_OTHERS, 0) AS outras_saidas,
            NVL(f.QTY_IN_SUGGESTION, 0) AS parcelas,
            NVL(f.QTY_IN_REQ, 0) AS solicitacoes,
            NVL(f.QTY_IN_ORD, 0) AS ordens,                
            f.QTY_INV_FORECAST AS previsao_estoque_disponivel,
            s.INV_TOTAL AS estoque_total,
            CASE WHEN f.QTY_OUT_FORECAST > 0 THEN round(f.QTY_INV_FORECAST/f.QTY_OUT_FORECAST, 2) ELSE 0 END AS QTY_FORECAST,
            CASE WHEN f.QTY_IN_ORD > 0 THEN ORD.NUM_ORDER ELSE 0 END AS id_ordem,
            SUG.NUM_REQ AS id_sugestao,
            ROUND(f.QTY_IN_SUGGESTION  * s.VAL_UNIT_ERP, 2) AS installments_amount,
            ROUND(f.QTY_IN_ORD  * s.VAL_UNIT_ERP, 2) AS ordens_amount,
            ROUND(f.QTY_IN_REQ  * s.VAL_UNIT_ERP, 2) AS SOLICITACAO_AMOUNT,
            (CASE WHEN f.QTY_IN_REQ > 0 THEN SUG.NUM_REQ ELSE 0 END) AS ID_SOLICITACAO
        ";

        $terms = " WHERE
                f.ID_COMPANY_FK = {$this->company_id}
                AND f.ID_SKU_FK = {$this->sku_id}
                AND f.DT_INV_PK BETWEEN TO_DATE('{$date_from}', 'YYYY-MM-DD') AND TO_DATE('{$date_to}', 'YYYY-MM-DD') ORDER BY c.DATE_CALENDAR_PK
        ";

        return DB::Select($table, $columns, $terms);
    }

    private function getTargetData()
    {

        $table = 'scm_sku_out';

        $columns = '
            qty_min_out AS baixo,
            qty_max_out AS alto,
            ceil(nvl(qty_min_out,0) / decode(qty_forecast,0,1,qty_forecast)) cover_min,
            ceil(nvl(qty_max_out,0) / decode(qty_forecast,0,1,qty_forecast)) cover_max
        ';

        $terms = "
            WHERE
                id_sku_fk = {$this->sku_id}
                AND id_company_fk = {$this->company_id}
        ";

        return DB::Select($table, $columns, $terms);

    }

    private function mockDataChart(string $date, $estoque): array
    {
        return [
            'PERIODO' => date("d/m/Y", $date),
            'PREVISAO_DE_CONSUMO' => 0,
            'OUTRAS_SAIDAS' => 0,
            'PARCELAS' =>0,
            'SOLICITACOES' =>0,
            'ORDENS'=> 0,
            'PREVISAO_ESTOQUE_DISPONIVEL' => 0,
            'ESTOQUE_TOTAL' => (float) $estoque,
            'QTY_FORECAST' => 0,
            'ID_ORDEM' => 0,
            'ID_SUGESTAO' => 0,
            'INSTALLMENTS_AMOUNT' => 0,
            'ORDENS_AMOUNT' => 0,
            'SOLICITACAO_AMOUNT' => 0,
            'ID_SOLICITACAO' => 0
        ];
    }
}