<?php

/**
 * <b>StockBalanceService</b>
 * Service responsible for managing the Stock Balance Graph data
 *
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class StockBalanceService
{
    private $lang;
    private $session;

    /**
     * StockBalanceService constructor.
     * @param $lang
     */
    public function __construct()
    {
        global $lang;
        $this->lang = $lang;
        $this->session = Login::getUserSession();
    }

    private function getEstabByUser($alias = '', $field = '')
    {
        if (!empty($alias)) {
            $alias .= '.'; 
        }

        if (empty($field)) {
            $field .= 'COD_ESTAB_FK'; 
        }


        if(!empty($this->session['estab'])) {
            return " AND {$alias}{$field} IN({$this->session['estab']}) ";
        }
        return '';
    }

    /**
     * <b>getStockBalance</b>
     * Retrieve the stock balance data to fill the graph.
     *
     * @param STRING $idCompanyPk
     * @param where $where
     * @return ARRAY
     */
    public function getStockBalance($user, $where)
    {

        $is_supplier = Gate::setSupplier('', 1);
        $manager_users = Redis::Get(USERSESSIONID . ":session:dashboard:manager-users");
        $estabIn = $this->getEstabByUser();

        $table = "vw_scm_aux_sku";

        $columns = "
            SUM(inv_total * val_unit_erp) AS inv_total_money,
            SUM(inv_available * val_unit_erp) AS available_money,
            SUM(NVL(qty_forecast * val_unit_erp, 0)) AS qty_forecast_money,
            ROUND(sum(inv_total), 0) as qty_total,
            ROUND(sum(inv_available), 0) as qty_disponible
        ";

        $terms = "WHERE id_company_fk = {$user['id_company_pk']} AND sit_sku = 1 {$estabIn} {$manager_users} {$is_supplier} {$where} ";//order by a.cod_local asc";

        $result = DB::Select($table, $columns, $terms)[0];

        $result['TOTAL_COVER'] = ceil(($result['QTY_FORECAST_MONEY'] != 0 ? $result['INV_TOTAL_MONEY'] / $result['QTY_FORECAST_MONEY'] : 0));
        $result['AVAILABLE_COVER'] = ceil(($result['QTY_FORECAST_MONEY'] != 0 ? $result['AVAILABLE_MONEY'] / $result['QTY_FORECAST_MONEY'] : 0));
        $result['TOTAL_VALUE'] = $result['INV_TOTAL_MONEY'];
        $result['TOTAL_LABEL'] = Utilities::Currency($result['INV_TOTAL_MONEY']);
        $result['AVAILABLE_VALUE'] = $result['AVAILABLE_MONEY'];
        $result['AVAILABLE_LABEL'] = Utilities::Currency($result['AVAILABLE_MONEY']);
        $result['AVAILABLE_TURNOVER'] = $this->transformToTurnover($result['AVAILABLE_COVER']);
        $result['TOTAL_TURNOVER'] = $this->transformToTurnover($result['TOTAL_COVER']);

        return $result;
    }

    /**
     * <b>transformToTurnover</b>
     * Returns stock turnover off Available and Total stock
     *
     * @param INT $available_conver or $total_cover
     * @return FLOAT
     */
    public function transformToTurnover($value)
    {
        if($value >0) $value = 30/$value;
        return number_format($value, 2, ',', '.');
    }

    /**
     * <b>getStockBalanceLabelInfo</b>
     * Retrieve the unified label info for the Stock Balance Graph on SKU Info Model.
     *
     * @param STRING $idCompanyPk
     * @param where $where
     * @return ARRAY
     */
    public function getStockBalanceLabelInfo($idCompanyPk, $where)
    {
        $fields = "
		--alto
		qty_max_out as high,
		qty_max_out_val as high_value,
		--baixo
		qty_min_out as low,
		qty_min_out_val as low_value,
		--total
		inv_total as total,
		inv_total_val as total_value,
		inv_days_total as total_days,
		--disponible
		inv_available as available,
		inv_available_val as available_value,
		inv_days as available_days,
		--excess total
		qty_excess_total as total_excess,
		qty_excess_total_val as total_excess_value,
		--excess
		qty_excess as available_excess,
		qty_excess_val as available_excess_value,
		prev_ideal as ideal_prediction,
		inv_total * val_unit_erp AS inv_total_money,
		inv_available * val_unit_erp AS available_money,
		NVL((qty_forecast * val_unit_erp), 0) AS qty_forecast_money";

        $table = "vw_scm_info_sku ";

        $estabIn = $this->getEstabByUser('', 'COD_ESTAB_PK');
        $terms = " WHERE ID_COMPANY_FK = {$idCompanyPk} {$estabIn} {$where}";
        $result = DB::Select($table, $fields, $terms)[0];

        $result['TOTAL_COVER'] = ceil(($result['QTY_FORECAST_MONEY'] != 0 ? $result['INV_TOTAL_MONEY'] / $result['QTY_FORECAST_MONEY'] : 0));
        $result['AVAILABLE_COVER'] = ceil(($result['QTY_FORECAST_MONEY'] != 0 ? $result['AVAILABLE_MONEY'] / $result['QTY_FORECAST_MONEY'] : 0));
        $result['AVAILABLE_TURNOVER'] = $this->transformToTurnover((float) $result['AVAILABLE_COVER']);
        $result['TOTAL_TURNOVER'] = $this->transformToTurnover((float) $result['TOTAL_COVER']);

        $return = [
            'HIGH' => (float)$result['HIGH'],
            'HIGH_VALUE' => (float)$result['HIGH_VALUE'],
            'LOW' => (float)$result['LOW'],
            'LOW_VALUE' => (float)$result['LOW_VALUE'],
            'TOTAL' => (float)$result['TOTAL'],
            'TOTAL_VALUE' => (float)$result['TOTAL_VALUE'],
            'TOTAL_DAYS' => (float)$result['TOTAL_DAYS'],
            'AVAILABLE' => (float)$result['AVAILABLE'],
            'AVAILABLE_VALUE' => (float)$result['AVAILABLE_VALUE'],
            'AVAILABLE_DAYS' => (float)$result['AVAILABLE_DAYS'],
            'TOTAL_EXCESS' => (float)$result['TOTAL_EXCESS'],
            'TOTAL_EXCESS_VALUE' => (float)$result['TOTAL_EXCESS_VALUE'],
            'AVAILABLE_EXCESS' => (float)$result['AVAILABLE_EXCESS'],
            'AVAILABLE_EXCESS_VALUE' => (float)$result['AVAILABLE_EXCESS_VALUE'],
            'IDEAL_PREDICTION' => $result['IDEAL_PREDICTION'] < 0 ? 0 : $result['IDEAL_PREDICTION'],
            'TOTAL_COVER' => $result['TOTAL_COVER'],
            'AVAILABLE_COVER' => $result['AVAILABLE_COVER'],
            'AVAILABLE_TURNOVER' => $result['AVAILABLE_TURNOVER'],
            'TOTAL_TURNOVER' => $result['TOTAL_TURNOVER']
        ];

        return $return;
    }

    public function getTableData($post, $company_id, $offset, $limit, $order = '')
    {
        $estabIn = $this->getEstabByUser('b');

        $columns = "(a.cod_local ||' - '|| c.desc_local) as desc_local,
	                CASE WHEN (b.qty_forecast * b.val_unit_erp) = 0 THEN 0
	                ELSE NVL(ROUND((a.qty_inv * b.val_unit_erp) / (b.qty_forecast * b.val_unit_erp), 0), 0)
	                END AS days_qty_inv,
	                a.qty_inv * b.val_unit_erp AS qty_inv_val,
	                a.qty_inv";

        $table = "vw_scr_erp300g_inventory a
                  INNER JOIN vw_scm_aux_sku b
                      ON a.id_company_fk = b.id_company_fk
                      AND a.cod_item_pk = b.cod_item_pk
                      AND a.cod_estab_pk = b.cod_estab_fk
                  INNER JOIN erp_local c
                      ON a.id_company_fk = c.id_company_fk
                      AND a.cod_local = c.cod_local_pk
                      AND a.cod_estab_pk = c.cod_estab_pk";

        $where = "WHERE a.cod_item_pk = '{$post['skuItem']}' AND a.cod_estab_pk = '{$post['codEstab']}'
                  AND a.id_company_fk = {$company_id} {$estabIn}";

        $terms = $where . $order;
        $queryResult = DB::Page($table, $offset, $limit, $columns, $terms);
        if (!$queryResult || $queryResult == null) {
            return [
                'error' => 'not found'
            ];
        }

        $cols = array_keys($queryResult[0]);
        $titles = array();

        foreach ($cols as $key => $col) {
            array_push($titles, $this->lang['info_sku']['erp'][$col]);
        }

        $raw = $this->select($post['skuItem'], $company_id, $post['codEstab']);
        return [
            'result' => $queryResult,
            'titles' => $titles,
            'raw_result' => $raw['data'],
            'rows' => $this->totalStockBalance($table, $where),
            'countEstab' => $this->countEstab($raw['data']),
            'countInfo' => $this->countInfo($raw['data'])
        ];
    }

    public function select($item, $idCompanyFk, $codEstabPk, $order = '')
    {
        $estabIn = $this->getEstabByUser('b');
        $query = "SELECT (a.cod_local ||' - '|| c.desc_local) as desc_local,
                     CASE WHEN ( b.qty_forecast * b.val_unit_erp ) = 0 THEN 0
                     ELSE nvl(round( (a.qty_inv * b.val_unit_erp) / (b.qty_forecast * b.val_unit_erp),0),0)
                     END AS days_qty_inv,
                     a.qty_inv * b.val_unit_erp AS qty_inv_val,
                     a.qty_inv
                 FROM vw_scr_erp300g_inventory a
                 INNER JOIN vw_scm_aux_sku b
                     ON a.id_company_fk = b.id_company_fk
                     AND a.cod_item_pk = b.cod_item_pk
                     AND a.cod_estab_pk = b.cod_estab_fk
                 INNER JOIN erp_local c
                     ON a.id_company_fk=c.id_company_fk
                     AND a.cod_local=c.cod_local_pk
                     AND a.cod_estab_pk=c.cod_estab_pk
                 WHERE a.cod_item_pk = '{$item}'
                     AND a.cod_estab_pk = '{$codEstabPk}'
                     AND a.id_company_fk = {$idCompanyFk} {$estabIn} {$order}";

        $result = DB::Procedure($query, true);
        return [
            'count_desc_local' => $this->lang,
            'titles' => $this->lang['info_sku']['erp'],
            'data' => $result,
        ];
    }

    private function countEstab($result)
    {
        $total = 0;
        foreach ($result as $row => $array) {
            foreach ($array as $column => $value) {
                if ($column == 'COD_LOCAL') {
                    $total = $total + $value;
                }
            }
        }

        return $total;
    }

    private function countInfo($result)
    {
        $total = [
            'COD_LOCAL' => 0,
            'QTY_INV' => 0
        ];
        foreach ($result as $row => $array) {
            foreach ($array as $column => $value) {
                if ($column == 'COD_LOCAL') {
                    $total['COD_LOCAL'] = $total['COD_LOCAL'] + $value;
                }

                if ($column == 'QTY_INV') {
                    $total['QTY_INV'] = $total['QTY_INV'] + $value;
                }
            }
        }

        $total['QTY_INV'] = Utilities::toFloatFormat($total['QTY_INV']);

        return $total;
    }

    /**
     * @param integer $id_company
     * @param string $where
     * @return array
     */
    public function detailStockBalancing($id_company, $where) : array
    {
        $estabIn = $this->getEstabByUser('a');
        $columns = 'g.inv_level AS inv_level,
                    nvl(round(SUM(g.qty), 0), 0) AS saldo,
                    ceil(CASE
                        WHEN SUM(g.qty_forecast) = 0 THEN 0
                        ELSE SUM(g.qty) / SUM(g.qty_forecast)
                    END) AS cobertura';

        $table = 'vw_scr_scm456g_gauge_grid g
                    INNER JOIN
                        VW_SCM_AUX_SKU a
                    ON a.id_company_fk = g.id_company_fk
                        AND a.id_sku_pk = g.id_sku_fk
                        AND sit_sku = 1';

        $terms = "WHERE g.id_company_fk = {$id_company} {$where}
                    AND g.inv_level in(2,4)
                    {$estabIn}
                    GROUP BY g.inv_level, g.id_company_fk
                    ORDER BY inv_level ASC";

        $query_result = DB::Select($table, $columns, $terms);

        return $this->buildDetailStockBalancingResponse($query_result);
    }

    /**
     * @param array $data
     * @return array
     */
    private function buildDetailStockBalancingResponse($data)
    {
        return [
            'minimum' => [
                'INV_LEVEL' => $data[0]['INV_LEVEL'] ?? 0,
                'SALDO' => $data[0]['SALDO'] ?? 0,
                'COBERTURA' => $data[0]['COBERTURA'] ?? 0,
            ],
            'maximum' => [
                'INV_LEVEL' => $data[1]['INV_LEVEL'] ?? 0,
                'SALDO' => $data[1]['SALDO'] ?? 0,
                'COBERTURA' => $data[1]['COBERTURA'] ?? 0,
            ]
        ];
    }

    private function totalStockBalance($table, $terms)
    {
        return DB::SelectRow($table, 'count(*) AS total', $terms);
    }
}
