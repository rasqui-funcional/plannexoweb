<?php

/**
 * <b>DeviationConsumptionService</b>
 * Service to get data for deviation of comsumption and your errors.
 * @param INT $companyId
 */
class DeviationConsumptionService
{

    public function __construct($companyId)
    {
        $this->companyId = $companyId;
        $this->skuId = (int) Request::get('skuId');
    }

    /**
     * buildMarginDeviation
     * Build the data to the margin max, min and deviation.
     * @return ARRAY
     */
    public function buildMarginDeviation()
    {   
        $dataMarginDeviation = $this->getMarginDeviation();
        if(!$dataMarginDeviation){
            $dataMarginDeviation = $this->mockNullMarginData();
        }else{
            $dataMarginDeviation = $this->prepareMarginData($dataMarginDeviation[0]);
        }

        return $dataMarginDeviation;
    }

    /**
     * buildChartData
     * Build the data to the chart and targets
     * @return ARRAY
     */
    public function buildChartData()
    {
        $chartData = $this->getChartData();
        if(!$chartData){
            $chartData = $this->mockNullChartData();
        }else{
            $chartData = $this->prepareChartData($chartData);
        }  

        return $chartData;       
    }

    /**
     * ******************************
     * ************ PRIVATE *********
     * ******************************
     */

    private function prepareChartData($chartData)
    {
        foreach($chartData as $key => $dayData){
            foreach($dayData as $dayFieldKey => $dayFieldValue){
                $data[$key][] = $this->formatFieldData($dayFieldKey, $dayFieldValue);
            }
        }

        return $data;
    }   

    private function prepareMarginData($marginData)
    {
        foreach($marginData as $key => $value){
            $data[] = $this->formatFieldData($key, $value);
        }

        return $data;
    }

    private function formatFieldData($field, $value)
    {       
        switch($field){
            case 'DATE_HIST':
                return date('d/m/Y', strtotime($value));
                break;
            default:
                return (float) $value;            
        }
    }

    public function mockNullMarginData(){
        return [0,0,0,0];
    }

    public function mockNullChartData(){
        $data = [0,0];

        for($i = 1; $i <= 7; $i++){
            $mock[] = $data;
        }

        return $mock;
    }

    private function getChartData()
    {
        $columns = "TO_CHAR(date_calendar_pk, 'DD-MM-YYYY') as date_hist,
                    CASE
                        WHEN NVL(saida, 0) < 0 THEN NVL(saida, 0) * (-1 )
                        ELSE NVL(saida, 0)
                    END AS saida";
        $table = "aux_calendar
                    LEFT JOIN (
                        SELECT
                            b.date_trans_pk,
                            SUM(b.qty_trans) AS saida
                        FROM
                            vw_scm_aux_sku a
                        INNER JOIN vw_scm_transaction_out_day b ON
                            a.id_company_fk = b.id_company_fk
                            AND a.cod_item_pk = b.cod_item_fk
                            AND a.cod_estab_fk = b.cod_estab_fk
                        WHERE
                            a.id_sku_pk = {$this->skuId}
                            AND b.id_company_fk = {$this->companyId}
                            AND b.yn_cons = 'Y'
                        GROUP BY
                            b.date_trans_pk
                        ORDER BY
                            b.date_trans_pk ) ON
                        date_trans_pk = date_calendar_pk";
        $terms = "WHERE
                    date_calendar_pk BETWEEN TRUNC(SYSDATE - ( SELECT DEV_PERIOD FROM SCM_CONFIG WHERE ID_COMPANY_FK = 2 --colocar company
                ), 'DD') AND TRUNC(SYSDATE - 1, 'DD')
                ORDER BY
                    date_calendar_pk";

        return DB::Select($table, $columns, $terms);
    }

    private function getMarginDeviation(){
        $Query = "SELECT
                        average,
                        --media
                    deviation,
                        --desviacion
                    average + ( dev_limit * deviation ) AS limit_max,
                        CASE
                            WHEN ( average - ( dev_limit * deviation ) ) < 0 THEN 0
                            ELSE ( average - ( dev_limit * deviation ) )
                        END AS limit_min
                    FROM
                        (
                        SELECT
                            a.average,
                            a.deviation,
                            CASE
                                WHEN b.dev_limit < 0 THEN b.dev_limit * (-1 )
                                ELSE b.dev_limit
                            END AS dev_limit
                        FROM
                            (
                            SELECT
                                CASE
                                    WHEN CEIL(AVG(saida) ) < 0 THEN CEIL(AVG(saida) ) * (-1 )
                                    ELSE CEIL(AVG(saida) )
                                END AS average,
                                CASE
                                    WHEN CEIL(STDDEV(saida) ) < 0 THEN CEIL(STDDEV(saida) ) * (-1 )
                                    ELSE CEIL(STDDEV(saida) )
                                END AS deviation,
                                id_company_fk
                            FROM
                                (
                                SELECT
                                    b.date_trans_pk,
                                    NVL(SUM(b.qty_trans), 0) AS saida,
                                    b.id_company_fk
                                FROM
                                    vw_scm_aux_sku a
                                INNER JOIN vw_scm_transaction_out_day b ON
                                    a.id_company_fk = b.id_company_fk
                                    AND a.cod_item_pk = b.cod_item_fk
                                    AND a.cod_estab_fk = b.cod_estab_fk
                                WHERE
                                    a.id_sku_pk = {$this->skuId}
                                    --colocar sku
                                    AND b.id_company_fk = {$this->companyId}
                                    --colocar company
                                    AND b.yn_cons = 'Y'
                                    AND b.date_trans_pk BETWEEN TRUNC(SYSDATE - ( SELECT dev_period FROM scm_config WHERE id_company_fk = {$this->companyId} --colocar company
                    ) - 1, 'DD') AND TRUNC(SYSDATE - 1, 'DD')
                                GROUP BY
                                    b.date_trans_pk,
                                    b.id_company_fk
                                ORDER BY
                                    b.date_trans_pk ) graph
                            GROUP BY
                                id_company_fk ) a
                        INNER JOIN scm_config b ON
                            a.id_company_fk = b.id_company_fk )";
        
        return DB::Procedure($Query, true);
    }

    

}