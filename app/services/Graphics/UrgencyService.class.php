<?php

/**
 * Class UrgencyService
 */
class UrgencyService
{
    /**
     * @var
     */
    private $session;
    /**
     * @var
     */
    private $company_id;
    /**
     * @var string
     */
    /**
     * UrgencyService constructor.
     * @param $company_id
     */
    public function __construct($user)
    {
        $this->company_id = $user['id_company_pk'];
        $this->user = $user['user_id'];
        $this->session = $user;
    }

    /**
     * @param string $type MM(Monthly)|IW (Weekly)
     * @return array
     */
    public function getUrgency(string $type = 'MM', int $interval = 365): array
    {
        $manager_users = Redis::Get(USERSESSIONID . ':session:dashboard:manager-users');
        return DB::Procedure($this->urgencyQuery($type, $interval, $manager_users), true);
    }


    /**
     * @param string $type MM(Monthly)|IW (Weekly)
     * @param int $interval
     * @return string
     */
    private function urgencyQuery(string $type = 'MM', int $interval = 365): string
    {
        $inEstabs = '';
        if(!empty($this->session['estab'])) {
            $inEstabs = " a.COD_ESTAB_FK IN({$this->session['estab']}) AND ";
        }

        $where = $this->buildPrefixColumn(Redis::Get(USERSESSIONID . ":session:dashboard:filters-query"), 'a.');
        $is_supplier = Gate::setSupplier('', 1);
        $manager_users = $this->buildPrefixColumn(Redis::Get(USERSESSIONID . ':session:dashboard:manager-users'), 'a.');

        return "
            SELECT
                CASE
                    WHEN '{$type}' = 'MM' THEN TO_CHAR(date_calendar, 'Mon/YYYY')
                    ELSE TO_CHAR(date_calendar-3, 'DD/MM/YYYY')
                END AS dt_related,
                total,
                total_urgent,
                CASE WHEN total > 0 THEN ROUND(total_urgent * 100 / total, 2) ELSE 0 END AS perc_total_urgent,
                total_urgent_lt,
                CASE WHEN total > 0 THEN ROUND(total_urgent_lt * 100 / total, 2) ELSE 0 END AS perc_total_urgent_lt
            FROM
                (
                SELECT
                    TRUNC (date_integration, '{$type}') date_calendar,
                    ROUND (SUM (qty_altered * val_unit_erp), 2) total,
                    SUM (DECODE (yn_urgent,'Y', qty_altered * val_unit_erp, 0)) total_urgent,
                    SUM(CASE WHEN date_arrival_altered > (TRUNC (date_integration, 'DD') + DECODE (v_lt_planner_in,-1, lt_planner, v_lt_planner_in) + DECODE (v_lt_purchase_in,-1, lt_buyer, v_lt_purchase_in) + DECODE (v_lt_supplier_in,-1, lt_supplier, v_lt_supplier_in) + DECODE (v_lt_receiving_in,-1, lt_receiving, v_lt_receiving_in) + DECODE (v_lt_others_in,-1, lt_others, v_lt_others_in)) THEN qty_altered * val_unit_erp ELSE 0 END) total_urgent_lt
                FROM
                    vw_scm_aux_sku a
                INNER JOIN scm_sku_purchase_sug_hist b ON
                    a.id_company_fk = b.id_company_fk AND 
                    b.id_sku_fk = a.id_sku_pk
                INNER JOIN scm_sku_purchase_sug_header c ON
                    b.id_company_fk = c.id_company_fk AND
                    id_integration_gtp = cod_header_pk
                WHERE
                    sit_psug = 2 AND
                    {$inEstabs}
                    a.id_company_fk = {$this->company_id} AND
                    (date_integration BETWEEN ( CURRENT_DATE - {$interval} ) AND CURRENT_DATE)
                    $manager_users
                    $is_supplier
                    $where
                GROUP BY
                    TRUNC (date_integration, '{$type}'))
            ORDER BY
                date_calendar
        ";
    }

    private function buildPrefixColumn($terms, $prefix)
    {
        return  str_replace('AND ', "AND {$prefix}", $terms);
    }
}
