<?php

/**
 * <b>ShoppingForecastServiceMonthly</b>
 * Service to control all the dashboard filters;
 * @param INT $companyId
 */
class ShoppingForecastServiceMonthly
{
    private $chartData;
    private $methodName;

    public function __construct($companyId)
    {
        $this->companyId = $companyId;
        $this->skuId = (int) Request::get('skuId');
    }
    
    /**
     * buildChartData
     * Build the data to the chart and targets
     * @return ARRAY
     */
    public function buildChartData()
    {
        $this->chartData = $this->getChartData();
        $this->addAutmaticAndManualForecast();
        if(!$this->skuId || !$this->chartData){
            $this->chartData = false;
        }else{
            $this->chartData = $this->prepareChartData($this->chartData);
        } 

        $Data['chartData'] = $this->chartData; 
        $Data['method_name'] = $this->methodName; 
        $Data['labels'] = $this->prepareLabels(); 

        return $Data;
    }

    /**
     * ******************************
     * ************ PRIVATE *********
     * ******************************
     */

    private function mockNullChartData(){
        $data = [
            "PERIOD" => 0,
            "YEAR3" => 0,
            "YEAR2" => 0,
            "YEAR1" => 0,
            "MONTH_FORECAST" => 0
        ];

        for($i = 1; $i <= 12; $i++){
            $mock[] = $data;
        }

        return $mock;
    }

    private function prepareChartData($chartData){
        
        foreach($chartData as $key => $dayData){
            foreach($dayData as $dayFieldKey => $dayFieldValue){
                $data[$key][] = $this->formatChartFieldData($dayFieldKey, $dayFieldValue);
            }
        }

        return $data;

    }

    private function formatChartFieldData($field, $value)
    {
        switch($field){
            case 'PERIOD':
                return date('m', strtotime($value)) - 1;
                break;
            default:
                return (float) $value;            
        }
    }


    private function getChartData()
    {   
      
        $Table = "vw_scr_scm445c_sku_forecast";
        $Columns = "TO_CHAR(date_calendar_pk, 'YYYY-MM-DD') as period,
                    year3,
                    year2,
                    year1";
        $Terms = "WHERE
                    id_company_fk = {$this->companyId}
                    AND id_sku_fk = {$this->skuId}
                    AND rownum <= 12
                ORDER BY 1";

        return DB::Select($Table, $Columns, $Terms);
    }

    private function prepareLabels(){

        /**
         * We're subtracting 1 in the months to adapt to Google Charts
         */
        $monthFrom = date('m') - 1; 
        $monthTo = date('m', strtotime('+11 months', time())) - 1;
        $year = date('Y');

        for($i = 3; $i >= 0; $i--){
            $Labels[] = [
                'from' => [$monthFrom, $year - $i],
                'to' => [$monthTo, $year - $i + 1]
            ];
        }

        return $Labels;
    }

    private function addAutmaticAndManualForecast()
    {
        $table = "scm_sku_month_forecast f INNER JOIN scm_sku s
                    ON f.id_company_fk = s.id_company_fk
                    AND f.id_sku_fk = s.id_sku_pk
                INNER JOIN
                    scm_sku_in si
                ON f.id_company_fk = si.id_company_fk AND f.id_sku_fk = si.id_sku_fk ";
        
        $columns = "
            f.id_company_fk,
            f.id_sku_fk,
            f.date_calendar_pk,
            f.month_forecast,
            f.month_forecast_method_name,
            (CASE
                WHEN si.v_qty_cons <> -1
                THEN
                (CASE
                    WHEN (si.v_date_cons_expiration > f.date_calendar_pk)
                    THEN
                        si.v_qty_cons
                    END)
            END)
            AS month_forecast_manual
        ";

        $terms = " WHERE f.id_company_fk = $this->companyId AND f.id_sku_fk = $this->skuId AND rownum <= 12 ORDER BY f.date_calendar_pk";

        $result = DB::Select($table, $columns, $terms);

        $this->methodName = $result[0]['MONTH_FORECAST_METHOD_NAME'];

        foreach ($result as $key => $row) {
            $this->chartData[$key]['MONTH_FORECAST_MANUAL'] = $row['MONTH_FORECAST_MANUAL'];
            $this->chartData[$key]['MONTH_FORECAST'] = $row['MONTH_FORECAST'];
        }

        return;

    }

}