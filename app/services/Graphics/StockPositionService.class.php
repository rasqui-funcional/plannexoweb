<?php

/**
 * <b>StockPosition.class:</b>
 * Service responsible for the Stock Position Chart;
 *
 */
class StockPositionService
{
    public function mockNullData(){
        $data = [
            "DATE_HIST" => 0,
            "SERVICE_LEVEL" => 0,
            "COVER" => 0,
            "EXCESS" => 0,
            "AVAILABLE_ITEMS" => 0,
            "AVAILABLE_ITEMS_VALUE" => 0
        ];

        for($i = 1; $i <= 12; $i++){
            $mock['stockposA'][] = $data;
            $mock['stockposT'][] = $data; 
        }

        return $mock;
    }
    
    /**
     * <b>getStock</b>
     * Retrieve data for the Stock Position Chart, based on given requirements.
     * @param periodArray
     * @param user
     * @param where
     * @param period
     * @return array
     */
    public function getStock($user, $where, $period='annual')
    {
        $result = $this->prepareQueryStock($period, $user, $where);        
        $result = $this->translateResult($result, $period);

        return $result;
    }

    /**
     * <b>translateResult</b>
     * Calls the translate method depending on the period.
     * @param periodArray
     * @param user
     * @param where
     * @return string
     */
    private function translateResult($result, $period)
    {
        switch($period){
            case 'annual': 
                $result = $this->translateResultAnnual($result);
                break;
            case 'weekly':
                $result;
                break;
            default:
                $result = $this->translateResultAnnual($result);
        }

        return $result;
    }

    /**
     * <b>translateResultAnnual</b>
     * Translate the result used by the getStock method.
     * @param result
     * @return array
     */
    private function translateResultAnnual($result)
    {
        foreach ($result as $key => $value) {
            $value['DATE_HIST'] = str_replace('/', '-', $value['DATE_HIST']);
            $date = date('m-Y',  strtotime($value['DATE_HIST']));
            $date_exploded = explode("-", $date);
            $month = $date_exploded[0];
            $year = $date_exploded[1];
            $month_name_portuguese_cursive = Date::formatMonth($month);
            $firstThreeCharactersMonth = Date::firstThreeCharactersMonth($month_name_portuguese_cursive);
            $result[$key]['DATE_HIST'] = $firstThreeCharactersMonth . '/' . $year;
        }

        return $result;
    }

    /**
     * <b>translateResultWeekly</b>
     * Translate the result used by the getStock method.
     * @param result
     * @return array
     */
    private function translateResultWeekly($result)
    {
        foreach ($result as $key => $value) {
            $result[$key]['DATE_HIST'] = date('d/m/Y', strtotime("{$value['DATE_HIST']}"));
        }

        return $result;
    }

    /**
     * <b>getStock</b>
     * Get the stock for a certain period.
     * @param periodArray
     * @param user
     * @param where
     * @return string
     */
    private function getStockByMonth($period, $user, $where){
        $is_supplier = Gate::setSupplier('', 1);
        $date_label = 'YYYY/MM/DD';
        $table = 'vw_scm_dashboard_stock_pos_w';
        $cover = 'ceil(avg(inv_total_days)) AS cover,';
        $db_period = 'IW';
        $calendar_where = " date_calendar_pk BETWEEN ( current_date - 91 ) AND current_date
            and to_char(date_calendar_pk, 'd') = to_char(current_date, 'd')";
            $fields = "to_char(date_trans-3, '$date_label') date_hist,";
        if($period != 'weekly') {
            $date_label = 'MM/YYYY';
            $table = 'vw_scm_dashboard_stock_pos';
            $cover = 'ceil(avg(inv_total_days)) AS cover,';
            $db_period = 'MM';
            $calendar_where = " date_calendar_pk BETWEEN ( current_date - 365 ) AND current_date";
            $fields = "TO_CHAR(date_trans-1, '$date_label') date_hist, ";
        }

        $fields .= "
            ROUND((SUM(total) - SUM(zeros)) * 100 / SUM(total),2) service_level,
            {$cover}
            ROUND(SUM(excess_val), 2) AS excess,
            SUM(item_disponible) AS available_items,
            ROUND(SUM(valor_disponible), 2) AS available_items_value";        

        $manager_users = Redis::Get(USERSESSIONID . ':session:dashboard:manager-users');
        
        $inEstabs = '';
        if(!empty($user['estab'])) {
            $inEstabs = " AND COD_ESTAB_FK IN({$user['estab']}) ";
        }

        $terms = "WHERE date_trans >= (
                SELECT DISTINCT
                    MIN(trunc(date_calendar_pk,'{$db_period}')) as dt_query
                FROM
                    aux_calendar
                WHERE
                    {$calendar_where}
            )
            AND id_company_fk = {$user['id_company_pk']} {$inEstabs} {$manager_users} {$is_supplier}"
            . $where
            . " GROUP BY date_trans ORDER BY to_char(date_trans, 'YYYYMMDD')";
        return DB::Select($table, $fields, $terms);
    }

    /**
     * <b>getStock</b>
     * Prepare the Query used by the getStock method.
     * @param periodArray
     * @param user
     * @param where
     * @return string
     */
    private function prepareQueryStock($period, $user, $where)
    {
        $arrayStock = [];
        $rowStock = $this->getStockByMonth($period, $user, $where);
        $arrayStockOrdered = $this->orderStockMonths($rowStock);
        $arrayStockOrdered = $this->addMonths($period, $arrayStockOrdered);
        return $arrayStockOrdered;
    }

    private function addMonths($period, $stocks)
    {
        if(count($stocks) == 13 || ($period != 'annual' && count($stocks) == 13)) {
            return $stocks;
        }

        $start_period =  date('Y-m-d', strtotime('next friday -13 week'));
        $interval_period = '1 week';
        $now = 'last friday';

        if($period != 'weekly') {
            $start_period = '-1 year';
            $interval_period = '1 month';
            $now = 'now';
        }
    
        $end = new \DateTime($now);
        $start = new \DateTime($now);
        $start = $start->modify($start_period);

        $interval = DateInterval::createFromDateString($interval_period);
        $intervals = new DatePeriod($start, $interval, $end);
        $stock_arr = [];
        
        foreach($intervals as $interval) {            
            $interval_format = $interval->format('d/m/Y');
            if($period != 'weekly') {
                $interval_format = '01/'.$interval->format('m/Y');
            }
            
            $date_arr = array_filter($stocks, function($k) use ($interval_format) {
                return $k['DATE_HIST'] == $interval_format;
            }, ARRAY_FILTER_USE_BOTH);
                
            if(!empty($date_arr)) {
                foreach($date_arr as $dt_arr) {
                    $date_arr = $dt_arr;
                }
                $stock_arr[] = $date_arr;

            } else {
                $stock_arr[] = [
                    'DATE_HIST' => $interval_format,
                    'SERVICE_LEVEL' => '0',
                    'COVER' => '0',
                    'EXCESS' => '0',
                    'AVAILABLE_ITEMS' => '0',
                    'AVAILABLE_ITEMS_VALUE' => '0'
                ];
            }
        }

        return $stock_arr;
    }

    private function orderStockMonths($arrayStock)
    {
        foreach($arrayStock as $key => $data) {
            if(strlen($arrayStock[$key]['DATE_HIST']) == 7){
                $date = '01-' . str_replace('/', '-', $data['DATE_HIST']);
                $date = date('d-m-Y', strtotime($date));
            } else {
                $date = date('d-m-Y', strtotime($data['DATE_HIST']));
            }
            $arrayStock[$key]['DATE_HIST'] = (string)date('d/m/Y', strtotime("{$date}"));
        }

        return $arrayStock;
    }


}
