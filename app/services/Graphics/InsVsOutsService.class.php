<?php

/**
 * <b>InsVsOutsService.class:</b>
 * Service to control all the dashboard filters;
 *
 */
class InsVsOutsService
{
    private $mes = [];

    public function __construct()
    {
        global $lang;
        $this->mes = [
            1 => $lang['months']['ustobr']['Jan'],
            2 => $lang['months']['ustobr']['Feb'],
            3 => $lang['months']['ustobr']['Mar'],
            4 => $lang['months']['ustobr']['Apr'],
            5 => $lang['months']['ustobr']['May'],
            6 => $lang['months']['ustobr']['Jun'],
            7 => $lang['months']['ustobr']['Jul'],
            8 => $lang['months']['ustobr']['Aug'],
            9 => $lang['months']['ustobr']['Sep'],
            10 => $lang['months']['ustobr']['Oct'],
            11 => $lang['months']['ustobr']['Nov'],
            12 => $lang['months']['ustobr']['Dec']
        ];
    }

    /**
     * <b>buildInsVsOuts</b>
     * build inserts versus outputs.
     * @param INT $company_id
     * @return array
     */
    public function buildInsVsOuts($user, $manager_users)
    {
        $is_supplier = Gate::setSupplier('a.', 1);
        $where = Redis::Get(USERSESSIONID . ":session:dashboard:filters-query");
        $estabsIn = '';
        if(!empty($user['estab'])) {
            $estabsIn = " AND COD_ESTAB_FK IN({$user['estab']}) ";
        }

        $query = "SELECT 
                TO_CHAR(c.period_date, 'DD-MM-YYYY') period_date,                
                nvl(m.estoque,0) estoque,
                nvl(m.entrada,0) entrada,
                nvl(m.saida,0) saida
            FROM(
            SELECT DISTINCT
                trunc(cal.date_calendar_pk, 'MM') AS period_date
            FROM aux_calendar cal
            WHERE 
                cal.date_calendar_pk BETWEEN current_date - 365 AND trunc(current_date, 'MM') 
            ) c 
            LEFT JOIN (
                SELECT
                    m.date_trans,
                    sum(m.qty_inv_total*m.val_unit) estoque,
                    sum(m.qty_in*m.val_unit) entrada,
                    sum(m.qty_out*m.val_unit) saida
                FROM vw_scm_aux_sku a
                INNER JOIN scm_sku_month_by_month m
                    ON m.id_company_fk = a.id_company_fk
                    AND m.id_sku_fk = a.id_sku_pk
                WHERE
                    a.sit_sku = 1       
                    and a.id_company_fk = {$user['id_company_pk']}
                    $manager_users
                    $is_supplier
                    $where
                    {$estabsIn}
                    --Filters
                GROUP BY
                    m.date_trans
            )m 
                ON c.period_date = m.date_trans
            ORDER BY c.period_date asc";

        $resultSet = DB::procedure($query, true);
        if ($resultSet) {
            return $this->prepareData($resultSet);
        }

        return $this->mockData();
    }

    /**
     * <b>buildInsVsOutsPerWeek</b>
     * build inserts versus outputs.
     * @param INT $company_id
     * @return array
     */
    public function buildInsVsOutsPerWeek($user, $manager_users)
    {
        $is_supplier = Gate::setSupplier('a.', 1);
        $where = Redis::Get(USERSESSIONID . ":session:dashboard:filters-query");

        $query = "SELECT 
                TO_CHAR(c.period_date-3, 'DD/MM/YYYY') period_date_week,                
                nvl(m.estoque,0) estoque,
                nvl(m.entrada,0) entrada,
                nvl(m.saida,0) saida
            FROM(
            SELECT DISTINCT
                trunc(cal.date_calendar_pk, 'IW') AS period_date
            FROM aux_calendar cal
            WHERE 
                cal.date_calendar_pk BETWEEN current_date - 84 AND trunc(current_date, 'IW')
            ) c 
            LEFT JOIN (
                SELECT
                    m.date_trans,
                    sum(m.qty_inv_total*m.val_unit) estoque,
                    sum(m.qty_in*m.val_unit) entrada,
                    sum(m.qty_out*m.val_unit) saida
                FROM vw_scm_aux_sku a
                INNER JOIN scm_sku_week_by_week m
                    ON m.id_company_fk = a.id_company_fk
                    AND m.id_sku_fk = a.id_sku_pk
                WHERE
                    a.sit_sku = 1       
                    and a.id_company_fk = {$user['id_company_pk']}
                    $manager_users
                    $is_supplier
                    $where
                    --Filters
                GROUP BY
                    m.date_trans
            )m 
                ON c.period_date = m.date_trans
            ORDER BY c.period_date asc";

        $resultSet = DB::procedure($query, true);
        if ($resultSet) {
            return $this->prepareData($resultSet);
        }

        return $this->mockData();
    }

    private function prepareData($query_result)
    {
        $data = array();
        foreach ($query_result as $row => $rowdata) {
            foreach ($rowdata as $col => $value) {
                $data[$row][$col] = $this->formatData($col, $value);
            }
        }

        return $data;
    }

    private function formatData($col, $value)
    {   
        switch (strtolower($col)) {
            case 'period_date':
                $timestamp = strtotime($value);
                $mes = (int) date('m', $timestamp);
                $ano = date('Y', $timestamp);
                return "{$this->mes[$mes]}/{$ano}";
                break;
            default:
                return $value;
                break;
        }
    }

    public function mockData()
    {
        return [
            [
                'PERIOD_DATE' => null,
                'ESTOQUE' => 0,
                'ENTRADA' => 0,
                'SAIDA' => 0
            ]
        ];
    }
}
