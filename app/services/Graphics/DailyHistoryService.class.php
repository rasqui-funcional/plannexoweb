<?php

/**
 * <b>DailyHistoryService</b>
 * Service to control all the dashboard filters;
 * @param INT $company_id
 */
class DailyHistoryService
{
    public function __construct($company_id, $lang)
    {
        $this->company_id = $company_id;
        $this->sku_id = Request::get('skuId');
        $this->date_from = Request::get('dateFrom');
        $this->date_to = Request::get('dateTo');
        $this->lang = $lang;
    }

    /**
     * buildChartData
     * Build the data to the chart and statistic
     * @return ARRAY
     */
    public function buildData()
    {
        $data_chart = $this->getChartData();

        $data_statistics = $this->getStatisticsData();

        $suggestion_by_day = $this->getDataHist('DATE_APPROVED');

        $data_open = $this->getDataHist('DATE_CREATED');

        $data_approved = $this->getDataApproved();

        $chart = $this->prepareData($data_chart, $data_statistics->data);

        if (!$data_statistics) {
            $data_statistics = $this->mockStatisticsData();
        }

        return [
            'dataChart' => $chart,
            'dataStatistics' => $data_statistics->data_statistics,
            'erpLastDays' => $this->erpLastDays(),
            'suggestionByDay' => $suggestion_by_day,
            'dataOpenByDay' => $data_open,
            'dataApprovedByDay' => $data_approved,
        ];
    }

    /**
     * ******************************
     * ************ PRIVATE *********
     * ******************************
     */

    /**
     * prepareData
     * Formata os dados, incluíndo valores de mínimo e máximo, e ordena-os para melhor visualização no gráfico
     * @param $database_data databaseData ARRAY
     * @param $data_statistics dataStatistics ARRAY
     * @return ARRAY
     */
    private function prepareData($database_data, $data_statistics)
    {
        $result = [];

        if (!$database_data) {
            return $result;
        }

        foreach ($database_data as $key => $data) {
            if (is_array($data)) {
                foreach ($data as $subkey => $subdata) {
                    $result[$key][] = $this->formatData($subkey, $subdata);
                }
            } else {
                $result[] = $this->formatData($key, $data);
            }

            array_splice($result[$key], 3, 0, $this->formatData('QTY_MAX_OUT', $data_statistics[$key]['QTY_MAX_OUT']));
            array_splice($result[$key], 3, 0, $this->formatData('QTY_MIN_OUT', $data_statistics[$key]['QTY_MIN_OUT']));
        }

        return $result;
    }

    private function mockStatisticsData()
    {
        return [0, 0, 0, 0, 0, 0, 0];
    }

    private function formatData($field, $value)
    {
        switch ($field) {
            case 'DATE_HIST':
                return date('d/m/Y', strtotime($value));

            case 'QTY_OUT_CONS':
                return $value * - 1;

            case 'AVG_3M':
            case 'AVG_6M':
            case 'AVG_12M':
            case 'OUT_LAST_30D':
                return number_format($value, 2, '.', '');
            default:
                return floor($value);
        }
    }

    private function getChartData()
    {
        $table = "VW_AUX_HIST_DAY";
        $columns = "distinct date_hist,
                    qty_in,
                    qty_out_cons,
                    inv_available,
                    inv_total";
        $terms = "WHERE
                    id_company_fk = {$this->company_id}
                    AND id_sku_fk = {$this->sku_id}
                    AND date_hist BETWEEN TO_DATE('{$this->date_from}', 'YYYY-MM-DD') AND TO_DATE('{$this->date_to}' , 'YYYY-MM-DD')
                    ORDER BY DATE_HIST ASC";

        return DB::Select($table, $columns, $terms);
    }

    /**
     * getStatisticsData
     * Retorna as informações de mínimo e máximo do gráfico de Análise Geral - Histórico diário
     * @return ARRAY
     */
    private function getStatisticsData()
    {
        $table = 'scm_sku_out_hist';
        $columns = 'inv_level, date_hist, qty_min_out, qty_max_out, purchase_req, purchase_order';
        $terms = "WHERE
                    id_sku_fk = {$this->sku_id}
                    AND id_company_fk = {$this->company_id}
                    AND date_hist BETWEEN TO_DATE('{$this->date_from}', 'YYYY-MM-DD') AND TO_DATE('{$this->date_to}' , 'YYYY-MM-DD')";

        $result = DB::Select($table, $columns, $terms);
        if (!$result) {
            return [];
        }

        $obj = new stdClass();
        $obj->data = $result;
        $obj->data_statistics = $this->formatDataApprovedArray($result, 'DATE_HIST');

        return $obj;
    }

    private function erpLastDays()
    {
        $table = "scm_sku_out";
        $columns = "out_last_30d, avg_3m, avg_6m, avg_12m";
        $terms = "WHERE
                   id_sku_fk = {$this->sku_id}
                   AND id_company_fk = {$this->company_id}";

        $result = DB::SelectRow($table, $columns, $terms);
        if (!$result) {
            return ['-', '-', '-', '-'];
        }
        return $this->formatDataErpLastDays($result);
    }

    /**
     * getDataHist
     * Retorna as informações de Aprovados no dia e Ordens em abeerto para
     * a elaboração do tooltip do gráfico de Análise Geral - Histórico diário
     * @param string $column coluna do banco que monta a query
     * @return ARRAY
     */
    private function getDataHist(string $column)
    {
        $table = 'scm_sku_purchase_sug_hist';
        $columns = "TO_CHAR({$column}, 'DD/MM/YYYY') AS {$column}, NUM_PARC_PK, QTY_ALTERED";
        $terms = "WHERE
                    id_sku_fk = {$this->sku_id}
                    AND id_company_fk = {$this->company_id}
                    AND {$column} <= TO_DATE('{$this->date_to}' , 'YYYY-MM-DD')";

        if ($column == 'DATE_CREATED') {
            $terms .= ' AND DATE_APPROVED IS NULL';
        }

        $result = DB::Select($table, $columns, $terms);

        if (!$result) {
            return [];
        }

        return $result;
    }

    /**
     * getDataApproved
     * Retorna as informações de Aprovados no dia para
     * a elaboração do tooltip do gráfico de Análise Geral - Histórico diário
     * @return ARRAY
     */
    private function getDataApproved()
    {
        $table = 'scm_sku_purchase_sug_hist';
        $columns = "to_char(DATE_APPROVED, 'YYYY-MM-DD') as date_approved, count(*) AS approved";
        $terms = "WHERE
                    id_sku_fk = {$this->sku_id}
                    AND id_company_fk = {$this->company_id}
                    AND DATE_APPROVED IS NOT NULL
                    GROUP BY to_char(DATE_APPROVED, 'YYYY-MM-DD')";
        
        $result = [];
        $query_result = DB::Select($table, $columns, $terms, null);

        if ($query_result) {
            $result = $this->formatDataApprovedArray($query_result, 'DATE_APPROVED');
        }

        return $result;
    }

    /**
     * formatDataApprovedArray
     * Formata o array de dados relativos à Aprovados no Dia, para melhor exibição da tooltip
     * @param ARRAY $data dados não formatados vindos do banco
     * @param ARRAY $column coluna a ser formatada
     * @return ARRAY
     */
    private function formatDataApprovedArray(array $data, string $column)
    {
        $new_result = [];
        foreach ($data as $row) {
            $new_result[$this->formatData('DATE_HIST', $row[$column])] = $row;
        }

        return $new_result;
    }

    private function formatDataErpLastDays($data)
    {
        $new_result = [];
        foreach ($data as $key => $value) {
            $new_result[$key] = $this->formatData($key, $value);
        }

        return $new_result;
    }
}
