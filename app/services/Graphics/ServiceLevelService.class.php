<?php

/**
 * <b>ServiceLevelService</b>
 * Service responsible for managing the Service Level Graph data
 * 
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class ServiceLevelService
{
    /**
     * <b>getServiceLevel</b>
     * Retrieve the stock balance data to fill the graph. 
     * 
     * @param STRING $idCompanyPk
     * @param where $where
     * @return ARRAY
     */
    public function getServiceLevel($user, $where)
    {
		$is_supplier = Gate::setSupplier('a.', 1);
        $manager_users = Redis::Get(USERSESSIONID . ":session:dashboard:manager-users");
        $estabsIn = '';
        if(!empty($user['estab'])) {
            $estabsIn = " AND COD_ESTAB_FK IN({$user['estab']}) ";
        }

        $sql = "
		SELECT
		b.inv_level,
		NVL(COUNT(id_sku_pk), 0) AS qty
		FROM
			(
			SELECT
				ROWNUM-1 inv_level
			FROM
				(
				SELECT
					1 a
				FROM
					dual
				CONNECT BY
					LEVEL <= 6 ) ) b
		LEFT JOIN vw_scm_aux_sku a ON
			a.inv_level = b.inv_level
			AND a.sit_sku = 1
			AND a.id_company_fk = {$user['id_company_pk']} {$manager_users} {$is_supplier} {$estabsIn}"
			. str_replace("AND ", "AND a.", $where) . "
		GROUP BY
			b.inv_level
		ORDER BY
			b.inv_level
		";
		
		$skus_values = array();

		$skus = DB::Procedure($sql,true);
		foreach ($skus as $key => $value) {
			array_push($skus_values,floatval($value['QTY']));
		}

		return $skus_values;
	}
}
