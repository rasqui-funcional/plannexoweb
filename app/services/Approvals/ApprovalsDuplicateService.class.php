<?php

/**
 * <b>ApprovalsDuplicateService</b>
 * Service responsible for managing Suggest methods
 * 
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class ApprovalsDuplicateService
{

    private $approvalsDuplicateService;

    public function __construct(){
        $this->user = Login::getUserSession();
    }


    /**
     * <b>insertPurchase</b>
     * Create a new purchase on the database,
     *  with status=0.
     * @return ARRAY
     */
    public function insertPurchase() {
        $procedure = "
            SELECT
                seq_scm_purchase_sug_header.NEXTVAL as cod_header_pk
            FROM
                dual
        ";

        return DB::Procedure($procedure, true);
    }


    /**
     * <b>updatePurchase</b>
     * Retrieve the suggest data.
     * @param STRING $userId.
     * @param STRING $companyId.
     * @param STRING $codHeaderPk.
     * @return ARRAY
     */
    public function updatePurchase($oldCodHeaderPk, $newCodHeaderPk, $companyId, $codEstabFk, $post) {
        $columns = "id_company_fk,
                    cod_sector,
                    yn_urgent,
                    cod_req_reason,
                    cod_buyer,
                    obs_req,
                    dt_exp_req,
                    cod_local_fk,
                    cod_estab_fk,
                    cod_user_fk,
                    cod_req, 
                    log_error, 
                    date_approved";

        $terms = "WHERE id_company_fk = $companyId AND cod_header_pk = $oldCodHeaderPk AND cod_estab_fk = '$codEstabFk'";
        $oldPurchase = DB::Select('scm_sku_purchase_sug_header', $columns, $terms)[0];
        $dt_exp = date("d-M-Y", strtotime($post['DT_EXP_REQ']));
        
        $data = [
            'id_company_fk' => "'{$companyId}'",
            'cod_header_pk' => "'{$newCodHeaderPk}'",
            'sit_header' => '0',
            'cod_sector' => "'{$post['COD_SECTOR']}'",
            'yn_urgent' => "'{$post['YN_URGENT']}'",
            'cod_req_reason' => "'{$post['COD_REQ_REASON']}'",
            'cod_buyer' => "'{$post['COD_BUYER']}'",
            'obs_req' => "'{$post['OBS_REQ']}'",
            'dt_exp_req' => "'{$dt_exp}'",
            'cod_local_fk' => "'{$post['COD_LOCAL_FK']}'",
            'cod_estab_fk' => "'{$oldPurchase['COD_ESTAB_FK']}'",
            'cod_user_fk' => "'{$oldPurchase['COD_USER_FK']}'",
            'cod_req' => 'NULL',
            'log_error' => 'NULL',
            'date_approved' => 'SYSDATE'
        ];
        $result = DB::Insert('scm_sku_purchase_sug_header', $data);
        return $result;
    }

    public function validatePurchase($purchase_suggestions)
    {
        $sku_arr = [];
        $suggestError = [];
        foreach($purchase_suggestions as $item)
        {
            $sku_arr[$item['ID_SKU_PK']][] = $item['DATE_ARRIVAL_ALTERED'];
            if(floatval($item['QTY_ALTERED']) < 1){
                $suggestError['qty_zero'] = true;
            }
        }

        foreach($sku_arr as $sku){
            if(count(array_unique($sku)) < count($sku))
            {
                $suggestError['duplicated_date'] = true;
            }
        }
        return $suggestError;
    }

    /**
     * <b>insertPurchaseHistory</b>
     * Create a new purchase history on the database,
     * @param STRING $data.
     * @return ARRAY
     */
    public function insertPurchaseHistory($user, $newCodHeaderPk, $old_cod_header_pk, $purchase_suggestions)
    {
        $associated_purchases = DB::Select(
            'scm_sku_purchase_sug_hist',
            '*',
            "WHERE ID_COMPANY_FK = {$user['id_company_pk']}
                AND id_integration_gtp = '{$old_cod_header_pk}'"
        );

        foreach ($purchase_suggestions as $purchase_suggestion) {
            $purchase_key = array_search($purchase_suggestion['ID_PSUG_PK'], array_column($associated_purchases, 'ID_PSUG_PK'));
            $associated_purchase = $associated_purchases[$purchase_key];
            unset($associated_purchases[$purchase_key]);
            $associated_purchases = array_values($associated_purchases);
            $data = [
                'RISK_SUPPLIER' => "'{$this->buildGenericField($associated_purchase['RISK_SUPPLIER'])}'",
                'ID_COMPANY_FK' => "'{$user['id_company_pk']}'",
                'ID_PSUG_PK' => "'{$associated_purchase['ID_PSUG_PK']}'",
                'NUM_PARC_PK' => "'{$associated_purchase['NUM_PARC_PK']}'",
                'ID_INTEGRATION_GTP' => "'{$newCodHeaderPk}'",
                'ID_INTEGRATION_ERP' => "'{$this->buildGenericField($associated_purchase['ID_INTEGRATION_ERP'])}'",
                'ID_SKU_FK' => "'{$associated_purchase['ID_SKU_FK']}'",
                'DATE_CREATED' => "'{$this->purchaseDate($associated_purchase['DATE_CREATED'])}'",
                'QTY_ORIGINAL' => "'{$associated_purchase['QTY_ORIGINAL']}'",
                'QTY_ALTERED' => "'{$this->buildGenericField($purchase_suggestion['QTY_ALTERED'])}'",
                'SIT_ALTERED_QTY' => "'{$associated_purchase['SIT_ALTERED_QTY']}'",
                'DATE_ARRIVAL_ORIGINAL' => "'{$this->purchaseDate($purchase_suggestion['DATE_ARRIVAL_ALTERED'])}'",
                'DATE_ARRIVAL_ALTERED' => "'{$this->purchaseDate($purchase_suggestion['DATE_ARRIVAL_ALTERED'])}'",
                'SIT_ALTERED_DATE' => "'{$associated_purchase['SIT_ALTERED_DATE']}'",
                'COD_LOCAL' => "'{$associated_purchase['COD_LOCAL']}'",
                'SIT_PSUG' => 1,
                'ID_USER' => "'{$user['user_id']}'",
                'DATE_APPROVED' => "'{$this->purchaseDate($associated_purchase['DATE_APPROVED'])}'",
                'OBS' => "'{$this->buildGenericField($purchase_suggestion['OBS'])}'",
                'ID_USER_CREATED' => "'{$associated_purchase['ID_USER_CREATED']}'",
                'SIT_AUTO_SUG' => "'{$associated_purchase['SIT_AUTO_SUG']}'",
                'SIT_URGENT' => "'{$this->buildGenericField($associated_purchase['SIT_URGENT'])}'",
                'DATE_INTEGRATION' => "'{$this->purchaseDate($associated_purchase['DATE_INTEGRATION'])}'",
                'LOG_ERROR' => "'{$this->buildGenericField(preg_replace("/'/", '"', $oldPurchase['LOG_ERROR']))}'",
                'ID_SUG_PK' => "'{$associated_purchase['ID_SUG_PK']}'",
                'COD_ORDER_UNIT' => "'{$associated_purchase['COD_ORDER_UNIT']}'"
            ];
            DB::Insert('scm_sku_purchase_sug_hist', $data);
        }
    }

    private function buildGenericField($value)
    {
        return $value != '' ? $value : '';
    }

    private function purchaseDate($date)
    {
        return (string) Utilities::purchaseDate($date);
    }

    /**
     * <b>updatePurchaseStatus</b>
     * Update the status of the Purchase to 1
     * @param STRING $codHeaderPk.
     * @param STRING $companyId.
     * @return ARRAY
     */
    public function updatePurchaseStatus($companyId, $codHeaderPk) {
        $data = [
            'sit_header' => 1
        ];

        $terms = "WHERE id_company_fk = $companyId AND cod_header_pk = $codHeaderPk";
        
        return DB::Update('scm_sku_purchase_sug_header', $data, $terms, "id_company='$companyId'&cod_header='$codHeaderPk'");

    }


        /**
     * <b>getDuplicationTableData</b>
     * Retrieve the data that fills the table inside the "Duplicar" modal.
     * @param STRING $offset.
     * @param STRING $limit.
     * @param STRING $columns.
     * @param STRING $terms.
     * @return ARRAY
     */
    public function selectDuplicateItems($companyId, $codHeaderPk, $order){
        $table = "
            vw_scm_aux_sku a
            INNER JOIN scm_sku_purchase_sug_hist b ON a.id_company_fk = b.id_company_fk
            AND a.id_sku_pk = id_sku_fk
            inner join scm_estab e
            on a.id_company_fk=e.id_company_fk
            and a.cod_estab_fk=e.cod_estab_pk
        ";

        $columns = "
            id_psug_pk,
            b.id_company_fk,
            cod_item_pk,
            id_sku_pk,
            desc_item,
            qty_altered,
            id_integration_gtp,
            date_arrival_altered,
            dbms_lob.substr(obs, 4000, 1) as obs,
            num_parc_pk
        ";

        $terms = "WHERE a.ID_COMPANY_FK='$companyId' AND b.id_integration_gtp = '$codHeaderPk' $order";
        $result['data'] = DB::Select($table, $columns, $terms);

        $first_element = $result['data'][0];
        unset($first_element['ID_PSUG_PK']);
        unset($first_element['ID_COMPANY_FK']);
        unset($first_element['NUM_PARC_PK']);

        $result['columns'] = array_keys($first_element);

        return $result;
    }

       /**
     * <b>select</b>
     * Retrieve the raw data that fills the table inside the "Duplicar" modal.
     * @param STRING $offset.
     * @param STRING $limit.
     * @param STRING $columns.
     * @param STRING $terms.
     * @return ARRAY
     */
    public function select($companyId, $codHeaderPk) {
        $columns = '
            a.id_sku_pk,
            a.cod_item_pk,
            a.desc_item,
            b.num_parc_pk,
            b.qty_altered,
            b.date_arrival_altered,
            dbms_lob.substr(obs, 4000, 1) as obs,
            b.id_integration_gtp, b.id_psug_pk
        ';

        $table = "vw_scm_aux_sku a INNER JOIN scm_sku_purchase_sug_hist b ON a.id_company_fk = b.id_company_fk AND a.id_sku_pk = b.id_sku_fk";
        $terms = "WHERE a.id_company_fk = {$companyId} AND b.id_integration_gtp = {$codHeaderPk}";
        $result['data'] = DB::Select($table, $columns, $terms);
        
        if(empty($result['data'])){
            return [
                'data' => [],
                'columns'=> []
            ];
        }

        $result['columns'] = array_keys($result['data'][0]);

        return $result;
    }
}
