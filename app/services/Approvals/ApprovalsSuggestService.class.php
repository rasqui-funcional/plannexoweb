<?php

/**
 * <b>ApprovalsSuggestService</b>
 * Service responsible for managing Suggest methods
 *
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class ApprovalsSuggestService
{
    /**
     * <b>getHeader</b>
     * Retrieve the header data.
     * @param STRING $offset .
     * @param STRING $limit .
     * @param STRING $columns .
     * @param STRING $terms .
     * @return ARRAY
     */
    public function getHeader($codHeaderPk, $idCompanyPk)
    {
        $table = "VW_AUX_HIST_APPROVED";
        $columns = "COD_HEADER_PK,  COD_HEADER_ERP, SIT_HEADER, DESC_ESTAB, DESC_LOCAL, DT_EXP_REQ, COD_REQ_REASON, COD_BUYER, COD_SECTOR, COD_LOCAL_FK";
        $terms = "where id_company_fk=$idCompanyPk and COD_HEADER_PK=$codHeaderPk";
        return DB::Select($table, $columns, $terms)[0];
    }

    /**
     * <b>setSupplier</b>
     * Retrieve the query data.     
     * @return STRING
     */
    private function setSupplier()
    {        
        return "AND a.cod_group11_fk = '1' ";
    }

    /**
     * <b>paginate</b>
     * Retrieve the data paginated.
     * @param STRING $offset .
     * @param STRING $limit .
     * @param STRING $columns .
     * @param STRING $terms .
     * @return ARRAY
     */
    public function paginate($offset, $limit, $order, $userId, $companyId, $codHeaderPk, $searchCol = null, $searchValue = null) {

        if(empty($companyId) || $companyId == NULL || empty($userId) || $userId == NULL){
            return [
                'data' => [],
                'columns' => []
            ];
        }

        $columns = '
            NUM_PARC_PK,
            COD_ITEM_PK,
            DESC_ITEM,
            DESC_ESTAB,
            VAL_UNIT_ERP,
            DESC_STD_UNIT,
            SIT_PSUG,
            QTY_ORIGINAL,
            QTY_ALTERED,
            DATE_ARRIVAL_ORIGINAL,
            DATE_ARRIVAL_ALTERED,
            DATE_CREATED,
            DBMS_LOB.SUBSTR(OBS, 4000, 1) AS OBS,
            B.ID_USER AS LOGIN,
            DATE_APPROVED,
            COD_ESTAB_FK,
            SIT_AUTO_SUG,
            CASE WHEN SIT_AUTO_SUG = 1 AND VAL_UNIT_ERP * QTY_ORIGINAL > 0
                THEN
                 (((VAL_UNIT_ERP * QTY_ORIGINAL) - (VAL_UNIT_ERP * QTY_ALTERED))/(VAL_UNIT_ERP * QTY_ORIGINAL))*(-1) * 100
                ELSE
                    0
            END
               AS ALTERED_INDEX
        ';

        $table = '
            VW_SCM_AUX_SKU A
            INNER JOIN SCM_SKU_PURCHASE_SUG_HIST B ON A.ID_COMPANY_FK = B.ID_COMPANY_FK
            AND A.ID_SKU_PK = ID_SKU_FK
            INNER JOIN SCM_ESTAB E
            ON A.ID_COMPANY_FK=E.ID_COMPANY_FK
            AND A.COD_ESTAB_FK=E.COD_ESTAB_PK
        ';

        $dynamicWhere = $this->buildSearch($searchCol, $searchValue);

        $supplier = null;
        if (can('supplier')) {
            $supplier = $this->setSupplier();
        }

        $terms = "WHERE a.ID_COMPANY_FK='$companyId' AND b.id_integration_gtp = '$codHeaderPk' $dynamicWhere $order $supplier";
        
        $result['data'] = DB::Page($table, $offset, $limit, $columns, $terms);
        if (empty($result['data'])) {
            return [
                'data' => [],
                'columns' => []
            ];
        }

        $result['get_cod_estab'] = $result['data'][0]['COD_ESTAB_FK'];
        $result['data'] = $this->formatResult($result['data']);
        $result['columns'] = array_keys($result['data'][0]);
        return $result;
    }

    public function orderValues($data, $order)
    {
        $key  = key($order);
        switch ($order[$key]) {
            case 'asc':
                usort(
                    $data['data'],
                    function ($a, $b) use ($key) {
                        return $a[$key] > $b[$key];
                    }
                );
                break;

            case 'desc':
                usort(
                    $data['data'],
                    function ($a, $b) use ($key) {
                        return $a[$key] < $b[$key];
                    }
                );
                break;

            default:
                break;
        }

        return $data;
    }

    private function calculateAlteredIndex($original_val, $altered_val)
    {
        if ($original_val && $altered_val) {
            return (($original_val - $altered_val)/$original_val)*(-1) * 100;
        }

        return 0;
    }

    private function buildSearch($col, $value)
    {
        if ($value == "" || $col =="") {
            return "";
        }

        switch ($col) {
            case 'DATE_ARRIVAL_ORIGINAL':
            case 'DATE_ARRIVAL_ALTERED':
            case 'DATE_CREATED':
            case 'DATE_APPROVED':
                $value = Date::translateDate($value);

                return sprintf("AND TRUNC(%s) = to_date('%s', 'DD/MM/YYYY')", $col, $value);
            default:
                return "AND ($col = '$value')";

        }
    }

    private function formatResult($data)
    {        
        $i = 0;
        foreach ($data as $row) {
            $data[$i]['DESC_ESTAB'] = $row['COD_ESTAB_FK'].' - '.$row['DESC_ESTAB'];

            $data[$i]['QTY_ORIGINAL_VAL'] = 0;
            if ($row['SIT_AUTO_SUG'] == '1') {
                $data[$i]['QTY_ORIGINAL_VAL'] = (float)$row['VAL_UNIT_ERP'] * (int)$row['QTY_ORIGINAL'];
            }

            $data[$i]['QTY_ALTERED_VAL'] = (float) $row['VAL_UNIT_ERP'] * (int) $row['QTY_ALTERED'];
            $data[$i]['LOGIN'] = $this->getUserLogin($data[$i]['LOGIN'])['LOGIN'];
            $altered_index = $data[$i]['ALTERED_INDEX'];
            unset($data[$i]['COD_ESTAB_FK'], $data[$i]['SIT_AUTO_SUG'], $data[$i]['ID_USER'], $data[$i]['ALTERED_INDEX']);
            $data[$i]['ALTERED_INDEX'] = $altered_index;
            $i++;
        }
        return $data;
    }

    /**
     * <b>select</b>
     * Retrieve the data paginated.
     * @param STRING $offset .
     * @param STRING $limit .
     * @param STRING $columns .
     * @param STRING $terms .
     * @return ARRAY
     */
    public function select($order, $userId, $companyId, $codHeaderPk, $searchCol, $searchValue)
    {
        if (empty($companyId) || $companyId == null || empty($userId) || $userId == null) {
            return [
                'data' => [],
                'columns' => []
            ];
        }

        $columns = '
            NUM_PARC_PK,
            COD_ITEM_PK,
            DESC_ITEM,
            DESC_ESTAB,
            VAL_UNIT_ERP,
            DESC_STD_UNIT,
            SIT_PSUG,
            QTY_ORIGINAL,
            QTY_ALTERED,
            DATE_ARRIVAL_ORIGINAL,
            DATE_ARRIVAL_ALTERED,
            DATE_CREATED,
            DBMS_LOB.SUBSTR(OBS, 4000, 1) AS OBS,
            B.ID_USER AS LOGIN,
            DATE_APPROVED,
            COD_ESTAB_FK,
            SIT_AUTO_SUG,
            CASE WHEN SIT_AUTO_SUG = 1 AND VAL_UNIT_ERP * QTY_ORIGINAL > 0
                THEN
                (((VAL_UNIT_ERP * QTY_ORIGINAL) - (VAL_UNIT_ERP * QTY_ALTERED))/(VAL_UNIT_ERP * QTY_ORIGINAL))*(-1) * 100
                ELSE
                    0
            END
            AS ALTERED_INDEX
        ';

        $table = '
            vw_scm_aux_sku a
            INNER JOIN scm_sku_purchase_sug_hist b ON a.id_company_fk = b.id_company_fk
            AND a.id_sku_pk = id_sku_fk
            inner join scm_estab e
            on a.id_company_fk=e.id_company_fk
            and a.cod_estab_fk=e.cod_estab_pk
        ';

        $dynamicWhere = $this->buildSearch($searchCol, $searchValue);
        
        $supplier = null;
        if (can('supplier')) {
            $supplier = $this->setSupplier();
        }

        $terms = "WHERE a.ID_COMPANY_FK='$companyId' AND b.id_integration_gtp = '$codHeaderPk' $dynamicWhere $order $supplier";

        $result['data'] = DB::Select($table, $columns, $terms);
        if (empty($result['data'])) {
            return [
                'data' => [],
                'columns' => []
            ];
        }
        $result['get_cod_estab'] = $result['data'][0]['COD_ESTAB_FK'];
        $result['data'] = $this->formatResult($result['data']);
        $result['columns'] = array_keys($result['data'][0]);
        return $result;
    }

    public function count($userId, $companyId, $codHeaderPk, $searchCol, $searchValue)
    {
        if (empty($companyId) || $companyId == null || empty($userId) || $userId == null) {
            return 0;
        }

        $columns = 'COUNT(COD_ESTAB_FK) AS TOTAL';

        $table = '
            VW_SCM_AUX_SKU A
            INNER JOIN SCM_SKU_PURCHASE_SUG_HIST B ON A.ID_COMPANY_FK = B.ID_COMPANY_FK
            AND A.ID_SKU_PK = ID_SKU_FK
            INNER JOIN SCM_ESTAB E
            ON A.ID_COMPANY_FK=E.ID_COMPANY_FK
            AND A.COD_ESTAB_FK=E.COD_ESTAB_PK
        ';

        $searched = $this->buildSearch($searchCol, $searchValue);
        
        $supplier = null;
        if (can('supplier')) {
            $supplier = $this->setSupplier();
        }

        $terms = "WHERE A.ID_COMPANY_FK='$companyId' AND B.ID_INTEGRATION_GTP = '$codHeaderPk' $searched $supplier";

        $result = DB::SelectRow($table, $columns, $terms);
        ;
        if (empty($result)) {
            return 0;
        }

        return $result['TOTAL'];
    }


    public function getByCodParam($companyId, $estab, $codParam)
    {
        $columns = "val_param as id, desc_param as value";
        $table = "scm_purchase_sug_param";
        $terms = "where id_company_fk = {$companyId} AND cod_estab_fk = '{$estab}'";

        $terms .= "AND cod_param = '{$codParam}'";
        $result = DB::Select($table, $columns, $terms);
        if (!$result || $result == null) {
            return [];
        }

        return $result;
    }
        
    public function getDeposit($companyId, $estab)
    {
        $Columns = "cod_local_pk as id, desc_local as value";
        $Table = "erp_local";
        $Terms = "WHERE id_company_fk = {$companyId} AND cod_estab_pk = '{$estab}'";
        $getDepositResult = DB::Select($Table, $Columns, $Terms);
        if (!$getDepositResult || $getDepositResult == null) {
            return [];
        }

        return $getDepositResult;
    }

    public function deleteItem($id_company, $id_ps, $num_par, $id_integration)
    {
        $Table = "scm_sku_purchase_sug_hist";
        $Terms = "WHERE id_company_fk = :id_company AND id_ps_pk = :id_ps AND num_parc_pk = :num_par AND id_integration_gtp = :id_integration";
        $parse = "id_company={$id_company}&id_ps={$id_ps}&num_parc_pk={$num_par}&id_integration={$id_integration}";

        return DB::Delete($Table, $Terms, $parse);
    }

    /**
     * @param $id_company_pk
     * @param $cod_header_pk
     * @param $searchInput
     * @param $search
     * @return mixed
     */
    public function getTotalFooter(int $id_company_pk, int $cod_header_pk, string $searchCol, string $searchValue)
    {
        $table = "vw_scm_aux_sku a
                    INNER JOIN scm_sku_purchase_sug_hist b
                        ON a.id_company_fk = b.id_company_fk AND a.id_sku_pk = id_sku_fk
                    INNER JOIN scm_estab e
                        ON a.id_company_fk = e.id_company_fk
                        AND a.cod_estab_fk = e.cod_estab_pk
        ";

        $dynamicWhere = $this->buildSearch($searchCol, $searchValue);

        $supplier = null;
        if (can('supplier')) {
            $supplier = $this->setSupplier();
        }

        $terms = "WHERE a.ID_COMPANY_FK = '{$id_company_pk}' AND b.id_integration_gtp = '{$cod_header_pk}' {$dynamicWhere} {$supplier}";

        $footer = DB::SelectRow($table, $this->getFooterCols(), $terms);
        $footer['ALTERED_INDEX'] = $this->calculateAlteredIndex(
            $footer['QTY_ORIGINAL_VAL'],
            $footer['QTY_ALTERED_VAL']
        );

        return $footer;
    }

    /**
     * @return string
     */
    private function getFooterCols() : string
    {
        return "
                SUM(NUM_PARC_PK) as NUM_PARC_PK,
                SUM(QTY_ORIGINAL) as QTY_ORIGINAL,
                SUM(QTY_ALTERED) as QTY_ALTERED,
                SUM(
                    CASE WHEN SIT_AUTO_SUG = 1
                        THEN
                            VAL_UNIT_ERP * QTY_ORIGINAL
                        ELSE 0
                    END
                ) as QTY_ORIGINAL_VAL,
                SUM(VAL_UNIT_ERP * QTY_ALTERED) as QTY_ALTERED_VAL,
                0 as ALTERED_INDEX";
    }

    private function getUserLogin($id_user)
    {
        return DB::SelectRow('UAC_USER', 'LOGIN', "WHERE ID_USER_PK = {$id_user}");
    }
}
