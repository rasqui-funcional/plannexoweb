<?php

/**
 * <b>ApprovalsService</b>
 * Service responsible for managing Suggest methods
 *
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class ApprovalsService
{
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function prepareData($data)
    {
        $result = [];
        foreach ($data as $key => $value) {
            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * <b>paginate</b>
     * Retrieve the data paginated.
     * @param STRING $offset .
     * @param STRING $limit .
     * @param STRING $order .
     * @param STRING $companyId .
     * @param STRING $search .
     * @param STRING|ARRAY $params .
     * @return ARRAY
     */
    public function paginate($offset, $limit, $order, $companyId, $searchValue, $searchCol, $params = null)
    {
        $columns = "TOTAL_PARCELAS,
                    COD_HEADER_PK,
                    COD_HEADER_ERP,
                    SIT_HEADER,
                    DESC_ESTAB,
                    DESC_LOCAL,
                    YN_URGENT,
                    DESC_REQ_REASON,
                    DESC_BUYER,
                    DESC_SECTOR,
                    DESC_USER,
                    DT_EXP_REQ,
                    DATE_INTEGRATION,
                    TO_CHAR(DATE_APPROVED, 'DD/MM/YYYY HH24:MI:SS') AS DATE_APPROVED,
                    OBS_REQ";
        $table = "VW_AUX_HIST_APPROVED";

        $dynamicWhere = $this->buildSearch($searchCol, $searchValue);
        $filtro = $this->prepareWhere($params);
        $fixedOrder = ", COD_HEADER_PK DESC";
        if ($order == "") {
            $order = "order by COD_HEADER_PK DESC";
            $fixedOrder = ""; 
        }

        $filtro = $this->prepareWhere($params);

        $is_supplier = null;
        if (can('supplier')) {
            $table = "VW_AUX_HIST_APPROVED_SUPPLIER";
            $is_supplier = $this->isSupplier($companyId);
        }
        $estabIn = $this->codEstabByUser();

        $terms = "WHERE ID_COMPANY_FK = {$companyId} {$estabIn} {$is_supplier} {$filtro} {$dynamicWhere} {$order} {$fixedOrder}";

        $result['data'] = DB::Page($table, $offset, $limit, $columns, $terms);
        if (empty($result['data'])) {
            return [
                'data' => [],
                'columns' => []
            ];
        }

        $result['columns'] = array_keys($result['data'][0]);
        return $result;
    }

    public function prepareDataFooter($cols, $userId, $companyId, $searchValue, $searchCol, $params = null)
    {
        $table = "VW_AUX_HIST_APPROVED";
        $dynamicWhere = $this->buildSearch($searchCol, $searchValue);
        $filtro = $this->prepareWhere($params);

        $is_supplier = null;
        if (can('supplier')) {
            $table = "VW_AUX_HIST_APPROVED_SUPPLIER";
            $is_supplier = $this->isSupplier($companyId);
        }

        $estabIn = $this->codEstabByUser();

        $terms = "WHERE ID_COMPANY_FK = {$companyId} {$estabIn} {$is_supplier} {$filtro} {$dynamicWhere} ";

        return DB::Select($table, $cols, $terms)[0];
    }

    private function buildSearch($col, $value)
    {
        if ($value == "" || $col == "") {
            return "";
        }

        switch ($col) {
            case 'COD_ITEM':
                return "AND COD_HEADER_PK IN (
                    SELECT DISTINCT ID_INTEGRATION_GTP 
                    from scm_sku_purchase_sug_hist ps
                    INNER JOIN SCM_SKU sku
                    ON sku.ID_SKU_PK  = ps.ID_SKU_FK 
                    WHERE sku.COD_ITEM_PK = '$value'
                )";
            case 'DT_EXP_REQ':
            case 'DATE_APPROVED':
                $value = Date::translateDate($value);
                return sprintf("AND TRUNC(%s) = to_date('%s', 'DD/MM/YYYY')", $col, $value);

            default:
                return "AND ($col = '$value')";
        }
    }

    private function prepareWhere($conditions)
    {
        if (!is_array($conditions) || is_null($conditions)) {
            return "";
        }

        return "AND (DATE_APPROVED BETWEEN TO_DATE('{$conditions['start']}', 'DD/MM/YYYY') AND TO_DATE('{$conditions['end']}', 'DD/MM/YYYY')) ";
    }

    /**
     * <b>select</b>
     * Retrieve the data paginated.
     * @param STRING $offset .
     * @param STRING $limit .
     * @param STRING $columns .
     * @param STRING $terms .
     * @return ARRAY
     */
    public function select($order, $userId, $companyId, $searchValue = null, $searchCol = null, $params = null)
    {
        $columns = "TOTAL_PARCELAS,
                    COD_HEADER_PK,
                    COD_HEADER_ERP,
                    SIT_HEADER,
                    DESC_ESTAB,
                    DESC_LOCAL,
                    YN_URGENT,
                    DESC_REQ_REASON,
                    DESC_BUYER,
                    DESC_SECTOR,
                    DESC_USER,
                    DT_EXP_REQ,
                    DATE_INTEGRATION,
                    TO_CHAR(DATE_APPROVED, 'DD/MM/YYYY HH24:MI:SS') AS DATE_APPROVED,
                    OBS_REQ";
        $table = "VW_AUX_HIST_APPROVED";

        $filtro = $this->prepareWhere($params);
        $dynamicWhere = $this->buildSearch($searchCol, $searchValue);
        $is_supplier = null;

        $fixedOrder = ", COD_HEADER_PK DESC";
        if ($order == "") {
            $order = "order by COD_HEADER_PK DESC";
            $fixedOrder = "";
        }

        if (can('supplier')) {
            $table = "VW_AUX_HIST_APPROVED_SUPPLIER";
            $is_supplier = $this->isSupplier($companyId);
        }

        $estabIn = $this->codEstabByUser();

        $terms = "WHERE ID_COMPANY_FK = {$companyId} {$estabIn} {$is_supplier} {$filtro} {$dynamicWhere} {$order} {$fixedOrder}";

        $result['data'] = DB::Select($table, $columns, $terms);

        if (empty($result['data'])) {
            return [
                'data' => [],
                'columns' => []
            ];
        }

        $result['columns'] = array_keys($result['data'][0]);
        return $result;
    }

    public function countRecords($order, $userId, $companyId, $searchValue = null, $searchCol = null, $params = null)
    {

        $columns = "count(*) as QTD";
        $table = "VW_AUX_HIST_APPROVED";

        $filtro = $this->prepareWhere($params);
        $dynamicWhere = $this->buildSearch($searchCol, $searchValue);
        $is_supplier = null;

        $fixedOrder = ", COD_HEADER_PK DESC";
        if ($order == "") {
            $order = "order by COD_HEADER_PK DESC";
            $fixedOrder = "";
        }

        if (can('supplier')) {
            $table = "VW_AUX_HIST_APPROVED_SUPPLIER";
            $is_supplier = $this->isSupplier($companyId);
        }

        $estabIn = $this->codEstabByUser();

        $terms = "WHERE ID_COMPANY_FK = {$companyId} {$estabIn} {$is_supplier} {$filtro} {$dynamicWhere} {$order} {$fixedOrder}";

        $result = DB::Select($table, $columns, $terms);

        if (empty($result)) {
            return [
                'data' => [],
                'columns' => []
            ];
        }

        return $result[0]['QTD'];
    }

    private function isSupplier($companyId)
    {
        /**
         * Essa query foi elaborada pelo Ronald.
         * Para trazer os históricos para perfil de fornecedor (cod_group11 = '1')
         * foi preciso fazer dessa forma, é uma excessão para essa tela pois trabalha com headers apenas
         */

        $table = "scm_sku_purchase_sug_hist h
        JOIN scm_sku s
            ON s.id_company_fk = h.id_company_fk
            AND s.id_sku_pk = h.id_sku_fk";
        $cols = 'distinct h.id_integration_gtp';
        $terms = "WHERE s.cod_group11_fk = '1' AND s.id_company_fk = {$companyId}";

        $codHeader =  DB::Select($table, $cols, $terms);

        $arrCodHeader = array_map(array($this,'prepareCodHeader'), $codHeader);
        $arrCodHeader = implode("','", $arrCodHeader);

        $termToSupplier = "AND cod_header_pk in ('{$arrCodHeader}') AND COD_HEADER_PK >= 90090";

        return $termToSupplier;

    }

    public function prepareCodHeader($val) {
        return $val['ID_INTEGRATION_GTP'];
    }

    private function codEstabByUser()
    {
        if(!empty($this->user['estab'])) {

            return " AND COD_ESTAB_FK IN({$this->user['estab']}) ";
        }

        return '';
    }
}

?>