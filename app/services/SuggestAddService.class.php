<?php

/**
 * SuggestAddService
 * Servers the Login controller with autenthic, validate and check user login.
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class SuggestAddService
{

    private $company_id;

    public function __construct($company_id)
    {
        $this->company_id = $company_id;
    }

    public function getSkuDescription($cod_item, $cod_estab_fk = null)
    {

        $cod_estab_filter = '';

        if($cod_estab_fk){
            $cod_estab_filter =" and cod_estab_fk = '{$cod_estab_fk}' ";
        }

        $result = DB::Select(
            "scm_sku",
            "desc_item, id_sku_pk, cod_estab_fk",
            "where id_company_fk = {$this->company_id} and cod_item_pk = '{$cod_item}' ".$cod_estab_filter." order by cod_estab_fk"
        );

        if (!$result) {
            return false;
        }
        
        return $result;
    }

    public function getDataPsugAndNumParc($sku_id)
    {
        $data = DB::Select(
            'scm_sku_purchase_sug',
            'COUNT(*) num_rows, NVL(MAX(num_parc_pk), 0)+ 1 num_parc, NVL(MAX(id_psug_pk), 0) psug',
            "WHERE id_company_fk = $this->company_id AND id_sku_fk = $sku_id"
        );

        if ($data) {
            return $data[0];
        }

        return false;
    }

    public function getDataNewPsug()
    {
        $data = DB::Select(
            'scm_sku_purchase_sug',
            'max(id_psug_pk) AS max_psug',
            "WHERE id_company_fk = $this->company_id"
        );

        return $data[0]['MAX_PSUG'];
    }

    public function insertSuggest($data)
    {
        return $data;
    }

}