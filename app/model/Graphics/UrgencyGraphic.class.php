<?php

/**
 * Class UrgencyGraphic
 */
class UrgencyGraphic
{

    /**
     * @var UrgencyService
     */
    private $service;

    /**
     * UrgencyGraphic constructor.
     */
    public function __construct(UrgencyService $service)
    {
        $this->service = $service;
    }

    /**
     * @return array
     */
    public function buildUrgency(): array
    {
        return [
            'monthly' => $this->generateArrayToChart($this->service->getUrgency()),
            'weekly' => $this->generateArrayToChart($this->service->getUrgency('IW', 84))
        ];
    }

    /**
     * @param $data
     * @return array
     */
    private function generateArrayToChart(array $data): array
    {
        return array_map(array($this, 'formatDataChart'), $data);
    }


    /**
     * @param $array_to_format
     * @return array
     */
    private function formatDataChart(array $array_to_format): array
    {
        return [
            "DATE" => $array_to_format['DT_RELATED'],
            "TOTAL" => $array_to_format['TOTAL'],
            "TOTAL_URGENT" => $array_to_format['TOTAL_URGENT'],
            "PERC_TOTAL_URGENT" =>  (float)$array_to_format['PERC_TOTAL_URGENT'],
            "TOTAL_URGENT_LT" => $array_to_format['TOTAL_URGENT_LT'],
            "PERC_TOTAL_URGENT_LT" => (float)$array_to_format['PERC_TOTAL_URGENT_LT']
        ];
    }
}