<?php

/**
 * Class AdhesionGraphic
 */
class AdhesionGraphic
{

    /**
     * @var AdhesionService
     */
    private $service;

    /**
     * AdhesionGraphic constructor.
     */
    public function __construct(AdhesionService $service)
    {
        $this->service = $service;
    }

    /**
     * @return array
     */
    public function buildAdhesion(): array
    {
        return [
            'monthly' => $this->generateArrayToChart($this->service->getAdhesion()),
            'weekly' => $this->generateArrayToChart($this->service->getAdhesion('W')),
            'total' => $this->generateAdhesionTotal($this->service->getAdhesionTotal()),
        ];
    }

    /**
     * @param $data
     * @return array
     */
    private function generateArrayToChart(array $data): array
    {
        return array_map(array($this, 'formatDataChart'), $data);
    }


    /**
     * @param $array_to_format
     * @return array
     */
    private function formatDataChart(array $array_to_format): array
    {
        return [
            "DATE" => $array_to_format['DT_RELATED'],
            "PLANNEXO" => Utilities::getPercentage($array_to_format['TOTAL'], $array_to_format['PLANNEXO']),
            "ERP" => Utilities::getPercentage($array_to_format['TOTAL'], $array_to_format['ERP']),
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    private function generateAdhesionTotal(array $data): array
    {
        return [
            "PLANNEXO" => Utilities::getPercentage($data['TOTAL'], $data['PLANNEXO']),
        ];

    }

}