<?php

/**
 *
 * Utilities.class [ HELPERS]
 * Classe responsável por manipular e validar dados do sistema!
 * @copyright (c) 2018, Alef Alves Lima - Bionexo
 */
class Utilities
{
    private static $Data;
    private static $Format;

    /**
     * setOffSet
     * A simple way to get the offset to use on pagination query
     *
     * @param INTEGER $page
     * @param INTEGER $limit
     * @return INTENGER
     */
    public static function setOffset($page, $limit)
    {
        return ($page - 1) * $limit;
    }

    /**
     * <b>suggestFormatter</b>
     * Formartar os dados da View 'suggest/list' conforme o esperado.
     * @throws Exception
     */
    public static function dataFormatter($Col, $Value)
    {
        global $lang;

        if (is_null($Value) || $Value === 'NULL' || is_array($Value)) {
            return null;
        }

        switch ($Col) {
            case 'PARC_DAYS':
            case 'QTY_TARGET':
            case 'INV_DAYS':
                return round($Value, 0);

            case 'OUT_YESTERDAY_NUM_DEV':
            case 'SERVICE_LEVEL_REAL':
            case 'SERVICE_LEVEL_TARGET':
            case 'QTY_ORIGINAL_VAL':
            case 'QTY_ALTERED_VAL':
            case 'VAL_ALTERED':
            case 'VAL_UNIT_ERP':
            case 'AVAILABLE_MONEY':
            case 'INV_AVAILABLE_MONEY':
            case 'VL_EXCESS':
            case 'TOTAL_MONEY':
            case 'INV_TOTAL_MONEY':
            case 'LAST_PURCHASE_PRICE':
            case 'QTY_INV_VAL':
            case 'VAL_TOTAL' :
            case 'VAL_UNIT':
                return self::Currency($Value);

            case 'QTY_ORIGINAL':
                return number_format($Value, 0, '', '.');

            /**
             * toFloatFormat
             */
            case 'QTY_FORECAST':
            case 'QTY_FORECAST_R3':
            case 'INV_AVAILABLE':
            case 'INV_TOTAL':
            case 'QTY_MIN_OUT':
            case 'QTY_PP':
            case 'QTY_MAX_OUT':
            case 'QTY_MIN_OUT_ORI':
            case 'QTY_MAX_OUT_ORI':
            case 'AVG_3M':
            case 'AVG_6M':
            case 'AVG_12M':
            case 'QTY_MIN_ERP':
            case 'QTY_MAX_ERP':
            case 'QTY_PP_ERP':
            case 'QTY_AVG_CONS_ERP':
            case 'OUT_LAST_30D':
            case 'OUT_MONTH_MINUS12':
            case 'OUT_MONTH_MINUS11':
            case 'OUT_MONTH_MINUS10':
            case 'OUT_MONTH_MINUS9':
            case 'OUT_MONTH_MINUS8':
            case 'OUT_MONTH_MINUS7':
            case 'OUT_MONTH_MINUS6':
            case 'OUT_MONTH_MINUS5':
            case 'OUT_MONTH_MINUS4':
            case 'OUT_MONTH_MINUS3':
            case 'OUT_MONTH_MINUS2':
            case 'OUT_MONTH_MINUS1':
            case 'OUT_CURRENT_MONTH':
            case 'QTY_INV':
            case 'QTY_TRANS':
                return self::toFloatFormat($Value);

            case 'QTY_NEXT_IN':
            case 'QTY_LAST_IN':
            case 'PURCHASE_REQ':
                return self::Unity($Value);

            case 'RISK_SUPPLIER':
                return ($Value < 0) ? 0 : $Value;

            case 'DATE_ADDED':
                $Day = substr($Value, 0, 2);
                $Month = substr($Value, 2, 2);
                $Year = substr($Value, 4, 4);
                return "{$Day}/{$Month}/{$Year}";

            case 'DATE_ARRIVAL_ORIGINAL':
            case 'DATE_CREATED':
            case 'DATE_NEXT_IN':
            case 'DATE_LAST_OUT':
            case 'DATE_LAST_IN':
            case 'LAST_PURCHASE_DATE':
            case 'DATE_MATURE':
            case 'DT_EXP_REQ':
            case 'V_DATE_CONS_EXPIRATION':
            case 'DATE_TRANS':
            case 'DATE_TRANS_ALT':
                $new_value = "-";
                $Datetime = strtotime($Value);
                if ($Datetime) {
                    $new_value = date('d/m/Y', $Datetime);
                }
                return $new_value;

            case 'DATE_INTEGRATION':
                $Datetime = strtotime($Value);
                return ($Datetime) ? date('d/m/Y h:i:s', $Datetime) : "-";

            case 'DATE_AMERICAN':
                $Datetime = strtotime($Value);
                return ($Datetime) ? date('Y/m/d', $Datetime) : "-";

            case 'DATE_MONTH_YEAR':
                $Datetime = strtotime($Value);
                return ($Datetime) ? date('m/Y', $Datetime) : "-";

            case 'ID_CALENDAR_FK':
                return self::calendarFormatter($Value);

            case 'COD_ESTAB_FK':
                return self::getEstab($Value);

            case 'INV_LEVEL':
                return "INV_LEVEL_{$Value}.png";

            case 'DATE_ARRIVAL_ALTERED':
                $date = new DateTime($Value);
                return $date->format('d/m/Y');

            case 'DATE_REQ':
            case 'DATE_ORDER':
            case 'DATE_EXP':
                // $date = new DateTime(self::shortMonthToInt($Value));
                // return $date->format('d/m/Y');
                $date = new DateTime($Value);
                return $date->format('d/m/Y');

            case 'OUT6':
                return number_format($Value, 2, ',', '');

            case 'ID_PROFILE':
            case 'ID_PROFILE_FK':
                return self::getPolitic($Value);

            case 'USER_ID':
                return self::getUser($Value);

            case 'SIT_SKU':
                return number_format($Value, 2, '.', '');

            case 'IND_EXCESS':
                return self::dispMovStatus($Value);

            case 'ALTERED_INDEX':
                return self::formatPercentage($Value);

            case 'SIT_PSUG':
                return self::formatSuggestStatus($Value);

            case 'YN_CONS':
                return $Value == 'Y' ? $lang['global']['yes'] : $lang['global']['no'];

            case 'COD_LOCAL':
            case 'COD_LOCAL_DEST':
            case 'TYPE_TRANS':
            case 'COD_SECTOR':
                return $Value == -1 ? $lang['info_sku']['erp']['nao_definido'] : $Value;

            default:
                return $Value;
        }
    }

    public static function toFloatFormat($value, $decimal = 4)
    {
        $parse_float = explode('.', $value);
        return isset($parse_float[1]) ? number_format($value, $decimal, ',','.') : $value;
    }

    public static function shortMonthToInt($date)
    {
        global $lang;

        $date_arr = explode('-', $date);
        $months = array_change_key_case(array_flip($lang['months']['ustobr']), CASE_UPPER);
        $nmonth = date('m',strtotime($months[strtoupper($date_arr[1])]));
        $date = DateTime::createFromFormat('y', $date_arr[2]);
        $date_arr[2] = $date->format('Y');

        return "{$date_arr[0]}-{$nmonth}-{$date_arr[2]}";
    }

    /**
     * <b>getPolitics</b>
     * Return politics of a company by ID
     * @param Integer $user_id
     * @return String
     */
    public static function getUser($user_id)
    {
        $session = Login::getUserSession();

        $user = Redis::HGet("company:{$session['id_company_pk']}:users", $user_id);
        if (!$user) {
            return self::updateUsers($session['id_company_pk'], $user_id);
        }

        return $user;
    }

    /**
     * <b>updatePolitics</b>
     * Update all politics of a company on cache.
     * @param $company_id
     * @param $user_id
     * @return mixed
     */
    public static function updateUsers($company_id, $user_id = null)
    {
        $Select = DB::Select('uac_user', 'id_user_pk,login', 'where active = 1 and id_company_fk = ' . $company_id);
        $Data = null;
        foreach ($Select as $Result) {
            $Data[$Result['ID_USER_PK']] = $Result['LOGIN'];
        }

        if(!is_null($Data)){
          Redis::HSet("company:{$company_id}:users", $Data);
        }
        return $Data[$user_id] ?? sprintf('User not found for id#%d', $user_id);
    }

    /**
     * <b>getPolitics</b>
     * Return politics of a company by ID
     * @param Integer $politic_id
     * @return String
     */
    public static function getPolitic($politic_id = null)
    {
        $session = Login::getUserSession();

        $politic = Redis::HGet("company:{$session['id_company_pk']}:politics", $politic_id);
        if (!$politic) {
            return self::updatePolitics($session['id_company_pk'], $politic_id);
        }

        return $politic;
    }

    /**
     * <b>updatePolitics</b>
     * Update all politics of a company on cache.
     * @param $company_id
     * @param null $politic_id
     * @return mixed
     */
    public static function updatePolitics($company_id, $politic_id = null)
    {
        $Select = DB::Select(
            'scm_profile',
            "id_profile_pk,id_profile_pk || '-' || cod_profile AS nome",
            "where id_company_fk = " . $company_id
        );

        $Data = null;
        foreach ($Select as $Result) {
            $Data[$Result['ID_PROFILE_PK']] = $Result['NOME'];
        }

        if(!is_null($Data)){
          Redis::HSet("company:{$company_id}:politics", $Data);
        }
        

        return $Data[$politic_id] ?? sprintf('Police not found for id#%d', $politic_id);
    }

    /**
     * <b>getEstab</b>
     * Retornar o estabelecimento do cliente conforme o ID
     * @param $estab_id
     * @return String
     */
    public static function getEstab($estab_id)
    {
        $session = Login::getUserSession();

        $estab = Redis::HGet("company:{$session['id_company_pk']}:estabs", $estab_id);
        if (!$estab) {
            return self::updateEstabs($session['id_company_pk'], $estab_id);
        }

        return $estab;
    }

    /**
     * <b>updateEstabs</b>
     * Atualiza os estabelecimentos do cliente no cache.
     * @param $company_id
     * @param $estab_id
     * @return mixed|string
     */
    public static function updateEstabs($company_id, $estab_id = null)
    {
        $Select = DB::Select(
            'SCM_ESTAB',
            " cod_estab_pk || '-' || desc_estab_short AS estab, cod_estab_pk",
            "WHERE id_company_fk = {$company_id}"
        );
        $Data = null;
        foreach ($Select as $Result) {
            $Data[$Result['COD_ESTAB_PK']] = $Result['ESTAB'];
        }
        if(!is_null($Data)){
          Redis::HSet("company:{$company_id}:estabs", $Data);
        }

        return $Data[$estab_id] ?? sprintf('Establishment not found for id#%d', $estab_id);
    }

    /**
     * <b>calendarFormatter</b>
     * Retornar a descrição do calendário armezenado no cache da companhia
     * @param $calendar_id
     * @return String
     */
    public static function calendarFormatter($calendar_id)
    {
        $session = Login::getUserSession();

        $calendar = Redis::HGet("company:{$session['id_company_pk']}:calendars", $calendar_id);

        if (!$calendar) {
            self::updateCalendars($session['id_company_pk']);
        }

        return $calendar;
    }

    /**
     * <b>updateCalendars</b>
     * Atualiza a descrição do calendário com o banco de dados.
     * @param $company_id
     */
    public static function updateCalendars($company_id, $calendars_id = null)
    {
        $Select = DB::Select('SCM_CAL_RECEIVING', 'id_calendar_pk, desc_calendar', "WHERE id_company_fk = {$company_id}");

        $Data = null;
        foreach ($Select as $Result) {
            $Data[$Result['ID_CALENDAR_PK']] = $Result['ID_CALENDAR_PK'] . " - " . $Result['DESC_CALENDAR'];
        }

        if(!is_null($Data)){
          Redis::HSet("company:{$company_id}:calendars", $Data);
        }
    }

    /**
     * <b>updateGroupping</b>
     * Atualiza a descrição do groupping com o lang.
     */
    public static function updateGroupping($company_id) 
    {
        global $lang;

        $groupping = [];
        for ($i=1; $i < 13; $i++) { 
            $groupping["cod_group{$i}_fk"]["title"] = $lang["column"]["cod_group{$i}_fk"];
            $groupping["cod_group{$i}_fk"]["abbr"] = $lang["column"]['abrev']["cod_group{$i}_fk"];
        }

        Redis::Set("company:{$company_id}:groupping", json_encode($groupping));
    }

    /**
     * <b>calculatePages</b>
     * Calcula o número de páginas para paginação.
     * @param Integer $Itens
     * @param Integer $Limit
     * @return Integer
     */
    public static function calculatePages($Itens, $Limit)
    {
        $PagesDivision = $Itens / $Limit;
        if ($PagesDivision < 1) {
            return 1;
        }

        return (int)(is_float($PagesDivision) ? ceil($PagesDivision) : $PagesDivision);
    }

    /**
     * <b>Currency</b>
     * Transformar um valor padrão do banco dados em padrão flaot para moeda real.
     * @param Float $Valor
     * @return Float
     */
    public static function Currency($Valor)
    {
        return number_format((float)$Valor, 2, ',', '.');
    }

    public static function bigNumber($value)
    {
        return number_format((float)$value, 0, ',', '.');
    }

    /**
     * <b>Unity</b>
     * Transforma um valor unitário em padrão com ponto a cada 3 casas
     * @param $valor
     * @return STRING
     */
    public static function Unity($valor)
    {
        if ($valor == 0) {
            return 0;
        }

        $round = round($valor);
        $valor = str_replace('.', '', $round);
        $valor = number_format((double)$valor, 0, '', '.');
        return $valor;
    }

    /**
     * <b>CurrencyDB</b>
     * Transformar um valor padrão da moeda real em padrão flaot do banco dados.
     * @param Float $Valor
     * @return Float
     */
    public static function CurrencyDB($Valor)
    {
        $Moeda = str_replace('.', '', $Valor);
        $Moeda = str_replace(',', '.', $Moeda);
        return $Moeda;
    }

    /**
     * <b>Email</b>
     * Verifcar se o email é válido.
     * @param STRING $Email
     */
    public static function Email($Email)
    {
        self::$Data = (string)$Email;
        self::$Format = '/[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\.\-]+\.[a-z]{2,4}$/';

        if (preg_match(self::$Format, self::$Data)):
            return true; else:
            return false;
        endif;
    }

    /**
     * <b>Name</b>
     * Fazer URL amigável ou somente tirar todos carecteres especiais e transformar em comuns.
     * @param STRING $Name
     * @return STRING
     */
    public static function Name($Name)
    {
        self::$Format = array();
        self::$Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
        self::$Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

        self::$Data = strtr(utf8_decode($Name), utf8_decode(self::$Format['a']), utf8_decode(self::$Format['b']));
        self::$Data = strip_tags(trim(self::$Data));
        self::$Data = str_replace(' ', '-', self::$Data);
        self::$Data = str_replace(array('-----', '----', '---', '---'), '-', self::$Data);

        return strtolower(utf8_decode(self::$Data));
    }

    /**
     * <b>Data</b>
     * Faz o tratamento da dada para inserção no banco de dados.
     * @param STRING $Data
     */
    public static function Data($Data)
    {
        self::$Format = explode(' ', $Data);
        self::$Data = explode('/', self::$Format[0]);

        if (empty(self::$Format[1])):
            self::$Format[1] = date('H:i:s');
        endif;

        self::$Data = self::$Data[2] . '-' . self::$Data[1] . '-' . self::$Data[0] . ' ' . self::$Format[1];
        return self::$Data;
    }

    /**
     * <b>Words</b>
     * Limita um número des palavra e concatena um pointer(usado em artigos na página principal, para colocar o continue lendo).
     * @param STRING $String
     * @param INT $Limite
     * @param STRING $Pointer
     */
    public static function Words($String, $Limite, $Pointer = null)
    {
        self::$Data = strip_tags(trim($String));
        self::$Format = (int)$Limite;

        $ArrWords = explode(' ', self::$Data);
        $NumWords = count($ArrWords);
        $NewWords = implode(' ', array_slice($ArrWords, 0, self::$Format));

        $Pointer = (empty($Pointer) ? null : $Pointer);
        return (self::$Format < $NumWords) ? $NewWords . $Pointer : $String;
    }

    /**
     * <b>LoginWall</b>
     * Uma chegagem para criar barreiras em acesso restritos.
     * @param $WallLevel
     * @return BOOLEAN
     */
    public static function LoginWall($WallLevel)
    {
        if ($WallLevel > $_SESSION['userlogin']['user_level']) {
            return false;
        }

        return true;
    }

    /**
     * <b>setFirstUpper</b>
     * Retorna a string com as primeiras letras de cada palavra em maiúsculo.
     * @param $Strign
     * @return STRING
     */
    public static function setFirstUpper($Strign)
    {
        return ucwords(mb_strtolower($Strign, 'utf-8'));
    }

    /**
     * <b>purchaseDate</b>
     * Retorna a string com as data pronta para entrar no banco de dados na tabela Purchase.
     * @param $date
     * @return STRING
     */
    public static function purchaseDate($date)
    {
        return date('d-M-Y', strtotime($date));
    }


    /**
     * Metodo para formatar o DESCRIPTION_JSON do campo da Tabela UAC_LOG
     * @param string $description
     * @return array $return
     */
    public static function formatDescriptionJsonLog($creator, $description)
    {
        if ($creator == 'Plannexo 1.0') {
            return self::formatDataLog(self::formatLogDescriptionScriptCase($description));
        }

        return self::formatDataLog(self::formatLogDescriptionPlannexo($description));
    }

    /**
     * @param $description
     * @return array
     */
    public static function formatLogDescriptionPlannexo($description)
    {
        $old = $new = $column = [];

        $description = json_decode($description);
        foreach ($description->fields as $field) {
            $old[] = trim($field->old);
            $new[] = trim($field->new);
            $column[] = trim($field->label);
        }

        return [$column, $old, $new];
    }

    /**
     * @param $description
     * @return array
     */
    public static function formatLogDescriptionScriptCase($description)
    {
        $fields = explode('fields <--', $description);
        $logs = explode('||', $fields[1]);
        $old = $new = $column = $return = [];
        foreach ($logs as $key => $value) {
            $value = explode(':', $value);
            if ($key % 2 == 0) {
                $column[] = explode(' (old)', $value[0])[0];
                $old[] = trim($value[1]);
            } else {
                $new[] = trim($value[1]);
            }
        }

        return [$column, $old, $new];
    }

    /**
     * @param $log
     * @return array
     */
    public static function formatDataLog($log)
    {
        return [
            'ALTERED_COLUMN' => implode(PHP_EOL, $log[0]),
            'OLD_VALUE' => implode(PHP_EOL, $log[1]),
            'NEW_VALUE' => implode(PHP_EOL, $log[2])
        ];
    }

    /**
     * @param $total
     * @param $number
     * @return float|int
     */
    public static function getPercentage($total, $number)
    {
        if ($total > 0) {
            return round($number / ($total / 100));
        }
        return 0;
    }

    public static function formatCsvFields($key, $value) {
        switch (strtoupper($key)) {
            case 'SIT_SKU':
            case 'SIT_SKU_ERP':
                return self::formatStatus($value);

            case 'INV_LEVEL':
                return self::formatInvLevel($value);

            case 'SIT_ANALYSED':
            case 'SIT_SAVED':
            case 'SIT_URGENT':
                return self::formatSituation($value);

            case 'OUT_WEEK_MONTH_PERCENT_AVG':
                return self::formatPercentage($value);

            default:
                return self::dataFormatter($key, $value);
        }
    }

    public function formatStatus($value)
    {
        global $lang;

        $status = ($value == 1.00 || $value == 1) ? 'ativo' : 'inativo';
        return $lang['status'][$status];
    }

    public function formatInvLevel($value)
    {
        global $lang;

        switch (strtoupper($value)) {
            case "0":
            case 'INV_LEVEL_0.PNG':
                return $lang['sinalizador']['zero'];
                break;

            case "1":
            case 'INV_LEVEL_1.PNG':
                return $lang['sinalizador']['muito_baixo'];
                break;

            case "2":
            case "INV_LEVEL_2.PNG":
                return $lang['sinalizador']['baixo'];
                break;

            case "3":
            case "INV_LEVEL_3.PNG":
                return $lang['sinalizador']['alvo'];
                break;

            case "4":
            case "INV_LEVEL_4.PNG":
                return $lang['sinalizador']['alto'];
                break;

            case "5":
            case "INV_LEVEL_5.PNG":
                return $lang['sinalizador']['muito_alto'];
                break;

            default:
                return $lang['sinalizador']['desconhecido'];
                break;
        }
    }

    public function formatSituation($value)
    {
        global $lang;

        switch ($value) {
            case "0":
            case 0:
                return $lang['situacao']['nao'];
                break;

            case "1":
            case 1:
                return $lang['situacao']['sim'];
                break;

            default:
                return $value;
                break;
        }
    }

    public static function formatFloat($value)
    {
        $parse_float = explode('.', $value);
        if(isset($parse_float[1])){
            return floor($value*100)/100;
        }
        return $value;
    }

    public function dateFormatDmY($Value)
    {
        $new_value = "-";
        $Datetime = strtotime($Value);
        if ($Datetime) {
            $new_value = date('d/m/Y', $Datetime);
        }
        return $new_value;
    }

    public function dateTimeFormat($Value)
    {
        $date = new DateTime($Value);
        return $date->format('d/m/Y');
    }

    public function formateDateAdded($Value)
    {
        $Day = substr($Value, 0, 2);
        $Month = substr($Value, 2, 2);
        $Year = substr($Value, 4, 4);
        return "{$Day}/{$Month}/{$Year}";
    }

    public function roundZero($Value)
    {
        return round($Value, 0);
    }

    public function ComaForDecimals($value)
    {
        return number_format($value, 2, ',', '');
    }

    public function getRiskSuplier($Value)
    {
        if ($Value < 0) {
            return 0;
        }
        return $Value;
    }

    public function dispMovStatus($Value)
    {
        global $lang;

        if ($Value == '0') {
            return $lang['dispmov'];
        }
        return $Value;
    }

    public function formatPercentage($percentage)
    {
        return number_format($percentage, 2, '.', '')."%";
    }

    public function formatSuggestStatus($Value)
    {
        global $lang;

        return $lang['aprovacoes']['status'][$Value];
    }
}
