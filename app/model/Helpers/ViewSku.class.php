<?php


class ViewSku
{
    public static function viewImg($value, $src)
    {
        return sprintf(
            '<td>%s</td>',
            (!isset($value)) ? $src : sprintf('<img src="/resources/img/%s">', $value)
        );
    }

    public static function defaultView($colunm, $value)
    {
        switch (strtolower($colunm)) {
            case 'sit_sku':
                return self::statusSku((int)$value);
                break;
            case 'desc_std_unit':
            case 'desc_item':
            case 'cod_profile':
            case 'cod_group2_fk':
            case 'cod_group3_fk':
            case 'cod_group4_fk':
            case 'cod_group6_fk':
                return self::skinTooltip($value);
                break;
            case 'id_calendar_fk':
                return self::calendar($value);
                break;
            case 'v_qty_max_in':
            case 'v_qty_min_in':
            global $lang;
                return ($value == -1 ? self::default($lang['global']['automatic']) : self::default($lang['global']['manual']));
                break;
            case 'qty_mult_order':
                if ($value < 1 && $value > 0) {
                    $value = '0'.$value;
                }
                return self::default($value);
                break;
            default:
                return self::default($value);
                break;
        }
    }

    private static function default($value)
    {
        return sprintf('<td>%s</td>', $value);
    }

    private static function statusSku($value)
    {
        return sprintf(
            '<td><span class="text-%s">%s</span></td>',
            ($value === 1) ? 'success' : 'danger',
            ($value === 1) ? 'Ativo' : 'Inativo'
        );
    }

    private static function skinTooltip($value)
    {
        $element_params = View::genericWidthTable($value);

        return sprintf(
            '<td class="%s"><span data-toggle="%s" title="%s">%s</span></td>',
            $element_params['td_class'],
            $element_params['data_toggle'],
            $element_params['title'],
            $value
        );
    }

    private static function calendar($value)
    {
        $element_params = View::genericWidthTable($value);

        return sprintf(
            '<td class="%s">
                <span data-toggle="%s" title="%s">%s</span>
            </td>',
            $element_params['td_class'],
            $element_params['data_toggle'],
            $element_params['title'],
            $value
        );
    }
}
