<?php


class Benchmarking
{
    private $timeStart;
    private $timeEnd;
    private $benchmark;
    private $className;

    /**
     * @param mixed $timeStart
     * @return Benchmarking
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;
        return $this;
    }

    /**
     * @param mixed $timeEnd
     * @return Benchmarking
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;
        return $this;
    }

    /**
     * @param string $className
     * @return Benchmarking
     */
    public function setClassName($className)
    {
        $this->className = $className;
        return $this;
    }

    /**
     * @return Benchmarking
     */
    public function benchmark()
    {
        $this->benchmark = $this->timeEnd - $this->timeStart;
        Logger::debug(
            $this->className,
            [
                'benchmark_trace' => sprintf('O Carregamento levou: %d microsecondos', $this->benchmark)
            ]
        );
    }
}
