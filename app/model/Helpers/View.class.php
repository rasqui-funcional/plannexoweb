<?php


class View
{
    public static function modalPurchaseOrder($params, $value)
    {
        global $lang;
        $highlightClass = ($params['SIT_URGENT'] == 1) ? "pln-background-red" : null;
        $tooltip = '';
        $bluePoint = '';
        $hideTooltip = ($params['GROUPED_INSTALLMENTS'] == 1) ? "d-none" : null;

        $valid7DaysDiff = self::valid7DaysDiff($params['DATE_ARRIVAL_ALTERED'], $params['ID_PSUG_PK']);
        if ($params['SIT_URGENT'] == 1 && $params['NUM_PARC_PK'] <= 2 && $valid7DaysDiff) {
            $tooltip = '<div class="pln-groupable-tooltip align-items-center tooltiptext '.$hideTooltip.'">
                    <button type="button" data-value="'.$params['ID_PSUG_PK'].'" data-sku="'.$params['ID_SKU_PK'].'" class="open-modal-groupable btn pln-btn-ghost d-xl-inline-block groupable-btn groupable" data-toggle="modal" data-target="#modal_groupable">
                      '.$lang['suggest']['group_suggests_1_2'].'
                    </button>
                    <span class="open-modal-groupable pln-link-color pln-cur-pointer ml-3 d-xl-inline-block more_info" data-toggle="modal" data-target="#modal_groupable">                        
                      '.$lang['suggest']['more_info'].'
                    </span>
                    </div>';
            $bluePoint = '<div class="pln-groupable-bluepoint'.$hideTooltip.'"></div>';
        } 
          
        $modal = sprintf(
            '<div class="pln-tooltip"><span class="badge badge-secondary td-data %s">%s</span>%s%s</div>'
            , $highlightClass,
            $value,
            $bluePoint,
            $tooltip
        );

        //Comentando código funcao ja exixtente em outra coluna (PURCHASE_ORDER)

        // if ($params['PURCHASE_ORDER'] > 0) {
        //     $modal = sprintf(
        //         '<span data-id="%s" data-toggle="modal" data-target="#modal_order" class="badge badge-secondary pln-cur-pointer %s">%s</span>',
        //         $params['ID_SKU_PK'],
        //         $highlightClass,
        //         $value
        //     );
        // }

        return $modal;
    }

    public static function modalOrder($params, $value)
    {
        if ($value < 1) {
            return $value;
        }

        $late_order = !($params['DATE_NEXT_IN'] && strtotime($params['DATE_NEXT_IN']) < time()) ?: 'pln-icon-color';
        return sprintf(
            '<span data-id="%s" data-toggle="modal" data-target="#modal_order" class="%s">%s</span>',
            $params['ID_SKU_PK'],
            "pln-cur-pointer {$late_order}",
            Utilities::bigNumber($value)
        );
    }

    public static function stock($params, $value, $change, $tilte = '')
    {
        $tooltip = 'data-toggle="tooltip" data-original-title="'.$tilte.'"';

        $str = sprintf(
            '<td><span class="pln-cur-pointer pln-link-color tableSug_td" data-id-sku="%s" 
                               data-change="%s" %s>%s</span></td>',
            $params['ID_SKU_PK'],
            $change,
            $tooltip,
            $value
        );

        if ($value === 0) {
            $str = sprintf(
                '<td><span %s>%s</span></td>',
                $tooltip,
                $value
            );
        }

        return $str;
    }

    public static function pradonizationValues($key, $value, $ViewData, $lang, $extra = null)
    {
        $content = null;
        global $lang;
        switch ($key) {
            case 'TOTAL_PARCELAS':
                $content = sprintf(
                    "<a class='total-parc' href='/%s/approvals/suggest/list/%s'>%s</a>",
                    $ViewData['user']['lang'],
                    $extra,
                    $value
                );
                break;

            case 'YN_URGENT':
                $content = $lang['aprovacoes'][$value];
                break;

            case 'SIT_HEADER':
                $content = $lang['aprovacoes']['status'][$value];
                break;

            case 'DT_EXP_REQ':
                $content = Utilities::dataFormatter('DT_EXP_REQ', $value);
                break;

            case 'DATE_APPROVED':
                $content = Utilities::dataFormatter('DATE_APPROVED', $value);
                break;

            case 'QTY_ORIGINAL_VAL':
                $format = Utilities::dataFormatter('QTY_ORIGINAL_VAL', $value);
                $content =  "{$lang['global']['country_currency']} {$format}";
                break;

            case 'QTY_ALTERED_VAL':
                $format = Utilities::dataFormatter('QTY_ALTERED_VAL', $value);
                $content = "{$lang['global']['country_currency']} {$format}";
                break;

            default:
                $content = $value;
                break;
        }

        return $content;
    }

    public static function genericWidthTable($value)
    {
        $data['td_class'] = 'text-nowrap';
        $data['data_toggle'] = '';
        $data['title'] = '';

        if (!is_numeric($value) && mb_strlen($value) > 12) {
            $data['td_class'] = 'text-truncate pln-td-mw-100';
            $data['data_toggle'] = 'tooltip';
            $data['title'] = $value;
        }

        return $data;
    }

    public static function optionsGenerate($items)
    {
        $aux = 1;
        $options = '';
        foreach ($items as $key => $value) {
            $options .= sprintf(
                '<option %s value="%s">%s</option>',
                $aux == 1 ? 'selected' : '',
                strtoupper($key),
                $value
            );

            $aux++;
        }

        return $options;
    }

    public static function dinamicFilters($groups, $itemsGroups)
    {
        global $lang;

        $output = "";
        foreach($groups as $key => $group) {
            $output .= sprintf('<div class="col-sm-4">
                                            <div class="element-box min-box-height-l5">
                                                <fieldset class="form-group">
                                                    <legend>
                                                        <span onclick="%s" class="pln-cur-pointer">
                                                            %s <i class="fa fa-search pln-link-color"></i>
                                                        </span>
                                                    </legend>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <input type="text" placeholder="filtrar %s" class="form-control pln-invi mb-1" id="ipt_find_%s" onkeyup="%s">
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <input type="checkbox" class="chk_check_all" id="chk_check_all_%s"> <label for="chk_check_all_%s">%s</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12 pln-content-scroll" id="div_filter_%s">
                                                            %s
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>',
                "hide_filter('ipt_find_{$key}')",
                $group,
                $group,
                $key,
                "search_filter('ipt_find_{$key}', 'div_filter_{$key}')",
                $key,
                $key,
                $lang['parametros']['todos'] ,
                $key,
                self::dinamicFiltersItems($itemsGroups["Group_{$key}"], $key)
            );
        }

        return $output;
    }

    private static function dinamicFiltersItems($itemsGroups, $key)
    {
        global $lang;

        $output = "";
        foreach ($itemsGroups as $key2 => $value) {
            $output .= sprintf(
                '<div class="d-flex %s flex-row text-nowrap align-items-center">
                            <input type="checkbox" name="cod_group%s_fk_filtro[]" id="cod_group%s_fk_filtro_%s" value="%s">
                            <label class="text-truncate pl-1" for="cod_group%s_fk_filtro_%s">%s</label>
                        </div>',
                ($key2 == 0) ? "mt-2" : null,
                $key,
                $key,
                $value["COD_GROUP_PK"],
                $value["COD_GROUP_PK"],
                $key,
                $value["COD_GROUP_PK"],
                $value["DESC_GROUP_CONT"]
            );
        }

        return $output;
    }

    private static function valid7DaysDiff($date, $id_psug)
    {
        return (new SuggestService(null))->validSevenDayDiff($date, $id_psug);
    }
}
