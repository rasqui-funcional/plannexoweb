<?php

/**
 * Login
 * Responsável por autenticar, validar, e checar usuário do sistema de login!
 *
 * @copyright (c) 2016, Alef Alves Lima AIR PUBLIC
 */
class Login
{
    /**
     * Constrói a classe informando:
     * @param INT $Level : Nível de acesso do usuário.
     */
    const SESSION_SUFIX = ':session';

    public static function check()
    {
        define('USERSESSIONID', Login::getSessionID());

        $session = Login::getUserSession();
        if (!$session) {
            Request::redirect('error/440');
        }

        define(USERID, $session['user_id']);

        self::renewLogin();
    }

    public static function getSessionID()
    {
        $session_id = session_id();
        if ($session_id == '') {
            $session_id = uniqid();
        }

        if (Redis::Get($session_id  . self::SESSION_SUFIX)) {

            return Redis::Get($session_id  . self::SESSION_SUFIX);
        }

        return $session_id . self::SESSION_SUFIX;
    }

    public static function getUserID()
    {
        return USERID;
    }

    public static function setUserSessionId($session_id)
    {
        $session_unique_id = session_id();
        return Redis::Set($session_unique_id . self::SESSION_SUFIX, $session_id);
    }

    public static function getUserSessionId()
    {
        return USERSESSIONID;
    }

    public static function getUserSession()
    {
        return Redis::HGet(Login::getSessionID() . self::SESSION_SUFIX);
    }

    public static function loadTagManager()
    {
        $session = Redis::HGet(Login::getSessionID() . self::SESSION_SUFIX);
        $company_id = self::handleIDSessionData($session['id_company_pk']);
        $user_id = self::handleIDSessionData($session['user_id']);


        $value = "dataLayer = [{
            generated_id: '" . self::buildUserID($company_id, $user_id) . "',
            lang: '{$session['lang']}',
            id_company_pk: {$company_id},
            company_name: '{$session['company_name']}',
            user_id: {$user_id},
            name: '{$session['name']}',
            email: '{$session['email']}',
            profile: '{$session['profile']}',
            user_profiles: " . self::setProfiles($session['user_profiles']) . ", 
            created_at: '{$session['created_at']}',
            ip: '{$session['ip']}'
        }]; ";

        return $value;
    }

    private static function handleIDSessionData($data)
    {
        return is_null($data) ? 0 : $data;
    }

    public static function getUserName()
    {
        return self::getUserSession()['name'];
    }

    public static function getUserProfile()
    {
        return Login::getUserSession()['profile'];
    }

    public static function getUserByKey($key)
    {
        $sess = Login::getUserSession();
        return key_exists($key, $sess) ? $sess[$key] : null;
    }
    
    public static function getUserSessionByKey($key)
    {
        $session = self::getUserSession();
        return (array_key_exists($key, $session)) ? $session[$key] : null;
    }

    public static function getUserSessionByKeys(array $keys)
    {
        $session = self::getUserSession();

        $response = [];
        foreach ($keys as $key) {
            if (array_key_exists($key, $session)) {
                $response[$key] = $session[$key];
            }
        }

        return $response;
    }

    public static function renewLogin()
    {
        $redis = Redis::GetConn();
        $redis->expire(USERSESSIONID . self::SESSION_SUFIX, 86400);
    }

    public static function bypassLogin($view)
    {
        switch ($view) {
            case 'login':
            case 'getlogin':
            case 'formlogin':
            case 'health':
            case 'error':
            case 'setsession':
                return true;

            default:
                return false;
        }
    }

    public static function simulateLogin()
    {
        $userData = [
            'lang' => 'br',
            'id_company_pk' => 2,
            'user_id' => 1,
            'name' => 'João das Neves'
        ];

        define(USERID, $userData['user_id']);
        define(USERSESSIONID, Login::getSessionID());

        return Redis::HSet(Login::getSessionID() . self::SESSION_SUFIX, $userData, null, 86400);
    }

    private static function setProfiles($profiles)
    {
        $data = array_filter(
            explode(', ', $profiles),
            function ($param) {
                return $param !== '';
            }
        );

        return json_encode($data);
    }

    private static function buildUserID($company_id, $user_id)
    {
        $company_id_with_4_characters = str_pad(
            $company_id,
            4,
            '0',
            STR_PAD_LEFT
        );

        $user_id_with_five_characters = str_pad(
            $user_id,
            5,
            '0',
            STR_PAD_LEFT
        );

        return $company_id_with_4_characters . $user_id_with_five_characters;
    }
}
