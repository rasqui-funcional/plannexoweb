<?php

/**
 * Politics
 * Responsável por validar e complementar informações relacionadas à Políticas
 *
 * @copyright (c) 2019, Wallace Randal, Bionexo
 */
class Politics
{
    /**
     * Informa se a política informada está relacionada à uma àrvore de decisão
     * @param INT $politicsId : id da Política(ID_PROFILE_PK)
     * @param INT $companyId
     * @return BOOL
     */
    public static function profile($politicsId, $companyId)
    {
        $table = "SCM_DECISION_TREE";
        $columns = "ID_PROFILE_FK";
        $terms = "WHERE ID_PROFILE_FK = $politicsId AND ID_COMPANY_FK = $companyId GROUP BY ID_PROFILE_FK";
        $result = DB::select($table, $columns, $terms);
        return $result;
    }
}
