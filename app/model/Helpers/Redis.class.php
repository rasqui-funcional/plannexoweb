<?php

/**
 * <b>DB</b>
 * Helper para auxuliar a declaração das classes de conexões.
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class Redis
{
    /**
     * <b>Get</b>
     * Returns a value of specific key.
     *
     * @param STRING $Key key seted in Redis.
     * @return STRING
     */
    public static function Get($Key)
    {
        $Redis = new FunctionsRedis();
        $Redis->Get($Key);
        return $Redis->getResult();
    }

    /**
     * <b>Set</b>
     * Write a value of specific key in Redis.
     *
     * @param STRING $Key key in Redis.
     * @param STRING $Content contents to be saved in Redis.
     * @param NULL/BOOLEAN/INT $Expire when isn`t used are set a standard time for expire, FALSE you persist the key
     * and if you want an specific time to expire use an INT.
     * @return BOOLEAN
     */
    public static function Set($Key, $Content, $Expire = null)
    {
        $Redis = new FunctionsRedis();
        $Redis->Set($Key, $Content, $Expire);
        return $Redis->getResult();
    }

    /**
     * <b>Del</b>
     * Deletes a key and your value of specific key.
     *
     * @param STRING $Key key seted in Redis.
     * @return BOOLEAN
     */
    public static function Del($Key)
    {
        $Redis = new FunctionsRedis();
        $Redis->Delete($Key);
        return $Redis->getResult();
    }

    /**
     * <b>HSet</b>
     * Write a hash of specific key in Redis.
     *
     * @param STRING $Key key in Redis.
     * @param ARRAY/STRING $ArrayorSubKey contents to be saved in Redis.
     * @param STRING $ContentSubKey when isn`t used the $ArrayorSubKey need to be an array with key and value,
     * otherwise pass the content of an subkey in the hash.
     * @return BOOLEAN
     */
    public static function HSet($Key, $ArrayorSubKey, $ContentSubKey = null, $Expire = null)
    {
        $Hashes = new FunctionsRedis();
        $Hashes->HSet($Key, $ArrayorSubKey, $ContentSubKey, $Expire);
        return $Hashes->getResult();
    }

    /**
     * <b>HGet</b>
     * Returns the value a hash of specific key and can be used if an subkey.
     *
     * @param STRING $Key key in Redis.
     * @param STRING $Field if used returns the value of an subkey.
     * @return STRING
     */
    public static function HGet($Key, $Field = null)
    {
        $Hashes = new FunctionsRedis();
        $Hashes->HGet($Key, $Field);
        return $Hashes->getResult();
    }

    /**
     * <b>Incr</b>
     * Increase a value of an specific key.
     *
     * If you don`t specifie the $Counter will increse one more.
     *
     * @param STRING $Key key in Redis.
     * @param INT $Counter increase the values of the counter key by larger integer values.
     * @return BOOLEAN
     */
    public static function Incr($Key, $Counter = null)
    {
        $Redis = new FunctionsRedis();
        $Redis->Increment($Key, $Counter);
        return $Redis->getResult();
    }

    /**
     * <b>Decr</b>
     * Decrease a value of an specific key.
     *
     * If you don`t specifie the $Counter will decrese one more.
     *
     * @param STRING $Key key in Redis.
     * @param INT $Counter decrease the values of the counter key by larger integer values.
     * @return BOOLEAN
     */
    public static function Decr($Key, $Counter = null)
    {
        $Redis = new FunctionsRedis();
        $Redis->Decrement($Key, $Counter);
        return $Redis->getResult();
    }

    /**
     * <b>GetConn</b>
     * Return the connection object for personalized uses of Redis.
     *
     * @return OBJECT
     */
    public static function GetConn()
    {
        $Redis = new FunctionsRedis();
        return $Redis->getConnection();
    }
}
