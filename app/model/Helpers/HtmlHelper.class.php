<?php


class HtmlHelper
{
    private static $regex = "/group1|group2|group3|group4|group5|group6|group7|group8|group9|group10|group11|group12/";
    private static $key = "";
    private static $value = "";
    private static $lang = [];


    public static function getTextAndOrderTable($key, $value, $lang): stdClass
    {
        self::$key = $key;
        self::$value = $value;
        self::$lang = $lang;

        $obj = new stdClass();
        $obj->title = self::getTitle() ?? self::getGroupTitle();
        $obj->abrev = self::getAbbreviation() ?? self::getGroupAbbrev();
        $obj->order = self::getOrder();

        return $obj;
    }

    private static function getTitle()
    {
        return self::$value['name']
            ? self::$lang['capa_pedido_lista'][strtolower(self::$value['name'])]
            : self::$lang['column'][strtolower(self::$key)];
    }

    private static function getAbbreviation()
    {
        return self::$value['name']
            ? self::$lang['capa_pedido_lista'][strtolower(self::$value['name'])]
            : self::$lang['column']['abrev'][strtolower(self::$key)];
    }

    private static function getOrder()
    {
        return is_null(self::$value['name']) ? "pln-cur-pointer" : false;
    }

    private static function getGroupTitle()
    {
        return preg_match(self::$regex, self::$key, $match)
            ? self::$lang['column'][strtolower(self::$key)]
            : '';
    }

    private static function getGroupAbbrev()
    {
        return (preg_match(self::$regex, self::$key, $match))
            ? self::$value
            : '';
    }
}
