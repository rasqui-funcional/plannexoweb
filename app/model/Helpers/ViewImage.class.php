<?php


class ViewImage
{
    public static function invLevelImg($level, $lang, array $extra = [])
    {
        $format = "<td%s>
            <span class='pln-cur-pointer' data-toggle='tooltip' title='%s'>
                N/A
            </span>
        </td>";

        if (!is_null($level)
            && file_exists(realpath(__DIR__ . '/../../resources/img') . "/INV_LEVEL_{$level}.png")) {
            $format = "<td%s>
                            <span class='pln-cur-pointer' data-toggle='tooltip' title='%s'>
                                <img src='/resources/img/INV_LEVEL_%s.png'>                            
                            </span>
                       </td>";
        }

        return sprintf($format, self::mounthAttributes($extra), self::setInvLevel($lang, $level), $level);
    }

    public static function indExcessImg($id_sug, $value)
    {
        $format = "<td><img src='/resources/img/IND_EXCESS_%s.png'></td>";
        $format_modal = "<td><img src='/resources/img/IND_EXCESS_%s.png' data-id='%s' data-toggle='modal' 
                            data-target='#modal_excess' class='pln-cur-pointer ind_excess_%s'></td>";

        return ($value == 1) ? sprintf($format_modal, $value, $id_sug, $value) : sprintf($format, $value);
    }

    private function setInvLevel($lang, $value): string
    {
        return $lang['inv_level'][$value];
    }

    private static function mounthAttributes(array $attributes = []): string
    {
        $strResult = "";
        if (count($attributes) < 1) {
            return $strResult;
        }

        foreach($attributes as $key => $value) {
            $strResult .= sprintf(" %s='%s'", $key, $value);
        }

        return $strResult;
    }
}
