<?php

/**
 * <b>DB</b>
 * Helper para auxuliar a declaração das classes de conexões.
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class DB
{
    private $DB;

    public static function getTableCols($Table)
    {
        $Cols = self::setColsTables();
        return $Cols[$Table];
    }

    private function setColsTables()
    {
        #$Table['vw_scr_scm420g_sug'] = "sit_analysed, sit_saved, sit_urgent, id_psug_pk, num_parc_pk, ind_excess, cod_item_pk, desc_item, cod_estab_fk, desc_std_unit, qty_original, parc_days, qty_altered, qty_mult_order, date_arrival_altered, out_week_month_percent_avg, total_out_last_month_avg, total_out_last_half_month_avg, total_out_last_week_avg,total_out_last_week, out_last_30d, val_altered, risk_supplier, default_local, obs, inv_level, id_sku_pk, val_unit_erp, qty_forecast, inv_available, available_money, qty_min_out, qty_target, qty_max_out, qty_avg_cons_erp, avg_3m, avg_6m, avg_12m, out_yesterday, out_yesterday_num_dev, days_no_out, out_month_minus12, out_month_minus11, out_month_minus10, out_month_minus9, out_month_minus8, out_month_minus7, out_month_minus6, out_month_minus5, out_month_minus4, out_month_minus3, out_month_minus2, out_month_minus1, out_current_month, orders_next_in, reqs_next_in, curve_abc, curve_pqr, curve_xyz, curve_123, out1, out2, out3, out4, out5, out6, out7, out8, out9, out10, out11, out12, qty_min_out_ori, qty_pp, qty_max_out_ori, qty_excess, vl_excess, qty_max_erp, qty_min_erp, id_profile, id_calendar_fk, date_added, date_created, date_arrival_original, id_user_created, qty_pp_erp, qty_final, date_next_in, qty_next_in, date_last_out, qty_last_out, date_last_in, qty_last_in, inv_total, inv_days, total_money, purchase_req, purchase_order, cod_group1_fk, cod_group2_fk, cod_group3_fk, cod_group4_fk, cod_group5_fk, cod_group6_fk, cod_group7_fk, cod_group8_fk, cod_group9_fk, cod_group10_fk, cod_group11_fk, cod_group12_fk, last_purchase_price, desc_supplier, last_purchase_date, last_purchase_qty, lt_supplier_out, lt_others_out, lt_buyer_out, lt_total_out, qty_min_order, service_level_target, service_level_real, date_mature, sku_life_cycle, user_id, date_last_calc, sit_pending_calc, v_qty_cons, v_date_cons_expiration, v_lt_planner_in, v_lt_purchase_in, v_lt_supplier_in, v_lt_receiving_in, v_lt_others_in, v_qty_min_in, v_qty_max_in, v_qty_max_restr, v_cons_profile, v_id_depend_sku, v_order_min, v_order_mult, v_in1, v_in2, v_in3, v_in4, v_in5, v_in6, v_in7, v_in8, v_in9, v_in10, v_in11, v_in12";
        $Table['vw_scr_scm420g_sug'] = "sit_analysed, sit_saved, sit_urgent, id_psug_pk, num_parc_pk, ind_excess, cod_item_pk, desc_item, cod_estab_fk, desc_std_unit, qty_original, parc_days, qty_altered, qty_mult_order, date_arrival_altered, out_week_month_percent_avg, total_out_last_month_avg, total_out_last_half_month_avg, total_out_last_week_avg,total_out_last_week, out_last_30d, val_altered, risk_supplier, default_local, obs, inv_level, id_sku_pk, val_unit_erp, qty_forecast, inv_available, available_money, qty_min_out, qty_target, qty_max_out, qty_avg_cons_erp, avg_3m, avg_6m, avg_12m, out_yesterday, out_yesterday_num_dev, days_no_out, out_month_minus12, out_month_minus11, out_month_minus10, out_month_minus9, out_month_minus8, out_month_minus7, out_month_minus6, out_month_minus5, out_month_minus4, out_month_minus3, out_month_minus2, out_month_minus1, out_current_month, reqs_next_in, curve_abc, curve_pqr, curve_xyz, curve_123, out1, out2, out3, out4, out5, out6, out7, out8, out9, out10, out11, out12, qty_min_out_ori, qty_pp, qty_max_out_ori, qty_excess, vl_excess, qty_max_erp, qty_min_erp, id_profile, id_calendar_fk, date_added, date_created, date_arrival_original, id_user_created, qty_pp_erp, qty_final, date_next_in, qty_next_in, date_last_out, qty_last_out, date_last_in, qty_last_in, inv_total, inv_days, total_money, purchase_req, orders_next_in, cod_group1_fk, cod_group2_fk, cod_group3_fk, cod_group4_fk, cod_group5_fk, cod_group6_fk, cod_group7_fk, cod_group8_fk, cod_group9_fk, cod_group10_fk, cod_group11_fk, cod_group12_fk, last_purchase_price, last_purchase_supplier, last_purchase_date, last_purchase_qty, lt_supplier_out, lt_others_out, lt_buyer_out, lt_total_out, qty_min_order, service_level_target, service_level_real, date_mature, sku_life_cycle, user_id, date_last_calc, sit_pending_calc, v_qty_cons, v_date_cons_expiration, v_lt_planner_in, v_lt_purchase_in, v_lt_supplier_in, v_lt_receiving_in, v_lt_others_in, v_qty_min_in, v_qty_max_in, v_qty_max_restr, v_cons_profile, v_id_depend_sku, v_order_min, v_order_mult, v_in1, v_in2, v_in3, v_in4, v_in5, v_in6, v_in7, v_in8, v_in9, v_in10, v_in11, v_in12";
        #$Table['vw_scr_scm405g_sku_list'] = "sit_sku, id_sku_pk, ind_excess, cod_item_pk, desc_item, cod_estab_fk, val_unit_erp, desc_std_unit, QTY_MIN_ORDER, QTY_MULT_ORDER, DEFAULT_LOCAL, curve_abc, curve_pqr, curve_xyz, curve_123, cod_profile, ID_CALENDAR_FK, inv_available, qty_inv_origin, inv_available_money, inv_total, inv_total_money, qty_forecast, QTY_FORECAST_R3, INV_DAYS, qty_min_out, QTY_MIN_OUT_ORI, qty_pp, qty_target, qty_max_out, QTY_MAX_OUT_ORI, qty_excess, VL_EXCESS, QTY_EXCESS_NEXT_IN, inv_level, avg_3m, avg_6m, avg_12m, qty_min_erp, qty_max_erp, qty_pp_erp, out_week_month_percent_avg, total_out_last_month_avg, total_out_last_half_month_avg, total_out_last_week_avg, total_out_last_week, qty_avg_cons_erp, out_last_30d, out_yesterday, out_yesterday_num_dev, out_month_minus12, out_month_minus11, out_month_minus10, out_month_minus9, out_month_minus8, out_month_minus7, out_month_minus6, out_month_minus5, out_month_minus4, out_month_minus3, out_month_minus2, out_month_minus1, out_current_month, ORDERS_NEXT_IN, REQS_NEXT_IN, SUG_NEXT_IN, date_next_in, qty_next_in, date_last_in, qty_last_in, date_last_out, qty_last_out, days_no_out, last_purchase_price, last_purchase_supplier, last_purchase_date, last_purchase_qty, last_parc_date, purchase_req, purchase_order, purchase_sugg, purchase_sugg_arr_date, LT_PLANNER_OUT, lt_buyer_out, lt_supplier_out, LT_RECEIVING_OUT, lt_others_out, lt_total_out, LT_ORIGIN_DRP, COD_ESTAB_ORIGIN_DRP_FK, TP_ORIGIN_DRP, out1, out2, out3, out4, out5, out6, out7, out8, out9, out10, out11, out12, service_level_real, service_level_target, cod_group1_fk, cod_group2_fk, cod_group3_fk, cod_group4_fk, cod_group5_fk, cod_group6_fk, cod_group7_fk, cod_group8_fk, cod_group9_fk, cod_group10_fk, cod_group11_fk, cod_group12_fk, sit_sku_erp, user_id, sku_life_cycle, date_mature, date_last_calc, date_added, sit_pending_calc, V_QTY_CONS, V_DATE_CONS_EXPIRATION, V_LT_PLANNER_IN, v_lt_purchase_in, v_lt_supplier_in, V_LT_RECEIVING_IN, V_LT_OTHERS_IN, v_qty_min_in, v_qty_max_in, v_qty_max_restr, V_CONS_PROFILE, V_ID_DEPEND_SKU, V_ORDER_MIN, V_ORDER_MULT, v_in1, v_in2, v_in3, v_in4, v_in5, v_in6, v_in7, v_in8, v_in9, v_in10, v_in11, v_in12 ";
        $Table['vw_scr_scm405g_sku_list'] = "sit_sku, id_sku_pk, ind_excess, cod_item_pk, desc_item, cod_estab_fk, val_unit_erp, desc_std_unit, QTY_MIN_ORDER, QTY_MULT_ORDER, DEFAULT_LOCAL, curve_abc, curve_pqr, curve_xyz, curve_123, cod_profile, ID_CALENDAR_FK, inv_available, qty_inv_origin, inv_available_money, inv_total, inv_total_money, qty_forecast, QTY_FORECAST_R3, INV_DAYS, qty_min_out, QTY_MIN_OUT_ORI, qty_pp, qty_target, qty_max_out, QTY_MAX_OUT_ORI, qty_excess, VL_EXCESS, QTY_EXCESS_NEXT_IN, inv_level, avg_3m, avg_6m, avg_12m, qty_min_erp, qty_max_erp, qty_pp_erp, out_week_month_percent_avg, total_out_last_month_avg, total_out_last_half_month_avg, total_out_last_week_avg,total_out_last_week, qty_avg_cons_erp, out_last_30d, out_yesterday, out_yesterday_num_dev, out_month_minus12, out_month_minus11, out_month_minus10, out_month_minus9, out_month_minus8, out_month_minus7, out_month_minus6, out_month_minus5, out_month_minus4, out_month_minus3, out_month_minus2, out_month_minus1, out_current_month, SUG_NEXT_IN, date_next_in, qty_next_in, date_last_in, qty_last_in, date_last_out, qty_last_out, days_no_out, last_purchase_price, last_purchase_supplier, last_purchase_date, last_purchase_qty, last_parc_date, purchase_req, orders_next_in, purchase_sugg, purchase_sugg_arr_date, LT_PLANNER_OUT, lt_buyer_out, lt_supplier_out, LT_RECEIVING_OUT, lt_others_out, lt_total_out, LT_ORIGIN_DRP, COD_ESTAB_ORIGIN_DRP_FK, TP_ORIGIN_DRP, out1, out2, out3, out4, out5, out6, out7, out8, out9, out10, out11, out12, service_level_real, service_level_target, cod_group1_fk, cod_group2_fk, cod_group3_fk, cod_group4_fk, cod_group5_fk, cod_group6_fk, cod_group7_fk, cod_group8_fk, cod_group9_fk, cod_group10_fk, cod_group11_fk, cod_group12_fk, sit_sku_erp, user_id, sku_life_cycle, date_mature, date_last_calc, date_added, sit_pending_calc, V_QTY_CONS, V_DATE_CONS_EXPIRATION, V_LT_PLANNER_IN, v_lt_purchase_in, v_lt_supplier_in, V_LT_RECEIVING_IN, V_LT_OTHERS_IN, v_qty_min_in, v_qty_max_in, v_qty_max_restr, V_CONS_PROFILE, V_ID_DEPEND_SKU, V_ORDER_MIN, V_ORDER_MULT, v_in1, v_in2, v_in3, v_in4, v_in5, v_in6, v_in7, v_in8, v_in9, v_in10, v_in11, v_in12 ";
        $Table['vw_scr_scm532g_sug_pur_agrup'] = "/*+FIRST_ROWS*/ a.cod_estab_fk, b.desc_estab, count(id_sug_pk) as total_sugs, a.auxiliar1, a.auxiliar2, a.auxiliar3, a.auxiliar4, a.auxiliar5, a.auxiliar6, a.auxiliar7, a.auxiliar8, a.auxiliar9, a.auxiliar10, a.auxiliar11, a.auxiliar12, a.auxiliar18, a.auxiliar19, a.auxiliar22, a.auxiliar20, a.auxiliar21, a.cod_buyer, a.cod_sector, a.cod_req_reason, a.cod_local_fk, sum(qty_altered) as total_qty, sum(val_unit_erp * qty_altered) as total_price, a.id_company_fk, a.default_local, a.psug_grouped_pk";
        $Table['VW_SCR_SCM800G_FOLLOW_UP'] = "/*+FIRST_ROWS*/ count(id_order_pk) as total_orders, a.auxiliar1, a.auxiliar2, a.auxiliar3, a.auxiliar4, a.auxiliar5, a.auxiliar6, a.auxiliar7, a.auxiliar8, a.auxiliar9, a.auxiliar10, a.auxiliar11, a.auxiliar12, a.auxiliar13";

        return $Table;
    }

    /**
     * <b>setFields</b>
     * Configura os dados de um Post para ser imbutido em uma query de Select
     * Essa função elimina valores vazios da query
     */
    public static function setFields($Fields)
    {
        $Places = '';
        foreach ($Fields as $Col => $Values) {
            if (self::notNull($Col) && self::notNull($Values)) {
                $Places .= ' AND ';
                if (is_array($Values)) {
                    $Places .= self::prepareArray($Col, $Values);
                } else {
                    $Values = trim($Values);
                    $Places .= $Col . " LIKE '{$Values}' ";
                }
            }
        }

        return $Places;
    }

    private function notNull($Value)
    {
        if ($Value == '' || is_null($Value)) {
            return false;
        }

        return true;
    }

    private function prepareArray($Col, $Array)
    {
        end($Array);
        $Max = key($Array);

        $PreparePlace = $Col . ' IN (';
        foreach ($Array as $Key => $Value) {
            $Value = trim($Value);
            $PreparePlace .= "'{$Value}'";
            if ($Max != $Key) {
                $PreparePlace .= ', ';
            }
        }

        $PreparePlace .= ')';
        return $PreparePlace;
    }

    /**
     * <b>setColumns</b>
     * Configura array de Colunas para ser injetado em uma Query
     * @param $array
     * @return string
     */
    public static function setColumns($array): string
    {
        return implode(', ', $array);
    }

    /**
     * <b>Select</b>
     * Declara a leitura do banco de dados com base no seguintes parâmetros:
     *
     * @param STRING $Table tabela no qual vai se conectar.
     * Para criar joins passe a string separando os bancos com ','.
     * @param STRING $Offset colunas no qual serão retornadas.
     * @param STRING $Limit colunas no qual serão retornadas.
     * @param STRING $Columns passe as colunas seperando por "," em uma string, ou declare FALSE para retornar todas colunas.
     * @param STRING $Terms criar filtros para obter resultados.
     * @param STRING $ParseString filtros para pegar a leitura.
     * @param BOOLEAN $debug flag para imprimir a query, tem como default o valor `false`
     * @return mixed
     */
    public static function Page($Table, $Offset, $Limit, $Columns = null, $Terms = null, $ParseString = null, $debug = false)
    {
        $Page = new Select();
        $Page->exePagination($Table, $Offset, $Limit, $Columns, $Terms, $ParseString, $debug);

        return $Page->getResult();
    }

    /**
     * <b>Select</b>
     * Declara a leitura do banco de dados com base no seguintes parâmetros:
     *
     * @param STRING $Table tabela no qual vai se conectar.
     * Para criar joins passe a string separando os bancos com ','.
     * @param STRING $Columns passe as colunas seperando por "," em uma string, ou declare FALSE para retornar todas colunas.
     * @param STRING $Terms criar filtros para obter resultados.
     * @param STRING $ParseString filtros para pegar a leitura.
     * @param BOOLEAN $debug flag para imprimir a query, tem como default o valor `false`
     * @return mixed
     */
    public static function Select($Table, $Columns = null, $Terms = null, $ParseString = null, $debug = false)
    {
        $Select = new Select();
        $Select->exeSelect($Table, $Columns, $Terms, $ParseString, $debug);
        return $Select->getResult();
    }

    public static function InsertAll($table, $columns, $data)
    {
        $insert = new Insert();
        $insert->exeInsertAll($table, $columns, $data);

        return $insert->getResult();
    }

    public static function SelectRow($Table, $Columns = null, $Terms = null, $ParseString = null, $debug = false)
    {
        $Select = new Select();
        $Select->exeSelectRow($Table, $Columns, $Terms, $ParseString, $debug);

        return $Select->getResult();
    }

    /**
     * <b>Insert</b>
     * Declara um insert no banco de dados com estrutura pronta.
     * Basta informar nome da Table, e um array atribuitivo com nome da coluna e valor.
     * @param STRING $Table recebe o nome da tabela
     * @param ARRAY $Data recebe um array com coluna e valor
     * @return mixed
     */
    public static function Insert($Table, $Data)
    {
        $Insert = new Insert();
        $Insert->exeInsert($Table, $Data);

        return $Insert->getResult();
    }

    /**
     * <b>Update</b>
     * Declara a atualização do banco de dados com base no seguintes parâmetros:
     *
     * @param STRING $Table tabela no qual vai se conectar.
     * @param ARRAY $Dados = Informe um array ( Nome da Coluna => Valor).
     * @param STRING $Terms criar filtros para criar o update.
     * @param STRING $ParseString filtros para pegar a leitura.
     * @return mixed
     */
    public static function Update($Table, $Data, $Termos, $ParseString)
    {
        $Update = new Update();
        $Update->exeUpdate($Table, $Data, $Termos, $ParseString);

        return $Update->getResult();
    }

    /**
     * <b>Delete</b>
     * Executa um delete do banco de dados com base no seguintes parâmetros:
     *
     * @param STRING $Table tabela no qual vai se conectar.
     * @param STRING $Terms criar filtros para criar o delete.
     * @param STRING $ParseString filtros para pegar a leitura.
     * @return mixed
     */
    public static function Delete($Table, $Terms, $ParseString)
    {
        $Delete = new Delete();
        $Delete->exeDelete($Table, $Terms, $ParseString);

        return $Delete->getResult();
    }

    /**
     * <b>Procedure</b>
     * Declara o procedure do banco de dados com base no seguintes parâmetros:
     *
     * @param STRING $Query query com procedure para obter os resultados.
     * @param STRING $ParseString filtros para pegar a leitura.
     * @param BOOLEAN $Return se TRUE retorna os dados de um select, caso contrário retorna apenas a confirmação do procedure.
     * @param BOOLEAN $debug flag para imprimir a query, tem como default o valor `false`
     * @return mixed
     */
    public static function Procedure($Query, $Return = null, $ParseString = null, $debug = false)
    {
        $Procedure = new Procedure();
        $Procedure->exeProcedure($Query, $ParseString, $debug);

        if ($Return) {
            return $Procedure->returnResult();
        }

        return $Procedure->checkResult();
    }

    /**
     * prepareOrder
     *
     * Receive an array of orders ['column' => 'asc/desc'] and prepare ORDER BY string.
     *
     * @param ARRAY $orders
     * @return STRING
     */
    public static function prepareOrder($orders)
    {
        if (!$orders || $orders == "false") {
            return null;
        }

        end($orders);
        $last_column = key($orders);
        $order_query = " ORDER BY ";
        foreach ($orders as $column => $col_order) {
            $order_query .= "{$column} {$col_order}";
            if ($last_column != $column) {
                $order_query .= ", ";
            }
        }

        return $order_query;
    }
}
