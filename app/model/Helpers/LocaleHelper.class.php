<?php


class LocaleHelper
{
    private static $default_locale = 'pt-BR';

    private static $locales = [
        'br' => 'pt-BR',
        'ar' => 'es-AR',
        'pe' => 'es-PE',
        'co' => 'es-CO',
        'us' => 'en-US'
    ];

    public static function getLocale(string $lang): string
    {
        return key_exists($lang, self::$locales)
            ? self::$locales[$lang]
            : self::$default_locale;
    }
}
