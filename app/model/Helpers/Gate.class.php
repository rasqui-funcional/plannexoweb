<?php

class Gate
{
    /**
     * check
     *
     * Catch the profile in the atual session and check if the user has permission
     *
     * @param STRING $profile
     * @param string $attribute
     * @return BOOLEAN
     */
    public static function check($profile, $attribute = 'profile')
    {
        $user_data = Login::getUserSession();

        return array_key_exists($attribute, $user_data) && $profile == $user_data[$attribute];
    }

    /**
     * open
     *
     * Confirm if the user logged is the user on $profile otherwise redirect to his home
     *
     * @param STRING $profile
     */
    public static function open($profile)
    {
        if (!self::check($profile)) {
            Request::home($profile);
        }
    }

    /**
     * close
     *
     * Confirm if the user logged is the user on $profile, so redirect to his home
     *
     * @param STRING $profile
     */
    public static function close($profile)
    {
        if (self::check($profile)) {
            Request::home($profile);
        }
    }

    /**
     * setSupplier
     *
     * Confirm if the user logged is a supplier
     *
     * @param string $prefix
     * @param array $user
     * @return mixed
     */
    public static function setSupplier($prefix='', $coGroup)
    {
        if (can('supplier')) {
            return " AND {$prefix}cod_group11_fk = '{$coGroup}' ";
        }

        return null;
    }
}
