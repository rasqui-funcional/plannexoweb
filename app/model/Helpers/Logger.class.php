<?php


class Logger
{
    public static function debug($message, $context = [])
    {
        if (is_array($context)) {
            (new AppLogger())->debug($message, $context);
        }
    }

    public static function warning($message, $context = [])
    {
        if (is_array($context)) {
            (new AppLogger())->warning($message, $context);
        }
    }

    public static function error($message, $context = [])
    {
        if (is_array($context)) {
            (new AppLogger())->error($message, $context);
        }
    }

    public static function info($message, $context = [])
    {
        if (is_array($context)) {
            (new AppLogger())->info($message, $context);
        }
    }
}
