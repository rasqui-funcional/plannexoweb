<?php


class ViewOrderCover
{
    public static function mountOptions($count, $attrs, $cod_check)
    {
        global $lang;

        $options = null;                            
        $options = ($count > 1) ? self::defaultOption() : '';

        foreach ($attrs as $attr) {
            $options .= sprintf(
                '<option value="%s" %s>%s</option>',
                $attr['VAL_PARAM'],
                self::isSelected($cod_check, $attr['VAL_PARAM']),
                $attr['DESC_PARAM'] 
            );
        }

        return $options;
    }

    public static function mountOptionsDeposit($count, $attrs, $cod_check)
    {
        global $lang;

        $options = null;
        $options = ($count > 1) ? self::defaultOption() : '';

        foreach ($attrs as $attr) {
            $options .= sprintf(
                '<option value="%s" %s>%s</option>',
                $attr['COD_LOCAL_PK'],
                self::isSelected($cod_check, $attr['COD_LOCAL_PK']),
                $attr['DESC_LOCAL']
            );
        }

        return $options;
    }

    private static function isSelected($cod_check, $val_param)
    {
        return $cod_check == $val_param ? "selected" : "";
    }

    private static function defaultOption()
    {
        global $lang;
        return sprintf('<option value="">%s</option>', $lang['global']['select']);
    }
}
