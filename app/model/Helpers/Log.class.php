<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 01/03/19
 * Time: 15:31
 */

class Log
{
    /**
     * @param $key Hash de comunicacao com o Redis
     * @param $type Tipo de identificador
     * @param $application Qual setor na aplicacao cedeu a alteraca
     * @param $action Ação corresponde da aplicacao do item acima $application
     * @param null $id Nao obrigatorio mas corresponde ao valor de acordo com o $type
     * @param array $data Contem os dados que vão sofre alteracoes
     * @return bool Retorna um booleano sendo True como sucessoe Falso como erro
     */
    public static function new($key, $type, $application, $action, $id = null, array $data)
    {
        $auditController = new AuditController();
        $create = $auditController->create($key, $type, $application, $action, $id, $data);

        return $create;
    }
}
