<?php /** @noinspection SpellCheckingInspection */

/**
 * Date.class [HELPERS]
 * Classe responsável por manipular e validar dados de data!
 * @author Wallace Randal de Moura
 * @copyright 2019 The Bionexo
 */
require("resources/lang/".COUNTRY.".php");

class Date
{
    private static $range = [];

    /***
     * <b>formatMonth</b>
     * Traduz o mês em número para nome(ex.: 01 = Janeiro, 02 = Fevereiro)
     *
     * @param string $month
     * @return string
     */
    public static function formatMonth($month)
    {
        global $lang;
        $month = ((int) $month)-1;

        if (isset($lang['months']['name'][$month])) {
            return $lang['months']['name'][$month];
        }
        return $month;
    }

    public static function firstThreeCharactersMonth($month)
    {
        return substr($month, 0, 3);
    }

    public static function createRangeDate($days = 7, $before = false, $format = 'd/m/Y')
    {
        $start = new datetimeImmutable('now');

        $position = ($before === true) ? '-' : '+';
        $modify = $start->modify("{$position}{$days} day");

        self::$range['start'] = $start->format("{$format}");
        self::$range['end'] = $modify->format("{$format}");

        if ($before === true) {
            self::$range['start'] = $modify->format("{$format}");
            self::$range['end'] = $start->format("{$format}");
        }

        return self::$range;
    }

    public static function translateDate($date, $hour = '')
    {
        $format = trim("d/m/Y {$hour}");
        $datetime = strtotime($date);
        if ($datetime) {
            return date($format, $datetime);
        }

        return '-';
    }
}
