<?php

/**
 * <b>BasicRedis.class:</b>
 * Class responsable for basics of read and write in Redis.
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class FunctionsRedis extends ConnRedis
{
    private $Result;

    /** @var Predis */
    private $RedisConn;

    public function __construct()
    {
        if (!$this->RedisConn) {
            $this->RedisConn = parent::getConnRedis();
        }
    }

    /**
     * <b>Set</b>
     * Write a value of specific key in Redis.
     *
     * @param STRING $Key key in Redis.
     * @param STRING $Content contents to be saved in Redis.
     * @param NULL/BOOLEAN/INT $Expire when isn`t used are set a standard time for expire, FALSE you persist the key
     * and if you want an specific time to expire use an INT.
     * @return BOOLEAN
     */
    public function Set($Key, $Content, $Expire = null)
    {
        $this->RedisConn->set($Key, $Content);
        parent::setExpire($Key, $Expire);
        $this->Result = true;
    }

    /**
     * <b>Get</b>
     * Returns a value of specific key.
     *
     * @param STRING $Key key seted in Redis.
     * @return STRING
     */
    public function Get($Key)
    {
        if (!$this->RedisConn->exists($Key)) {
            $this->Result = false;
        } else {
            $this->Result = $this->RedisConn->get($Key);
        }
    }

    /**
     * <b>Delete</b>
     * Deletes a key and your value of specific key.
     *
     * @param STRING $Key key seted in Redis.
     * @return BOOLEAN
     */
    public function Delete($Key)
    {
        if (!$this->RedisConn->exists($Key)) {
            $this->Result = false;
        } else {
            $this->Result = $this->RedisConn->del($Key);
        }
    }

    /**
     * <b>HSet</b>
     * Write a hash of specific key in Redis.
     *
     * @param STRING $Key key in Redis.
     * @param ARRAY/STRING $ArrayorSubKey contents to be saved in Redis.
     * @param STRING $ContentSubKey when isn`t used the $ArrayorSubKey need to be an array with key and value,
     * otherwise pass the content of an subkey in the hash.
     * @return BOOLEAN
     */
    public function HSet($Key, $ArrayorSubKey, $ContentSubKey = null, $Expire = null)
    {
        if ($this->Delete($Key)) {
            $this->Result = false;
            return false;
        }

        if ($ContentSubKey) {
            $this->Result = $this->RedisConn->hset($Key, $ArrayorSubKey, $ContentSubKey);
        } else {
            $this->Result = $this->RedisConn->hmset($Key, $ArrayorSubKey);
        }

        parent::setExpire($Key, $Expire);
    }

    /**
     * <b>HGet</b>
     * Returns the value a hash of specific key and can be used if an subkey.
     *
     * @param STRING $Key key in Redis.
     * @param STRING $Field if used returns the value of an subkey.
     * @return STRING
     */
    public function HGet($Key, $Field = null)
    {
        if ($Field) {
            $this->Result = $this->RedisConn->hget($Key, $Field);
        } else {
            $this->Result = $this->RedisConn->hgetall($Key);
        }
    }


    /**
     * <b>Increment</b>
     * Increase a value of an specific key.
     *
     * If you don`t specifie the $Counter will increse one more.
     *
     * @param STRING $Key key in Redis.
     * @param INT $Counter increase the values of the counter key by larger integer values.
     * @return BOOLEAN
     */
    public function Increment($Key, $Counter = null)
    {
        if (!$Counter) {
            $this->RedisConn->incr($Key);
        } else {
            $this->RedisConn->incrby($Key, $Counter);
        }
    }

    /**
     * <b>Decrement</b>
     * Decrease a value of an specific key.
     *
     * If you don`t specifie the $Counter will decrese one more.
     *
     * @param STRING $Key key in Redis.
     * @param INT $Counter decrease the values of the counter key by larger integer values.
     * @return BOOLEAN
     */
    public function Decrement($Key, $Counter = null)
    {
        if (!$Counter) {
            $this->RedisConn->decr($Key);
        } else {
            $this->RedisConn->decrby($Key, $Counter);
        }
    }

    /**
     * <b>getResult</b>
     * Returns the value saved in $Result class variable.
     *
     * @return STRING
     */
    public function getResult()
    {
        return $this->Result;
    }

    /**
     * <b>getConnection</b>
     * Returns connection object of Redis.
     *
     * @return OBJECT
     */
    public function getConnection()
    {
        return $this->RedisConn;
    }
}
