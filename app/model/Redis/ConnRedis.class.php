<?php

/**
 * Conn.class [ CONEXÃO ]
 * Classe abstrata de conexão. Padrão SingleTon.
 *  Retorna um objeto PDO pel método estático getConn();
 *
 * @copyright (c) 2018, Alef Alves | Bionexo
 */
class ConnRedis
{
    private static $Connect = null;

    public static function getConnRedis()
    {
        return self::Connect();
    }

    /**
     * <b>Connect</b>
     * Connects at Redis server using Predis as client.
     */
    private static function Connect()
    {
        try {
            if (self::$Connect == null) {
                self::$Connect = new Predis\Client(self::getConfig());
                self::$Connect->connect();
                if (!self::$Connect) {
                    throw new Predis\Connection\ConnectionException('Redis doesn`t connect');
                }
            }
        } catch (Predis\Connection\ConnectionException $e) {
            Logger::error(__CLASS__ . ":redis:connect", [
                $e->getCode(), $e->getFile(), $e->getMessage(), $e->getTrace()
            ]);
            Request::redirect('error/503');
            exit;
        }
        return self::$Connect;
    }

    private function getConfig()
    {
        $Config = [
            'scheme' => 'tcp',
            'host' => REDISHOST,
            'port' => 6379,
            'ssl' => ['verify_peer' => false]
        ];

        Logger::info(__CLASS__ . ":redis:config", $Config);
        return $Config;
    }

    public static function setExpire($Key, $Expire)
    {
        if (is_int($Expire)) {
            self::$Connect->expire($Key, $Expire);
        } elseif (is_null($Expire)) {
            self::$Connect->expire($Key, 86400);
        } else {
            self::$Connect->persist($Key);
        }
    }
}
