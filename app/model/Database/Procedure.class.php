<?php

/**
 * <b>Procedure.class:</b>
 * Classe responsável por leituras com procedure no bando de dados.
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class Procedure extends Conn
{
    private $Query;
    private $Places;
    private $Parse;
    private $Result;

    /** @var PDOStatament */
    private $Procedure;

    /** @var PDOS */
    private $Conn;

    /**
     * <b>ExeProcedure</b>
     * Executa a leitura do banco de dados com base no seguintes parametros:
     *
     * @param STRING $Query query com procedure para obter os resultados.
     * @param STRING $ParseString filtros para pegar a leitura.
     * @param BOOLEAN $debug flag para imprimir a query, tem como default o valor `false`
     */
    public function exeProcedure($Query, $ParseString = null, $debug = false)
    {
        if (!empty($ParseString)) {
            parse_str($ParseString, $this->Places);
        }

        $this->Query = $Query;

        if ($debug) {
            debug($Query);
        }

        $this->Execute();
    }

    public function checkResult()
    {
        return $this->Result;
    }

    public function returnResult()
    {
        return $this->Procedure->fetchAll();
    }

    public function getRowCount()
    {
        return $this->Procedure->rowCount();
    }

    /**
     *  *********************************************
     *  **************** PRIVATE METHODS ************
     *  *********************************************
     */
    private function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Procedure = $this->Conn->prepare($this->Query);
        $this->Procedure->setFetchMode(PDO::FETCH_ASSOC);
    }

    private function getSyntax()
    {
        if ($this->Places) {
            $this->Places = array_map('trim', $this->Places);
            $this->Places = array_map('strip_tags', $this->Places);

            foreach ($this->Places as $Vinculo => $Valor) {
                if ($Vinculo == 'limit' || $Vinculo == 'offset') {
                    $Valor = (int)$Valor;
                }

                $this->Procedure->bindValue(":{$Vinculo}", $Valor, (is_int($Valor) ? PDO::PARAM_INT : PDO::PARAM_STR));
            }
        }
    }

    private function clearData()
    {
        $this->Query = trim(strip_tags($this->Query));
    }

    private function Execute()
    {
        $this->Connect();
        try {
            $this->getSyntax();
            $this->Result = $this->Procedure->execute();
        } catch (PDOException $e) {
            $this->Result = null;
            WSErro("Error ao executar o Procedure: {$e->getMessage()}", $e->getCode());
        }
    }
}
