<?php

/**
 * <b>Insert.class:</b>
 * Classe responsável por cadastros genéticos no bando de dados!
 *
 * @copyright (c) 2016, Alef Lima | Bionexo
 */
class Insert extends Conn
{
    private $Table;
    private $Dados;
    private $Result;

    /** @var PDOStatament */
    private $Insert;

    /** @var PDOS */
    private $Conn;

    /**
     * <b>ExeInsert</b> Executa um cadastro simples no banco de dados com estrutura pronta.
     *  Basta informar nome da Table, e um array atribuitivo com nome da coluna e valor.
     *
     * @param STRING $Table = Insira o nome da Table.
     * @param ARRAY $Dados = Informe um array ( Nome da Coluna => Valor).
     */
    public function exeInsert($Table, array $Dados)
    {
        $this->Table = (string) $Table;
        $this->Dados = $Dados;

        $this->getSyntax();
        $this->Execute();
    }

    public function exeInsertAll($table, $columns, $data)
    {
        $this->Table = (string) $table;

        $this->getSyntaxAll($columns, $data);
        $this->Execute();
    }

    private function getSyntaxAll($columns, $data)
    {
        $this->Insert = "INSERT ALL ";
        $columns_query = implode(', ', $columns);

        foreach ($data as $insert_data) {
            $data_query = implode(', ', $insert_data);
            $this->Insert .= " INTO {$this->Table} ({$columns_query}) VALUES ({$data_query}) ";
        }

        $this->Insert .= " SELECT * FROM dual ";
    }

    //** Obtem o resultado da ultima ID */
    public function getResult()
    {
        return $this->Result;
    }

    /**
     *  *********************************************
     *  **************** PRIVATE METHODS ************
     *  *********************************************
     */
    /*     * Conecta ao banco de dados */
    private function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Insert = $this->Conn->prepare($this->Insert);
    }

    /*     * Faz a sintaxe do código de para inserir dados */

    private function getSyntax()
    {
        $Fields = implode(', ', array_keys($this->Dados));
        $Places = implode(', ', $this->Dados);
        $this->Insert = "INSERT INTO {$this->Table} ({$Fields}) VALUES ({$Places})";
    }

    /*     * Realiza a inserção dados  e atribuindo a Result o valor da ultima ID */

    private function Execute()
    {
        $this->Connect();
        try {
            $this->Insert->execute();
            $this->Result = true;
        } catch (Exception $e) {
            $this->Result = null;
            WSErro("Erro ao criar: {$e->getMessage()}", WS_ERROR);
        }
    }
}
