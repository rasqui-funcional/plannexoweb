<?php

/**
 * Conn.class [ CONEXÃO ]
 * Classe abstrata de conexão. Padrão SingleTon.
 *  Retorna um objeto PDO pel método estático getConn();
 *
 * @copyright (c) 2018, Alef Alves | Bionexo
 */
class Conn
{
    /** @var PDO */
    private static $Connect = null;

    public static function getConn()
    {
        return self::Conectar();
    }

    /*     * Retonar um objeto PDO SingleTon Pattern. */

    /**
     * Conecta com o bando de dados com pattert singleton
     * Retonar um objeto PDO!
     */
    private static function Conectar()
    {
        try {
            if (self::$Connect == null) {
                $dsn = 'oci:dbname=' . DBHOST . ';charset=utf8';
                self::$Connect = new PDO($dsn, DBUSER, DBPASS);
            }
        } catch (PDOException $e) {
            Logger::error(__CLASS__. ":connect:pdo", [
                $e->getCode(),
                $e->getFile(),
                $e->getMessage(),
                $e->getTrace()
            ]);

            Request::redirect('error/503');
            exit;
        }

        self::$Connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dblanguage = self::dbLanguageByCountry();
       // self::$Connect->exec("ALTER SESSION SET nls_date_language='{$dblanguage}'");
        return self::$Connect;
    }

    private static function dbLanguageByCountry()
    {
        switch (COUNTRY) {
            case 'ar':
            case 'co':
            case 'pe':
                return 'SPANISH';
                break;
            
            default:
                return 'BRAZILIAN PORTUGUESE';
                break;
        }
    }
}
