<?php

/**
 * <b>Delete.class:</b>
 * Classe responsável por latualizações genéricas no bando de dados!
 *
 * @copyright (c) 2018, Alef Alves Lima | Bionexo
 */
class Delete extends Conn
{
    private $Table;
    private $Terms;
    private $Places;
    private $Result;

    /** @var PDOStatament */
    private $Delete;

    /** @var PDOS */
    private $Conn;

    /**
    * <b>exeDelete</b>
    * Executa um delete do banco de dados com base no seguintes parâmetros:
    *
    * @param STRING $Table tabela no qual vai se conectar.
    * @param STRING $Terms criar filtros para criar o delete.
    * @param STRING $ParseString filtros para pegar a leitura.
    */
    public function exeDelete($Table, $Terms, $ParseString)
    {
        $this->Table = (string)$Table;
        $this->Terms = (string)$Terms;

        parse_str($ParseString, $this->Places);
        $this->getSyntax();
        $this->Execute();
    }

    public function getResult()
    {
        return $this->Result;
    }

    public function getRowCount()
    {
        return $this->Delete->rowCount();
    }

    /**
     * setPlaces
     * Criar/substituir novos places para leitura
     * @param STRING $ParseString
     */
    public function setPlaces($ParseString)
    {
        parse_str($ParseString, $this->Places);
        $this->getSyntax();
        $this->Execute();
    }

    /**
     *  *********************************************
     *  **************** PRIVATE METHODS ************
     *  *********************************************
     */
    private function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Delete = $this->Conn->prepare($this->Delete);
    }

    private function getSyntax()
    {
        $this->Delete = "DELETE FROM {$this->Table} {$this->Terms}";
    }

    private function Execute()
    {
        $this->Connect();
        try {
            $this->Delete->execute($this->Places);
            $this->Result = true;
        } catch (PDOException $e) {
            $this->Result = null;
            WSErro("Erro ao deletar: {$e->getMessage()}", $e->getCode());
        }
    }
}
