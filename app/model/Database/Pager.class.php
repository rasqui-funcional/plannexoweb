<?php

/**
 * <b>Select.class:</b>
 * Classe responsável por leituras específicas no bando de dados.
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class Pager extends Conn
{
    private $Query;
    private $Places;
    private $Result;

    /** @var PDOStatament */
    private $Select;

    /** @var PDOS */
    private $Conn;

    /**
     * <b>ExeSelect</b>
     * Executa a leitura do banco de dados com base no seguintes parametros:
     *
     * @param STRING $Table tabela no qual vai se conectar.
     * Para criar joins passe a string separando os bancos com ','.
     * @param INT $Offset colunas no qual serão retornadas.
     * @param INT $Limit colunas no qual serão retornadas.
     * @param STRING $Columns colunas no qual serão retornadas.
     * @param STRING $Terms criar filtros para obter resultados.
     * @param STRING $ParseString filtros para pegar a leitura.
     * @return mixed
     */
    public function exePager($Table, $Offset, $Limit, $Columns = null, $Terms = null, $ParseString = null)
    {
        if (!empty($ParseString)) {
            parse_str($ParseString, $this->Places);
        }

        $Columns = (!$Columns ? '*' : $Columns);

        $this->Query = "SELECT * FROM (  SELECT ROWNUM rnum,  a.* FROM ( SELECT {$Columns} FROM {$Table} {$Terms} ) a WHERE  ROWNUM <= {$Offset} + {$Limit}) WHERE rnum >= {$Offset}";
        $this->Execute();

        return $this->getResult();
    }

    public function getResult()
    {
        return $this->Result;
    }

    public function getRowCount()
    {
        return $this->Select->rowCount();
    }

    /**
     *  *********************************************
     *  **************** PRIVATE METHODS ************
     *  *********************************************
     */
    private function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Select = $this->Conn->prepare($this->Query);
        $this->Select->setFetchMode(PDO::FETCH_ASSOC);
    }

    private function getSyntax()
    {
        if ($this->Places) {
            foreach ($this->Places as $Vinculo => $Valor) {
                if ($Vinculo == 'limit' || $Vinculo == 'offset') {
                    $Valor = (int) $Valor;
                }

                $this->Select->bindValue(":{$Vinculo}", $Valor, (is_int($Valor) ? PDO::PARAM_INT : PDO::PARAM_STR));
            }
        }
    }

    private function Execute()
    {
        $this->Connect();
        try {
            $this->getSyntax();
            $this->Select->execute();
            $this->Result = $this->Select->fetchAll();
        } catch (PDOException $e) {
            $this->Result = null;
            WSErro("Error ao executar o Select: {$e->getMessage()}", $e->getCode());
        }
    }
}
