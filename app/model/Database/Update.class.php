<?php

/**
 * <b>Update.class:</b>
 * Classe responsável por atualizações genéricas no bando de dados!
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class Update extends Conn
{
    private $Tabela;
    private $Dados;
    private $Termos;
    private $Places;
    private $Result;

    /** @var PDOStatament */
    private $Update;

    /** @var PDOS */
    private $Conn;

    /**
    * <b>exeUpdate</b>
    * Executa a atualização do banco de dados com base no seguintes parâmetros:
    *
    * @param STRING $Table tabela no qual vai se conectar.
    * @param ARRAY $Dados = Informe um array ( Nome da Coluna => Valor).
    * @param STRING $Terms criar filtros para criar o update.
    * @param STRING $ParseString filtros para pegar a leitura.
    */
    public function exeUpdate($Table, array $Dados, $Termos, $ParseString)
    {
        $this->Table = (string) $Table;
        $this->Dados = $Dados;
        $this->Termos = (string) $Termos;

        parse_str($ParseString, $this->Places);
        $this->getSyntax();
        $this->Execute();
    }

    public function getResult()
    {
        return $this->Result;
    }

    public function getRowCount()
    {
        return $this->Update->rowCount();
    }

    /**
     * setPlaces
     * Criar/substituir novos places para leitura
     * @param STRING $ParseString
     */
    public function setPlaces($ParseString)
    {
        parse_str($ParseString, $this->Places);
        $this->getSyntax();
        $this->Execute();
    }

    /**
     *  *********************************************
     *  **************** PRIVATE METHODS ************
     *  *********************************************
     */
    private function Connect()
    {
        $this->Conn = parent::getConn();
        $this->Update = $this->Conn->prepare($this->Update);
    }

    private function getSyntax()
    {
        $Places = [];
        foreach ($this->Dados as $Key => $Value) {
            $Places[] = $Key . ' = :' . $Key;
        }

        $Places = implode(', ', $Places);
        $this->Update = "UPDATE {$this->Table} SET {$Places} {$this->Termos}";
        Logger::debug(__CLASS__ . ':sql:syntax:update', [$this->Update]);
    }

    private function bindParam()
    {
        $AllData = array_merge($this->Dados, $this->Places);

        foreach ($AllData as $Column => $Data) {
            $this->Update->bindValue(":{$Column}", $Data, (is_int($Data) ? PDO::PARAM_INT : PDO::PARAM_STR));
        }
    }

    private function Execute()
    {
        $this->Connect();
        $this->bindParam();
        try {
            $this->Update->execute();
            $this->Result = true;
        } catch (PDOException $e) {
            $this->Result = null;
            WSErro("Erro ao atualizar: {$e->getMessage()}", $e->getCode());
        }
    }
}
