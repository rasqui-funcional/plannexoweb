<?php

/**
 * can
 *
 * Check if the user is the user passed on $profile
 *
 * @param STRING $profile
 * @param STRING $attribute
 * @return BOOLEAN
 */
function can($profile, $attribute = 'profile')
{
    return Gate::check($profile, $attribute);
}

/**
 * cannot
 *
 * Check if the user isn`t the user passed on $profile
 *
 * @param STRING $profile
 * @return BOOLEAN
 */
function cannot($profile)
{
    if (can($profile)) {
        return false;
    }

    return true;
}
