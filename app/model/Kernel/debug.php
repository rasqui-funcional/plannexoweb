<?php

/**
 * debug
 *
 * A simplest function to debug ;)
 *
 * @param $items
 */
function debug(...$items)
{
    foreach ($items as $item) {
        echo '<pre>';
        var_export($item);
        echo '</pre>';
    }
}

/**
 * dd
 *
 * A simplest function to debug and kill after process ;)
 *
 * @param $items
 */
function dd(...$items)
{
    debug($items);
    die();
}

function get_caller_info() {
    $c = '';
    $file = '';
    $func = '';
    $class = '';
    $trace = debug_backtrace();
    if (isset($trace[2])) {
        $file = $trace[2]['file'];
        $func = $trace[2]['function'];
        if ((substr($func, 0, 7) == 'include') || (substr($func, 0, 7) == 'require')) {
            $func = '';
        }
    } else if (isset($trace[1])) {
        $file = $trace[1]['file'];
        $func = '';
    }
    if (isset($trace[3]['class'])) {
        $class = $trace[3]['class'];
        $func = $trace[3]['function'];
        $file = $trace[2]['file'];
    } else if (isset($trace[2]['class'])) {
        $class = $trace[2]['class'];
        $func = $trace[2]['function'];
        $file = $trace[2]['file'];
    }
    if ($file != '') $file = basename($file);
    $c .= $file . ": ";
    $c .= ($class != '') ? ":" . $class . "->" : "";
    $c .= ($func != '') ? $func . "(): " : "";
    return($c);
}

function invoker()
{
    debug(get_caller_info());
}
