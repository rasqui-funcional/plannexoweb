<?php

/**
 * @param $string
 * @return bool
 */
function is_json($string)
{
    if (is_array($string) || is_object($string)) {
        return false;
    }

    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE) ? true : false;
}
