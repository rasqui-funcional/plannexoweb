<?php

/**
 * addView
 *
 * Add an view passing his route path inside other
 *
 * @param STRING $view_route
 * @return STRING erro message
 */
function addView($view_route)
{
    if (file_exists("resources/views/{$view_route}.php")) {
        $ViewData = $_SESSION['view_data'];
        if (file_exists("resources/lang/{$_SESSION['country']}.php")) {
            require "resources/lang/{$_SESSION['country']}.php";
        } else {
            require "resources/lang/br.php";
        }
        require "resources/views/{$view_route}.php";
    } else {
        echo "<b>Error on addView</b> This view '{$view_route}' doesn`t exist, maybe in another dimension.";
    }
}

/**
 * addComponent
 *
 * @param STRING $component
 */
function addComponent($component)
{
    if (file_exists("resources/components/{$component}")) {
        $ViewData = $_SESSION['view_data'];
        if (file_exists("resources/lang/{$_SESSION['country']}.php")) {
            require "resources/lang/{$_SESSION['country']}.php";
        } else {
            require "resources/lang/br.php";
        }
        require "resources/components/{$component}";
    } else {
        echo "This component '{$view_route}' wasn`t found, try again.";
    }
}
