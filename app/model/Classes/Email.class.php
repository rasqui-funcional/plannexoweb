<?php

/**
 * Email [ MODEL ]
 * Modelo responsável por configurar a PHPMailer, validar os dados e disparar emails no sistema.
 * @author alef2
 */
class Email {

    /** @var PHPMailer */
    private $Mail;

    /** EMAIL DATA */
    private $Data;

    /** CORPO DO EMAIL */
    private $Assunto;
    private $Mensagem;
    private $Anexo;

    /** REMETENTE */
    private $RemetenteNome;
    private $RemetenteEmail;

    /** DESTINO */
    private $DestinoNome;
    private $DestinoEmail;

    /** CONTROLE */
    private $Error;
    private $Result;

    function __construct($Dir = null) {

        require ("{$Dir}_app/Library/PHPMailer/phpmailer.class.php");

        $this->Mail = new PHPMailer;
        $this->Mail->Host = MAILHOST;
        $this->Mail->Port = MAILPORT;
        $this->Mail->Username = MAILUSER;
        $this->Mail->Password = MAILPASS;
        $this->Mail->CharSet = 'UTF-8';
    }
    
    /**
     * 
     * <b>Enviar</b>
     * Função que dispara o envio do email utilizando o PHP Mailer.
     * 
     * @param ARRAY $Data 
     *      Campos Necessários no Array:<br>
     *         'DestinoNome', <br>
                'DestinoEmail',<br>
                'RemetenteNome',<br>
                'RemetenteEmail',<br>
                'Assunto',<br>
                'Mensagem'<br>
     * @param FILE $Anexo
     */
    public function Enviar(array $Data, $Anexo = false) {
        $this->Data = $Data;
        $this->ClearCode();
        $this->Anexo = $Anexo;

        if (in_array('', $this->Data)):
            $this->Error = ['Preencha todos os campos para enviar o email.', 'warning'];
            $this->Result = false;
        elseif (!Check::Email($this->Data['RemetenteEmail'])):
            $this->Error = ['Insira um email com formato válido.', 'warning'];
            $this->Result = false;
        else:
            $this->setMail();
            $this->Config();
            $this->sendMail();
            $this->Clear();
        endif;
    }

    public function addAnexo($Imagem) {
        $this->Anexo = $Imagem;
        $this->Mail->addAttachment($this->Anexo['tmp_name'], $this->Anexo['name']);
    }

    public function addCC($Email, $Nome = null) {
        if (Check::Email($Email)):
            $this->Mail->addCC($Email, $Nome);
        endif;
    }

    public function addBCC($Email) {
        if (Check::Email($Email)):
            $this->Mail->addBCC($Email);
        endif;
    }

    function getResult() {
        return $this->Result;
    }

    function getError() {
        return $this->Error;
    }

    public function getAnexo() {
        return $this->Anexo;
    }

    /*     * **************************** */
    /*     * *********PRIVATES*********** */
    /*     * **************************** */

    private function Clear() {

        $this->Mail->clearAllRecipients();
        $this->Mail->clearCCs();
        $this->Mail->clearAddresses();
        $this->Mail->clearBCCs();
        $this->Mail->clearAttachments();
    }

    private function ClearCode() {
        array_map('strip_tags', $this->Data);
        array_map('trim', $this->Data);
    }

    private function setMail() {
        $this->Assunto = $this->Data['Assunto'];
        $this->Mensagem = $this->Data['Mensagem'];
        $this->RemetenteEmail = $this->Data['RemetenteEmail'];
        $this->RemetenteNome = $this->Data['RemetenteNome'];
        $this->DestinoEmail = $this->Data['DestinoEmail'];
        $this->DestinoNome = $this->Data['DestinoNome'];

        $this->Data = null;
        $this->setMsg();
        $this->sendMail();
    }

    private function setMsg() {
        $this->Mensagem = "{$this->Mensagem}<br><small>Recebida em: " . date('d/m/Y H:i') . "</small>";
    }

    private function Config() {
        //SMTP AUTH
        $this->Mail->isSMTP();
        $this->Mail->SMTPAuth = true;
        $this->Mail->isHTML();

        //SMTP AUTH
        $this->Mail->From = MAILUSER;
        $this->Mail->FromName = $this->RemetenteNome;
        $this->Mail->addReplyTo($this->RemetenteEmail, $this->RemetenteNome);

        //SMTP AUTH
        $this->Mail->Subject = $this->Assunto;
        $this->Mail->Body = $this->Mensagem;
        $this->Mail->addAddress($this->DestinoEmail, $this->DestinoNome);
        $this->Mail->addBCC('backup@trescompany.com');
    }

    private function sendMail() {
        if ($this->Mail->send()):
            $this->Result = true;
        else:
            $this->Error = ["Erro ao enviar email. Contate o admin. ( {$this->Mail->ErrorInfo} )", 'warning'];
            $this->Result = false;
        endif;
    }

}
