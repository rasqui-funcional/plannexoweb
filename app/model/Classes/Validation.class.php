<?php
/**
 * <b>Validation.class:</b>
 * Classe responsável por validar os dados passados vi form.
 *
 * @copyright (c) 2018, Jonas Rocha | Bionexo
 */

class Validation
{
    /**
     * <b>filter</b>
     * Chama as validações necessárias para a variavel $data em acordo com o seu tipo.
     * @param array/string/integer/boolean/double $data
     * @return array/string/integer/boolean/double
     */
    public function filter($data)
    {
        $result = [];

        if (is_array($data)) {
            $result = $this->filterArray($data);
        } else {
            $result = $this->filterData($data);
        }

        return $result;
    }
    

    /**
     * <b>filterArray</b>
     * Monta um array e um array de arrays com valores sem sql injection.
     * @param array/string/integer/boolean/double $data
     * @return string/
     */
    public function filterArray($data)
    {
        $result = [];

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $subkey => $subvalue) {
                    $result[$key][$subkey] = $this->filterData($subvalue);
                }
            } else {
                $result[$key] = $this->filterData($value);
            }
        }

        return $result;
    }

    /**
     * <b>validate</b>
     * Faz a validação anti injection dos dados chamados no filter.
     * @param array/string/integer/boolean/double $data
     * @return array
     */
    public function filterData($data)
    {
        $data = trim($data);
        $data = addslashes($data);
        $data = strip_tags($data);
        $data = $this->checkSqlInjections($data);

        return $data;
    }

    /**
     * <b>validate</b>
     * Troca palavras reservadas do sql para uma string vazia.
     * @param string $data
     * @return array
     */
    private function checkSqlInjections($string)
    {
        $sql_reserved_words = ['select', 'update', 'insert', 'drop'];

        foreach ($sql_reserved_words as $word) {
            $string = str_ireplace($word, '', $string);
        }

        return trim(preg_replace('/\s+/', ' ', $string));
    }
}