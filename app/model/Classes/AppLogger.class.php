<?php

/**
 * <b>Log</b>
 * This Classe is responsible for write application log into a file.
 * $logger => variable channel name to write log. Default channel name is plannexoweb
 * $json_formatter => Objetct to formatter json logger
 * $path => default path create log file. Defautl /tmp/
 *
 * @copyright (c) 2018, Bionexo
 */
class AppLogger extends Monolog\Logger
{
    public function __construct()
    {
        parent::__construct(
            'plannexoweb'
        );

        $this->pushHandler($this->setHandler());
    }

    private function setHandler()
    {
        return (new Monolog\Handler\StreamHandler(
            sprintf("php://stdout", date('Y-m-d')),
            Monolog\Logger::DEBUG
        ))->setFormatter(new Monolog\Formatter\JsonFormatter());
    }
}
