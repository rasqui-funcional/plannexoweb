<?php

class SetParameter
{
    public function __construct()
    {
        define('CALENDAR_SKU_PARAM', 'V_CALENDAR');

        define('BASIC_INFORMATION_PARAMS', [
            'USER_ID',
            'DEFAULT_LOCAL',
            'OBSERVATION',
            'CURVE_XYZ',
            'COD_GROUP1_FK',
            'COD_GROUP2_FK',
            'COD_GROUP3_FK',
            'COD_GROUP4_FK',
            'COD_GROUP5_FK',
            'COD_GROUP6_FK',
            'COD_GROUP7_FK',
            'COD_GROUP8_FK',
            'COD_GROUP9_FK',
            'COD_GROUP10_FK',
            'COD_GROUP11_FK',
            'COD_GROUP12_FK',
            'SIT_SKU',
        ]);

        define('POLICY_SKU_PARAMS', [
            'ID_PROFILE_FK',
            'V_LT_PLANNER_IN',
            'V_LT_PURCHASE_IN',
            'V_LT_SUPPLIER_IN',
            'V_LT_RECEIVING_IN',
            'V_LT_OTHERS_IN',
            'V_ORDER_MIN',
            'V_ORDER_MULT',
            'V_QTY_MIN_IN',
            'V_QTY_CONS',
            'V_DATE_CONS_EXPIRATION',
            'COD_PROFILE'
        ]);

        define('INTERDEPENDENCE_PARAMS', [
            'COD_ITEM_PK',
            'COD_ESTAB_FK',
            'COD_ESTAB_PK',
            'V_DEPEND_DATE_VALID',
            'V_DEPEND_PERC_OUT',
            'V_DEPEND_PROFILE_FLAG'
        ]);

        define('SUGGEST_TABLE_PARAMS', [
            'ID_SUG_PK',
            'ID_PSUG_PK',
            'QTY_ALTERED',
            'DATE_ARRIVAL_ALTERED',
            'OBS',
            'GROUPED_INSTALLMENTS'
        ]);
    }
}
