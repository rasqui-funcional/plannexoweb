<?php

/**
 * Classe criada
 */
class Fields
{
    private $ViewData;
    private $Company;
    private $User;
    public  $lang;

    public function __construct()
    {
        global $lang;
        $UserSession = Login::getUserSession();
        $this->User = USERID;
        $this->Company = $UserSession['id_company_pk'];
        $this->lang = $lang;
    }

    public function getFields()
    {
        $this->getEstab();
        $this->getUser();
        $this->getProfile();
        $this->getCatSystem();
        $this->getDeposit();
        $this->getCalendar();

        $this->ViewData['Groups'] = $this->getNewDinamicFieldsGrouping();
        foreach ($this->ViewData['Groups'] as $num_group_pk => $desc_group) {
            $this->getGroup($num_group_pk);
        }

        return $this->ViewData;
    }

    public function getDinamicFieldsGrouping($sit_group = 1)
    {
        $result = null;
        if ($this->Company) {
            $result = DB::Select(
                'SCM_GROUP_NAME',
                'NUM_GROUP_PK, DESC_GROUP',
                "WHERE ID_COMPANY_FK = {$this->Company} AND SIT_GROUP = {$sit_group}"
            );
        }
        
        return $result ?? [];
    }

    public function getNewDinamicFieldsGrouping($sit_group = 1)
    {
        $groups = [];
        if (!$this->Company) {
            return $groups;
        }

        if ($sit_group == 2) {
            $groupsActive = Redis::HGet("company:{$this->Company}:group:active");
            $groupsInactive = Redis::HGet("company:{$this->Company}:group:inactive");
            if (!$groupsActive || !$groupsInactive) {
                return $this->setDinamicGrouping();
            }

            return ['active' => $groupsActive, 'inactive' => $groupsInactive];
        }

        $node = ($sit_group == 1) ? 'active' : 'inactive';
        $groups = Redis::HGet("company:{$this->Company}:group:{$node}");
        if (!$groups) {
            return $this->setDinamicGrouping()[$node];
        }

        return $groups;
    }

    public function getTableColumns($Table)
    {
        $ColsTable = DB::getTableCols($Table);
        $ColsArray = explode(",", $ColsTable);
        $ColsArray = array_map("trim", $ColsArray);
        return $ColsArray;
    }

    public static function getSelectFilter($View)
    {
        $company = (new self)->Company;
        $table = "uac_filters";
        $columns = "filter_nome, filter_id";
        $terms = "where filter_tela_id = '{$View}' AND ID_COMPANY_FK = {$company} order by filter_id asc";

        $result = DB::Select($table, $columns, $terms);
        if ($result) {
            foreach ($result as $Filter) {
                $filters[] = ['value' => $Filter['FILTER_ID'], 'name' => $Filter['FILTER_NOME']];
            }
        } else {
            $filters = [];
        }

        return $filters;
    }

    /**
     * <b>unsetFilterColuns</b>
     * These columns are required and should not appear in the exibition filter
     * @param array $data
     * @param array $columnsToUnset
     * @return array
     */
    public function unsetFilterColumns(array $data, array $columnsToUnset): array
    {
        return array_filter($data, function ($item) use ($columnsToUnset) {
            return !in_array($item, $columnsToUnset);
        });
    }

    private function setDinamicGrouping()
    {
        $groupData = [
            'active' => [],
            'inactive' => [],
        ];

        $groups = DB::Select(
            'SCM_GROUP_NAME',
            'NUM_GROUP_PK, DESC_GROUP, SIT_GROUP',
            "WHERE ID_COMPANY_FK = {$this->Company} ORDER BY SIT_GROUP DESC"
        );

        foreach ($groups as $group) {
            if ($group['SIT_GROUP'] == 1) {
                $groupData['active'][$group['NUM_GROUP_PK']] = $group['DESC_GROUP'];
                continue;
            }

            $groupData['inactive'][$group['NUM_GROUP_PK']] = $group['DESC_GROUP'];
        }

        if (count($groupData['active']) > 0) {
            Redis::HSet("company:{$this->Company}:group:active", $groupData['active']);
        }

        if (count($groupData['inactive']) > 0) {
            Redis::HSet("company:{$this->Company}:group:inactive", $groupData['inactive']);
        }

        return $groupData;
    }

    private function getGroup($GroupFK)
    {
        $Select = DB::Select("SCM_GROUP_CONT", "ID_COMPANY_FK, DESC_GROUP_CONT, cod_group_pk",
            "WHERE NUM_GROUP_FK = {$GroupFK} AND ID_COMPANY_FK = " . $this->Company . " ORDER BY DESC_GROUP_CONT");
        if ($Select) {
            $this->ViewData["Group_{$GroupFK}"] = $Select;

            /**
             * Setting on cache
             */
            foreach ($Select as $Result) {
                $Data[$Result['COD_GROUP_PK']] = $Result['DESC_GROUP_CONT'];
            }

            Redis::HSet("company:{$this->Company}:group-{$GroupFK}", $Data);
        } else {
            $this->ViewData["Group_{$GroupFK}"][] = ['value' => 0, 'name' => 'Nenhum fitro.', 'disable'];
        }
    }

    private function getDeposit()
    {
        $userSession = Login::getUserSession();
        $inEstabs = '';
        if(!empty($userSession['estab'])) {
            $inEstabs = " AND COD_ESTAB_PK IN({$userSession['estab']})";
        }

        $Select = DB::Select("ERP_LOCAL", "cod_local_pk,cod_local_pk || ' - ' || desc_local as descricao",
            "WHERE id_company_fk = " . $this->Company . $inEstabs . " ORDER BY COD_LOCAL_PK");
        if ($Select) {
            $this->ViewData["Deposit"] = $Select;

            /**
             * Setting on cache
             */
            foreach ($Select as $Result) {
                $Data[$Result['COD_LOCAL_PK']] = $Result['DESCRICAO'];
            }

            Redis::HSet("company:{$this->Company}:deposit", $Data);
        } else {
            $this->ViewData["Deposit"][] = ['value' => 0, 'name' => 'Nenhum fitro.', 'disable'];
        }
    }

    private function getCalendar()
    {
        $Select = DB::Select('scm_cal_receiving',
            "id_calendar_pk,id_calendar_pk || ' - ' || desc_calendar as desc_calendar",
            'WHERE id_company_fk = ' . $this->Company . ' AND id_calendar_pk != -1   ORDER BY id_calendar_pk');

        if ($Select) {
            foreach ($Select as $Estab) {
                $this->ViewData['Calendar'][] = ['code' => $Estab['ID_CALENDAR_PK'], 'name' => $Estab['DESC_CALENDAR']];
            }
        } else {
            $this->ViewData['Calendar'][] = ['value' => 0, 'name' => 'Nenhum Calendário', 'disable'];
        }
    }

    //get only groups with sit_group = 1
    private function getEstab()
    {
        $userSession = Login::getUserSession();
        $inEstabs = '';
        if(!empty($userSession['estab'])) {
            $inEstabs = " AND COD_ESTAB_PK IN({$userSession['estab']})";
        }

        $notInter = " AND cod_estab_pk not like 'INTERDEPENDENCIA-%'";

        $Select = DB::Select('scm_estab', "cod_estab_pk,cod_estab_pk || '-' || desc_estab_short AS nome",
            'WHERE id_company_fk = ' . $this->Company . $inEstabs . $notInter. ' ORDER BY cod_estab_pk');
        if ($Select) {
            foreach ($Select as $Estab) {
                $this->ViewData['Estab'][] = ['code' => $Estab['COD_ESTAB_PK'], 'name' => $Estab['NOME']];
            }
        } else {
            $this->ViewData['Estab'][] = ['value' => 0, 'name' => 'Nenhum fitro.', 'disable'];
        }
    }

    private function getUser()
    {
        /**
         * Devido ao fato da tabela UAC_USER_ESTAB que relaciona o usuário ao estabelecimento
         * não estar devidamente preenchida
         * estou tirando o filtro de estabelecimentos.
         */
        // $this->userEstabs = $this->getUserEstabs();
        // $estabsCond = '';
        // if(!empty($this->userEstabs)){
        //     $estabsCond = ' AND uue.COD_ESTAB_PK IN ('. implode(',',$this->userEstabs) .')';
        // }
        // $table = 'UAC_USER uu
        //             left join UAC_USER_ESTAB uue
        //             ON uu.ID_USER_PK = uue.ID_USER_PK';
        // $conditions = 'where active = 1 and id_company_fk = ' . $this->Company  . $estabsCond;
        // $Select = DB::Select($table, 'distinct uu.id_user_pk,login', $conditions);

        $Select = DB::Select('uac_user', 'id_user_pk,login',
            'where active = 1 and id_company_fk = ' . $this->Company);
        if ($Select) {
            $this->ViewData['User'] = $Select;
        } else {
            $this->ViewData['User'][] = ['value' => 0, 'name' => 'Nenhum fitro.', 'disable'];
        }
    }

    private function getUserEstabs()
    {
        $estabs = DB::Select('UAC_USER_ESTAB', 'id_user_pk,cod_estab_pk',
            'where id_user_pk = ' . $this->User);
        $estabsId = [];
        foreach($estabs as $estab){
            $estabsId[] = $estab['COD_ESTAB_PK'];
        }
        return $estabsId;
    }

    private function getProfile()
    {
        $Select = DB::Select('scm_profile', "id_profile_pk, id_profile_pk || ' - ' || cod_profile AS nome",
            "where id_company_fk = " . $this->Company . ' ORDER BY id_profile_pk');

        if ($Select) {
            $this->ViewData['Profile'] = $Select;
        } else {
            $this->ViewData['Profile'][] = ['value' => 0, 'name' => 'Nenhum fitro.', 'disable'];
        }
    }

    private function getCatSystem()
    {
        $Select = DB::Select('scm_group_cont', "cod_group_pk, desc_group_cont",
            "where num_group_fk = 1 and id_company_fk = " . $this->Company);

        if ($Select) {
            $this->ViewData['CatSystem'] = $Select;
        } else {
            $this->ViewData['CatSystem'][] = ['value' => 0, 'name' => 'Nenhum fitro.', 'disable'];
        }
    }
}
