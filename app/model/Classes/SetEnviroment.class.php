<?php

class SetEnviroment
{
    public function __construct()
    {
        define('DBHOST', getenv("DBSERVIDORNAME"));
        define('DBUSER', getenv("DBUSUARIO"));
        define('DBPASS', getenv("DBSENHA"));

        define('REDISHOST', getenv("REDISHOST"));

        define('BIOIDURL', getenv("BIOIDURL"));
        define('BIOVIEWURL', getenv("BIOVIEWURL"));

        define('HOME', $_SERVER['HTTP_HOST']);
    }
}
