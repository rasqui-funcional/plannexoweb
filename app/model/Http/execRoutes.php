<?php

require('routes.php');

$permissions = file_exists(__DIR__. '/../../permissions.php')
    ? include(__DIR__. '/../../permissions.php')
    : null;


/**
 * Inicia a Classe de Rotas
 */
$AdminRoutes = new Routes($Routes, $permissions, $_SERVER['REQUEST_URI']);
$AdminRequest = new Request();

/**
 * Armazena o caminho da View.
 */
$View = $AdminRoutes->getView();

/**
 * Armazena o pais vindo da URL.
 */
$Country = $AdminRoutes->getCountry();
$_SESSION['country'] = $Country;
$Currency = $AdminRoutes->getCurrency();
$_SESSION['currency'] = $Currency;

if (file_exists("resources/lang/{$Country}.php")) {
    require("resources/lang/{$Country}.php");
} else {
    require("resources/lang/br.php");
}

$ErrorNumber = ($View == 'error' ? Request::get('number') : false);

/**
 * Login checks
 */
if (!Login::bypassLogin($View)) {
    Login::check();
}

/***
 * Verifica as permissoes e acesso por perfil de usuario logado
 */
$config['rules'] = [
    ''
];


/**
 * Obtem o nome do Controller respectivo a View e o declara;
 */
$ControllerPath = $AdminRoutes->getControllerPath();
$ControllerName = $AdminRoutes->getController();

require("controllers/{$ControllerPath}.class.php");
$AdminController = new $ControllerName();

/**
 * Obtem o nome da Função respectiva a View e retorna o $ViewData;
 */
$FunctionName = $AdminRoutes->getControllerFunction();
$ViewData = $AdminController->$FunctionName();
$_SESSION['view_data'] = $ViewData;
