<?php

/**
 * <b>Routes</b>
 */
class Routes
{
    private $Routes;
    private $permissions;
    private $URI;
    private $View;
    private $Controller;
    private $ControllerPath;
    private $ViewArray;
    private $Params;
    private $Country;
    private $Currency;
    private $uri;

    public function __construct($routes, $permissions, $request_uri)
    {
        $this->Routes = $routes;
        $this->permissions = $permissions;
        $this->setUri($request_uri);
        $this->setCountry();
        $this->setCurrency();
        $this->isAllowed(Login::getUserProfile());
    }

    public function getRoutes()
    {
        return $this->Routes;
    }

    public function getCountry()
    {
        return $this->Country;
    }

    private function setCountry()
    {
        $this->Country = 'br';
        if (!empty($this->uri[0])) {
            $this->Country = $this->uri[0];
        }

        $login = Login::getUserSession();
        if(isset($login['lang']) && !empty($login['lang'])) {
            $this->Country = $login['lang'];
        }
        define('COUNTRY', $this->Country);
    }

    public function getCurrency()
    {
        return $this->Currency;
    }

    private function setCurrency()
    {
        $this->Currency = 'BRL';
        $login = Login::getUserSession();
        if(isset($login['currency']) && !empty($login['currency'])) {
            $this->Currency = $login['currency'];
        }
        define('CURRENCY', $this->Currency);
    }

    public function getViewCSS()
    {
        return $this->getCSS();
    }

    public function isAjax()
    {
        if (strpos($this->View, "ajax") !== false) {
            return true;
        }

        return false;
    }

    /**
     * <b>getView</b>
     * Cronstroi o array de URI e retornar a View
     *
     */
    public function getView()
    {
        $this->URI = $this->getURI();
        if (is_null($this->URI)) {
            return Request::home();
        }

        return $this->searchView();
    }

    /**
     * <b>getControllerPath</b>
     * Retorna apenas o Controller sem a função
     */
    public function getControllerPath()
    {
        return substr($this->ControllerPath, 0, strpos($this->ControllerPath, '@'));
    }

    /**
     * <b>getController</b>
     * Retorna apenas o Controller sem a função
     */
    public function getController()
    {
        $ControllerPathArray = explode("/", $this->getControllerPath());
        return end($ControllerPathArray);
    }

    /**
     * <b>getControllerFunction</b>
     * Retorna a função do Controller
     */
    public function getControllerFunction()
    {
        return substr($this->ControllerPath, strpos($this->ControllerPath, '@') + 1, strlen($this->ControllerPath));
    }

    /**
     * <b>getParams</b>
     * Retorna os Parametros com os Valores
     */
    public function getParams()
    {
        $this->setParamsValue();
        return $this->Params;
    }

    /**
     * ************ PRIVATE ***************
     */

    private function getCSS()
    {
        if (file_exists("resources/css/{$this->View}.css") && !is_dir("resources/css/{$this->View}.css")) {
            echo "<link rel='stylesheet' href='/resources/css/{$this->View}.css'/>";
        } else {
            return false;
        }
    }

    private function getURI()
    {
        unset($this->uri[0]);

        $uri = [];
        foreach ($this->uri as $part) {
            $uri[] = $part;
        }

        return $uri;
    }

    private function setUri($uri)
    {
        $this->uri = explode('/', substr($uri, 1));
    }

    private function searchView()
    {
        foreach ($this->Routes as $View => $ControllerPath) {
            if ($this->checkView($View)) {
                $this->ControllerPath = $ControllerPath;
                break;
            }
        }

        if (!$this->View) {
            Request::redirect('error/404');
        } else {
            $this->getParams();
            return $this->View;
        }
    }

    private function checkView($View)
    {
        $this->sliptViewParams($View);
        $ArrayView = explode("/", $View);
        if (count($ArrayView) == count($this->URI)) {
            $ViewChecker = true;
            foreach ($this->ViewArray as $Key => $ViewPart) {
                if ($ViewPart != $this->URI[$Key]) {
                    $ViewChecker = false;
                    $this->View = false;
                    $this->Params = false;
                    break;
                }
            }

            return $ViewChecker;
        } else {
            $this->View = false;
            $this->Params = false;
        }

        return false;
    }

    private function sliptViewParams($View)
    {
        $positionParams = strpos($View, '{');
        $this->View = ($positionParams ? substr($View, 0, $positionParams - 1) : $View);
        $this->ViewArray = explode("/", $this->View);
        if ($positionParams) {
            $this->Params = substr($View, $positionParams, strlen($View));
            $this->Params = str_replace('{', '', $this->Params);
            $this->Params = str_replace('}', '', $this->Params);
            $this->Params = explode("/", $this->Params);
        } else {
            $this->Params = false;
        }
    }

    private function setParamsValue()
    {
        $paramI = 0;
        for ($i = count($this->ViewArray); $i < count($this->URI); $i++) {
            $_SESSION['params'][$this->Params[$paramI]] = $this->URI[$i];
            unset($this->Params[$paramI]);
            $paramI++;
        }
    }
    
    private function isAllowed($profile)
    {
        $part = explode('/', substr($_SERVER['REQUEST_URI'], 1));
        $denny_id = array_search($part[1], $this->permissions['roles']['denny'][$profile]);
        if ($this->permissions['roles']['denny'][$profile][$denny_id] == $part[1]) {
            Request::redirect('error/403');
        }
    }
}
