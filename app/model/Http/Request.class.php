<?php

class Request
{
    private $Terms;
    private $Parses;

    public static function post()
    {
        $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);

        if (!$post) {
            return false;
        }

        return $post;
    }

    /**
     * <b>get</b>
     * Get params setted on  route
     *
     * @param STRING $Parameter the param name used in route ex:
     * if route is 'test/{clientid}' your param is 'clientid'.
     */
    public static function get($Parameter)
    {
        if (!isset($_SESSION['params'][$Parameter])) {
            return false;
        }

        return $_SESSION['params'][$Parameter];
    }

    /**
     * <b>redirect</b>
     * Easyly redirect only setting the target route
     *
     * @param STRING $Route route previusly inserted on routes.php.
     */
    public static function redirect($Route)
    {
        $scheme = 'https';
        if (HOME == 'localhost') {
            $scheme = 'http';
        }

        $URL = $scheme . "://" . HOME . "/" . COUNTRY . "/$Route";
        header("Location: $URL");
    }

    public static function home()
    {
        $user_data = Login::getUserSession();
        define('COUNTRY', $user_data['lang']);

        switch ($user_data['profile']) {
            case 'buyer':
            case 'supplier':
                self::redirect('dashboard');
                break;

            default:
                self::redirect('suggest/filters');
                break;
        }
    }

    /**
     * @param $referer
     * @return bool
     */
    public static function verifyReferer($referer) : bool
    {
        return strstr(filter_input(INPUT_SERVER, 'HTTP_REFERER'), $referer);
    }

    /**
     * @param string $csv_name
     */
    public static function generateCSVHeaders($csv_name = 'data')
    {
        $csv_name = Utilities::Name($csv_name) . date('-d-m-Y\.\c\s\v');
        header('Content-Type: text/csv; charset=ISO-8859-1');
        header("Content-Disposition: attachment; filename={$csv_name}");
    }
}
