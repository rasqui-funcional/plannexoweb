<li class=" has-sub-menu">
    <a href="layouts_menu_top_image.html">
        <div class="icon-w">
            <i class="fas fa-pencil-alt pln-icon-color"></i>
        </div>
        <span class="text-uppercase"><?= $lang['menu']['planning'] ?></span></a>
    <div class="sub-menu-w">
        <div class="sub-menu-header text-uppercase">
            <?= $lang['menu']['planning'] ?>
        </div>
        <div class="sub-menu-icon">
            <i class="ti-pencil pln-icon-color"></i>
        </div>
        <div class="sub-menu-i">
            <ul class="sub-menu">
                <?php if (cannot('spdmBuyer')) { ?>
                    <li>
                        <a href="#" class="gotoback"
                           data-route="/<?= $Country ?>/sku/filters"><?= $lang['menu']['sku'] ?></a>
                    </li>
                    <li>
                        <a href="#" class="gotoback"
                           data-route="/<?= $Country ?>/suggest/filters"><?= $lang['menu']['suggest'] ?></a>
                    </li>

                    <li>
                        <a href="#" class="gotoback"
                           data-route="/<?= $Country ?>/ordercover/filters"><?= $lang['menu']['ordercover'] ?></a>
                    </li>
                    <li>
                        <a href="#" class="gotoback"
                           data-route="/<?= $Country ?>/approvals/list"><?= $lang['menu']['approvals'] ?></a>
                    </li>
                <? } ?>
                <?php if (cannot("supplier")) { ?>
                    <li>
                        <a href="#" class="gotoback"
                           data-route="/<?= $Country ?>/purchaseorder/filters"><?= $lang['menu']['purchaseorder'] ?></a>
                    </li>
                <?php } ?>

                <?php if (TagService::hasTag('fup') && Login::getUserSessionByKey('type') != '2') { ?>
                    <li>
                        <a href="<?= BIOVIEWURL ?>followup/filters">
                            <?= $lang['menu']['followup'] ?>
                            <span class="menu-items--label-new"><?= $lang['menu']['new'] ?></span>
                        </a>
                    </li>
                <?php } ?>

            </ul>
        </div>
    </div>
</li>