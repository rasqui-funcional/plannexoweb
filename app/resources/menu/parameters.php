<li class="has-sub-menu hide">
    <a href="apps_bank.html">
        <div class="icon-w">
            <div class="fa fa-cubes pln-icon-color"></div>
        </div>
        <span>CONFIGURAÇÕES DE ITENS</span></a>
    <div class="sub-menu-w">
        <div class="sub-menu-header">
            CONFIGURAÇÕES DE ITENS
        </div>
        <div class="sub-menu-icon">
            <i class="fa fa-cubes pln-icon-color"></i>
        </div>
        <div class="sub-menu-i">
            <ul class="sub-menu">
                <li>
                    <a href="apps_email.html">Curvas</a>
                </li>
                <li>
                    <a href="apps_support_dashboard.html">Políticas</a>
                </li>
                <li>
                    <a href="apps_support_index.html">Matriz</a>
                </li>
                <li>
                    <a href="apps_crypto.html">Variáveis <strong
                                class="badge badge-danger">Novo</strong></a>
                </li>
            </ul>
        </div>
    </div>
</li>

<li class="has-sub-menu ">
    <a href="#">
        <div class="icon-w">
            <i class="fas fa-sliders-h pln-icon-color"></i>
        </div>
        <span class="text-uppercase"><?= $lang['menu']['params'] ?></span></a>
    <div class="sub-menu-w">
        <div class="sub-menu-header text-uppercase">
            <?= $lang['menu']['params'] ?>
        </div>
        <div class="sub-menu-icon">
            <i class="fas fa-sliders-h pln-icon-color"></i>
        </div>
        <div class="sub-menu-i">
            <ul class="sub-menu">
                <li>
                    <a href="#" class="gotoback"
                       data-route="/<?= $Country ?>/parameters/politics/list"><?= $lang['menu']['polices'] ?></a>
                </li>
                <li>
                    <a href="#" class="gotoback"
                       data-route="/<?= $Country ?>/parameters/calendar/list"><?= $lang['menu']['calendar'] ?></a>
                </li>
                <li>
                    <a href="<?= BIOVIEWURL ?>parameters/emailmodel"><?= $lang['menu']['emails'] ?></a>
                </li>
                <?php if (can("sysadmin", 'login')) { ?>
                    <li>
                        <a href="<?= BIOVIEWURL ?>users"><?= $lang['menu']['users'] ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</li>

<li class="has-sub-menu hide">
    <a href="#">
        <div class="icon-w">
            <div class="ti-settings pln-icon-color"></div>
        </div>
        <span>ADMINISTRAÇÃO</span></a>
    <div class="sub-menu-w">
        <div class="sub-menu-header">
            ADMINISTRAÇÃO
        </div>
        <div class="sub-menu-icon">
            <i class="ti-settings pln-icon-color"></i>
        </div>
        <div class="sub-menu-i">
            <ul class="sub-menu">
                <li>
                    <a href="uikit_modals.html">Usuários <strong
                                class="badge badge-danger">Novo</strong></a>
                </li>
                <li>
                    <a href="uikit_alerts.html">Log</a>
                </li>
                <li>
                    <a href="uikit_grid.html">Perfis</a>
                </li>
                <li>
                    <a href="uikit_progress.html">Planta</a>
                </li>
                <li>
                    <a href="uikit_popovers.html">Apps</a>
                </li>
            </ul>
            <ul class="sub-menu">
                <li>
                    <a href="uikit_tooltips.html">Empresas</a>
                </li>
                <li>
                    <a href="uikit_buttons.html">ExTplorer</a>
                </li>
                <li>
                    <a href="uikit_dropdowns.html">Jobs</a>
                </li>
                <li>
                    <a href="uikit_typography.html">Menus</a>
                </li>
                <li>
                    <a href="uikit_typography.html">Oracle</a>
                </li>
                <li>
                    <a href="uikit_typography.html">Atualização</a>
                </li>
                <li>
                    <a href="uikit_typography.html">Email</a>
                </li>
            </ul>
        </div>
    </div>
</li>

<li class="has-sub-menu hide">
    <a href="#">
        <div class="icon-w">
            <div class="fa fa-eye pln-icon-color"></div>
        </div>
        <span>S&OP</span></a>
    <div class="sub-menu-w">
        <div class="sub-menu-header">
            S&OP
        </div>
        <div class="sub-menu-icon">
            <i class="fa fa-eye pln-icon-color"></i>
        </div>
        <div class="sub-menu-i">
            <ul class="sub-menu">
                <li>
                    <a href="emails_welcome.html">Orçamentos</a>
                </li>
                <li>
                    <a href="emails_order.html">Importar Orçamento</a>
                </li>
                <li>
                    <a href="emails_payment_due.html">Gestão de Planejamento</a>
                </li>
                <li>
                    <a href="emails_forgot.html">Planej. Det.</a>
                </li>
                <li>
                    <a href="emails_activate.html">Histórico de Planejamento</a>
                </li>
                <li>
                    <a href="emails_activate.html">Previsto</a>
                </li>
                <li>
                    <a href="emails_activate.html">Parâmetros de Planejamento</a>
                </li>
                <li>
                    <a href="emails_activate.html">Ciclos</a>
                </li>
                <li>
                    <a href="emails_activate.html">Cadastro de Índice</a>
                </li>
                <li>
                    <a href="emails_activate.html">Permissões de Edição</a>
                </li>
            </ul>
        </div>
    </div>
</li>