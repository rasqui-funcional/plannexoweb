<li class="selected has-sub-menu">
    <a href="index.php">
        <div class="icon-w">
            <i class="fas fa-chart-bar pln-icon-color"></i>
        </div>
        <span class="text-uppercase"><?= $lang['menu']['reports'] ?></span></a>
    <div class="sub-menu-w">
        <div class="sub-menu-header text-uppercase">
            <?= $lang['menu']['reports'] ?>
        </div>
        <div class="sub-menu-icon">
            <i class="fa fa-bar-chart-o pln-icon-color"></i>
        </div>
        <div class="sub-menu-i">
            <ul class="sub-menu">
                <li>
                    <a href="#" class="gotoback"
                       data-route="/<?= $Country ?>/dashboard"><?= $lang['menu']['dashboard'] ?></a>
                </li>
                <?php if (!can("supplier")) { ?>
                    <li>
                        <a href="/<?= $Country ?>/resume/filters"><?= $lang['menu']['resume'] ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</li>