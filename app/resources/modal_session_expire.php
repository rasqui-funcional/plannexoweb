<div class="modal fade" id="session" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="session_expire" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
                
      </div>
      <div class="modal-body">
        <div class="d-flex flex-column justify-content-center align-items-center">
            <h5 class="to-hide"><?= $lang['sessao']['expirando'] ?></h5>
            <h2 id="session_message"><?= $lang['sessao']['tempo'] ?></h2>
        </div>
      </div>
      <div class="modal-footer d-flex justify-content-center">
          <button type="button" id="expiration_confirm" class="btn pln-btn-orange hide"><?= $lang['modal']['ok'] ?></button>
      </div>
    </div>
  </div>
</div>