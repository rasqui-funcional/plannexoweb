<tfoot>
    <tr>
        <?php
        foreach ($ViewData['exibitions_columns'] as $column) {
            $hidden = isset($ViewData['header_cols'][$column]) ? null : 'pln-invi';    
        ?>
        
        <th class="<?php echo $hidden ?>"><?php echo $ViewData['footer']['data'][$column] ?></th>
        <?php }?>
    </tr>
    <input type="hidden" id="hdn_total_items" value="<?=$ViewData['footer']['count']?>" />
    <input type="hidden" id="total_page_number" value="<?php echo $ViewData['footer']['pages'] ?>">
</tfoot>
