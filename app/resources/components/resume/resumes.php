<tbody>
    <? $coverage_columns = [
        'inv_available_money',
        'inv_total_money',
        'qty_min_out_money',
        'qty_target_money',
        'qty_max_out_money',
        'qty_excess',
        'qty_min_out_ori_money',
        'qty_max_out_ori_money',
        'qty_min_erp_money',
        'qty_max_erp_money',
    ] ?>

    <input type="hidden" name="cod_estab" value="<?= $ViewData["cod_estab"] ?>">
    <?php
    foreach ($ViewData['data_table'] as $data) { ?>
    <tr class="object_table_tr">
        <?php foreach ($ViewData['exibitions_columns'] as $column) {
            $hidden = isset($ViewData['header_cols'][$column]) ? null : 'pln-invi';
            switch($column){
                case 'inv_level':
                    echo ViewImage::invLevelImg(
                        $data[$column]['value']
                        , $lang
                        , ['class' => $hidden, 'data-column' => 'inv_level', 'data-value' => $data[$column]['value']]
                    );
                    break;

                case 'sit_sku':
                if ($data[$column]['name'] == "1.00") {
                    $class = "'text-success'";
                    $text = $lang['filtro']['card_status']['ativo'];
                } else {
                    $class = "'text-danger'";
                    $text = $lang['filtro']['card_status']['inativo'];
                }
                ?>
                <td class="<?php echo $hidden ?>" data-column="sit_sku" data-value="<?php echo $data[$column]['value'] ?>">
                    <span class="<?php echo $class?>"><?php echo $text?></span>
                </td>
                <?php
                 case 'id_profile_fk':
                 ?>
                 <td class="text-truncate pln-td-mw-100 <?php echo $hidden ?>" data-column="id_profile" data-value="<? echo $data[$column]['value'] ?>"> <span title="<? echo $data[$column]['name'] ?>"><? echo $data[$column]['name'] ?></span></td>
                 <?
                break;
                default:
                ?>
                <? if (in_array($column, $coverage_columns)) { ?>
                    <td class="nowrap <?php echo $hidden ?>"  <?php echo (in_array($column, $ViewData['redirect_columns']) ? "data-column='{$column}' data-value='{$data[$column]['value']}' " : null )  ?>>
                        <span title="<? echo $data[$column]['name'] ?>"><? echo $data[$column]['name'] ?></span>
                    </td>
                <? } else { ?>
                    <td class="text-truncate pln-td-mw-100 <?php echo $hidden ?>"  <?php echo (in_array($column, $ViewData['redirect_columns']) ? "data-column='{$column}' data-value='{$data[$column]['value']}' " : null )  ?>>
                        <span title="<? echo $data[$column]['name'] ?>"><? echo $data[$column]['name'] ?></span>
                    </td>
                <? } ?>
                <?
                break;
            }
        ?>
        <?php } ?>
    </tr>
    <?php } ?>
</tbody>