<tbody id="tablePurchaseOrderListBody">
    <?php foreach ($ViewData['data_table'] as $data): ?>
    <tr class="object_table_tr <?can('spdmBuyer') ?: print 'table_tr'?>" id="<?php echo $data['ID_SKU_PK']; ?>" data-id-sku="<?php echo $data['ID_SKU_PK']; ?>"
        data-lt="<?php echo $data['LT_TOTAL_OUT']?>"
        data-out6="<?php echo $data['OUT6'] ? Utilities::dataFormatter('OUT6', $data['OUT6']) : 0 ?>">

        <?php foreach ($ViewData['header_cols'] as $column):
            switch($column):
                case 'INV_LEVEL':
                    echo ViewImage::invLevelImg($data[$column]['value'], $lang);
                    break;
                case 'DESC_ITEM': ?>
                    <td class="text-truncate pln-td-mw-200">
                        <span data-toggle="tooltip"
                                title="<?= $data[$column];?>">
                            <?= $data[$column];?>
                        </span>
                    </td>
                    <? break;
                case 'SIT_SKU':
                ?>
                <td class="text-center">
                    <?php
                    $class = 'text-danger';
                    $status = $lang['filtro']['card_status']['inativo'];

                    if($data[$column] == '1.00') {
                        $class = 'text-success';
                        $status = $lang['filtro']['card_status']['ativo'];
                    }
                    ?>
                    <span class="<?php echo $class ?>">
                        <?php echo $status ?>
                    </span>
                </td>
                <?  break;
                default:
                    $element_params = View::genericWidthTable($data[$column]);
                ?>
                    <td class="<?= $element_params['td_class'] ?>">
                        <span data-toggle="<?= $element_params['data_toggle'] ?>"
                              title="<?= $element_params['title'] ?>">
                            <?= $data[$column] ?>
                        </span>
                    </td>
                <? break;
            endswitch;
        endforeach;
        ?>
    </tr>
    <? endforeach; ?>
</tbody>
