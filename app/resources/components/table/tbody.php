<tbody id="ListBody">
    <?php foreach ($ViewData['data_table'] as $data) { ?>
        <tr class="object_table_tr">
            <?php foreach ($ViewData['header_cols'] as $column) { ?>
                    <td class="text-center"><?php echo $data[$column] ?></td>
            <? } ?>
        </tr>
    <? } ?>
</tbody>
