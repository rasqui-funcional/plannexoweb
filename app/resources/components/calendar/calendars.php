<tbody id="CalendarListBody">
    <?php
    array_unshift($ViewData['header_cols'],'check');
    foreach ($ViewData['data_table'] as $data) { ?>
        <tr data-id="<?php echo $data['id_calendar_pk'] ?>" class="object_table_tr">
            <?php foreach ($ViewData['header_cols'] as $column) { ?>
                <td class="text-truncate pln-td-mw-100">
                    <?php if ($column === 'check') { ?>
                        <span style="vertical-align: sub;">
                            <input class="input_chk" type="checkbox">
                        </span>
                    <?php } else { ?>
                        <?php echo $data[$column] ?>
                    <?php } ?>
                </td>
            <?php } ?>    
        </tr>
    <?php } ?>
</tbody>
