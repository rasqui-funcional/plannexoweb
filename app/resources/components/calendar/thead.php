<thead>
    <tr class="tablePurchaseOrderList_tr">
        <?php
        $is_first_column = true;
        $header_name = false;
        $header_abrev = false;
        $header_id = false;
        array_unshift($ViewData['header_cols'], 'check');

        foreach ($ViewData['header_cols'] as $col_id => $col_name) {
            if (is_int($col_id)) {
                $header_name = $lang['column'][strtolower($col_name)];
                $header_abrev = ( $lang['column']['abrev'][strtolower($col_name)] ? $lang['column']['abrev'][strtolower($col_name)] : false);
                $header_id = $col_name;
            } elseif($col_name) {
                $header_name = $col_name;
                $header_abrev = ( $lang['column']['abrev'][strtolower($col_id)] ? $lang['column']['abrev'][strtolower($col_id)] : false);
                $header_id = $col_id;
            } else {
                $header_name = $lang['column'][strtolower($col_id)];
                $header_abrev = ( $lang['column']['abrev'][strtolower($col_id)] ? $lang['column']['abrev'][strtolower($col_id)] : false);
                $header_id = $col_id;
            }
            ?>
            <?php if($header_id === 'check') { ?>
                <th style="width: 1%">
                    <!--span style="vertical-align: sub;"><input id="chk_all" type="checkbox"></span-->
                </th>
            <?php } else { ?>
                <th>
                    <div class="d-flex flex-row pln-cur-pointer order-col" data-id="<?=$header_id?>">
                        <span data-toggle="tooltip" title="<?php echo $header_name ?>">
                            <?= ( $header_abrev ? $header_abrev : $header_name) ?>
                        </span>
                        
                        <i class="fa fa-sort<?php echo isset($ViewData['order'][$header_id]) ? "-" . $ViewData['order'][$header_id] : "" ?> pln-link-color ml-1 align-self-center"></i>
                    </div>
                </th>
            <?php } ?>
        <?php } ?>
    </tr>
</thead>