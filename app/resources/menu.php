<?php include('loader.php') ?>
<?php include('modal_session_expire.php') ?>
<div class="all-wrapper solid-bg-all">
    <div class="layout-w">
        <div class="overlay"></div>
        <!--------------------
START - Main Menu
-------------------->
        <div id="menu-retratil"
             class="menu-w d-color-scheme-light color-style-default menu-position-side menu-side-left menu-layout-compact sub-menu-style-inside sub-menu-color-light selected-menu-color-light menu-activated-on-click menu-has-selected-link">
            <div class="pln-logo-menu">
                <a href="#" class="gotoback" data-route="/<?php echo $Country ?>/home">
                    <div><img width="120" src="/resources/img/plannexo-color.svg"></div>
                </a>
            </div>

            <h1 class="menu-page-header">
                Page Header
            </h1>
            <ul class="main-menu">
                <li class="sub-header">
                    <span></span>
                </li>
                <!--Relatórios-->
                <?
                if (cannot('spdmBuyer')) {
                    //Relatórios
                    include_once 'menu/reports.php';

                }
                //Planejamento
                include_once 'menu/planning.php';

                if (cannot("supplier") &&
                    cannot('spdmBuyer') &&
                    cannot('spdmPharmaceutical') &&
                    cannot('spdmPlanner')
                ) {
                    include_once 'menu/parameters.php';
                } ?>
            </ul>
        </div>
        <!--------------------
        END - Main Menu
        -------------------->
        <div class="content-w">
            <!--------------------
            START - Top Bar
            -------------------->
            <div class="top-bar pln-background-orange d-flex justify-content-between">
                <!--------------------
                START - Top Menu Controls
                -------------------->
                <div id="open_menu"
                     class="top-menu-out-inside d-flex justify-content-start align-items-center pln-cur-pointer ml-3">
                    <i class="fas fa-bars pln-menu-bars"></i>
                    <div class="ml-2 text-uppercase"><?= $lang['menu']['title'] ?></div>
                </div>
                <div class="img-logo">
                    <img src="/resources/img/plannexo-white.svg" alt="Plannexo" width="120">
                </div>

                <div class="top-menu-controls">
                    <div class="logged-user-w pnl-logged-user-w d-flex align-items-center">
                        <div class="logged-user-i">
                            <?php if (Login::getUserSession()) : ?>
                                <div class="dropdown float-right">
                                    <a class="btn btn-dropdown dropdown-toggle" href="#" role="button"
                                       id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="true">
                                        <?= Login::getUserSessionByKey('company_name') ?>
                                        <span class="ml-1"><?= Login::getUserSessionByKey('initials') ?></span>
                                    </a>

                                    <div class="pln-drop-down-menu dropdown-menu dropdown-menu-right p-0"
                                         aria-labelledby="dropdownMenuLink">
                                        <div class="pln-dropdown-item dropdown-item">
                                            <p><?= Login::getUserSessionByKey('name') ?></p>
                                            <small><?= Login::getUserSessionByKey('email') ?></small>
                                        </div>

                                        <div class="pln-dropdown-list">
                                            <div class="pln-dropdown-item dropdown-item dropdown-item-active">
                                                <p><?= Login::getUserSessionByKey('company_name') ?></p>
                                                <small><?= Login::getUserSessionByKey('register') ?></small>
                                                <a class="stretched-link" href="#"></a>
                                            </div>
                                        </div>

                                        <div class="pln-dropdown-item dropdown-item d-flex justify-content-end">
                                            <a class="btn-sm mr-3 pln-link-color gotoback hide" href="#"
                                               data-route="/<?php echo $Country ?>/edit">
                                                Editar conta
                                            </a>

                                            <a class="btn pln-btn-primary gotoback" href="#"
                                               data-route="/<?php echo $Country ?>/logout">
                                                <i class="fas fa-sign-out-alt mr-1"></i><?php echo $lang['sair'] ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!--------------------
                    END - User avatar and menu in secondary top menu
                    -------------------->
                </div>
                <!--------------------
                END - Top Menu Controls
                -------------------->
            </div>
            <!--------------------
            END - Top Bar
            -------------------->
