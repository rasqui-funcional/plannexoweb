let gulp = require('gulp');
let uglify = require('gulp-uglify-es').default;
let concat = require('gulp-concat');
let sourcemaps = require('gulp-sourcemaps');
const commons = {
    utilities: 'js/utilities.js',
    serialize: 'js/serialize.js',
    searchFilter: 'js/search-filter.js',
    throttledresize: 'js/throttledresize.js',
    jqueryUi: 'js/dragdrop/jqueryUI.js',
    cookie: 'js/dragdrop/cookie.js',
    jsFunctions: 'js/dragdrop/jsfunctions.js',
    multisortable: 'js/jquery.multisortable.js',
    moment: 'js/moment/moment.js',
    tooltip: 'js/bootstrap/tooltip.js',
    popover: 'js/bootstrap/popover.js',
    momentLocales: 'js/moment.locales.js',
    twbsPagination: 'js/twbsPagination.js',
    pagination: 'js/pagination/pagination.js',
    routes: 'js/pagination/routes.js',
    daterangepicker: 'js/bootstrap-daterangepicker/daterangepicker.js',
    fullscreen: 'js/fullscreen/fullscreenController.js',
    modal: 'js/bootstrap/modal.js',
    tab: 'js/bootstrap/tab.js',
    dropdown: 'js/bootstrap/dropdown.js',
    abstractInfoSku: 'js/info_sku/AbstractInfoSku.js',
    infoSku: 'js/info_sku/infoSkuController.js',
    erp: 'js/info_sku/erp.js',
    basicInformation: 'js/info_sku/basicInformationController.js',
    calendar: 'js/info_sku/calendarController.js',
    interdependence: 'js/info_sku/interdependenceController.js',
    policy: 'js/info_sku/policyController.js',
    childrenItem: 'js/info_sku/childrenItemController.js',
    log: 'js/info_sku/logController.js',
    alert: 'js/bootstrap/alert.js',
}

gulp.task('Dashboard', function() {
    return gulp.src([        
        commons.utilities,
        commons.serialize,
        commons.searchFilter,
        commons.throttledresize,
        'js/dashboard/filter.js',
        'js/dashboard.js',
        'js/dashboard_filter.js',
        'js/dashboard_data.js',
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-dashboard-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('ResumeFilters', function() {
    return gulp.src([
        commons.jqueryUi,
        commons.cookie,
        commons.jsFunctions,
        commons.multisortable,
        commons.tooltip,
        commons.popover,
        commons.utilities,
        commons.serialize,
        commons.searchFilter,
        'js/resume/filter.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-resume-filters-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('ResumeList', function() {
    return gulp.src([
        commons.utilities,
        commons.twbsPagination,
        commons.pagination,
        commons.routes,
        'js/resume/list.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-resume-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
 });

 gulp.task('SkuFilters', function() {
    return gulp.src([
        commons.jqueryUi,
        commons.cookie,
        commons.jsFunctions,
        commons.multisortable,
        commons.tooltip,
        commons.popover,
        commons.daterangepicker,
        commons.utilities,
        commons.serialize,
        commons.searchFilter,
        'js/sku/filter.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-sku-filters-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
 });

gulp.task('SkuTestFilters', function() {
    return gulp.src([
        commons.jqueryUi,
        commons.cookie,
        commons.jsFunctions,
        commons.multisortable,
        commons.tooltip,
        commons.popover,
        commons.daterangepicker,
        commons.utilities,
        commons.serialize,
        commons.searchFilter,
        'js/sku-test/filter.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('all-sku-test-filters-min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('js/uglify'))
});

 gulp.task('SkuList', function(){
    return gulp.src([
        commons.twbsPagination,
        'js/tableHeadFixer.js',
        commons.throttledresize,
        commons.fullscreen,
        commons.jqueryUi,
        commons.daterangepicker,
        commons.modal,
        commons.tab,
        commons.tooltip,
        'js/sku/list.js',
        commons.serialize,
        commons.utilities,
        commons.abstractInfoSku,
        commons.alert,
        commons.infoSku,
        commons.erp,
        commons.basicInformation,
        commons.calendar,
        commons.interdependence,
        commons.policy,
        commons.childrenItem,
        commons.log
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-sku-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
 });

gulp.task('SkuTestList', function(){
    return gulp.src([
        commons.twbsPagination,
        'js/tableHeadFixer.js',
        commons.throttledresize,
        commons.fullscreen,
        commons.jqueryUi,
        commons.daterangepicker,
        commons.modal,
        commons.tab,
        commons.tooltip,
        'js/sku-test/list.js',
        commons.serialize,
        commons.utilities,
        commons.abstractInfoSku,
        commons.alert,
        commons.infoSku,
        commons.erp,
        commons.basicInformation,
        commons.calendar,
        commons.interdependence,
        commons.policy,
        commons.childrenItem,
        commons.log
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('all-sku-test-list-min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('js/uglify'))
});

 gulp.task('SuggestFilters', function() {
    return gulp.src([
        commons.jqueryUi,
        commons.cookie,
        commons.jsFunctions,
        commons.multisortable,
        commons.tooltip,
        commons.popover,
        commons.utilities,
        commons.serialize,
        commons.searchFilter,
        'js/suggest/filter.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-suggest-filters-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('SuggestList', function() {
    return gulp.src([
        commons.twbsPagination,
        'js/tableHeadFixer.js',
        commons.throttledresize,
        commons.fullscreen,
        commons.pagination,
        commons.routes,
        commons.jqueryUi,
        commons.daterangepicker,
        commons.tab,
        commons.modal,
        'js/suggest/list.js',
        commons.utilities,
        commons.abstractInfoSku,
        commons.alert,
        commons.infoSku,
        commons.erp,
        commons.basicInformation,
        commons.calendar,
        commons.interdependence,
        commons.policy,
        commons.childrenItem,
        commons.log,
        'js/suggest/manualsuggest.js',
        'js/i18n/**/suggest.js',
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-suggest-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('OrderCoverFilters', function() {
    return gulp.src([
        commons.tooltip,
        commons.utilities, 
        commons.serialize, 
        commons.searchFilter, 
        'js/ordercover/filter.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-ordercover-filters-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('OrderCoverList', function() {
    return gulp.src([
        commons.utilities,
        commons.twbsPagination,
        commons.pagination,
        commons.routes,
        commons.serialize,
        commons.daterangepicker,
        'js/ordercover/list.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-ordercover-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('OrderCoverSuggestionsList', function() {
    return gulp.src([
        commons.utilities,
        commons.twbsPagination,
        commons.pagination,
        commons.routes,
        commons.serialize,
        commons.daterangepicker,
        'js/ordercover/suggestions/list.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-ordercover-suggestions-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});


gulp.task('ApprovalsList', function() {
    return gulp.src([
        commons.utilities,
        commons.twbsPagination,
        commons.pagination,
        commons.routes,
        commons.serialize,
        'js/approvals/list.js',
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-approvals-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('ApprovalsSuggList', function() {
    return gulp.src([
        commons.twbsPagination,
        commons.utilities,
        commons.serialize,
        commons.pagination,
        commons.routes,
        'js/approvals/suggest/list.js',
        'js/approvals/suggest/duplicate.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-approvals-sugg-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('PurchaseOrderFilter', function() {
    return gulp.src([
        commons.utilities,
        commons.serialize,
        commons.searchFilter,        
        commons.tooltip,
        commons.daterangepicker,
        'js/notificationSweetAlert.js',
        'js/editForm.js',
        'js/purchaseorder/filter.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-purchaseorder-filters-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('PurchaseOrderList', function() {
    return gulp.src([
        commons.utilities,
        commons.twbsPagination,
        commons.pagination,
        commons.routes,
        commons.serialize,
        'js/purchaseorder/list.js',
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-purchaseorder-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('PurchaseOrderOrderList', function() {
    return gulp.src([
        commons.abstractInfoSku,
        commons.alert,
        commons.infoSku,
        commons.erp,
        commons.basicInformation,
        commons.calendar,
        commons.interdependence,
        commons.policy,
        commons.childrenItem,
        commons.log,
        commons.utilities,
        commons.jqueryUi,
        commons.twbsPagination,
        commons.fullscreen,
        commons.pagination,
        commons.routes,
        commons.serialize,
        commons.tooltip,
        commons.tab,
        commons.modal,
        'js/purchaseorder/orders/list.js',
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-purchaseorder-orderlist-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('PoliticsList', function() {
    return gulp.src([
        commons.jqueryUi,
        'js/date_i18n/datepicker-pt-BR.js',
        commons.utilities,
        commons.twbsPagination,
        commons.searchFilter,
        commons.pagination,
        commons.routes,
        commons.serialize,
        'js/parameters/politics/list.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-politics-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('CalendarList', function() {
    return gulp.src([
        commons.utilities,
        commons.twbsPagination,
        'js/bootstrap-year-calendar/bootstrap-year-calendar.min.js',
        'js/bootstrap-year-calendar/using/*.js',
        commons.pagination,
        commons.routes,
        'js/parameters/calendar/list.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-calendar-list-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('CommonJs', function() {
    return gulp.src([        
        'js/i18n/i18n.js',
        'js/i18n/**/global.js',
        'js/i18n/**/infoSku.js',
        'js/i18n/**/suggest.js',
        'js/jquery/jquery.min.js',
        'js/i18n/country.js',
        'js/popper.js/popper.min.js',
        'js/sweetalert2.js',
        'js/bootstrap/util.js',
        commons.modal,
        commons.dropdown,
        commons.moment,
        commons.momentLocales,
        'js/menu.js',
        'js/header.js',
        'js/session.js',
        'js/loader-plannexo.js',
        
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('all-commons-min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('js/uglify'))
});

gulp.task('SuggestTestFilters', function() {
    return gulp.src([
        commons.jqueryUi,
        commons.cookie,
        commons.jsFunctions,
        commons.multisortable,
        commons.tooltip,
        commons.popover,
        commons.utilities,
        commons.serialize,
        commons.searchFilter,
        'js/suggest-test/filter.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('all-suggest-test-filters-min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('js/uglify'))
});

gulp.task('SuggestTestList', function() {
    return gulp.src([
        commons.twbsPagination,
        'js/tableHeadFixer.js',
        commons.throttledresize,
        commons.fullscreen,
        commons.pagination,
        commons.routes,
        commons.jqueryUi,
        commons.daterangepicker,
        commons.tab,
        commons.modal,
        'js/suggest-test/list.js',
        commons.utilities,
        commons.abstractInfoSku,
        commons.alert,
        commons.infoSku,
        commons.erp,
        commons.basicInformation,
        commons.calendar,
        commons.interdependence,
        commons.policy,
        commons.childrenItem,
        commons.log,
        'js/suggest/manualsuggest.js',
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('all-suggest-test-list-min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('js/uglify'))
});

gulp.task('default', gulp.parallel(
    'Dashboard', 
    'CommonJs', 
    'ResumeFilters', 
    'ResumeList', 
    'SkuFilters', 
    'SkuList', 
    'SuggestFilters',
    'SuggestList',
    'OrderCoverFilters',
    'OrderCoverList',
    'OrderCoverSuggestionsList',
    'ApprovalsList',
    'ApprovalsSuggList',
    'PurchaseOrderFilter',
    'PurchaseOrderList',
    'PurchaseOrderOrderList',
    'PoliticsList',
    'CalendarList',
    'SuggestTestFilters',
    'SuggestTestList',
    'SkuTestFilters',
    'SkuTestList'
));

/*WATCHERS*/
gulp.task('watch', function(){
    gulp.watch(['js/menu.js', 'js/header.js', 'js/session.js', 'js/loader-plannexo.js', 'js/i18n/**/*.js'], gulp.series(['CommonJs']));
    gulp.watch('js/dashboard/*.js', gulp.series(['Dashboard']));
    gulp.watch('js/resume/*.js', gulp.series(['ResumeFilters', 'ResumeList']));
    gulp.watch('js/sku/*.js', gulp.series(['SkuFilters', 'SkuList']));
    gulp.watch('js/info_sku/*.js', gulp.series(['SkuList', 'SuggestList', 'PurchaseOrderOrderList']));
    gulp.watch('js/suggest/*.js', gulp.series(['SuggestList', 'SuggestFilters']));
    gulp.watch('js/ordercover/**/*.js', gulp.series(['OrderCoverFilters', 'OrderCoverList', 'OrderCoverSuggestionsList']));
    gulp.watch('js/approvals/**/*.js', gulp.series(['ApprovalsList', 'ApprovalsSuggList']));
    gulp.watch('js/purchaseorder/**/*.js', gulp.series(['PurchaseOrderFilter', 'PurchaseOrderList', 'PurchaseOrderOrderList',]));
    gulp.watch('js/parameters/politics/*.js', gulp.series(['PoliticsList']));
    gulp.watch('js/parameters/calendar/*.js', gulp.series(['CalendarList']));
    gulp.watch('js/suggest-test/*.js', gulp.series(['SuggestTestList', 'SuggestTestFilters']));
    gulp.watch('js/sku-test/*.js', gulp.series(['SkuTestList', 'SkuTestList']));
});