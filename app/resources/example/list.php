<?php 
$JS= [    
    'utilities',
    'twbsPagination',
    'tableHeadFixer',
    'pagination/pagination',
    'example/list'
];
?>

<div class="content-i">
    <div class="content-box">
        <div style="" class="element-wrapper">
            <a href="/<?=$Country?>/resume/filters" class="pln-link-color"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Criar lang para VOLTAR</a>
            <div class="d-flex flex-row justify-content-between align-items-center">
                <div>
                    <h4 class="pln-font-color">Criar lang para titulo</h4>
                </div>
                <div class="d-flex flex-column">
                    <button type="button" id="saveData" class="btn pln-btn-orange"><?php echo $lang['parametros']['salvar'] ?></button>
                </div>
            </div>
            <div class="element-header"></div>
            
            <div class="element-box">                
                
                <div id="table_insert">
                    
                </div>
                <div class="d-flex mt-1 align-items-start pln-items-search" style="display: none !important">
                    
                    <div class="d-flex flex-row align-items-center">
                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?=$lang['sugcompras_lista']['pagina']['qtd_a']?></span>
                            <span id="page_items_to"></span>
                            <span><?=$lang['sugcompras_lista']['pagina']['qtd_de']?></span>
                            <span id="page_items_total"></span>
                        </span>
                        <div class="">
                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>
                            <small>
                                <?=$lang['sugcompras_lista']['pagina']['qtd_total']?> <span id="spn_total_page_number"></span>
                            </small> 
                        </div>
                    </div>
                                            
                    <div class="d-flex flex-row align-items-center ml-3">
                        <span class="pln-bold-600"><?=$lang['sugcompras_lista']['pagina']['titulo']?></span>
                        <span class="ml-1"><input type="text" maxlength="10" id="goto_pagenumber" class="form-control form-control-sm input_just_number form-control form-control-sm-sm pln-input-60" /></span>
                        <span class="ml-1"><button type="button" id="goToPageNumber" class="btn pln-btn-orange" /><?=$lang['sugcompras_lista']['buscar']?></span>
                    </div>
                    <div class="d-flex flex-row align-items-center ml-auto">
                        <span class="pln-bold-600"><?=$lang['sugcompras_lista']['visualizar']?></span>
                        <span class="ml-1">
                            <select id="items_per_page" class="form-control form-control-sm">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </span>
                        <span class="ml-1"><?=$lang['sugcompras_lista']['por_pagina']?></span>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
<div class="modal loading" data-keyboard="false" data-backdrop="static" id="modal_loading" tabindex="-1" role="dialog" aria-labelledby="botao_loading" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document" style="margin-top: 15%">
        <div class="modal-content">
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated actived" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
</div>	
<button id="btn_loading" type="button" data-toggle="modal" data-target="#modal_loading" hidden></button>

