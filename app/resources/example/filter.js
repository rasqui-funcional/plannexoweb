class exampleController{
    constructor(){
        /************INPUTS TO CHANTE RANGE DATE**********/
        this.formSerielized;
        this.loading = jQuery("#btn_loading");
        
        /************************/                                        		
        this.listenerBtns();
        
        //if the page have a input daterange, this line will be required
        //this.daterangepicker();        
    }

    listenerBtns(){
        
        //controlling the label of delete button
        jQuery("#modal_edit").on("show.bs.modal", function(){
            jQuery("#select_filtro").val() == "" ? jQuery("#confirm_delete").addClass("invisible") : jQuery("#confirm_delete").removeClass("invisible");
        });
        
        //controlling the validator field when modal closes
        jQuery("#modal_edit").on("hide.bs.modal", function(){
            jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});
            jQuery("#filter_nome").val("");
        });

        jQuery('.progress-bar').css("background-color","#F96737");
		jQuery('.progress .progress-bar').css("width",function() {
			return jQuery(this).attr("aria-valuenow") + "%";
	      	
        });
        
        jQuery("[data-toggle=tooltip]").tooltip();                       
       
        jQuery('.chk_check_all').click(function () {    
            jQuery(this).parent().parent().parent().find('input:checkbox').prop('checked', this.checked);    
        });
        
        jQuery("#clean_form").on("click", (element)=>{
            this.cleanForm();
        });

        //send to list page
        jQuery("#btn_search").on("click", ()=>{
            this.search();
        });

        //confirm remove item from database
        jQuery("#confirm_delete").on("click", ()=>{
            this.confirmDeleteFilter();
        });
                
        //My filter change event
        jQuery("#select_filtro").on("change", (element)=>{
			this.cleanForm();
						
			if(jQuery(element.currentTarget).val() == ""){
				//$("#save_filter").prop("disabled",true);
				jQuery("#btn_filtro_text").text("Salvar filtro");
				jQuery("#filter_nome").val("");
				jQuery("#modal_titulo").text("Novo Filtro");				
				jQuery("#btnfiltro_excluir").hide();
				jQuery("#filter_perfil_Publico").prop("checked", true);
			}else{
				this.loading.click();
				jQuery("#btn_filtro_text").text("Editar filtro");
				jQuery("#filter_nome").val(jQuery("select[name=select_filtro] option:selected").text());
				jQuery("#modal_titulo").text("Edição de Filtro");				
				jQuery("#btnfiltro_excluir").show();

				$.ajax({
					url: '/br/ajax/filter/getFilterById',
					type: 'POST',
					dataType: 'json',
					data: {
						filter_id: jQuery(element.currentTarget).val()
					},
					success: (response)=> {	
						this.editForm(response.data[0]);
						this.loading.click();
						//$("#save_filter").prop("disabled",false);
					},
					error: function (error) {
						$("#btn_loading").click();
						console.log("error", error)	
					}
				});
			}
        });
        
        jQuery("#save_filter").on("click", (element)=>{
            this.saveFilter();
        });
    }

    saveFilter(){
        
        if(jQuery("#filter_nome").val() == ""){
            jQuery("#filter_nome").css({"border": "1px solid red"});
            return false;
        }else{
            jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});
            
        }
        jQuery("#save_filter").attr("disabled",true).find('i').removeClass('pln-invi');
        jQuery("#spn_save").html("Salvando");		
        this.setForm();		


        let perfil = jQuery("input:radio[name=filter_perfil]:checked").val(),
                name = jQuery("#filter_nome").val(),
            view_id = jQuery("#spn_filter_id").attr("data-original-title"),
            filter_id = jQuery("#select_filtro").val();
                
        let objPost = {};
            objPost.url =  ( filter_id == "" ? '/br/ajax/filter/saveFilter' : '/br/ajax/filter/updateFilter');
            objPost.verb = ( filter_id == "" ? 'save' : 'update');
            objPost.type = 'POST';
            objPost.data = {
                filter_perfil: perfil,
                filter_nome: name,
                filter_data: this.formSerielized,
                filter_tela_id: view_id,
                filter_id: filter_id
            };
            this.filterService(objPost);		
        
    }

    confirmDeleteFilter(){
		const swalWithBootstrapButtons = swal.mixin({
			confirmButtonClass: "btn btn-danger mr-2",
			cancelButtonClass: "btn btn-default",
			buttonsStyling: false
		  });
		swalWithBootstrapButtons({			
			title: "Deletar filtro?",
			type: 'warning',
			showCancelButton: true,						
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		  }).then((result) => {
			if (result.value) {
				this.deleteFilter();
			}else{
				jQuery("#confirm_delete").find('span').html('Deletar');
				jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
				swal.close();
			}
		  })
    }
    
    deleteFilter(){
		jQuery("#confirm_delete").find('span').html('Deletando');
		jQuery("#confirm_delete").find('i').removeClass('fa fa-times pln-danger').addClass('fa fa-spin fa-spinner');
		let name = jQuery("#filter_nome").val(),
			filter_id = jQuery("#select_filtro").val(),
			objPost = {};
				objPost.url = '/br/ajax/filter/deleteFilter';
				objPost.verb = 'delete';
				objPost.type = 'POST';
				objPost.data = {				
					filter_nome: name,				
					filter_id: filter_id
				};
			this.filterService(objPost);	
	}

    setForm(){
		this.formSerielized = JSON.parse(jQuery('#formulario').serializeJSON());		
		jQuery("#filter_temp").val(JSON.stringify(this.formSerielized));
    }
    
    editForm(formulario){

		if(formulario.FILTER_PERFIL == "Privado"){
			jQuery("#filter_perfil_Privado").prop("checked", true);
		}else{
			jQuery("#filter_perfil_Publico").prop("checked", true);
		}

		let form = document.getElementById('formulario');
		let json = JSON.parse(formulario.FILTER_DATA);
		
		for(let name in json){
			
			//se for um array, percorre seus valores
			if( typeof(json[name]) == "object"){
				json[name].forEach(function(item){
                    
                    let field = document.getElementById(name + "_"+ item);
                    
                    try{
                        field.checked = true;
                    }catch(err){
                        console.log(err)
                    }
				});
			}

			let field = form.querySelector("[name='" + name+"']");
			
			if(field){
				switch(field.type){
					
					case 'radio':
						field = form.querySelector("[name=" + name + "][value='"+json[name]+"']");
						field.checked = true;
					break;					
					default:					
						field.value = json[name];
				}				
			}
        }
        
        jQuery(".select_date").change();
        //set the daterange on datepicker plugin
        //if the page have a input daterange, this line will be required
        //this.setDatesOnEdit(json.date_req_order_cond, json.dr_date_req_order_cond.split(" "), "#input_daterange_date_req");		        
        
    }

    //if the page have a input daterange, this line will be required
    /*setDatesOnEdit(dateType, dateRange, inputId){
        if(dateType != undefined && dateType != null && dateType != false){
			if( dateType == "INTERVALO"){				
				jQuery(inputId).data('daterangepicker').setStartDate(dateRange[0]);
				jQuery(inputId).data('daterangepicker').setEndDate(dateRange[2]);
			}
		}
    }*/

    search(){
		document.formulario.submit();
	}

    //responsável por enviar requisições POST ao banco de dados e tratar os retornos dependendo do verbo utilizado
	filterService(postObj){
		$.ajax({
			url: postObj.url,
			type: postObj.type,
			dataType: 'json',
			data: postObj.data,
			success: (response)=> {
				if(!response.error){
					switch(postObj.verb){
						//se for delete, trato o ícone de deleção, removo o item do <select> e fecho a modal
						case 'delete':						
							jQuery("#confirm_delete").find('span').html('Deletar');
							jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
							jQuery("#select_filtro").find('option[value='+postObj.data.filter_id+']').remove();
							jQuery("#modal_edit").modal("hide");
							jQuery("#btn_filtro_text").text("Salvar filtro");
						break;
						//se for novo registro, trato o ícone de salvamento
						case 'save':							
							//popula o select com o novo filtro criado
							this.fillFilter(response.data);
							this.setBtnSave()
                            jQuery("#modal_edit").modal("hide");                            
						break;
						//se for edição de registro, trato o ícone de salvamento e atualizo o nome no <select>
						case 'update':
							jQuery("#select_filtro").find('option[value='+postObj.data.filter_id+']').text(postObj.data.filter_nome);
							this.setBtnSave();
							jQuery("#modal_edit").modal("hide");
						break;
					}					
				}else{
					alert('Este serviço está indisponível no momento, caso persista, entre em contato com o suporte');
				}
			},
			error: (error)=> {
				this.setBtnSave();				
				alert('Este serviço está indisponível no momento, caso persista, entre em contato com o suporte')
				console.log("error", error)	
				console.log("url", this.data.data)
			}
		});
    }
    
    fillFilter(dataset){
		var select = jQuery("#select_filtro");
		select.html('');
		select.append("<option value=''></option>");
		dataset.forEach(function(item){
			select.append(`<option value='${item.value}'>${item.name}</option>`);
		})	
    }
    
    setBtnSave(){
		jQuery("#save_filter").attr("disabled", false).find('i').addClass('pln-invi');
		jQuery("#spn_save").html("Salvar");
	}

    cleanForm(){
        document.getElementById("formulario").reset();		
    }
}

let classe = new exampleController();