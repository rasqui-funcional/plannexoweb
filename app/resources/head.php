<!DOCTYPE html>
<html>
    <head>
        <?php if (getenv("ENV") != 'DEV'): ?>
            <!-- Data Layer -->
            <script> <?= Login::loadTagManager(); ?> </script>
            <!-- Google Tag Manager -->
            <script>
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','GTM-PWMZF9R');
            </script>
            <!-- End Google Tag Manager -->
        <?php endif; ?>
        <meta charset="utf-8">
        <title><?php echo ($ViewData['page_title'] ? $ViewData['page_title']  :  "Bionexo | Plannexoweb") ?></title>
        <meta name="global_country" content="<?= $Country ?>">
        <!-- <link href="favicon.png" rel="shortcut icon"> -->
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <!--link href="/resources/css/bootstrap-daterangerpicker/daterangepicker.css" rel="stylesheet"-->
        <link href="/resources/css/datatables/dataTables.bootstrap.min.css?<?php echo time() ?>" rel="stylesheet">
        <link href="/resources/css/sweetAlert/sweetalert2.min.css" rel="stylesheet">
        <!--link href="/resources/css/fullcalendar/fullcalendar.min.css" rel="stylesheet"-->
        <!--link href="/resources/css/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet"-->
        <!-- <link href="/resources/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet"> -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
        <!-- <link href="/resources/fonts/themefy/themify-icons.css" rel="stylesheet"> -->
        <link href="/resources/css/main.css?version=4.4.0" rel="stylesheet">
        <link href="/resources/css/plannexo.css?v=<?=VERSION ?>" rel="stylesheet">
        <link href="/resources/css/info_sku/style.css?v=<?=VERSION ?>" rel="stylesheet">
        <!-- CSS especifíco da View -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">    
        <link href="/resources/css/bootstrap-year-calendar/bootstrap-year-calendar.min.css" rel="stylesheet">
        <?php
        /**
         * Faz o autoload dos arquivos CSS passados na view.
         */
        $AdminRoutes->getViewCSS();
        ?>
    </head>
    <body class="menu-position-side menu-side-left full-screen">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PWMZF9R"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    <input type="hidden" id="bioViewUrl" value="<?php echo BIOVIEWURL ?>" />
    <input type="hidden" id="bioIdUrl" value="<?php echo BIOIDURL ?>" />
    <input type="hidden" id="country" value="<?php echo $Country ?>" />
    <input type="hidden" id="hiddenCompanyId" value="<?php echo Login::getUserSessionByKey('id_company_pk') ?>" />
    <input type="hidden" id="userProfile" value="<?php echo Login::getUserSessionByKey('profile') ?>" />
    <input type="hidden" id="sysLocale" value="<?php echo LocaleHelper::getLocale($Country) ?>" />
    <input type="hidden" id="currency" value="<?php echo $Currency ?>" />
    <?php
    /**
     * Adiciona o menu no corpo do compose.
     */
    require('menu.php') 
    ?>
