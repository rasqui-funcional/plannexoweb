<?php
$lang['suggest'] =
[
    'duplicate' => 'Duplicar sugerencia de compra',
    'setor_code' => 'Código Sector',
    'expected_date' => 'Fecha esperada',
    'reason'=>'Motivo',
    'mark_as_urgent' => 'Marcar como urgente',
    'approved_suggests' => 'Sugerencias aprobadas',
    'grouped_suggestion' => 'Sugerencia agrupada',
];
