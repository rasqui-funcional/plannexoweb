<?php
$lang['column']['sit_sku'] = "Status del Sistema";
$lang['column']['abrev']['sit_sku'] = "Status Sistema";
$lang['column']['inv_level'] = "Señalizador";
$lang['column']['abrev']['inv_level'] = "Señ.";
$lang['column']['cod_estab_fk'] = "Establecimiento";
$lang['column']['abrev']['cod_estab_fk'] = "estab.";
$lang['column']['cod_item'] = "Código del Ítem";
$lang['column']['abrev']['cod_item'] = "Cód. Ítem";
$lang['column']['desc_item'] = "Descripción del ítem";
$lang['column']['abrev']['desc_item'] = "Desc. ítem";
$lang['column']['user_id'] = "Gestor";
$lang['column']['abrev']['user_id'] = "Gestor";
$lang['column']['id_supplier'] = "Código de Proveedor";
$lang['column']['abrev']['id_supplier'] = "Cód. Prov.";
$lang['column']['desc_supplier'] = "Proveedor";
$lang['column']['abrev']['desc_supplier'] = "Prov.";
$lang['column']['cod_group1_fk'] = "Categoría del Sistema";
$lang['column']['abrev']['cod_group1_fk'] = "Cat. Sis.";
$lang['column']['cod_group2_fk'] = "Grupo";
$lang['column']['abrev']['cod_group2_fk'] = "Grupo";
$lang['column']['cod_group3_fk'] = "Subgrupo";
$lang['column']['abrev']['cod_group3_fk'] = "Subgrupo";
$lang['column']['cod_group4_fk'] = "Clase";
$lang['column']['abrev']['cod_group4_fk'] = "Clase";
$lang['column']["cod_group5_fk"]="Estandarización";
$lang['column']['abrev']["cod_group5_fk"]="Estandarización";
$lang['column']["cod_group6_fk"]="Contrato";
$lang['column']['abrev']["cod_group6_fk"]="Contratos";
$lang['column']["cod_group7_fk"]="Alto Costo";
$lang['column']['abrev']["cod_group7_fk"]="#";
$lang['column']["cod_group8_fk"]="OTRAS AGRUPACIONES";
$lang['column']['abrev']["cod_group8_fk"]="Test";
$lang['column']["cod_group9_fk"]="Vacío";
$lang['column']['abrev']["cod_group9_fk"]="Quimioterápicos";
$lang['column']["cod_group10_fk"]="Vacío";
$lang['column']['abrev']["cod_group10_fk"]="Vacío";
$lang['column']["cod_group11_fk"]="Vacío";
$lang['column']['abrev']["cod_group11_fk"]="Vacío";
$lang['column']["cod_group12_fk"]="Vacío";
$lang['column']['abrev']["cod_group12_fk"]="Vacío";
$lang['column']["num_req"]="Número de la Solicitud";
$lang['column']['abrev']['num_req'] = "Núm. Solicitud";
$lang['column']['date_req'] = "Fecha de Solicitud";
$lang['column']['abrev']['date_req'] = "Fech. Solicitud";
$lang['column']['num_order'] = "Número de la Orden";
$lang['column']['abrev']['num_order'] = "Núm. Orden";
$lang['column']['date_order'] = "Fecha de la Orden";
$lang['column']['abrev']['date_order'] = "Fech. Orden";
$lang['column']['date_exp'] = "Fecha de Entrega";
$lang['column']['abrev']['date_exp'] = "Fech. Entrega";
$lang['column']['qty_parc'] = "Número de Sugerencias";
$lang['column']['abrev']['qty_parc'] = "Ctd. Sug.";

$lang['column']['total_sugs'] = "Número de Sugerencias";
$lang['column']['abrev']['total_sugs'] = "CTD. SUGERENCIA";

$lang['column']['qty_rec'] = "QTY_REC";
$lang['column']['abrev']['qty_rec'] = "QTY_REC";
$lang['column']['qty_pending'] = "Cantidad Pendiente";
$lang['column']['abrev']['qty_pending'] = "Ctd. Pend.";
$lang['column']['val_unit'] = "Valor por Unidad ($)";
$lang['column']['abrev']['val_unit'] = "Val. Uni. ($)";
$lang['column']['dias_atraso'] = "Días de Retraso";
$lang['column']['abrev']['dias_atraso'] = "D. Retraso";
$lang['column']['inv_available'] = "Saldo Stock Disponible en Unidades";
$lang['column']['abrev']['inv_available'] = "Sal. Est. Disp.(Un.)";
$lang['column']['inv_total'] = "Saldo Stock Total en Unidades";
$lang['column']['abrev']['inv_total'] = "Sal. Est. Total (Un.)";
$lang['column']['id_sku_pk'] = "Código del SKU";
$lang['column']['abrev']['id_sku_pk'] = "Cód. SKU";
$lang['column']['total_orders'] = "Ctd Órdenes";
$lang['column']['qty_coverage'] = "Cobertura";
$lang['column']['sim'] = "Sí";
$lang['column']['nao'] = "No";

$lang['column']['total_unity'] = "Cantidad de Unidades";
$lang['column']['abrev']['total_unity'] = "CTD. UNIDADES";
$lang['column']['total_qty'] = "Cantidad de Unidades";
$lang['column']['abrev']['total_qty'] = "CTD. UNIDADES";
$lang['column']['total_price'] = "VALOR TOTAL ($)";
$lang['column']['abrev']['total_price'] = "VALOR TOTAL ($)";
$lang['column']['date_arrival_altered'] = "Fecha Esperada";
$lang['column']['sit_urgent'] = "Urgente";
$lang['column']['abrev']['sit_urgent'] = "Urgente";
$lang['column']['abrev']['date_arrival_altered'] = $lang['column']['date_arrival_altered'];
$lang['column']['auxiliar19'] = $lang['column']['date_arrival_altered'];
$lang['column']['abrev']['auxiliar19'] = $lang['column']['date_arrival_altered'];
$lang['column']['id_profile'] = "Política";
$lang['column']['abrev']['id_profile'] = "Política";
$lang['column']['id_calendar_fk']="Calendário";
$lang['column']['abrev']['id_calendar_fk']="Calend.";
$lang['column']['val_total']="Total ($)";
$lang['column']['abrev']['val_total']="Total ($)";

//politics
$lang['column']['ID_PROFILE_PK'] = "ID";
$lang['column']['COD_PROFILE'] = 'Código de la Política';
$lang['column']['abrev']['COD_PROFILE'] = 'Cód. Política';
$lang['column']['DESC_PROFILE'] = 'Descripción de la Política';
$lang['column']['abrev']['DESC_PROFILE'] = 'Desc. Pol.';
$lang['column']['QUANTIDADE'] = 'SKUs';
$lang['column']['SIT_MIN_MAX_ADD_ERP'] = 'Agregar mín./máx ERP';
$lang['column']['DESC_CALENDAR'] = 'Calendario';
$lang['column']['NEXT_DATE'] = 'Próxima Fecha';
$lang['column']['LAST_DATE'] = 'Última Fecha';
$lang['column']['LT_PLANNER'] = 'Lead Time Planificador (Política)';
$lang['column']['abrev']['LT_PLANNER'] = 'L. T. Planif. (Pol.)';
$lang['column']['LT_BUYER'] = 'Lead Time Compra (Política)';
$lang['column']['abrev']['LT_BUYER'] = 'L. T. Compra (Pol.)';
$lang['column']['LT_SUPPLIER'] = 'Lead Time Proveedor (Política)';
$lang['column']['abrev']['LT_SUPPLIER'] = 'L. T. Forn. (Pol.)';
$lang['column']['LT_RECEIVING'] = 'Lead Time Recepción (Política)';
$lang['column']['abrev']['LT_RECEIVING'] = 'L. T. Recep. (Pol.)';
$lang['column']['LT_OTHERS'] = 'Lead Time Otros (Política)';
$lang['column']['abrev']['LT_OTHERS'] = 'L. T. Otros (Pol.)';
$lang['column']['SERVICE_LEVEL'] = 'Nivel de Servicio';
$lang['column']['MIN_LIMIT_BOTTOM'] = "Límite Mínimo";
$lang['column']['abrev']['MIN_LIMIT_BOTTOM'] = "Límite Mín.";
$lang['column']['MAX_LIMIT_BOTTOM'] = "Límite Máximo";
$lang['column']['abrev']['MAX_LIMIT_BOTTOM'] = "Límite Máx.";
$lang['column']['DAYS_MIN'] = "STOCK MÍNIMO";

$lang['column']['curve_abc'] = "ABC";
$lang['column']['curve_pqr'] = "PQR";
$lang['column']['curve_123'] = "123";
$lang['column']['curve_xyz'] = "XYZ";
$lang['column']['cod_group12_fk'] = "cod_group12_fk";
$lang['column']['inv_total_money'] = "STOCK TOTAL ($)";
$lang['column']['abrev']['inv_total_money'] = "STOCK TOTAL ($)";
$lang['column']['inv_available_money'] = "STOCK DISPONIBLE ($)";
$lang['column']['abrev']['inv_available_money'] = "STOCK DISP. ($)";
$lang['column']['qty_min_out_money'] = "Min. ($)";
$lang['column']['qty_target_money'] = "Objetivo ($)";
$lang['column']['qty_max_out_money'] = "Máx. ($)";
$lang['column']['qty_excess'] = "Exceso ($)";
$lang['column']['qty_min_out_ori_money'] = "Mínimo Sugerido ($)";
$lang['column']['abrev']['qty_min_out_ori_money'] = "Mín. Sug. ($)";
$lang['column']['qty_max_out_ori_money'] = "Máximo Sugerido ($)";
$lang['column']['abrev']['qty_max_out_ori_money'] = "Máx. Sug. ($)";
$lang['column']['qty_min_erp_money'] = "Mín. (ERP) ($)";
$lang['column']['qty_max_erp_money'] = "Máx. (ERP) ($)";
$lang['column']['val_purchase_sugg'] = "Sugerencia de Compra ($)";
$lang['column']['abrev']['val_purchase_sugg'] = "Sug. Compra ($)";
$lang['column']['val_purchase_req'] = "Solicitud de Compra ($)";
$lang['column']['abrev']['val_purchase_req'] = "Sol. Compra ($)";
$lang['column']['val_purchase_order'] = "Órden de Compra ($)";
$lang['column']['sku_life_cycle'] = "Ciclo de Vida";
$lang['column']['total_sku'] = "Cantidad de SKUs";
$lang['column']['abrev']['total_skus'] = "Ctd. SKUs";
$lang['column']['id_profile_fk'] = "Política";
$lang['column']['total_skus'] = "Ctd. SKUs";
$lang['column']['id_calendar_pk'] = "Código";
$lang['column']['desc_calendar'] = "Descripción";
$lang['column']['avg_days_interval'] = "Lote Medio de Compras";
$lang['column']['total_sku'] = "Número de SKUs";
$lang['column']['dt_receiving_pk'] = "Última Fecha";
$lang['column']['inv_available_coverage'] = 'COBERTURA STOCK DISPONIBLE (Días)';
$lang['column']['inv_total_coverage'] = 'COBERTURA STOCK TOTAL (Días)';
$lang['column']['qty_min_out_coverage'] = 'COBERTURA MIN. (Días)';
$lang['column']['qty_target_coverage'] = 'COBERTURA OBJETIVO (Días)';
$lang['column']['qty_max_out_coverage'] = 'COBERTURA MAX. (Días)';
$lang['column']['qty_excess_coverage'] = 'COBERTURA EXCESO (Días)';
$lang['column']['qty_min_out_ori_coverage'] = 'COBERTURA MÍNIMO SUGERIDO (DÍAS)';
$lang['column']['qty_max_out_ori_coverage'] = 'COBERTURA MÁXIMO SUGERIDO (DÍAS)';
$lang['column']['qty_min_erp_coverage'] = 'COBERTURA MÍNIMO (ERP) (Días)';
$lang['column']['qty_max_erp_coverage'] = 'COBERTURA MÁXIMO (ERP) (Días)';
$lang['download_csv'] = "Descargar CSV";

/****************MESES DINÂMICOS**************/
$lang['months']['ustobr'] = ["Jan" => "Jan","Feb" => "Feb","Mar" => "Mar","Apr" => "Abr","May" => "May","Jun" => "Jun","Jul" => "Jul","Aug" => "Ago","Sep" => "Sep", "Oct" => "Oct","Nov" => "Nov","Dec" => "Dic"];

for ($number_month = 1; $number_month <= 12; $number_month++) {
    $lang['column']['out_month_minus' . $number_month] = date('M/Y', strtotime("-{$number_month} months"));
    $m = substr($lang['column']['out_month_minus' . $number_month], 0, 3);
    $lang['column']['out_month_minus' . $number_month] = substr_replace($lang['column']['out_month_minus' . $number_month],
        $lang['months']['ustobr'][$m], 0, 3);
}
   foreach ($lang['months']['ustobr'] as $key => $value) {
       if (date('M') == $key) {
           $data = $value.'/'.date('Y');
       }
   }
$lang['column']['out_current_month'] = $data;
/****************FIM MESES DINÂMICOS**************/
