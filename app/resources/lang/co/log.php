<?php
//=======Lang para Log - lista======//
$lang['log']['id'] = "ID";
$lang['log']['data_inclusao'] = "Fecha de Inclusión";
$lang['log']['ip'] = "IP";
$lang['log']['app'] = "Aplicación";
$lang['log']['acao'] = "Acción";
$lang['log']['origin'] = "Origen";
$lang['log']['usuario'] = "Usuario";
$lang['log']['campo'] = "Campo";
$lang['log']['val_antigo'] = "Valor Antiguo";
$lang['log']['val_novo'] = "Valor Nuevo";
$lang['log']['csv_name'] = "info_sku_log";
