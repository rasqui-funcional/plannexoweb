<?php
$lang['menu'] =
    [
        'title'=> 'Menú',
        'reports' => 'Informes',
        'dashboard' => 'Dashboard',
        'resume' => 'Resumen',
        'planning' => 'Planificación',
        'sku' => 'SKU',
        'suggest' => 'Sugerencia de Compras',
        'ordercover' => 'Carátula de Pedidos',
        'approvals' => 'Historico de Aprobaciones',
        'purchaseorder' => 'Órdenes de Compra',
        'params'=> 'Parámetros',
        'polices' => 'Política',
        'calendar' => 'Calendario',
        'logout' => 'Salir',
        'followup' => 'Follow Up',
        'new' => 'Nuevo',
        'users' => 'Usuários',
        'emails' => 'Modelos de Email',
    ];