<?php
$lang['suggest'] =
[
    'duplicate' => 'Duplicar sugerencia de compra',
    'setor_code' => 'Código Sector',
    'expected_date' => 'Fecha esperada',
    'reason'=>'Motivo',
    'mark_as_urgent' => 'Marcar como urgente',
    'approved_suggests' => 'Sugerencias aprobadas',
    'grouped_suggestion' => 'Sugerencia agrupada',
    'about_installments_grouping' => 'Sobre agrupamiento de entregas',
    'grouping_paragraph1' => 'Al detectar una entrega urgente con fecha en la misma semana que una entrega normal, Plannexo alerta sobre la posibilidad de agrupar las entregas.',
    'grouping_paragraph2' => 'Esto puede ayudar a la recepción de los ítems.',
    'grouping_paragraph3' => 'Esta acción puede violar las políticas del ítem. Tenga cuidado al utilizar este recurso.',
    'group_suggests' => 'Agrupar las entregas',
    'more_info' => 'Conoce mas',
    'amount_changed' => 'Cantidad modificada',
    'changed_arrival_date' => 'Fecha alterada de llegada',
    'note' => 'Observación',
    'required_field' => 'Campo Obrigatorio',
    'group' => 'Agrupar',
    'cancel' => 'Cancelar',
    'group_suggests_1_2' => 'Agrupar entregas 1 e 2'
];
