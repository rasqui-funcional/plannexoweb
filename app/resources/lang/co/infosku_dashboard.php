<?php
$lang['info_sku']['carregando'] = "Cargando información...";
$lang['info_sku']['item'] = "Ítem:";
$lang['info_sku']['estab'] = "Estab.:";
$lang['info_sku']['curva'] = "Curva:";
$lang['info_sku']['politica'] = "Política:";
$lang['info_sku']['categoria'] = "Categoría:";
$lang['info_sku']['unidade'] = "Unidad:";
$lang['info_sku']['sinalizador'] = "Señalizador:";
$lang['info_sku']['status_erp'] = "Status ERP:";
$lang['info_sku']['status_sistema'] = "STATUS:";
$lang['info_sku']['status_ativo'] = "Activo";
$lang['info_sku']['moeda'] = "$";
$lang['info_sku']['tab']['dashboard'] = "Dashboard";
$lang['info_sku']['tab']['info'] = "Informaciones";
$lang['info_sku']['tab']['movto_erp'] = "Movimiento ERP";
$lang['info_sku']['tab']['log'] = "LOG";

//=========== Lang para StockBalanceTable =====================================//
$lang['info_sku']['erp']['DESC_LOCAL'] = "Depósito";
$lang['info_sku']['erp']['QTY_INV'] = "Ctd. Disponible";
$lang['info_sku']['erp']['COD_TRANS_PK'] = "Código";
$lang['info_sku']['erp']['DATE_TRANS'] = "Fecha de entrada";
$lang['info_sku']['erp']['QTY_TRANS'] = "Cantidad";

// //=========== Lang para OutTransactionTable =====================================//
$lang['info_sku']['erp']['DATE_TRANS_ALT'] = "Fecha de Salida";
$lang['info_sku']['erp']['COD_LOCAL_DEST'] = "Depósito Destino";
$lang['info_sku']['erp']['COD_SECTOR'] = "Depósito Origen";
$lang['info_sku']['erp']['QTY_TRANS'] = "Cantidad";
$lang['info_sku']['erp']['TYPE_TRANS'] = "Tipo";
$lang['info_sku']['erp']['YN_CONS'] = "Consumo";
$lang['info_sku']['erp']['COD_TRANS_PK'] = "Código";
$lang['info_sku']['erp']['COD_LOCAL'] = "Depósito";
$lang['info_sku']['erp']['DATE_REQ'] = "Fecha de Solicitud";
$lang['info_sku']['erp']['QTY_REQ'] = "Ctd. Solicitud";
$lang['info_sku']['erp']['DESC_LOCAL'] = "Depósito";
$lang['info_sku']['erp']['NUM_ORDER'] = "Número de Orden";
$lang['info_sku']['erp']['DATE_ORDER'] = "Fecha de la Orden";
$lang['info_sku']['erp']['DATE_EXP'] = "Fecha esperada de Entrega";
$lang['info_sku']['erp']['QTY_PENDING'] = "Cantidad Pendiente";
$lang['info_sku']['erp']['QTY_REC'] = "Cantidad recibida";
$lang['info_sku']['erp']['QTY_PARC'] = "Cantidad necesaria";
$lang['info_sku']['erp']['SUPPLIER'] = "Proveedor";
$lang['info_sku']['erp']['MONTH_TRANS_PK'] = "Mes";
$lang['info_sku']['erp']['QTY_TRANS'] = "Consumo";
$lang['info_sku']['erp']['DAYS_QTY_INV'] = "Cobertura (días)";
$lang['info_sku']['erp']['QTY_INV'] = "CTD. DISPONIBLE";
$lang['info_sku']['erp']['QTY_INV_VAL'] = "Stock Financiero ($)";
$lang['info_sku']['erp']['id_sku_pk'] = "Cód. SKU";
$lang['info_sku']['erp']['qty_altered'] = "Ctd.";
$lang['info_sku']['erp']['date_arrival_altered'] = "Fecha de Entrega";
$lang['info_sku']['erp']['obs'] = "Observación";

//ABA DASHBOARD
$lang['info_sku']['dashboard']['balance_estoque'] = "BALANCE DE STOCK";
$lang['info_sku']['dashboard']['prox_sugest'] = "PRÓXIMA SUGERENCIA";
$lang['info_sku']['dashboard']['estoque_excesso'] = "STOCK EN EXCESO";
$lang['info_sku']['dashboard']['previsao_compras'] = "PREVISIÓN DE COMPRAS:";
$lang['info_sku']['dashboard']['mais_detalhes'] = "Más Detalles";
$lang['info_sku']['dashboard']['excesso_disponivel'] = "EXCESO DISPONIBLE";
$lang['info_sku']['dashboard']['excesso_total'] = "EXCESO TOTAL";
$lang['info_sku']['dashboard']['previsao_para_ideal'] = "Previsión para ideal en";
$lang['info_sku']['dashboard']['dias'] = "día(s)";
$lang['info_sku']['dashboard']['qtd'] = "Ctd:";
$lang['info_sku']['dashboard']['em_atraso'] = "en retraso";
$lang['info_sku']['dashboard']['diario'] = "Diario";
$lang['info_sku']['dashboard']['previsao_estatistica'] = "Previsión Estadística";
$lang['info_sku']['dashboard']['valor_total'] = "Valor total:";
$lang['info_sku']['dashboard']['analise_geral'] = "Análisis General";
$lang['info_sku']['dashboard']['ordem_atraso'] = "ÓRDENES EN RETRASO";
$lang['info_sku']['dashboard']['ordem_primeira'] = "Primera";
$lang['info_sku']['dashboard']['ordem_fornecedor'] = "Proveedor";
$lang['info_sku']['dashboard']['atraso'] = "Retraso";
$lang['info_sku']['dashboard']['ordem_dias'] = "días";
$lang['info_sku']['dashboard']['ordem_qtd'] = "Ctd";
$lang['info_sku']['dashboard']['un'] = "Un";
$lang['info_sku']['dashboard']['historico'] = "HISTORIAL:";
$lang['info_sku']['dashboard']['periodo'] = "Período:";
$lang['info_sku']['dashboard']['movto_erp'] = "Movimientos del ERP";
$lang['info_sku']['dashboard']['30_dias'] = "Últimos 30 días";
$lang['info_sku']['dashboard']['3_meses'] = "Media 3 Meses";
$lang['info_sku']['dashboard']['6_meses'] = "Media 6 Meses";
$lang['info_sku']['dashboard']['12_meses'] = "Media 12 Meses";
$lang['info_sku']['dashboard']['posicao_estoque'] = "POSICIÓN DE STOCK:";
$lang['info_sku']['dashboard']['excesso'] = "Exceso";
$lang['info_sku']['dashboard']['semanal'] = "Semanal";
$lang['info_sku']['dashboard']['mensal'] = "Mensual";
$lang['info_sku']['dashboard']['desvio_consumo'] = "Desviación de Consumo";
$lang['info_sku']['dashboard']['variacao'] = "Variación de Consumo (últimos 7 días)";
$lang['info_sku']['dashboard']['lead_time_total'] = "Lead time total:";
$lang['info_sku']['dashboard']['ate'] = "hasta";
$lang['info_sku']['dashboard']['ordens'] = "órdenes";