<?php
//=======Lang para Capa de Pedidos - filtros
$lang['capa_pedido_filtros']['titulo'] = "Carátula de Pedidos";
$lang['capa_pedido_filtros']['agrupar'] = "Agrupar por";
$lang['capa_pedido_filtros']['estab'] = "Establecimiento";
$lang['capa_pedido_filtros']['politica'] = "Política";
$lang['capa_pedido_filtros']['data_entrega'] = "Fecha Entrega";
$lang['capa_pedido_filtros']['sit_urgente'] = "Urgente";
$lang['capa_pedido_filtros']['gestor'] = "Gestor";
$lang['capa_pedido_filtros']['calendario'] = "Calendario";
$lang['capa_pedido_filtros']['deposito'] = "Depósito";
$lang['capa_pedido_filtros']['deposito_padrao'] = "Depósito estándar";
$lang['capa_pedido_filtros']['calendario'] = "Calendario";

//=======Lang para Capa de Pedidos - lista
$lang['capa_pedido_lista']['titulo'] = "Carátula de pedidos";
$lang['capa_pedido_lista']['busca'] = "Búsqueda:";
$lang['capa_pedido_lista']['acoes'] = "Acciones:";
$lang['capa_pedido_lista']['agrupar'] = "Agrupar";
$lang['capa_pedido_lista']['processar'] = "Procesar";
$lang['capa_pedido_lista']['aprovar'] = "Aprobar";
$lang['capa_pedido_lista']['cancelar'] = "Cancelar";
$lang['capa_pedido_lista']['aplicar'] = "Aplicar";
$lang['capa_pedido_lista']['selecione'] = "Seleccione";
$lang['capa_pedido_lista']['definicao_agrupamento'] = "Rellene las definiciones de agrupación";
$lang['capa_pedido_lista']['cod_comprador'] = "Cód. Comprador";
$lang['capa_pedido_lista']['cod_setor'] = "Cód. Sector";
$lang['capa_pedido_lista']['motivo'] = "Motivo";
$lang['capa_pedido_lista']['deposito'] = "Depósito";
$lang['capa_pedido_lista']['col_parcelas'] = "CTD. SUGERENCIA";
$lang['capa_pedido_lista']['col_estab'] = "ESTAB";
$lang['capa_pedido_lista']['col_qtdunidades'] = "CTD. UNIDADES";
$lang['capa_pedido_lista']['col_valor_total'] = "VALOR TOTAL ($)";
$lang['capa_pedido_lista']['col_cod_comprador'] = "CÓD. COMPRADOR";
$lang['capa_pedido_lista']['comprador'] = "COMPRADOR";
$lang['capa_pedido_lista']['col_cod_setor'] = "CÓD. SECTOR";
$lang['capa_pedido_lista']['setor'] = "SECTOR";
$lang['capa_pedido_lista']['col_deposito'] = "DEPÓSITO";
$lang['capa_pedido_lista']['col_motivo'] = "MOTIVO";
$lang['capa_pedido_lista']['col_data_esperada'] = "FECHA ESPERADA";
$lang['capa_pedido_lista']['col_obs'] = "OBSERVACIONES";
$lang['capa_pedido_lista']['col_urgente'] = "URGENTE";
$lang['capa_pedido_lista']['sugestoes'] = "Sugerencias";
$lang['capa_pedido_lista']['sugestao'] = "Sugerencia";
$lang['capa_pedido_lista']['alerta_agrupamento'] = "Ingrese las definiciones de la agrupación";
$lang['capa_pedido_lista']['campo_obrigatorio'] = "Campo obligatorio";
$lang['capa_pedido_lista']['total'] = "Total:";
$lang['capa_pedido_lista']['sit_urgent'] = "Urgente";
$lang['capa_pedido_parcela']['desagrupar'] = "Desagrupar";
//=======Lang para Capa de Pedidos - Parcelas
$lang['capa_pedido_parcela']['titulo'] = "Carátula de pedido - Sugerencias";
$lang['capa_pedido_parcela']['download'] = "Descargar CSV";

//=======Lang para Capa de Pedidos - Parcelas - CSV
$lang['capa_pedido_parcela']['columncsv']['NUM_PARC'] = "Sugerencia";
$lang['capa_pedido_parcela']['columncsv']['COD_ITEM_PK'] = "Cód. ítem";
$lang['capa_pedido_parcela']['columncsv']['DESC_ITEM'] = "Desc. ítem";
$lang['capa_pedido_parcela']['columncsv']['V_ORDER_MULT'] = "Múltiple";
$lang['capa_pedido_parcela']['columncsv']['V_ORDER_MIN'] = "Mínimo";
$lang['capa_pedido_parcela']['columncsv']['QTY_ORIGINAL'] = "Ctd. Origen";
$lang['capa_pedido_parcela']['columncsv']['TOTAL_VALUE_ORIGINAL'] = "Valor Actual";
$lang['capa_pedido_parcela']['columncsv']['TOTAL_VALUE'] = "Valor Alterado";
$lang['capa_pedido_parcela']['columncsv']['QTY_ALTERED'] = "Ctd. Alterado";
$lang['capa_pedido_parcela']['columncsv']['OBS'] = "Observación";
$lang['capa_pedido_parcela']['columncsv']['DATE_ARRIVAL_DMY'] = "Fecha Entrega";
//=======HEADER======//
$lang['capa_pedido_parcela']['header']['parcela'] = "Sugerencia";
$lang['capa_pedido_parcela']['header']['cod_item'] = "CÓD. ÍTEM";
$lang['capa_pedido_parcela']['header']['descricao'] = "DESCRIPCIÓN";
$lang['capa_pedido_parcela']['header']['multiplo'] = "MÚLTIPLE";
$lang['capa_pedido_parcela']['header']['min'] = "MÍNIMO";
$lang['capa_pedido_parcela']['header']['qtd_origem'] = "CTD. ORIGEN";
$lang['capa_pedido_parcela']['header']['valor_atual'] = "VALOR ACTUAL ($)";
$lang['capa_pedido_parcela']['header']['qtd_alterado'] = "CTD. ALTERADO";
$lang['capa_pedido_parcela']['header']['valor_alterado'] = "VALOR ALTERADO";
$lang['capa_pedido_parcela']['header']['data_entrega'] = "FECHA ENTREGA";
$lang['capa_pedido_parcela']['header']['obs'] = "OBSERVACIÓN";

//=======GENERAL=====//
$lang['capa_pedido_table']['yes'] = "Sí";