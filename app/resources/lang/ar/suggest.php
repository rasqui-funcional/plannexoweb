<?php
$lang['suggest'] =
[
    'duplicate' => 'Duplicar sugerencia de compra',
    'setor_code' => 'Código Sector',
    'expected_date' => 'Fecha esperada',
    'reason'=>'Motivo',
    'mark_as_urgent' => 'Marcar como urgente',
    'approved_suggests' => 'Sugerencias aprobadas',
    'grouped_suggestion' => 'Sugerencia agrupada',
    'about_installments_grouping' => 'Sobre agrupar entregas',
    'grouping_paragraph1' => 'Al detectar una entrega urgente con fecha en la misma semana que una entrega comun, Plannexo alerta sobre la posibilidad de agrupar las entregas.',
    'grouping_paragraph2' => 'Eso puede ayudar a controlar la recepción de los Items.',
    'grouping_paragraph3' => 'Esta acción puede violar las políticas del item. Tenga cuidado al utilizar este recurso.',
    'group_suggests' => 'Agrupar entregas',
    'more_info' => 'Sepa más',
    'amount_changed' => 'Cantidad alterada',
    'changed_arrival_date' => 'Fecha alterada de llegada',
    'note' => 'Observación',
    'required_field' => 'Campo Obligatorio',
    'group' => 'Agrupar',
    'cancel' => 'Cancelar',
    'group_suggests_1_2' => 'Agrupar entregas 1 y 2'
];
