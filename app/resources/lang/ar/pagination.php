<?php
$lang['pagination'] =
    [
        'page' => 'Página',
        'qtd_a' => 'a',
        'qtd_de' => 'de',
        'total' => 'Total de páginas',
        'per_page' => 'por página',
    ];
