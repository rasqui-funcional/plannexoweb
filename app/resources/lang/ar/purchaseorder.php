<?php
$lang['purchaseorder']['list']['orders'] = "Órdenes";
$lang['purchaseorder']['list']['items'] = "Ítems";
$lang['purchaseorder']['delayed'] = "Órdenes retrasadas";
$lang['purchaseorder']['filter']['select']['intervalo'] = "Intervalo";
$lang['purchaseorder']['filter']['select']['hoje'] = "Hoy";
$lang['purchaseorder']['filter']['select']['pr_7dias'] = "Próximos 7 días";
$lang['purchaseorder']['filter']['select']['pr_30dias'] = "Próximos 30 días";
$lang['purchaseorder']['filter']['select']['pr_semana'] = "Próxima semana (lun-dom)";
$lang['purchaseorder']['filter']['select']['mes_atual'] = "Este mes";
$lang['purchaseorder']['filter']['select']['pr_mes'] = "Próximo mes";
