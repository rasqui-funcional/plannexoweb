<?php
$lang['resumo']['view'] = "SCM429G";
//=======Lang para Resumo - filtros======//
$lang['resumo_filtros']['titulo'] = "Resumen";
$lang['resumo_filtros']['colunas']['total_sku'] = "SKUs";
$lang['resumo_filtros']['colunas']['estab'] = "Estab.";
$lang['resumo_filtros']['colunas']['abc'] = "ABC";
$lang['resumo_filtros']['colunas']['xyz'] = "XYZ";
$lang['resumo_filtros']['colunas']['pqr'] = "PQR";
$lang['resumo_filtros']['colunas']['123'] = "123";
$lang['resumo_filtros']['colunas']['sinalizador'] = 'Señalizador';
$lang['resumo_filtros']['colunas']['life_cycle'] = "Ciclo de vida";
$lang['resumo_filtros']['colunas']['politica'] = "Política";
$lang['resumo_filtros']['colunas']['gestor'] = "Gestor";
$lang['resumo_filtros']['colunas']['deposito_padrao'] = 'Depósito Estándar';
$lang['resumo_filtros']['colunas']['qtd_itens'] = "Ctd. Ítems";
$lang['resumo_filtros']['colunas']['inv_total_money'] = "Saldo Stock Total ($)";
$lang['resumo_filtros']['colunas']['available_money'] = "Saldo Stock Disp. ($)";
$lang['resumo_filtros']['colunas']['inv_available_money'] = "Saldo Stock Disp. ($)";
$lang['resumo_filtros']['colunas']['min'] = "Mínimo ($)";
$lang['resumo_filtros']['colunas']['alvo'] = "Objetivo";
$lang['resumo_filtros']['colunas']['max'] = "Máximo ($)";
$lang['resumo_filtros']['colunas']['excesso'] = "Exceso";
$lang['resumo_filtros']['colunas']['min_sugerido'] = "Mínimo Sugerido ($)";
$lang['resumo_filtros']['colunas']['max_sugerido'] = "Máximo Sugerido ($)";
$lang['resumo_filtros']['colunas']['min_erp'] = "Mínimo (ERP) ($)";
$lang['resumo_filtros']['colunas']['max_erp'] = "Máximo (ERP) ($)";
$lang['resumo_filtros']['colunas']['sugestao_compra'] = "Sugerencia de Compra ($)";
$lang['resumo_filtros']['colunas']['solicit_compra'] = "Solicitud Compra ($)";
$lang['resumo_filtros']['colunas']['ordem_compra'] = "Orden de Compra ($)";
$lang['resumo_filtros']['filtros'] = 'FILTROS';
$lang['resumo_filtros']['agrupamentos'] = 'AGRUPACIONES';

//=======Lang para Resumo - lista======//

$lang['resumo_lista']['titulo'] = "Resumen";
$lang['resumo_lista']['csv'] = "Descargar CSV";
