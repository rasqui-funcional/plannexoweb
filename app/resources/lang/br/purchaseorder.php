<?php

$lang['purchaseorder']['list']['orders'] = "Ordens";
$lang['purchaseorder']['list']['items'] = "Itens";
$lang['purchaseorder']['delayed'] = "Ordens atrasadas";
$lang['purchaseorder']['filter']['select']['intervalo'] = "Intervalo";
$lang['purchaseorder']['filter']['select']['hoje'] = "Hoje";
$lang['purchaseorder']['filter']['select']['pr_7dias'] = "Próximos 7 dias";
$lang['purchaseorder']['filter']['select']['pr_30dias'] = "Próximos 30 dias";
$lang['purchaseorder']['filter']['select']['pr_semana'] = "Próxima semana (seg-dom)";
$lang['purchaseorder']['filter']['select']['mes_atual'] = "Este mês";
$lang['purchaseorder']['filter']['select']['pr_mes'] = "Próximo mês";
