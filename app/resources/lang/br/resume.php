<?
$lang['resumo']['view'] = "SCM429G";
//=======Lang para Resumo - filtros======//
$lang['resumo_filtros']['titulo'] = "Resumo";
$lang['resumo_filtros']['colunas']['total_sku'] = "SKUs";
$lang['resumo_filtros']['colunas']['estab'] = "Estab.";
$lang['resumo_filtros']['colunas']['abc'] = "ABC";
$lang['resumo_filtros']['colunas']['xyz'] = "XYZ";
$lang['resumo_filtros']['colunas']['pqr'] = "PQR";
$lang['resumo_filtros']['colunas']['123'] = "123";
$lang['resumo_filtros']['colunas']['sinalizador'] = 'Sinalizador';
$lang['resumo_filtros']['colunas']['life_cycle'] = "Ciclo de vida";
$lang['resumo_filtros']['colunas']['politica'] = "Política";
$lang['resumo_filtros']['colunas']['gestor'] = "Gestor";
$lang['resumo_filtros']['colunas']['deposito_padrao'] = 'Depósito Padrão';
$lang['resumo_filtros']['colunas']['qtd_itens'] = "Qtde. Itens";
$lang['resumo_filtros']['colunas']['inv_total_money'] = "Saldo Estoque Total (R$)";
$lang['resumo_filtros']['colunas']['available_money'] = "Saldo Estoque Disp. (R$)";
$lang['resumo_filtros']['colunas']['inv_available_money'] = "Saldo Estoque Disp. (R$)";
$lang['resumo_filtros']['colunas']['min'] = "Mínimo (R$)";
$lang['resumo_filtros']['colunas']['alvo'] = "Alvo";
$lang['resumo_filtros']['colunas']['max'] = "Máximo (R$)";
$lang['resumo_filtros']['colunas']['excesso'] = "Excesso";
$lang['resumo_filtros']['colunas']['min_sugerido'] = "Mínimo Sugerido (R$)";
$lang['resumo_filtros']['colunas']['max_sugerido'] = "Máximo Sugerido (R$)";
$lang['resumo_filtros']['colunas']['min_erp'] = "Mínimo (ERP) (R$)";
$lang['resumo_filtros']['colunas']['max_erp'] = "Máximo (ERP) (R$)";
$lang['resumo_filtros']['colunas']['sugestao_compra'] = "Sugestão de Compra (R$)";
$lang['resumo_filtros']['colunas']['solicit_compra'] = "Solicit Compra (R$)";
$lang['resumo_filtros']['colunas']['ordem_compra'] = "Ordem de Compra (R$)";
$lang['resumo_filtros']['filtros'] = 'FILTROS';
$lang['resumo_filtros']['agrupamentos'] = 'AGRUPAMENTOS';

//=======Lang para Resumo - lista======//

$lang['resumo_lista']['titulo'] = "Resumo";
$lang['resumo_lista']['csv'] = "Download CSV";
