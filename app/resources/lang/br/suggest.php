<?php
$lang['suggest'] =
[
    'duplicate' => 'Duplicar sugestão de compra',
    'setor_code' => 'Código Setor',
    'expected_date' => 'Data esperada',
    'reason'=>'Motivo',
    'mark_as_urgent' => 'Marcar como urgente',
    'approved_suggests' => 'Sugestões aprovadas',
    'grouped_suggestion' => 'Sugestão agrupada',
    'about_installments_grouping' => 'Sobre agrupamento de parcelas',
    'grouping_paragraph1' => 'Ao detectar uma parcela urgente com entrega na mesma semana de uma entrega comum o Plannexo alerta sobre a possibilidade de agrupamento das parcelas.',
    'grouping_paragraph2' => 'Isso pode ajudar a controlar o recebimento dos itens.',
    'grouping_paragraph3' => 'Esta ação pode violar as políticas do item. Cuidado ao utilizar este recurso.',
    'group_suggests' => 'Agrupar parcelas',
    'more_info' => 'Saiba mais',
    'amount_changed' => 'Quantidade alterada',
    'changed_arrival_date' => 'Data alterada de chegada',
    'note' => 'Observação',
    'required_field' => 'Campo Obrigatório',
    'group' => 'Agrupar',
    'cancel' => 'Cancelar',
    'group_suggests_1_2' => 'Agrupar parcelas 1 e 2'
];
