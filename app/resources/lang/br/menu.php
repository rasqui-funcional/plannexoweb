<?php
$lang['menu'] =
    [
        'title'=> 'Menu',
        'reports' => 'Relatórios',
        'dashboard' => 'Dashboard',
        'resume' => 'Resumo',
        'planning' => 'Planejamento',
        'sku' => 'SKU',
        'suggest' => 'Sugestão de Compras',
        'ordercover' => 'Capa de Pedidos',
        'approvals' => 'Histórico de Aprovações',
        'purchaseorder' => 'Ordens de Compra',
        'params'=> 'Parâmetros',
        'polices' => 'Política',
        'calendar' => 'Calendário',
        'logout' => 'Sair',
        'followup' => 'Follow Up',
        'new' => 'Novo',
        'users' => 'Usuários',
        'emails' => 'Modelos de Email',
    ];