<?
//=======Lang para Capa de Pedidos - filtros
$lang['capa_pedido_filtros']['titulo'] = "Capa de Pedidos";
$lang['capa_pedido_filtros']['agrupar'] = "Agrupar por";
$lang['capa_pedido_filtros']['estab'] = "Estabelecimento";
$lang['capa_pedido_filtros']['politica'] = "Política";
$lang['capa_pedido_filtros']['data_entrega'] = "Data Entrega";
$lang['capa_pedido_filtros']['sit_urgente'] = "Urgente";
$lang['capa_pedido_filtros']['gestor'] = "Gestor";
$lang['capa_pedido_filtros']['calendario'] = "Calendário";
$lang['capa_pedido_filtros']['deposito'] = "Depósito";
$lang['capa_pedido_filtros']['deposito_padrao'] = "Depósito padrão";
$lang['capa_pedido_filtros']['calendario'] = "Calendário";

//=======Lang para Capa de Pedidos - lista
$lang['capa_pedido_lista']['titulo'] = "Capa de pedidos";
$lang['capa_pedido_lista']['busca'] = "Busca:";
$lang['capa_pedido_lista']['acoes'] = "Ações:";
$lang['capa_pedido_lista']['agrupar'] = "Agrupar";
$lang['capa_pedido_lista']['processar'] = "Processar";
$lang['capa_pedido_lista']['aprovar'] = "Aprovar";
$lang['capa_pedido_lista']['cancelar'] = "Cancelar";
$lang['capa_pedido_lista']['aplicar'] = "Aplicar";
$lang['capa_pedido_lista']['selecione'] = "Selecione";
$lang['capa_pedido_lista']['definicao_agrupamento'] = "Preencha as definições de agrupamento";
$lang['capa_pedido_lista']['cod_comprador'] = "Cód. Comprador";
$lang['capa_pedido_lista']['cod_setor'] = "Cód. Setor";
$lang['capa_pedido_lista']['motivo'] = "Motivo";
$lang['capa_pedido_lista']['deposito'] = "Depósito";
$lang['capa_pedido_lista']['col_parcelas'] = "QTD SUGESTÃO";
$lang['capa_pedido_lista']['col_estab'] = "ESTAB";
$lang['capa_pedido_lista']['col_qtdunidades'] = "QTDE UNIDADES";
$lang['capa_pedido_lista']['col_valor_total'] = "VALOR TOTAL (R$)";
$lang['capa_pedido_lista']['col_cod_comprador'] = "CÓD. COMPRADOR";
$lang['capa_pedido_lista']['comprador'] = "COMPRADOR";
$lang['capa_pedido_lista']['col_cod_setor'] = "CÓD. SETOR";
$lang['capa_pedido_lista']['setor'] = "SETOR";
$lang['capa_pedido_lista']['col_deposito'] = "DEPÓSITO";
$lang['capa_pedido_lista']['col_motivo'] = "MOTIVO";
$lang['capa_pedido_lista']['col_data_esperada'] = "DATA ESPERADA";
$lang['capa_pedido_lista']['col_obs'] = "OBSERVAÇÕES";
$lang['capa_pedido_lista']['col_urgente'] = "URGENTE";
$lang['capa_pedido_lista']['sugestoes'] = "Sugestões";
$lang['capa_pedido_lista']['sugestao'] = "Sugestão";
$lang['capa_pedido_lista']['alerta_agrupamento'] = "Insira as definições do agrupamento";
$lang['capa_pedido_lista']['campo_obrigatorio'] = "Campo obrigatório";
$lang['capa_pedido_lista']['total'] = "Total:";
$lang['capa_pedido_lista']['sit_urgent'] = "Urgente";
$lang['capa_pedido_parcela']['desagrupar'] = "Desagrupar";
$lang['capa_pedido_parcela']['excluir'] = "Excluir";
//=======Lang para Capa de Pedidos - Parcelas
$lang['capa_pedido_parcela']['titulo'] = "Capa de pedido - Sugestões";
$lang['capa_pedido_parcela']['download'] = "Download CSV";

//=======Lang para Capa de Pedidos - Parcelas - CSV
$lang['capa_pedido_parcela']['columncsv']['NUM_PARC'] = "Sugestão";
$lang['capa_pedido_parcela']['columncsv']['COD_ITEM_PK'] = "Cód. item";
$lang['capa_pedido_parcela']['columncsv']['DESC_ITEM'] = "Desc. item";
$lang['capa_pedido_parcela']['columncsv']['V_ORDER_MULT'] = "Multiplo";
$lang['capa_pedido_parcela']['columncsv']['V_ORDER_MIN'] = "Mínimo";
$lang['capa_pedido_parcela']['columncsv']['QTY_ORIGINAL'] = "Qtd. Origem";
$lang['capa_pedido_parcela']['columncsv']['TOTAL_VALUE_ORIGINAL'] = "Valor Atual";
$lang['capa_pedido_parcela']['columncsv']['TOTAL_VALUE'] = "Valor Alterado";
$lang['capa_pedido_parcela']['columncsv']['QTY_ALTERED'] = "Qtd. Alterado";
$lang['capa_pedido_parcela']['columncsv']['OBS'] = "Observação";
$lang['capa_pedido_parcela']['columncsv']['DATE_ARRIVAL_DMY'] = "Data Entrega";
//=======HEADER======//
$lang['capa_pedido_parcela']['header']['parcela'] = "Sugestão";
$lang['capa_pedido_parcela']['header']['cod_item'] = "CÓD. ITEM";
$lang['capa_pedido_parcela']['header']['descricao'] = "DESCRIÇÃO";
$lang['capa_pedido_parcela']['header']['multiplo'] = "MÚLTIPLO";
$lang['capa_pedido_parcela']['header']['min'] = "MÍNIMO";
$lang['capa_pedido_parcela']['header']['qtd_origem'] = "QTDE. ORIGEM";
$lang['capa_pedido_parcela']['header']['valor_atual'] = "VALOR ATUAL (R$)";
$lang['capa_pedido_parcela']['header']['qtd_alterado'] = "QTDE. ALTERADO";
$lang['capa_pedido_parcela']['header']['valor_alterado'] = "VALOR ALTERADO";
$lang['capa_pedido_parcela']['header']['data_entrega'] = "DATA ENTREGA";
$lang['capa_pedido_parcela']['header']['obs'] = "OBSERVAÇÃO";

//=======GENERAL=====//
$lang['capa_pedido_table']['yes'] = "Sim";