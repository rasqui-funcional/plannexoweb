<?
$lang['calendar']['list'] = 'calendários';
$lang['calendar']['edit_calendar'] = 'Editar Calendário';
$lang['calendar']['titulo'] = 'Lista de Calendários';
$lang['calendar']['clear'] = 'Limpar calendário';
$lang['calendar']['fill'] = 'Preencher calendário';
$lang['calendar']['weekly'] = 'Semanalmente';
$lang['calendar']['monthly'] = 'Mensalmente';
$lang['calendar']['week'] = 'Semana';
$lang['calendar']['month'] = 'Mês';
$lang['calendar']['weeks'] = 'Semana (s)';
$lang['calendar']['all'] = 'todo(a)';
$lang['calendar']['each'] = 'a cada';
$lang['calendar']['dias'] = 'dias';
$lang['calendar']['dia'] = 'dia';
$lang['calendar']['politicas'] = 'políticas';
$lang['calendar']['segunda'] = 'Segunda';
$lang['calendar']['terca'] = 'Terça';
$lang['calendar']['quarta'] = 'Quarta';
$lang['calendar']['quinta'] = 'Quinta';
$lang['calendar']['sexta'] = 'Sexta';
$lang['calendar']['first'] = 'Primeira';
$lang['calendar']['third'] = 'Terceira';
