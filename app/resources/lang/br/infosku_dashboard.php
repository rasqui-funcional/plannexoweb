<?
//=========== Lang para Info SKU =====================================//
$lang['info_sku']['carregando'] = "Carregando informações...";
$lang['info_sku']['item'] = "Item:";
$lang['info_sku']['estab'] = "Estab.:";
$lang['info_sku']['curva'] = "Curva:";
$lang['info_sku']['politica'] = "Política:";
$lang['info_sku']['categoria'] = "Categoria:";
$lang['info_sku']['unidade'] = "Unidade:";
$lang['info_sku']['sinalizador'] = "Sinalizador:";
$lang['info_sku']['status_erp'] = "Status ERP:";
$lang['info_sku']['status_sistema'] = "STATUS:";
$lang['info_sku']['status_ativo'] = "Ativo";
$lang['info_sku']['moeda'] = "R$";
$lang['info_sku']['tab']['dashboard'] = "Dashboard";
$lang['info_sku']['tab']['info'] = "Informações";
$lang['info_sku']['tab']['movto_erp'] = "Movimento ERP";
$lang['info_sku']['tab']['log'] = "LOG";

//=========== Lang para StockBalanceTable =====================================//
$lang['info_sku']['erp']['DESC_LOCAL'] = "Depósito";
$lang['info_sku']['erp']['QTY_INV'] = "Qtd. Disponível";
$lang['info_sku']['erp']['COD_TRANS_PK'] = "Código";
$lang['info_sku']['erp']['DATE_TRANS'] = "Data de entrada";
$lang['info_sku']['erp']['QTY_TRANS'] = "Quantidade";

// //=========== Lang para OutTransactionTable =====================================//
$lang['info_sku']['erp']['DATE_TRANS_ALT'] = "Data de Saída";
$lang['info_sku']['erp']['COD_LOCAL_DEST'] = "Depósito Destino";
$lang['info_sku']['erp']['COD_SECTOR'] = "Depósito Origem";
$lang['info_sku']['erp']['QTY_TRANS'] = "Quantidade";
$lang['info_sku']['erp']['TYPE_TRANS'] = "Tipo";
$lang['info_sku']['erp']['YN_CONS'] = "Consumo";
$lang['info_sku']['erp']['COD_TRANS_PK'] = "Código";
$lang['info_sku']['erp']['COD_LOCAL'] = "Depósito";
$lang['info_sku']['erp']['DATE_REQ'] = "Data da Solicitação";
$lang['info_sku']['erp']['QTY_REQ'] = "Qtd. Requisição";
$lang['info_sku']['erp']['DESC_LOCAL'] = "Depósito";
$lang['info_sku']['erp']['NUM_ORDER'] = "Número de Ordem";
$lang['info_sku']['erp']['DATE_ORDER'] = "Data da Ordem";
$lang['info_sku']['erp']['DATE_EXP'] = "Data esperada da Entrega";
$lang['info_sku']['erp']['QTY_PENDING'] = "Quantidade Pendente";
$lang['info_sku']['erp']['QTY_REC'] = "Quantidade recebida";
$lang['info_sku']['erp']['QTY_PARC'] = "Quantidade necessária";
$lang['info_sku']['erp']['SUPPLIER'] = "Fornecedor";
$lang['info_sku']['erp']['MONTH_TRANS_PK'] = "Mês";
$lang['info_sku']['erp']['QTY_TRANS'] = "Consumo";
$lang['info_sku']['erp']['DAYS_QTY_INV'] = "Cobertura (dias)";
$lang['info_sku']['erp']['QTY_INV'] = "QTD. DISPONÍVEL";
$lang['info_sku']['erp']['QTY_INV_VAL'] = "Estoque Financeiro (R$)";
$lang['info_sku']['erp']['id_sku_pk'] = "Cód. SKU";
$lang['info_sku']['erp']['qty_altered'] = "Qtde.";
$lang['info_sku']['erp']['date_arrival_altered'] = "Data de Entrega";
$lang['info_sku']['erp']['obs'] = "Observação";

//ABA DASHBOARD
$lang['info_sku']['dashboard']['balance_estoque'] = "BALANCEAMENTO DE ESTOQUE";
$lang['info_sku']['dashboard']['prox_sugest'] = "PRÓXIMA SUGESTÃO";
$lang['info_sku']['dashboard']['estoque_excesso'] = "ESTOQUE EM EXCESSO";
$lang['info_sku']['dashboard']['previsao_compras'] = "PREVISÃO DE COMPRAS:";
$lang['info_sku']['dashboard']['mais_detalhes'] = "Mais Detalhes";
$lang['info_sku']['dashboard']['excesso_disponivel'] = "EXCESSO DISPONÍVEL";
$lang['info_sku']['dashboard']['excesso_total'] = "EXCESSO TOTAL";
$lang['info_sku']['dashboard']['previsao_para_ideal'] = "Previsão para o ideal em";
$lang['info_sku']['dashboard']['dias'] = "dia(s)";
$lang['info_sku']['dashboard']['qtd'] = "Qtd:";
$lang['info_sku']['dashboard']['em_atraso'] = "em atraso";
$lang['info_sku']['dashboard']['diario'] = "Diário";
$lang['info_sku']['dashboard']['previsao_estatistica'] = "Previsão Estatística";
$lang['info_sku']['dashboard']['valor_total'] = "Valor total:";
$lang['info_sku']['dashboard']['analise_geral'] = "Análise Geral";
$lang['info_sku']['dashboard']['ordem_atraso'] = "ORDENS EM ATRASO";
$lang['info_sku']['dashboard']['ordem_primeira'] = "Primeira";
$lang['info_sku']['dashboard']['ordem_fornecedor'] = "Fornecedor";
$lang['info_sku']['dashboard']['atraso'] = "Atraso";
$lang['info_sku']['dashboard']['ordem_dias'] = "dias";
$lang['info_sku']['dashboard']['ordem_qtd'] = "Qtd";
$lang['info_sku']['dashboard']['un'] = "Un";
$lang['info_sku']['dashboard']['historico'] = "HISTÓRICO:";
$lang['info_sku']['dashboard']['periodo'] = "Período:";
$lang['info_sku']['dashboard']['movto_erp'] = "Movimentos do ERP";
$lang['info_sku']['dashboard']['30_dias'] = "Últimos 30 dias";
$lang['info_sku']['dashboard']['3_meses'] = "Média 3 Meses";
$lang['info_sku']['dashboard']['6_meses'] = "Média 6 Meses";
$lang['info_sku']['dashboard']['12_meses'] = "Média 12 Meses";
$lang['info_sku']['dashboard']['posicao_estoque'] = "POSIÇÃO DE ESTOQUE:";
$lang['info_sku']['dashboard']['excesso'] = "Excesso";
$lang['info_sku']['dashboard']['semanal'] = "Semanal";
$lang['info_sku']['dashboard']['mensal'] = "Mensal";
$lang['info_sku']['dashboard']['desvio_consumo'] = "Desvio de Consumo";
$lang['info_sku']['dashboard']['variacao'] = "Variação de Consumo (últimos 7 dias)";
$lang['info_sku']['dashboard']['lead_time_total'] = "Lead time total:";
$lang['info_sku']['dashboard']['ate'] = "até";
$lang['info_sku']['dashboard']['ordens'] = "ordens";