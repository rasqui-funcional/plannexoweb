<?php
$lang['column']['sit_sku'] = "Status do Sistema";
$lang['column']['abrev']['sit_sku'] = "Status Sistema";
$lang['column']['inv_level'] = "Sinalizador";
$lang['column']['abrev']['inv_level'] = "Sin.";
$lang['column']['cod_estab_fk'] = "Estabelecimento";
$lang['column']['abrev']['cod_estab_fk'] = "estab";
$lang['column']['cod_item'] = "Código do Item";
$lang['column']['abrev']['cod_item'] = "Cód. Item";
$lang['column']['desc_item'] = "Descrição do item";
$lang['column']['abrev']['desc_item'] = "Desc. item";
$lang['column']['user_id'] = "Gestor";
$lang['column']['abrev']['user_id'] = "Gestor";
$lang['column']['id_supplier'] = "Código do Fornecedor";
$lang['column']['abrev']['id_supplier'] = "Cód. Forn";
$lang['column']['desc_supplier'] = "Fornecedor";
$lang['column']['abrev']['desc_supplier'] = "Forn.";

$fields = new Fields();
$groupings = $fields->getNewDinamicFieldsGrouping();
foreach ($groupings as $key => $value) {
    $lang['column']["cod_group{$key}_fk"] = $value;
    $lang['column']['abrev']["cod_group{$key}_fk"] = $value;

    // sku
    $lang['vw_scr_scm405g_sku_list']["cod_group{$key}_fk"] = $value;
    $lang['vw_scr_scm405g_sku_list']['abrev']["cod_group{$key}_fk"] = $value;

    //suggest
    $lang['vw_scr_scm420g_sug']["cod_group{$key}_fk"] = $value;
    $lang['vw_scr_scm420g_sug']['abrev']["cod_group{$key}_fk"] = $value;

    // ordem de compra
    $lang['ordem_compra_filtros']["cod_group{$key}_fk"] = $value;
    $lang['ordem_compra_filtros']['abrev']["cod_group{$key}_fk"] = $value;
}

$lang['column']["num_req"]="Número da Solicitação";
$lang['column']['abrev']['num_req'] = "Núm. Solicitação";
$lang['column']['date_req'] = "Data da Solicitação";
$lang['column']['abrev']['date_req'] = "Dt. Solicitação";
$lang['column']['num_order'] = "Número da Ordem";
$lang['column']['abrev']['num_order'] = "Núm. Ordem";
$lang['column']['date_order'] = "Data da Ordem";
$lang['column']['abrev']['date_order'] = "Dt. Ordem";
$lang['column']['date_exp'] = "Data de Entrega";
$lang['column']['abrev']['date_exp'] = "Dt. Expiração";
$lang['column']['qty_parc'] = "Quantidade de Sugestões";
$lang['column']['abrev']['qty_parc'] = "Qtd. Sug.";

$lang['column']['total_sugs'] = "Quantidade de Sugestões";
$lang['column']['abrev']['total_sugs'] = "QTD SUGESTÃO";

$lang['column']['qty_rec'] = "QTY_REC";
$lang['column']['abrev']['qty_rec'] = "QTY_REC";
$lang['column']['qty_pending'] = "Quantidade Pendente";
$lang['column']['abrev']['qty_pending'] = "Qtd. Pend.";
$lang['column']['val_unit'] = "Valor por Unidade (R$)";
$lang['column']['abrev']['val_unit'] = "Val. Uni. (R$)";
$lang['column']['dias_atraso'] = "Dias de Atraso";
$lang['column']['abrev']['dias_atraso'] = "D. Atraso";
$lang['column']['inv_available'] = "Saldo Estoque Disponível em Unidades";
$lang['column']['abrev']['inv_available'] = "Sal. Est. Disp.(Un.)";
$lang['column']['inv_total'] = "Saldo Estoque Total em Unidades";
$lang['column']['abrev']['inv_total'] = "Sal. Est. Total (Un.)";
$lang['column']['id_sku_pk'] = "Código do SKU";
$lang['column']['abrev']['id_sku_pk'] = "Cód. SKU";
$lang['column']['total_orders'] = "Qtd Ordens";
$lang['column']['qty_coverage'] = "Cobertura";
$lang['column']['sim'] = "Sim";
$lang['column']['nao'] = "Não";

$lang['column']['total_unity'] = "Quantidade de Unidades";
$lang['column']['abrev']['total_unity'] = "QTDE UNIDADES";
$lang['column']['total_qty'] = "Quantidade de Unidades";
$lang['column']['abrev']['total_qty'] = "QTDE UNIDADES";
$lang['column']['total_price'] = "VALOR TOTAL (R$)";
$lang['column']['abrev']['total_price'] = "VALOR TOTAL (R$)";
$lang['column']['date_arrival_altered'] = "Data Esperada";
$lang['column']['sit_urgent'] = "Urgente";
$lang['column']['abrev']['sit_urgent'] = "Urgente";
$lang['column']['abrev']['date_arrival_altered'] = $lang['column']['date_arrival_altered'];
$lang['column']['auxiliar19'] = $lang['column']['date_arrival_altered'];
$lang['column']['abrev']['auxiliar19'] = $lang['column']['date_arrival_altered'];
$lang['column']['id_profile'] = "Política";
$lang['column']['abrev']['id_profile'] = "Política";
$lang['column']['id_calendar_fk']="CalendÁrio";
$lang['column']['abrev']['id_calendar_fk']="Calend.";
$lang['column']['val_total']="Total (R$)";
$lang['column']['abrev']['val_total']="Total (R$)";

//politics
$lang['column']['ID_PROFILE_PK'] = "ID";
$lang['column']['COD_PROFILE'] = 'Código da Política';
$lang['column']['abrev']['COD_PROFILE'] = 'Cód. Política';
$lang['column']['DESC_PROFILE'] = 'Descrição da Política';
$lang['column']['abrev']['DESC_PROFILE'] = 'Desc. Pol.';
$lang['column']['QUANTIDADE'] = 'SKUs';
$lang['column']['SIT_MIN_MAX_ADD_ERP'] = 'Adicionar mín./máx ERP';
$lang['column']['DESC_CALENDAR'] = 'Calendário';
$lang['column']['NEXT_DATE'] = 'Próxima Data';
$lang['column']['LAST_DATE'] = 'Última Data';
$lang['column']['LT_PLANNER'] = 'Lead Time Planejador (Política)';
$lang['column']['abrev']['LT_PLANNER'] = 'L. T. Planej. (Pol.)';
$lang['column']['LT_BUYER'] = 'Lead Time Compra (Política)';
$lang['column']['abrev']['LT_BUYER'] = 'L. T. Compra (Pol.)';
$lang['column']['LT_SUPPLIER'] = 'Lead Time Fornecedor (Política)';
$lang['column']['abrev']['LT_SUPPLIER'] = 'L. T. Forn. (Pol.)';
$lang['column']['LT_RECEIVING'] = 'Lead Time Recebimento (Política)';
$lang['column']['abrev']['LT_RECEIVING'] = 'L. T. Receb. (Pol.)';
$lang['column']['LT_OTHERS'] = 'Lead Time Outros (Política)';
$lang['column']['abrev']['LT_OTHERS'] = 'L. T. Outros (Pol.)';
$lang['column']['SERVICE_LEVEL'] = 'Nível de Serviço';
$lang['column']['MIN_LIMIT_BOTTOM'] = "Limite Mínimo";
$lang['column']['abrev']['MIN_LIMIT_BOTTOM'] = "Limite Mín.";
$lang['column']['MAX_LIMIT_BOTTOM'] = "Limite Máximo";
$lang['column']['abrev']['MAX_LIMIT_BOTTOM'] = "Limite Máx.";
$lang['column']['DAYS_MIN'] = "ESTOQUE MÍNIMO";

$lang['column']['curve_abc'] = "ABC";
$lang['column']['curve_pqr'] = "PQR";
$lang['column']['curve_123'] = "123";
$lang['column']['curve_xyz'] = "XYZ";
$lang['column']['inv_total_money'] = "ESTOQUE TOTAL (R$)";
$lang['column']['abrev']['inv_total_money'] = "ESTOQUE TOTAL (R$)";
$lang['column']['inv_available_money'] = "ESTOQUE DISPONÍVEL (R$)";
$lang['column']['abrev']['inv_available_money'] = "ESTOQUE DISP. (R$)";
$lang['column']['qty_min_out_money'] = "Min. (R$)";
$lang['column']['qty_target_money'] = "Alvo (R$)";
$lang['column']['qty_max_out_money'] = "Max. (R$)";
$lang['column']['qty_excess'] = "Excesso (R$)";
$lang['column']['qty_min_out_ori_money'] = "Mínimo Sugerido (R$)";
$lang['column']['abrev']['qty_min_out_ori_money'] = "Mín. Sug. (R$)";
$lang['column']['qty_max_out_ori_money'] = "Máximo Sugerido (R$)";
$lang['column']['abrev']['qty_max_out_ori_money'] = "Máx. Sug. (R$)";
$lang['column']['qty_min_erp_money'] = "Mín. (ERP) (R$)";
$lang['column']['qty_max_erp_money'] = "Máx. (ERP) (R$)";
$lang['column']['val_purchase_sugg'] = "Sugestão de Compra (R$)";
$lang['column']['abrev']['val_purchase_sugg'] = "Sug. Compra (R$)";
$lang['column']['val_purchase_req'] = "Solicitação de Compra (R$)";
$lang['column']['abrev']['val_purchase_req'] = "Sol. Compra (R$)";
$lang['column']['val_purchase_order'] = "Ordem de Compra (R$)";
$lang['column']['sku_life_cycle'] = "Cliclo de Vida";
$lang['column']['total_sku'] = "Quantidade de SKUs";
$lang['column']['abrev']['total_skus'] = "QTd. SKUs";
$lang['column']['id_profile_fk'] = "Política";
$lang['column']['total_skus'] = "Qtde. Skus";
$lang['column']['id_calendar_pk'] = "Código";
$lang['column']['desc_calendar'] = "Descrição";
$lang['column']['avg_days_interval'] = "Lote Médio de Compras";
$lang['column']['total_sku'] = "Número de Sku's";
$lang['column']['dt_receiving_pk'] = "Última Data";
$lang['column']['inv_available_coverage'] = 'COBERTURA ESTOQUE DISPONÍVEL (Dias)';
$lang['column']['inv_total_coverage'] = 'COBERTURA ESTOQUE TOTAL (Dias)';
$lang['column']['qty_min_out_coverage'] = 'COBERTURA MIN. (Dias)';
$lang['column']['qty_target_coverage'] = 'COBERTURA ALVO (Dias)';
$lang['column']['qty_max_out_coverage'] = 'COBERTURA MAX. (Dias)';
$lang['column']['qty_excess_coverage'] = 'COBERTURA EXCESSO (Dias)';
$lang['column']['qty_min_out_ori_coverage'] = 'COBERTURA MÍNIMO SUGERIDO (DIAS)';
$lang['column']['qty_max_out_ori_coverage'] = 'COBERTURA MÁXIMO SUGERIDO (DIAS)';
$lang['column']['qty_min_erp_coverage'] = 'COBERTURA MÍNIMO (ERP) (Dias)';
$lang['column']['qty_max_erp_coverage'] = 'COBERTURA MÁXIMO (ERP) (Dias)';
$lang['download_csv'] = "Download CSV";

/****************MESES DINÂMICOS**************/
$lang['months']['ustobr'] = ["Jan" => "Jan","Feb" => "Fev","Mar" => "Mar","Apr" => "Abr","May" => "Mai","Jun" => "Jun","Jul" => "Jul","Aug" => "Ago","Sep" => "Set", "Oct" => "Out","Nov" => "Nov","Dec" => "Dez"];
for ($number_month = 1; $number_month <= 12; $number_month++) {
    $lang['column']['out_month_minus' . $number_month] = date('M/Y', strtotime("-{$number_month} months"));
    $m = substr($lang['column']['out_month_minus' . $number_month], 0, 3);
    $lang['column']['out_month_minus' . $number_month] = substr_replace($lang['column']['out_month_minus' . $number_month],
        $lang['months']['ustobr'][$m], 0, 3);
}
   foreach ($lang['months']['ustobr'] as $key => $value) {
       if (date('M') == $key) {
           $data = $value.'/'.date('Y');
       }
   }
$lang['column']['out_current_month'] = $data;
/****************FIM MESES DINÂMICOS**************/
