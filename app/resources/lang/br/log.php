<?php

//=======Lang para Log - lista======//
$lang['log']['id'] = "ID";
$lang['log']['data_inclusao'] = "Data de Inclusão";
$lang['log']['ip'] = "IP";
$lang['log']['app'] = "Aplicação";
$lang['log']['acao'] = "Ação";
$lang['log']['origin'] = "Origem";
$lang['log']['usuario'] = "Usuário";
$lang['log']['campo'] = "Campo";
$lang['log']['val_antigo'] = "Valor Antigo";
$lang['log']['val_novo'] = "Valor Novo";
$lang['log']['csv_name'] = "info_sku_log";
