<?php
for($number_month = 1; $number_month <=12; $number_month++){
    $base = strtotime(date('Y-m',time()) . '-01 00:00:01');
    $rawMonth = date('M/Y',strtotime("-{$number_month} months", $base));
	$thisMonth = substr($rawMonth,0,3);
    $lang['vw_scr_scm405g_sku_list']['out_month_minus'.$number_month] = substr_replace($rawMonth, $lang['months']['ustobr'][$thisMonth], 0,3);
    $lang['vw_scr_scm420g_sug']['out_month_minus'.$number_month] = substr_replace($rawMonth, $lang['months']['ustobr'][$thisMonth], 0,3);

}
foreach ($lang['months']['ustobr'] as $key => $value) {
    if (date('M') == $key) {
        $data = $value.'/'.date('Y');
    }
}
$lang['vw_scr_scm405g_sku_list']['out_current_month'] = $data;
$lang['vw_scr_scm420g_sug']['out_current_month'] = $data;