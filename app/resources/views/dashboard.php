<script>
    var appliedFilters = '<?php echo $ViewData['applied_filters'] ?>'
</script>
<?php $supplier = true;
if(can("buyer")){
    $supplier = false;
}
?>
<div class="content-i pln-dashboard">
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">

                    <div class="d-flex flex-row justify-content-between mb-2 align-items-center">
                        <div>
                            <h4 class="pln-font-color"><?php echo  $lang['dashboard']['titulo'] ?></h4>
                            <!-- <div id="updateJobInfo">
                            <?php if (Login::getUserSession()) : ?>
                              <div class="pln-update-job-info-box text-left">
                                <div class="w-100 pln-small-line-height">
                                  <small><?=$lang['global']['status']?>: <span id="updateJobStatus" class="font-weight-bold-700"><?=$lang['global']['loading']?></span></small>
                                </div>
                                <div class="w-100 pln-small-line-height">
                                  <small><?=$lang['global']['last_update']?>: <span id="updateJobLastUpdate" class="font-weight-bold-700"><?=$lang['global']['loading']?></span></small>
                                </div>
                              </div>
                            <?php endif; ?>
                          </div> -->
                        </div>
                        <div class="d-flex flex-column">
                            <div>
                                <span class="pln-bold-600 mt-1 mr-1"><?=$lang['filtro']['my_filters']?>:</span>
                                <form action="dashboard" method="post" id="myfilter_form">
                                    <select id="selected_filter" name="selected_filter"
                                            class="form-control form-control-sm pln-select-filter-size">
                                        <option value=""></option>
                                        <?php
                                        if ($ViewData['filters']) {
                                            foreach ($ViewData['filters'] as $Select) {
                                                ?>
                                                <option
                                                    <?php echo(isset($Select['disable']) ? 'disable' : null) ?>
                                                    <?php echo($ViewData['selected_filter']['FILTER_ID'] == $Select['value'] ? 'selected' : '') ?>
                                                        value="<?php echo  $Select['value'] ?>"
                                                >
                                                    <?php echo  $Select['name'] ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </form>
                            </div>

                            <div class="d-flex">
								<span class="pln-link-color pln-font-smaller pln-cur-pointer mr-auto"
                                      data-target=".modal-filters-dashboard"
                                      data-toggle="modal"><?=$lang['filtro']['more_filters']?></span>
                                <span id="openFilterModal"
                                      class="pln-link-color pln-invi pln-font-smaller pln-cur-pointer"
                                      data-dismiss="modal" data-target="#modal_edit" data-toggle="modal"><span
                                            id="btn_filtro_text"><?=$lang['filtro']['edit_fillter']?></span></span>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <!--#plannexo INICIO DO CONTEÚDO-->
                    <div class="element-content">
                        <div class="row">
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="el-tablo">
                                    <div class="pln-box-title pln-font-color">
                                        <?php echo  $balanceamento['header'] ?>
                                    </div>
                                    <div class="value pln-critico">
                                        <?php echo(isset($ViewData['indicadores']['urgentes']['TOTAL']) ? Utilities::Unity($ViewData['indicadores']['urgentes']['TOTAL']) : 0) ?>
                                    </div>
                                    <div>
                                        <span><?=$lang['global']['country_currency']?> <?php echo (isset($ViewData['indicadores']['urgentes']['TOTAL_VAL']) ? Utilities::Currency($ViewData['indicadores']['urgentes']['TOTAL_VAL']) : '0,00') ?></span>
                                    </div>
                                    <div class="mt-1">
                                        <a href="/<?php echo $Country ?>/suggest/list/urgent" onclick="loaderShow()" class="pln-link-color">
                                            <?=$lang['global']['details']?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="el-tablo">
                                    <div class="pln-box-title pln-font-color">
                                        <?=$lang['sku_form']['data']['proximas_parcs']?>
                                    </div>
                                    <div class="value">
                                        <?php echo(isset($ViewData['indicadores']['prox_parcel']['TOTAL']) ? Utilities::Unity($ViewData['indicadores']['prox_parcel']['TOTAL']) : 0) ?>
                                    </div>
                                    <span style="vertical-align: bottom">	<?=$lang['sku_form']['data']['data_alterada_select']['sete_dias']?></span>
                                    <div>
								<span>
									<?=$lang['global']['country_currency']?> <?php echo (isset($ViewData['indicadores']['prox_parcel']['TOTAL_VAL']) ? Utilities::Currency($ViewData['indicadores']['prox_parcel']['TOTAL_VAL']) : '0,00') ?>
								</span>
                                    </div>
                                    <div class="mt-1">
                                        <a href="/<?php echo $Country ?>/suggest/list/upcoming" onclick="loaderShow()" class="pln-link-color">
                                            <?=$lang['global']['details']?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="el-tablo">
                                    <div class="pln-box-title pln-font-color">
                                        <?=$lang['suggest']['approved_suggests']?>
                                    </div>
                                    <div class="value">
                                        <?php echo(isset($ViewData['indicadores']['parcel_apr']['TOTAL']) ? Utilities::Unity($ViewData['indicadores']['parcel_apr']['TOTAL']) : 0) ?>
                                    </div>
                                    <span style="vertical-align: bottom">	<?=$lang['sku_form']['data']['data_alterada_select']['last_sete_dias']?></span>
                                    <div>
								<span>
									<?=$lang['global']['country_currency']?> <?php echo (isset($ViewData['indicadores']['parcel_apr']['TOTAL_VAL']) ? Utilities::Currency($ViewData['indicadores']['parcel_apr']['TOTAL_VAL']) : '0,00') ?>
								</span>
                                    </div>
                                    <div class="mt-1">

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xxxl-3">
                                <div class="el-tablo">
                                    <div class="pln-box-title pln-font-color">
                                        <?=$lang['purchaseorder']['delayed']?>
                                    </div>
                                    <div class="value">
                                        <?php echo(isset($ViewData['indicadores']['ordens']['TOTAL']) ? Utilities::Unity($ViewData['indicadores']['ordens']['TOTAL']) : 0) ?>
                                    </div>
                                    <div>
                                        <span>
                                            <?=$lang['global']['country_currency']?> <?php echo (isset($ViewData['indicadores']['ordens']['TOTAL_VAL']) ? Utilities::Currency($ViewData['indicadores']['ordens']['TOTAL_VAL']) : '0,00')?>
                                        </span>
                                    </div>
                                    <div class="mt-1 hide">
                                        <a href="#" class="pln-link-color"><?=$lang['info_sku']['dashboard']['mais_detalhes']?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--#plannexo FIM DO CONTEÚDO-->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <div class="element-content">
                        <div class="row">
                            <div class="col-sm-6 col-sm-6 col-xl-5">
                                <span class="pln-box-title pln-font-color">
                                    <?php echo  $balanceamentoestoque['header'] ?>
                                </span>
                            </div>
                            <div class="col-sm-6 col-xl-7">
                                <span class="pln-box-title pln-font-color text-uppercase">
                                    <?=$lang['info_sku']['dashboard']['balance_estoque']?>
                                </span>
                                <span class=" float-right">
							<a href="" data-target=".sku_detalhe"
                               data-toggle="modal"
                               id="maisdetalhes" class="pln-link-color"><?=$lang['info_sku']['dashboard']['mais_detalhes']?></a>
						</span>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-6 col-xl-5">
                                <div class="element-box h100">

                                    <div class="stockValue-spin col-sm-12 text-center d-flex justify-content-center">
                                        <div class="loader"></div>
                                    </div>

                                    <div id="stockValueChart" style="display: none;">
                                        <div class="row my-4">
                                            <div class="col-6 text-center">
                                                <div class="pln-bold text-uppercase"><?=$balanceamentoestoque['texto']['low']?>:</div>
                                                <span class="badge background-min-color text-white font-weight-bold"><?=$balanceamentoestoque['moeda']?> <?= Utilities::Currency($ViewData['detailStockBalancing']['minimum']['SALDO']) ?></span>
                                                <div class="pln-text-label-small"><?= $ViewData['detailStockBalancing']['minimum']['COBERTURA'] ?> <?=$balanceamentoestoque['texto']['daysCoverage']?></div>
                                            </div>

                                            <div class="col-6 text-center">
                                                <div class="pln-bold text-uppercase"><?=$balanceamentoestoque['texto']['high']?>:</div>
                                                <span class="badge background-max-color text-white font-weight-bold"><?=$balanceamentoestoque['moeda']?> <?= Utilities::Currency($ViewData['detailStockBalancing']['maximum']['SALDO']) ?></span>
                                                <div class="pln-text-label-small"><?= $ViewData['detailStockBalancing']['maximum']['COBERTURA'] ?> <?=$balanceamentoestoque['texto']['daysCoverage']?></div>
                                            </div>
                                        </div>
                                    
                                        <div id="stockValue" class="stock-value-chart-height"></div>
                                        <div id="stockValueLabel" class="float-right mb-5 stock-value-label-width">
                                            <div id="stockValue-disp-col" class="w-50 text-center float-left">
                                            </div>
                                            <div id="stockValue-total-col" class="w-50 text-center float-left">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xl-7">
                                <div class="element-box h100 py-3">

                                    <div class="stockBalanceChart-spin col-sm-12 text-center d-flex justify-content-center">
                                        <div class="loader"></div>
                                    </div>

                                    <div id="stockBalanceChart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Análise geral-->
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <h6 class="element-header pln-font-color">
                        <?=$lang['global']['evolution']?>
                    </h6>
                    <div class="element-content">
                        <div class="row">
                            <div class="col-sm-7 d-flex flex-row pln-pos-estoque-row">
                                <div class="pln-box-title pln-font-color">
                                    <?=$lang['info_sku']['dashboard']['posicao_estoque']?>
                                </div>

                                <div class="pln-analise-geral"><a href="javascript:void(0)" id="levelService" onclick="trocaFiltroChart('levelService')" class="pln-link-decoration-none"><?php echo $posicaoestoque['texto'][4]?></a></div>
                                <div class="pln-analise-geral"> | <a href="javascript:void(0)" id="excesso" onclick="trocaFiltroChart('excesso')" class="pln-link-color"><?php echo $posicaoestoque['texto'][5]?></a> |</div>
                                <div class="pln-analise-geral"><a href="javascript:void(0)" id="estoqueFinanceiro" onclick="trocaFiltroChart('estoqueFinanceiro')" class="pln-link-color"><?php echo $posicaoestoque['texto'][3]?></a></div>
                            </div>

                            <div class="col-sm-2 d-flex flex-row justify-content-end pln-pos-estoque-periodo-row">
                                <div class="pln-analise-geral "><a href="javascript:void(0)" id="pde_trimestral" onclick="getStockWeekly()" class="pln-link-color"><?php echo $posicaoestoque['semanal']?></a></div>
                                <div class="pln-analise-geral"> | <a href="javascript:void(0)" id="pde_anual" onclick="getStockMonthly()" class="pln-link-decoration-none"><?php echo $posicaoestoque['mensal']?></a> </div>
                            </div>


                            <div class="col-sm-3 d-flex flex-row pln-pos-estoque-row">
                                <div class="pln-box-title pln-font-color">
                                    <?=$lang['global']['position']?>
                                </div>
                                <div class="pln-analise-geral pln-box-title  pln-font-color">
                                    <?php echo $ViewData['datepercent'] ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="element-box h100">

                                    <div class="col-sm-12 posicaoEstoqueChart-spin text-center d-flex justify-content-center">
                                        <div class="loader"></div>
                                    </div>

                                    <div id="posicaoEstoqueChart" width="100%" height="500px"></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="pln-analise-geral-nivel-servico d-flex justify-content-center flex-column element-content">
                                    <span class="pln-font-smaller align-self-center"><?php echo $posicaoestoque['texto'][4]?></span>

                                    <span class="align-self-center pln-font-xxlarge font-weight-bold"><?php echo(isset($ViewData['lospercentage']) ? $ViewData['lospercentage'] . "%" : 0)?></span>

                                </div>
                                <div class="pln-analise-geral-excesso d-flex justify-content-center flex-column element-content">
                                    <span class="pln-font-smaller align-self-center"><?php echo $posicaoestoque['texto'][5]?></span>
                                    <span class="align-self-center pln-font-xlarge font-weight-bold"><?=$lang['global']['country_currency']?> <?php echo Utilities::Currency($ViewData['unified']['EXCESS_VAL'])?></span>
                                    <span class="align-self-center"><?php echo $ViewData['unified']['DAYS'].$posicaoestoque['dias']?></span>
                                </div>
                                <div class="pln-analise-geral-total-disp d-flex justify-content-center flex-column element-content">
                                    <span class="pln-font-smaller align-self-center"><?php echo $posicaoestoque['texto'][3]?></span>
                                    <span class="align-self-center pln-font-xlarge font-weight-bold"><?=$lang['global']['country_currency']?> <?php echo Utilities::Currency($ViewData['unified']['AVAILABLE_VAL'])?></span>
                                    <span class="align-self-center"><?php echo $ViewData['unified']['AVAILABLE_DAYS_TOTAL'].$posicaoestoque['dias']?></span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--wrapper-->

                <div class="element-wrapper">
                    <div class="element-content">
                        <!--ENTRADAS X SAÍDAS-->
                        <div class="row">
                            <div class="col-sm-9 d-flex flex-row">
                                <div class="pln-box-title pln-font-color">
                                    <?php echo $entradasaidas['header'] ?>
                                </div>
                            </div>
                            <div class="col-sm-3 d-flex flex-row justify-content-end pln-pos-entrada-saida-row">
                                <div class="pln-analise-geral "><a href="javascript:void(0)" id="pes_semanal" onclick="trocaPeriodoEntradaSaida('W', 'pes_semanal')" class="pln-link-color"><?php echo $entradasaidas['semanal']?></a></div>
                                <div class="pln-analise-geral"> | <a href="javascript:void(0)" id="pes_anual" onclick="trocaPeriodoEntradaSaida('A', 'pes_anual')" class="pln-link-decoration-none"><?php echo $entradasaidas['mensal']?></a> </div>
                            </div>
                            <div class="col-sm-3 d-flex flex-row justify-content-end">
                                <!-- <div class="pln-analise-geral "><a href="javascript:void(0)" class="pln-link-color">Semana</a></div> -->
                                <!-- <div class="pln-analise-geral">| <a href="javascript:void(0)" class="pln-link-color">Mês</a> |</div> -->
                                <!-- <div class="pln-analise-geral"><a href="javascript:void(0)" class="pln-link-color">Ano</a></div> -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-box">
                                    <div class="col-sm-12 plnDoubleBarLineChart-spin text-center d-flex justify-content-center">
                                        <div class="loader"></div>
                                    </div>
                                    <div id="plnDoubleBarLineChart" width="100%" height="500px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="element-wrapper">
                    <div class="element-content">
                        <!--INDICE DE URGENCIA-->
                        <div class="row">
                            <div class="col-sm-10 d-flex flex-row pln-urgency-index-type-row">
                                <div class="pln-box-title pln-font-color">
                                    <?php echo $indicedeurgencia['header'] ?>
                                </div>
                                <div class="pln-analise-geral"><a href="javascript:void(0)" id="financialVolume" onclick="urgencyChartType(this.id, '.pln-link-urgency-type')" class="pln-link-urgency-type pln-link-decoration-none"> <?php echo $indicedeurgencia['texto'][0]?></a></div>
                                <div class="pln-analise-geral"> | <a href="javascript:void(0)" id="urgencyPercent" onclick="urgencyChartType(this.id, '.pln-link-urgency-type')" class="pln-link-urgency-type pln-link-color"><?php echo $indicedeurgencia['texto'][1]?></a></div>
                            </div>
                            <div class="col-sm-2 d-flex flex-row justify-content-end pln-urgency-index-period-row">
                                <div class="pln-analise-geral "><a href="javascript:void(0)" id="weekly_urgency" onclick="urgencyChartPeriod(this.id, '.pln-link-urgency-period')" class="pln-link-urgency-period pln-link-color"><?php echo $indicedeurgencia['texto'][2]?></a></div>
                                <div class="pln-analise-geral">| <a href="javascript:void(0)" id="monthly_urgency" onclick="urgencyChartPeriod(this.id, '.pln-link-urgency-period')" class="pln-link-urgency-period pln-link-decoration-none"><?php echo $indicedeurgencia['texto'][3]?></a></div>
                            </div>

                            <div class="col-sm-2 d-flex flex-row">
                                <div class="pln-urgency-date pln-box-title pln-font-color">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-box mb-0 px-0 py-3">

                                    <div class="col-sm-10 plnUrgencyIndexChart-spin text-center d-flex justify-content-center">
                                        <div class="loader"></div>
                                    </div>

                                    <div id="financialVolumeChart"></div>
                                    <div id="urgencyPercentChart" style="display: none"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(!$supplier): ?>
                    <div class="element-wrapper">
                        <div class="element-content">
                            <!--INDICE DE ADERENCIA-->
                            <div class="row">
                                <div class="col-sm-8 d-flex flex-row">
                                    <div class="pln-box-title pln-font-color">
                                        <?php echo $indicedeaderencia['header'] ?>
                                    </div>
                                </div>
                                <div class="col-sm-2 d-flex flex-row justify-content-end pln-adhension-index-row">
                                    <div class="pln-analise-geral"><a href="javascript:void(0)" id="weekly_adhension" onclick="adhensionIndexChartWeekly()" class="pln-link-adhension-period pln-link-color"><?=$indicedeaderencia['texto'][0]?></a></div>
                                    <div class="pln-analise-geral">| <a href="javascript:void(0)" id="monthly_adhension" onclick="adhensionIndexChartMonthly()" class="pln-link-adhension-period pln-link-decoration-none"><?=$indicedeaderencia['texto'][1]?></a></div>
                                </div>

                                <div class="col-sm-2 d-flex flex-row">
                                    <div class="pln-adhension-date pln-box-title pln-font-color">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="element-box mb-0 px-0 py-3">

                                        <div class="col-sm-10 plnAdhensionIndexChart-spin text-center d-flex justify-content-center">
                                            <div class="loader"></div>
                                        </div>

                                        <div id="adhensionIndexChart"></div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="element-box mb-0 p-0 h100 pln-vertical-graph-wrapper">

                                        <div class="plnAdhensionIndexChart-spin d-flex justify-content-center flex-column element-content h100">
                                            <div class="loader align-self-center"></div>
                                        </div>

                                        <div class="pln-adhension-index-chart d-flex justify-content-center flex-column element-content h100">
                                            <h1 class="align-self-center pln-adhension-index-value  pln-font-color"></h1>
                                            <span class="align-self-center pln-font-color text-uppercase"><?=$indicedeaderencia['adhesion_on_plannexo'] ?></span>
                                        </div>
                                        <div class="pln-adhension-vertical-graph"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
        <!--Análise geral end-->
    </div>
    <!-- content-box end-->
</div>
<!-- content-i end -->
</div>
</div>
<div class="display-type"></div>
</div>


<!-- Modal salvar pesquisa -->
<div class="modal fade" id="modal_edit" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="botao_editar" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_titulo" name="modal_titulo"><?=$lang['filtro']['new_filter']?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">

                <div style="width: 100%">
                    <input placeholder="Digite nome do novo filtro" maxlength="50" type="text" class="form-control" name="filter_nome" id="filter_nome">
                </div>
                <div style="width: 100%; margin-top: 5px">
                    <input name="filter_perfil" id="filter_perfil" type="hidden" value="Publico">
                </div>
                <input type="hidden" name="filter_tela_id" id="filter_tela_id" value="SCM453C">
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <span class="pln-danger pln-cur-pointer" id="confirm_delete" onclick="deleteFilter()"><i class="fa fa-times pln-danger"></i> <span><?=$lang['filtro']['delete']?></span></span>
                <button type="button" class="btn pln-btn-orange" id="btnfiltro_salvar" onclick="execSerialization()"><i class="fa fa-save"></i> <span class="saving_spn"><?=$lang['filtro']['save']?></span></button>
            </div>
        </div>
    </div>
</div>


<!--MODAL DETALHES DE SKU-->
<div aria-hidden="true" aria-labelledby="DetalheSKU" class="modal fade sku_detalhe" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?= $lang['dashboard']['sku_detalhe']['titulo'] ?>
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span>
                    <small><?= $lang['modal']['fechar'] ?></small>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="font-weight-bold"><?= $lang['dashboard']['sku_detalhe']['sinalizador'] ?></th>
                        <th class="font-weight-bold"><?= $lang['dashboard']['sku_detalhe']['qtd_sku'] ?></th>
                        <th class="font-weight-bold"><?= $lang['dashboard']['sku_detalhe']['percentual'] ?></th>
                        <th class="font-weight-bold"><?= $lang['dashboard']['sku_detalhe']['estoque_disponivel'] ?></th>
                        <th class="font-weight-bold"><?= $lang['dashboard']['sku_detalhe']['excesso'] ?></th>
                        <th class="font-weight-bold"><?= $lang['dashboard']['sku_detalhe']['solicitacaoes_compra'] ?></th>
                        <th class="font-weight-bold"><?= $lang['dashboard']['sku_detalhe']['ordens_compra'] ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($ViewData['linhaData'] as $key => $value) {
                        echo $value;
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <tr class="bg-dark text-white">
                        <td><?= $lang['dashboard']['sku_detalhe']['total'] ?></td>
                        <td><?= number_format($ViewData['totais']['SKUS'], 0, "", ".") ?></td>
                        <td>100%</td>
                        <td><?= $lang['parametros']['real'] ?> <?= Utilities::Currency($ViewData['totais']['SALDO']) ?></td>
                        <td><?= $lang['parametros']['real'] ?> <?= Utilities::Currency($ViewData['totais']['QTY_EXCESS']) ?></td>
                        <td><?= $lang['parametros']['real'] ?> <?= Utilities::Currency($ViewData['totais']['REQ_PURCH']) ?></td>
                        <td><?= $lang['parametros']['real'] ?> <?= Utilities::Currency($ViewData['totais']['PURCH_ORDER']) ?></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- MODAL FILTER -->
<div aria-hidden="true" aria-labelledby="FiltrosSKU" class="modal fade modal-filters-dashboard" role="dialog"
     tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form action="dashboard" method="post" id="formulario">
            <!--input type="hidden" name="selected_filter" id="selected_filter" value=""-->
            <div class="modal-content" style="background-color:#f9f9f9">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?=  $lang['dashboard_filtros']['titulo'] ?>
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span
                                aria-hidden="true"> &times;</span>
                        <small><?=$lang['global']['close']?></small>
                    </button>
                </div>
                <div class="modal-body modal-dashboard">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-2 float-left">
                                <a href="javascript:void(0)"
                                   id="limpar_filtros"><?=  $lang['dashboard_filtros']['limpar'] ?></a>
                            </div>
                        </div>
                        <div class="row">
                            <!--SKU-->
                            <div class="col-sm-4">
                                <div class="element-box pln-min-height-modal-dashboard-l1">
                                    <fieldset class="form-group">
                                        <legend><span><?=  $lang['filtro']['card_sku']['titulo'] ?></span></legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="cod_item"><?=  $lang['filtro']['card_sku']['codigo'] ?></label>
                                                    <input type='text' class='form-control' maxlength="15"
                                                           name='cod_item_pk' id='cod_item_pk' value=''>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="desc_item"><?=  $lang['filtro']['card_sku']['desc'] ?></label>
                                                    <input type='text' class='form-control' maxlength="50"
                                                           name='desc_item' id='desc_item' value=''>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <!--SKU FIM-->

                            <!--SINALIZADOR-->
                            <div class="col-sm-4">
                                <div class="element-box pln-min-height-modal-dashboard-l1">
                                    <fieldset class="form-group">
                                        <legend><span><?=  $lang['filtro']['card_parametros']['sinalizador'] ?></span>
                                        </legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row mt-2">
                                                    <?php
                                                    $inv_level = array('', '', '', '', '', '');
                                                    if (is_array($ViewData['chkvetor']['inv_level'])) {
                                                        foreach ($ViewData['chkvetor']['inv_level'] as $chave => $valor) {
                                                            $inv_level[$valor] = "checked";
                                                        }
                                                    }
                                                    ?>
                                                    <div class="col-sm-12">
                                                        <input type="checkbox" name="inv_level[]" id="inv_level_0"
                                                               value="0" <?=  $inv_level[0] ?>>
                                                        <label for="inv_level_0"><?=  $lang['dashboard_filtros']['sinalizador']['zero'] ?></label>

                                                        <input class="ml-2" type="checkbox" name="inv_level[]"
                                                               id="inv_level_1" value="1" <?=  $inv_level[1] ?>>
                                                        <label for="inv_level_1"><?=  $lang['dashboard_filtros']['sinalizador']['muito_baixo'] ?></label>

                                                        <input class="ml-2" type="checkbox" name="inv_level[]"
                                                               id="inv_level_2" value="2" <?=  $inv_level[2] ?>>
                                                        <label for="inv_level_2"><?=  $lang['dashboard_filtros']['sinalizador']['baixo'] ?></label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input type="checkbox" name="inv_level[]" id="inv_level_3"
                                                               value="3" <?=  $inv_level[3] ?>>
                                                        <label for="inv_level_3"><?=  $lang['dashboard_filtros']['sinalizador']['ideal'] ?></label>

                                                        <input class="ml-2" type="checkbox" name="inv_level[]"
                                                               id="inv_level_4" value="4" <?=  $inv_level[4] ?>>
                                                        <label for="inv_level_4"><?=  $lang['dashboard_filtros']['sinalizador']['alto'] ?></label>

                                                        <input class="ml-2" type="checkbox" name="inv_level[]"
                                                               id="inv_level_5" value="5" <?=  $inv_level[5] ?>>
                                                        <label for="inv_level_5"><?=  $lang['dashboard_filtros']['sinalizador']['muito_alto'] ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <legend><span><?=  $lang['dashboard_filtros']['curvas']['titulo'] ?></span>
                                        </legend>
                                        <div class="row">
                                            <?php
                                            $curve_abc["A"] = "";
                                            $curve_abc["B"] = "";
                                            $curve_abc["C"] = "";
                                            if ($ViewData['chkvetor']['curve_abc']) {
                                                $curve_abc[$ViewData['chkvetor']['curve_abc']] = "checked";
                                            }

                                            $curve_pqr["P"] = "";
                                            $curve_pqr["Q"] = "";
                                            $curve_pqr["R"] = "";
                                            if ($ViewData['chkvetor']['curve_pqr']) {
                                                $curve_pqr[$ViewData['chkvetor']['curve_pqr']] = "checked";
                                            }

                                            $curve_xyz["X"] = "";
                                            $curve_xyz["Y"] = "";
                                            $curve_xyz["Z"] = "";
                                            if ($ViewData['chkvetor']['curve_xyz']) {
                                                $curve_xyz[$ViewData['chkvetor']['curve_xyz']] = "checked";
                                            }

                                            $curve_123["1"] = "";
                                            $curve_123["2"] = "";
                                            $curve_123["3"] = "";
                                            if ($ViewData['chkvetor']['curve_123']) {
                                                $curve_123[$ViewData['chkvetor']['curve_123']] = "checked";
                                            }

                                            ?>
                                            <div class="col-sm-6">
                                                <div class="row mt-2">
                                                    <div class="col-sm-12">
                                                        <input type="checkbox" name="curve_abc[]" id="curve_abc_A"
                                                               value="A" <?=  $curve_abc['A'] ?> > <label
                                                                for="curve_abc_A"><?=  $lang['dashboard_filtros']['curvas']['a'] ?></label>
                                                        <input class="ml-2" type="checkbox" name="curve_abc[]"
                                                               id="curve_abc_B" value="B" <?=  $curve_abc['B'] ?> >
                                                        <label for="curve_abc_B"><?=  $lang['dashboard_filtros']['curvas']['b'] ?></label>
                                                        <input class="ml-2" type="checkbox" name="curve_abc[]"
                                                               id="curve_abc_C" value="C" <?=  $curve_abc['C'] ?> >
                                                        <label for="curve_abc_C"><?=  $lang['dashboard_filtros']['curvas']['c'] ?></label>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input type="checkbox" name="curve_pqr[]" id="curve_pqr_P"
                                                               value="P" <?=  $curve_pqr['P'] ?> > <label
                                                                for="curve_pqr_P"><?=  $lang['dashboard_filtros']['curvas']['p'] ?></label>
                                                        <input class="ml-2" type="checkbox" name="curve_pqr[]"
                                                               id="curve_pqr_Q" value="Q" <?=  $curve_pqr['Q'] ?> >
                                                        <label for="curve_pqr_Q"><?=  $lang['dashboard_filtros']['curvas']['q'] ?></label>
                                                        <input class="ml-2" type="checkbox" name="curve_pqr[]"
                                                               id="curve_pqr_R" value="R" <?=  $curve_pqr['R'] ?> >
                                                        <label for="curve_pqr_R"><?=  $lang['dashboard_filtros']['curvas']['r'] ?></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="row mt-2">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-12">
                                                            <input type="checkbox" name="curve_xyz[]" id="curve_xyz_X"
                                                                   value="X" <?=  $curve_xyz['X'] ?> > <label
                                                                    for="curve_xyz_X"><?=  $lang['dashboard_filtros']['curvas']['x'] ?></label>
                                                            <input class="ml-2" type="checkbox" name="curve_xyz[]"
                                                                   id="curve_xyz_Y" value="Y" <?=  $curve_xyz['Y'] ?> >
                                                            <label for="curve_xyz_Y"><?=  $lang['dashboard_filtros']['curvas']['y'] ?></label>
                                                            <input class="ml-2" type="checkbox" name="curve_xyz[]"
                                                                   id="curve_xyz_Z" value="Z" <?=  $curve_xyz['Z'] ?> >
                                                            <label for="curve_xyz_Z"><?=  $lang['dashboard_filtros']['curvas']['z'] ?></label>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <input type="checkbox" name="curve_123[]" id="curve_123_1"
                                                                   value="1" <?=  $curve_123['1'] ?> > <label
                                                                    for="curve_123_1"><?=  $lang['dashboard_filtros']['curvas']['1'] ?></label>
                                                            <input class="ml-2" type="checkbox" name="curve_123[]"
                                                                   id="curve_123_2" value="2" <?=  $curve_123['2'] ?> >
                                                            <label for="curve_123_2"><?=  $lang['dashboard_filtros']['curvas']['2'] ?></label>
                                                            <input class="ml-2" type="checkbox" name="curve_123[]"
                                                                   id="curve_123_3" value="3" <?=  $curve_123['3'] ?> >
                                                            <label for="curve_123_3"><?=  $lang['dashboard_filtros']['curvas']['3'] ?></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <!--SINALIZADOR FIM-->

                            <!--ESTABELECIMENTOS-->
                            <div class="col-sm-4">
                                <div class="element-box pln-min-height-modal-dashboard-l1">
                                    <fieldset class="form-group">
                                        <legend><span><?=  $lang['filtro']['card_estab']['titulo'] ?></span></legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="checkbox" id="chk_check_all_estabelecimento"> <label
                                                        for="chk_check_all_estabelecimento"><?=  $lang['parametros']['todos'] ?></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 d-flex flex-column pln-content-scroll">
                                                <?php
                                                $cod_estab_fk = array();
                                                if (isset($ViewData['chkvetor']['cod_estab_fk'])) {
                                                    if (is_array($ViewData['chkvetor']['cod_estab_fk'])) {
                                                        foreach ($ViewData['chkvetor']['cod_estab_fk'] as $chave => $valor) {
                                                            $cod_estab_fk[$valor] = "checked";
                                                        }
                                                    }
                                                }

                                                $x = 0;
                                                foreach ($ViewData['Estab'] as $Estab) {
                                                    if ($x == 0) {
                                                        $class = "class='mt-2 d-flex flex-row text-nowrap align-items-center'";
                                                    } else {
                                                        $class = "class='d-flex flex-row text-nowrap align-items-center'";
                                                    }

                                                    if (array_key_exists($Estab['code'], $cod_estab_fk)) {
                                                        $chk_state = $cod_estab_fk[$Estab['code']];
                                                    } else {
                                                        $chk_state = "";
                                                    }

                                                    echo "
														<div {$class}>
															<input type='checkbox' name=\"cod_estab_fk[]\" id=\"cod_estab_fk_{$Estab['code']}\" value=\"{$Estab['code']}\" {$chk_state} >
															<label for='cod_estab_fk_{$Estab['code']}'>{$Estab['name']}</label>
														</div>
														";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <!--ESTABELECIMENTOS FIM-->

                            <!--POLITICA-->
                            <div class="col-sm-4">
                                <div class="element-box pln-min-height-modal-dashboard-l1">
                                    <fieldset class="form-group">
                                        <legend><span><?=  $lang['filtro']['card_parametros']['politica'] ?></span>
                                        </legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="checkbox" id="chk_check_all_politica"> <label
                                                        for="chk_check_all_politica"><?=  $lang['parametros']['todos'] ?></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 pln-content-scroll">
                                                <?php //sku_form_campos($sql,"iid_profile",$_POST['iid_profile']);?>
                                                <?php
                                                $iid_profile_chk = array();
                                                if (isset($ViewData['chkvetor']['id_profile'])) {
                                                    if (is_array($ViewData['chkvetor']['id_profile'])) {
                                                        foreach ($ViewData['chkvetor']['id_profile'] as $chave => $valor) {
                                                            $valor = (int)$valor;
                                                            $iid_profile_chk[$valor] = "checked";
                                                        }
                                                    }
                                                }

                                                foreach ($ViewData['Profile'] as $row) {
                                                    if (array_key_exists($row['ID_PROFILE_PK'], $iid_profile_chk)) {
                                                        $chk_state = $iid_profile_chk[$row['ID_PROFILE_PK']];
                                                    } else {
                                                        $chk_state = "";
                                                    }

                                                    echo "<div class='d-flex flex-row text-nowrap align-items-center'>
												<input type='checkbox' name='id_profile[]' id='id_profile_{$row['ID_PROFILE_PK']}' value='{$row['ID_PROFILE_PK']}' {$chk_state} >
												<label class='text-truncate pl-1' for='id_profile_{$row['ID_PROFILE_PK']}'>{$row['NOME']}</label>
												</div>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <!--POLITICA FIM-->

                            <!--GESTOR-->
                            <? if (!can('supplier')): ?>
                                <div class="col-sm-4">
                                    <div class="element-box pln-min-height-modal-dashboard-l1">
                                        <fieldset class="form-group">
                                            <legend><span><?=$lang['filtro']['card_gestor']['titulo']?></span></legend>
                                            <div class="row">
                                                <div class="col-sm-12 pln-content-scroll">
                                                    <? foreach($ViewData['User'] as $Key => $User): ?>
                                                        <div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                            <input type='checkbox' name='user_id[]' id='user_id_<?= $User['ID_USER_PK']?>' value='<?= $User['ID_USER_PK']?>'>
                                                            <label class="text-truncate pl-1" for='user_id_<?= $User['ID_USER_PK']?>'><?= $User['LOGIN']?></label>
                                                        </div>
                                                    <? endforeach; ?>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            <? endif; ?>
                            <!--GESTOR FIM-->

                            <!-- FILTROS DINAMICOS -->
                            <?php foreach ($ViewData['Groups'] as $key => $Group): ?>
                                <div class="col-sm-4">
                                    <div class="element-box min-box-height-l5">
                                        <fieldset class="form-group">
                                            <legend>
                                        <span onclick="hide_filter('ipt_find_<?php echo $key ?>')"
                                              class="pln-cur-pointer"><?php echo $Group ?>
                                            <i class="fa fa-search pln-link-color"></i>
                                        </span>
                                            </legend>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="text" placeholder="filtrar <?php echo $Group ?>"
                                                           class="form-control pln-invi mb-1"
                                                           id="ipt_find_<?php echo $key ?>"
                                                           onkeyup="search_filter('ipt_find_<?php echo $key ?>', 'div_filter_<?php echo $key ?>')">
                                                </div>
                                                <div class="col-sm-12">
                                                    <input type="checkbox" class="chk_check_all"
                                                           id="chk_check_all_<?php echo $key ?>"> <label
                                                            for="chk_check_all_<?php echo $key ?>"><?= $lang['parametros']['todos'] ?></label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 pln-content-scroll"
                                                     id="div_filter_<?php echo $key ?>">
                                                    <?php
                                                    foreach ($ViewData['Group_' . $key] as $Key2 => $Campo) {
                                                        ?>
                                                        <div class="d-flex <?= ($Key2 == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                            <input type='checkbox'
                                                                   name='cod_group<?php echo $key ?>_fk[]'
                                                                   id='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>'
                                                                   value='<?= $Campo['COD_GROUP_PK'] ?>'>
                                                            <label class='text-truncate pl-1'
                                                                   for='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>'><?= $Campo['DESC_GROUP_CONT'] ?></label>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <!--END FILTROS DINAMICOS -->
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button id="btn_novo" type="button" class="btn pln-btn-primary float-right" data-dismiss="modal" data-target="#modal_edit" data-toggle="modal" style="width:80px;text-align:center;">
                            <span id="btn_filtro_text"><?=$lang['global']['save']?></span>
                        </button>
                        <button class="btn btn-secondary" data-dismiss="modal" type="button"> <?=$lang['dashboard_filtros']['botao']['cancelar'] ?></button>
                        <button class="btn pln-btn-primary" onclick="removeFromLocalStorage()" type="submit"> <?=$lang['dashboard_filtros']['botao']['aplicar'] ?></button>
                    </div>
                </div>
        </form>
    </div>
</div>
<!-- MODAL FILTER END -->

<input type="hidden" id="can_supplier" value="<?php echo $supplier ?>">
<input type="hidden" id="filtroPeriodoSelecionado" value="A">
<input type="hidden" id="filtroChartSelecionado" value="levelService">
<input type="hidden" id="dataPosicaoEstoque_A" value='<?php echo $ViewData['stockposA'] ?>'>
<input type="hidden" id="dataPosicaoEstoque_T" value="<?php echo $ViewData['stockposT'] ?>">

<script>
    filterData = <?= json_encode($ViewData['selected_filter'])?>;
    obj_stock_pos_labels = <?=(json_encode($lang['dashboard']['pos_estoque']))?>;
    obj_stock_balance = <?=(json_encode($ViewData['unified']))?>;
    obj_stock_min = <?= (json_encode($ViewData['detailStockBalancing']['minimum']['SALDO'])) ?>;
    obj_stock_max = <?= (json_encode($ViewData['detailStockBalancing']['maximum']['SALDO'])) ?>;
    obj_stock_balance_labels = <?=(json_encode($lang['dashboard']['balanceamento']))?>;
    obj_inout_labels = <?=(json_encode($lang['dashboard']['entradas_saidas']))?>;
    obj_locale = <?=(json_encode($lang['dashboard']['locale']))?>;
</script>
<!-- Google Charts Loader -->
<script src="https://www.gstatic.com/charts/loader.js"></script>
<?php
$JS = [
    'uglify/all-dashboard-min'
];
?>
