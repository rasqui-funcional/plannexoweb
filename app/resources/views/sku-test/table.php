<?php
if (!$ViewData['dataTable']) {
    echo 'false';
    return;
}
?>
<thead>
    <tr>
        <? foreach ($ViewData['Cols'] as $ColName => $ColData): ?>
                <th>
                    <div class="d-flex flex-row pln-cur-pointer order-col" data-id="<?= $ColName ?>">
                        <?= (!isset($lang['vw_scr_scm405g_sku_list']['abrev'][strtolower($ColName)]) ?
                            $lang['vw_scr_scm405g_sku_list'][strtolower($ColName)] :
                            $lang['vw_scr_scm405g_sku_list']['abrev'][strtolower($ColName)]) ?>
                        <i class="fa fa-sort<?= $ColData['order'] == null ? "" : "-" . $ColData['order'] ?> pln-link-color ml-1 align-self-center"></i>
                    </div>
                </th>
        <? endforeach; ?>
    </tr>
</thead>

<tbody>
<?php
foreach ($ViewData['dataTable'] as $key => $Data) {
    ?>
    <tr class="table_tr" id="<?=$Data['ID_SKU_PK']?>" data-id-sku="<?=$Data['ID_SKU_PK']?>" data-lt="<?= $Data['LT_TOTAL_OUT'] ?>" data-out6="<?= $Data['OUT6'] ? Utilities::dataFormatter('OUT6', $Data['OUT6']) : 0 ?>" data-days-horizon="<?=$Data['DAYS_HORIZON']?>">
        <?php
        foreach ($ViewData['Cols'] as $ColName => $ColData) {
            $Value = Utilities::dataFormatter($ColName, $Data[$ColName]);
            switch ($ColData['type']) {
                case 'img':
                    echo ViewImage::invLevelImg($Data[$ColName], $lang);
                    break;

                case 'text-modal':
                    echo View::stock($Data, $Value, 'OverStock', 'Info');
                    break;

                case 'order':
                    echo '<td>' . View::modalOrder($Data, $Value) . '</td>';
                    break;
                case 'ind_excess':                            
                    echo ViewImage::indExcessImg($Data['ID_SKU_PK'], $Data[$ColName]);
                    break;
                case 'desc_item': ?>
                    <td class="text-truncate pln-td-mw-200">
                        <span data-toggle="tooltip"
                                title="<?= $Value;?>">
                            <?= $Value;?>
                        </span>
                    </td>
                <?    break;

                case 'max_width_100': ?>
                    <td class="text-truncate pln-td-mw-100">
                        <span data-toggle="tooltip"
                                title="<?= $Value;?>">
                            <?= $Value;?>
                        </span>
                    </td>
                <?    break;

                /* case 'variacao_consumo':
                    ?>
                    <td class="pln-custom-tooltip">
                        <div class="d-block text-right pln-tooltip <? echo $Value > 0 ? 'text-danger' : 'pln-text-dark' ?>">
                            <span class="td-data pln-bold-600">
                                <? echo $Value == 0 ? "&mdash; {$lang['vw_scr_scm405g_sku_list']['no_variation']} &mdash;" : "{$Value}%" ?>
                                <i class="fas <? echo $Value == 0 ? 'd-none' : ($Value > 0 ? 'fa-arrow-up' : 'fa-arrow-down') ?>"></i>
                            </span>

                            <span class="tooltiptext bg-white shadow-sm">
                            <div class="tooltip-title <? echo $Value > 0 ? 'bg-danger' : 'bg-grey' ?>">
                                <?= $Value == 0 ? $lang['vw_scr_scm405g_sku_list']['no_consumption_variation'] : ($Value > 0 ? $lang['vw_scr_scm405g_sku_list']['increase_consumption'] : $lang['vw_scr_scm405g_sku_list']['consumption_reduction']) ?>
                            </div>

                            <div class="tooltip-body d-flex justify-content-around">
                                <div>
                                    <div class="cmd-value"><?= (!is_null($Data['TOTAL_OUT_LAST_MONTH_AVG'])) ? round($Data['TOTAL_OUT_LAST_MONTH_AVG'], 2) : 0 ?></div>
                                    <div class="cmd-days"><? echo $lang['vw_scr_scm405g_sku_list']['cmd30'] ?></div>
                                </div>

                                <div>
                                    <div class="cmd-value"><?= (!is_null($Data['TOTAL_OUT_LAST_HALF_MONTH_AVG']) ? round($Data['TOTAL_OUT_LAST_HALF_MONTH_AVG'], 2) : 0) ?></div>
                                    <div class="cmd-days"><? echo $lang['vw_scr_scm405g_sku_list']['cmd15'] ?></div>
                                </div>

                                <div>
                                    <div class="cmd-value <? echo $Value == 0 ? 'pln-text-dark' : ($Value > 0 ? 'text-danger' : 'text-grey') ?> ">
                                        <?= (!is_null($Data['TOTAL_OUT_LAST_WEEK_AVG']) ? round($Data['TOTAL_OUT_LAST_WEEK_AVG'], 2) : 0) ?>
                                    </div>
                                    <div class="cmd-days"><? echo $lang['vw_scr_scm405g_sku_list']['cmd7'] ?></div>
                                </div>
                            </div>
                            </span>
                        </div>
                    </td>
                <? break; */

                default:
                        echo ViewSku::defaultView($ColName, $Value);
                        break;
            }
        }
        ?>
    </tr>
    <?php
}
?>
</tbody>

<script> initialize(); </script>
