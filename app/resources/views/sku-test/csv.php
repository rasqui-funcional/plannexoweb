<?php
if (!empty($ViewData['error'])) {
    echo $ViewData['error'];
    exit;
}
$session = Login::getUserSession();
$users = Redis::HGet("company:{$session['id_company_pk']}:users");
$estabs = Redis::HGet("company:{$session['id_company_pk']}:estabs");
$calendar = Redis::HGet("company:{$session['id_company_pk']}:calendars");
ob_end_clean();

Request::generateCSVHeaders($lang['vw_scr_scm405g_sku_list']['name']);

$output = fopen('php://output', 'w');
$translatedTitles = [];
$titles = [];
foreach ($ViewData['dataTable'][0] as $title => $value) {
    $titles[$title] = $title;
}
foreach($ViewData['collumns_exib'] as $collum => $value){
    $translatedTitles[$value] = $lang['vw_scr_scm405g_sku_list'][strtolower($value)];
}

fputcsv($output, mb_convert_encoding($translatedTitles, 'ISO-8859-1'), ';');
foreach ($ViewData['dataTable'] as $row) {
    unset($titles['USER_ID']);
    unset($titles['COD_ESTAB_FK']);
    unset($titles['ID_CALENDAR_FK']);
    $row['COD_ESTAB_FK'] = $estabs[$row['COD_ESTAB_FK']];
    $row['ID_CALENDAR_FK'] = $calendar[$row['ID_CALENDAR_FK']];
    $row['USER_ID'] = $users[$row['USER_ID']];
    foreach ($titles as $title) {
        $row[$title] = Utilities::formatCsvFields($title, $row[$title]);
    }
    $data = [];
    foreach($ViewData['collumns_exib'] as $collum => $value){
        $data[$value] = $row[strtoupper($value)];
    }
    fputcsv($output, mb_convert_encoding($data, 'ISO-8859-1'), ';');
}
?>