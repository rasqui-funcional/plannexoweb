<script>
    let filter_temp = <?= $ViewData['filter_temp'] ?>;
</script>

<div id="result"></div>

<div class="content-i pln-skuform">
    <div class="content-box">
        <!--START - Recent Ticket Comments-->
        <div class="element-wrapper">
            <div class="d-flex flex-row justify-content-between mb-2 align-items-center">
                <div>
                    <h4 class="pln-font-color"><span data-toggle="tooltip"
                                                     title="<?= $lang['sku']['view'] ?>"><?= $lang['sku_form']['cols'][''] ?></span>
                    </h4>
                </div>

                <div class="d-flex flex-row">
                    <span class="pln-bold-600 mt-1 mr-1"><?= $lang['filtro']['my_filters'] ?>:</span>
                    <div class="d-flex flex-column align-items-end">
                        <div>
                            <select id="select_filtro" name="select_filtro"
                                    class="form-control-sm pln-select-filter-size">
                                <option value=""></option>
                                <?php
                                if ($ViewData['select_filtro']) {
                                    foreach ($ViewData['select_filtro'] as $Select) {
                                        ?>
                                        <option <?= (isset($Select['disable']) ? 'disable' : null) ?>
                                                value="<?= $Select['value'] ?>"><?= $Select['name'] ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="align-self-start">
							<span id="btn_filtro" data-toggle="modal" data-target="#modal_edit"
                                  class="pln-link-color pln-font-smaller pln-cur-pointer">
								<span id="btn_filtro_text"><?= $lang['filtro']['save_filter'] ?></span>
							</span>
                        </div>
                    </div>
                    <div class="d-flex flex-column align-items-end ml-4">
                        <button type="button" onclick="pesquisar();" id="btn_pesquisa" class="btn pln-btn-orange">
                            <?= $lang['parametros']['pesquisa'] ?>
                        </button>
                        <span class="pln-link-color pln-font-smaller pln-cur-pointer align-self-start"
                              onclick="cleanForm('formulario')">
							<?= $lang['filtro']['clean_filters']; ?>
						</span>
                    </div>
                </div>
            </div>
            <hr>
            <form name="formulario" id="formulario" method="post" action="/<?= $Country ?>/sku-test/list">
                <input type="hidden" name="filter_temp" id="filter_temp"/>
                <!-- #plannexo AQUI VEM O CONTE�DO DA P�GINA -->
                <!-- <input type="hidden" name="allFilters" id="allFilters"> -->
                <div class="row">

                    <div class="col-sm-4">
                        <div class="element-box pln-sku-min-box-height-l1">
                            <fieldset class="form-group">
                                <legend>
                                    <span><?= $lang['filtro']['card_estab']['titulo'] ?></span>
                                </legend>
                                <div class="row">
                                    <div class="col-sm-12 pln-content-scroll">
                                        <?php
                                        foreach ($ViewData['Estab'] as $Estab) {
                                            ?>
                                            <div>
                                                <input type='checkbox' name="cod_estab_fk[]"
                                                       id="cod_estab_fk_<?= $Estab['code'] ?>"
                                                       value="<?= $Estab['code'] ?>">
                                                <label for='cod_estab_fk_<?= $Estab['code'] ?>'><?= $Estab['name'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="element-box pln-sku-min-box-height-l1">
                            <fieldset class="form-group">
                                <legend>
                                    <span><?= $lang['filtro']['card_status']['titulo'] ?></span>
                                </legend>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_status']['erp'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="sit_sku_erp[]" id="sit_sku_erp_1" value="1"> <label
                                                for="sit_sku_erp_1"><?= $lang['filtro']['card_status']['ativo'] ?></label>
                                        <input type="checkbox" name="sit_sku_erp[]" class="ml-2" id="sit_sku_erp_0"
                                               value="0"> <label
                                                for="sit_sku_erp_0"><?= $lang['filtro']['card_status']['inativo'] ?></label>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_status']['sistema'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="sit_sku[]" id="sit_sku_1" value="1"> <label
                                                for="sit_sku_1"><?= $lang['filtro']['card_status']['ativo'] ?></label>
                                        <input type="checkbox" name="sit_sku[]" class="ml-2" id="sit_sku_0" value="0">
                                        <label for="sit_sku_0"><?= $lang['filtro']['card_status']['inativo'] ?></label>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_status']['analisada'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="sit_analysed[]" id="sit_analysed_1" value="1">
                                        <label for=""><?= $lang['filtro']['card_status']['sim'] ?></label>
                                        <input type="checkbox" name="sit_analysed[]" id="sit_analysed_0" class="ml-2"
                                               id="" value="0"> <label
                                                for=""><?= $lang['filtro']['card_status']['nao'] ?></label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="element-box pln-sku-min-box-height-l1">
                            <fieldset class="form-group">
                                <legend>
                                    <span><?= $lang['filtro']['card_curvas']['titulo'] ?></span>
                                </legend>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_curvas']['abc'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="curve_abc[]" id="curve_abc_A" value="A"> <label
                                                for="curve_abc_A">A</label>
                                        <input type="checkbox" name="curve_abc[]" id="curve_abc_B" value="B"
                                               class="ml-2"> <label for="curve_abc_B">B</label>
                                        <input type="checkbox" name="curve_abc[]" id="curve_abc_C" value="C"
                                               class="ml-2"> <label for="curve_abc_C">C</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_curvas']['xyz'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="curve_xyz[]" id="curve_xyz_X" value="X"> <label
                                                for="curve_xyz_X">X</label>
                                        <input type="checkbox" name="curve_xyz[]" id="curve_xyz_Y" value="Y"
                                               class="ml-2"> <label for="curve_xyz_Y">Y</label>
                                        <input type="checkbox" name="curve_xyz[]" id="curve_xyz_Z" value="Z"
                                               class="ml-2"> <label for="curve_xyz_Z">Z</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_curvas']['pqr'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="curve_pqr[]" id="curve_pqr_P" value="P"> <label
                                                for="curve_pqr_P">P</label>
                                        <input type="checkbox" name="curve_pqr[]" id="curve_pqr_Q" value="Q"
                                               class="ml-2"> <label for="curve_pqr_Q">Q</label>
                                        <input type="checkbox" name="curve_pqr[]" id="curve_pqr_R" value="R"
                                               class="ml-2"> <label for="curve_pqr_R">R</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_curvas']['123'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="curve_123[]" id="curve_123_1" value="1"> <label
                                                for="curve_123_1">1</label>
                                        <input type="checkbox" name="curve_123[]" id="curve_123_2" value="2"
                                               class="ml-2"> <label for="curve_123_2">2</label>
                                        <input type="checkbox" name="curve_123[]" id="curve_123_3" value="3"
                                               class="ml-2"> <label for="curve_123_3">3</label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                </div>
                <!-- Linha-->
                <div class="row">
                    <div class="col-sm-4">
                        <div class="element-box min-box-height-l2">
                            <fieldset class="form-group">
                                <legend>
                                    <span><?= $lang['filtro']['card_sku']['item'] ?></span>
                                </legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for='cod_item'><?= $lang['filtro']['card_sku']['codigo'] ?></label>
                                            <input type='text' class='form-control' name='cod_item_pk' id='cod_item_pk'
                                                   value=''>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for='cod_item'><?= $lang['filtro']['card_sku']['desc'] ?></label>
                                            <input type='text' class='form-control' name='desc_item' id='desc_item'
                                                   value=''>
                                        </div>
                                    </div>
                            </fieldset>
                        </div>
                    </div>

                    <!-- card -->
                    <div class="col-sm-8">
                        <div class="element-box min-box-height-l2">
                            <div class="row d-flex flex-row">
                                <div class="col-sm-6">
                                    <fieldset class="form-group">
                                        <legend>
                                            <span onclick="hide_filter('ipt_find_exibicao')" class="pln-cur-pointer">
                                                <?= $lang['filtro']['card_exibicao']['titulo'] ?>
                                                <i class="fa fa-search pln-link-color"></i>
                                            </span>
                                        </legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" placeholder="filtrar exibição "
                                                       class="form-control pln-invi mb-1" id="ipt_find_exibicao"
                                                       onkeyup="search_filter('ipt_find_exibicao', 'div_exibition')">
                                            </div>
                                            <div class="col-sm-4 d-flex align-items-center mt-1">
                                                <input type="checkbox" id="chk_check_all_exibicao" class="chk_check_all"
                                                       name="chk_check_all_exibicao">
                                                <label class="pl-1 pln-font-smaller"
                                                       for="chk_check_all_exibicao"><?= $lang['parametros']['todos'] ?></label>
                                            </div>
                                            <div class="col-sm-4 d-flex align-items-center justify-content-center mt-1">
                                                <i data-toggle="tooltip" title="Mover para cima"
                                                   class="fa fa-arrow-up pln-order-blue pln-cur-pointer i_multiorder_exibition"></i>
                                                <i data-toggle="tooltip" title="Mover para baixo"
                                                   class="fa fa-arrow-down pln-order-blue pln-cur-pointer i_multiorder_exibition ml-3"></i>
                                            </div>
                                            <div class="col-sm-4 d-flex align-items-center justify-content-end">
                                                <span class="pr-2 pln-font-smaller"><?= $lang['filtro']['card_ordenacao']['ordenar_todos'] ?></span>
                                                <i id="sendAllToOrder"
                                                   class="fa fa-arrow-right pln-order-blue pln-cur-pointer pr-2"></i>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="div_exibicao"
                                                 class="col-sm-12 d-flex flex-column mt-3 pln-content-scroll">
                                                <div id="div_exibition">
                                                    <? foreach ($ViewData['Cols'] as $Col): ?>
                                                        <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                            <input type="checkbox" name='chkvetor[]'
                                                                   id='chkvetor_<?= $Col ?>'
                                                                   value='<?= $Col ?>' <?= isset($ViewData['chkvetor_chk'][$Col]) ?>>
                                                            <label class='text-truncate pl-1'> <?= $lang['vw_scr_scm405g_sku_list'][strtolower($Col)] ?></label>
                                                            <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                        </div>
                                                    <? endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-sm-6">
                                    <fieldset class="form-group">
                                        <legend>
                                            <span>
                                                <?= $lang['filtro']['card_ordenacao']['titulo'] ?>
                                                <i class="fa fa-info-circle pln-order-blue pln-cur-pointer"
                                                   data-container="body" data-toggle="popover"
                                                   data-trigger="hover" data-placement="top" data-html="true"
                                                   data-content="<?= $lang['filtro']['card_ordenacao']['helper'] ?>"></i>
                                            </span>
                                        </legend>
                                        <div class="row">
                                            <div class="col-sm-12 d-flex align-items-center justify-content-between div_order_top mt-1">
                                                <span id="cleanOrder"
                                                      class="hideOrder pln-cur-pointer pln-font-smaller"><i
                                                            id="sendAllToOrder"
                                                            class="fa fa-times pln-danger pln-cur-pointer"></i> <?= $lang['filtro']['card_ordenacao']['remover_todos'] ?></span>
                                                <span class="hideOrder">
															<i data-toggle="tooltip" title="Mover para cima"
                                                               class="fa fa-arrow-up pln-order-blue pln-cur-pointer i_multiorder_order"></i>
															<i data-toggle="tooltip" title="Mover para baixo"
                                                               class="fa fa-arrow-down pln-order-blue pln-cur-pointer i_multiorder_order ml-3"></i>
														</span>
                                                <span class="hideOrder pln-font-smaller"><?= $lang['filtro']['sort_data'] ?> <img
                                                            title="Ordenar"
                                                            src="../../../resources/img/sort.png"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="div_order" class="col-sm-12 mt-3 pln-content-scroll">

                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- LINHA -->
                <div class="row">
                    <!-- card -->
                    <div class="col-sm-12">
                        <div class="element-box min-box-height-l2">
                            <fieldset class="form-group">
                                <legend>
                                    <span><?= $lang['filtro']['card_filtros_especiais']['titulo'] ?></span>
                                </legend>
                                <div class="row ">
                                    <div class="col-sm-5">
                                        <input class="" name="qty_excess_next_in" id="qty_excess_next_in"
                                               type="checkbox">
                                        <label for="qty_excess_next_in"><?= $lang['filtro']['card_filtros_especiais']['exc_prox_entrada'] ?></label>
                                    </div>
                                    <div class="col-sm-5 offset-sm-1">
                                        <input class="" id="qty_inv_origin" name="qty_inv_origin" type="checkbox">
                                        <label for="qty_inv_origin"><?= $lang['sku_form']['card_filtros_especiais']['saldo'] ?></label>
                                    </div>
                                    <div class="col-sm-12 p-0">
                                        <div class="col-sm-12 d-flex flex-row align-items-center">
                                            <div class="col-sm-5 d-flex flex-row pl-0">
                                                <div>
                                                    <label for=""><?= $lang['filtro']['card_filtros_especiais']['ordem'] ?></label>
                                                </div>
                                                <div class="ml-3">
                                                    <input name="purchase_order[]" id="purchase_order_1" value="1"
                                                           type="checkbox">
                                                    <label for=""><?= $lang['filtro']['card_filtros_especiais']['sim'] ?></label>
                                                    <input class="ml-1" name="purchase_order[]" id="purchase_order_0"
                                                           value="0" type="checkbox">
                                                    <label for=""><?= $lang['filtro']['card_filtros_especiais']['nao'] ?></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 offset-sm-1 d-flex flex-row">
                                                <div>
                                                    <label for="purchase_req"><?= $lang['filtro']['card_filtros_especiais']['solicitacao'] ?></label>
                                                </div>
                                                <div class="ml-3">
                                                    <input id="purchase_req_1" name="purchase_req[]" value="1"
                                                           type="checkbox">
                                                    <label for=""><?= $lang['filtro']['card_filtros_especiais']['sim'] ?></label>
                                                    <input class="ml-1" id="purchase_req_0" name="purchase_req[]"
                                                           value="0" type="checkbox">
                                                    <label for=""><?= $lang['filtro']['card_filtros_especiais']['nao'] ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label for=""><?= $lang['filtro']['card_filtros_especiais']['compra'] ?></label>
                                            <input type="text" name="last_purchase_supplier" id="last_purchase_supplier"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-5 offset-sm-1">
                                        <div class="form-group">
                                            <label for=""><?= $lang['sku_form']['card_filtros_especiais']['cobertura'] ?></label>
                                            <select class="form-control" id="inv_available" name="inv_available">
                                                <option value=""></option>
                                                <option value="1"><?= $lang['sku_form']['card_filtros_especiais']['abaixo_min'] ?></option>
                                                <option value="2"><?= $lang['sku_form']['card_filtros_especiais']['abaixo_igual_min'] ?></option>
                                                <option value="3"><?= $lang['sku_form']['card_filtros_especiais']['abaixo_leadtime'] ?></option>
                                                <option value="4"><?= $lang['sku_form']['card_filtros_especiais']['abaixo_ponto_pedido'] ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-5">
                                        <label for=""><?= $lang['sku_form']['card_filtros_especiais']['previsao'] ?></label>
                                        <div class="d-flex align-items-start">
                                            <select class="form-control mr-2" name="range_qty_forecast"
                                                    id="select_forecast">
                                                <option value=""></option>
                                                <option value=">="><?= $lang['sku_form']['card_filtros_especiais']['maior_igual'] ?></option>
                                                <option value="<="><?= $lang['sku_form']['card_filtros_especiais']['menor_igual'] ?></option>
                                                <option value=">"><?= $lang['sku_form']['card_filtros_especiais']['maior_que'] ?></option>
                                                <option value="<"><?= $lang['sku_form']['card_filtros_especiais']['menor_que'] ?></option>
                                                <option value="="><?= $lang['sku_form']['card_filtros_especiais']['igual_a'] ?></option>
                                            </select>
                                            <input type="number" min="0" class="form-control w-25" name="qty_forecast">
                                        </div>
                                    </div>
                                    <div class="col-sm-5 offset-sm-1">
                                        <div class="form-group">
                                            <label for=""><?= $lang['filtro']['card_filtros_especiais']['cobertura_abaixo_dias'] ?></label>
                                            <input type="text" class="form-control" name="inv_days" id="inv_days">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-5">
                                        <label for="out_last_30d"><?= $lang['sku_form']['card_filtros_especiais']['saidas'] ?></label>
                                        <div class="d-flex align-items-start">
                                            <select class="form-control mr-2" name="range_out_last_30d"
                                                    id="range_out_last_30d">
                                                <option value=""></option>
                                                <option value=">="><?= $lang['sku_form']['card_filtros_especiais']['maior_igual'] ?></option>
                                                <option value="<="><?= $lang['sku_form']['card_filtros_especiais']['menor_igual'] ?></option>
                                                <option value=">"><?= $lang['sku_form']['card_filtros_especiais']['maior_que'] ?></option>
                                                <option value="<"><?= $lang['sku_form']['card_filtros_especiais']['menor_que'] ?></option>
                                                <option value="="><?= $lang['sku_form']['card_filtros_especiais']['igual_a'] ?></option>
                                            </select>
                                            <input type="number" min="0" class="form-control w-25" name="out_last_30d">
                                        </div>
                                    </div>
                                    <div class="col-sm-5 offset-sm-1">
                                        <label for="date_last_in"><?= $lang['sku_form']['card_filtros_especiais']['ultentrada'] ?></label>
                                        <div class="d-flex align-items-start">
                                            <select class="form-control mr-2" id="date_last_in">
                                                <option value=""><?= $lang['sku_form']['card_filtros_especiais']['marior_que'] ?></option>
                                            </select>
                                            <div class="input-group">
                                                <input type="text" name="date_last_in" id="singledate"
                                                       class="form-control">
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-danger" id="cleanDate" type="button">
                                                        Limpar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>
                </div>
                <!--LINHA -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="element-box min-box-height-l4">
                            <fieldset class="form-group">
                                <legend>
                                    <span><?= $lang['filtro']['card_parametros']['titulo'] ?></span>
                                </legend>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-12 font-weight-bold"><?= $lang['filtro']['card_parametros']['sinalizador'] ?></div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-sm-12">
                                                <input name="inv_level[]" id="inv_level_0" value='0' type="checkbox">
                                                <label for="inv_level_0"><?= $lang['filtro']['card_parametros']['zero'] ?></label>
                                                <input name="inv_level[]" id="inv_level_1" value='1' class="ml-2"
                                                       type="checkbox"> <label
                                                        for="inv_level_1"><?= $lang['filtro']['card_parametros']['min2'] ?></label>
                                                <input name="inv_level[]" id="inv_level_2" value='2' class="ml-2"
                                                       type="checkbox"> <label
                                                        for="inv_level_2"><?= $lang['filtro']['card_parametros']['min'] ?></label>
                                            </div>
                                            <div class="col-sm-12">
                                                <input name="inv_level[]" id="inv_level_3" value='3' type="checkbox">
                                                <label for="inv_level_3"><?= $lang['filtro']['card_parametros']['alvo'] ?></label>
                                                <input name="inv_level[]" id="inv_level_4" value='4' class="ml-2"
                                                       type="checkbox"> <label
                                                        for="inv_level_4"><?= $lang['filtro']['card_parametros']['max'] ?></label>
                                                <input name="inv_level[]" id="inv_level_5" value='5' class="ml-2"
                                                       type="checkbox"> <label
                                                        for="inv_level_5"><?= $lang['filtro']['card_parametros']['max2'] ?></label>
                                            </div>
                                        </div>

                                        <div class="row mt-3">
                                            <div class="col-sm-12 font-weight-bold"><?= $lang['filtro']['card_parametros']['ciclo'] ?></div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-sm-12">
                                                <input name="sku_life_cycle[]" id="sku_life_cycle_1" value="1"
                                                       type="checkbox"> <label
                                                        for=""><?= $lang['filtro']['card_parametros']['nova'] ?></label>
                                                <input name="sku_life_cycle[]" id="sku_life_cycle_2" value="2"
                                                       class="ml-2" type="checkbox"> <label
                                                        for=""><?= $lang['filtro']['card_parametros']['madura'] ?></label>
                                                <input name="sku_life_cycle[]" id="sku_life_cycle_3" value="3"
                                                       class="ml-2" type="checkbox"> <label
                                                        for=""><?= $lang['filtro']['card_parametros']['desc'] ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-12 font-weight-bold"><?= $lang['filtro']['card_parametros']['politica'] ?></div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-sm-12 pln-content-scroll">
                                                <?
                                                $iid_profile_chk = array();
                                                if (isset($ViewData['chkvetor']['id_profile'])) {
                                                    if (is_array($ViewData['chkvetor']['id_profile'])) {
                                                        foreach ($ViewData['chkvetor']['id_profile'] as $chave => $valor) {
                                                            $valor = (int)$valor;
                                                            $iid_profile_chk[$valor] = "checked";
                                                        }

                                                    }
                                                }

                                                foreach ($ViewData['Profile'] as $row) {
                                                    if (array_key_exists($row['ID_PROFILE_PK'], $iid_profile_chk))
                                                        $chk_state = $iid_profile_chk[$row['ID_PROFILE_PK']];
                                                    else
                                                        $chk_state = "";

                                                    echo "
                                                        <div class='d-flex flex-row text-nowrap align-items-center'>
															<input type='checkbox' name='id_profile[]' id='id_profile_{$row['ID_PROFILE_PK']}' value='{$row['ID_PROFILE_PK']}' {$chk_state} >
															<label class='text-truncate pl-1' for='id_profile_{$row['ID_PROFILE_PK']}'>{$row['NOME']}</label>
                                                        </div>
                                                    ";
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="element-box min-box-height-l5">
                            <fieldset class="form-group">
                                <legend>
                                    <span onclick="hide_filter('ipt_find_gestor')" class="pln-cur-pointer">
                                        <?= $lang['filtro']['card_gestor']['titulo'] ?>
                                        <i class="fa fa-search pln-link-color"></i>
                                    </span>
                                </legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" placeholder="filtrar gestor"
                                               class="form-control pln-invi mb-1" id="ipt_find_gestor"
                                               onkeyup="search_filter('ipt_find_gestor', 'div_gestor')">
                                    </div>
                                    <div class="col-sm-12 pln-content-scroll" id="div_gestor">
                                        <?php
                                        foreach ($ViewData['User'] as $Key => $User) {
                                            ?>
                                            <div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name='user_id[]'
                                                       id='user_id_<?= $User['ID_USER_PK'] ?>'
                                                       value='<?= $User['ID_USER_PK'] ?>'>
                                                <label class="text-truncate pl-1"
                                                       for='user_id_<?= $User['ID_USER_PK'] ?>'><?= $User['LOGIN'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- DEPOSITO PADRAO -->
						<div class="col-sm-4">
							<div class="element-box pln-sugestao-min-box-height-l1">
								<fieldset class="form-group">
									<legend><span onclick="hide_filter('ipt_find_deposito_padrao')" class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['deposito_padrao'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" placeholder="filtrar classe" class="form-control pln-invi mb-1" id="ipt_find_deposito_padrao" onkeyup="search_filter('ipt_find_deposito_padrao', 'div_filter_deposito_padrao')">
										</div>
										<div class="col-sm-12">
											<input type="checkbox" class="chk_check_all" id="chk_check_all_deposito_padrao"> <label for="chk_check_all_deposito_padrao"><?= $lang['parametros']['todos'] ?></label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12 pln-content-scroll" id="div_filter_deposito_padrao">
										<?php
										foreach ($ViewData['Deposit'] as $Key => $Deposit) {
										?>
											<div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
											<input type='checkbox' name="default_local[]" id="default_local_filtro_<?= $Deposit['COD_LOCAL_PK'] ?>" value="<?= $Deposit['COD_LOCAL_PK'] ?>">
											<label class="text-truncate pl-1" for='default_local_filtro_<?= $Deposit['COD_LOCAL_PK'] ?>'><?= $Deposit['DESCRICAO'] ?></label>
											</div>
										<?php
										}
										?>
										</div>
									</div>
								</fieldset>
							</div>
						<!-- FIM DEPOSITO PADRAO -->
						</div>
                    <!-- FILTROS DINAMICOS -->
                    <?php foreach ($ViewData['Groups'] as $key => $Group) { ?>
                        <div class="col-sm-4">
                            <div class="element-box min-box-height-l5">
                                <fieldset class="form-group">
                                    <legend>
                                        <span onclick="hide_filter('ipt_find_<?php echo $key ?>')"
                                              class="pln-cur-pointer"><?php echo $Group ?>
                                            <i class="fa fa-search pln-link-color"></i>
                                        </span>
                                    </legend>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" placeholder="filtrar <?php echo $Group ?>"
                                                   class="form-control pln-invi mb-1"
                                                   id="ipt_find_<?php echo $key ?>"
                                                   onkeyup="search_filter('ipt_find_<?php echo $key ?>', 'div_filter_<?php echo $key ?>')">
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="checkbox" class="chk_check_all"
                                                   id="chk_check_all_<?php echo $key ?>"> <label
                                                    for="chk_check_all_<?php echo $key ?>"><?= $lang['parametros']['todos'] ?></label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 pln-content-scroll"
                                             id="div_filter_<?php echo $key ?>">
                                            <?php
                                            foreach ($ViewData['Group_' . $key] as $Key2 => $Campo) {
                                                ?>
                                                <div class="d-flex <?= ($Key2 == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                    <input type='checkbox'
                                                           name='cod_group<?php echo $key ?>_fk[]'
                                                           id='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>'
                                                           value='<?= $Campo['COD_GROUP_PK'] ?>'>
                                                    <label class='text-truncate pl-1'
                                                           for='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>'><?= $Campo['DESC_GROUP_CONT'] ?></label>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    <?php } ?>
                    <!--END FILTROS DINAMICOS -->
                </div>

                <!--END - Recent Ticket Comments-->

                <input type="hidden" value="" name='ordena' id='ordena' value='ordena'>
                <input type="hidden" value="" name='orderby' id='orderby' value=''>

            </form>
        </div>
    </div>
</div>
<p id="demo"></p>
<div class="display-type"></div>

<div class="modal fade" id="modal_edit" data-keyboard="true" tabindex="-1" role="dialog" aria-labelledby="botao_editar"
     aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_titulo" name="modal_titulo">Novo Filtro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">

                <div style="width: 100%">
                    <input placeholder="Digite nome do novo filtro" maxlength="50" type="text" class="form-control"
                           name="filter_nome" id="filter_nome">
                </div>
                <div style="width: 100%; margin-top: 5px">
                    <!--								<input type="radio" name="filter_perfil" id="filter_perfil_Publico" value="Publico" checked>-->
                    <!--								<label for="filter_perfil1">Público</label>-->
                    <!--								<input type="radio" name="filter_perfil" id="filter_perfil_Privado" value="Privado">-->
                    <!--								<label for="filter_perfil2">Privado</label>-->
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <input type="hidden" name="filter_tela_id" id="filter_tela_id" value="SCM405G">
                <span class="pln-danger pln-cur-pointer" id="confirm_delete" onclick="confirmDeleteFilter()"><i
                            class="fa fa-times pln-danger"></i> <span><?= $lang['filtro']['delete'] ?></span></span>


                <button type="button" class="btn pln-btn-orange" onclick="saveFilter('SCM405G')" id="btnfiltro_salvar">
                    <span id="spn_save"><?= $lang['filtro']['save'] ?></span><i
                            class="fa fa-spin fa-spinner pln-invi"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="clear"></div>

<?php
$JS = ['uglify/all-sku-test-filters-min'];
?>