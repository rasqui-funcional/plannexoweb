<?php
if (!$ViewData['data_table']) {
    echo 'false';
    return;
}
?>

<script> orderCoverClass.checkHasDateArrival = '<?php echo $ViewData['has_date']; ?>'; </script>
<form id="formulario">
    <div id="controlSize" class="table-responsive">
        <table id="tableOrderCover" class="table dataTable ordercover-table bigger-table">
            <thead>
                <tr>
                    <th>
                        <div class="d-flex flex-column align-items-center">
                            <span class="pt-1"><input id="chk_all" type="checkbox"/></span>
                        </div>
                    </th>
                    <?php
                    foreach ($ViewData['header_cols_fixed'] as $col_id => $col_name) {
                        if ($col_id == 'DATE_ARRIVAL_ALTERED' && !$ViewData['has_date']) {
                            $col_id = 'auxiliar19';
                        }

                        $text = HtmlHelper::getTextAndOrderTable($col_id, $col_name, $lang);

                        if ($col_id != "") {
                            ?>
                            <th>
                                <div class="d-flex flex-row order-col <?php echo $text->order; ?>" data-id="<?php echo $col_id ?>">
                                    <span>
                                        <?php echo $text->abrev ?>
                                    </span>
                                    <?php if ($text->order) : ?>
                                        <i class="fa pln-link-color ml-1 align-self-center fa-sort<?= isset($ViewData['order'][$col_id]) ? "-" . $ViewData['order'][$col_id] : "" ?>"></i>
                                    <?php endif; ?>
                                </div>
                            </th>
                            <?php
                        }
                    }
                    ?>
                </tr>
            </thead>

            <tfoot>
                <tr>
                    <th>
                        <input type="hidden" id="hdn_total_items" value="<?= $ViewData['footer']['count'] ?>"/>
                        <?php echo $lang['capa_pedido_lista']['total'] ?>
                    </th>
                    <?php
                    foreach ($ViewData['header_cols_fixed'] as $col_id => $col_name) {
                        if ($col_id != "") {
                            echo sprintf(
                                "<th>%s</th>",
                                array_key_exists('total', $col_name)
                                ? $ViewData['footer']['data'][$col_name['total']]
                                : ""
                            );
                        }
                    }
                    ?>
                </tr>

                <input type="hidden" id="total_page_number" value="<?php echo $ViewData['footer']['pages'] ?>">
            </tfoot>

            <tbody id="tableOrderCoverBody">
            <?php foreach ($ViewData['data_table'] as $key => $Data) : ?>
                <tr class="object_table_tr">
                    <td class="text-center">
                        <input data-psgug="<?php echo $Data['psug_pk'] ?>" data-default-local="<?php echo $Data['default_local'] ?>" class="input_chk" name="cod_estab_fk" value="<?php echo $Data['estab_cod'] ?>" type="checkbox">
                    </td>

                    <?php
                    if (count($ViewData['header_cols']) > 0) {
                        foreach ($ViewData['header_cols'] as $col_id => $col_name) {
                            if ($col_id != "") {
                                echo sprintf(
                                    "<td data-column='%s' data-value='%s'>%s</td>",
                                    $col_id,
                                    $Data[$col_id]['value'],
                                    $Data[$col_id]['name']
                                );
                            }
                        }
                    }
                    ?>

                    <td>
                        <span class="estab" data-estab-id="<?php echo $Data['estab_cod'] ?>">
                            <?php echo "{$Data['estab_cod']} - {$Data['estab_name']}" ?>
                        </span>
                    </td>
                    <td>
                        <span class="pln-order-blue suggestion_ordercover pln-cur-pointer">
                            <?php
                                echo sprintf(
                                    "%s %s",
                                    $Data['suggestions_qty'],
                                    ngettext($lang['capa_pedido_lista']['sugestao'], $lang['capa_pedido_lista']['sugestoes'], (int) $Data['suggestions_qty'])
                                );
                            ?>
                        </span>
                    </td>

                    <td class="pln-min-w-100">
                        <select name="cod_buyer" class="form-control form-control-sm select_cod_comprador">                                                        
                        <?php
                            echo ViewOrderCover::mountOptions(count($Data['opts']['cod_buyer']), $Data['opts']['cod_buyer'], $Data['cod_buyer']);                            
                        ?>
                        </select>
                    </td>
                    <td class="pln-min-w-100">
                        <select name="cod_sector" class="form-control form-control-sm select_cod_setor">
                        <?php
                            echo ViewOrderCover::mountOptions(count($Data['opts']['cod_sector']), $Data['opts']['cod_sector'], $Data['cod_sector']);
                        ?>
                        </select>
                    </td>
                    <td class="pln-min-w-100">
                        <select name="cod_local_fk" class="form-control form-control-sm select_deposito">
                        <?php
                            echo ViewOrderCover::mountOptionsDeposit(count($Data['deposit']), $Data['deposit'], $Data['default_local']);
                        ?>
                        </select>
                    </td>
                    <td class="pln-min-w-100">
                        <select name="cod_reason" class="form-control form-control-sm select_motivo">
                            <?php
                                echo ViewOrderCover::mountOptions(count($Data['opts']['cod_reason']), $Data['opts']['cod_reason'], $Data['cod_reason']);
                            ?>
                        </select>
                    </td>

                    <td><?php echo number_format($Data['total_qty'], 0, '', '.') ?></td>
                    <td class="text-nowrap">
                        <span>
                            <?=$lang['global']['country_currency']?> <?php echo $Data['total_value'] ?>
                        </span>
                    </td>

                    <td>
                        <input type="date" min="<?php echo date('Y-m-d') ?>" name="dt_exp_req" value="<?php echo $Data['dt_exp_req'] ?>" class="form-control dt_exp_req_input form-control-sm dt_exp_req singledate" onkeydown="return false">
                        <input type="hidden" class="dt_exp_req_label no-validate" value="<?php echo(!$ViewData['has_date'] ? '-' : $Data['dt_exp_req']) ?>">
                    </td>

                    <td>
                        <input name="yn_urgent" class="sit_urgent" <?php echo $Data['sit_urgent'] == "1" ? 'checked' : '' ?> type="checkbox"><?= $lang['capa_pedido_table']['yes'] ?>
                    </td>

                    <td class="pln-min-w-200">
                        <textarea name="obs_req" maxlength="200"class="form-control form-control-sm txtarea_obs no-validate"></textarea>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</form>

<script>
    orderCoverClass.initialize();
    paginationClass.initialize();
</script>
