<!--<script>
	var filter_temp = ;
	delete filter_temp.filter_temp;
</script>
-->

<div class="content-i pln-content">
	<div class="content-box">
		<!--START - Recent Ticket Comments-->
		<div class="element-wrapper">
			<div class="d-flex flex-row justify-content-between mb-2 align-items-center">
				<div>
					<h4 class="pln-font-color"><span data-toggle="tooltip" title="<?= $lang['sugcompras']['view'] ?>"><?php echo $lang['capa_pedido_filtros']['titulo'] ?></span></h4>
				</div>
				<div class="d-flex flex-row">
					<span class="pln-bold-600 mt-1 mr-1"><?= $lang['filtro']['my_filters'] ?>:</span>
					<div class="d-flex flex-column align-items-end">
						<div>
							<select id="select_filtro" name="select_filtro" class="form-control-sm pln-select-filter-size">
								<option value=""></option>
								<?php
								if ($ViewData['select_filtro']) {
									foreach ($ViewData['select_filtro'] as $Select) {
										?>
										<option <?= (isset($Select['disable']) ? 'disable' : null) ?> value="<?= $Select['value'] ?>"><?= $Select['name'] ?></option>
										<?php

									}
								}
								?>
							</select>
						</div>							
						<div class="align-self-start">
							<span id="btn_filtro"  data-toggle="modal" data-target="#modal_edit" class="pln-link-color pln-font-smaller pln-cur-pointer">
								<span id="btn_filtro_text"><?=$lang['filtro']['save_filter']?></span>
							</span>
						</div>
					</div>
					<div class="d-flex flex-column align-items-end ml-4">
						<button type="button" onclick="pesquisar();" id="btn_pesquisa" class="btn pln-btn-orange">
							<?= $lang['parametros']['pesquisa'] ?>
						</button>
						
						<span class="pln-link-color pln-font-smaller pln-cur-pointer align-self-start" onclick="cleanForm('formulario')">
							<?= $lang['filtro']['clean_filters']?>
						</span>
					</div>
				</div>									
			</div>
			<hr>
			<form name="formulario" id="formulario" method="post" action="/<?= $Country ?>/ordercover/list">				
				<!-- #plannexo AQUI VEM O CONTEÚDO DA PÁGINA -->
				<input type="hidden" name="filter_temp" id="filter_temp" />
				<div class="row">
					<!-- AGRUPAR POR -->
					<div class="col-sm-8">
						<div class="element-box pln-capapedido-min-box-height-l1">
							<fieldset class="form-group">								
								<legend><span onclick="hide_filter('ipt_find_agrupar')" class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['agrupar'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar agrupamento" class="form-control pln-invi mb-1" id="ipt_find_agrupar" onkeyup="search_filter('ipt_find_agrupar', 'div_filter_agrupar')">
									</div>									
								</div>

								<div class="row">
									<div class="col-sm-12 pln-content-scroll" id="div_filter_agrupar">
										<div class="d-flex mt-2 flex-row text-nowrap align-items-center">
											<input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_cod_estab_fk" value="cod_estab_fk">
											<label class="text-truncate pl-1" for='cod_group_fk_cod_estab_fk'><?= $lang['capa_pedido_filtros']['estab'] ?></label>
										</div>																				
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_id_profile" value="id_profile">
											<label class="text-truncate pl-1" for='cod_group_fk_id_profile'><?= $lang['capa_pedido_filtros']['politica'] ?></label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_date_arrival_altered" value="date_arrival_altered">
											<label class="text-truncate pl-1" for='cod_group_fk_date_arrival_altered'><?= $lang['capa_pedido_filtros']['data_entrega'] ?></label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_sit_urgent" value="sit_urgent">
											<label class="text-truncate pl-1" for='cod_group_fk_sit_urgent'><?= $lang['capa_pedido_filtros']['sit_urgente'] ?></label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_user_id" value="user_id">
											<label class="text-truncate pl-1" for='cod_group_fk_user_id'><?= $lang['capa_pedido_filtros']['gestor'] ?></label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_id_calendar_fk" value="id_calendar_fk">
											<label class="text-truncate pl-1" for='cod_group_fk_id_calendar_fk'><?= $lang['capa_pedido_filtros']['calendario'] ?></label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_cod_local_fk" value="cod_local_fk">
											<label class="text-truncate pl-1" for='cod_group_fk_cod_local_fk'><?= $lang['capa_pedido_filtros']['deposito'] ?></label>
										</div>										
										<?php foreach ($ViewData['SearchFields'] as $key => $value): ?>
											<div class="d-flex flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name="cod_group_fk[]"
                                                       id="cod_group_fk_cod_group<?= $key ?>_fk"
                                                       value="cod_group<?= $key ?>_fk">
                                                <label class="text-truncate pl-1" for='cod_group_fk_cod_group<?= $key ?>_fk'>
                                                    <?= $value ?>
                                                </label>
											</div>
										<?php endforeach; ?>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<!-- FIM AGRUPAR POR -->
					
					<!-- FIM URGENTE -->
					<div class="col-sm-4">
						<div class="element-box pln-capapedido-min-box-height-l1">
							<fieldset class="form-group">								
								<legend><span onclick="hide_filter('ipt_find_estabelecimento')" class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['sit_urgente'] ?></span></legend>
								<div class="row">
									<div class="col-sm-12 d-flex">										
										<div class="ml-1"><input name="sit_urgent_filtro[]" id="sit_urgent_filtro_1" value="1" type="checkbox"> <label for="sit_urgent_filtro_1"><?=$lang['global']['yes']?></label></div>
										<div class="ml-3"><input name="sit_urgent_filtro[]" id="sit_urgent_filtro_0" value="0" type="checkbox"> <label for="sit_urgent_filtro_0"><?=$lang['global']['no']?></label></div>
									</div>	
								</div>
							</fieldset>
						</div>
					</div>
					<!-- FIM URGENTE -->
				</div>

				<div class="row">
					<!-- ESTABELECIMENTO -->
					<div class="col-sm-4">
						<div class="element-box pln-capapedido-min-box-height-l1">
							<fieldset class="form-group">
								
								<legend><span onclick="hide_filter('ipt_find_estabelecimento')" class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['estab'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar estabelecimentos" class="form-control pln-invi mb-1" id="ipt_find_estabelecimento" onkeyup="search_filter('ipt_find_estabelecimento', 'div_filter_estabelecimento')">
									</div>
									<div class="col-sm-12">											
										<input type="checkbox" class="chk_check_all" id="chk_check_all_estab"> <label for="chk_check_all_estab"><?= $lang['parametros']['todos'] ?></label> 
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 pln-content-scroll" id="div_filter_estabelecimento">
									<?php foreach ($ViewData['Estab'] as $Key => $Estab): ?>
                                            <div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name="cod_estab_fk[]" id="cod_estab_fk_<?= $Estab['code'] ?>" value="<?= $Estab['code'] ?>">
                                                <label class="text-truncate pl-1" for='cod_estab_fk_<?= $Estab['code'] ?>'><?= $Estab['name'] ?></label>
                                            </div>
									<?php endforeach; ?>
									</div>
								</div>
								
							</fieldset>
						</div>
					</div>
					<!-- FIM ESTABELECIMENTO -->
					
					<!-- DEPOSITO PADRAO -->
					<div class="col-sm-4">
						<div class="element-box pln-capapedido-min-box-height-l1">                               
							<fieldset class="form-group">								
								<legend><span onclick="hide_filter('ipt_find_deposito_padrao')" class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['deposito_padrao'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar classe" class="form-control pln-invi mb-1" id="ipt_find_deposito_padrao" onkeyup="search_filter('ipt_find_deposito_padrao', 'div_filter_deposito_padrao')">
									</div>
									<div class="col-sm-12">											
										<input type="checkbox" class="chk_check_all" id="chk_check_all_deposito_padrao"> <label for="chk_check_all_deposito_padrao"><?= $lang['parametros']['todos'] ?></label> 
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 pln-content-scroll" id="div_filter_deposito_padrao">
									    <?php foreach ($ViewData['Deposit'] as $Key => $Deposit): ?>
									    	<div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name="default_local_filtro[]" id="default_local_filtro_<?= $Deposit['COD_LOCAL_PK'] ?>" value="<?= $Deposit['COD_LOCAL_PK'] ?>">
                                                <label class="text-truncate pl-1" for='default_local_filtro_<?= $Deposit['COD_LOCAL_PK'] ?>'><?= $Deposit['DESCRICAO'] ?></label>
									    	</div>
									    <?php endforeach; ?>
									</div>
								</div>
								                            
							</fieldset>
						</div>
					</div>
					<!-- FIM DEPOSITO PADRAO -->					
					<!-- GESTOR -->
					<div class="col-sm-4">
						<div class="element-box min-box-height-l5">
							<fieldset class="form-group">								
								<legend><span onclick="hide_filter('ipt_find_gestor')" class="pln-cur-pointer"><?php echo $lang['filtro']['card_gestor']['titulo'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar gestor" class="form-control pln-invi mb-1" id="ipt_find_gestor" onkeyup="search_filter('ipt_find_gestor', 'div_filter_gestor')">
									</div>
									<div class="col-sm-12">											
										<input type="checkbox" class="chk_check_all" id="chk_check_all_gestor"> <label for="chk_check_all_gestor"><?= $lang['parametros']['todos'] ?></label> 
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 pln-content-scroll" id="div_filter_gestor">
									<?php
									foreach ($ViewData['User'] as $Key => $User) {
										?>
										<div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
										<input type='checkbox' name='user_id_filtro[]' id='user_id_filtro_<?= $User['ID_USER_PK'] ?>' value='<?= $User['ID_USER_PK'] ?>'>
										<label class="text-truncate pl-1" for='user_id_filtro_<?= $User['ID_USER_PK'] ?>'><?= $User['LOGIN'] ?></label>
										</div>
										<?php
									}
									?>
									</div>
								</div>
							</fieldset>
						</div>	
					</div>
					<!-- FIM GESTOR -->
				</div>
				
				<div class="row">
					<!-- CALENDARIO -->
					<div class="col-sm-4">
						<div class="element-box min-box-height-l5">                            
							<fieldset class="form-group">
								<legend><span onclick="hide_filter('ipt_find_calendario')" class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['calendario'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar calendário" class="form-control pln-invi mb-1" id="ipt_find_calendario" onkeyup="search_filter('ipt_find_calendario', 'div_filter_calendario')">
									</div>
									<div class="col-sm-12">											
										<input type="checkbox" class="chk_check_all" id="chk_check_all_calendario"> <label for="chk_check_all_calendario"><?= $lang['parametros']['todos'] ?></label> 
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 pln-content-scroll" id="div_filter_calendario">
										<?php

									/**
									 * Classe
									 */
									foreach ($ViewData["calendars"] as $Key => $Campo) {
										?>
										<div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
										<input type="checkbox" name="id_calendar_fk_filtro[]" id="id_calendar_fk_filtro_<?= $Campo['ID_CALENDAR_PK'] ?>" value="<?= $Campo['ID_CALENDAR_PK'] ?>">
										<label class="text-truncate pl-1" for="id_calendar_fk_filtro_<?= $Campo["ID_CALENDAR_PK"] ?>"><?= $Campo["DESC_CALENDAR"] ?></label>
										</div>
										<?php
									}
									?>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<!-- FIM CALENDARIO -->

					<!-- POLITICA -->
					<div class="col-sm-4">
						<div class="element-box min-box-height-l5">                            
							<fieldset class="form-group">
								<legend><span onclick="hide_filter('ipt_find_contrato')" class="pln-cur-pointer"><?php echo $lang['filtro']['card_parametros']['politica'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar política" class="form-control pln-invi mb-1" id="ipt_find_contrato" onkeyup="search_filter('ipt_find_contrato', 'div_filter_politica')">
									</div>
									<div class="col-sm-12">
										<input type="checkbox" class="chk_check_all" id="chk_check_all_politica"> <label for="chk_check_all_politica"><?= $lang['parametros']['todos'] ?></label> 
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 pln-content-scroll" id="div_filter_politica">
										<?php
										foreach($ViewData['Profile'] as $Profile){
											?>
											<div class='d-flex flex-row text-nowrap align-items-center'>
											<input type='checkbox' name='id_profile_filtro[]' id='id_profile_filtro_<?= $Profile['ID_PROFILE_PK']?>' value='<?= $Profile['ID_PROFILE_PK']?>'>
											<label class='text-truncate pl-1' for='id_profile_filtro_<?= $Profile['ID_PROFILE_PK']?>'><?= $Profile['NOME'] ?></label>
											</div>
											<?php
										}
										?>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<!-- FIM POLITICA -->

					<!-- FILTROS DINAMICOS -->
					<?php foreach($ViewData['Groups'] as $key => $Group): ?>
						<div class="col-sm-4">
							<div class="element-box min-box-height-l5">
								<fieldset class="form-group">
									<legend><span onclick="hide_filter('ipt_find_<?php echo $key ?>')" class="pln-cur-pointer"><?php echo $Group  ?> <i class="fa fa-search pln-link-color"></i></span></legend>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" placeholder="filtrar <?php echo $Group ?>" class="form-control pln-invi mb-1" id="ipt_find_<?php echo $key ?>" onkeyup="search_filter('ipt_find_<?php echo $key ?>', 'div_filter_<?php echo $key ?>')">
										</div>
										<div class="col-sm-12">
											<input type="checkbox" class="chk_check_all" id="chk_check_all_<?php echo $key?>"> <label for="chk_check_all_<?php echo $key?>"><?= $lang['parametros']['todos'] ?></label>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12 pln-content-scroll" id="div_filter_<?php echo $key ?>">
                                            <?php foreach ($ViewData['Group_'.$key] as $Key2 => $Campo): ?>
                                                <div class="d-flex <?= ($Key2 == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                    <input type='checkbox' name='cod_group<?php echo $key ?>_fk_filtro[]' id='cod_group<?php echo $key ?>_fk_filtro_<?= $Campo['COD_GROUP_PK'] ?>' value='<?= $Campo['COD_GROUP_PK'] ?>'>
                                                    <label class='text-truncate pl-1' for='cod_group<?php echo $key ?>_fk_filtro_<?= $Campo['COD_GROUP_PK'] ?>'><?= $Campo['DESC_GROUP_CONT'] ?></label>
                                                </div>
                                            <?php endforeach; ?>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					<?php endforeach; ?>
					<!--END FILTROS DINAMICOS -->
				</div>								
				<div class="row">										
					
				</div>
				<!--END - Recent Ticket Comments-->
			</form>
		</div>
	</div>
</div>
<p id="demo"></p>
<div class="display-type"></div>

	<div class="modal fade" id="modal_edit" data-keyboard="true" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="botao_editar" aria-hidden="true">
		<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_titulo" name="modal_titulo"><?= $lang['filtro']['new_filter'] ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" id>
						<span aria-hidden="true">x</span>
					</button>
				</div>
				<div class="modal-body">

				<div style="width: 100%">
					<input placeholder="Digite nome do novo filtro" maxlength="50" type="text" class="form-control" name="filter_nome" id="filter_nome" >
				</div>
				<div style="width: 100%; margin-top: 5px">
					<input name="filter_perfil" id="filter_perfil" type="hidden" value="Publico">
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-between">					
				<span class="pln-danger pln-cur-pointer" id="confirm_delete" onclick="confirmDeleteFilter()" ><i class="fa fa-times pln-danger"></i> <span><?= $lang['filtros']['delete']?></span></span>
				<button type="button" class="btn pln-btn-orange" onclick="saveFilter()" id="btnfiltro_salvar"><span id="spn_save"><?= $lang['filtro']['save']?></span> <i class="fa fa-spin fa-spinner pln-invi"></i></button>
			</div>
		</div>
	</div>
</div>

</div>

<div class="clear"></div>
<?php
$JS = ['uglify/all-ordercover-filters-min'];
?>

