<div class="modal fade" id="agroup_fields" data-keyboard="true" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_titulo" name="modal_titulo"><?php echo $lang['capa_pedido_lista']['definicao_agrupamento'] ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row d-flex flex-row justify-content-center">
                    <h5 id="definition_msg" class="text-danger pln-invi"><?php echo $lang['capa_pedido_lista']['alerta_agrupamento'] ?></h5>
                </div>
                <div class="row mt-3 ml-3 mr-3 fields-to-agroup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for=""><?php echo $lang['capa_pedido_lista']['cod_comprador'] ?></label>
                            <select class="form-control form-control-sm agrup_cod_comprador">
                                <option value=""><?php echo $lang['capa_pedido_lista']['selecione'] ?></option>
                            </select>
                            <div class="invalid-feedback">
                                <?php echo $lang['capa_pedido_lista']['campo_obrigatorio'] ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><?php echo $lang['capa_pedido_lista']['cod_setor'] ?></label>
                            <select class="form-control form-control-sm agrup_cod_setor">
                                <option value=""><?php echo $lang['capa_pedido_lista']['selecione'] ?></option>
                            </select>
                            <div class="invalid-feedback">
                                <?php echo $lang['capa_pedido_lista']['campo_obrigatorio'] ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><?php echo $lang['capa_pedido_lista']['motivo'] ?></label>
                            <select class="form-control form-control-sm agrup_motivo">
                                <option value=""><?php echo $lang['capa_pedido_lista']['selecione'] ?></option>
                            </select>
                            <div class="invalid-feedback">
                                <?php echo $lang['capa_pedido_lista']['campo_obrigatorio'] ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><?php echo $lang['capa_pedido_lista']['deposito'] ?></label>
                            <select class="form-control form-control-sm agrup_deposito">
                                <option value=""><?php echo $lang['capa_pedido_lista']['selecione'] ?></option>
                            </select>
                            <div class="invalid-feedback">
                                <?php echo $lang['capa_pedido_lista']['campo_obrigatorio'] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <span class="pln-danger pln-cur-pointer" data-dismiss="modal" aria-label="Close"><span><?php echo $lang['capa_pedido_lista']['cancelar'] ?></span></span>
                <button type="button" class="btn pln-btn-orange" id="agroup_confirm"><i class="fa fa-object-group" aria-hidden="true"></i> <span id="spn_save"><?php echo $lang['capa_pedido_lista']['agrupar'] ?></span> <i class="fa fa-spin fa-spinner pln-invi"></i></button>
            </div>
        </div>
    </div>
</div>