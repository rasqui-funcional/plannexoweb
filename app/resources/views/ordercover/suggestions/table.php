<?php
if (!$ViewData['data_table']) {
    echo 'false';
    return;
}
?>
<form id="formulario">
    <input type="hidden" id="delete_sug" value="<?php echo $ViewData['data_table'][0]['PSUG_GROUPED_PK'] ?>"/>

    <div id="controlSize" class="table-responsive">
        <table id="tableOrderCoverSuggestions" class="table dataTable ordercover-table bigger-table">
            <thead>
                <tr class="tableOrderCoverSuggestions_tr">
                    <th>
                        <div class="d-flex flex-column align-items-center">
                            <span></span><span class="pt-1"><input id="chk_all" type="checkbox"/></span>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row">
                            <span data-toggle="tooltip"
                                  title="<?php echo $lang['capa_pedido_parcela']['header']['parcela'] ?>">
                                <?php echo $lang['capa_pedido_parcela']['header']['parcela'] ?>
                            </span>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row">
                            <span data-toggle="tooltip"
                                      title=""><?php echo $lang['capa_pedido_parcela']['header']['cod_item'] ?></span>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row">
                            <span data-toggle="tooltip"
                                  title=""><?php echo $lang['capa_pedido_parcela']['header']['descricao'] ?></span>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row">
                            <span data-toggle="tooltip"
                                  title="VALOR TOTAL (<?=$lang['global']['country_currency']?>)"><?php echo $lang['capa_pedido_parcela']['header']['multiplo'] ?></span>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row">
                            <span data-toggle="tooltip"
                                  title=""><?php echo $lang['capa_pedido_parcela']['header']['min'] ?></span>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row pln-cur-pointer order-col" data-id="QTY_ORIGINAL">
                            <span data-toggle="tooltip"
                                  title="<?php echo $lang['capa_pedido_parcela']['header']['valor_atual'] ?>">
                                <?php echo $lang['capa_pedido_parcela']['header']['qtd_origem'] ?>
                            </span>
                            <i class="fa fa-sort<?= isset($ViewData['order']['QTY_ORIGINAL']) ? "-" . $ViewData['order']['QTY_ORIGINAL'] : "" ?> pln-link-color ml-1 align-self-center"></i>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row pln-cur-pointer order-col" data-id="TOTAL_VALUE_ORIGINAL">
                            <span data-toggle="tooltip"
                                  title="<?php echo $lang['capa_pedido_parcela']['header']['valor_atual'] ?>">
                                <?php echo $lang['capa_pedido_parcela']['header']['valor_atual'] ?>
                            </span>
                            <i class="fa fa-sort<?= isset($ViewData['order']['TOTAL_VALUE_ORIGINAL']) ? "-" . $ViewData['order']['TOTAL_VALUE_ORIGINAL'] : "" ?> pln-link-color ml-1 align-self-center"></i>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row">
                            <span data-toggle="tooltip"
                                  title=""><?php echo $lang['capa_pedido_parcela']['header']['qtd_alterado'] ?></span>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row pln-cur-pointer order-col" data-id="QTY_ALTERED">
                            <span data-toggle="tooltip"
                                  title="<?php echo $lang['capa_pedido_parcela']['header']['valor_alterado'] ?>">
                                <?php echo $lang['capa_pedido_parcela']['header']['valor_alterado'] ?>
                            </span>
                            <i class="fa fa-sort<?= isset($ViewData['order']['QTY_ALTERED']) ? "-" . $ViewData['order']['QTY_ALTERED'] : "" ?> pln-link-color ml-1 align-self-center"></i>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row">
                            <span data-toggle="tooltip"
                                  title=""><?php echo $lang['capa_pedido_parcela']['header']['data_entrega'] ?></span>
                        </div>
                    </th>
                    <th>
                        <div class="d-flex flex-row">
                            <span data-toggle="tooltip"
                                  title=""><?php echo $lang['capa_pedido_parcela']['header']['obs'] ?></span>
                        </div>
                    </th>
                </tr>
            </thead>

            <tfoot>
                <tr>
                    <th><input type="hidden" id="hdn_total_items" value="<?= $ViewData['footer']['count'] ?>"/></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th><?php echo $ViewData['footer']['data']['TOTAL_VALUE'] ?></th>
                    <th></th>
                    <th></th>
                </tr>

                <input type="hidden" id="total_page_number" value="<?php echo $ViewData['footer']['pages'] ?>">
            </tfoot>

            <tbody id="tableOrderCoverSuggestionsBody">
            <?php foreach ($ViewData['data_table'] as $key => $Data) { ?>
                <tr class="tableOrderCoverSuggestions_tr" data-id-sku="<?php echo $Data['ID_SKU_FK'] ?>">
                    <td class="text-center">
                        <input class="input_chk" value="<?= $Data['ID_SUG_PK'] ?>"
                               data-num-parc="<?= $Data['NUM_PARC'] ?>" data-id-psug="<?= $Data['ID_PSUG_PK'] ?>"
                               data-id-psug-grouped="<?= $Data['PSUG_GROUPED_PK'] ?>" type="checkbox">

                    </td>
                    <td class="text-center"><span><?php echo $Data['NUM_PARC'] ?> </span></td>
                    <td><span class="COD_ITEM_PK"><?php echo $Data['COD_ITEM_PK'] ?></span></td>
                    <td class="pln-min-w-100 text-truncate pln-td-mw-50">
                        <span data-toggle="tooltip" title="<?php echo $Data['DESC_ITEM'] ?>">
                            <?php echo $Data['DESC_ITEM'] ?>
                        </span>
                    </td>
                    <td><span><?php echo $Data['V_ORDER_MULT'] ?></span></td>
                    <td><span><?php echo $Data['V_ORDER_MIN'] ?></span></td>
                    <td><span><?php echo Utilities::dataFormatter('QTY_ORIGINAL',$Data['QTY_ORIGINAL']) ?></span></td>
                    <td><span><?php echo $Data['TOTAL_VALUE_ORIGINAL'] ?></span></td>
                    <td>
                        <input type="text" class="form-control form-control-sm qtd_alterado QTY_ALTERED get-changed" value="<?php echo $Data['QTY_ALTERED'] ?>">
                    </td>
                    <td><span><?php echo $Data['TOTAL_VALUE'] ?></span></td>
                    <td><input type="date" min="<?= date('Y-m-d') ?>" class="form-control form-control-sm singledate DATE_ARRIVAL_ALTERED get-changed"
                               value="<?php echo $Data['DATE_ARRIVAL_DMY'] ?>" onkeydown="return false"></td>
                    <td class="pln-min-w-200">
                        <textarea class="form-control form-control-sm txtarea_obs get-changed"><?php echo $Data['OBS'] ?></textarea>
                    </td>
                </tr>
                <?php
            } ?>
            </tbody>
        </table>
    </div>
</form>
<script>
    orderCoverSuggest.initialize();
    paginationClass.initialize();
</script>
