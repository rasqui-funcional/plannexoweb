<?php
$JS = [
    'uglify/all-ordercover-suggestions-list-min',
    "i18n/{$Country}/orderCover",
];
?>

<div class="content-i">
    <div class="content-full-table">
        <div style="" class="element-wrapper">
            <!-- title header -->
            <div class="title-header d-flex flex-row align-items-end mt-3">
                <div>
                    <a href="/<?php echo $Country ?>/ordercover/list" class="pln-link-color" onclick="loaderShow()"><i
                                class="fa fa-angle-double-left"
                                aria-hidden="true"></i> <?php echo $lang['sugcompras_lista']['voltar_list'] ?></a>
                    <h4 class="pln-font-color"><?php echo $lang['capa_pedido_parcela']['titulo'] ?></h4>
                </div>

                <span class="ml-auto pln-items-search mb-2 mr-3" style="display: none !important">
                    <span id="download_csv"
                          class="pln-link-color pln-cur-pointer mr-3"><i class="fas fa-file-csv"></i> <?php echo $lang['capa_pedido_parcela']['download'] ?></span>
                    <span class="ml-auto mr-2 " id="delete_items">
                        <button type="button" id="toDisallow" class="btn pln-btn-ghost" disabled>
                            <i class="fa fa-times"
                               aria-hidden="true"></i> <?php echo $lang['capa_pedido_parcela']['excluir'] ?>
                        </button>
                    </span>
                    <span class="ml-auto mr-2 " id="confirm_delete">
                        <button type="button" id="toAgroup" class="btn pln-btn-ghost" disabled>
                            <i class="fa fa-object-ungroup"
                               aria-hidden="true"></i> <?php echo $lang['capa_pedido_parcela']['desagrupar'] ?>
                        </button>
                    </span>
                    <button type="button" id="saveData"
                            class="btn pln-btn-orange"><i class="fa fa-save mr-1"></i><?php echo $lang['parametros']['salvar'] ?></button>
                </span>

            </div>
            <!-- title header end -->

            <div class="element-box-full">
                <div id="table_insert"></div>

                <div class="d-flex mt-1 align-items-start pln-items-search" style="display: none !important">
                    <div class="d-flex flex-row align-items-center ml-3 mr-3">
                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_a'] ?></span>
                            <span id="page_items_to"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_de'] ?></span>
                            <span id="page_items_total"></span>
                        </span>

                        <div class="d-flex flex-row">
                            <div class="d-flex flex-column mr-3">
                                <small>
                                    <?php echo $lang['sugcompras_lista']['itens']['mostrando'] ?>
                                    <span id="total_items"></span>
                                    <?php echo $lang['capa_pedido_lista']['sugestoes'] ?>
                                </small>
                                <small>
                                    <?php echo $lang['sugcompras_lista']['pagina']['qtd_total'] ?>
                                    <span id="spn_total_page_number"></span>
                                </small>
                            </div>

                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>
                        </div>
                    </div>

                    <div class="d-flex flex-row align-items-center">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['pagina']['titulo'] ?></span>
                        <span class="ml-1">
                            <input type="text" maxlength="10" id="goto_pagenumber" class="form-control form-control-sm input_just_number form-control form-control-sm-sm pln-input-60"/>
                        </span>
                        <span class="ml-1">
                            <button type="button" id="goToPageNumber" class="btn pln-btn-ghost"/>
                            <?php echo $lang['sugcompras_lista']['buscar'] ?>
                        </span>
                    </div>

                    <div class="d-flex flex-row align-items-center ml-auto mr-3">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['visualizar'] ?></span>

                        <?php include_once('resources/views/items-per-page.php'); ?>

                        <span class="ml-1"><?php echo $lang['sugcompras_lista']['por_pagina'] ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>