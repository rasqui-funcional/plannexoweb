<div aria-hidden="true" id="modal_manual_suggest" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label_order">
                <?=$lang['sugcompras_manual']['titulo']?>
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span><small><?=$lang['modal']['fechar']?></small></button>
            </div>
            <div class="modal-body">
                <!--
                <div id="order_loading" class="text-center"><i class='fa fa-spin fa-spinner'></i> <?=$lang['sugcompras_lista']['carregando']?></div>
                <div id="include_order">        
                </div>
                -->
                <div class="table-responsive table-wrapper-scroll-y">
                    <table id="tableManualSug" class="table dataTable table-black-style"> 
                        <thead>
                            <th><?php echo $lang['sugcompras_manual']['item'] ?></th>
                            <th><?php echo $lang['sugcompras_manual']['estab'] ?></th>                                
                            <th><?php echo $lang['sugcompras_manual']['descricao'] ?></th>
                            <th><?php echo $lang['sugcompras_manual']['qtde'] ?></th>
                            <th><?php echo $lang['sugcompras_manual']['data_entrega'] ?></th>
                            <th><?php echo $lang['sugcompras_manual']['urgente'] ?></th>
                            <th><?php echo $lang['sugcompras_manual']['aprovado'] ?></th>
                            <th></th>
                        </thead>
                        <tfoot></tfoot>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-between mt-3"> 
                    <div>
                        <span class="pln-link-color pln-cur-pointer" id="newSuggest"><i class="fa fa-plus"></i> <span><?php echo $lang['sugcompras_manual']['nova_sug'] ?></span></span>
                    </div>
                    <div>
                        <button type="button" class="btn pln-btn-orange" id="suggestionValidate"><?php echo $lang['sugcompras_manual']['adicionar'] ?></button>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>