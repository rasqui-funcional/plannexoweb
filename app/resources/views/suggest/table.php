<? if (!$ViewData['datatable']) {
    echo 'false';
    return;
}
?>
<thead>
<tr>
    <?php foreach ($ViewData['cols'] as $column_name => $column_data): ?>
        <th>
            <div class="d-flex flex-row pln-cur-pointer order-col align-items-center" data-id="<?= $column_name ?>">
                <?php
                $span_text = '';
                if (!isset($lang['vw_scr_scm420g_sug']['abrev'][strtolower($column_name)])) {
                    $span_text = $lang['vw_scr_scm420g_sug'][strtolower($column_name)];
                } else {
                    $span_text = $lang['vw_scr_scm420g_sug']['abrev'][strtolower($column_name)];
                }

                $title = ($column_name == 'OUT_WEEK_MONTH_PERCENT_AVG')
                    ? $lang['vw_scr_scm420g_sug']['title'][strtolower($column_name)]
                    : $lang['vw_scr_scm420g_sug'][strtolower($column_name)];
                ?>
                <span data-toggle="tooltip" title="<?= $title ?>">
                        <?= $span_text ?>
                    </span>
                <i class="fa fa-sort<?= $column_data['order'] == null ? "" : "-" . $column_data['order'] ?> pln-link-color ml-1 align-self-center"></i>
            </div>
            <div class="d-flex flex-column align-items-center">
                <?= $column_name == 'SIT_SAVED' ? '<label class="pln-container-checkbox mt-3 mb-2"><input id="ap_all" type="checkbox" /><span class="pln-orange-checkbox"></span></label>' : '' ?>
            </div>
        </th>
    <? endforeach; ?>
</tr>
</thead>

<tbody>
<?php foreach ($ViewData['datatable'] as $key => $data):
    $suggest_status = 'pln-tr-normal';
    if ($data['SIT_ANALYSED']) $suggest_status = 'pln-tr-analised';
    if ($data['SIT_SAVED']) $suggest_status = 'pln-tr-approved';
    ?>
    <tr class="table_tr <?= $suggest_status; ?> <?= isset($data['GROUPED_INSTALLMENTS']) && $data['GROUPED_INSTALLMENTS'] == 1 && $data['NUM_PARC_PK'] > 1 ? 'deactivate' : '';?>"
        id="<?= $data['ID_SKU_PK'] ?>" data-id-sku="<?= $data['ID_SKU_PK'] ?>" data-lt="<?= $data['LT_TOTAL_OUT'] ?>"
        data-out6="<?= $data['OUT6'] ? Utilities::dataFormatter('OUT6', $data['OUT6']) : 0 ?> ">
        <? $counter = 1; ?>
        <? foreach ($ViewData['cols'] as $column_name => $column_data): ?>
            <? $value = Utilities::dataFormatter($column_name, $data[$column_name]); ?>
            <? switch ($column_data['type']):
                case 'input-text': ?>
                    <?= sprintf(
                        '
                            <td>
                                <input type="text" tabindex="%s"
                                        class="form-control form-control-sm input_just_number to-serialize"
                                        value="%s" name="%s">
                            </td>
                        ',
                        $counter,
                        $value,
                        $column_name
                    ); ?>
                    <? break; ?>

                <? case 'input-date': ?>
                    <? $time = strtotime(str_replace("/", "-", $value)); ?>
                    <? $is_weekend = (date('N', $time) >= 6 ? 'is-invalid' : ''); ?>
                    <?= sprintf(
                        '
                                        <td>
                                            <input type="date" min="%s"
                                                 class="suggest-date-changed-arrival
                                                    form-control form-control-sm singledate to-serialize %s"
                                                 value="%s" name="%s" onkeydown="return false" required>
                                        </td>
                                    ',
                        date('Y-m-d'),
                        $is_weekend,
                        date('Y-m-d', $time),
                        $column_name
                    ); ?>
                    <? break; ?>
                <? case 'input-textarea': ?>
                    <?= sprintf(
                        '<td class="pln-column-obs">
                                        <textarea rows="1" class="form-control form-control-sm to-serialize pln-remove-scroll pln-textarea-resize m-0" name="%s" maxlength="400">%s</textarea>
                                    </td>',
                        $column_name,
                        $value
                    ); ?>
                    <? break; ?>
                <? case 'img': ?>
                    <?= ViewImage::invLevelImg($data[$column_name], $lang); ?>
                    <? break; ?>
                <? case 'ind_excess': ?>
                    <?= ViewImage::indExcessImg($data['ID_SUG_PK'], $data[$column_name]); ?>
                    <? break; ?>
                <? case 'num_parc': ?>
                    <td class="pln-custom-tooltip">
                        <?= View::modalPurchaseOrder($data, $value); ?>
                        <? unset($data['SIT_URGENT']); ?>
                    </td>
                    <? break; ?>
                <? case 'qty_sol': ?>
                    <?= View::stock($data, $value, 'PurchaseRequest'); ?>
                    <? break; ?>
                <? case 'order': ?>
                    <td>
                        <?= View::modalOrder($data, $value); ?>
                    </td>
                    <? break; ?>
                <? case 'modal_stock': ?>
                    <?= View::stock($data, $value, 'OverStock'); ?>
                    <? break; ?>
                <? case 'highlight-fields': ?>
                    <td>
                        <!-- Procurar coluna que representa data parcela -->
                        <? if ($data['DATE_NEXT_IN'] && strtotime($data['DATE_NEXT_IN']) < time()): ?>
                            <span class="pln-text-red"><?= $value ?></span>
                        <? else: ?>
                            <?= $value ?>
                        <? endif; ?>
                    </td>
                    <? break; ?>
                <? case 'checkbox': ?>
                    <td class="text-center">
                        <label class="pln-container-checkbox">
                            <input <?= $column_name == 'SIT_ANALYSED' ? "data-id-sku='{$data['ID_SKU_PK']}'" : null ?>
                                    name="<?= strtolower($column_name) ?>"
                                    class="<?= $column_name == 'SIT_SAVED' ? 'input_ap' : 'input_an' ?> to-serialize"
                                    type="checkbox" <?= $value ? 'checked' : null ?> />
                            <span class="pln-orange-checkbox"></span>
                        </label>
                    </td>
                    <? break; ?>
                <? case 'desc_item': ?>
                    <td class="text-truncate pln-td-mw-200">
                                    <span data-toggle="tooltip"
                                          title="<?= $value;?>">
                                        <?= $value;?>
                                    </span>
                    </td>
                    <? break; ?>

                    <? /* case 'variacao_consumo':
                    $randNumber = rand(-1, 2)
                    ?>
                    <td class="pln-custom-tooltip">
                        <div class="d-block text-right pln-tooltip <? echo $value > 0 ? 'text-danger' : 'pln-text-dark' ?>">
                            <span class="td-data">
                            <? echo $value == 0 ? "&mdash; {$lang['vw_scr_scm405g_sku_list']['no_variation']} &mdash;" : "{$value}%" ?>
                            <i class="fas <? echo $value == 0 ? 'd-none' : ($value > 0 ? 'fa-arrow-up' : 'fa-arrow-down') ?>"></i>
                            </span>

                            <span class="tooltiptext bg-white shadow-sm">
                            <div class="tooltip-title <? echo $value > 0 ? 'bg-danger' : 'bg-grey' ?>">
                                <? echo $value == 0 ? $lang['vw_scr_scm405g_sku_list']['no_consumption_variation'] : ($value > 0 ? $lang['vw_scr_scm405g_sku_list']['increase_consumption'] : $lang['vw_scr_scm405g_sku_list']['consumption_reduction']) ?>
                            </div>

                            <div class="tooltip-body d-flex justify-content-around">
                                <div>
                                <div class="cmd-value"><?= (!is_null($data['TOTAL_OUT_LAST_MONTH_AVG'])) ? round($data['TOTAL_OUT_LAST_MONTH_AVG'], 2) : 0 ?></div>
                                <div class="cmd-days"><? echo $lang['vw_scr_scm405g_sku_list']['cmd30'] ?></div>
                                </div>

                                <div>
                                <div class="cmd-value"><?= (!is_null($data['TOTAL_OUT_LAST_HALF_MONTH_AVG']) ? round($data['TOTAL_OUT_LAST_HALF_MONTH_AVG'], 2) : 0) ?></div>
                                <div class="cmd-days"><? echo $lang['vw_scr_scm405g_sku_list']['cmd15'] ?></div>
                                </div>

                                <div>
                                <div class="cmd-value <? echo $value == 0 ? 'pln-text-dark' : ($value > 0 ? 'text-danger' : 'text-grey')  ?> ">
                                    <?= (!is_null($data['TOTAL_OUT_LAST_WEEK_AVG']) ? round($data['TOTAL_OUT_LAST_WEEK_AVG'], 2) : 0) ?>
                                </div>
                                <div class="cmd-days"><? echo $lang['vw_scr_scm405g_sku_list']['cmd7'] ?></div>
                                </div>
                            </div>
                            </span>
                        </div>
                    </td>
                <? break; */ ?>

                <? case 'mult_order':
                    $value = ($value < 1 && $value > 0) ? '0' . $value : $value;
                    ?>
                    <td class="text-truncate pln-td-mw-200">
                        <span data-toggle="tooltip"
                              title="<?= $value;?>">
                            <?= $value;?>
                        </span>
                    </td>
                    <? break; ?>

                <? default: ?>
                    <? if ($column_name == "ID_PROFILE"
                        || $column_name == "COD_GROUP2_FK"
                        || $column_name == "COD_GROUP3_FK"
                        || $column_name == "COD_GROUP4_FK"
                        || $column_name == "COD_GROUP6_FK"
                        || $column_name == "DESC_SUPPLIER"
                    ): ?>
                        <? $element_params = View::genericWidthTable($value); ?>
                        <td class="<?= $element_params['td_class'] ?>">
                            <span data-toggle="<?= $element_params['data_toggle'] ?>"
                                  title="<?= $element_params['title'] ?>">
                                <?= $value ?>
                            </span>
                        </td>
                    <? elseif ($column_name === 'SIT_URGENT'): ?>
                        <td>
                            <span>
                                <?= $value === '1' ? 'Sim' : 'Não' ?>
                            </span>
                        </td>
                    <? elseif ($column_name === 'PURCHASE_REQ'): ?>
                        <td>
                            <span class="pln-cur-pointer pln-link-color tableSug_td" data-id-sku="1769110140" data-change="PurchaseRequest" data-toggle="tooltip" data-original-title="Estoque">
                                <?= $value ?>
                            </span>
                        </td>

                    <? else: ?>
                        <td class="<?= ($column_name == "ID_CALENDAR_FK" ? 'text-nowrap' : '') ?>">
                            <?= $value ?>
                        </td>
                    <? endif; ?>
                    <? break; ?>
                <? endswitch; ?>
            <? $counter++; ?>
        <? endforeach; ?>

        <input name="id_sug_pk" class="to-serialize" type="hidden" value="<?= $data['ID_SUG_PK'] ?>"/>
        <input name="id_sku_pk" class="to-serialize" type="hidden" value="<?= $data['ID_SKU_PK'] ?>"/>
        <input name="cod_item_pk" class="to-serialize" type="hidden" value="<?= $data['COD_ITEM_PK'] ?>"/>
    </tr>
<? endforeach; ?>
</tbody>
<script>initialize();</script>
