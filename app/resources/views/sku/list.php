<!-- Google Charts Loader -->
<script src="https://www.gstatic.com/charts/loader.js"></script>
<?php

$JS = ['uglify/all-sku-list-min'];

?>
<div class="content-i">
    <div class="content-full-table">
        <div class="element-wrapper">
            <div class="title-header d-flex flex-row align-items-end mt-3">
                <div>
                    <a href="/<?php echo $Country?>/sku/filters" onclick="loaderShow()" class="pln-link-color"><i class="fa fa-angle-double-left" aria-hidden="true"></i> <?php echo $lang['sugcompras_lista']['voltar']?></a>
                    <h4 class="pln-font-color"><?php echo  $lang['sku_form']['cols'][''] ?></h4>
                </div>

                <div class="d-flex align-items-end pln-items-search ml-5 mb-2" style="display: none !important">                
                    <div class="d-flex flex-column align-items-start">
                        
                        <span>
                            <select class="form-control form-control-sm" id="suggest-seach-col">
                                <option value="cod_item_pk"><?php echo  $lang['vw_scr_scm420g_sug']['cod_item_pk'] ?></option>
                                <option value="desc_item"><?php echo  $lang['vw_scr_scm420g_sug']['desc_item'] ?></option>
                                <option value="cod_estab_fk"><?php echo  $lang['vw_scr_scm420g_sug']['cod_estab_fk'] ?></option>
                                <option value="id_sku_pk"><?php echo  $lang['vw_scr_scm420g_sug']['id_sku_pk'] ?></option>
                                <option value="curve_abc"><?php echo  $lang['vw_scr_scm420g_sug']['curve_abc'] ?></option>
                                <option value="curve_pqr"><?php echo  $lang['vw_scr_scm420g_sug']['curve_pqr'] ?></option>
                                <option value="curve_xyz"><?php echo  $lang['vw_scr_scm420g_sug']['curve_xyz'] ?></option>
                                <option value="curve_123"><?php echo  $lang['vw_scr_scm420g_sug']['curve_123'] ?></option>
                                <?php foreach ($ViewData['SearchFields'] as $key => $value): ?>
                                    <option value="cod_group<?= $key ?>_fk"><?= $value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </span>
                    </div>

                    <div class="d-flex flex-row align-items-end ml-1">
                        <span><input type="text" class="form-control form-control-sm col" id="suggest-search-input" /></span>
                        <span class="ml-1"><button type="button" class="btn pln-btn-ghost" onclick="suggestSearch()" /><?php echo  $lang['parametros']['buscar'] ?></span>
                    </div>
                </div>
                <div class="ml-auto pln-items-search-sug mb-2 mr-3">
                    <span id="download_csv_sku_list" class="pln-link-color pln-cur-pointer mr-3">
                        <i class="fas fa-file-csv"></i>
                        <?php echo $lang['resumo_lista']['csv'] ?>
                    </span>

                    <button type="button" id="openFullscreen" class="btn pln-btn-ghost">
                        <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                        <?php echo $lang['botao']['expandir'] ?>
                    </button>
                </div>
            </div>
            
            <div class="element-box-full">                
                <div id="table_insert">
                    <form id="formulario">
                        <div id="controlSize" class="table-responsive">
                            <table id="tableSku" class="table dataTable sku-table bigger-table">
                                <tfoot class="hide">
                                    <tr></tr>
                                </tfoot>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="d-flex mt-1 align-items-center pln-items-search" style="display: none !important" id="pln-pagination-footer">

                    <div class="d-flex flex-row align-items-center ml-3 mr-3">

                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?php echo  $lang['sugcompras_lista']['pagina']['qtd_a'] ?></span>
                            <span id="page_items_to"></span>
                            <span><?php echo  $lang['sugcompras_lista']['pagina']['qtd_de'] ?></span>
                            <span id="page_items_total"></span>
                        </span>
                        <div class="d-flex flex-row">
                            <div class="d-flex flex-column mr-3">
                                <small>
                                    <?php echo $lang['sugcompras_lista']['itens']['mostrando'] ?> <span id="total_items"></span> <?php echo $lang['sugcompras_lista']['itens']['sku'] ?>
                                </small>
                                <small>
                                    <?php echo $lang['sugcompras_lista']['pagina']['qtd_total']?> <span id="spn_total_page_number"></span>
                                </small>
                            </div>
                            
                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>
                            
                        </div>
                    </div>

                    <div class="d-flex flex-row align-items-center">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['pagina']['titulo']?></span>
                        <span class="ml-1"><input type="number" min="0" maxlength="10" id="goto_pagenumber" class="form-control form-control-sm pln-input-60" /></span>
                        <span class="ml-1"><button type="button" onclick="goToPageNumber()" class="btn pln-btn-ghost" /><?php echo  $lang['sugcompras_lista']['buscar'] ?></span>
                    </div>
                    <div class="d-flex flex-row align-items-center ml-auto mr-3">
                        <span class="pln-bold-600"><?php echo  $lang['sugcompras_lista']['visualizar'] ?></span>
                        <?php include_once('resources/views/items-per-page.php'); ?>
                        <span class="ml-1"><?php echo  $lang['sugcompras_lista']['por_pagina'] ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- divs que fecham o menu end -->
<input type='hidden' name='campoordena' id='campoordena' value='<?php echo  $campoordena ?>'>
<input type='hidden' name='tipoordena' id='tipoordena' value='<?php echo  $tipoordena ?>'>
<?php include_once('resources/views/infosku/index.php'); ?>
<?php include_once('resources/views/modals/modal_excess.php'); ?>

<div aria-hidden="true" id="modal_order" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label_order">
                    <?php echo $lang['sugcompras_lista']['ordem']['titulo']?>
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span><small><?php echo $lang['modal']['fechar']?></small></button>
            </div>
            <div class="modal-body">
                <div id="order_loading" class="text-center"><i class='fa fa-spin fa-spinner'></i> <?php echo $lang['sugcompras_lista']['carregando']?></div>
                <div id="include_order">
                </div>
            </div>
        </div>
    </div>
</div>