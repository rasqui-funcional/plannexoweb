<?php
$JS = [
    'uglify/all-approvals-list-min',
];

?>
<div class="content-i">
    <div class="content-full-table">
        <div class="element-wrapper">
            <!-- title header -->
            <div class="title-header d-flex flex-row align-items-end mt-3">
                <div>
                    <h4 class="pln-font-color">
                        <?php echo $lang['aprovacoes']['historico_de_aprovacoes']; ?>
                    </h4>
                </div>

                <div class="d-flex align-items-end pln-items-search ml-5 mb-2" style="display: none !important">
                    <div class="d-flex flex-column align-items-start">
                        <span>
                            <select class="form-control form-control-sm" id="seach_col">
                                <? foreach($ViewData['filters'] as $col): ?>
                                    <option value="<?= $col?>"><?= $lang['aprovacoes'][$col]?></option>
                                <? endforeach; ?>
                            </select>
                        </span>
                    </div>
                    <div class="d-flex flex-row align-items-end ml-1">
                        <span id="change-input">
                            <input type="number" class="form-control form-control-sm col" id="search_input"/>
                            <select class="form-control form-control-sm hide" id="aux_status">
                                <option value="1"><?= $lang['aprovacoes']['status'][1] ?></option>
                                <option value="2"><?= $lang['aprovacoes']['status'][2] ?></option>
                                <option value="3"><?= $lang['aprovacoes']['status'][3] ?></option>
                            </select>
                            <select class="form-control form-control-sm hide" id="aux_urgent">
                                <option value="Y"><?= $lang['aprovacoes']['Y'] ?></option>
                                <option value="N"><?= $lang['aprovacoes']['N'] ?></option>
                            </select>
                            <select class="form-control form-control-sm hide" id="aux_estabs">
                                <? foreach ($ViewData['estabs']['Estab'] as $key => $value): ?>
                                    <option value="<?= $value['code'] ?>"><?= $value['name'] ?></option>
                                <? endforeach ?>
                            </select>
                        </span>
                        <span class="ml-1">
                            <button type="button" class="btn pln-btn-ghost" id="search"/>
                            <?php echo $lang['parametros']['buscar'] ?>
                        </span>
                    </div>
                </div>

                <span class="ml-auto pln-items-search mb-2 mr-3" style="display: none !important">
                        <span id="download" class="pln-link-color pln-cur-pointer">
                            <i class="fas fa-file-csv mr-1"></i><?php echo $lang['download_csv'] ?>
                        </span>
                    </span>
            </div>
            <!-- title header end -->

            <div class="element-box-full">
                <div id="table_insert"></div>

                <div class="d-flex mt-1 align-items-start pln-items-search" style="display: none !important">
                    <div class="d-flex flex-row align-items-center ml-3 mr-3">
                            <span class="pln-invi" id="page_from_to">
                                <span id="page_items_from"></span>
                                <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_a'] ?></span>
                                <span id="page_items_to"></span>
                                <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_de'] ?></span>
                                <span id="page_items_total"></span>
                            </span>

                        <div class="d-flex flex-row">
                            <div class="d-flex flex-column mr-3 justify-content-center">
                                <small>
                                    <?php echo $lang['sugcompras_lista']['itens']['mostrando'] ?>
                                    <span id="total_items"></span>
                                    <?php echo $lang['aprovacoes']['aprovacoes']; ?>
                                </small>
                                <small>
                                    <?php echo $lang['aprovacoes']['pagina']['qtd_total']?> <span id="spn_total_page_number"></span>
                                </small>
                            </div>

                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>
                        </div>
                    </div>

                    <div class="d-flex flex-row align-items-center">
                        <span class="pln-bold-600"><?=$lang['pagination']['page']?></span>

                        <span class="ml-1">
                                <input type="text" maxlength="10"
                                       id="goto_pagenumber"
                                       class="form-control input_just_number form-control-sm pln-input-60"/>
                            </span>

                        <span class="ml-1">
                                <button type="button" id="goToPageNumber" class="btn pln-btn-ghost"/>
                                <?php echo $lang['sugcompras_lista']['buscar'] ?>
                            </span>
                    </div>

                    <div class="d-flex flex-row align-items-center ml-auto mr-3">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['visualizar']; ?></span>

                        <?php include_once('resources/views/items-per-page.php'); ?>

                        <span class="ml-1"><?php echo $lang['sugcompras_lista']['por_pagina']; ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal loading" data-keyboard="false" data-backdrop="static" id="modal_loading" tabindex="-1"
     role="dialog" aria-labelledby="botao_loading" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document" style="margin-top: 15%">
        <div class="modal-content">
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated actived" role="progressbar"
                         aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<button id="btn_loading" type="button" data-toggle="modal" data-target="#modal_loading" hidden></button>

<!-- divs que fecham o menu end -->
<input type='hidden' name='campoordena' id='campoordena' value='<?php echo $campoordena ?>'>
<input type='hidden' name='tipoordena' id='tipoordena' value='<?php echo $tipoordena ?>'>
<input type='hidden' name='indicator' id='indicator' value='<?php echo $ViewData['indicator'] ?>'>
