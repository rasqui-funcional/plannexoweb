<?php
if (!$ViewData['table'] || count($ViewData['table']['data']) < 1) {
    echo 'false';
    return;
}
?>

<div id="controlSize" class="teste table-responsive">
    <input type="hidden" id="total_page_number" value="<?php echo $ViewData['footer']['pages']; ?>"/>
    <input type="hidden" id="hdn_total_items" value="<?php echo $ViewData['footer']['count']; ?>"/>

    <table id="approvalTable" class="table dataTable approval-table bigger-table">
        <thead>
            <tr>
                <?php
                unset($ViewData['table']['columns']['RNUM']);
                foreach ($ViewData['table']['columns'] as $key => $value):?>
                    <th>
                        <div class='d-flex flex-row pln-cur-pointer order-col' data-id='<?= $key ?>'>
                            <span data-toggle="tooltip"
                                title="<?= isset($lang['aprovacoes']['abbr'][$key]) ? $lang['aprovacoes'][$key] : '' ?>">
                                <?= (!isset($lang['aprovacoes']['abbr'][$key]) ? $lang['aprovacoes'][$key] : $lang['aprovacoes']['abbr'][$key]) ?>
                            </span>
                            
                            <i class="fa fa-sort<?= $value['order'] == null ? "" : "-" . $value['order']?> pln-link-color ml-1 align-self-end"></i>
                        </div>
                    </th>
                <? endforeach ?>
            </tr>
        </thead>
 
        <tfoot>
        <tr>
            <?php
            foreach ($ViewData['table']['columns'] as $ColName => $ColType) {
                echo sprintf("<th>%s</th>", $ViewData['footer']['data'][$ColName]);
            }
            ?>
            <input type="hidden" id="hdn_total_items" value="<?php echo $ViewData['footer']['count']; ?>"/>
        </tr>

        <input type="hidden" id="total_page_number" value="<?php echo $ViewData['footer']['pages']; ?>">
        </tfoot>

        <tbody>
        <?php foreach ($ViewData['table']['data'] as $row): ?>
            <? unset($row['RNUM']); ?>
            <tr>
            <? foreach ($row as $key => $value): ?>
                <? $columnValue = View::pradonizationValues($key, $value, $ViewData, $lang, $row['COD_HEADER_PK']); ?>
                <? $element_params = View::genericWidthTable($columnValue); ?>
                <? if ($key == 'DATE_INTEGRATION' || $key == 'DATE_APPROVED'): ?>
                <td>
                        <span
                              data-toggle="<?= $element_params['data_toggle'] ?>"
                              title="<?= $element_params['title'] ?>">
                            <?= $columnValue ?>
                        </span>
                    </td>                 
                <?elseif ($key !== 'TOTAL_PARCELAS'):?>
                <td class="<?= $element_params['td_class'] ?>">
                        <span
                              data-toggle="<?= $element_params['data_toggle'] ?>"
                              title="<?= $element_params['title'] ?>">
                            <?= $columnValue ?>
                        </span>
                    </td>      
                <? else: ?>
                    <td>
                        <div class='d-flex flex-column align-items-center'><?= $columnValue ?></div>
                    </td>
                <? endif; ?>
            <? endforeach; ?>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
</div>

<script>
    paginationClass.initialize();
</script>
