<?php
if (!$ViewData['table']) {
    echo 'false';
    return;
}
?>
<div id="controlSize" class="teste table-responsive">
    <input type="hidden" id="total_page_number_dpl" value="<?php echo $ViewData['footer']['pages']; ?>"/>
    <input type="hidden" id="hdn_total_items_dpl" value="<?php echo $ViewData['footer']['count']; ?>"/>
    <table id="duplicateTable" class="table dataTable approval-table">
        <thead>
        <tr>
            <th></th>
            <?php
            foreach ($ViewData['table']['columns'] as $key => $value) {
                echo "
                <th>
                  <div class='d-flex flex-row pln-cur-pointer order-col-duplicate' id='$key'>";
                echo $lang['aprovacoes']['duplicacao'][$key];
                echo "
                          <i class='fa fa-sort";
                echo ($value['order'] == null) ? "" : "-" . $value['order'];
                echo " pln-link-color ml-1 align-self-end'></i>
                  </div>
                </th>";
            }
            ?>
        </tr>
        </thead>
        <tbody id="modalTable">
        <?php
        $tdDesc = "<div class='d-flex flex-column align-items-left'>";
        $tdCommon = "<div class='d-flex flex-column align-items-start'>";
        $i = 0;
        echo "<input type='hidden' name='id_psug_pk' value='{$value}'>";
        foreach ($ViewData['table']['data'] as $row) {
            echo "<tr>";
            echo "<td><a href='#' class='remover'><span class='text-danger'>X {$lang["global"]['delete']}</span></a></td>";
            foreach ($row as $key => $value) {
                switch ($key) {
                    case 'ID_PSUG_PK': ?> 
                      <input type='hidden' name='purchase_suggestions[<?= $i ?>][ID_PSUG_PK]' value="<?= $value ?>">
                      <? break;
                    case 'ID_COMPANY_FK': ?>
                        <input type='hidden' name='purchase_suggestions[<?= $i ?>][ID_COMPANY_FK]' value="<?= $value ?>">
                        <? break;
                    case 'COD_ITEM_PK': ?>
                        <td name="purchase_suggestions[<?= $i ?>][COD_ITEM_PK]" class='COD_ITEM_PK'><?= $value ?></td>
                        <? break;
                    case 'ID_SKU_PK':?>
                        <input type='hidden' class='ID_SKU_PK' name='purchase_suggestions[<?= $i ?>][ID_SKU_PK]' value="<?= $value ?>">
                        <?
                        echo "<td>{$value}</td>";
                        break;
                    case 'ID_INTEGRATION_GTP':
                        echo "<input type='hidden' name='id_integration' value='{$value}'>";
                        break;
                    case 'DESC_ITEM':
                        echo "<td>" . $value . "</td>";
                        break;
                    case 'QTY_ALTERED':
                        echo "<td>" . $tdCommon . "<input type='text' class='QTY_ALTERED form-control input_just_number form-control-sm pln-input-80' name='purchase_suggestions[". $i . "][QTY_ALTERED]' value='{$value}' size='30'></td>";
                        break;
                    case 'DATE_ARRIVAL_ALTERED':
                        $data = date('Y-m-d', strtotime($value));
                        echo "<td>" . $tdCommon . "<div class='fomr-control'><input type='date' min=" . date('Y-m-d') . " name='purchase_suggestions[". $i . "][DATE_ARRIVAL_ALTERED]' class='form-control DATE_ARRIVAL_ALTERED' value='{$data}'></div></td>";
                        break;
                    case 'OBS':
                        echo "<td>" . $tdCommon . "<input type='text' name='purchase_suggestions[". $i . "][OBS]' class='txtarea_obs form-control form-control-sm' id='dupl_obs' value='{$value}'></td>";
                        break;
                    case 'NUM_PARC_PK':
                      echo "<input type='hidden' name='num_parc_pk' value='{$value}'>";
                      break;
                }
            }
            echo "</tr>";
            $i++;
        }
        ?>
        </tr>
        </tbody>
        <tfoot>
        <input type="hidden" id="total_page_number_dpl" value="<?= $ViewData['footer']['pages'] ?>SKU's">
        <input type="hidden" id="items_per_page_dpl" value="10">
        <input type="hidden" name="estab" id="estab_form">
        </tfoot>
    </table>
</div>
</div>

<script>
    var id_company = "<?php echo $ViewData['user']['id_company_pk'];?>";
</script>
