<form action="/br/approvals/duplicate/save" id="form" method="POST">
    <div class="modal fade" id="modal_duplicate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
         aria-hidden="true" style="padding-right: 15px;">
        <div class="modal-dialog pln-modal-info modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span
                                aria-hidden="true"> &times;</span><small><?= $lang['modal']['fechar'] ?></small>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-box">
                        <div class="row">
                            <div class="col-md-11">
                                <h4><?= $lang['suggest']['duplicate'] ?></h4>
                            </div>
                            <div class="col-md-1">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?= $lang['global']['buyer_code'] ?>:</label>
                                    <select name="COD_BUYER" class="form-control" id="cod_buyer" required>
                                        <option id="default" value=""><?= $lang['global']['load_content'] ?></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?= $lang['global']['warehouse'] ?>:</label>
                                    <select class="form-control" name="COD_LOCAL_FK" id="deposit" required>
                                        <option id="default" value=""><?= $lang['global']['load_content'] ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?= $lang['suggest']['setor_code'] ?>:</label>
                                    <select name="COD_SECTOR" class="form-control" id="cod_sector" required>
                                        <option id="default" value=""><?= $lang['global']['load_content'] ?></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?= $lang['suggest']['expected_date'] ?>:</label>
                                    <input type="date" id="dt_exp_req" name="DT_EXP_REQ" min="<?php echo date('Y-m-d'); ?>"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?= $lang['suggest']['reason'] ?>:</label>
                                    <select class="form-control" id="cod_reason" name="COD_REQ_REASON" required>
                                        <option id="default" value=""><?= $lang['global']['load_content'] ?></option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <p><?= $lang['suggest']['mark_as_urgent'] ?>:</p>
                                    <div class="form-check form-check-inline">
                                        <input name="YN_URGENT" class="form-check-input" type="checkbox"
                                               value="option1">
                                        <label class="form-check-label"><?= $lang['global']['urgent'] ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?= $lang['global']['notes'] ?>:</label>
                                    <textarea name="OBS_REQ" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="controlSize" class="table-responsive">
                                    <div id="modaltable_load"
                                         class="row basicInformation pln-h-100 d-flex align-items-center"
                                         style="display: block !important;">
                                        <div class="col-sm-12 text-center d-flex justify-content-center">
                                            <div class="loader"></div>
                                        </div>
                                    </div>
                                    <div class="dplTableContainer"></div>
                                    <div class="mt-3">
                                        <button type="submit" class="btn pln-btn-orange float-right" id="saveDuplicate">
                                            <?= $lang['aprovacoes']['sugestoes']['SALVAR'] ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>