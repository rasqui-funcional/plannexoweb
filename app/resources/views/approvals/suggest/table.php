<?php
if (!$ViewData['table'] || count($ViewData['table']['data']) < 1) {
    echo 'false';
    return;
}
?>
<div id="controlSize" class="teste table-responsive">
    <input type="hidden" id="total_page_number" value="<?= $ViewData['footer']['pages']; ?>"/>
    <input type="hidden" id="hdn_total_items" value="<?= $ViewData['footer']['count']; ?>"/>
    <input type="hidden" id="cod_estab_pk" value="<?= $ViewData['table']['get_cod_estab']; ?>"/>
    
    <table class="table dataTable approval-table bigger-table">
        <thead>
        <tr>
            <?
            unset($ViewData['table']['columns']['RNUM']);
            foreach ($ViewData['table']['columns'] as $key => $value):
            ?>
            <th>
                <div class='d-flex flex-row pln-cur-pointer order-col' data-id='<?= $key ?>'>
                    <span data-toggle="tooltip"
                        title="<?= isset($lang['aprovacoes']['sugestoes']['abbr'][$key]) ? $lang['aprovacoes']['sugestoes'][$key] : '' ?>">
                        <?= (!isset($lang['aprovacoes']['sugestoes']['abbr'][$key]) ? $lang['aprovacoes']['sugestoes'][$key] : $lang['aprovacoes']['sugestoes']['abbr'][$key]) ?>
                    </span>
                    
                    <i class="fa fa-sort<?= $value['order'] == null ? "" : "-" . $value['order']?> pln-link-color ml-1 align-self-center"></i>
                </div>
            </th>
            <? endforeach; ?>
        </tr>
        </thead>
        <tfoot>
            <tr>
                <?php
                    foreach (array_keys($ViewData['table']['columns']) as $key) {
                        switch($key){
                            case 'QTY_ORIGINAL_VAL':
                            case 'QTY_ALTERED_VAL':
                                $ViewData['footer']['data'][$key] = $lang['global']['country_currency'] . ' ' . Utilities::dataFormatter($key, $ViewData['footer']['data'][$key]);
                            break;
                            case 'QTY_ALTERED':
                                $ViewData['footer']['data'][$key] = number_format($ViewData['footer']['data'][$key], 0, '', '.');
                            break;
                            case 'QTY_ORIGINAL':
                                $ViewData['footer']['data'][$key] = Utilities::dataFormatter($key, $ViewData['footer']['data'][$key]);    
                            break;
                            case 'ALTERED_INDEX':
                                $ViewData['footer']['data'][$key] = number_format($ViewData['footer']['data'][$key], 2, '.', '') . '%' ?? '-';
                            break;
                        }                        
                        printf('<th>%s</th>'
                            , $ViewData['footer']['data'][$key]
                        );
                    }
                ?>
            </tr>
        </tfoot>
        <tbody>
        <?
        foreach ($ViewData['table']['data'] as $rows):
            ?>
            <tr>
                <?
                unset($rows['RNUM']);
                foreach ($rows as $key => $value):
                    ?>
                    <td class='<?php echo $key == 'DESC_ITEM' ? 'text-truncate pln-td-mw-100' : '' ?>'>
                    <span data-toggle="tooltip" title="<?php echo $key == 'DESC_ITEM' ? $value : '' ?>">
                <?
                switch ($key):
                    case 'DATE_ARRIVAL_ORIGINAL':
                    case 'DATE_ARRIVAL_ALTERED':
                    case 'DATE_CREATED':
                    case 'DATE_APPROVED':
                        printf('%s'
                            , $value ? Utilities::dataFormatter($key, $value) : '-'
                        );
                        break;
                    case 'QTY_ORIGINAL_VAL':
                        echo $lang['global']['country_currency']  . ' ' . Utilities::dataFormatter('QTY_ORIGINAL_VAL', $value);
                        break;
                    case 'QTY_ALTERED_VAL':
                    case 'VAL_UNIT_ERP':
                        printf('%s'
                            , $value ? $lang['global']['country_currency']  . ' ' . Utilities::dataFormatter($key, $value) : '-'
                        );
                        break;
                    case 'QTY_ALTERED':
                        echo number_format($value, 0, '', '.');
                    break;
                    case 'QTY_ORIGINAL':
                        printf('%s'
                            , Utilities::dataFormatter($key, $value)
                        );
                        break;
                    case 'SIT_PSUG':
                        printf('%s', $lang['aprovacoes']['status'][$value]);
                        break;
                    case 'ALTERED_INDEX':
                        echo number_format($value, 2, '.', '') . '%' ?? '-';
                        break;
                    default:
                        echo $value ?? '-';
                        break;
                endswitch; ?>
                </span>
                </td>
                <? endforeach; ?>
            </tr>
        <? endforeach ?>
        </tbody>
    </table>
</div>

<script>
    paginationClass.initialize();
</script>