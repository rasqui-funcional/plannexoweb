<div class="content-i">
    <div class="content-full-table">
        <div class="element-wrapper">
            <!--HEADER BEGIN -->

            <div class="title-header d-flex flex-row align-items-end mt-3">
                <div class="w-100 mr-3">
                    <a class="pln-link-color" href="/<?=$ViewData['user']['lang']?>/approvals/list/<?=$ViewData['approvals_order'] ? urlencode($ViewData['approvals_order']) : ''?>" onclick="loaderShow()">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        <?= $lang['sugcompras_lista']['voltar_list'] ?>
                    </a>
                    <input type='hidden' id='reason' value="<?php echo $ViewData['header']['COD_REQ_REASON']; ?>">
                    <input type='hidden' id='buyer' value="<?php echo $ViewData['header']['COD_BUYER']; ?>">
                    <input type='hidden' id='sector' value="<?php echo $ViewData['header']['COD_SECTOR']; ?>">
                    <input type='hidden' id='deposit_code' value="<?php echo $ViewData['header']['COD_LOCAL_FK']; ?>">
                    <input type="hidden" id="cod_header_pk" value="<?php echo $ViewData['header']['COD_HEADER_PK']; ?>">
                    <input type="hidden" id="table_empty" value="<?php echo $ViewData['tempty']; ?>">
                    <input type="hidden" id="set_dt_exp_req" value="<?php echo Utilities::dataFormatter('DT_EXP_REQ', $ViewData['header']['DT_EXP_REQ']) ?>">
                    <div class="pln-font-xlarge pln-bold-600">
                        <?php echo $lang['aprovacoes']['sugestoes']['codigo_de_integracao']; ?>:
                        <?php echo $ViewData['header']['COD_HEADER_PK']; ?>
                    </div>

                    <div class="d-flex flex-row align-items-center">
                        <div class="d-flex">
                            <div class="pln-bold-600"><?php echo $lang['aprovacoes']['sugestoes']['status'] ?>:</div>
                            <div class="ml-1"><?php echo $lang['aprovacoes']['status'][$ViewData['header']['SIT_HEADER']]; ?></div>
                        </div>
                        <div class="ml-1 mr-1 pln-pipe"></div>
                        <div class="d-flex">
                            <div class="pln-bold-600"><?php echo $lang['aprovacoes']['sugestoes']['codigo_solicitacao'] ?>
                                :
                            </div>
                            <div class="ml-1"><?php echo Utilities::Unity($ViewData['header']['COD_HEADER_ERP']); ?></div>
                        </div>
                        <div class="ml-1 mr-1 pln-pipe"></div>
                        <div class="d-flex">
                            <div class="pln-bold-600"><?php echo $lang['aprovacoes']['sugestoes']['estabelecimento'] ?>
                                :
                            </div>
                            <div class="ml-1"><?php echo $ViewData['header']['DESC_ESTAB']; ?></div>
                            <input type="hidden" id="estab" value="<?php echo $ViewData['header']['DESC_ESTAB']; ?>">
                        </div>
                        <div class="ml-1 mr-1 pln-pipe"></div>
                        <div class="d-flex">
                            <div class="pln-bold-600"><?php echo $lang['aprovacoes']['sugestoes']['deposito'] ?>:</div>
                            <div class="ml-1"><?php echo $ViewData['header']['DESC_LOCAL']; ?></div>
                        </div>
                    </div>

                    <div class="w-100 mt-3 d-flex flex-row justify-content-between">
                        <div class="d-flex align-items-end">
                            <div class="d-flex flex-row align-items-end">
                                <span>
                                    <select class="form-control form-control-sm" id="seach_col">
                                        <? foreach($ViewData['filters'] as $col): ?>
                                            <option value="<?= $col?>"><?= $lang['aprovacoes']['sugestoes'][$col]?></option>
                                        <? endforeach; ?>                                
                                    </select>
                                </span>
                            </div>
                            <div class="d-flex flex-row align-items-end ml-1">
                                <span>
                                    <input type="number" class="form-control form-control-sm col" id="search_input"/>
                                    <select class="form-control form-control-sm hide" id="aux_estabs">
                                        <? foreach ($ViewData['estabs']['Estab'] as $key => $value): ?>
                                            <option value="<?= $value['code'] ?>"><?= $value['name'] ?></option>
                                        <? endforeach ?>
                                    </select>
                                    <select class="form-control form-control-sm hide" id="aux_status">
                                        <option value="1"><?= $lang['aprovacoes']['status'][1] ?></option>
                                        <option value="2"><?= $lang['aprovacoes']['status'][2] ?></option>
                                        <option value="3"><?= $lang['aprovacoes']['status'][3] ?></option>
                                    </select>
                                </span>
                                <span class="ml-1">
                                    <button type="button" class="btn pln-btn-ghost" id="search"><?= $lang['parametros']['buscar'] ?></button>
                                </span>
                            </div>
                        </div>
                        <div class="d-flex align-items-end">
                            <div class="d-flex flex-row">
                                <div class="d-flex flex-column mr-3 justify-content-center">
                                    <span id="download" class="pln-link-color pln-cur-pointer">
                                        <i class="fas fa-file-csv"></i> <?php echo $lang['capa_pedido_parcela']['download'] ?>
                                    </span>
                                </div>
                                <button type="button" class="btn pln-btn-ghost" id="duplicate"/>
                                <i class="fa fa-clone"></i> <?php echo $lang['aprovacoes']['sugestoes']['duplicar'] ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIM -->

            <div class="element-box-full">
                <div id="table_insert"></div>

                <div class="d-flex mt-1 align-items-start pln-items-search" style="display: none !important"
                     id="pln-pagination-footer">
                    <div class="d-flex flex-row align-items-center ml-3 mr-3">
                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?= $lang['sugcompras_lista']['pagina']['qtd_a'] ?></span>
                            <span id="page_items_to"></span>
                            <span><?= $lang['sugcompras_lista']['pagina']['qtd_de'] ?></span>
                            <span id="page_items_total"></span>
                        </span>
                        <div class="d-flex flex-row">
                            <div class="d-flex flex-column mr-3 justify-content-center">
                                <small>
                                    <span id="total_items"></span> <?php echo $lang['aprovacoes']['aprovacoes']; ?>
                                </small>
                                <small>
                                    <?php echo $lang['aprovacoes']['pagina']['qtd_total']?> <span id="spn_total_page_number"></span>
                                </small>
                            </div>

                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>
                        </div>
                    </div>

                    <div class="d-flex flex-row align-items-center">
                        <span class="pln-bold-600"><?=$lang['pagination']['page']?></span>
                        <span class="ml-1">
                            <input type="text" maxlength="10" id="goto_pagenumber" class="form-control input_just_number form-control-sm pln-input-60"/>
                        </span>
                        <span class="ml-1">
                            <button type="button" id="goToPageNumber" class="btn pln-btn-ghost"><?= $lang['sugcompras_lista']['buscar'] ?></button>
                        </span>
                    </div>

                    <div class="d-flex flex-row align-items-center ml-auto mr-3">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['visualizar']; ?></span>

                        <span class="ml-1">
                                <select id="items_per_page" class="form-control form-control-sm">
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                            </span>

                        <span class="ml-1"><?php echo $lang['sugcompras_lista']['por_pagina']; ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once 'duplicate.php'; ?>

<?php
$JS = ['uglify/all-approvals-sugg-list-min'];
?>
