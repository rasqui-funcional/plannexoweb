<?php 
$JS= ['uglify/all-calendar-list-min'];
?>

<div class="content-i">
    <div class="content-full-table">
        <div class="element-wrapper">
            <div class="title-header d-flex flex-row justify-content-between align-items-end mt-3 mx-3">
                <div>
                    <h4 class="pln-font-color"><?php echo $lang['calendar']['titulo'] ?></h4>
                </div>
                <div>
                    <button id="new_calendar" class="btn pln-btn-orange"><?=$lang['parametros']['novo']?></button>
                </div>
            </div>

            <div class="element-box-full">
                <div class="d-flex align-items-end pln-items-search mx-3">
                    <div class="d-flex flex-row align-items-end ml-1">
                        <div class="d-flex flex-column align-items-start">
                            <span class="pln-bold-600"><?=$lang['global']['search']?>:</span>
                            <span><input type="text" class="form-control form-control-sm col" id="search_input"></span>
                        </div>
                        <span class="ml-1"><button type="button" id="calendarSearch" class="btn pln-btn-ghost"><?=$lang['global']['search']?>
                    </button></span></div>

                    <span class="ml-auto">
                        <button type="button" disabled id="parameters_calendar_remove" class="btn btn-outline-danger"><b>X</b> <?=$lang['global']['delete']?></button>
                        <button type="button" disabled id="parameters_calendar_duplicate" class="btn pln-btn-ghost"><i class="fa fa-clone"></i> <?=$lang['global']['duplicate']?> </button>
                    </span>
                </div>
                <div id="table_insert">
                </div>
                <div class="d-flex mt-1 align-items-center pln-items-search" style="display: none !important">
                    <div class="d-flex flex-row align-items-center ml-3 mr-3">
                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_a']; ?></span>
                            <span id="page_items_to"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_de']; ?></span>
                            <span id="page_items_total"></span>
                        </span>
                        <div class="d-flex flex-row">
                            <div class="d-flex flex-column mr-3">
                                <small>
                                    <?php echo $lang['sugcompras_lista']['itens']['total'] ?> <span id="total_items"></span>
                                    <?php echo $lang['calendar']['list']; ?>
                                </small>
                                <small>
                                <?php echo $lang['sugcompras_lista']['pagina']['qtd_total']; ?> <span
                                    id="spn_total_page_number"></span>
                                </small>
                            </div>
                            
                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>
                           
                        </div>
                    </div>

                    <div class="d-flex flex-row align-items-center ml-3">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['pagina']['titulo']; ?></span>
                        <span class="ml-1"><input type="text" maxlength="10" id="goto_pagenumber"
                                class="form-control form-control-sm input_just_number form-control form-control-sm-sm pln-input-60" /></span>
                        <span class="ml-1"><button type="button" id="goToPageNumber"
                                class="btn pln-btn-ghost" /><?php echo $lang['sugcompras_lista']['buscar']; ?></span>
                    </div>
                    <div class="d-flex flex-row align-items-center ml-auto mr-3">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['visualizar']; ?></span>
                        <span class="ml-1">
                            <select id="items_per_page" class="form-control form-control-sm">
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </span>
                        <span class="ml-1"><?php echo $lang['sugcompras_lista']['por_pagina']; ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once('modal.php'); ?>