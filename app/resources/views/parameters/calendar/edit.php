<div class="content-i">
    <div class="content-box">
        <div style="" class="element-wrapper">
            <a href="/<?php echo $Country; ?>/resume/filters" class="pln-link-color"><i class="fa fa-angle-double-left"
                    aria-hidden="true"></i><?php echo $lang['parametros']['voltar'] ?></a>
            <div class="d-flex flex-row justify-content-between align-items-center">
                <div>
                    <h4 class="pln-font-color"><?php echo $lang['calendar']['titulo'] ?></h4>
                </div>
                <div class="d-flex flex-column">
                    <button type="button" id="saveData"
                        class="btn pln-btn-orange"><?php echo $lang['parametros']['salvar'] ?></button>
                </div>
            </div>
            <div class="element-header"></div>

            <div class="element-box">
                <div id="table_insert">
                </div>
                <div class="d-flex mt-1 align-items-center pln-items-search" style="display: none !important">
                    <div class="d-flex flex-row align-items-center">
                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_a']; ?></span>
                            <span id="page_items_to"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_de']; ?></span>
                            <span id="page_items_total"></span>
                        </span>
                        <div class="">
                            <small>
                                <?php echo $lang['sugcompras_lista']['itens']['total'] ?> <span id="total_items"></span>
                                <?php echo $lang['politics']['politicas']; ?>
                            </small>
                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>
                            <small>
                                <?php echo $lang['sugcompras_lista']['pagina']['qtd_total']; ?> <span
                                    id="spn_total_page_number"></span>
                            </small>
                        </div>
                    </div>

                    <div class="d-flex flex-row align-items-center ml-3">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['pagina']['titulo']; ?></span>
                        <span class="ml-1"><input type="text" maxlength="10" id="goto_pagenumber"
                                class="form-control form-control-sm input_just_number form-control form-control-sm-sm pln-input-60" /></span>
                        <span class="ml-1"><button type="button" id="goToPageNumber"
                                class="btn pln-btn-orange" /><?php echo $lang['sugcompras_lista']['buscar']; ?></span>
                    </div>
                    <div class="d-flex flex-row align-items-center ml-auto">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['visualizar']; ?></span>
                        <span class="ml-1">
                            <select id="items_per_page" class="form-control form-control-sm">
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </span>
                        <span class="ml-1"><?php echo $lang['sugcompras_lista']['por_pagina']; ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>