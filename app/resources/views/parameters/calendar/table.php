<form id="formulario">
    <div id="controlSize" class="table-responsive">
        <table id="tableCalendarList" class="table dataTable resume-table object_table calendar-table bigger-table">
            <?php addComponent('calendar/thead.php') ?>
            <?php addComponent('calendar/calendars.php') ?>
            <?php addComponent('calendar/tfoot.php') ?>
        </table>
    </div>
</form>
<script>
    classe.initialize();
    paginationClass.initialize();
</script>
