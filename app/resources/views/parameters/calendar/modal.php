<input type="hidden" id="feriados" value="01-0-Ano Novo, 21-03-Tiradentes, 1-04-Dia do trabalhador, 02-10-Finados, 15-10-Proclamação da República, 25-11-Natal">
<input type="hidden" id="calendar_id" value="">
<div class="modal fade" id="calendar_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog pln-modal-info modal-lg">
        <div class="modal-content" style="background-color: #eaebef;">
            <div class="modal-header">
                <h4><?=$lang['calendar']['edit_calendar']?></h4>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span>
                    <small><?= $lang['modal']['fechar'] ?></small>
                </button>
            </div>
            <div class="modal-body">
                <div class="element-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="element-box">
                                <div class="row d-flex flex-row align-items-start justify-content-between">
                                    <div>
                                        <span class="pln-bold-600"><?=$lang['global']['title']?></span>
                                        <span><input type="text" id="desc_calendar" class="form-control form-control-sm col"></span>
                                    </div>
                                    <div>
                                        <button id="save" class="btn pln-btn-orange"><?=$lang['global']['save']?></button>
                                        <button id="update" class="btn pln-btn-orange"><?=$lang['global']['save_editions']?></button>
                                    </div>
                                </div>

                                <div class="row mt-4 d-flex flex-row justify-content-between">
                                    <div class="d-flex flex-row">
                                        <span class="pln-bold-600 mr-1 text-uppercase"><?=$lang['column']['avg_days_interval']?>:</span>
                                        <div class="mr-1">14</div>
                                        <div><?=$lang['global']['days']?></div>
                                    </div>
                                    <span class="pln-link-color pln-cur-pointer" id="clear_calendar"><?=$lang['calendar']['clear']?></span>
                                </div>

                                <div id="calendar" class="calendar"></div>
                                <div class="row d-flex flex-column align-items-start">
                                    <span class="mt-4 pln-bold-600"><?=$lang['global']['automatic_filling']?></span>
                                    <div class="d-flex flex-row align-items-center">
                                        <span class="mr-2">
                                            <select id="period_interval" class="form-control form-control-sm">
                                                <option value="W"><?=$lang['calendar']['weekly']?></option>
                                                <option value="M"><?=$lang['calendar']['monthly']?></option>
                                            </select>
                                        </span>
                                        <span class="mr-2 weekly-each-text">
                                            <?=$lang['calendar']['each']?>
                                        </span>
                                        <span class="mr-2 weekly-numbers">
                                            <select id="each_week" class="form-control form-control-sm">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </span>

                                        <span class="mr-2 weekly-week-text">
                                            <?=$lang['calendar']['weeks']?>
                                        </span>

                                        <span class="mr-2 monthly-all-text pln-invi">
                                            <?=$lang['calendar']['all']?>
                                        </span>
                                        <span class="mr-2 monthly-day-week pln-invi">
                                            <select id="day_or_week" class="form-control form-control-sm">
                                                <option value="D"><?=$lang['global']['day']?></option>
                                                <option value="W"><?=$lang['global']['week']?></option>
                                            </select>
                                        </span>

                                        <span class="mr-2 monthly-input pln-invi">
                                            <input id="day_of_month" type="text" class="form-control form-control-sm">
                                        </span>

                                        <span class="monthly-month-text pln-invi">
                                            <?=$lang['global']['of']?> <?=$lang['global']['month']?>
                                        </span>
                                    </div>
                                </div>
                                <div class="row d-flex flex-column align-items-start">
                                    <span class="mt-4 pln-bold-600 monthly-repeat-text"><?=$lang['global']['repeat']?></span>
                                    <span class="mt-2 pln-invi monthly-repeat-select">
                                        <select class="form-control form-control-sm" id="repeat_week">
                                            <option value="1"><?=$lang['calendar']['first']?></option>
                                            <option value="2"><?=$lang['calendar']['segunda']?></option>
                                            <option value="3"><?=$lang['calendar']['third']?></option>
                                            <option value="4"><?=$lang['calendar']['quarta']?></option>
                                            <option value="5"><?=$lang['calendar']['quinta']?></option>
                                        </select>
                                    </span>
                                    <span class="mt-2 monthly-repeat-buttons">                                        
                                        <button
                                            data-toggle="tooltip"
                                            title="<?php echo $lang['calendar']['segunda'] ?>"
                                            value="monday"
                                            data-day-of-week="1"
                                            type="button"
                                            style="border-radius: 15px; width: 30px; height: 30px; padding: 6px 0px; background-color: #999999; border: none; color: white"
                                            class="btn btn-week"
                                        >
                                            <?=substr($lang['calendar']['segunda'], 0, 1);?>
                                        </button>
                                        <button
                                            data-toggle="tooltip"
                                            title="<?php echo $lang['calendar']['terca'] ?>"
                                            value="tuesday"
                                            data-day-of-week="2"
                                            type="button"
                                            style="border-radius: 15px; width: 30px; height: 30px; padding: 6px 0px; background-color: #999999; border: none; color: white"
                                            class="btn btn-week"
                                        >
                                            <?=substr($lang['calendar']['terca'], 0, 1);?>
                                        </button>
                                        <button
                                            data-toggle="tooltip"
                                            title="<?php echo $lang['calendar']['quarta'] ?>"
                                            value="wednesday"
                                            data-day-of-week="3"
                                            type="button"
                                            style="border-radius: 15px; width: 30px; height: 30px; padding: 6px 0px; background-color: #999999; border: none; color: white"
                                            class="btn btn-week"
                                        >
                                            <?=substr($lang['calendar']['quarta'], 0, 1);?>
                                        </button>
                                        <button
                                            data-toggle="tooltip"
                                            title="<?php echo $lang['calendar']['quinta'] ?>"
                                            value="thursday"
                                            data-day-of-week="4"
                                            type="button"
                                            style="border-radius: 15px; width: 30px; height: 30px; padding: 6px 0px; background-color: #999999; border: none; color: white"
                                            class="btn btn-week"
                                        >
                                            <?=substr($lang['calendar']['quinta'], 0, 1);?>
                                        </button>
                                        <button
                                            data-toggle="tooltip"
                                            title="<?php echo $lang['calendar']['sexta'] ?>"
                                            value="friday"
                                            data-day-of-week="5"
                                            type="button"
                                            style="border-radius: 15px; width: 30px; height: 30px; padding: 6px 0px; background-color: #999999; border: none; color: white"
                                            class="btn btn-week"
                                        >
                                            <?=substr($lang['calendar']['sexta'], 0, 1);?>
                                        </button>
                                    </span>
                                </div>
                                
                                <div class="row d-flex flex-column align-items-start mt-4">
                                    <span class="mt-4 pln-bold-600"><?=$lang['global']['ends']?></span>

                                    <div class="mt-2 d-flex flex-row align-items-end">
                                        <div class="radio mr-2">
                                            <input type="radio" id="ends_in_ten_years" checked name="ends_in" id="parameters_calendar_ends_on_ten_years">
                                            <label for="parameters_calendar_ends_on_ten_years"><?=$lang['global']['in']?> 10 <?=$lang['global']['years']?></label>
                                        </div>
                                    </div>


                                    <div class="mt-2 d-flex flex-row align-items-end">
                                        <div class="radio mr-2">
                                            <input type="radio" id="ends_in_period" name="ends_in" id="parameters_calendar_ends_on_date">
                                            <label for="parameters_calendar_ends_on_date"><?=$lang['global']['in']?></label>
                                        </div>
                                        <span>
                                            <input id="ends_in" val="0" class="form-control form-control-sm" type="date" />
                                        </span>
                                    </div>
                                    
                                </div>
                                <div class="row mt-3">
                                    <div class="">
                                        <button id="exec" class="btn pln-btn-orange"><?=$lang['calendar']['fill']?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
