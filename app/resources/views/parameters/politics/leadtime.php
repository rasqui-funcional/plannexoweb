<div class="col-sm-12">
    <div class="element-box leadtime_div">
        <div class="row">
            <div class="col-sm-12 d-flex flex-row justify-content-between">
                <div class="d-flex flex-column">
                    <span class="pln-bold-600"><?php echo $lang['politics']['planejador'] ?></span>
                    <div class="hide_manual" id="LT_PLANNER_DIV">
                        <label for="LT_PLANNER"><?php echo $lang['politics']['valor_manual_dias'] ?></label>
                        <input type="text" id="LT_PLANNER" name="LT_PLANNER" class="form-control leadtime">
                    </div>
                    
                    <div class="hide_automatic pln-invi" id="LT_PLANNER_EXCEPTION_DIV">
                        <label for="LT_PLANNER_EXCEPTION"><?php echo $lang['politics']['excecao'] ?></label>
                        <input type="text" id="LT_PLANNER_EXCEPTION" name="LT_PLANNER_EXCEPTION" class="form-control leadtime">
                    </div>
                    <small class="hide_automatic pln-invi">
                        <?php echo $lang['politics']['msg_calc'] ?>
                    </small>
                    <div class="d-flex flex-row align-items-center mt-1">
                        <input id="LT_PLANNER_CHK" name="LT_PLANNER_CHK" type="checkbox" class="auto_erp">
                        <label for="LT_PLANNER_CHK" class="ml-1"><?php echo $lang['politics']['automatico_erp'] ?></label>
                    </div>
                </div>

                <div class="d-flex flex-column">
                    <span class="pln-bold-600"><?php echo $lang['politics']['comprador'] ?></span>
                    
                    <div class="hide_manual" id="LT_BUYER_DIV">
                        <label for="LT_BUYER"><?php echo $lang['politics']['valor_manual_dias'] ?></label>
                        <input type="text" id="LT_BUYER" name="LT_BUYER" class="form-control leadtime">
                    </div>

                    <div class="hide_automatic pln-invi" id="LT_BUYER_EXCEPTION_DIV">
                        <label for="LT_BUYER_EXCEPTION"><?php echo $lang['politics']['excecao'] ?></label>
                        <input type="text" id="LT_BUYER_EXCEPTION" name="LT_BUYER_EXCEPTION" class="form-control leadtime">
                    </div>

                    <small class="hide_automatic pln-invi">
                        <?php echo $lang['politics']['msg_calc'] ?> 
                    </small>
                    <div class="d-flex flex-row align-items-center mt-1">
                        <input id="LT_BUYER_CHK" name="LT_BUYER_CHK" type="checkbox" class="auto_erp">
                        <label for="LT_BUYER_CHK" class="ml-1"><?php echo $lang['politics']['automatico_erp'] ?></label>
                    </div>
                </div>

                <div class="d-flex flex-column">
                    <span class="pln-bold-600"><?php echo $lang['politics']['fornecedor'] ?></span>
                    
                    <div class="hide_manual" id="LT_SUPPLIER_DIV">
                        <label for=""><?php echo $lang['politics']['valor_manual_dias'] ?></label>
                        <input type="text" id="LT_SUPPLIER" name="LT_SUPPLIER" class="form-control leadtime">
                    </div>

                    <div class="hide_automatic pln-invi" id="LT_SUPPLIER_EXCEPTION_DIV">
                        <label for="LT_SUPPLIER_EXCEPTION"><?php echo $lang['politics']['excecao'] ?></label>
                        <input type="text" id="LT_SUPPLIER_EXCEPTION" name="LT_SUPPLIER_EXCEPTION" class="form-control leadtime">
                    </div>
                    
                    <small class="hide_automatic pln-invi">
                        <?php echo $lang['politics']['msg_calc'] ?> 
                    </small>
                    <div class="d-flex flex-row align-items-center mt-1">
                        <input id="LT_SUPPLIER_CHK" name="LT_SUPPLIER_CHK" type="checkbox" class="auto_erp">
                        <label for="LT_SUPPLIER_CHK" class="ml-1"><?php echo $lang['politics']['automatico_erp'] ?></label>
                    </div>                                            
                </div>

                <div class="d-flex flex-column">
                    <span class="pln-bold-600"><?php echo $lang['politics']['recebimento'] ?></span>
                
                    <div class="hide_manual" id="LT_RECEIVING_DIV">
                        <label for="LT_RECEIVING"><?php echo $lang['politics']['valor_manual_dias'] ?></label>
                        <input type="text" id="LT_RECEIVING" name="LT_RECEIVING" class="form-control leadtime">
                    </div>

                    <div class="hide_automatic pln-invi" id="LT_RECEIVING_EXCEPTION_DIV">
                        <label for=""><?php echo $lang['politics']['excecao'] ?></label>
                        <input type="text" id="LT_RECEIVING_EXCEPTION" name="LT_RECEIVING_EXCEPTION" class="form-control leadtime">
                    </div>

                    <small class="hide_automatic pln-invi">
                        <?php echo $lang['politics']['msg_calc'] ?> 
                    </small>
                    <div class="d-flex flex-row align-items-center mt-1">
                        <input id="LT_RECEIVING_CHK" name="LT_RECEIVING_CHK" type="checkbox" class="auto_erp">
                        <label for="LT_RECEIVING_CHK" class="ml-1"><?php echo $lang['politics']['automatico_erp'] ?></label>
                    </div>
                </div>
                
                <div class="d-flex flex-column">
                    <span class="pln-bold-600"><?php echo $lang['politics']['outros'] ?></span>
                    
                    <div class="hide_manual" id="LT_OTHERS_DIV">
                        <label for="LT_OTHERS"><?php echo $lang['politics']['valor_manual_dias'] ?></label>
                        <input type="text" id="LT_OTHERS" name="LT_OTHERS" class="form-control leadtime">
                    </div>
                    
                    <div class="hide_automatic pln-invi" id="LT_OTHERS_EXCEPTION_DIV">
                        <label for="LT_OTHERS_EXCEPTION"><?php echo $lang['politics']['excecao'] ?></label>
                        <input type="text" id="LT_OTHERS_EXCEPTION" name="LT_OTHERS_EXCEPTION" class="form-control leadtime">
                    </div>

                    <small class="hide_automatic pln-invi">
                        <?php echo $lang['politics']['msg_calc'] ?> 
                    </small>
                    <div class="d-flex flex-row align-items-center mt-1">
                        <input id="LT_OTHERS_CHK" name="LT_OTHERS_CHK" type="checkbox" class="auto_erp">
                        <label for="LT_OTHERS_CHK" class="ml-1"><?php echo $lang['politics']['automatico_erp'] ?></label>
                    </div>
                </div>
            </div>                                    
        </div>

        <div class="mt-3">
            <input type="checkbox" name="M_LT" id="M_LT">
            <label for="M_LT">As sugestões de compra sempre devem respeitar o lead time</label>
        </div>
    </div>
</div>