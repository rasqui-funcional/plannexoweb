<div class="col-sm-6">
    <div id="stock_min_div" class="element-box pln-h-100">
        <div class="row">
            <div class="col-sm-2"><?php echo $lang['politics']['automatico'] ?></div>
            <div class="col-sm-3 ">
                <span class="hide_automatic pln-invi"><?php echo $lang['politics']['limitador_minimo'] ?></span> 
                <span class="hide_manual"><?php echo $lang['politics']['min_dias'] ?></span> 
            </div>
            <div class="col-sm-3  hide_automatic pln-invi"><?php echo $lang['politics']['limitador_maximo'] ?></div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <input name="DAYS_MIN_CHK" id="DAYS_MIN_CHK" type="checkbox">
            </div>
            <div class="col-sm-3 pln-invi hide_automatic">
                <div class="d-flex flex-row align-items-center">
                    <input type="text" name="MIN_LIMIT_BOTTOM" class="form-control mr-1 onlynumber">
                    <span><?php echo $lang['politics']['dias'] ?></span>
                </div>
            </div>
            <div class="col-sm-3 hide_manual">
                <div class="d-flex flex-row align-items-center">
                    <input type="text" name="DAYS_MIN" class="form-control mr-1 onlynumber">
                    <span><?php echo $lang['politics']['dias'] ?></span>
                </div>
            </div>
            <div class="col-sm-3 hide_automatic pln-invi">
                <div class="d-flex flex-row align-items-center">
                    <input type="text" name="MAX_LIMIT_BOTTOM" class="form-control mr-1 onlynumber">
                    <span><?php echo $lang['politics']['dias'] ?></span>
                </div>
            </div>
        </div>

        <div class="row mt-3 hide_automatic pln-invi">
            <div class="col-sm-8">
                <div class="form-group">
                    <label for=""><?php echo $lang['politics']['nivel_servico_multiplicador'] ?></label>
                    <select name="SERVICE_LEVEL"  class="form-control" id="service_level">
                        <option value=""></option>
                        <option value="99.99"><?php echo $lang['politics']['nivel_servico_99.99'] ?></option>
                        <option value="99.98"><?php echo $lang['politics']['nivel_servico_99.98'] ?></option>
                        <option value="99.97"><?php echo $lang['politics']['nivel_servico_99.97'] ?></option>
                        <option value="99.96"><?php echo $lang['politics']['nivel_servico_99.96'] ?></option>
                        <option value="99.95"><?php echo $lang['politics']['nivel_servico_99.95'] ?></option>
                        <option value="99.9"><?php echo $lang['politics']['nivel_servico_99.9'] ?></option>
                        <option value="99"><?php echo $lang['politics']['nivel_servico_99'] ?></option>
                        <option value="98"><?php echo $lang['politics']['nivel_servico_98'] ?></option>
                        <option value="97"><?php echo $lang['politics']['nivel_servico_97'] ?></option>
                        <option value="96"><?php echo $lang['politics']['nivel_servico_96'] ?>
                        <option value="95"><?php echo $lang['politics']['nivel_servico_95'] ?>
                        <option value="94"><?php echo $lang['politics']['nivel_servico_94'] ?>
                        <option value="93"><?php echo $lang['politics']['nivel_servico_93'] ?>
                        <option value="90"><?php echo $lang['politics']['nivel_servico_90'] ?>
                        <option value="87.5"><?php echo $lang['politics']['nivel_servico_87.5'] ?>
                        <option value="85"><?php echo $lang['politics']['nivel_servico_85'] ?>
                        <option value="80"><?php echo $lang['politics']['nivel_servico_80'] ?>
                        <option value="70"><?php echo $lang['politics']['nivel_servico_70'] ?>
                        <option value="60"><?php echo $lang['politics']['nivel_servico_60'] ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="mt-3">
            <input type="checkbox" name="SIT_MIN_MAX_ADD_ERP" id="SIT_MIN_MAX_ADD_ERP">
            <label for="SIT_MIN_MAX_ADD_ERP">Adicionar mínimo e máximo do ERP</label>
        </div>
    </div>
</div>