<?php 
$JS= [
    'uglify/all-politics-list-min',
     "i18n/{$Country}/police",
];
?>

<div class="content-i">
    <div class="content-full-table">
        <div class="element-wrapper">
            <!--<a href="/<?php #echo $Country; ?>/resume/filters" class="pln-link-color"><i class="fa fa-angle-double-left" aria-hidden="true"></i> <?php #echo $lang['parametros']['voltar'] ?></a>-->
            <div class="d-flex flex-row justify-content-between align-items-center mt-3 mx-3">
                <div>
                    <h4 class="pln-font-color"><?php echo $lang['politics']['titulo'] ?></h4>
                </div>
                <div class="d-flex flex-column">
                    <button type="button" id="new_politic" class="btn pln-btn-orange"><?php echo $lang['parametros']['novo'] ?></button>
                </div>
            </div>
            <div class="d-flex align-items-end pln-items-search mb-1 mx-3" style="display: none !important">
                <div class="d-flex flex-column align-items-start">
                    <span class="pln-bold-600" ><?php echo $lang['sugcompras_lista']['busca']; ?></span>
                    <span>
                        <select class="form-control form-control-sm" id="seach_col">                                
                            <option value="cod_profile"><?php echo $lang['politics']['cod_profile'] ?></option>
                            <option value="desc_profile"><?php echo $lang['politics']['desc_profile'] ?></option>
                            <option value="desc_calendar"><?php echo $lang['politics']['desc_calendar'] ?></option>
                        </select>
                    </span>
                </div>
                <div class="d-flex flex-row align-items-end ml-1">
                    <span><input type="text" class="form-control form-control-sm col" id="search_input" /></span>
                    <span class="ml-1"><button type="button" id="politcsSearch" class="btn pln-btn-ghost" /><?php echo $lang['sugcompras_lista']['buscar']; ?></span>
                </div>
                <span class="ml-auto">
                    <button type="button" id="duplicate" class="btn pln-btn-ghost"><i class="fa fa-clone"></i> <?php echo $lang['politics']['duplicar']; ?> </button>
                </span>
            </div>
            
            <div class="element-box-full">                
                <div id="table_insert">
                    
                </div>
                <div class="d-flex mt-1 align-items-center pln-items-search" style="display: none !important">
                    
                    <div class="d-flex flex-row align-items-center ml-3 mr-3">
                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_a']; ?></span>
                            <span id="page_items_to"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_de']; ?></span>
                            <span id="page_items_total"></span>
                        </span>
                        <div class="d-flex flex-row">
                            <div class="d-flex flex-column mr-3">
                                <small>                            
                                    <?php echo $lang['sugcompras_lista']['itens']['total'] ?> <span id="total_items"></span> <?php echo $lang['politics']['politicas']; ?>
                                </small>
                                 <small>
                                     <?php echo $lang['sugcompras_lista']['pagina']['qtd_total']; ?> <span id="spn_total_page_number"></span>
                                 </small> 
                            </div>
                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>
                        </div>
                    </div>
                                            
                    <div class="d-flex flex-row align-items-center ml-3">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['pagina']['titulo']; ?></span>
                        <span class="ml-1"><input type="text" maxlength="10" id="goto_pagenumber" class="form-control form-control-sm input_just_number form-control form-control-sm-sm pln-input-60" /></span>
                        <span class="ml-1"><button type="button" id="goToPageNumber" class="btn pln-btn-ghost" /><?php echo $lang['sugcompras_lista']['buscar']; ?></span>
                    </div>
                    <div class="d-flex flex-row align-items-center ml-auto mr-3">
                        <span class="pln-bold-600"><?php echo $lang['sugcompras_lista']['visualizar']; ?></span>
                        <span class="ml-1">
                            <select id="items_per_page" class="form-control form-control-sm">
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </span>
                        <span class="ml-1"><?php echo $lang['sugcompras_lista']['por_pagina']; ?></span>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div> 
<!-- EDIT MODAL -->
<div class="modal fade" id="modal_politic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog pln-modal-info modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ml-3" id="politic-modal-action"><?php echo $lang['politics']['editar'] ?></h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span><small><?=$lang['modal']['fechar']?></small></button>
            </div>
            <div class="modal-body">
                <div class="element-content">
                    <form name="politic_form" id="politic_form" method="post" action="/<?=$Country?>/parameters/politics/update">
                    <input type="hidden" id="edit_id_profile_pk" name="ID_PROFILE_PK">
                    <div class="row">
                        <div class="col-sm-12 d-flex justify-content-end align-items-center">
                            <span class="pln-danger pln-cur-pointer mr-3" id="confirm_delete" style="display: inline;"><i class="fa fa-times pln-danger"></i> <span><?php echo $lang['politics']['deletar']; ?></span></span>
                            <button type="button" id="save_new" class="btn pln-btn-orange btn-post"><?php echo $lang['politics']['salva_novo'] ?></button>
                            <button type="button" id="update" class="btn pln-btn-orange btn-post"><?php echo $lang['politics']['salva_alteracao'] ?></button>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6"><span class="pln-bold-600"><?php echo $lang['politics']['infos_basicas'] ?></span></div>
                        <div class="col-sm-6"><span class="pln-bold-600"><?php echo $lang['politics']['estoque_min'] ?></span></div>
                    </div>

                    <div class="row">
                        <?php include("basic_info.php"); ?>
                        <?php include("stock_min.php"); ?>
                    </div>

                    <!-- CALENDÁRIO -->
                    <div class="row mt-4">
                        <div class="col-sm-12"><span class="pln-bold-600"><?php echo $lang['politics']['calendario'] ?></span></div>
                    </div>

                    <div class="row">
                        <?php include("calendar.php"); ?>
                    </div>
                    <!-- CALENDÁRIO FIM-->

                    <!-- LEAD TIME -->
                    <div class="row mt-3">
                        <div class="col-sm-12"><span class="pln-bold-600"><?php echo $lang['politics']['lt'] ?></span></div>
                    </div>
                    <div class="row">
                        <?php include("leadtime.php"); ?>
                    </div>
                    <!-- LEAD TIME FIM-->
                    </form>
                </div>
                <!-- element-content end --> 
            </div>
            <!-- modal-body end -->
        </div>
    </div>
</div>
<!-- modal_politic end -->

