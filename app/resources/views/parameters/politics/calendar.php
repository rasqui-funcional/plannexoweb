<div class="col-sm-12">
    <div class="element-box">
        <div class="row">
            
                <div class="col-sm-2">
                    <select name="ID_CALENDAR_FK" class="form-control" id="calendars">
                        
                    </select>
                </div>
                <div class="d-flex flex-row align-items-center col-sm-2">
                    <?php echo $lang['politics']['lote_compra']  ?> <span id="purchase_count"></span> 
                </div>
                <div class="d-flex flex-row align-items-center col-sm-2">
                    <input name="SIT_ONLY_FIRST_PARC" id="SIT_ONLY_FIRST_PARC" value="1" type="checkbox">
                    <label for="SIT_ONLY_FIRST_PARC"><?php echo $lang['politics']['somente_primiera_parc'] ?></label>
                </div>
                <div id="DAYS_HORIZON_DIV" class="col-sm-4">
                    <div class="d-flex flex-row align-items-center">
                        <span><?php echo $lang['politics']['horiz_programacao'] ?></span>
                        <input type="text" id="DAYS_HORIZON" name="DAYS_HORIZON" class="form-control col-sm-2 ml-2 mr-2 onlynumber"> 
                        <span><?php echo $lang['politics']['dias'] ?></span> 
                    </div>
                </div>
        </div>

        <!-- CALENDÁRIO UI -->
        <div id="politcs_calendar" class="row mt-4">
        </div>
        <!-- CALENDÁRIO UI FIM-->

        <div class="mt-3">
            <input type="checkbox" name="M_CALENDAR" id="M_CALENDAR">
            <label for="M_CALENDAR">As sugestões de compra sempre devem respeitar o calendário de entrega</label>
        </div>
    </div>
</div>
