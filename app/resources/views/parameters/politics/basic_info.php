<div class="col-sm-6">
    <div class="element-box pln-h-100">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for=""><?php echo $lang['politics']['cod_profile'] ?></label>
                    <input type="text" maxlength="20" id="COD_PROFILE" name="COD_PROFILE" class="form-control">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for=""><?php echo $lang['politics']['descricao'] ?></label>
                    <input type="text" name="DESC_PROFILE" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12"><?php echo $lang['politics']['depo_disp'] ?> <span id="deposits_count"></span>
            <span onclick="hide_filter('ipt_find_deposito')" class="pln-cur-pointer">
                    <i class="fa fa-search pln-link-color"></i>
            </span>
            </div>
            <div class="col-sm-12">
                <input type="text" placeholder="filtrar depósitos" class="form-control no-validate pln-invi mb-1" id="ipt_find_deposito" onkeyup="search_filter('ipt_find_deposito', 'div_deposito')">
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-sm-12 mt-3">
                <input type="checkbox" id="select_all" class="">
                <label for="select_all"><?php echo $lang['politics']['todos'] ?></label>
            </div>
            <div id="div_deposito" class="col-sm-12 pln-content-scroll">
            </div>
        </div>
    </div>
</div>