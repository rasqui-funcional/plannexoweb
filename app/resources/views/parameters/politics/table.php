<?php
if (empty($ViewData['table']['data'])) {
    echo 'false';
    return;
}
?>
<form id="formulario">
    <div id="controlSize" class="table-responsive">
        <table id="" class="table dataTable purchaseorder-table object_table policy-table bigger-table">
            <thead>
            <tr>
                <th class="text-center"></th>
                <? foreach ($ViewData['table']['columns'] as $key => $value): ?>
                    <th>
                        <div class='d-flex flex-row pln-cur-pointer order-col' data-id='<?= $key ?>'>
                            <span data-toggle="tooltip" title="<?= $lang['column'][$key] ?>">
                                <?= (!isset($lang['column']['abrev'][$key]) ? $lang['column'][$key] : $lang['column']['abrev'][$key]) ?>
                            </span>
                            <i class="fa fa-sort<?= isset($value['order']) ? "-" . $value['order'] : "" ?> pln-link-color ml-1 align-self-center"></i>
                        </div>
                    </th>
                <? endforeach; ?>
            </tr>
            </thead>
            <tfoot>
            <input type="hidden" id="hdn_total_items" value="<?= $ViewData['footer']['count']; ?>"/>
            <input type="hidden" id="total_page_number" value="<?= $ViewData['footer']['pages']; ?>">
            <tr>
                <th></th>
                <? foreach ($ViewData['table']['columns'] as $key => $value): ?>
                    <? if ($key == 'QUANTIDADE'): ?>
                        <th><?= Utilities::Unity($ViewData['footer']['skus']); ?></th>
                    <? else: ?>
                        <th></th>
                    <? endif; ?>
                <? endforeach; ?>
            </tr>
            <input type="hidden" id="total_page_number" value="<?= $ViewData['footer']['pages'] ?>">
            </tfoot>
            <tbody id="tablePoliticsBody">
            <? foreach ($ViewData['table']['data'] as $row): ?>
                <tr class='object_table_tr'>
                    <td class="text-center">
                        <input class="input_chk" type="checkbox">
                    </td>
                    <? foreach ($row as $key => $value):
                        $class = '';
                        $data_value = '';
                        if ($key === 'ID_PROFILE_PK' || $key === 'QUANTIDADE') {
                            $class = $key . ' ';
                            $data_value = $value;
                        }
                        $element_params = View::genericWidthTable($value);
                        $class .= $element_params['td_class'] ?>
                        <td class="<?= $class ?>" data-value="<?= $data_value ?>">
                            <? switch ($key):
                                case 'SERVICE_LEVEL': ?>
                                    <span data-toggle="<?= $element_params['data_toggle'] ?>"
                                          title="<?= $element_params['title'] ?>">
                                        <?= $value ?> %
                                    </span>
                                <? break;
                                case 'LT_PLANNER':
                                case 'LT_BUYER':
                                case 'LT_SUPPLIER':
                                case 'LT_RECEIVING':
                                case 'LT_OTHERS':
                                    $current_value = ($value > -1 ? $value : '');
                                    $day_or_days = ($value > 1 ? $lang['politics']['dias'] : $lang['politics']['dia']);
                                    $lead_time_others = ($value != -1 ? $day_or_days : 'Automático');
                                    echo $current_value . ' ' . $lead_time_others;
                                    break;
                                case 'COD_PROFILE': 
                                     if (count(Politics::profile($row['ID_PROFILE_PK'], $ViewData['user']['id_company_pk'])) > 0): ?>
                                        <span data-toggle="<?= $element_params['data_toggle'] ?>"
                                              class="pln-politic" title="<?= $element_params['title'] ?>">
                                            <?= $value ?>
                                        </span>
                                    <? else: ?>
                                        <span data-toggle="<?= $element_params['data_toggle'] ?>"
                                              title="<?= $element_params['title'] ?>">
                                            <?= $value ?>
                                        </span>
                                    <? endif; ?>
                                    <? break; ?>
                                <? default: ?>
                                    <span data-toggle="<?= $element_params['data_toggle'] ?>"
                                          title="<?= $element_params['title'] ?>">
                                        <?= $value ?>
                                    </span>
                                    <? break; ?>
                            <? endswitch; ?>
                        </td>
                    <? endforeach; ?>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div>
</form>
<script>
    classe.initialize();
    paginationClass.initialize();
</script>
