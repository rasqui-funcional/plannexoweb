<?php
$JS = ['uglify/all-purchaseorder-list-min'];
?>
<div class="content-i">
    <div class="content-full-table">
        <div class="element-wrapper">
            <!-- title header -->
            <div class="title-header d-flex flex-row align-items-end mt-3">
                <div>
                    <a href="/<?= $Country ?>/purchaseorder/filters" class="pln-link-color" onclick="loaderShow()"><i
                                class="fa fa-angle-double-left"
                                aria-hidden="true"></i>
                        <?= $lang['sugcompras_lista']['voltar'] ?></a>
                    <h4 class="pln-font-color">
                        <?= $lang['ordem_compra_filtros']['titulo'] ?>
                    </h4>
                </div>
            </div>
            <!-- title header end -->

            <div class="element-box-full">
                <div class="d-flex align-items-end pln-items-search" style="display: flex !important">
                    <!--DONT REMOVE THIS CODE
                    <div class="d-flex flex-column align-items-start">
                        <span class="pln-bold-600" ><?= $lang['ordem_compra_lista']['busca'] ?></span>
                        <span><input type="text" class="form-control form-control-sm col" id="purchaseorder_search_input" /></span>
                    </div>

                    <div class="d-flex flex-row align-items-end ml-1">
                        <span class="ml-1"><button type="button" class="btn pln-btn-orange" onclick="suggestSearch()" /><?= $lang['sugcompras_lista']['buscar'] ?></span>
                    </div>
                    -->
                </div>

                <div id="table_insert"> </div>

                <div class="d-flex mt-1 align-items-start pln-items-search" style="display: none !important">

                    <div class="d-flex flex-row align-items-center ml-3 mr-3">
                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?= $lang['sugcompras_lista']['pagina']['qtd_a'] ?></span>
                            <span id="page_items_to"></span>
                            <span><?= $lang['sugcompras_lista']['pagina']['qtd_de'] ?></span>
                            <span id="page_items_total"></span>
                        </span>
                        <div class="d-flex flex-row">
                            <div class="d-flex flex-column mr-3">
                                <small>
                                    <?php echo $lang['sugcompras_lista']['itens']['mostrando'] ?>
                                    <span id="total_items"></span>
                                    <?php echo $lang['purchaseorder']['list']['orders'] ?>
                                </small>
                                <small>
                                    <?=$lang['sugcompras_lista']['pagina']['qtd_total']?>
                                    <span id="spn_total_page_number"></span>
                                </small>
                            </div>
                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>

                        </div>
                    </div>

                    <div class="d-flex flex-row align-items-center">
                        <span class="pln-bold-600">
                            <?= $lang['sugcompras_lista']['pagina']['titulo'] ?></span>
                        <span class="ml-1"><input type="text" maxlength="10" id="goto_pagenumber"
                                                  class="form-control form-control-sm input_just_number form-control form-control-sm-sm pln-input-60"/></span>
                        <span class="ml-1"><button type="button" id="goToPageNumber" class="btn pln-btn-ghost"/>
                            <?= $lang['sugcompras_lista']['buscar'] ?></span>
                    </div>
                    <div class="d-flex flex-row align-items-center ml-auto mr-3">
                        <span class="pln-bold-600"><?= $lang['sugcompras_lista']['visualizar'] ?></span>
                            <?php include_once('resources/views/items-per-page.php'); ?>
                        <span class="ml-1"><?= $lang['sugcompras_lista']['por_pagina'] ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>