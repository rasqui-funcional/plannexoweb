<div class="content-i pln-content">
    <div class="content-box">
        <!--START - Recent Ticket Comments-->
        <div class="element-wrapper">
            <div class="d-flex flex-row justify-content-between mb-2 align-items-center">
                <div>
                    <h4 class="pln-font-color">
                        <span data-toggle="tooltip" id="spn_filter_id"  title="<?= $lang['ordem_compra']['view'] ?>">
                            <?php echo $lang['ordem_compra_filtros']['titulo'] ?>
                        </span>
                    </h4>
                </div>
                <div class="d-flex flex-row">
                    <span class="pln-bold-600 mt-1 mr-1"><?= $lang['filtro']['my_filters'] ?></span>
                    <div class="d-flex flex-column align-items-end">
                        <div>
                            <select id="select_filtro" name="select_filtro"
                                    class="form-control-sm pln-select-filter-size">
                                <option value=""></option>
                                <?php
                                if ($ViewData['select_filtro']) {
                                    foreach ($ViewData['select_filtro'] as $Select) {
                                        ?>
                                        <option <?= (isset($Select['disable']) ? 'disable' : null) ?>
                                                value="<?= $Select['value'] ?>"><?= $Select['name'] ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="align-self-start">
							<span id="btn_filtro" data-toggle="modal" data-target="#modal_edit"
                                  class="pln-link-color pln-font-smaller pln-cur-pointer">
								<span id="btn_filtro_text"><?= $lang['filtro']['save_filter']?></span>
							</span>
                        </div>
                    </div>
                    <div class="d-flex flex-column align-items-end ml-4">
                        <button type="button" id="btn_search" class="btn pln-btn-orange">
                            <?= $lang['parametros']['pesquisa'] ?>
                        </button>

                        <span class="pln-link-color pln-font-smaller pln-cur-pointer align-self-start" id="clean_form">
							<?= $lang['filtro']['clean_filters'] ?>
						</span>
                    </div>
                </div>
            </div>

            <hr>
            <form name="formulario" id="formulario" method="post" action="/<?= $Country ?>/purchaseorder/list">
                <!-- #plannexo AQUI VEM O CONTEÚDO DA PÁGINA -->
                <input type="hidden" name="filter_temp" id="filter_temp">

                <!-- FILTROS ESPECIAIS -->
                <div class="row">
                    <div class="col-sm-8">
                        <div class="element-box pln-ordemcompra-min-box-height-l1">
                            <fieldset class="form-group">
                                <legend><span><?= $lang['filtro']['card_filtros_especiais']['titulo'] ?></span></legend>
                                <div class="row mb-1 align-items-center">
                                    <div class="col-sm-2"><?php echo $lang['ordem_compra_filtros']['ordens'] ?></div>
                                    <div class="col-sm-5 d-flex flex-row">
                                        <div class="mr-3">
                                            <input type="radio" class="ord_atraso" value="com" id="ord_atraso_com"
                                                   name="ord_atraso[]">
                                            <label for="ord_atraso_com"><?php echo $lang['ordem_compra_filtros']['com_atraso'] ?></label>
                                        </div>
                                        <div class="mr-3">
                                            <input type="radio" class="ord_atraso" value="sem" id="ord_atraso_sem"
                                                   name="ord_atraso[]">
                                            <label for="ord_atraso_sem"><?php echo $lang['ordem_compra_filtros']['no_prazo'] ?></label>
                                        </div>
                                        <div>
                                            <input type="radio" class="ord_atraso" value="todas" checked
                                                   id="ord_atraso_todas" name="ord_atraso[]">
                                            <label for="ord_atraso_todas"><?php echo $lang['ordem_compra_filtros']['todas'] ?></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 d-flex flex-row align-items-center justify-content-start">
                                        <span class="mr-1"><?php echo $lang['ordem_compra_filtros']['qtde_dias'] ?></span>
                                        <select id="dias_atraso" name="dias_atraso" class="form-control mr-1 w-50"
                                                disabled>
                                            <option value=""></option>
                                            <option value="<="><?= $lang['ordem_compra_filtros']['menor_igual'] ?></option>
                                            <option value=">="><?= $lang['ordem_compra_filtros']['maior_igual'] ?></option>
                                            <option value="<"><?= $lang['ordem_compra_filtros']['menor_que'] ?></option>
                                            <option value=">"><?= $lang['ordem_compra_filtros']['maior_que'] ?></option>
                                        </select>

                                        <input type="text" id="qtde_dias_atraso" name="qtde_dias_atraso"
                                               class="form-control mr-1 pln-input-60" disabled>
                                        <span class="float-left"><?= $lang['ordem_compra_filtros']['dias'] ?></span>
                                    </div>

                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-2"><?php echo $lang['ordem_compra_filtros']['data_req'] ?></div>
                                    <div class="col-sm-5">
                                        <select id="date_req_order_cond" name="date_req_order_cond"
                                                class="form-control select_date">
                                            <?= View::optionsGenerate($lang['purchaseorder']['filter']['select']) ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control order_daterange"
                                               id="input_daterange_date_req" name="date_req" value=""/>
                                        <label class="pln-invi" id="lbl_daterange_date_req"></label>
                                    </div>
                                </div>

                                <div class="row align-items-center mt-1 mb-1">
                                    <div class="col-sm-2"><?php echo $lang['ordem_compra_filtros']['data_ordem'] ?></div>
                                    <div class="col-sm-5">
                                        <select id="date_order_in_cond" name="date_order_in_cond"
                                                class="form-control select_date">
                                            <?= View::optionsGenerate($lang['purchaseorder']['filter']['select']); ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control order_daterange"
                                               id="input_daterange_date_order" name="date_order" value=""/>
                                        <label class="pln-invi" id="lbl_daterange_date_order"></label>
                                    </div>
                                </div>

                                <div class="row align-items-center">
                                    <div class="col-sm-2"><?php echo $lang['ordem_compra_filtros']['data_esperada'] ?></div>
                                    <div class="col-sm-5">
                                        <select id="date_exp_in_cond" name="date_exp_in_cond"
                                                class="form-control select_date">
                                            <?= View::optionsGenerate($lang['purchaseorder']['filter']['select']); ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control order_daterange"
                                               id="input_daterange_date_exp" name="date_exp" value=""/>
                                        <label class="pln-invi" id="lbl_daterange_date_exp"></label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <!-- AGRUPAR POR -->
                    <div class="col-sm-4">
                        <div class="element-box pln-ordemcompra-min-box-height-l1">
                            <fieldset class="form-group">
                                <legend><span onclick="hide_filter('ipt_find_agrupar')"
                                              class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['agrupar'] ?> <i
                                                class="fa fa-search pln-link-color"></i></span></legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" placeholder="filtrar agrupamento"
                                               class="form-control pln-invi mb-1" id="ipt_find_agrupar"
                                               onkeyup="search_filter('ipt_find_agrupar', 'div_filter_agrupar')">
                                    </div>
                                    <div class="col-sm-12">
                                        <input type="checkbox" class="chk_check_all" id="chk_check_all_agrupar"> <label
                                                for="chk_check_all_agrupar"><?= $lang['parametros']['todos'] ?></label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 pln-content-scroll" id="div_filter_agrupar">
                                        <div class="d-flex mt-2 flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_cod_estab_fk"
                                                   value="cod_estab_fk">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_cod_estab_fk'><?= $lang['ordem_compra_filtros']['estab'] ?></label>
                                        </div>
                                        <div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_user_id"
                                                   value="user_id">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_user_id'>Gestor</label>
                                        </div>
                                        <div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_num_req"
                                                   value="num_req">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_num_req'><?= $lang['ordem_compra_filtros']['num_solicitacao'] ?></label>
                                        </div>
                                        <div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_num_order"
                                                   value="num_order">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_num_order'><?= $lang['ordem_compra_filtros']['ord_compra'] ?></label>
                                        </div>
                                        <div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_cod_item"
                                                   value="cod_item">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_cod_item'><?= $lang['ordem_compra_filtros']['cod_item'] ?></label>
                                        </div>
                                        <div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_desc_supplier"
                                                   value="desc_supplier">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_desc_supplier'><?= $lang['ordem_compra_filtros']['desc_fornecedor'] ?></label>
                                        </div>
                                        <div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_date_req"
                                                   value="date_req">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_date_req'><?= $lang['ordem_compra_filtros']['data_req'] ?></label>
                                        </div>
                                        <div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_date_order"
                                                   value="date_order">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_date_order'><?= $lang['ordem_compra_filtros']['data_ordem'] ?></label>
                                        </div>
                                        <div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_date_exp"
                                                   value="date_exp">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_date_exp'><?= $lang['ordem_compra_filtros']['data_expiracao'] ?></label>
                                        </div>
                                        <div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="cod_group_fk[]" id="cod_group_fk_inv_level"
                                                   value="inv_level">
                                            <label class="text-truncate pl-1"
                                                   for='cod_group_fk_inv_level'><?= $lang['ordem_compra_filtros']['sinalizador'] ?></label>
                                        </div>
                                        <?php foreach ($ViewData['SearchFields'] as $key => $value): ?>
                                            <div class="d-flex flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name="cod_group_fk[]"
                                                       id="cod_group_fk_cod_group<?= $key ?>_fk"
                                                       value="cod_group<?= $key ?>_fk">
                                                <label class="text-truncate pl-1" for='cod_group_fk_cod_group<?= $key ?>_fk'>
                                                    <?= $value ?>
                                                </label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- FIM AGRUPAR POR -->
                </div>
                <!-- FIM FILTROS ESPECIAIS-->

                <!-- FILTROS SKU-->
                <div class="row">
                    <div class="col-sm-8">
                        <div class="element-box">
                            <fieldset class="form-group">
                                <legend><span><?= $lang['filtro']['card_sku']['titulo'] ?></span></legend>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""><?php echo $lang['ordem_compra_filtros']['cod_fornecedor'] ?></label>
                                            <input type="text" name="id_supplier" id="id_supplier" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""><?php echo $lang['ordem_compra_filtros']['desc_fornecedor'] ?></label>
                                            <input type="text" name="desc_supplier" id="desc_supplier"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""><?php echo $lang['ordem_compra_filtros']['cod_item'] ?></label>
                                            <input type="text" name="cod_item" id="cod_item" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""><?php echo $lang['ordem_compra_filtros']['req_compra'] ?></label>
                                            <input type="text" name="num_req" id="num_req" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""><?php echo $lang['ordem_compra_filtros']['ord_compra'] ?></label>
                                            <input type="text" name="num_order" id="num_order" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for=""><?php echo $lang['filtro']['card_parametros']['ciclo'] ?></label>
                                        <div class="d-flex flex-row">
                                            <div class="d-flex flex-row">
                                                <div class="mr-4"><input type="checkbox" name="sku_life_cycle[]"
                                                                         id="sku_life_cycle_1" value="1"
                                                                         class="form-control mr-1"><?php echo $lang['filtro']['card_parametros']['nova'] ?>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-row">
                                                <div class="mr-4"><input type="checkbox" name="sku_life_cycle[]"
                                                                         id="sku_life_cycle_2" value="2"
                                                                         class="form-control mr-1"><?php echo $lang['filtro']['card_parametros']['madura'] ?>
                                                </div>
                                            </div>

                                            <div class="d-flex flex-row">
                                                <div><input type="checkbox" name="sku_life_cycle[]" id="sku_life_cycle_3"
                                                            value="3"
                                                            class="form-control mr-1"><?php echo $lang['filtro']['card_parametros']['desc'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 d-flex flex-column">
                                        <div>
                                            <label for=""><?php echo $lang['filtro']['card_parametros']['sinalizador'] ?></label>
                                            <div class="d-flex flex-row">
                                                <div class="d-flex flex-row">
                                                    <div class="mr-4"><input type="checkbox" name="inv_level[]"
                                                                             id="inv_level_0" value="0"
                                                                             class="form-control mr-1"><?php echo $lang['ordem_compra_filtros']['zero'] ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-row">
                                                    <div class="mr-4"><input type="checkbox" name="inv_level[]"
                                                                             id="inv_level_1" value="1"
                                                                             class="form-control mr-1"><?php echo $lang['ordem_compra_filtros']['muito_baixo'] ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-row">
                                                    <div><input type="checkbox" name="inv_level[]" id="inv_level_2"
                                                                value="2"
                                                                class="form-control mr-1"><?php echo $lang['ordem_compra_filtros']['baixo'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="d-flex flex-row">
                                                <div class="d-flex flex-row">
                                                    <div class="mr-4"><input type="checkbox" name="inv_level[]"
                                                                             id="inv_level_3" value="3"
                                                                             class="form-control mr-1"><?php echo $lang['ordem_compra_filtros']['alvo'] ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-row">
                                                    <div class="mr-4"><input type="checkbox" name="inv_level[]"
                                                                             id="inv_level_4" value="4"
                                                                             class="form-control mr-1"><?php echo $lang['ordem_compra_filtros']['alto'] ?>
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-row">
                                                    <div><input type="checkbox" name="inv_level[]" id="inv_level_5"
                                                                value="5"
                                                                class="form-control mr-1"><?php echo $lang['ordem_compra_filtros']['muito_alto'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for=""><?php echo $lang['ordem_compra_filtros']['status_sistema'] ?></label>
                                        <div class="d-flex flex-row">
                                            <div class="d-flex flex-row">
                                                <div class="mr-4"><input type="checkbox" name="sit_sku[]" id="sit_sku_1"
                                                                         value="1"
                                                                         class="form-control mr-1"><?php echo $lang['ordem_compra_filtros']['status_ativo'] ?>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-row">
                                                <div><input type="checkbox" name="sit_sku[]" id="sit_sku_0" value="0"
                                                            class="form-control mr-1"><?php echo $lang['ordem_compra_filtros']['status_inativo'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>

                    <!-- FILTROS CURVAS-->
                    <div class="col-sm-4">
                        <div class="element-box pln-ordemcompra-min-box-height-l1">
                            <fieldset class="form-group">
                                <legend><span><?= $lang['filtro']['card_curvas']['titulo'] ?></span></legend>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_curvas']['abc'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="curve_abc[]" id="curve_abc_A" value="A"> <label
                                                for="curve_abc_A">A</label>
                                        <input type="checkbox" name="curve_abc[]" id="curve_abc_B" value="B"
                                               class="ml-2"> <label for="curve_abc_B">B</label>
                                        <input type="checkbox" name="curve_abc[]" id="curve_abc_C" value="C"
                                               class="ml-2"> <label for="curve_abc_C">C</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_curvas']['xyz'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="curve_xyz[]" id="curve_xyz_X" value="X"> <label
                                                for="curve_xyz_X">X</label>
                                        <input type="checkbox" name="curve_xyz[]" id="curve_xyz_Y" value="Y"
                                               class="ml-2"> <label for="curve_xyz_Y">Y</label>
                                        <input type="checkbox" name="curve_xyz[]" id="curve_xyz_Z" value="Z"
                                               class="ml-2"> <label for="curve_xyz_Z">Z</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_curvas']['pqr'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="curve_pqr[]" id="curve_pqr_P" value="P"> <label
                                                for="curve_pqr_P">P</label>
                                        <input type="checkbox" name="curve_pqr[]" id="curve_pqr_Q" value="Q"
                                               class="ml-2"> <label for="curve_pqr_Q">Q</label>
                                        <input type="checkbox" name="curve_pqr[]" id="curve_pqr_R" value="R"
                                               class="ml-2"> <label for="curve_pqr_R">R</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <?= $lang['filtro']['card_curvas']['123'] ?>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="checkbox" name="curve_123[]" id="curve_123_1" value="1"> <label
                                                for="curve_123_1">1</label>
                                        <input type="checkbox" name="curve_123[]" id="curve_123_2" value="2"
                                               class="ml-2"> <label for="curve_123_2">2</label>
                                        <input type="checkbox" name="curve_123[]" id="curve_123_3" value="3"
                                               class="ml-2"> <label for="curve_123_3">3</label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- FIM FILTROS CURVAS-->
                </div>
                <!-- FIM FILTROS SKU-->

                <div class="row">
                    <!-- ESTABELECIMENTO -->
                    <div class="col-sm-4">
                        <div class="element-box pln-ordemcompra-min-box-height-l1">
                            <fieldset class="form-group">
                                <legend><span onclick="hide_filter('ipt_find_estabelecimento')"
                                              class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['estab'] ?> <i
                                                class="fa fa-search pln-link-color"></i></span></legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" placeholder="filtrar estabelecimentos"
                                               class="form-control pln-invi mb-1" id="ipt_find_estabelecimento"
                                               onkeyup="search_filter('ipt_find_estabelecimento', 'div_filter_estabelecimento')">
                                    </div>
                                    <div class="col-sm-12">
                                        <input type="checkbox" class="chk_check_all" id="chk_check_all_estab"> <label
                                                for="chk_check_all_estab"><?= $lang['parametros']['todos'] ?></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 pln-content-scroll" id="div_filter_estabelecimento">
                                        <?php
                                        foreach ($ViewData['Estab'] as $Key => $Estab) {
                                            ?>
                                            <div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name="cod_estab_fk[]"
                                                       id="cod_estab_fk_<?= $Estab['code'] ?>"
                                                       value="<?= $Estab['code'] ?>">
                                                <label class="text-truncate pl-1"
                                                       for='cod_estab_fk_<?= $Estab['code'] ?>'><?= $Estab['name'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>
                    <!-- FIM ESTABELECIMENTO -->

                    <!-- POLITICA -->
                    <div class="col-sm-4">
                        <div class="element-box min-box-height-l5">
                            <fieldset class="form-group">
                                <legend><span onclick="hide_filter('ipt_find_contrato')"
                                              class="pln-cur-pointer"><?php echo $lang['filtro']['card_parametros']['politica'] ?> <i
                                                class="fa fa-search pln-link-color"></i></span></legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" placeholder="filtrar política"
                                               class="form-control pln-invi mb-1" id="ipt_find_contrato"
                                               onkeyup="search_filter('ipt_find_contrato', 'div_filter_politica')">
                                    </div>
                                    <div class="col-sm-12">
                                        <input type="checkbox" class="chk_check_all" id="chk_check_all_politica"> <label
                                                for="chk_check_all_politica"><?= $lang['parametros']['todos'] ?></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 pln-content-scroll" id="div_filter_politica">
                                        <?php
                                        foreach ($ViewData['Profile'] as $Profile) {
                                            ?>
                                            <div class='d-flex flex-row text-nowrap align-items-center'>
                                                <input type='checkbox' name='id_profile[]'
                                                       id='id_profile_<?= $Profile['ID_PROFILE_PK'] ?>'
                                                       value='<?= $Profile['ID_PROFILE_PK'] ?>'>
                                                <label class='text-truncate pl-1'
                                                       for='id_profile_<?= $Profile['ID_PROFILE_PK'] ?>'><?= $Profile['NOME'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- FIM POLITICA -->

                    <!-- GESTOR -->
                    <div class="col-sm-4">
                        <div class="element-box min-box-height-l5">
                            <fieldset class="form-group">
                                <legend><span onclick="hide_filter('ipt_find_gestor')"
                                              class="pln-cur-pointer"><?php echo $lang['filtro']['card_gestor']['titulo'] ?> <i
                                                class="fa fa-search pln-link-color"></i></span></legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" placeholder="filtrar gestor"
                                               class="form-control pln-invi mb-1" id="ipt_find_gestor"
                                               onkeyup="search_filter('ipt_find_gestor', 'div_filter_gestor')">
                                    </div>
                                    <div class="col-sm-12">
                                        <input type="checkbox" class="chk_check_all" id="chk_check_all_gestor"> <label
                                                for="chk_check_all_gestor"><?= $lang['parametros']['todos'] ?></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 pln-content-scroll" id="div_filter_gestor">
                                        <?php
                                        foreach ($ViewData['User'] as $Key => $User) {
                                            ?>
                                            <div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name='user_id[]'
                                                       id='user_id_<?= $User['ID_USER_PK'] ?>'
                                                       value='<?= $User['ID_USER_PK'] ?>'>
                                                <label class="text-truncate pl-1"
                                                       for='user_id_<?= $User['ID_USER_PK'] ?>'><?= $User['LOGIN'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- FIM GESTOR -->

                    <!-- DEPOSITO PADRAO -->
                    <div class="col-sm-4">
                        <div class="element-box pln-ordemcompra-min-box-height-l1">
                            <fieldset class="form-group">
                                <legend><span onclick="hide_filter('ipt_find_deposito_padrao')"
                                              class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['deposito_padrao'] ?> <i
                                                class="fa fa-search pln-link-color"></i></span></legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" placeholder="filtrar depósito"
                                               class="form-control pln-invi mb-1" id="ipt_find_deposito_padrao"
                                               onkeyup="search_filter('ipt_find_deposito_padrao', 'div_filter_deposito_padrao')">
                                    </div>
                                    <div class="col-sm-12">
                                        <input type="checkbox" class="chk_check_all" id="chk_check_all_deposito_padrao">
                                        <label for="chk_check_all_deposito_padrao"><?= $lang['parametros']['todos'] ?></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 pln-content-scroll" id="div_filter_deposito_padrao">
                                        <?php foreach ($ViewData['Deposit'] as $Key => $Deposit) : ?>
                                            <div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name="default_local[]" id="default_local_<?= $Deposit['COD_LOCAL_PK'] ?>" value="<?= $Deposit['COD_LOCAL_PK'] ?>">
                                                <label class="text-truncate pl-1" for='default_local_<?= $Deposit['COD_LOCAL_PK'] ?>'><?= $Deposit['DESCRICAO'] ?></label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- FIM DEPOSITO PADRAO -->

                    <!-- FILTROS DINAMICOS -->
                    <?php foreach ($ViewData['Groups'] as $key => $Group) { ?>
                        <div class="col-sm-4">
                            <div class="element-box min-box-height-l5">
                                <fieldset class="form-group">

                                    <legend><span onclick="hide_filter('ipt_find_<?php echo $key ?>')"
                                                  class="pln-cur-pointer"><?php echo $Group ?> <i
                                                    class="fa fa-search pln-link-color"></i></span></legend>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" placeholder="filtrar <?php echo $Group ?>"
                                                   class="form-control pln-invi mb-1"
                                                   id="ipt_find_<?php echo $key ?>"
                                                   onkeyup="search_filter('ipt_find_<?php echo $key ?>', 'div_filter_<?php echo $key ?>')">
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="checkbox" class="chk_check_all"
                                                   id="chk_check_all_<?php echo $key ?>"> <label
                                                    for="chk_check_all_<?php echo $key ?>"><?= $lang['parametros']['todos'] ?></label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 pln-content-scroll"
                                             id="div_filter_<?php echo $key ?>">
                                            <?php
                                            foreach ($ViewData['Group_' . $key] as $Key2 => $Campo) {
                                                ?>
                                                <div class="d-flex <?= ($Key2 == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                    <input type='checkbox'
                                                           name='cod_group<?php echo $key ?>_fk[]'
                                                           id='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>'
                                                           value='<?= $Campo['COD_GROUP_PK'] ?>'>
                                                    <label class='text-truncate pl-1'
                                                           for='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>'><?= $Campo['DESC_GROUP_CONT'] ?></label>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- FILTROS DINAMICOS -->
                </div>

                <!--END - Recent Ticket Comments-->
            </form>
        </div>
    </div>
</div>
<p id="demo"></p>
<div class="display-type"></div>

<div class="modal fade" id="modal_edit" data-keyboard="true" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="botao_editar" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_titulo" name="modal_titulo"><?= $lang['filtro']['new_filter'] ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id>
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <input placeholder="Digite nome do novo filtro" maxlength="50" type="text" class="form-control"
                           name="filter_nome" id="filter_nome">
                </div>

                <div>
                    <input name="filter_perfil" id="filter_perfil" type="hidden" value="Publico">
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <span class="pln-danger pln-cur-pointer" id="confirm_delete"><i
                            class="fa fa-times pln-danger"></i> <span><?= $lang['filtro']['delete'] ?></span></span>

                <button type="button" class="btn pln-btn-orange" id="save_filter"><span id="spn_save"><?= $lang['filtro']['save'] ?></span> <i
                            class="fa fa-spin fa-spinner pln-invi"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal loading" id="modal_loading" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="botao_loading" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document" style="margin-top: 15%">
        <div class="modal-content">
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated actived" role="progressbar"
                         aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="clear"></div>
<?php
$JS = ['uglify/all-purchaseorder-filters-min'];
?>

<script>
    let filter_temp = <?= $ViewData['filter_temp'] ?>;
</script>
