<form id="formulario">
    <div id="controlSize" class="table-responsive">
        <table id="tablePurchaseOrderList" class="table dataTable list-table object_table bigger-table">
            <?php addComponent('table/thead.php') ?>
            <?php addComponent('purchaseorder/orders.php') ?>
            <?php addComponent('table/tfoot.php') ?>
        </table>
    </div>
</form>
<script>
    classe.initialize();
    paginationClass.initialize();
</script>
