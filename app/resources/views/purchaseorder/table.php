<?php
if (!$ViewData['data_table']) {
    echo 'false';
    return;
}
?>
<form id="formulario">
    <div id="controlSize" class="table-responsive">
        <table id="tablePurchaseOrder" class="table dataTable purchaseorder-table object_table bigger-table">
            <thead>
                <tr>
                    <th>
                        <div class="d-flex flex-row pln-cur-pointer order-col" data-id="TOTAL_ORDERS">
                            <span data-toggle="tooltip"
                                  title="QTDE ORDENS"><?php echo $lang['ordem_compra_filtros']['col_ordens'] ?></span>
                            <i class="fa fa-sort<?= isset($ViewData['order']['TOTAL_ORDERS']) ? "-" . $ViewData['order']['TOTAL_ORDERS'] : "" ?> pln-link-color ml-1 align-self-center"></i>
                        </div>
                    </th>
                    <?php foreach ($ViewData['header_cols'] as $ColName):
                        if (!empty($ColName)):
                            $title = $lang['ordem_compra_filtros'][strtolower($ColName)];
                            $css = isset($ViewData['order'][$ColName]) ? "-" . $ViewData['order'][$ColName] : "";
                    ?>
                            <th>
                                <div class="d-flex flex-row pln-cur-pointer order-col" data-id="<?= $ColName ?>">
                                    <span data-toggle="tooltip" title="<?= $title ?>"><?= $title ?></span>
                                    <i class="fa fa-sort<?= $css ?> pln-link-color ml-1 align-self-center"></i>
                                </div>
                            </th>
                    <?php
                        endif;
                    endforeach;
                    ?>
                </tr>
            </thead>

            <tfoot>
                <tr></tr>
                <input type="hidden" id="hdn_total_items" value="<?php echo $ViewData['footer']['count'] ?>"/>
                <input type="hidden" id="total_page_number" value="<?php echo $ViewData['footer']['pages'] ?>">
            </tfoot>

            <tbody id="tablePurchaseOrderBody">
            <?php foreach ($ViewData['data_table'] as $key => $Data) { ?>
                <tr class="object_table_tr">
                    <td style="min-width: 100px">
                        <span class="pln-order-blue list_purchaseorder pln-cur-pointer">
                            <?php echo $Data['TOTAL_ORDERS']['value'] ?>
                        </span>
                    </td>
                    <?php
                    foreach ($ViewData['header_cols'] as $ColName) {
                        $column = strtoupper($ColName);
                        if (!empty($ColName)) {
                            switch ($ColName) {
                                case 'inv_level':
                                    ?>
                                    <td data-column="<?php echo $ColName ?>"
                                        data-value="<?php echo $Data[$column]['value'] ?>"><img
                                                src="/resources/img/<?php echo $Data[$column]['name'] ?>"></td>
                                    <?
                                    break;
                                default:
                                    ?>
                                    <td data-column="<?php echo $ColName ?>"
                                        data-value="<?php echo $Data[$column]['value'] ?>"><?php echo $Data[$column]['name'] ?></td>
                                    <?
                                    break;
                            }
                            ?>

                            <?php
                        }
                    }
                    ?>
                </tr>
                <?php
            } ?>
            </tbody>
        </table>
    </div>
</form>
<script>
    classe.initialize();
    paginationClass.initialize();
</script>
