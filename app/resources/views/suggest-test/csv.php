<?php
if (!empty($ViewData['error'])) {
    echo $ViewData['error'];
    exit;
}

$session = Login::getUserSession();
$estabs = Redis::HGet("company:{$session['id_company_pk']}:estabs");
$politics = Redis::HGet("company:{$session['id_company_pk']}:politics");
$users = Redis::HGet("company:{$session['id_company_pk']}:users");
$calendar = Redis::HGet("company:{$session['id_company_pk']}:calendars");
ob_end_clean();

Request::generateCSVHeaders($lang['vw_scr_scm420g_sug']['name']);

$output = fopen('php://output', 'w');

$translatedTitles = [];
$suggest_keys = array_keys($ViewData['data'][0]);
array_pop($suggest_keys);
$titles = [];
foreach ($suggest_keys as $suggest_key) {
    $titles[$suggest_key] = $suggest_key;
    $translatedTitles[$suggest_key] = $lang['vw_scr_scm420g_sug'][strtolower($suggest_key)];
}

fputcsv($output, mb_convert_encoding($translatedTitles, 'ISO-8859-1'), ';');
foreach ($ViewData['data'] as $row) {
    unset($row['ID_SUG_PK']);
    unset($titles['USER_ID']);
    unset($titles['COD_ESTAB_FK']);
    unset($titles['ID_CALENDAR_FK']);
    unset($titles['ID_PROFILE']);
    foreach ($titles as $title) {
        $row[$title] = Utilities::formatCsvFields($title, $row[$title]);
    }
    $row['COD_ESTAB_FK'] = $estabs[$row['COD_ESTAB_FK']];
    $row['ID_PROFILE'] = $politics[$row['ID_PROFILE']];
    $row['USER_ID'] = $users[$row['USER_ID']];
    $row['ID_CALENDAR_FK'] = $calendar[$row['ID_CALENDAR_FK']];
   fputcsv($output, mb_convert_encoding($row, 'ISO-8859-1'), ';');
}

