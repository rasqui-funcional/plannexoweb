
<script>
	var filter_temp = <?=$ViewData['filter_temp']?>;
	
</script>

<div class="content-i pln-content">
	<div class="content-box">
		<!--START - Recent Ticket Comments-->
		<div-- class="element-wrapper">
			<div class="d-flex flex-row justify-content-between mb-2 align-items-center">
				<div>
					<h4 class="pln-font-color"><span data-toggle="tooltip" title="<?=$lang['sugcompras']['view']?>"><?=$lang['sugcompras_form']['titulo']?></span></h4>
				</div>
				<div class="d-flex flex-row">
					<span class="pln-bold-600 mt-1 mr-1"><?=$lang['filtro']['my_filters']?>:</span>
					<div class="d-flex flex-column align-items-end">
						<div>
							<select id="select_filtro" name="select_filtro" class="form-control-sm pln-select-filter-size">
								<option value=""></option>
								<?php
								if($ViewData['select_filtro']){
                                    foreach($ViewData['select_filtro'] as $Select){
                                        ?>
                                        <option <?= (isset($Select['disable']) ? 'disable' : null) ?> value="<?= $Select['value'] ?>"><?= $Select['name'] ?></option>
                                        <?php
                                    }
								}
								?>
							</select>
						</div>							
						<div class="align-self-start">
							<span id="btn_filtro"  data-toggle="modal" data-target="#modal_edit" class="pln-link-color pln-font-smaller pln-cur-pointer">
								<span id="btn_filtro_text"><?=$lang['filtro']['save_filter']?></span>
							</span>
						</div>
					</div>
					<div class="d-flex flex-column align-items-end ml-4">
						<button type="button" onclick="pesquisar();" id="btn_pesquisa" class="btn pln-btn-orange">
							<?=$lang['parametros']['pesquisa']?>
						</button>
						
						<span class="pln-link-color pln-font-smaller pln-cur-pointer align-self-start" onclick="cleanForm('formulario')">
							<?=$lang['filtro']['clean_filters']?>
						</span>
					</div>
				</div>									
			</div>
			<hr>
			<form name="formulario" id="formulario" method="post" action="/<?=$Country?>/suggest-test/list">
				<!-- #plannexo AQUI VEM O CONTE�DO DA P�GINA -->
				<input type="hidden" name="filter_temp" id="filter_temp" />
				<div class="row">						
					<div class="col-sm-4">
						<div class="element-box pln-sugg-min-box-height-l1">
							<fieldset class="form-group">
								<legend><span><?=$lang['filtro']['card_estab']['titulo']?></span></legend>
								<div class="row">
									<div class="col-sm-12 pln-content-scroll">
										<?php
										foreach($ViewData['Estab'] as $Estab){
											?>
											<div>
												<input type='checkbox' name="cod_estab_fk[]" id="cod_estab_fk_<?=$Estab['code']?>" value="<?=$Estab['code']?>">
												<label for='cod_estab_fk_<?=$Estab['code']?>'><?=$Estab['name']?></label>
												</div>
											<?php
										}
										?> 
									</div>
								</div>
							</fieldset>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="element-box pln-sugg-min-box-height-l1">
							<fieldset class="form-group">
								<legend><span><?=$lang['filtro']['card_status']['titulo']?></span></legend>
								<div class="row">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_status']['erp']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="sit_sku_erp[]" id="sit_sku_erp_1" value="1"> <label for="sit_sku_erp_1"><?=$lang['filtro']['card_status']['ativo']?></label> 
										<input type="checkbox" name="sit_sku_erp[]" class="ml-2" id="sit_sku_erp_0" value="0"> <label for="sit_sku_erp_0"><?=$lang['filtro']['card_status']['inativo']?></label>
									</div>
								</div>

								<div class="row mt-3">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_status']['sistema']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="sit_sku[]" id="sit_sku_1" value="1"> <label for="sit_sku_1"><?=$lang['filtro']['card_status']['ativo']?></label> 
										<input type="checkbox" name="sit_sku[]" class="ml-2" id="sit_sku_0" value="0"> <label for="sit_sku_0"><?=$lang['filtro']['card_status']['inativo']?></label>
									</div>
								</div>								
								<div class="row mt-3">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_status']['analisada']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="sit_analysed[]" id="sit_analysed_1" value="1"> <label for=""><?=$lang['filtro']['card_status']['sim']?></label> 
										<input type="checkbox" name="sit_analysed[]" id="sit_analysed_0" class="ml-2" id="" value="0"> <label for=""><?=$lang['filtro']['card_status']['nao']?></label>
									</div>
								</div>
								<div class="row mt-3">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_status']['aprovada']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="sit_saved[]" id="sit_saved_1" value="1"> <label for=""><?=$lang['filtro']['card_status']['sim']?></label> 
										<input type="checkbox" name="sit_saved[]" id="sit_saved_0" class="ml-2" id="" value="0"> <label for=""><?=$lang['filtro']['card_status']['nao']?></label>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="element-box pln-sugg-min-box-height-l1">                               
							<fieldset class="form-group">
								<legend><span><?=$lang['filtro']['card_curvas']['titulo']?></span></legend>
								<div class="row">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_curvas']['abc']?>
									</div>	
									<div class="col-sm-7">
										<input type="checkbox" name="curve_abc[]" id="curve_abc_A" value="A"> <label for="curve_abc_A">A</label> 
										<input type="checkbox" name="curve_abc[]" id="curve_abc_B" value="B" class="ml-2"> <label for="curve_abc_B">B</label> 
										<input type="checkbox" name="curve_abc[]" id="curve_abc_C" value="C" class="ml-2"> <label for="curve_abc_C">C</label> 
									</div>
								</div>

								<div class="row">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_curvas']['xyz']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="curve_xyz[]" id="curve_xyz_X" value="X"> <label for="curve_xyz_X">X</label> 
										<input type="checkbox" name="curve_xyz[]" id="curve_xyz_Y" value="Y" class="ml-2"> <label for="curve_xyz_Y">Y</label> 
										<input type="checkbox" name="curve_xyz[]" id="curve_xyz_Z" value="Z" class="ml-2"> <label for="curve_xyz_Z">Z</label> 
									</div>
								</div>

								<div class="row">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_curvas']['pqr']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="curve_pqr[]" id="curve_pqr_P" value="P"> <label for="curve_pqr_P">P</label> 
										<input type="checkbox" name="curve_pqr[]" id="curve_pqr_Q" value="Q" class="ml-2"> <label for="curve_pqr_Q">Q</label> 
										<input type="checkbox" name="curve_pqr[]" id="curve_pqr_R" value="R" class="ml-2"> <label for="curve_pqr_R">R</label> 
									</div>
								</div>

								<div class="row">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_curvas']['123']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="curve_123[]" id="curve_123_1" value="1"> <label for="curve_123_1">1</label> 
										<input type="checkbox" name="curve_123[]" id="curve_123_2" value="2" class="ml-2"> <label for="curve_123_2">2</label> 
										<input type="checkbox" name="curve_123[]" id="curve_123_3" value="3" class="ml-2"> <label for="curve_123_3">3</label> 
									</div>
								</div>                                
							</fieldset>
						</div>
					</div>

				</div>
				<!-- Linha-->
				<div class="row">
					<div class="col-sm-4">
						<div class="element-box min-box-height-sku">
							<fieldset class="form-group">
								<legend><span><?=$lang['filtro']['card_sku']['item']?></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="cod_item"><?=$lang['filtro']['card_sku']['codigo']?></label>
											<input type="text" class="form-control"  name="cod_item_pk" id="cod_item_pk">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="cod_item"><?=$lang['filtro']['card_sku']['desc']?></label>
											<input type="text" class="form-control" name="desc_item" id="desc_item">
										</div>
									</div> 
								</div>
							</fieldset>
						</div>	
					</div>	

					<!-- card -->
					<div class="col-sm-8">
						<div class="element-box min-box-height-l2">
							<div class="row d-flex flex-row">
								<div class="col-sm-6">
									<fieldset class="form-group">
										<legend>
											<span onclick="hide_filter('ipt_find_exibicao')" class="pln-cur-pointer">
											<?=$lang['filtro']['card_exibicao']['titulo']?> <i class="fa fa-search pln-link-color"></i>
											</span>											
										</legend>
										<div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" placeholder="filtrar exibição " class="form-control pln-invi mb-1" id="ipt_find_exibicao" onkeyup="search_filter('ipt_find_exibicao', 'div_exibition')">
                                            </div>
											<div class="col-sm-4 d-flex align-items-center mt-1">
												<input type="checkbox" class="chk_check_all" id="chk_check_all_exibicao" name="chk_check_all_exibicao">													
												<label class="pl-1 pln-font-smaller" for="chk_check_all_exibicao"><?=$lang['parametros']['todos']?></label>													
											</div>
											<div class="col-sm-4 d-flex align-items-center justify-content-center mt-1">
												<i data-toggle="tooltip" title="Mover para cima" class="fa fa-arrow-up pln-order-blue pln-cur-pointer i_multiorder_exibition"></i>
												<i data-toggle="tooltip" title="Mover para baixo" class="fa fa-arrow-down pln-order-blue pln-cur-pointer i_multiorder_exibition ml-3"></i>
											</div>
											<div class="col-sm-4 d-flex align-items-center justify-content-end">
												<span class="pr-2 pln-font-smaller"> 	<?=$lang['filtro']['card_ordenacao']['ordenar_todos']?></span>
												<i id="sendAllToOrder" class="fa fa-arrow-right pln-order-blue pln-cur-pointer pr-2"></i>
											</div>
										</div>
										<div class="row">
											<div id="div_exibicao" class="col-sm-12 d-flex flex-column mt-3 pln-content-scroll">
												<div id="div_exibition">
												<?php
													foreach($ViewData['Cols'] as $Col){
													?>
														<div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
															<input type="checkbox" name='chkvetor[]' id='chkvetor_<?= $Col ?>' value='<?= $Col ?>'>
															<label class='text-truncate pl-1'> <?= $lang['vw_scr_scm420g_sug'][$Col] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
														</div>
													<?php
														}
													?>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
								<div class="col-sm-6">
									<fieldset class="form-group">
										<legend>
											<span>
												<?=$lang['filtro']['card_ordenacao']['titulo']?> 
												<i class="fa fa-info-circle pln-order-blue pln-cur-pointer" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="<?=$lang['filtro']['card_ordenacao']['helper']?>"></i>
											</span>
										</legend>
										<div class="row">
											<div class="col-sm-12 d-flex align-items-center justify-content-between div_order_top mt-1">
												<span id="cleanOrder" class="hideOrder pln-cur-pointer pln-font-smaller"><i id="sendAllToOrder" class="fa fa-times pln-danger pln-cur-pointer"></i> <?=$lang['filtro']['card_ordenacao']['remover_todos']?></span>
												<span class="hideOrder">
													<i data-toggle="tooltip" title="Mover para cima" class="fa fa-arrow-up pln-order-blue pln-cur-pointer i_multiorder_order"></i>
													<i data-toggle="tooltip" title="Mover para baixo" class="fa fa-arrow-down pln-order-blue pln-cur-pointer i_multiorder_order ml-3"></i>
												</span>
												<span class="hideOrder pln-font-smaller">
                                                    <?=$lang['filtro']['sort_data']?>
                                                    <img title="<?=$lang['filtro']['sort_data']?>" src="../../../resources/img/sort.png">
                                                </span>
											</div>
										</div>
										<div class="row">
											<div id="div_order" class="col-sm-12 mt-3 pln-content-scroll">
												
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>	
					</div>																		
				</div>
				<div class="row">
					<!-- card -->
					<div class="col-sm-12">
						<div class="element-box">
							<fieldset class="form-group">
								<legend><span><?=$lang['filtro']['card_filtros_especiais']['titulo']?></span></legend>									
								<div class="row">
									<div class="col-sm-4 d-flex flex-row align-items-center">
										<div class="form-group">
											<label for=""><?=$lang['filtro']['card_filtros_especiais']['cobertura_abaixo_dias']?></label>
											<input type="text" name="inv_days" id="inv_days" class="form-control col-11">
										</div>
									</div>
									<div class="col-sm-4 d-flex flex-row align-items-center">
										<div>
											<label for=""><?=$lang['filtro']['card_filtros_especiais']['ordem']?></label>
										</div>
										<div class="ml-3">
											<input name="purchase_order[]" id="purchase_order_1" value="1" type="checkbox"> 
											<label for=""><?=$lang['filtro']['card_filtros_especiais']['sim']?></label>
											<input class="ml-1" name="purchase_order[]" id="purchase_order_0" value="0" type="checkbox"> 
											<label for=""><?=$lang['filtro']['card_filtros_especiais']['nao']?></label>
										</div>
									</div>
									<div class="col-sm-4 d-flex flex-row align-items-center">
										<div>
											<label for="purchase_req"><?=$lang['filtro']['card_filtros_especiais']['solicitacao']?></label>
										</div>
										<div class="ml-3">
											<input id="purchase_req_1" name="purchase_req[]" value="1" type="checkbox"> 
											<label for=""><?=$lang['filtro']['card_filtros_especiais']['sim']?></label>
											<input class="ml-1" id="purchase_req_0" name="purchase_req[]" value="0" type="checkbox"> 
											<label for=""><?=$lang['filtro']['card_filtros_especiais']['nao']?></label>
										</div>
									</div>
								</div>

								<div class="row mt-2">
									<div class="col-sm-4 d-flex flex-row align-items-center">
										<div>
											<label for=""><?=$lang['sugcompras_form']['card_filtros_especiais']['cob_abaixo_min']?></label>
											<input type="checkbox" name="inv_days_min[]" id="inv_days_min_1" value="1" class="form-control">
										</div>
									</div>										
									<div class="col-sm-4 d-flex flex-row align-items-center">
										<div>
											<label for=""><?=$lang['sugcompras_form']['card_filtros_especiais']['urgente']?></label>
										</div>
										<div class="ml-3"> 
											<input id="sit_urgent_1" name="sit_urgent[]" value="1" type="checkbox"> 
											<label for=""><?=$lang['filtro']['card_filtros_especiais']['sim']?></label>
											<input class="ml-1" id="sit_urgent_0" value="0" name="sit_urgent[]" type="checkbox"> 
											<label for=""><?=$lang['filtro']['card_filtros_especiais']['nao']?></label>
										</div>
									</div>
									<div class="col-sm-4 d-flex flex-row align-items-center">
										<div>
											<label for=""><?=$lang['filtro']['card_filtros_especiais']['atrasado']?></label>
											<input type="checkbox" name="risk_supplier[]" id="risk_supplier_1" value="1" class="form-control">
										</div>
									</div>
								</div>

								
							</fieldset>
						</div>	
					</div>
				</div>
				<!--LINHA -->
				<div class="row">
					<div class="col-sm-7">
						<div class="element-box min-box-height-parametros">
							<fieldset class="form-group">
								<legend><span><?=$lang['filtro']['card_parametros']['titulo']?></span></legend>
								<div class="row">
									<div class="col-sm-7">
										<div class="row">
											<div class="col-sm-12 font-weight-bold"><?=$lang['filtro']['card_parametros']['sinalizador']?></div>
										</div>
										<div class="row mt-2">
											<div class="col-sm-12">
												<input name="inv_level[]" id="inv_level_0" value='0' type="checkbox"> <label for="inv_level_0"><?=$lang['filtro']['card_parametros']['zero']?></label>
												<input name="inv_level[]" id="inv_level_1" value='1' class="ml-2" type="checkbox"> <label for="inv_level_1"><?=$lang['filtro']['card_parametros']['min2']?></label>
												<input name="inv_level[]" id="inv_level_2" value='2' class="ml-2" type="checkbox"> <label for="inv_level_2"><?=$lang['filtro']['card_parametros']['min']?></label>
											</div>
											<div class="col-sm-12">
												<input name="inv_level[]" id="inv_level_3" value='3' type="checkbox"> <label for="inv_level_3"><?=$lang['filtro']['card_parametros']['alvo']?></label>
												<input name="inv_level[]" id="inv_level_4" value='4' class="ml-2" type="checkbox"> <label for="inv_level_4"><?=$lang['filtro']['card_parametros']['max']?></label>
												<input name="inv_level[]" id="inv_level_5" value='5' class="ml-2" type="checkbox"> <label for="inv_level_5"><?=$lang['filtro']['card_parametros']['max2']?></label>
											</div>                                  
										</div>

										<div class="row mt-3">
											<div class="col-sm-12 font-weight-bold"><?=$lang['filtro']['card_parametros']['ciclo']?></div>
										</div>
										<div class="row mt-2">
											<div class="col-sm-12">
												<input name="sku_life_cycle[]" id="sku_life_cycle_1" value="1" type="checkbox"> <label for=""><?=$lang['filtro']['card_parametros']['nova']?></label>
												<input name="sku_life_cycle[]" id="sku_life_cycle_2" value="2" class="ml-2" type="checkbox"> <label for=""><?=$lang['filtro']['card_parametros']['madura']?></label>
												<input name="sku_life_cycle[]" id="sku_life_cycle_3" value="3" class="ml-2" type="checkbox"> <label for=""><?=$lang['filtro']['card_parametros']['desc']?></label>
											</div>                                                                  
										</div>
									</div>
									<div class="col-sm-5">
										<div class="row">
											<div class="col-sm-12 font-weight-bold"><?=$lang['filtro']['card_parametros']['politica']?></div>
										</div>
										<div class="row mt-2">
											<div class="col-sm-12 pln-content-scroll">
												<?php
												foreach($ViewData['Profile'] as $Profile){
													?>
													<div class='d-flex flex-row text-nowrap align-items-center'>
													<input type='checkbox' name='id_profile[]' id='id_profile_<?= $Profile['ID_PROFILE_PK']?>' value='<?= $Profile['ID_PROFILE_PK']?>'>
													<label class='text-truncate pl-1' for='id_profile_<?= $Profile['ID_PROFILE_PK']?>'><?= $Profile['NOME'] ?></label>
													</div>
													<?php
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="element-box min-box-height-l4">                            
							<fieldset class="form-group mb-0">
								<legend><span><?php echo $lang['sku_form']['data']['titulo']; ?></span></legend>
								<div>
									<div class="form-group">
										<label for=""> <?php echo $lang['sku_form']['data']['alterada']; ?></label>
										<select id="data_alterada_chegada" name="data_alterada_chegada" class="form-control">													
											<option selected value=""><?php echo $lang['sku_form']['data']['data_alterada_select']['selecione']; ?></option>
											<option value="PERSONALIZADO"><?php echo $lang['sku_form']['data']['data_alterada_select']['personalizado']; ?></option>
											<option value="MES_ATUAL"><?php echo $lang['sku_form']['data']['data_alterada_select']['esse_mes']; ?></option>
											<option value="PR_7DIAS"><?php echo $lang['sku_form']['data']['data_alterada_select']['sete_dias']; ?></option>
											<option value="PR_30DIAS"><?php echo $lang['sku_form']['data']['data_alterada_select']['trinta_dias']; ?></option>
											<option value="PR_SEMANA"><?php echo $lang['sku_form']['data']['data_alterada_select']['proxima_semana']; ?></option>
											<option value="PR_MES"><?php echo $lang['sku_form']['data']['data_alterada_select']['proximo_mes']; ?></option>
										</select>												
									</div>
								</div>
								<div>
									<div class="form-group w-100">
										<p class="pln-invi" id="show_date_label"></p>		
										<div class="interval d-flex flex-row align-items-center">
											<select class="form-control float-left mr-2 w-25" id="interval_select" name="interval_select" >
												<option value="interval"><?php echo $lang['sku_form']['data']['intervalo']['intervalo']; ?></option>
												<option value="next"><?php echo $lang['sku_form']['data']['intervalo']['proximos']; ?></option>
											</select>
											<div class="interval-dates d-flex flex-row align-items-center">
												<input type="date" id="interval_start" name="interval_start" class="float-left form-control pln-input-date">
												<span class="float-left mx-2"><?php echo $lang['sku_form']['data']['ate']; ?></span>
												<input type="date" id="interval_end" name="interval_end" class="float-left form-control pln-input-date">
											</div>
											<div class="d-flex flex-row align-items-center">
												<div class="interval-days d-flex flex-row align-items-center" style="display: none !important;">
													<input type="number" name="interval_days" id="interval_days" class="form-control" maxlength="5" min="1" max="99">
													<label class="pl-2 d-flex flex-row align-items-center" > <?php echo $lang['sku_form']['data']['dias']; ?> </label>
												</div>	
											</div>	
										</div>
			
									</div>
								</div>
								<div>
									<div class="form-group mb-0">
										<label for=""><?=$lang['sku_form']['data']['proximas_parcs']?></label>
										<div class="d-flex flex-row align-items-center">
											<input type="number" class="form-control w-25" id="num_parc_pk" name="num_parc_pk" maxlength="2" min="1" max="99">
											<label class="pl-2 d-flex flex-row align-items-center" > <?php echo $lang['sku_form']['data']['parcs']; ?> </label>
										</div>										
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="element-box min-box-height-l5">
                            <fieldset class="form-group">
								<legend>									
									<span onclick="hide_filter('ipt_find_gestor')" class="pln-cur-pointer">
										<?=$lang['filtro']['card_gestor']['titulo']?> <i class="fa fa-search pln-link-color"></i>
									</span>
								</legend>
                                <div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar gestor " class="form-control pln-invi mb-1" id="ipt_find_gestor" onkeyup="search_filter('ipt_find_gestor', 'div_gestor')">
									</div>
                                    <div class="col-sm-12 pln-content-scroll" id="div_gestor">
                                        <?php
                                        foreach($ViewData['User'] as $Key => $User){
                                            ?>
                                            <div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name='user_id[]' id='user_id_<?= $User['ID_USER_PK']?>' value='<?= $User['ID_USER_PK']?>'>
                                                <label class="text-truncate pl-1" for='user_id_<?= $User['ID_USER_PK']?>'><?= $User['LOGIN']?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
					<!-- DEPOSITO PADRAO -->
						<div class="col-sm-4">
							<div class="element-box pln-sugestao-min-box-height-l1">
								<fieldset class="form-group">
									<legend><span onclick="hide_filter('ipt_find_deposito_padrao')" class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['deposito_padrao'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
									<div class="row">
										<div class="col-sm-12">
											<input type="text" placeholder="filtrar classe" class="form-control pln-invi mb-1" id="ipt_find_deposito_padrao" onkeyup="search_filter('ipt_find_deposito_padrao', 'div_filter_deposito_padrao')">
										</div>
										<div class="col-sm-12">
											<input type="checkbox" class="chk_check_all" id="chk_check_all_deposito_padrao"> <label for="chk_check_all_deposito_padrao"><?= $lang['parametros']['todos'] ?></label>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12 pln-content-scroll" id="div_filter_deposito_padrao">
										<?php
										foreach ($ViewData['Deposit'] as $Key => $Deposit) {
										?>
											<div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
											<input type='checkbox' name="default_local[]" id="default_local_filtro_<?= $Deposit['COD_LOCAL_PK'] ?>" value="<?= $Deposit['COD_LOCAL_PK'] ?>">
											<label class="text-truncate pl-1" for='default_local_filtro_<?= $Deposit['COD_LOCAL_PK'] ?>'><?= $Deposit['DESCRICAO'] ?></label>
											</div>
										<?php
										}
										?>
										</div>
									</div>
								</fieldset>
							</div>
						<!-- FIM DEPOSITO PADRAO -->
						</div>
                    <!-- FILTROS DINAMICOS -->
						<?php foreach( $ViewData['Groups'] as $key => $Group) { ?>
							<div class="col-sm-4">
								<div class="element-box min-box-height-l5">
									<fieldset class="form-group">

										<legend><span onclick="hide_filter('ipt_find_<?php echo $key ?>')" class="pln-cur-pointer"><?php echo $Group ?> <i class="fa fa-search pln-link-color"></i></span></legend>
										<div class="row">
											<div class="col-sm-12">
												<input type="text" placeholder="filtrar <?php echo $Group?>" class="form-control pln-invi mb-1" id="ipt_find_<?php echo $key ?>" onkeyup="search_filter('ipt_find_<?php echo $key ?>', 'div_filter_<?php echo $key ?>')">
											</div>
											<div class="col-sm-12">
												<input type="checkbox" class="chk_check_all" id="chk_check_all_<?php echo $key?>"> <label for="chk_check_all_<?php echo $key?>"><?= $lang['parametros']['todos'] ?></label>
											</div>
										</div>

										<div class="row">
											<div class="col-sm-12 pln-content-scroll" id="div_filter_<?php echo $key ?>">
											<?php
											foreach ($ViewData['Group_' . $key] as $Key2 => $Campo) {
											?>
												<div class="d-flex <?= ($Key2 == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
													<input type='checkbox' name='cod_group<?php echo $key ?>_fk[]' id='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>' value='<?= $Campo['COD_GROUP_PK'] ?>'>
													<label class='text-truncate pl-1' for='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>'><?= $Campo['DESC_GROUP_CONT'] ?></label>
												</div>
											<?php
											}
											?>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						<?php }?>
					<!--END FILTROS DINAMICOS -->
					</div>
				<!--END - Recent Ticket Comments-->
			</form>
		</div>
	</div>
</div>
<p id="demo"></p>
<div class="display-type"></div>

	<div class="modal fade" id="modal_edit" data-keyboard="true" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="botao_editar" aria-hidden="true">
		<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_titulo" name="modal_titulo"><?=$lang['filtro']['new_filter']?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="<?=$lang['global']['close']?>" id>
						<span aria-hidden="true">x</span>
					</button>
				</div>
				<div class="modal-body">

				<div style="width: 100%">
					<input placeholder="<?=$lang['filtro']['fill_filter_name'] ?>" maxlength="50" type="text" class="form-control" name="filter_nome" id="filter_nome" >
				</div>
				<div style="width: 100%; margin-top: 5px">
					<input name="filter_perfil" id="filter_perfil" type="hidden" value="Publico">
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-between">
				<span class="pln-danger pln-cur-pointer" id="confirm_delete" onclick="confirmDeleteFilter()" ><i class="fa fa-times pln-danger"></i> <span><?=$lang['filtro']['delete']?></span></span>
				<button type="button" class="btn pln-btn-orange" onclick="saveFilter()" id="btnfiltro_salvar"><span id="spn_save"><?=$lang['filtro']['save']?></span> <i class="fa fa-spin fa-spinner pln-invi"></i></button>
			</div>
		</div>
	</div>
</div>

<div class="clear"></div>
<?php
$JS = ['uglify/all-suggest-test-filters-min'];
?>

