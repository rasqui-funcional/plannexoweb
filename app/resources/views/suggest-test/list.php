<!-- Google Charts Loader -->
<script src="https://www.gstatic.com/charts/loader.js"></script>
<?php
$JS= [
    'uglify/all-suggest-test-list-min',
    "i18n/{$Country}/manualsuggest",
];
?>
<div class="content-i">
    <div class="content-full-table">
        <div style="" class="element-wrapper">
            <!-- title header -->
            <div class="title-header d-flex flex-row align-items-end mt-3">
                <div>
                    <a href="#" data-route="/<?=$Country?>/suggest-test/filters" onclick="loaderShow()" class="pln-link-color back-to-the-filters">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i> <?=$lang['sugcompras_lista']['voltar']?>
                    </a>
                    <h4 class="pln-font-color"><?=$lang['sugcompras_lista']['titulo']?></h4>
                </div>
                
                <div class="d-flex align-items-end pln-items-search-sug ml-3 mb-2" style="display: none !important">
                    <div class="d-flex flex-column align-items-start">
                        
                        <span>
                            <select class="form-control form-control-sm" id="suggest-seach-col">
                                <option value="id_psug_pk"><?=$lang['vw_scr_scm420g_sug']['id_psug_pk']?></option>
                                <option value="cod_item_pk"><?=$lang['vw_scr_scm420g_sug']['cod_item_pk']?></option>
                                <option value="desc_item"><?=$lang['vw_scr_scm420g_sug']['desc_item']?></option>
                                <option value="cod_estab_fk"><?=$lang['vw_scr_scm420g_sug']['cod_estab_fk']?></option>
                                <option value="id_sku_pk"><?=$lang['vw_scr_scm420g_sug']['id_sku_pk']?></option>
                                <option value="curve_abc"><?=$lang['vw_scr_scm420g_sug']['curve_abc']?></option>
                                <option value="curve_pqr"><?=$lang['vw_scr_scm420g_sug']['curve_pqr']?></option>
                                <option value="curve_xyz"><?=$lang['vw_scr_scm420g_sug']['curve_xyz']?></option>
                                <option value="curve_123"><?=$lang['vw_scr_scm420g_sug']['curve_123']?></option>
                                <option value="desc_supplier"><?=$lang['vw_scr_scm420g_sug']['fornecedor_nome']?></option>
                                <?php foreach($ViewData['SearchFields'] as $key => $value): ?>
                                    <option value="cod_group<?= $key ?>_fk"><?= $value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </span>
                    </div>

                    <div class="d-flex flex-row align-items-end ml-1">
                        <span><input type="text" class="form-control form-control-sm col" id="suggest_search_input" /></span>
                        <span class="ml-1">
                            <button type="button" class="btn pln-btn-ghost" onclick="suggestSearch()" />
                            <?=$lang['sugcompras_lista']['buscar']?>
                        </span>
                    </div>
                </div>

                <span class="ml-auto pln-items-search-sug mb-2 mr-2" style="display: none !important">
                    <span id="download_csv_suggest_list" class="pln-link-color pln-cur-pointer mr-3 d-none d-xl-inline-block">
                        <i class="fas fa-file-csv mr-1"></i>
                        <?php echo $lang['resumo_lista']['csv'] ?>
                    </span>
                    
                    <button type="button" id="openFullscreen" class="btn pln-btn-ghost d-none d-xl-inline-block"><i class="fa fa-arrows-alt mr-1" aria-hidden="true"></i> <?php echo $lang['botao']['expandir'] ?></button>
                    <button type="button" class="btn pln-btn-ghost" data-toggle="modal" id="open_modal_manual_suggest" data-target="#modal_manual_suggest"><i class="fa fa-plus mr-1"></i> <?=$lang['sugcompras_lista']['add_sug']?> </button>
                    <button type="button" onclick="saveData()" class="btn pln-btn-orange ml-2"><i class="fa fa-save mr-1"></i> <?=$lang['sugcompras_lista']['salvar']?> </button>
                    <div class="dropdown d-inline-block d-xl-none ml-1">
                        <button class="btn dropdown-toggle pln-more-options" type="button" id="dropdownMoreOptions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMoreOptions">
                            <div class="dropdown-item">
                                <span id="download_csv_suggest_list_more_options" class="pln-link-color pln-cur-pointer mr-3">
                                    <i class="fas fa-file-csv mr-1"></i>
                                    <?php echo $lang['resumo_lista']['csv'] ?>
                                </span>
                            </div>
                            <div class="dropdown-item">
                                <span id="openFullscreenMoreOptions" class="pln-text-color-primary pln-cur-pointer mr-3">
                                    <i class="fa fa-arrows-alt mr-1" aria-hidden="true"></i>
                                    <?php echo $lang['botao']['expandir'] ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </span>
            </div>
            <!-- title header end -->
            <div class="element-box-full">
                <input type="hidden" id="checkFormWithoutSaving" value="true">
                <div id="table_insert">
                    <form id="formulario">
                        <div id="controlSize" class="table-responsive">
                            <table id="tableSug" class="table dataTable suggest-table bigger-table">

                                <tfoot class="hide">
                                    <tr>
                                        <th colspan="2">
                                            <?= $lang['sugcompras_lista']['itens']['total'] ?>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="d-flex mt-1 align-items-center pln-items-search-sug" style="display: none !important">

                    <div class="d-flex flex-row align-items-center ml-3 mr-3">

                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?=$lang['sugcompras_lista']['pagina']['qtd_a']?></span>
                            <span id="page_items_to"></span>
                            <span><?=$lang['sugcompras_lista']['pagina']['qtd_de']?></span>
                            <span id="page_items_total"></span>
                        </span>
                        <div class="d-flex flex-row">
                            <div class="d-flex flex-column mr-3">
                                <small>
                                    <?php echo $lang['sugcompras_lista']['itens']['mostrando'] ?>
                                    <span id="total_items"></span>
                                    <?php echo $lang['sugcompras_lista']['itens']['sku'] ?>
                                </small>

                                <small>
                                    <?=$lang['sugcompras_lista']['pagina']['qtd_total']?>
                                    <span id="spn_total_page_number"></span>
                                </small>
                            </div>
                            
                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>
                        </div>
                    </div>

                    <div class="d-flex flex-row align-items-center">
                        <span class="pln-bold-600"><?=$lang['sugcompras_lista']['pagina']['titulo']?></span>
                        <span class="ml-1"><input type="number" min="0" maxlength="10" id="goto_pagenumber" class="form-control form-control-sm pln-input-60" /></span>
                        <span class="ml-1"><button type="button" onclick="goToPageNumber()" class="btn pln-btn-ghost" /><?=$lang['sugcompras_lista']['buscar']?></span>
                    </div>
                    <div class="d-flex flex-row align-items-center ml-auto mr-3">
                        <span class="pln-bold-600"><?=$lang['sugcompras_lista']['visualizar']?></span>
                        <?php include_once('resources/views/items-per-page-test.php'); ?>
                        <span class="ml-1"><?=$lang['sugcompras_lista']['por_pagina']?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" id="modal_order" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label_order">
                <?=$lang['sugcompras_lista']['ordem']['titulo']?>
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span><small><?=$lang['modal']['fechar']?></small></button>
            </div>
            <div class="modal-body">
                <div id="order_loading" class="text-center"><i class='fa fa-spin fa-spinner'></i> <?=$lang['sugcompras_lista']['carregando']?></div>
                <div id="include_order">
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('resources/views/suggest-test/manualsuggest.php'); ?>
<?php include_once('resources/views/infosku/index.php'); ?>
<?php include_once('resources/views/modals/modal_excess.php'); ?>
<?php include_once('resources/views/suggest-test/moreinfo.php'); ?>