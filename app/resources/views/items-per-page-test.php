<span class="ml-1">
    <select id="items_per_page" class="form-control form-control-sm">
        <option selected value="20">20</option>
        <option value="30">30</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="250">250</option>
        <option value="500">500</option>
<!--        <option value="1000">1000</option>-->
    </select>
</span>