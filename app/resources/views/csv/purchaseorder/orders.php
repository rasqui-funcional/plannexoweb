<?php
    if(isset($ViewData['error'])) {
        echo $ViewData['error'];
        exit;
    }

    Request::generateCSVHeaders($lang['ordem_compra_filtros']['titulo']);

    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    $translatedTitles = [];

    $lt_total_out_index = array_search('LT_TOTAL_OUT', $ViewData['table']['columns']);
    unset($ViewData['table']['columns'][$lt_total_out_index]);
    $out_6_index = array_search('OUT6', $ViewData['table']['columns']);
    unset($ViewData['table']['columns'][$out_6_index]);

    foreach($ViewData['table']['columns'] as $title) {
        $translatedTitles[$title] = $lang['column'][strtolower($title)];
    }

    fputcsv($output, mb_convert_encoding($translatedTitles, 'ISO-8859-1'), ';');

    // loop over the rows, outputting them
    foreach($ViewData['table']['data'] as $row) {
        unset($row['LT_TOTAL_OUT']);
        unset($row['OUT6']);
        $row['DATE_REQ'] = date('d/m/Y', strtotime($row['DATE_REQ']));
        $row['DATE_ORDER'] = date('d/m/Y', strtotime($row['DATE_ORDER']));
        $row['DATE_EXP'] = date('d/m/Y', strtotime($row['DATE_EXP']));
        fputcsv($output, mb_convert_encoding($row, 'ISO-8859-1'), ';');
    }
?>