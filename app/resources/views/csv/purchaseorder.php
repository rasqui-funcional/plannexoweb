<?php
if (isset($ViewData['error'])) {
    echo $ViewData['error'];
    exit;
}

    Request::generateCSVHeaders($lang['ordem_compra_filtros']['titulo']);

    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    $translatedTitles = [];
    // $translatedTitles['total_orders'] = $lang['ordem_compra_filtros']['col_ordens'];
    foreach($ViewData['table']['columns'] as $title) {
        $translatedTitles[$title] = $lang['column'][strtolower($title)];
    }

    fputcsv($output, mb_convert_encoding($translatedTitles, 'ISO-8859-1'), ';');

    // loop over the rows, outputting them
    foreach($ViewData['table']['data'] as $row) {
        $data = [];
        foreach ($row as $key => $value) {
            $data[$key] = Utilities::formatCsvFields($key, $value);
        }

        fputcsv($output, mb_convert_encoding($data, 'ISO-8859-1'), ';');
    }

?>