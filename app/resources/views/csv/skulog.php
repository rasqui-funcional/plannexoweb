<?php
if (isset($ViewData['error'])) {
    echo $ViewData['error'];
    exit;
}

Request::generateCSVHeaders($lang['log']['csv_name']);

$output = fopen('php://output', 'w');

$translatedTitles = [];
foreach (array_keys($lang['log']) as $title) {
    $translatedTitles[$title] = $lang['log'][$title];
}
array_pop($translatedTitles);
fputcsv($output, mb_convert_encoding($translatedTitles, 'ISO-8859-1'), ';');

foreach ($ViewData['table'] as $row) {
    $description = Utilities::formatDescriptionJsonLog($row['CREATOR'], $row['DESCRIPTION']);
    $row['ALTERED_COLUMN'] = $description['ALTERED_COLUMN'];
    $row['OLD_VALUE'] = $description['OLD_VALUE'];
    $row['NEW_VALUE'] = $description['NEW_VALUE'];
    unset($row['DESCRIPTION']);

    fputcsv($output, mb_convert_encoding($row, 'ISO-8859-1'), ';');
}
