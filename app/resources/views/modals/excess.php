
<table class="table dataTable">
    <thead>
    <tr>
        <th><?=$lang['sugcompras_lista']['excesso']['estab']?></th>
        <th><?=$lang['sugcompras_lista']['excesso']['qtd_excesso']?></th>
        <th><?=$lang['sugcompras_lista']['excesso']['cobertura']?></th>
    </tr>
    </thead>    
    <tbody>
    <?php
        if($ViewData['estab_excess']){
            foreach ($ViewData['estab_excess'] as $key => $Data) {
        ?>
            <tr>
                <td><?=$Data['COD_ESTAB_FK'] ?></td>
                <td><?=$Data['QTY_EXCESS'] ?></td>
                <td><?=$Data['INV_DAYS'] ?></td>
            </tr>
        <?php
            }
        }
    ?>
    </tbody>    
</table>