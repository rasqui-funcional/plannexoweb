<div aria-hidden="true" id="modal_excess" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label_excess">
                <?=$lang['sugcompras_lista']['excesso']['itens']?>
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span><small><?=$lang['modal']['fechar']?></small></button>
            </div>
            <div class="modal-body">
                <div id="excess_loading" class="text-center"><i class='fa fa-spin fa-spinner'></i> <?=$lang['sugcompras_lista']['carregando']?></div>
                <div id="include_excess">
                </div>
            </div>
        </div>
    </div>
</div>