<table class="table dataTable">
    <thead>
    <tr>
        <th><?= $lang['sugcompras_lista']['ordem']['cod'] ?></th>
        <th><?= $lang['sugcompras_lista']['ordem']['fornec'] ?></th>
        <th><?= $lang['sugcompras_lista']['ordem']['qtd_nec'] ?></th>
        <th><?= $lang['sugcompras_lista']['ordem']['qtd_rec'] ?></th>
        <th><?= $lang['sugcompras_lista']['ordem']['qtd_pendente'] ?></th>
        <th><?= $lang['sugcompras_lista']['ordem']['data_ordem'] ?></th>
        <th><?= $lang['sugcompras_lista']['ordem']['data_esperada'] ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    if ($ViewData['orders']) {
        foreach ($ViewData['orders'] as $key => $Data) {
            ?>
            <tr>
                <td><?= $Data['NUM_ORDER'] ?></td>
                <td><?= $Data['DESC_SUPPLIER'] ?></td>
                <td><?= Utilities::bigNumber($Data['QTY_ORIG']) ?></td>
                <td><?= Utilities::bigNumber($Data['QTY_REC']) ?></td>
                <td><?= Utilities::bigNumber($Data['QTY_PARC']) ?></td>
                <td><?= ($Data['DATE_ORDER'] ? date('d/m/Y', strtotime($Data['DATE_ORDER'])) : null) ?></td>
                <td><?= $Data['DATE_EXP'] ?></td>
            </tr>
            <?php
        }
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <th><?= $lang['global']['total_all']?>: (<?= $ViewData['total'][0]['COUNT(*)'] ?>)</th>
        <th></th>
        <th></th>
        <th></th>
        <th><?= Utilities::bigNumber($ViewData['total'][0]['QTY_PARC']) ?></th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>
</table>