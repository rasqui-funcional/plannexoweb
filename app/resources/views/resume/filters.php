<!--<script>
	var filter_temp = ;
	delete filter_temp.filter_temp;
</script>
-->
<?php //print_r($ViewData) ?>

<div class="content-i pln-content">
	<div class="content-box">
		<!--START - Recent Ticket Comments-->
		<div class="element-wrapper">
			<div class="d-flex flex-row justify-content-between mb-2 align-items-center">
				<div>
					<h4 class="pln-font-color"><span data-toggle="tooltip" id="spn_filter_id" title="<?= $lang['resumo']['view'] ?>"><?php echo $lang['resumo_filtros']['titulo'] ?></span></h4>
				</div>
				<div class="d-flex flex-row">
					<span class="pln-bold-600 mt-1 mr-1"><?=$lang['filtro']['my_filters']?>:</span>
					<div class="d-flex flex-column align-items-end">
						<div>
							<select id="select_filtro" name="select_filtro" class="form-control-sm pln-select-filter-size">
								<option value=""></option>
								<?php
								if ($ViewData['select_filtro']) {
									foreach ($ViewData['select_filtro'] as $Select) {
										?>
										<option <?= (isset($Select['disable']) ? 'disable' : null) ?> value="<?= $Select['value'] ?>"><?= $Select['name'] ?></option>
										<?php

									}
								}
								?>
							</select>
						</div>
						<div class="align-self-start">
							<span id="btn_filtro"  data-toggle="modal" data-target="#modal_edit" class="pln-link-color pln-font-smaller pln-cur-pointer">
								<span id="btn_filtro_text"><?=$lang['filtro']['save_filter']?></span>
							</span>
						</div>
					</div>
					<div class="d-flex flex-column align-items-end ml-4">
						<button type="button" id="btn_search" class="btn pln-btn-orange">
							<?= $lang['parametros']['pesquisa'] ?>
						</button>

						<span class="pln-link-color pln-font-smaller pln-cur-pointer align-self-start" id="clean_form">
							<?=$lang['filtro']['clean_filters']?>
						</span>
					</div>
				</div>
			</div>
			<hr>
			<form name="formulario" id="formulario" method="post" action="/<?= $Country ?>/resume/list">
				<!-- #plannexo AQUI VEM O CONTEÚDO DA PÁGINA -->
				<input type="hidden" name="filter_temp" id="filter_temp" />

				<!-- FIM ORGANIZAÇÃO -->

				<!-- AGRUPAR POR -->
				<div class="row">
					<div class="col-sm-12">
						<span class="pln-box-title pln-font-color"><?php echo $lang['resumo_filtros']['agrupamentos']; ?></span>
					</div>
				</div>
				<div class="row">

					<div class="col-sm-4">
						<div class="element-box min-box-height-l2">
							<fieldset class="form-group">
								<legend><span onclick="hide_filter('ipt_find_agrupar')" class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['agrupar'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar agrupamento" class="form-control pln-invi mb-1" id="ipt_find_agrupar" onkeyup="search_filter('ipt_find_agrupar', 'div_filter_agrupar')">
									</div>
									<div class="col-sm-12">
										<input type="checkbox" class="chk_check_all" id="chk_check_all_agrupar"> <label for="chk_check_all_agrupar"><?= $lang['parametros']['todos'] ?></label>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-12 pln-content-scroll" id="div_filter_agrupar">
										<div class="d-flex mt-2 flex-row text-nowrap align-items-center">
											<input type='checkbox' name="group_columns[]" id="group_columns_curve_abc" value="curve_abc">
											<label class="text-truncate pl-1" for='group_columns_curve_abc'>
                                                <?= $lang['resumo_filtros']['colunas']['abc'] ?>
                                            </label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="group_columns[]" id="group_columns_curve_xyz" value="curve_xyz">
											<label class="text-truncate pl-1" for='group_columns_curve_xyz'>
                                                <?= $lang['resumo_filtros']['colunas']['xyz'] ?>
                                            </label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="group_columns[]" id="group_columns_curve_pqr" value="curve_pqr">
											<label class="text-truncate pl-1" for='group_columns_curve_pqr'>
                                                <?= $lang['resumo_filtros']['colunas']['pqr'] ?>
                                            </label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="group_columns[]" id="group_columns_curve_123" value="curve_123">
											<label class="text-truncate pl-1" for='group_columns_curve_123'>
                                                <?= $lang['resumo_filtros']['colunas']['123'] ?>
                                            </label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="group_columns[]" id="group_columns_inv_level" value="inv_level">
											<label class="text-truncate pl-1" for='group_columns_inv_level'>
                                                <?= $lang['resumo_filtros']['colunas']['sinalizador'] ?>
                                            </label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="group_columns[]" id="group_columns_sku_life_cycle" value="sku_life_cycle">
											<label class="text-truncate pl-1" for='group_columns_sku_life_cycle'>
                                                <?= $lang['resumo_filtros']['colunas']['life_cycle'] ?>
                                            </label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="group_columns[]" id="group_columns_cod_estab_fk" value="cod_estab_fk">
											<label class="text-truncate pl-1" for='group_columns_cod_estab_fk'>
                                                <?= $lang['resumo_filtros']['colunas']['estab'] ?>
                                            </label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="group_columns[]" id="group_columns_id_profile_fk" value="id_profile_fk">
											<label class="text-truncate pl-1" for='group_columns_id_profile_fk'>
                                                <?= $lang['resumo_filtros']['colunas']['politica'] ?>
                                            </label>
										</div>
										<div class="d-flex flex-row text-nowrap align-items-center">
											<input type='checkbox' name="group_columns[]" id="group_columns_user_id" value="user_id">
											<label class="text-truncate pl-1" for='group_columns_user_id'>
                                                <?= $lang['resumo_filtros']['colunas']['gestor'] ?>
                                            </label>
										</div>
                                        <!--<div class="d-flex flex-row text-nowrap align-items-center">
                                            <input type='checkbox' name="group_columns[]" id="cod_group_fk_inv_level" value="standard_deposit">
                                            <label class="text-truncate pl-1" for='standard_deposit'>
                                                <?//= $lang['resumo_filtros']['colunas']['deposito_padrao'] ?>
                                            </label>
                                        </div>-->

										<?php foreach ($ViewData['SearchFields'] as $key => $value) { ?>
    										<div class="d-flex flex-row text-nowrap align-items-center">
												<input type="checkbox" name="group_columns[]" id="group_columns_cod_group<?=$key?>_fk" value="cod_group<?=$key?>_fk" >
                                                <label class="text-truncate pl-1" for='cod_group<?=$key?>_fk' >
                                                    <?=$value?>
                                                </label>
    										</div>
										<?php } ?>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<!-- FIM AGRUPAR POR -->
					<div class="col-sm-8">
						<div class="element-box min-box-height-l2">
							<div class="row d-flex flex-row">
								<div class="col-sm-6">
									<fieldset class="form-group">
										<legend>
											<span onclick="hide_filter('ipt_find_exibicao')" class="pln-cur-pointer">
											<?=$lang['filtro']['card_exibicao']['titulo']?> <i class="fa fa-search pln-link-color"></i>
											</span>
										</legend>
										<div class="row">
										<div class="col-sm-12">
											<input type="text" placeholder="filtrar exibição " class="form-control pln-invi mb-1" id="ipt_find_exibicao" onkeyup="search_filter('ipt_find_exibicao', 'div_exibition')">
										</div>
											<div class="col-sm-4 d-flex align-items-center mt-1">
												<input type="checkbox" class="chk_check_all" id="chk_check_all_exibicao" name="chk_check_all_exibicao" >
												<label class="pl-1 pln-font-smaller" for="chk_check_all_exibicao"><?=$lang['parametros']['todos']?></label>
											</div>
											<div class="col-sm-4 d-flex align-items-center justify-content-center mt-1">
												<i data-toggle="tooltip" title="Mover para cima" class="fa fa-arrow-up pln-order-blue pln-cur-pointer i_multiorder_exibition"></i>
												<i data-toggle="tooltip" title="Mover para baixo" class="fa fa-arrow-down pln-order-blue pln-cur-pointer i_multiorder_exibition ml-3"></i>
											</div>
											<div class="col-sm-4 d-flex align-items-center justify-content-end">
												<span data-toggle="tooltip" title="Enviar checados para ordenação" class="pr-2 pln-font-smaller"> 	<?=$lang['filtro']['card_ordenacao']['ordenar_todos']?></span>
												<i id="sendAllToOrder" data-toggle="tooltip" title="Enviar checados para ordenação" class="fa fa-arrow-right pln-order-blue pln-cur-pointer pr-2"></i>
											</div>
										</div>
										<div class="row">
											<div id="div_exibicao" class="col-sm-12 d-flex flex-column mt-3 pln-content-scroll">
												<div id="div_exibition">
													<!-- <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_cod_estab_fk' value='cod_estab_fk'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['estab'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_curve_abc' value='curve_abc'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['abc'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_curve_xyz' value='curve_xyz'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['xyz'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_curve_pqr' value='curve_pqr'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['pqr'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_curve_123' value='curve_123'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['123'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
													</div>
													<div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_inv_level' value='inv_level'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['sinalizador'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div> -->
													<?php
														// foreach ($ViewData['SearchFields'] as $Col) {
														?>
														<!-- <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
															<input type="checkbox" name='exibition_columns[]' id='exibition_columns_cod_group<?= $Col['NUM_GROUP_PK'] ?>_fk' value='cod_group<?= $Col['NUM_GROUP_PK'] ?>_fk'>
															<label class='text-truncate pl-1'> <?= $Col['DESC_GROUP'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
														</div> -->
													<?php
														// }
                                                    ?>
                                                    <!-- <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_sku_life_cycle' value='sku_life_cycle'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['life_cycle'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div> -->
                                                    <!-- <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_id_profile_fk' value='id_profile_fk'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['politica'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_user_id' value='user_id'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['gestor'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div> -->
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_total_skus' value='total_skus'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['qtd_itens'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_inv_available_money' value='inv_available_money'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['inv_available_money'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_inv_total_money' value='inv_total_money'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['inv_total_money'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_qty_min_out_money' value='qty_min_out_money'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['min'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_qty_target_money' value='qty_target_money'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['alvo'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_qty_max_out_money' value='qty_max_out_money'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['max'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_qty_excess' value='qty_excess'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['excesso'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_qty_min_out_ori_money' value='qty_min_out_ori_money'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['min_sugerido'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_qty_max_out_ori_money' value='qty_max_out_ori_money'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['max_sugerido'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_qty_min_erp_money' value='qty_min_erp_money'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['min_erp'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_qty_max_erp_money' value='qty_max_erp_money'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['max_erp'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_val_purchase_sugg' value='val_purchase_sugg'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['sugestao_compra'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_val_purchase_req' value='val_purchase_req'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['solicit_compra'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
                                                    <div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                                        <input type="checkbox" name='exibition_columns[]' id='exibition_columns_val_purchase_order' value='val_purchase_order'>
                                                        <label class='text-truncate pl-1'> <?= $lang['resumo_filtros']['colunas']['ordem_compra'] ?> </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                                    </div>
												</div>
											</div>
										</div>
									</fieldset>
								</div>
								<div class="col-sm-6">
									<fieldset class="form-group">
										<legend>
											<span>
												<?=$lang['filtro']['card_ordenacao']['titulo']?>
												<i class="fa fa-info-circle pln-order-blue pln-cur-pointer" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-html="true" data-content="<?=$lang['filtro']['card_ordenacao']['helper']?>"></i>
											</span>
										</legend>
										<div class="row">
											<div class="col-sm-12 d-flex align-items-center justify-content-between div_order_top mt-1">
												<span id="cleanOrder" class="hideOrder pln-cur-pointer pln-font-smaller"><i class="fa fa-times pln-danger pln-cur-pointer"></i> <?=$lang['filtro']['card_ordenacao']['remover_todos']?></span>
												<span class="hideOrder">
													<i data-toggle="tooltip" title="Mover para cima" class="fa fa-arrow-up pln-order-blue pln-cur-pointer i_multiorder_order"></i>
													<i data-toggle="tooltip" title="Mover para baixo" class="fa fa-arrow-down pln-order-blue pln-cur-pointer i_multiorder_order ml-3"></i>
												</span>
												<span class="hideOrder pln-font-smaller"><?=$lang['filtro']['sort_data']?> <img title="<?=$lang['filtro']['sort_data']?>" src="../../../resources/img/sort.png"></span>
											</div>
										</div>
										<div class="row">
											<div id="div_order" class="col-sm-12 mt-3 pln-content-scroll">

											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
				</div>
          <!-- FIM ORGANIZAÇÃO-->
				</div>

				<!-- FILTROS CURVAS-->
				<div class="row">
					<div class="col-sm-12">
						<span class="pln-box-title pln-font-color"><?php echo $lang['resumo_filtros']['filtros']; ?></span>
					</div>
				</div>
                <div class="row">
					<div class="col-sm-4">
						<div class="element-box pln-capapedido-min-box-height-l1">
							<fieldset class="form-group">
								<legend><span><?=$lang['filtro']['card_curvas']['titulo']?></span></legend>
								<div class="row">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_curvas']['abc']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="curve_abc[]" id="curve_abc_A" value="A">
                                        <label for="curve_abc_A">
                                            <?= $lang['dashboard_filtros']['curvas']['a'] ?>
                                        </label>

                                        <input type="checkbox" name="curve_abc[]" id="curve_abc_B" value="B" class="ml-2">
                                        <label for="curve_abc_B">
                                            <?= $lang['dashboard_filtros']['curvas']['b'] ?>
                                        </label>

                                        <input type="checkbox" name="curve_abc[]" id="curve_abc_C" value="C" class="ml-2">
                                        <label for="curve_abc_C">
                                            <?= $lang['dashboard_filtros']['curvas']['c'] ?>
                                        </label>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_curvas']['xyz']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="curve_xyz[]" id="curve_xyz_X" value="X">
                                        <label for="curve_xyz_X">
                                            <?= $lang['dashboard_filtros']['curvas']['x'] ?>
                                        </label>

                                        <input type="checkbox" name="curve_xyz[]" id="curve_xyz_Y" value="Y" class="ml-2">
                                        <label for="curve_xyz_Y">
                                            <?= $lang['dashboard_filtros']['curvas']['y'] ?>
                                        </label>

                                        <input type="checkbox" name="curve_xyz[]" id="curve_xyz_Z" value="Z" class="ml-2">
                                        <label for="curve_xyz_Z">
                                            <?= $lang['dashboard_filtros']['curvas']['z'] ?>
                                        </label>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_curvas']['pqr']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="curve_pqr[]" id="curve_pqr_P" value="P">
                                        <label for="curve_pqr_P">
                                            <?= $lang['dashboard_filtros']['curvas']['p'] ?>
                                        </label>

                                        <input type="checkbox" name="curve_pqr[]" id="curve_pqr_Q" value="Q" class="ml-2">
                                        <label for="curve_pqr_Q">
                                            <?= $lang['dashboard_filtros']['curvas']['q'] ?>
                                        </label>

                                        <input type="checkbox" name="curve_pqr[]" id="curve_pqr_R" value="R" class="ml-2">
                                        <label for="curve_pqr_R">
                                            <?= $lang['dashboard_filtros']['curvas']['r'] ?>
                                        </label>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-5">
										<?=$lang['filtro']['card_curvas']['123']?>
									</div>
									<div class="col-sm-7">
										<input type="checkbox" name="curve_123[]" id="curve_123_1" value="1">
                                        <label for="curve_123_1">
                                            <?= $lang['dashboard_filtros']['curvas']['1'] ?>
                                        </label>

										<input type="checkbox" name="curve_123[]" id="curve_123_2" value="2" class="ml-2">
                                        <label for="curve_123_2">
                                            <?= $lang['dashboard_filtros']['curvas']['2'] ?>
                                        </label>

                                        <input type="checkbox" name="curve_123[]" id="curve_123_3" value="3" class="ml-2">
                                        <label for="curve_123_3">
                                            <?= $lang['dashboard_filtros']['curvas']['3'] ?>
                                        </label>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<!-- FIM FILTROS CURVAS-->

                    <!-- Parâmetros -->
                    <div class="col-sm-4">
                        <div class="element-box pln-capapedido-min-box-height-l1">
                            <fieldset class="form-group">
                                <legend><span><?=$lang['filtro']['card_parametros']['titulo']?></span></legend>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="row">
                                            <div class="col-sm-12 font-weight-bold"><?=$lang['column']['inv_level']?></div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-sm-12" style="white-space: nowrap">
                                                <input name="inv_level[]" id="inv_level_0" value='0' type="checkbox">
                                                <label for="inv_level_0">
                                                    <?= $lang['dashboard_filtros']['sinalizador']['zero'] ?>
                                                </label>

                                                <input name="inv_level[]" id="inv_level_1" value='1' class="ml-2" type="checkbox">
                                                <label for="inv_level_1">
                                                    <?= $lang['dashboard_filtros']['sinalizador']['muito_baixo'] ?>
                                                </label>

                                                <input name="inv_level[]" id="inv_level_2" value='2' class="ml-2" type="checkbox">
                                                <label for="inv_level_2">
                                                    <?= $lang['dashboard_filtros']['sinalizador']['baixo'] ?>
                                                </label>
                                            </div>
                                            <div class="col-sm-12" style="white-space: nowrap">
                                                <input name="inv_level[]" id="inv_level_3" value='3' type="checkbox">
                                                <label for="inv_level_3">
                                                    <?= $lang['dashboard_filtros']['sinalizador']['ideal'] ?>
                                                </label>

                                                <input name="inv_level[]" id="inv_level_4" value='4' class="ml-2" type="checkbox">
                                                <label for="inv_level_4">
                                                    <?= $lang['dashboard_filtros']['sinalizador']['alto'] ?>
                                                </label>

                                                <input name="inv_level[]" id="inv_level_5" value='5' class="ml-2" type="checkbox">
                                                <label for="inv_level_5">
                                                    <?= $lang['dashboard_filtros']['sinalizador']['muito_alto'] ?>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row mt-3">
                                            <div class="col-sm-12 font-weight-bold"><?=$lang['filtro']['card_parametros']['ciclo']?></div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-sm-12" style="white-space:nowrap">
                                                <input name="sku_life_cycle[]" id="sku_life_cycle_1" value="1" type="checkbox"> <label for=""><?=$lang['filtro']['card_parametros']['nova']?></label>
                                                <input name="sku_life_cycle[]" id="sku_life_cycle_2" value="2" class="ml-2" type="checkbox"> <label for=""><?=$lang['filtro']['card_parametros']['madura']?></label>
                                                <input name="sku_life_cycle[]" id="sku_life_cycle_3" value="3" class="ml-2" type="checkbox"> <label for=""><?=$lang['filtro']['card_parametros']['desc']?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- FIM Parâmetros -->


                    <!-- ESTABELECIMENTO -->
                    <div class="col-sm-4">
                        <div class="element-box pln-capapedido-min-box-height-l1">
                            <fieldset class="form-group">

                                <legend><span onclick="hide_filter('ipt_find_estabelecimento')" class="pln-cur-pointer"><?php echo $lang['capa_pedido_filtros']['estab'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" placeholder="filtrar estabelecimentos" class="form-control pln-invi mb-1" id="ipt_find_estabelecimento" onkeyup="search_filter('ipt_find_estabelecimento', 'div_filter_estabelecimento')">
                                    </div>
                                    <div class="col-sm-12">
                                        <input type="checkbox" class="chk_check_all" id="chk_check_all_estab"> <label for="chk_check_all_estab"><?= $lang['parametros']['todos'] ?></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 pln-content-scroll" id="div_filter_estabelecimento">
                                        <?php
                                        foreach ($ViewData['Estab'] as $Key => $Estab) {
                                            ?>
                                            <div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                <input type='checkbox' name="cod_estab_fk[]" id="cod_estab_fk_<?= $Estab['code'] ?>" value="<?= $Estab['code'] ?>">
                                                <label class="text-truncate pl-1" for='cod_estab_fk_<?= $Estab['code'] ?>'><?= $Estab['name'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>
                    <!-- FIM ESTABELECIMENTO -->
				</div>

				<div class="row">
                    <!-- POLITICA -->
					<div class="col-sm-4">
						<div class="element-box min-box-height-l5">
							<fieldset class="form-group">
								<legend><span onclick="hide_filter('ipt_find_contrato')" class="pln-cur-pointer"><?php echo $lang['filtro']['card_parametros']['politica'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar política" class="form-control pln-invi mb-1" id="ipt_find_contrato" onkeyup="search_filter('ipt_find_contrato', 'div_filter_politica')">
									</div>
									<div class="col-sm-12">
										<input type="checkbox" class="chk_check_all" id="chk_check_all_politica"> <label for="chk_check_all_politica"><?= $lang['parametros']['todos'] ?></label>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 pln-content-scroll" id="div_filter_politica">
										<?php
										foreach($ViewData['Profile'] as $Profile){
											?>
											<div class='d-flex flex-row text-nowrap align-items-center'>
											<input type='checkbox' name='id_profile_fk[]' id='id_profile_fk_<?= $Profile['ID_PROFILE_PK']?>' value='<?= $Profile['ID_PROFILE_PK']?>'>
											<label class='text-truncate pl-1' for='id_profile_fk_<?= $Profile['ID_PROFILE_PK']?>'><?= $Profile['NOME'] ?></label>
											</div>
											<?php
										}
										?>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<!-- FIM POLITICA -->

					<!-- GESTOR -->
					<div class="col-sm-4">
						<div class="element-box min-box-height-l5">
							<fieldset class="form-group">
								<legend><span onclick="hide_filter('ipt_find_gestor')" class="pln-cur-pointer"><?php echo $lang['filtro']['card_gestor']['titulo'] ?> <i class="fa fa-search pln-link-color"></i></span></legend>
								<div class="row">
									<div class="col-sm-12">
										<input type="text" placeholder="filtrar gestor" class="form-control pln-invi mb-1" id="ipt_find_gestor" onkeyup="search_filter('ipt_find_gestor', 'div_filter_gestor')">
									</div>
									<div class="col-sm-12">
										<input type="checkbox" class="chk_check_all" id="chk_check_all_gestor"> <label for="chk_check_all_gestor"><?= $lang['parametros']['todos'] ?></label>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 pln-content-scroll" id="div_filter_gestor">
									<?php
									foreach ($ViewData['User'] as $Key => $User) {
										?>
										<div class="d-flex <?= ($Key == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
										<input type='checkbox' name='user_id[]' id='user_id_<?= $User['ID_USER_PK'] ?>' value='<?= $User['ID_USER_PK'] ?>'>
										<label class="text-truncate pl-1" for='user_id_<?= $User['ID_USER_PK'] ?>'><?= $User['LOGIN'] ?></label>
										</div>
										<?php
									}
									?>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<!-- FIM GESTOR -->
					<!-- FILTROS DINAMICOS -->
                    <?php foreach ($ViewData['Groups'] as $key => $Group): ?>
                        <div class="col-sm-4">
                            <div class="element-box min-box-height-l5">
                                <fieldset class="form-group">
                                    <legend>
                                        <span onclick="hide_filter('ipt_find_<?php echo $key ?>')"
                                              class="pln-cur-pointer"><?php echo $Group ?>
                                            <i class="fa fa-search pln-link-color"></i>
                                        </span>
                                    </legend>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" placeholder="filtrar <?php echo $Group ?>"
                                                   class="form-control pln-invi mb-1"
                                                   id="ipt_find_<?php echo $key ?>"
                                                   onkeyup="search_filter('ipt_find_<?php echo $key ?>', 'div_filter_<?php echo $key ?>')">
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="checkbox" class="chk_check_all"
                                                   id="chk_check_all_<?php echo $key ?>"> <label
                                                    for="chk_check_all_<?php echo $key ?>"><?= $lang['parametros']['todos'] ?></label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 pln-content-scroll"
                                             id="div_filter_<?php echo $key ?>">
                                            <?php
                                            foreach ($ViewData['Group_' . $key] as $Key2 => $Campo) {
                                                ?>
                                                <div class="d-flex <?= ($Key2 == 0 ? "mt-2" : null) ?> flex-row text-nowrap align-items-center">
                                                    <input type='checkbox'
                                                           name='cod_group<?php echo $key ?>_fk[]'
                                                           id='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>'
                                                           value='<?= $Campo['COD_GROUP_PK'] ?>'>
                                                    <label class='text-truncate pl-1'
                                                           for='cod_group<?php echo $key ?>_fk_<?= $Campo['COD_GROUP_PK'] ?>'><?= $Campo['DESC_GROUP_CONT'] ?></label>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    <?php endforeach; ?>
					<!-- FILTROS DINAMICOS -->
				</div>

				<!--END - Recent Ticket Comments-->
			</form>
		</div>
	</div>
</div>
<p id="demo"></p>
<div class="display-type"></div>

	<div class="modal fade" id="modal_edit" data-keyboard="true" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="botao_editar" aria-hidden="true">
		<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_titulo" name="modal_titulo"><?=$lang['filtro']['new_filter']?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="<?=$lang['global']['close']?>" id>
						<span aria-hidden="true">x</span>
					</button>
				</div>
				<div class="modal-body">

				<div style="width: 100%">
					<input placeholder="<?=$lang['filtro']['fill_filter_name'] ?>" maxlength="50" type="text" class="form-control" name="filter_nome" id="filter_nome" >
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-between">
				<span class="pln-danger pln-cur-pointer" id="confirm_delete"><i class="fa fa-times pln-danger"></i> <span><?=$lang['filtros']['delete']?></span></span>
				<button type="button" class="btn pln-btn-orange" id="save_filter"><span id="spn_save"><?=$lang['filtro']['save']?></span> <i class="fa fa-spin fa-spinner pln-invi"></i></button>
			</div>
		</div>
	</div>
</div>

<div class="clear"></div>
<?php
$JS = ['uglify/all-resume-filters-min'];
?>

