<?php
$JS= ['uglify/all-resume-list-min'];
?>
<div class="content-i">
    <div class="content-full-table">
        <div class="element-wrapper">
            <div class="title-header d-flex flex-row align-items-end mt-3">
                <div>
                    <a href="/<?=$Country?>/resume/filters" class="pln-link-color" onclick="loaderShow()"><i class="fa fa-angle-double-left" aria-hidden="true"></i> <?=$lang['parametros']['voltar_list']?></a>
                    <h4 class="pln-font-color"><?=$lang['resumo_lista']['titulo']?></h4>
                </div>
                <div class="ml-auto pln-items-search mb-2 mr-3">
                    <span class="ml-auto d-flex">
                        <span id="download_csv" class="pln-link-color pln-cur-pointer"><i class="fas fa-file-csv"></i> <?php echo $lang['resumo_lista']['csv'] ?> </span>
                    </span>
                </div>  
            </div>
            <!-- title header end -->

            <div class="element-box-full">              
                <div id="table_insert">
                </div>
                <div class="d-flex mt-1 align-items-center pln-items-search" style="display: none !important">

                    <div class="d-flex flex-row align-items-center ml-3 mr-3">

                        <span class="pln-invi" id="page_from_to">
                            <span id="page_items_from"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_a']; ?></span>
                            <span id="page_items_to"></span>
                            <span><?php echo $lang['sugcompras_lista']['pagina']['qtd_de']; ?></span>
                            <span id="page_items_total"></span>
                        </span>

                        <div class="d-flex flex-row">
                            <div class="d-flex flex-column mr-3">
                                <small>                            
                                    <?php echo $lang['sugcompras_lista']['itens']['mostrando'] ?>
                                    <span id="total_items"></span>
                                    <?php echo $lang['sugcompras_lista']['items'] ?>
                                </small>
                                <small>
                                    <?php echo $lang['sugcompras_lista']['pagina']['qtd_total']; ?>
                                    <span id="spn_total_page_number"></span>
                                </small> 
                            </div>
                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="pagination"></ul>
                            </nav>

                        </div>
                    </div>

                    <div class="d-flex flex-row align-items-center ml-3">
                        <span class="pln-bold-600"><?=$lang['sugcompras_lista']['pagina']['titulo']?></span>
                        <span class="ml-1"><input type="text" maxlength="10" id="goto_pagenumber" class="form-control form-control-sm input_just_number form-control form-control-sm-sm pln-input-60" /></span>
                        <span class="ml-1"><button type="button" id="goToPageNumber" class="btn pln-btn-ghost" /><?=$lang['sugcompras_lista']['buscar']?></span>
                    </div>
                    <div class="d-flex flex-row align-items-center ml-auto mr-3">
                        <span class="pln-bold-600"><?=$lang['sugcompras_lista']['visualizar']?></span>
                        <?php include_once('resources/views/items-per-page.php'); ?>
                        <span class="ml-1"><?=$lang['sugcompras_lista']['por_pagina']?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

