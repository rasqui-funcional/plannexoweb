<form id="formulario">
    <div id="controlSize" class="table-responsive">
        <table id="" class="table dataTable resume-table object_table bigger-table">
            <?php addComponent('table/thead.php') ?>
            <?php addComponent('resume/resumes.php') ?>
            <?php addComponent('resume/tfoot.php') ?>
        </table>
    </div>
</form>
<script>
    resumeClass.initializeTable();
    paginationClass.initialize();
</script>