<?php
if ($ViewData['error']) {
    echo $ViewData['error'];
    exit;
}

Request::generateCSVHeaders($lang['resumo_filtros']['titulo']);

$output = fopen('php://output', 'w');

$translatedTitles = [];
foreach ($ViewData['data'][0] as $title => $value) {
    $translatedTitles[$title] = $lang['column'][strtolower($title)];
}
fputcsv($output, mb_convert_encoding($translatedTitles, 'ISO-8859-1'), ';');

foreach ($ViewData['data'] as $row) {
    $data = [];
    foreach ($row as $key => $value) {
        $data[$key] = Utilities::formatCsvFields($key, $value);
    }

    fputcsv($output, mb_convert_encoding($data, 'ISO-8859-1'), ';');
}