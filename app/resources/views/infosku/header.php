<?php
/* @todo está dando erro quando carregado aqui 
$JS = [
    "i18n/br/infoSku",
];*/
?>

<div class="info_modal_header_loading d-flex justify-content-center pln-modal-title"><?php echo $lang['info_sku']['carregando']?> </div>
<div class="info_modal_header pln-invi">
    <div class="d-flex flex-row mb-2 align-items-center">
        <div id="sinalizator_tooltip" data-toggle="tooltip" data-original-title="">               
            <img id="info_sinalizator" class="" src="">
        </div>
        <div data-toggle="tooltip" id="info_item" class="badge badge-pln mx-2"></div>
        <div data-toggle="tooltip" id="info_description" class="pln-modal-title d-flex d-flex align-items-center"></div>
    </div>

    <div class="d-flex flex-row align-items-center mb-2">
        <div class="d-flex pln-modal-text-small mr-4">
            <div class="pln-bold-700"><?php echo $lang['info_sku']['estab'] ?> </div>
            <div class="ml-1 pln-text-details" id="info_cod_estab">1</div>
            <div class="ml-1 pln-text-details" id="info_estab"></div>
        </div>

        <div class="d-flex pln-modal-text-small mr-4">
            <div class="pln-bold-700"><?php echo $lang['info_sku']['curva'] ?></div>
            <div id="info_curve" class="ml-1 pln-text-details"></div>
        </div>

        <div class="d-flex pln-modal-text-small mr-4">
            <div class="pln-bold-700"><?php echo $lang['info_sku']['politica'] ?></div>
            <div id="info_politics" class="ml-1 pln-text-details"></div>
        </div>

        <div class="d-flex pln-modal-text-small mr-4">
            <div class="pln-bold-700"><?php echo $lang['info_sku']['categoria'] ?></div>
            <div id="info_category" class="ml-1 pln-text-details"></div>
        </div>

        <div class="d-flex pln-modal-text-small mr-4">
            <div class="pln-bold-700"><?php echo $lang['info_sku']['unidade'] ?></div>
            <div id="info_unit" class="ml-1 pln-text-details"></div>
        </div>

        <div class="d-flex align-items-center pln-modal-text-small mr-4">
            <div class="pln-bold-700"><?php echo $lang['info_sku']['gestor'] ?></div>
            <div class="ml-1 pln-text-details" id="info_manager_in_header"></div>
        </div>

        <div class="d-flex align-items-center pln-modal-text-small mr-4">
            <div class="pln-bold-700"><?php echo $lang['info_sku']['status_erp'] ?></div>
            <div class="ml-1 pln-text-details" id="info_status_erp"></div>
        </div>

        <div class="d-flex align-items-center pln-modal-text-small">
            <div class="pln-bold-700"><?php echo $lang['info_sku']['valor_unitario'] ?></div>
            <div class="ml-1 pln-text-details" id="val_unit_erp"></div>
        </div>
          
        <div class="d-flex align-items-center pln-modal-text-small right-align pr-2">            
            <div class="info_sinalizator_container m-0">
                <span class="pln-bold-700 text-dark"><?php echo $lang['info_sku']['status_sistema'] ?></span>
                <span>
                    <button type="button"
                            id="<?(can('spdmPharmaceutical') || can('spdmPlanner') ?: print 'info_btn_status')?>"
                            class="btn btn-success btn-sm <?(cannot('spdmPharmaceutical') && cannot('spdmPlanner') ?: print 'disabled')?>">
                        <i class=""></i> <?php echo $lang['info_sku']['status_ativo'] ?>
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>



<!--INICIO TABS-->
<ul class="nav nav-tabs smaller fixed-tabs pt-0 border-bottom-0">
    <li class="nav-item mr-0">
        <a class="nav-link active pln-modal-tab px-3 pln-tab-infosku" id="dashboard_tab" data-toggle="tab"
            href="#tab_dasboard"><?php echo $lang['info_sku']['tab']['dashboard'] ?></a>
    </li>
    <li class="nav-item mr-0">
        <a class="nav-link pln-modal-tab px-3 pln-tab-infosku" data-toggle="tab" data-tooltip="tooltip" href="#tab_info"
            id="information_tab"><?php echo $lang['info_sku']['tab']['info'] ?></a>
    </li>
    <li class="nav-item mr-0">
        <a class="nav-link pln-modal-tab px-3 pln-tab-infosku" data-toggle="tab" data-tooltip="tooltip" id="erp_tab"
            href="#tab_movto_erp"><?php echo $lang['info_sku']['tab']['movto_erp'] ?></a>
    </li>
    <li class="nav-item mr-0">
        <a class="nav-link pln-modal-tab px-3 pln-tab-infosku" data-toggle="tab" data-tooltip="tooltip" id="log_tab"
            href="#tab_log"><?php echo $lang['info_sku']['tab']['log'] ?></a>
    </li>
</ul>
<!--FIM TABS-->