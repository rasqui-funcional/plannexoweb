<!-- CONTAINER TAB MOVIMENTO ERP -->
<div class="col-sm-12">
    <div class="element-content">
        <div class="row">
            <select class="form-control form-control-sm mb-4 mt-3 ml-2" id="erp-select">
                <option value="MonthlyConsumption"><?=$lang['sku_detalhes_erp']['consumo']['titulo']?></option>
                <option value="OverStock"><?=$lang['sku_detalhes_erp']['saldo']['titulo']?></option>
                <option value="PurchaseOrder"><?=$lang['sku_detalhes_erp']['ordem']['titulo']?></option>
                <option value="PurchaseRequest"><?=$lang['sku_detalhes_erp']['solicitacao']['titulo']?></option>
                <option value="InTransaction"><?=$lang['sku_detalhes_erp']['entrada']['titulo']?></option>
                <option value="OutTransaction"><?=$lang['sku_detalhes_erp']['saida']['titulo']?></option>
            </select>
            <div class="col-sm-12">
                <div class="element-box">
                    <div class="row erp-spin d-flex align-items-center">
                        <div style="" class="col-sm-12 d-flex justify-content-center align-items-center"><div class="loader"></div></div>
                    </div>
                    <div id="erp-warning-box"  style="display: none">
                        <div class="table-responsive">
                            <div class="swal2-content text-center">
                                <div id="swal2-content"><?=$lang['global']['no_items_to_display']?></div>
                            </div>
                        </div>
                    </div>
                    <div id="erp-info-box">
                        <div class="row element-erp">
                            <div class="col-sm-12 d-flex flex-row justify-content-between">
                                <div class="d-flex flex-row align-items-center ml-3">
                                    <span id="search_label" class="pln-bold-600" style="display: none"></span>
                                    <span class="ml-1" style="display: none" id="input_wrapper">
                                        <input type="text" maxlength="10" id="cod_search" class="form-control input_just_number form-control-sm pln-input-100">
                                    </span>
                                    <span class="ml-1" style="display: none" id="btn_wrapper">
                                        <button type="button" class="btn pln-btn-orange" id="search_cod">Buscar</button>
                                    </span>

                                    <div class="ml-4" style="width: 205px">
                                        <a class="clearSearch" style="display: none"><span class="pln-link-color">X Limpar Busca</span></a></div>
                                </div>
                                <div>
                                    <a class="download pln-link-color pln-cur-pointer"><span class="pln-link-color"><i class="fas fa-file-csv"></i> Download CSV</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive element-erp">
                            <div class="erp_table">
                            </div>
                            <div class="d-flex mt-1 align-items-center" style="display: none" id="pln-pagination-footer_erp">
                                <div class="d-flex flex-row align-items-center">
                                        <span class="pln-invi" id="page_from_to_erp">
                                            <span id="page_items_from_erp"></span>
                                            <span><?=$lang['sugcompras_lista']['pagina']['qtd_a']?></span>
                                            <span id="page_items_to_erp"></span>
                                            <span><?=$lang['sugcompras_lista']['pagina']['qtd_de']?></span>
                                            <span id="page_items_total_erp"></span>
                                        </span>
                                    <div class="">
                                        <small>
                                            <?=$lang['global']['total']?>: <span id="total_items_erp"></span> <?=$lang['global']['skus']?>
                                        </small>
                                        <nav aria-label="Page navigation">
                                            <ul class="pagination pln-pagination" id="paginationERP"></ul>
                                        </nav>
                                        <small>
                                            <?=$lang['pagination']['total']?>: <span id="spn_total_page_number_erp"></span>
                                        </small>
                                    </div>
                                </div>
                                <div class="d-flex flex-row align-items-center ml-3">
                                    <span class="pln-bold-600"><?=$lang['pagination']['page']?></span>
                                    <span class="ml-1"><input type="text" maxlength="10" id="goto_pagenumber_erp" class="form-control input_just_number form-control-sm pln-input-60" /></span>
                                    <span class="ml-1"><button type="button" class="btn pln-btn-orange" id="searchERPPage" ><?=$lang['sugcompras_lista']['buscar']?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>