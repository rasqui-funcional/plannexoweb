<div id="controlSize" class="teste table-responsive">
<table id="approvalTable" class="table dataTable approval-table table-black-style"> 
        <thead>
            <tr>
                <?php
                    foreach ($ViewData['table']['cols'] as $key => $value) {
                ?>
                <th>
                    <div class='d-flex flex-row pln-cur-pointer order-col-erp' data-id="<?php echo $key; ?>">
                        <?php echo $lang['info_sku']['erp'][$key]; ?>
                        <i class='fa fa-sort<?php echo ($value['order'] == null) ? "" : "-".$value['order']; ?> pln-link-color ml-1 align-self-end'
                        ></i>
                    </div>
                </th>
                <?php 
                    }
                ?>
            </tr>
        </thead>                    
        <tbody>
            <?php foreach ($ViewData['table']['data']['result'] as $rows) {
                unset($rows['RNUM']);
            ?>
                <tr>
                <?php foreach ($rows as $key => $value) { ?>
                        <td><div class='d-flex flex-column'>
                    <?php                        
                        switch ($key) {
                            case 'DATE_TRANS':case 'DATE_ORDER':case 'DATE_REQ':
                                echo Utilities::dataFormatter('DATE_TRANS', $value);
                                break;
                            case 'DATE_EXP':
                                echo Utilities::dataFormatter('DATE_EXP', $value);
                                break;
                            case 'DATE_EXP_REQ':
                                echo $value;
                                break;
                            case 'DATE_TRANS_ALT':
                                echo Utilities::dataFormatter('DATE_TRANS_ALT', $value);
                                break;
                            case 'QTY_TRANS':case 'QTY_INV':
                                echo Utilities::toFloatFormat($value);
                                break;
                            case 'QTY_INV_VAL':
                                echo Utilities::dataFormatter('QTY_INV_VAL', $value);
                                break;
                            case 'QTY_PENDING':case 'QTY_REQ':case 'QTY_PARC':
                                echo Utilities::Unity($value);
                                break;
                            case 'COD_LOCAL': case 'COD_LOCAL_DEST': case 'TYPE_TRANS': case 'COD_SECTOR':
                                echo $value == -1 ? $lang['info_sku']['erp']['nao_definido']: $value;
                                break;
                            case 'YN_CONS':
                                echo $value == 'Y' ? $lang['column']['sim']: $lang['column']['nao'];
                                break;
                            case 'VAL_TOTAL':
                            case 'VAL_UNIT':
                                echo Utilities::Currency($value);
                                break;
                            default:
                                echo $value;
                                break;
                        }
                    ?>
                        </div></td>
                <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <?php foreach($ViewData['table']['cols'] as $key => $value) { ?>
                    <th><?php echo $ViewData['footer']['data'][$key] ?></th>
                <?php } ?>
            </tr>
            <input type="hidden" id="total_page_number_erp" value="<?php echo $ViewData['footer']['pages']; ?>" />
            <input type="hidden" id="hdn_total_items_erp" value="<?php echo $ViewData['footer']['count']; ?>" />
            <input type="hidden" id="items_per_page_erp" value="15">
        </tfoot>   
    </table>     
</div>

