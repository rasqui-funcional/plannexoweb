<div class="col-sm-12">
    <div class="element-content">
        <div class="row">
            <!-- INicio grafico de BALANCEAMENTO DE ESTOQUE 2.0 -->
            <div class="col-sm-6 col-xl-5 my-3">
                <span class="pln-bold-600">
                    <?=$lang['info_sku']['dashboard']['balance_estoque']?>
                </span>
                <div class="element-box h100">
                    <div class="balanceamentoEstoqueChart-spin col-sm-12 text-center d-flex justify-content-center">
                        <div class="loader"></div>
                    </div>

                    <div class="balanceamentoEstoqueChart-chart"> 
                        <div class="row mb-4">
                            <div class="col-6 text-center">
                                <div class="pln-bold text-uppercase"><?=$balanceamentoestoque['texto']['low']?>:</div>
                                <span id="stock-balance-minimum-unit" class="badge background-min-color text-white"></span>
                                <div class="pln-text-label-small stock-balance-minimum-cover"><span></span> <?= $ViewData['detailStockBalancing']['minimum']['COBERTURA'] ?> <?=$balanceamentoestoque['texto']['daysCoverage']?></div>
                            </div>
                            <div class="col-6 text-center">
                                <div class="pln-bold text-uppercase"><?=$balanceamentoestoque['texto']['high']?>:</div>
                                <span id="stock-balance-maximum-unit" class="badge background-max-color text-white"></span>
                                <div class="pln-text-label-small stock-balance-maximum-cover"><span></span> <?= $ViewData['detailStockBalancing']['maximum']['COBERTURA'] ?> <?=$balanceamentoestoque['texto']['daysCoverage']?></div>
                            </div>
                        </div>

                        <div>
                            <div id="stockBalance" class="stock-value-chart-height"></div>
                            <div id="stockValueLabel" class="float-right mb-3 stock-value-label-width">
                                <div id="sku-stockValue-disp-col" class="w-50 text-center float-left">
                                </div>
                                <div id="sku-stockValue-total-col" class="w-50 text-center float-left">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-sm-6 col-xl-7 my-3">

                <div class="d-flex flex-column justify-content-between h-100">
                    <div class="w-100 h-33">
                        <span class="pln-bold-600"><?=$lang['info_sku']['dashboard']['estoque_excesso']?></span>
                        <div class="element-box pln-box-balanceamento-estoque h-100">
                            <div class="d-flex align-items-center p-3 h-100 m-0">
                                <div class="d-flex justify-content-between w-100">
                                    <div class="text-center">
                                        <div class="pln-modal-text-small text-uppercase"><?php echo $lang['info_sku']['dashboard']['excesso_total'] ?></div>
                                        <div class="pln-bold-700 my-1" id="excess_total"></div>
                                        <div id="excess_total_val" class="pln-text-details pln-modal-text-small"></div>
                                    </div>
                                    <div class="text-center">
                                        <div class="pln-modal-text-small text-uppercase"><?php echo $lang['info_sku']['dashboard']['excesso_disponivel'] ?></div>
                                        <div class="pln-bold-700 my-1" id="excess_available"></div>
                                        <div id="excess_available_value" class="pln-text-details pln-modal-text-small"></div>
                                    </div>
                                    <div class="text-center">
                                        <div class="pln-modal-text-small text-uppercase"
                                            ><?php echo $lang['info_sku']['dashboard']['previsao_para_ideal'] ?>
                                        </div>
                                        <div class="pln-modal-text-small text-uppercase">
                                            <div class="pln-bold-700 my-1">
                                                <span id="ideal_prediction"></span> <?php echo $lang['info_sku']['dashboard']['dias'] ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="w-100 h-33">
                        <span class="pln-bold-600"><?=$lang['info_sku']['dashboard']['prox_sugest']?></span>                    

                        <div class="element-box d-flex align-items-center pln-box-prox-parc h-100 m-0 p-3">
                            <div class="col-4 div-nextparc">
                                <span class="pln-bold-700 text-lowercase">
                                    <span id="info_nextparc_days"></span> 
                                    <?=$lang['info_sku']['dashboard']['dias']?>
                                </span> 
                                <span class="pln-bold-700 text-lowercase"><?=$lang['info_sku']['dashboard']['em_atraso']?></span>
                            </div>
                            <div class="d-inline-block mr-5">
                                <span class="pln-modal-text-small pln-bold-700"><?=$lang['info_sku']['dashboard']['qtd']?></span> 
                                <span class="pln-modal-text-small pln-text-details"><span id="info_nextparc_qty"></span> <?=$lang['info_sku']['dashboard']['un']?></span>
                            </div>
                            <div class="d-inline-block">
                                <span class="pln-modal-text-small pln-bold-700"><?=$lang['info_sku']['dashboard']['valor_total']?></span>
                                <span class="pln-modal-text-small pln-text-details"><?=$lang['info_sku']['moeda']?><span id="info_nextparc_val"></span></span>
                            </div>
                        </div>
                    </div>

                    <div class="w-100 h-33">
                        <span class="pln-bold-600"><?=$lang['info_sku']['dashboard']['ordem_atraso']?></span>                    
                        <div class="element-box d-flex align-items-center pln-box-ordem-atraso h-100 m-0 p-3">

                            <div class="col-4">
                                <div class="d-flex align-items-center div-lateorder">
                                    <span class="pln-bold-700 text-lowercase mr-1" id="info_order_late_qtd"></span>
                                </div>
                            </div>

                            <div class="d-inline-block">
                                <div class="d-inline-block align-items-center mr-3">
                                    <span class="pln-modal-text-small pln-bold-700 mr-1"><?php echo $lang['info_sku']['dashboard']['ordem_primeira'] ?></span>
                                    <span><?php echo $lang['info_sku']['dashboard']['ordem_codigo'] ?></span>
                                    <span class="pln-modal-text-small pln-text-details" id="info_code_item"></span>
                                </div>
                                <div class="d-inline-block align-items-center mr-3">
                                    <span class="pln-modal-text-small pln-bold-700 mr-1"><?php echo $lang['info_sku']['dashboard']['atraso'] ?></span>
                                    <span class="pln-modal-text-small pln-text-details"><span id="info_days_late"></span> <?php echo $lang['info_sku']['dashboard']['ordem_dias'] ?></span>
                                </div>
                                <div class="d-inline-block align-items-center">
                                    <span class="pln-modal-text-small pln-bold-700 mr-1"><?php echo $lang['info_sku']['dashboard']['ordem_qtd'] ?></span>
                                    <span class="pln-modal-text-small pln-text-details"><span id="info_order_late_uni"></span> <?php echo $lang['info_sku']['dashboard']['un'] ?></span>
                                </div>
                                <div class="d-flex align-items-start mt-1">
                                    <div class="pln-modal-text-small pln-bold-700 mr-1"><?php echo $lang['info_sku']['dashboard']['ordem_fornecedor'] ?></div>
                                    <span class="pln-modal-text-small pln-text-details" id="info_order_late_supplier"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!--PREVISÃO DE COMPRAS-->
        <div class="row mt-5">
            <div class="col-sm-12 d-flex justify-content-between">
                <div class="d-flex flex-row align-items-end">
                    <div class="pln-box-title pln-font-color">
                    <?php echo $lang['info_sku']['dashboard']['previsao_compras'] ?>
                    </div>
                    <div class="pln-infosku-links"><a href="javascript:void(0)" id="shoppingforecast_day" class="pln-link-decoration-none"><?php echo $lang['info_sku']['dashboard']['diario'] ?></a></div>
                    <div class="pln-infosku-links"> | <a href="javascript:void(0)" id="shoppingforecast_month" class="pln-link-color"><?php echo $lang['info_sku']['dashboard']['previsao_estatistica'] ?></a></div> 
                </div>
                <span class="previsaoCompras-chart-dia">
                    <div class="d-flex align-items-center mb-2">
                        <span class="pln-bold-600 mr-1">
                            <?php echo $lang['info_sku']['dashboard']['periodo'] ?>
                        </span>
                        <span class="d-flex align-items-center">
                            <input type="date" class="form-control form-control-sm pln-input-date mr-2" required="required"  id="forecastStartDate">
                            <span class="mr-2"><?php echo $lang['info_sku']['dashboard']['ate'] ?></span>
                            <input type="date" class="form-control form-control-sm pln-input-date mr-2" required="required" id="forecastEndDate">
                            <button type="button" id="forecastApply" class="btn btn-sm pln-btn-primary"><i class=""></i><?php echo $lang['capa_pedido_lista']['aplicar']; ?></button>
                        </span>
                    </div>
                </span>
            </div>
        </div>
        <div class="row">        
            <div class="col-sm-12">
                <div class="element-box pln-h-100" style="padding-bottom:0px !important">
                    <div class="row previsaoCompras-spin d-flex align-items-center">
                        <div style="min-height:300px" class="col-sm-12 d-flex justify-content-center align-items-center"><div class="loader"></div></div>
                    </div>
                    <div id="previsao_compras_dia" class="previsaoCompras-chart-dia"></div>
                    <div id="previsao_compras_mes" class="previsaoCompras-chart-mes"></div>
                    <div class="prev-compras-label-footer out6">
                        <span class="pln-bold-600"> <?php echo $lang['info_sku']['dashboard']['variacao']; ?>: </span>
                        <span id="out6-value"></span>
                    </div>
                    <div class="lead-time prev-compras-label-footer pln-invi">
                        <span class="pln-bold-600"><?php echo $lang['info_sku']['dashboard']['lead_time_total'] ?></span> 
                        <span id="lead-time-value" class="ml-1 mr-1"></span> 
                        <span><?php echo $lang['info_sku']['dashboard']['dias'] ?></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-sm-12">
                <span class="pln-box-title pln-font-color"><?echo$lang['info_sku']['dashboard']['analise_geral']?></span>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10 d-flex justify-content-between">
                <div class="d-flex flex-row align-items-end">
                    <div class="pln-box-title pln-font-color">
                        <?echo $lang['info_sku']['dashboard']['historico']?>
                    </div>
                    <!--
                    <div class="pln-infosku-links"><a href="javascript:void(0)" id="history_day" class="pln-link-decoration-none"><?php echo $lang['info_sku']['dashboard']['diario'] ?></a></div>
                    <div class="pln-infosku-links"> | <a href="javascript:void(0)" id="history_consumption" class="pln-link-color"><?php echo $lang['info_sku']['dashboard']['desvio_consumo'] ?></a></div> 
                    -->
                </div>
                <span class="historico-chart-dia">
                <div class="d-flex align-items-center mb-2">
                    <span class="pln-bold-600 mr-1">
                    <?echo $lang['info_sku']['dashboard']['periodo'] ?>
                    </span>
                    <span class="d-flex align-items-center">        
                        <input type="date" class="form-control form-control-sm pln-input-date mr-2" required="required" id="historyStartDate">
                        <span class="mr-2"><?php echo $lang['info_sku']['dashboard']['ate'] ?></span>
                        <input type="date" class="form-control form-control-sm pln-input-date mr-2" required="required" id="historyEndDate">
                        <button type="button" id="historyApply" class="btn btn-sm pln-btn-primary"><i class=""></i><?php echo $lang['capa_pedido_lista']['aplicar']; ?></button>
                    </span>
                </div>
                </span>
            </div>
            <div class="col-sm-2 d-flex justify-content-end">
                <span class="pln-link-color pln-cur-pointer" id="history_movto_erp"><?php echo $lang['info_sku']['dashboard']['movto_erp'] ?></span>
            </div>
        </div>

        <div class="row">        
            <div class="col-sm-10">
                <div class="element-box pln-h-100 px-0" style="padding-bottom:0px !important">
                    <div class="row historico-spin d-flex align-items-center">
                    <div style="min-height:300px" class="col-sm-12 d-flex justify-content-center align-items-center"><div class="loader"></div></div>
                    </div>                    
                    <div id="historico" class="historico-chart-dia"></div>
                    <!-- <div id="historico_consumo" class="historico-chart-consumo"></div> -->
                </div>
            </div>
            <div class="col-sm-2">
                <div class="element-box pln-h-100 d-flex flex-column justify-content-between">
                    <div class="d-flex flex-column align-items-end"> 
                        <span><?php echo $lang['info_sku']['dashboard']['30_dias'] ?></span>
                        <span class="pln-font-xlarge pln-bold-600" id="info_average_30"></span>
                    </div>
                    
                    <div class="d-flex flex-column align-items-end"> 
                        <span><?php echo $lang['info_sku']['dashboard']['3_meses'] ?></span>
                        <span class="pln-font-xlarge pln-bold-600" id="info_average_3"></span>
                    </div>
                    
                    <div class="d-flex flex-column align-items-end"> 
                        <span><?php echo $lang['info_sku']['dashboard']['6_meses'] ?></span>
                        <span class="pln-font-xlarge pln-bold-600" id="info_average_6"></span>
                    </div>
                    
                    <div class="d-flex flex-column align-items-end">
                        <span><?php echo $lang['info_sku']['dashboard']['12_meses'] ?></span>
                        <span class="pln-font-xlarge pln-bold-600" id="info_average_12"></span>
                    </div>                    
                </div>
            </div>
        </div>

        <!--POSIÇÃO DE ESTOQUE-->
        <div class="row mt-5">
            <div class="col-sm-12 d-flex justify-content-between">
                <div class="d-flex flex-row align-items-end">
                    <div class="pln-box-title pln-font-color">
                    <?php echo $lang['info_sku']['dashboard']['posicao_estoque'] ?>
                    </div>
                    <div class="pln-infosku-links"><a href="javascript:void(0)" id="stockpos_excess" class="pln-link-decoration-none"><?php echo $lang['info_sku']['dashboard']['excesso'] ?></a></div>
                </div>
                <div class="d-flex flex-row align-items-end">
                    
                    <div class="pln-infosku-links"><a href="javascript:void(0)" id="stockpos_quarter" class="pln-link-decoration-none"><?php echo $lang['info_sku']['dashboard']['semanal'] ?></a></div>
                    <div class="pln-infosku-links"> | <a href="javascript:void(0)" id="stockpos_year" class="pln-link-color"><?php echo $lang['info_sku']['dashboard']['mensal'] ?></a></div> 
                </div>
            </div>
        </div>
        <div class="row">        
            <div class="col-sm-12">
                <div class="element-box pln-h-100" style="padding-bottom:0px !important">
                    <div class="row posicaoestoque-spin d-flex align-items-center">
                        <div style="min-height:300px" class="col-sm-12 d-flex justify-content-center align-items-center"><div class="loader"></div></div>
                    </div>
                    <div id="posicao_estoque" class="posicaoestoque-chart"></div>
                    <div id="" class=""></div>
                </div>
            </div>
        </div>
    </div>
</div>