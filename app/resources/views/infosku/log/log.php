<div class="col-sm-12">
    <div class="element-content">        
        <div class="element-box">
            <div class="col-sm-12 text-center d-flex justify-content-center">
                <div id="log-table-loader" class="loader"></div>
            </div>
            <div class="d-flex align-items-end log-pln-items-search mb-2">
                <span class="ml-auto d-flex">
                    <span id="download_csv" class="pln-link-color pln-cur-pointer">
                        <i class="fas fa-file-csv"></i>
                        <?php echo $lang['capa_pedido_parcela']['download'] ?>
                    </span>
                </span>
            </div>
            <div id="logControlSize" class="table-responsive">
                <div class="log-table-container"></div>
                <div class="d-flex mt-1 align-items-start log-pln-items-search mt-2" style="display: none !important">
                    
                    <div class="d-flex flex-row align-items-center">
                        <span class="pln-invi" id="log_page_from_to">
                            <span id="log_page_items_from"></span>
                            <span><?=$lang['sugcompras_lista']['pagina']['qtd_a']?></span>
                            <span id="log_page_items_to"></span>
                            <span><?=$lang['sugcompras_lista']['pagina']['qtd_de']?></span>
                            <span id="log_page_items_total"></span>
                        </span>
                        <div class="">
                            <nav aria-label="Page navigation">
                                <ul class="pagination pln-pagination" id="logPagination"></ul>
                            </nav>
                            <small>
                                <?=$lang['sugcompras_lista']['pagina']['qtd_total']?> <span id="log_spn_total_page_number"></span>
                            </small> 
                        </div>
                    </div>
                                        
                    <div class="d-flex flex-row align-items-center ml-3">
                        <span class="pln-bold-600"><?=$lang['sugcompras_lista']['pagina']['titulo']?></span>
                        <span class="ml-1"><input type="text" maxlength="10" id="log_goto_pagenumber" class="form-control form-control-sm input_just_number form-control form-control-sm-sm pln-input-60" /></span>
                        <span class="ml-1"><button type="button" id="logGoToPageNumber" class="btn pln-btn-orange" /><?=$lang['sugcompras_lista']['buscar']?></span>
                    </div>
                    <div class="d-flex flex-row align-items-center ml-auto">
                        <span class="pln-bold-600"><?=$lang['sugcompras_lista']['visualizar']?></span>
                        <span class="ml-1">
                            <select id="log_items_per_page" class="form-control form-control-sm">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </span>
                        <span class="ml-1"><?=$lang['sugcompras_lista']['por_pagina']?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
