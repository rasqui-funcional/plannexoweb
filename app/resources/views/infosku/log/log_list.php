<?php
if (!$ViewData['dataTable']) {
    echo 'Não há itens a serem exibidos.';
    return;
}

?>
<div class="table-responsive table-wrapper-scroll-y">
    <table id="object_table" class="table">
        <thead>
        <tr>
            <th class="pln-borderless pln-padding-bottom-0 pln-blue-th"> <?php echo $lang['log']['id'] ?> </th>
            <th class="pln-borderless pln-padding-bottom-0 pln-blue-th"> <?php echo $lang['log']['data_inclusao'] ?> </th>
            <th class="pln-borderless pln-padding-bottom-0 pln-blue-th"> <?php echo $lang['log']['usuario'] ?> </th>
            <th class="pln-borderless pln-padding-bottom-0 pln-blue-th"> <?php echo $lang['log']['ip'] ?> </th>
            <th class="pln-borderless pln-padding-bottom-0 pln-blue-th"> <?php echo $lang['log']['app'] ?> </th>
            <th class="pln-borderless pln-padding-bottom-0 pln-blue-th"> <?php echo $lang['log']['acao'] ?> </th>
            <th class="pln-borderless pln-padding-bottom-0 pln-blue-th"> <?php echo $lang['log']['campo'] ?> </th>
            <th class="pln-borderless pln-padding-bottom-0 pln-blue-th"> <?php echo $lang['log']['val_antigo'] ?> </th>
            <th class="pln-borderless pln-padding-bottom-0 pln-blue-th"> <?php echo $lang['log']['val_novo'] ?> </th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>
                <input type="hidden" id="log_hdn_total_items" value="<?= $ViewData['footer']['count'] ?>"/>
                Total: <span id="log_total_items"></span>
            </th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <input type="hidden" id="log_total_page_number" value="<?php echo $ViewData['footer']['pages'] ?>">
        </tfoot>
        <tbody>
        <?php
        foreach ($ViewData['dataTable'] as $key => $Data) {
            ?>
            <tr>
                <td> <?php echo $Data['ID'] ?> </td>
                <td> <?php echo $Data['INSERTED_DATE'] ?> </td>
                <td> <?php echo $Data['USERNAME'] ?> </td>
                <td> <?php echo $Data['IP_USER'] ?> </td>
                <td> <?php echo $Data['APPLICATION'] ?> </td>
                <td> <?php echo $Data['SC_FIELD_0'] ?> </td>
                <td> <?php
                    foreach ($Data['DESCRIPTION']['ALTERED_COLUMN'] as $value) {
                        echo $value . "<br>";
                    }
                    ?> </td>
                <td> <?php
                    foreach ($Data['DESCRIPTION']['OLD_VALUE'] as $value) {
                        echo $value . "<br>";
                    }
                    ?> </td>
                <td> <?php
                    foreach ($Data['DESCRIPTION']['NEW_VALUE'] as $value) {
                        echo $value . "<br>";
                    }
                    ?> </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>