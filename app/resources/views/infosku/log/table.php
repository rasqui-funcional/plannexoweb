<div id="sku_log_without_data" class="d-flex flex-column align-items-center">
    <?php echo $lang['geral']['sem_itens']; ?>
</div>
<div id="sku_log_with_data" class="table-log pln-content-scroll control-size-table-modal">
    <table class="table dataTable bigger-table info-sku-log-table mt-0">
        <thead>
            <tr>
                <th>
                    <div class="d-flex flex-row pln-cur-pointer order-col" data-col-log="ID">
                        <span><?php echo $lang['log']['id']; ?></span>
                        <i class="fa fa-sort pln-link-color ml-1 align-self-end"></i>
                    </div>
                </th>
                <th>
                    <div class="d-flex flex-row pln-cur-pointer order-col" data-col-log="CREATOR">
                        <span><?php echo $lang['log']['origin']; ?></span>
                        <i class="fa fa-sort pln-link-color ml-1 align-self-end"></i>
                    </div>
                </th>
                <th>
                    <div class="d-flex flex-row pln-cur-pointer order-col" data-col-log="INSERTED_DATE">
                        <span><?php echo $lang['log']['data_inclusao']; ?></span>
                        <i class="fa fa-sort pln-link-color ml-1 align-self-end"></i>
                    </div>
                </th>
                <th>
                    <div class="d-flex flex-row pln-cur-pointer order-col" data-col-log="USERNAME">
                        <span><?php echo $lang['log']['usuario']; ?></span>
                        <i class="fa fa-sort pln-link-color ml-1 align-self-end"></i>
                    </div>
                </th>
                <th>
                    <div class="d-flex flex-row pln-cur-pointer order-col" data-col-log="IP_USER">
                        <span><?php echo $lang['log']['ip']; ?></span>
                        <i class="fa fa-sort pln-link-color ml-1 align-self-end"></i>
                    </div>
                </th>
                <th>
                    <div class="d-flex flex-row pln-cur-pointer order-col" data-col-log="APPLICATION">
                        <span><?php echo $lang['log']['app']; ?></span>
                        <i class="fa fa-sort pln-link-color ml-1 align-self-end"></i>
                    </div>
                </th>
                <th>
                    <div class="d-flex flex-row pln-cur-pointer order-col" data-col-log="ACTION">
                        <span><?php echo $lang['log']['acao']; ?></span>
                        <i class="fa fa-sort pln-link-color ml-1 align-self-end"></i>
                    </div>
                </th>
                <th>
                    <div class="d-flex flex-row">
                        <span><?php echo $lang['log']['campo']; ?></span>
                    </div>
                </th>
                <th>
                    <div class="d-flex flex-row">
                        <span><?php echo $lang['log']['val_antigo']; ?></span>
                    </div>
                </th>
                <th>
                    <div class="d-flex flex-row">
                        <span><?php echo $lang['log']['val_novo']; ?></span>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody id="sku_log_body_table"></tbody>
        <tfoot>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tfoot>
    </table>
</div>
