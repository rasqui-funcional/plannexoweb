<div class="d-flex mt-1 align-items-center pln-items-search">
    <div class="d-flex flex-row align-items-center">
        <div>
            <small>
                <?=$lang['global']['total']?>: <span id="total_items">
                            <span id="sku_log_page_items_from"></span>
                            <span><?=$lang['pagination']['qtd_a']?></span>
                            <span id="sku_log_page_items_to"></span>
                            <span><?=$lang['pagination']['qtd_de']?></span>
                            <span id="sku_log_quantity_items"></span>
                        </span> <?=strtoupper("{$lang['global']['skus']} {$lang['sku_detalhes']['log']['titulo']}")?>
            </small>
            <nav aria-label="Page navigation">
                <ul class="pagination pln-pagination" id="pagination">
                    <li class="page-item first disabled">
                        <a href="#" class="page-link">
                            <i class="fas fa-angle-double-left"></i>
                        </a>
                    </li>
                    <li class="page-item prev disabled">
                        <a href="#" class="page-link">
                            <i class="fas fa-angle-left"></i>
                        </a>
                    </li>

                    <li class="page-item" id="sku_log_page_item_1_background">
                        <a href="#" class="page-link" id="sku_log_page_item_1_value">1</a>
                    </li>
                    <li class="page-item" id="sku_log_page_item_2_background">
                        <a href="#" class="page-link" id="sku_log_page_item_2_value">2</a>
                    </li>
                    <li class="page-item" id="sku_log_page_item_3_background">
                        <a href="#" class="page-link" id="sku_log_page_item_3_value">3</a>
                    </li>

                    <li class="page-item next">
                        <a href="#" class="page-link">
                            <i class="fas fa-angle-right"></i>
                        </a>
                    </li>
                    <li class="page-item last">
                        <a href="#" class="page-link">
                            <i class="fas fa-angle-double-right"></i>
                        </a>
                    </li>
                </ul>
            </nav>
            <small>
                <?=$lang['pagination']['total']?>: <span id="sku_log_quantity_pages"></span>
            </small>
        </div>
    </div>

    <div class="d-flex flex-row align-items-center ml-3">
        <span class="pln-bold-600"><?=$lang['pagination']['page']?>:</span>
        <span class="ml-1">
            <input
                    type="text"
                    maxlength="10"
                    id="sku_log_goto_page_number"
                    class="form-control input_just_number form-control-sm pln-input-60"
            />
        </span>
        <span class="ml-1">
            <button type="button" class="btn pln-btn-orange" id="sku_log_search_button">
                <?=$lang['global']['search']?>
            </button>
        </span>
    </div>
    
    <div class="d-flex flex-row align-items-center ml-auto">
        <span class="pln-bold-600"><?=$lang['global']['visualize']?>:</span>
        <span class="ml-1">
            <select id="sku_log_items_per_page" class="form-control form-control-sm">
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </span>
        <span class="ml-1"><?=$lang['pagination']['per_page']?></span>
    </div>
</div>
