<div class="col-sm-12">
    <div class="element-content">
        <div class="element-box">
            <div id="sku_log_load" class="row skuLog pln-h-100 d-flex align-items-center" style="display: block !important;">
                <div class="col-sm-12 text-center d-flex justify-content-center"><div class="loader"></div></div>
            </div>
            <div id="sku_log_data" class="table-responsive">
                <div class="align-items-end log-pln-items-search mb-2 d-flex" style="">
                    <span class="ml-auto d-flex">
                        <span id="download_csv" class="pln-link-color pln-cur-pointer">
                            <i class="fas fa-file-csv"></i>
                            <?=$lang['csv']['download']?>
                        </span>
                    </span>
                </div>
                <?php 
                    include('log/table.php'); 
                    include('log/pagination.php'); 
                ?>
            </div>
        </div>
    </div>
</div>