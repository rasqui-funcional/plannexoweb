<?php if (cannot('spdmPharmaceutical') && cannot('spdmPlanner')) { ?>
    <div class="title-header d-flex flex-row align-items-end ml-auto">
    <span class="ml-auto">
        <span class="ml-auto mr-3">
            <button type="button" id="recalculate_sku" class="btn btn-outline-danger">
                <?= $lang['info_sku']['recalculate'] ?>
            </button>
        </span>
        <span class="ml-auto" id="confirm_delete">
            <button type="button" id="info_sku_save_click" class="btn disabled">
                <i class="fa fa-save" aria-hidden="true"></i>
                <?= $lang['parametros']['salvar'] ?>
            </button>
        </span>
    </span>
    </div>
<?php } ?>