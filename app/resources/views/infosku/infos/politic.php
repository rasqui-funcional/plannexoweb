<div class="col-sm-12">
    <div class="element-content">
        <div class="row mt-3">
            <div class="col-sm-6">
                <span class="pln-bold-600 text-uppercase">
                    <?=$lang['sku_detalhes_politica']['police']?>
                </span>
            </div>

            <div class="col-sm-6">
                <span class="pln-bold-600 text-uppercase">
                    <?=$lang['sku_detalhes_politica']['sku_parameters']?>
                </span>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="element-box h-100 mb-0">
                    <div id="policy_load_part_1" class="row basicInformation pln-h-100 d-flex align-items-center">
                        <div class="col-sm-12 text-center d-flex justify-content-center">
                            <div class="loader"></div>
                        </div>
                    </div>

                    <div id="policy_data_part_1">
                        <div class="row mt-2">
                            <div class="col-sm-6">
                                <div class="row">
                                    <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_politica']['card1']['police_code']?>:</span>
                                    <select class="form-control form-control-sm mr-40" id="info_sku_policy_code"></select>
                                </div>

                                <div class="row mt-3">
                                    <span class="pln-bold-600 w-100p text-uppercase"><?=$lang['sku_detalhes_politica']['card1']['description']?></span>
                                    <span class="w-100p c-62666b" id="info_sku_policy_desc_profile"></span>
                                </div>

                                <div class="row mt-3">
                                    <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_politica']['card1']['available_deposits']?> (<span id="info_sku_policy_deposits_qty"></span>)</span>
                                    <select class="form-control form-control-sm mr-40 h-100" id="info_sku_policy_deposits" size="8" disabled></select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="row">
                                    <span class="pln-bold-600 text-right w-100p text-uppercase"><?=$lang['sku_detalhes_politica']['card1']['lead_time']?></span>
                                    <hr class="mb-0 mt-0 w-100p bb-62666b" />
                                </div>

                                <div class="row">
                                    <ul class="list-group w-100p">
                                        <li class="list-group-item text-right mt-3 li-type-disabled text-uppercase" id="info_sku_policy_lt_planner_div">
                                            <?=strtoupper($lang['global']['planner'])?> (<?=$lang['global']['days']?>):
                                            <span class="badge p-0 policy_lt_span">0</span>
                                        </li>
                                        <li class="list-group-item text-right mt-4 li-type-disabled text-uppercase" id="info_sku_policy_lt_buyer_div">
                                            <?=strtoupper($lang['global']['buyer'])?> (<?=$lang['global']['days']?>):
                                            <span class="badge p-0 policy_lt_span">0</span>
                                        </li>
                                        <li class="list-group-item text-right mt-4 li-type-disabled text-uppercase" id="info_sku_policy_lt_supplier_div">
                                            <?=strtoupper($lang['global']['supplier'])?> (<?=$lang['global']['days']?>):
                                            <span class="badge p-0 policy_lt_span">0</span>
                                        </li>
                                        <li class="list-group-item text-right mt-4 li-type-disabled text-uppercase" id="info_sku_policy_lt_receiving_div">
                                            <?=strtoupper($lang['sku_detalhes_variaveis']['card3']['recebimento'])?> (<?=$lang['global']['days']?>):
                                            <span class="badge p-0 policy_lt_span">0</span>
                                        </li>
                                        <li class="list-group-item text-right mt-4 li-type-disabled text-uppercase" id="info_sku_policy_lt_others_div">
                                            <?=strtoupper($lang['global']['others'])?>(<?=$lang['global']['days']?>):
                                            <span class="badge p-0 policy_lt_span">0</span>
                                        </li>
                                        <li class="list-group-item text-right mt-4 li-type-disabled text-uppercase" id="info_sku_policy_lt_total_days">
                                            <?=strtoupper($lang['global']['total'])?> (<?=$lang['global']['days']?>):
                                            <span class="badge p-0 policy_lt_total">0</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-sm-6">
                                <div class="row h-100">
                                    <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_politica']['card5']['titulo']?> (<span id="info_sku_unavailable_deposits_qty"></span>)</span>
                                    <select class="form-control form-control-sm mr-40 h-100" id="info_sku_unavailable_deposits" size="6" disabled></select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="row">
                                    <span class="pln-bold-600 text-right w-100p text-uppercase"><?=$lang['sku_detalhes_politica']['card5']['minimum_stock']?></span>
                                    <hr class="mb-0 mt-0 w-100p bb-62666b" />
                                </div>

                                <div class="row">
                                    <ul class="list-group w-100p">
                                        <li class="list-group-item text-right mt-3 li-type-disabled text-uppercase">
                                            <?=strtoupper($lang['global']['minimum'])?> (<?=$lang['global']['days']?>):
                                            <span class="badge p-0" id="info_sku_policy_days_min_div text-uppercase"><?=$lang['global']['automatic']?></span>
                                        </li>
                                        <li class="list-group-item text-right mt-3 li-type-disabled text-uppercase">
                                            <?=strtoupper($lang['sku_detalhes_politica']['card2']['maximo'])?>. (<?=$lang['global']['days']?>):
                                            <span class="badge p-0" id="info_sku_policy_max_limit_bottom">0</span>
                                        </li>
                                        <li class="list-group-item text-right mt-3 li-type-disabled text-uppercase">
                                            <?=strtoupper($lang['sku_detalhes_politica']['card2']['minimo'])?>. (<?=$lang['global']['days']?>):
                                            <span class="badge p-0" id="info_sku_policy_min_limit_bottom">0</span>
                                        </li>
                                        <li class="list-group-item text-right mt-3 li-type-disabled text-uppercase">
                                            <?=strtoupper($lang['sku_detalhes_politica']['card2']['servico'])?>:
                                            <span class="badge p-0" id="info_sku_policy_service_level">0.0</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="element-box mb-0">
                    <div id="policy_load_part_2" class="row basicInformation pln-h-100 d-flex align-items-center">
                        <div class="col-sm-12 text-center d-flex justify-content-center">
                            <div class="loader"></div>
                        </div>
                    </div>

                    <div id="policy_data_part_2">
                        <div class="row mt-2">
                            <div class="col-sm-12">
                                <div class="row">
                                    <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_politica']['card1']['lead_time']?></span>
                                    <hr class="mb-0 mt-0 w-100p bb-62666b" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <div class="form-group row mt-2 mb-2">
                                    <div class="col-sm-4 pt-50 pb-50"><?=strtoupper($lang['global']['planner'])?> (<?=$lang['global']['days']?>):</div>
                                    <div class="col-sm-2 mt-1" id="info_sku_policy_lt_planner_input">
                                        <input class="form-control pt-20 pb-20" type="number" value="0" maxlength="5">
                                    </div>
                                    <div class="col-sm-6 pt-50 pb-50">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="info_sku_policy_lt_planner_check">
                                            <label class="form-check-label" for="info_sku_policy_lt_planner_check"><?=$lang['global']['automatic']?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mt-2 mb-2">
                                    <div class="col-sm-4 pt-50 pb-50"><?=strtoupper($lang['global']['buyer'])?> (<?=$lang['global']['days']?>):</div>
                                    <div class="col-sm-2 mt-1" id="info_sku_policy_lt_buyer_input">
                                        <input class="form-control pt-20 pb-20" type="number" value="0" maxlength="5">
                                    </div>
                                    <div class="col-sm-6 pt-50 pb-50">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="info_sku_policy_lt_buyer_check">
                                            <label class="form-check-label" for="info_sku_policy_lt_buyer_check"><?=$lang['global']['automatic']?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mt-2 mb-2">
                                    <div class="col-sm-4 pt-50 pb-50"><?=strtoupper($lang['global']['supplier'])?> (<?=$lang['global']['days']?>):</div>
                                    <div class="col-sm-2 mt-1" id="info_sku_policy_lt_supplier_input">
                                        <input class="form-control pt-20 pb-20" type="number" value="0" maxlength="5">
                                    </div>
                                    <div class="col-sm-6 pt-50 pb-50">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="info_sku_policy_lt_supplier_check">
                                            <label class="form-check-label" for="info_sku_policy_lt_supplier_check"><?=$lang['global']['automatic']?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mt-2 mb-2">
                                    <div class="col-sm-4 pt-50 pb-50"><?=strtoupper($lang['sku_detalhes_variaveis']['card3']['recebimento'])?> (<?=$lang['global']['days']?>):</div>
                                    <div class="col-sm-2 mt-1" id="info_sku_policy_lt_receiving_input">
                                        <input class="form-control pt-20 pb-20" type="number" value="0" maxlength="5">
                                    </div>
                                    <div class="col-sm-6 pt-50 pb-50">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="info_sku_policy_lt_receiving_check">
                                            <label class="form-check-label" for="info_sku_policy_lt_receiving_check"><?=$lang['global']['automatic']?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mt-2 mb-2">
                                    <div class="col-sm-4 pt-50 pb-50"><?=strtoupper($lang['global']['others'])?>(<?=$lang['global']['days']?>):</div>
                                    <div class="col-sm-2 mt-1" id="info_sku_policy_lt_others_input">
                                        <input class="form-control pt-20 pb-20" type="number" value="0" maxlength="5">
                                    </div>
                                    <div class="col-sm-6 pt-50 pb-50">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="info_sku_policy_lt_others_check">
                                            <label class="form-check-label" for="info_sku_policy_lt_others_check"><?=$lang['global']['automatic']?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mt-2 mb-2">
                                    <div class="col-sm-3 pt-50 pb-50">
                                        <?=strtoupper($lang['global']['total'])?>(<?=$lang['global']['days']?>):<span id="info_sku_total_dynamics">---</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-sm-12">
                                <div class="row">
                                    <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_politica']['card5']['minimum_stock']?></span>
                                    <hr class="mb-0 mt-0 w-100p bb-62666b" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <div class="form-group row mt-2">
                                    <div class="col-sm-3 pt-50 pb-50"><?=strtoupper($lang['global']['minimum'])?> (<?=$lang['global']['units']?>):</div>
                                    <div class="col-sm-3 pt-50 pb-50">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="info_sku_policy_days_min_check">
                                            <label class="form-check-label" for="info_sku_policy_days_min_check"><?=$lang['global']['automatic']?></label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 mt-1" id="info_sku_policy_days_min_input" data-info="info_sku_policy_days_min_info">
                                        <input class="form-control col-sm-3 pt-20 pb-20" type="number" value="0" maxlength="5">
                                    </div>
                                    <div class="col-sm-6 mt-2" id="info_sku_policy_days_min_info">
                                        <span class="pln-bold-600">1</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-sm-12">
                                <div class="row">
                                    <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_variaveis']['card2']['titulo']?></span>
                                    <hr class="mb-0 mt-0 w-100p bb-62666b" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <div class="form-group row mt-2">
                                    <div class="col-sm-4 pt-50 pb-50 max-width-28"><?=strtoupper($lang['sku_detalhes_variaveis']['card2']['titulo'])?> (<?=$lang['global']['monthly']?>):</div>
                                    <div class="col-sm-2 pt-50 pb-50 pl-0 pr-0">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="info_sku_policy_desc_cons_check">
                                            <label class="form-check-label" for="info_sku_policy_desc_cons_check"><?=$lang['global']['automatic']?></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 mt-1 pr-0" id="info_sku_policy_desc_cons_input">
                                        <input class="form-control pt-20 pb-20" type="number" value="50" maxlength="22">
                                    </div>
                                    <div class="col-sm-4 mt-1" id="expiration_date">
                                        <input class="form-control pt-20 pb-20 col-sm-12" id="info_sku_policy_expiration_date" type="date">
                                    </div>
                                </div>

                                <div class="form-group row mt-2">
                                    <div class="form-group col-md-6">
                                        <label class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_variaveis']['card2']['consumo']?></label>
                                        <select class="form-control col-md-6" id="info_sku_consumer_profile"></select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_variaveis']['card2']['previsao']?></label>
                                        <select class="form-control col-md-6" id="info_sku_forecast_type">
                                            <option value="1"><?= $lang['sku_detalhes_variaveis']['card2']['prev_stat_other_outs'] ?></option>
                                            <option value="2"><?= $lang['sku_detalhes_variaveis']['card2']['prev_stat'] ?></option>
                                            <option value="3"><?= $lang['sku_detalhes_variaveis']['card2']['other_outs'] ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-sm-12">
                                <div class="row">
                                    <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_variaveis']['card4']['titulo']?></span>
                                    <hr class="mb-0 mt-0 w-100p bb-62666b" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <div class="form-group row mt-3">
                                    <div class="form-group col-md-4">
                                        <label class="pln-bold-600" for="info_sku_policy_v_order_min"><?=strtoupper($lang['sku_detalhes_variaveis']['card4']['titulo'])?> (<?=$lang['global']['minimum']?>):</label>
                                        <input class="form-control" type="number" id="info_sku_policy_v_order_min" maxlength="22" min="1">
                                        <small class="multiple-min text-danger d-none"><?=$lang['politics']['biggerThanZero']?></small>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="pln-bold-600" for="info_sku_policy_v_order_mult"><?=$lang['sku_detalhes_variaveis']['card4']['multiplo']?>:</label>
                                        <input class="form-control" type="number" id="info_sku_policy_v_order_mult" maxlength="22" min="1">
                                        <small class="multiple-batch text-danger d-none"><?=$lang['politics']['biggerThanZero']?></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>