<div class="col-sm-12">
    <div class="element-content">
        <div class="row mt-3">
            <div class="col-sm-12">
                <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_estoques']['itemfilho']['titulo']?></span>
            </div>
        </div>
        <div class="element-box">
            <div class="children-table-container"></div>
            <div class="col-sm-12 text-center d-flex justify-content-center">
                <div id="children-table-loader" class="loader"></div>
            </div>
        </div>
    </div>
</div>
