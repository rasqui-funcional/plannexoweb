<div id="interdependence_container" class="col-sm-12">
    <div class="element-content">
        <div class="row mt-3">
            <div class="col-sm-12">
                <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_inter']['card']['titulo']?></span>
            </div>
        </div>
            <div id="interdependence-content" class="element-box">
                <div class="row">
                    <div class="col-sm-2 pln-box-title text-uppercase"><?=$lang['sku_detalhes_inter']['origin_code']?></div>
                    <div class="col-sm-2 pln-box-title text-uppercase"><?=$lang['sku_detalhes_inter']['origin_of_establishment']?></div>
                    <div class="col-sm-2 pln-box-title text-uppercase"><?=$lang['sku_detalhes_inter']['expiration_date']?></div>
                    <div class="col-sm-2 pln-box-title text-uppercase"><?=$lang['sku_detalhes_inter']['output_percent'] ?>(%)</div>
                    <div class="col-sm-1 pln-box-title text-uppercase"><?=$lang['sku_detalhes_inter']['police_origin']?></div>
                    <div class="col-sm-1 pln-box-title text-uppercase"><?=$lang['sku_detalhes_inter']['herdar_estoque']?></div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <input
                                type="number"
                                id="sku_info_interdependence_cod_origin"
                                class="form-control form-control-sm"
                        />
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control form-control-sm" id="sku_info_interdependence_estab_select">
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" id="interdep_expiration" class="form-control form-control-sm">
                        <div style="color: red; font-size: 12px;" class="mt-1" id="interdep_expiration_error_message">
                            <?=$lang['global']['expiration_date_error']?>
                        </div>
                    </div>
                    <div class="col-sm-2"><input type="number" id="interdep_percetil" min="1" max="100" class="form-control form-control-sm"></div>
                    <div class="col-sm-1">
                        <div style="height:30px"><input type="checkbox" class="form-control" id="chk-profile"><?=$lang['sku_detalhes_inter']['herdar']?></div>
                    </div>
                    <div class="col-sm-1">
                        <div style="height:30px"><input type="checkbox" class="form-control" id="chk-stock"><?=$lang['sku_detalhes_inter']['herdar_estoque']?></div>
                    </div>
                    <div
                            class="col-sm-2 pln-box-title"
                            style="cursor: pointer"
                            id="sku_info_interdependence_remove_button"
                    >
                        X <?=$lang['sku_detalhes_inter']['delete']?>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-2 pln-box-title text-uppercase"><?=$lang['sku_detalhes_inter']['police_origin']?>: </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-2" id="sku_info_interdependence_policy_of_origin"></div>
                </div>
            </div>
        <div id="interdependence_loader" class="element-box">
            <div class="col-sm-12 text-center d-flex justify-content-center">
                <div class="loader"></div>
            </div>
        </div>
    </div>
</div>