<div class="col-sm-12">
    <div class="element-content">
        <div class="row mt-4">
            <div class="col-sm-12">
                <span class="pln-bold-600">
                    <?=$lang['sku_detalhes_info']['basic_info']?>
                </span>
                <div class="col-sm-12">
                    <div class="alert alert-info alert-dismissible d-none save-sku-success">
                        <?= $lang['info_sku']['save_success'] ?>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="element-box">
                    <div id="basic_info_load" class="row basicInformation pln-h-100 d-flex align-items-center" style="display: block !important;">
                        <div class="col-sm-12 text-center d-flex justify-content-center"><div class="loader"></div></div>
                    </div>
                    <div id="basic_info_data">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <span class="pln-bold-600 text-uppercase">
                                        <?=$lang['sku_detalhes_info']['item']?>:
                                    </span>
                                </div>
                                <div class="row" id="info_sku_item"></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                <span class="pln-bold-600 text-uppercase">
                                    <?=$lang['sku_detalhes_info']['gestor']?>:
                                </span>
                                </div>
                                <div class="row">
                                    <select
                                            class="form-control form-control-sm"
                                            style="margin-right: 40px;"
                                            id="info_sku_managers"
                                    >
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                <span class="pln-bold-600 text-uppercase">
                                    <?=$lang['sku_detalhes_info']['default_warehouse']?>:
                                </span>
                                </div>
                                <div class="row">
                                    <select
                                            class="form-control form-control-sm"
                                            style="margin-right: 40px;"
                                            id="info_sku_standard_deposits"
                                    >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-2">
                                <div class="row">
                                <span class="pln-bold-600 text-uppercase">
                                    <?=$lang['sku_detalhes_info']["establishment"]?>:
                                </span>
                                </div>
                                <div class="row" id="info_sku_establishment"></div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                <span class="pln-bold-600 text-uppercase">
                                    <?=$lang['sku_detalhes_info']["unity"]?>:
                                </span>
                                </div>
                                <div class="row" id="info_sku_unity"></div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                <span class="pln-bold-600 text-uppercase">
                                    <?="{$lang['global']['cost']} {$lang['global']['unity_abbr']} ({$lang['global']['country_currency']}) {$lang['global']['erp']}"?>:
                                </span>
                                </div>
                                <div class="row" id="info_sku_unity_value"></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                <span class="pln-bold-600 text-uppercase">
                                    <?=$lang['global']['category']?>:
                                </span>
                                </div>
                                <div class="row" id="info_sku_category"></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                <span class="pln-bold-600 text-uppercase">
                                    <?=$lang['global']['comments']?>:
                                </span>
                                </div>
                                <div class="row" id="info_sku_category">
                                    <textarea rows="3" cols="35" id="info_sku_observation"></textarea>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="row">
                                    <span class="pln-bold-600 text-uppercase">
                                        <?=$lang['sku_detalhes_info']['curvas']?>:
                                    </span>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="row">
                                            <span class="pln-bold-600 text-uppercase" style="margin-right: 10px;">
                                                <?=$lang['sku_detalhes_info']['abc']?>:
                                            </span>
                                            <div id="info_sku_curve_abc"></div>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="row">
                                        <span class="pln-bold-600 text-uppercase" style="margin-right: 10px;">
                                            <?=$lang['sku_detalhes_info']['pqr']?>:
                                        </span>
                                            <div id="info_sku_curve_pqr"></div>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="row">
                                        <span class="pln-bold-600 text-uppercase" style="margin-right: 10px;">
                                            <?=$lang['sku_detalhes_info']['123']?>:
                                        </span>
                                            <div id="info_sku_curve_123"></div>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="row">
                                        <span class="pln-bold-600 text-uppercase" style="margin-right: 10px;">
                                            <?=$lang['sku_detalhes_info']['xyz']?>:
                                        </span>
                                            <div id="info_sku_curve_xyz"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="row">
                                            <span class="pln-bold-600 text-uppercase" style="margin-right: 10px;">
                                                <?=$lang['sku_detalhes_info']['cod_sku']?>:
                                            </span>
                                            <div id="info_sku_cod"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                        <span class="pln-bold-600 text-uppercase">
                            <?=$lang['sku_detalhes_info']['grupo']?>
                        </span>
                        </div>
                        <div
                                class="row"
                                id="info_sku_scm_group"
                        >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
