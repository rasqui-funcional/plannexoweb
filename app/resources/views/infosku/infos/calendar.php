<div class="col-sm-12">
    <div class="element-content">
        <div class="row mt-3">
            <div class="col-sm-7">
                <span class="pln-bold-600 text-uppercase">
                    <?=$lang['sku_detalhes_calendario']['calendar']?>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="element-box">
                    <div id="calendar_load" class="row basicInformation pln-h-100 d-flex align-items-center"
                         style="display: block !important;">
                        <div class="col-sm-12 text-center d-flex justify-content-center">
                            <div class="loader"></div>
                        </div>
                    </div>
                    <div id="calendar_data">
                        <div id="sku_info_calendar_without_data text-uppercase">
                            <?=$lang['global']['no_items_to_display']?>
                        </div>
                        <div id="sku_info_calendar_with_data">
                            <div class="row">
                                <div class="col-sm-2">
                                    <select
                                            class="form-control form-control-sm"
                                            style="margin-right: 40px;"
                                            id="info_sku_calendar_batch_of_purchases"
                                    >
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <span class="pln-bold-600 text-uppercase"><?=$lang['sku_detalhes_calendario']['shopping_lote']?> (<?=$lang['global']['days']?>):</span>
                                    <span class="pln-bold-600" id="info_sku_calendar_batch_of_purchases_in_days"></span>
                                </div>
                            </div>
                            <div class="row mt-4 justify-content-between">
                                <div class="col-sm-1" style="margin-right: 90px">
                                    <div id="datepicker1" width="270"></div>
                                </div>
                                <div class="col-sm-1" style="margin-right: 90px">
                                    <div id="datepicker2" width="270"></div>
                                </div>
                                <div class="col-sm-1" style="margin-right: 90px">
                                    <div id="datepicker3" width="270"></div>
                                </div>
                                <div class="col-sm-1" style="margin-right: 90px">
                                    <div id="datepicker4" width="270"></div>
                                </div>
                                <div class="col-sm-1" style="margin-right: 90px">
                                    <div id="datepicker5" width="270"></div>
                                </div>
                                <div class="col-sm-1" style="margin-right: 90px">
                                    <div id="datepicker6" width="270"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
