<? if (!$ViewData) {
    echo 'false';
    return;
} ?>

<? if (is_null($ViewData['data']['children'])):
    echo $ViewData['data']['error'];
    return;
endif; ?>

<div class="table-responsive table-wrapper-scroll-y">
    <table id="children-table" class="table">
        <thead>
            <tr>
                <th class="pln-borderless pln-padding-bottom-0 pln-infos-title text-left pln-children-table-th text-uppercase"><?=$lang['sku_detalhes_estoques']['parent_item']?></th>
                <th class="pln-borderless pln-padding-bottom-0 pln-blue-th text-right pln-children-table-th"><?=$lang['sku_detalhes_estoques']['stock_available'] ?></th>
                <th class="pln-borderless pln-padding-bottom-0 pln-blue-th text-right pln-children-table-th"><?=$lang['sku_detalhes_estoques']['coverage']?></th>
                <th class="pln-borderless pln-padding-bottom-0 pln-blue-th text-right pln-children-table-th"><?=$lang['sku_detalhes_estoques']['stock_financial']?></th>
                <th class="pln-borderless pln-padding-bottom-0 pln-blue-th text-right pln-children-table-th"><?=$lang['sku_detalhes_estoques']['solicitation']?></th>
                <th class="pln-borderless pln-padding-bottom-0 pln-blue-th text-right pln-children-table-th"><?=$lang['sku_detalhes_estoques']['orders']?></th>
            </tr>
            <tr>
                <td class='pln-borderless pln-padding-top-0 pln-children-table-th pln-infos-heavy-title text-left'>
                    <?= $ViewData['data']['parent']['DESC_ITEM']; ?>
                </td>
                <td class='pln-borderless pln-padding-top-0 pln-children-table-th text-right' >
                    <span>
                        <?= Utilities::bigNumber($ViewData['data']['parent']['QTY_INV']); ?>
                    </span>
                </td>
                <td class='pln-borderless pln-padding-top-0 pln-children-table-th text-right' >
                    <span><?php echo Utilities::bigNumber($ViewData['data']['parent']['COBERTURA']) . " {$lang['global']['days']}"; ?></span>
                </td>
                <td class='pln-borderless pln-padding-top-0 pln-children-table-th text-right' >
                    <span><?php echo  "{$lang['global']['country_currency']} " . Utilities::Currency($ViewData['data']['parent']['ESTOQUE_FINANCEIRO']); ?></span>
                </td>
                <td class='pln-borderless pln-padding-top-0 pln-children-table-th text-right' >
                    <span>
                        <?= Utilities::bigNumber($ViewData['data']['parent']['SOLICITACOES']); ?>
                    </span>
                </td>
                <td class='pln-borderless pln-padding-top-0 pln-children-table-th text-right' >
                    <span>
                        <?= Utilities::bigNumber($ViewData['data']['parent']['ORDENS']); ?>
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="pln-borderless pln-padding-top-0 pln-children-table-th pln-infos-title">
                    <?=$lang['sku_detalhes_estoques']['itemfilho']['titulo']?>
                </td>
            </tr>
        </thead>
        <tbody>
            <? foreach($ViewData['data']['children'] as $item):
                $tRows += 1; ?>
                <tr>
                    <td class="text-left">
                        <span>
                            <?= $item['DESC_ITEM']; ?>
                        </span>
                    </td>
                    <td class="text-right">
                        <span>
                            <?= Utilities::bigNumber($item['QTY_INV']); ?>
                        </span>
                    </td>
                    <td class="text-right">
                    <span>
                        <? if (!empty($item['COBERTURA'])): ?>
                            <?= $item['COBERTURA'] . ' dias'; ?>
                        <? endif; ?>
                    </span>
                    </td>
                    <td class="text-right">
                    <span>
                        <? if (!empty($item['ESTOQUE_FINANCEIRO'])): ?>
                            <?= $posicaoestoque['moeda'] . Utilities::Currency($item['ESTOQUE_FINANCEIRO']); ?>
                        <? endif; ?>
                    </span>
                    </td>
                    <td class="text-right">
                        <span>
                            <?= Utilities::bigNumber($item['SOLICITACOES']); ?>
                        </span>
                    </td>
                    <td class="text-right">
                        <span>
                            <?= Utilities::bigNumber($item['ORDENS']); ?>
                        </span>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <th class="text-left">
                    <?=$lang['sku_detalhes_estoques']['itemfilho']['titulo']?>: <span><?php echo Utilities::bigNumber($tRows); ?></span>
                </th>
                <th class="text-right">
                    <span>
                        <?= Utilities::bigNumber($ViewData['data']['footer']['TAVAILABLE']); ?>
                    </span>
                </th>
                <th class="text-right">
                    <span>
                        <?= Utilities::bigNumber($ViewData['data']['footer']['TCOVERAGE']) . " dias"; ?>
                    </span>
                </th>
                <th class="text-right">
                     <span><?php echo "{$lang['global']['country_currency']} " . Utilities::Currency($ViewData['data']['footer']['TINVVAL']); ?></span>
                </th>
                <th class="text-right">
                    <span>
                        <?= Utilities::bigNumber($ViewData['data']['footer']['TREQ']); ?>
                    </span>
                </th>
                <th class="text-right">
                    <span>
                        <?= Utilities::bigNumber($ViewData['data']['footer']['TORDERS']); ?>
                    </span>
                </th>
            </tr>
        </tfoot>
    </table>
</div>
