<div class="modal fade m-3 rounded" id="modal_info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">

    <div class="modal-dialog pln-modal-info modal-lg modal-dialog-scrollable">
        <div class="modal-content">

            <div class="modal-header position-sticky bg-white pb-0 pt-3 pln-modal-header">
                <div class="container-fluid">
                    <div class="row">
                        <button aria-label="Close" class="close p-0 m-0 pln-close-modal" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span><small><?=$lang['modal']['fechar']?></small></button>
                        <div class="col-sm-12">
                            <?php include('header.php'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-body pt-0">
                <?php include('modal-body.php'); ?>
            </div>

            <div class="modal-footer pln-modal-footer p-3" style="display: none;">
                <?php include('modal-footer.php'); ?>
            </div>

        </div>
    </div>

</div>