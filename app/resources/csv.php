<?php
if ($ViewData['error']) {
    echo $ViewData['error'];
    exit;
}

Request::generateCSVHeaders();
// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');
$translatedTitles = [];
foreach ($ViewData['data'][0] as $title => $value) {
    $translatedTitles[$title] = $lang['column'][strtolower($title)];
}

fputcsv($output, mb_convert_encoding($translatedTitles, 'ISO-8859-1'), ';');

// loop over the rows, outputting them
foreach ($ViewData['data'] as $row) {
    $data = [];
    foreach ($row as $key => $value) {
        $data[$key] = Utilities::formatCsvFields($key, $value);
    }

    fputcsv($output, mb_convert_encoding($data, 'ISO-8859-1'), ';');
}
?>