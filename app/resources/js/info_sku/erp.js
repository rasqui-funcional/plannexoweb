class erp extends AbstractInfoSku{
    constructor() {
        super();
        this.erpTab = jQuery("#erp_tab");
        this.btn = jQuery("#search_cod");
        this.listenerBtns();
        this.sku_item = "";
        this.cod_estab = "";
        this.table_option = $("#erp-select").val();
        this.limit = "";
        this.page = "";
        this.init = false;
        this.sku_id_temp;
        this.cod = "";
        this.orderCol = "";
        this.orderType = "";
    }

    //adiciona eventos aos componentes que terão interação com o usuário (linha das tabelas, botão de alterar status e etc)
    listenerBtns(){
        utilsInstance.onlyNumbers("#goto_pagenumber_erp")

        $('#dedashboard_tab').on('click',(e) => {
            this.init = false
        });

        $('#searchERPPage').on('click',(e) => {
            this.goToPageNumberERP()
        });

        $('#info_tab').on('click',(e) => {
            this.init = false
        });

        $('#log_tab').on('click',(e) => {
            this.init = false
        });

        $('.download').unbind("click").on('click',(x) => {
            var selectedTable = jQuery("#erp-select").val();
            this.sku_item = jQuery('#info_item').html();
            this.cod_estab = jQuery('#info_cod_estab').html();
            window.open(`/${myCountry.getCountry()}/csv/${selectedTable}/${this.sku_item}/${this.cod_estab}/${this.orderCol}/${this.orderType}/${this.cod}`);
        });
        

        this.erpTab.unbind("click").on("click",(e)=> {
            if(this.sku_id_temp != AbstractInfoSku.getSkuID){
                jQuery('.erp_table').html(' ')
                jQuery('.erp_table').empty()
                this.orderCol = '';
                this.orderType = '';
                this.cod = '';
                $('#cod_search').val('');
                $('#cod_search').empty();
                jQuery('#erp-select').val('MonthlyConsumption')
                this.sku_item = jQuery('#info_item').html()
                this.cod_estab = jQuery('#info_cod_estab').html()
                this.page = jQuery("#goto_pagenumber_erp").val()
                jQuery("#pln-pagination-footer_erp").show()
                this.init = true
                this.loadTableIni("MonthlyConsumption", this.sku_item, this.cod_estab, 1, '','', true)
                this.init = false
            }
            this.sku_id_temp = AbstractInfoSku.getSkuID;
        });

        $('#erp-select').on('change',(e) => {
            jQuery('.erp_table').html(' ')
            jQuery('.erp_table').empty()
            this.orderCol = '';
            this.orderType = '';
            this.cod = '';
            $('#cod_search').val('');
            $('#cod_search').empty();
            this.table_option = $('#erp-select').val()
            switch (this.table_option){

             case "PurchaseOrder":
                jQuery('#search_label').show()
                jQuery('#input_wrapper').show()
                jQuery('#btn_wrapper').show()
                jQuery('.clearSearch').show()
                jQuery('#search_label').html("Ordem")
                break;
            case "PurchaseRequest":
                jQuery('#search_label').show()
                jQuery('#input_wrapper').show()
                jQuery('#btn_wrapper').show()
                jQuery('.clearSearch').show()
                jQuery('#search_label').html("Depósito")
                break;
            case "InTransaction":
                jQuery('#search_label').show()
                jQuery('#input_wrapper').show()
                jQuery('#btn_wrapper').show()
                jQuery('.clearSearch').show()
                jQuery('#search_label').html("Código")
                break;
            default:
                jQuery('#search_label').hide()
                jQuery('#input_wrapper').hide()
                jQuery('#btn_wrapper').hide()
                jQuery('.clearSearch').hide()
                break;
            }

            this.sku_item = jQuery('#info_item').html()
            this.cod_estab = jQuery('#info_cod_estab').html()
            this.page = jQuery("#goto_pagenumber_erp").val()
            jQuery("#pln-pagination-footer_erp").show()
            this.init = true
            this.loadTableIni($('#erp-select').val(), this.sku_item, this.cod_estab, 1, '', '', true)
            this.init = false
        });

        this.btn.on('click', (e) => {
            this.init = true
            this.cod = $('#cod_search').val();
            this.loadTableIni(this.table_option, this.sku_item, this.cod_estab, 1, '', this.cod, true)
            this.init = false
        });

        $('.clearSearch').on('click', (e) => {
           
            $('#cod_search').val('') 
            $('#cod_search').empty()
            this.loadTableIni(this.table_option, this.sku_item, this.cod_estab, 1, '', '', true)
        });

        $('.erp_table').unbind("click").delegate('.order-col-erp', 'click', (e) => {
            let this_icon = jQuery(e.currentTarget).find("i"),
                sort = this_icon.hasClass("fa-sort"),
                sortAsc = this_icon.hasClass("fa-sort-asc"),
                iconEl = this_icon,                
                order = "",
                columnName = jQuery(e.currentTarget).attr('data-id');
                //searchValue = jQuery('#search_input').val();

            jQuery(e.currentTarget).parent().parent().find("i").each(function(){
                jQuery(this).removeClass("fa-sort-asc fa-sort-desc").addClass("fa-sort")
            });

            if(sort){
                this_icon.removeClass("fa-sort").addClass("fa-sort-asc");
                order="asc";
            }else{
                if(sortAsc){
                    iconEl.removeClass("fa-sort-asc").addClass("fa-sort-desc");
                    order="desc";
                }else{
                    iconEl.removeClass("fa-sort-desc").addClass("fa-sort-asc");
                    order="asc";
                }
            }

            this.orderCol = columnName;
            this.orderType = order;
            const columnOrder = {[columnName] : order};            
            this.loadTableIni(this.table_option, this.sku_item, this.cod_estab, 1, columnOrder, this.cod, false)
        });
    }

    loadTableIni(table, skuItem, codEstab, page = 1, order, search, recalcFooter) { 
        $(".element-erp").hide();
        $("#erp-warning-box").hide();
        $(".erp-spin").attr("style", "display:flex !important");
        $.ajax({
            url: `/${myCountry.getCountry()}/infosku/erp/${table}`,
            data:{
                table: table,
                skuItem: skuItem,
                codEstab,
                page: page,
                order: order,
                search: search
            },
            type: 'POST',
            datatype: 'text',
            error: function (error) {
                console.log(error);
                alert(i18nInfoSku[myCountry.getCountry()].notPossible + i18nInfoSku[myCountry.getCountry()][table]+'. '+i18nGlobals[myCountry.getCountry()].caseErrPersist);
            }
        }).done(response=>{

            $(".element-erp").show();
            $(".erp-spin").attr("style", "display:none !important");
            if(response == 'false'){
                $('#erp-info-box').hide();
                $("#erp-warning-box").show();
            }else{
                $('#erp-info-box').show();
                $('.erp_table').html(response);
            }
            if(recalcFooter){
                $('#paginationERP').twbsPagination('destroy');                
                this.paginate(page);
            }
            this.paginateLegendsERP();
            this.init = true;
            
        });        
    }

    fillTableRows(data) {
        data = Object.values(data)
        if (!Array.isArray(data) || !data.length) 
        {
            this.execSwal("",i18nGlobals[myCountry.getCountry()].table.noDataSpecificItem, "warning");
        } else {
                jQuery("#erp_table").append(`<thead><tr>`);
                    Object.values(data[0]).forEach(function (value, index) {
                    jQuery("#erp_table").append(`<th style=" background-color: #333333;
                    color: #ffffff;">${value}</th>`);
                    });
                jQuery("#erp_table").append(`</tr></thead>`);

                Object.values(data[1]).forEach(function (value, index) {
                jQuery("#erp_table").append(`<tbody><tr>`);
                    Object.values(value).forEach(function (newValue, index) {
                    jQuery("#erp_table").append(`<td>${newValue}</td>`);
                    });
                jQuery("#erp_table").append(`</tr></tbody>`);
            });
        }
    }

    execSwal(header, text, alertType){
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn pln-btn-orange',        
        buttonsStyling: false,
      });
      swalWithBootstrapButtons({        
        animation: false,
        text: text,
        type: alertType,
        showCancelButton: false,
        confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButton
      });    
}

    
    //ir para página X (Buscar)
    goToPageNumberERP(){
        if( parseInt(jQuery("#goto_pagenumber_erp").val()) == jQuery('#paginationERP').twbsPagination("getCurrentPage") ){
            return false;
        }else if( parseInt(jQuery("#goto_pagenumber_erp").val()) > parseInt(jQuery("#total_page_number_erp").val()) ){
            execSwal("",`${i18nGlobals[myCountry.getCountry()].table.insertPage} ${jQuery("#total_page_number_erp").val()}`, "warning");
            return false;
        }else if( jQuery("#goto_pagenumber_erp").val() == "" || parseInt(jQuery("#goto_pagenumber_erp").val()) =="0" ){        
            execSwal("",i18nGlobals[myCountry.getCountry()].table.insertValidPage , "warning");
            return false;
        }
            this.init=false,
            this.cod = $('#cod_search').val(); 
            this.pageNumber = jQuery("#goto_pagenumber_erp").val();
            this.loadTableIni(this.table_option, this.sku_item, this.cod_estab, this.pageNumber, '', this.cod, true)        
    }

    paginateLegendsERP(){

        var totalPageNumber = jQuery("#total_page_number_erp").val(),
            itemsPerPage = parseInt(jQuery('#items_per_page_erp').val()),        
            currentPage = jQuery("#paginationERP").twbsPagination("getCurrentPage"),
            pageItemsTo = itemsPerPage * currentPage,
            pageItemsFrom = (pageItemsTo - itemsPerPage) + 1,
            totalItems = parseInt(jQuery("#hdn_total_items_erp").val());

        jQuery("#spn_total_page_number_erp").html( totalPageNumber );
        jQuery("#page_items_from_erp").html( pageItemsFrom );
        jQuery("#page_items_to_erp").html( pageItemsTo > totalItems ? totalItems : pageItemsTo );
        jQuery("#page_items_total_erp").html( totalItems );        

        jQuery("#total_items_erp").html( jQuery("#page_from_to_erp").html() );
    }
    //realiza a paginação e invoca getTableService() para popular a tabela com 
    //os dados refentes à pagina e a quantidade de itens escolhida
    paginate(startPg) {    
        var objPagination = jQuery('#paginationERP');
        objPagination.twbsPagination({
            totalPages: jQuery("#total_page_number_erp").val(),
            startPage: startPg == undefined ? 1 : startPg*1,
            first: ' <i class="fas fa-angle-double-left"></i>',
            last: '<i class="fas fa-angle-double-right"></i>',
            prev: ' <i class="fas fa-angle-left"></i>',
            next: '<i class="fas  a fa-angle-right"></i>',
            visiblePages: 3,
            onPageClick: (event, page) =>{
                let limit = jQuery("#items_per_page_erp").val();
                if(this.init){
                    this.table_option = $('#erp-select').val()
                    this.sku_item = jQuery('#info_item').html()
                    this.cod_estab = jQuery('#info_cod_estab').html()
                    this.limit = jQuery('#items_per_page_erp').val()
                    this.page = jQuery("#goto_pagenumber_erp").val()
                    this.cod = $('#cod_search').val(); 
                    jQuery("#pln-pagination-footer_erp").show();
                    this.loadTableIni(this.table_option, this.sku_item, this.cod_estab, page, '', this.cod, false)
                }
                this.init = true;
            }
        })
    }
}