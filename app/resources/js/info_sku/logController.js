class LogController extends AbstractInfoSku {
    constructor() {
        super();
        this.orderColumns = {};
        this.quantityPages = null;
        this.listenLogTabClick();
        this.listenColumnOrder();
        this.listenSeachButton();
        this.listenItemsPerPage();
        this.listenPaginationButtons();
        this.listenDownloadCsvClick();
        utilsInstance.onlyNumbers('#sku_log_goto_pagenumber');
    }
    
    listenLogTabClick() {
        jQuery('#log_tab').unbind('click').on('click', () => {
            jQuery('#sku_log_goto_page_number').val(1)
            this.fetch();
        });
    }

    // todo regra muito confusa, se faz necessário feratorar dps
    listenColumnOrder() {
        jQuery('.table-log .order-col').unbind('click').on('click', (element) => {
            var currentIcon = jQuery(element.currentTarget).find('i');
            var sort = currentIcon.hasClass('fa-sort');
            var sortAsc = currentIcon.hasClass('fa-sort-asc');
            var iconElement = currentIcon;
            var columnName = jQuery(element.currentTarget).attr('data-col-log');
            var order = '';
  
            if (sort) {
                currentIcon.removeClass('fa-sort').addClass('fa-sort-asc');
                order = 'asc';
            } else {
                if (sortAsc) {
                    iconElement.removeClass('fa-sort-asc').addClass('fa-sort-desc');
                    order = 'desc';
                } else {
                    iconElement.removeClass('fa-sort-desc').addClass('fa-sort-asc');
                    order = 'asc';
                }
            }
  
            this.orderColumns[columnName] = order;
            this.fetch();
        });
    }

    listenSeachButton() {
        jQuery('#sku_log_search_button').unbind('click').on('click', () => {
            const pageNumber = jQuery('#sku_log_goto_page_number').val();
            if (pageNumber > this.quantityPages) {
                this.execSwal(`${i18nGlobals[myCountry.getCountry()].table.insertPage} ${this.quantityPages}`, 'warning');
            } else if (pageNumber < 1) {
                this.execSwal(i18nGlobals[myCountry.getCountry()].table.insertValidPage, 'warning');
            } else {
                this.fetch();
            }
        });
    }
    
    listenerBtns(){
        this.logTab.unbind("click").on("click", ()=>{
            if(this.sku_id_temp != AbstractInfoSku.getSkuID){
                jQuery(".log-pln-items-search").removeClass("d-flex").hide();
                jQuery('.log-table-container').html("");
                this.loader.show();
                this.getTableServiceIni();
            }
        });
    }

    listenItemsPerPage() {
        jQuery('#sku_log_items_per_page').unbind('change').on('change', () => {
            jQuery('#sku_log_goto_page_number').val('');
            this.fetch();
        });

        jQuery("#download_csv").unbind("click").on("click", function() {
            let searchValue = AbstractInfoSku.getSkuID;
            window.open(`/${myCountry.getCountry()}/csv/skulog/${searchValue}`)
        });

        //evento do combo que altera a quantidade de itens por página que o usuário deseja visualizar.
        jQuery("#log_items_per_page").on("change", (element)=>{
            this.loading.click();
            let limit = jQuery(element.currentTarget).val();                            
            this.global_init=false;
            
            this.getTableService(1, limit, true, false);
        });
    }

    listenPaginationButtons() {
        jQuery('.page-item.first').unbind('click').on('click', () => {
            if (!jQuery('.page-item.first').hasClass('disabled')) {
                jQuery('#sku_log_goto_page_number').val(1);
                this.fetch();
            }
        });

        jQuery('.page-item.prev').unbind('click').on('click', () => {
            if (!jQuery('.page-item.prev').hasClass('disabled')) {
                const idButtonSelected = this.idButtonSelected();
                const value = parseInt(jQuery(idButtonSelected).html(), 10);
                jQuery('#sku_log_goto_page_number').val(value - 1);
                this.fetch();
            }
        });

        jQuery('.page-item first').unbind('click').on('click', () => {
            const value = jQuery('#sku_log_page_item_1_value').html();
            jQuery('#sku_log_goto_page_number').val(value);
            this.fetch();
        });

        jQuery('#sku_log_page_item_1_background').unbind('click').on('click', () => {
            const value = jQuery('#sku_log_page_item_1_value').html();
            jQuery('#sku_log_goto_page_number').val(value);
            this.fetch();
        });

        jQuery('#sku_log_page_item_2_background').unbind('click').on('click', () => {
            const value = jQuery('#sku_log_page_item_2_value').html();
            jQuery('#sku_log_goto_page_number').val(value);
            this.fetch();
        });

        jQuery('#sku_log_page_item_3_background').unbind('click').on('click', () => {
            const value = jQuery('#sku_log_page_item_3_value').html();
            jQuery('#sku_log_goto_page_number').val(value);
            this.fetch();
        });

        jQuery('.page-item.next').unbind('click').on('click', () => {
            if (!jQuery('.page-item.next').hasClass('disabled')) {
                const idButtonSelected = this.idButtonSelected();
                const value = parseInt(jQuery(idButtonSelected).html(), 10);
                jQuery('#sku_log_goto_page_number').val(value + 1);
                this.fetch();
            }
        });

        jQuery('.page-item.last').unbind('click').on('click', () => {
            if (!jQuery('.page-item.last').hasClass('disabled')) {
                const value = jQuery('#sku_log_quantity_pages').html();
                jQuery('#sku_log_goto_page_number').val(value);
                this.fetch();
            }
        });

    }

    fetch() {
        jQuery('#sku_log_data').hide();
        jQuery('#sku_log_without_data').attr('style', 'display:none').removeClass('d-flex');
        
        jQuery('#sku_log_load').attr('style', 'display:flex !important');

        $.ajax({
            url: `/${myCountry.getCountry()}/infosku/log/fetch`,
            type: 'POST',
            datatype: 'json',
            data: {
                id: AbstractInfoSku.getSkuID,
                orderColumns: this.orderColumns,
                page: parseInt(jQuery('#sku_log_goto_page_number').val()) || 1,
                limit: jQuery('#sku_log_items_per_page').val()
            },
            success: (response) => {
                jQuery('#sku_log_load').attr('style', 'display:none !important');
                jQuery('#sku_log_data').show();
                jQuery('#sku_log_body_table').empty();
                jQuery('.pln-items-search, .log-pln-items-search').removeClass('d-none').addClass('d-flex');

                const { items, startItem, endItem, quantityPages, quantitySkuLog, listFooterPages } = response.data;
                this.quantityPages = quantityPages;

                if (items.length !== 0) {
                    jQuery('#sku_log_with_data').show();
                    jQuery('#sku_log_without_data').hide();

                    jQuery('#sku_log_page_items_from').html(startItem);
                    jQuery('#sku_log_page_items_to').html(endItem);
                    jQuery('#sku_log_quantity_items').html(quantitySkuLog);
                    jQuery('#sku_log_quantity_pages').html(quantityPages);
                    this.buildFooterPages(listFooterPages);

                    items.forEach((item) => {
                        var labels = "",
                            valuesOld = "",
                            valuesNew = "";

                        const description = JSON.parse(item.DESCRIPTION);

                        var row = `<tr>
                            <td>${item.ID}</td>
                            <td>${item.CREATOR}</td>
                            <td>${item.INSERTED_DATE}</td>
                            <td>${item.USERNAME}</td>
                            <td>${item.IP_USER}</td>
                            <td>${item.APPLICATION}</td>
                            <td>${item.ACTION}</td>`;

                        if (description.fields.length !== 0) {
                            for (var i = 0; i < description.fields.length; i++ ) {
                                if (typeof labels === "undefined") {
                                    labels = `${description.fields[i].label}</br>`;
                                } else {
                                    labels += `${description.fields[i].label}</br>`;
                                }

                                if (typeof valuesOld === "undefined") {
                                    valuesOld = `${description.fields[i].old}</br>`;
                                } else {
                                    valuesOld += `${description.fields[i].old}</br>`;
                                }

                                if (typeof valuesNew === "undefined") {
                                    valuesNew = `${description.fields[i].new}</br>`;
                                } else {
                                    valuesNew += `${description.fields[i].new}</br>`;
                                }
                            }
                        }

                        row += `<td>${labels}</td>`;
                        row += `<td>${valuesOld}</td>`;
                        row += `<td>${valuesNew}</td>`;
                        row += `</tr>`;

                        jQuery('#sku_log_body_table').append(row);
                    });
                } else {
                    jQuery('#sku_log_without_data').addClass('d-flex').show();
                    jQuery('#sku_log_with_data').hide();
                    jQuery('.pln-items-search, .log-pln-items-search').removeClass('d-flex').addClass('d-none');
                }
            },
        });
    }

    execSwal(text, alertType) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            animation: false,
            text: text,
            type: alertType,
            showCancelButton: false,
            confirmButtonText: 'Ok'
        });
    }

    buildFooterPages(listFooterPages) {
        const condiction = listFooterPages['condiction'];
        const list = listFooterPages['list'];
        switch (condiction) {
            case 'start':
                this.buildFooterPageStart(list);
                break;
            case 'end':
                this.buildFooterPageEnd(list);
                break;
            case 'middle':
                this.buildFooterPageMiddle(list);
                break;
            default:
                throw 'Invalid condiction';
        }
    }

    buildFooterPageStart(list) {
        jQuery('#sku_log_page_item_1_background').show();
        jQuery('#sku_log_page_item_2_background').show();
        jQuery('#sku_log_page_item_3_background').show();

        if (list.length == 3) {
            jQuery('.page-item.first').addClass('disabled');
            jQuery('.page-item.prev').addClass('disabled');

            jQuery('#sku_log_page_item_1_background').addClass('active');
            jQuery('#sku_log_page_item_1_value').html(list[0]);

            jQuery('#sku_log_page_item_2_background').removeClass('active');
            jQuery('#sku_log_page_item_2_value').html(list[1]);

            jQuery('#sku_log_page_item_3_background').removeClass('active');
            jQuery('#sku_log_page_item_3_value').html(list[2]);

            jQuery('.page-item.next').removeClass('disabled');
            jQuery('.page-item.last').removeClass('disabled');
        } else if (list.length == 2) {
            jQuery('.page-item.first').addClass('disabled');
            jQuery('.page-item.prev').addClass('disabled');

            jQuery('#sku_log_page_item_1_background').addClass('active');
            jQuery('#sku_log_page_item_1_value').html(list[0]);

            jQuery('#sku_log_page_item_2_background').removeClass('active');
            jQuery('#sku_log_page_item_2_value').html(list[1]);

            jQuery('#sku_log_page_item_3_background').hide();

            jQuery('.page-item.next').removeClass('disabled');
            jQuery('.page-item.last').removeClass('disabled');
        } else if (list.length == 1) {
            jQuery('.page-item.first').addClass('disabled');
            jQuery('.page-item.prev').addClass('disabled');

            jQuery('#sku_log_page_item_1_background').addClass('active');
            jQuery('#sku_log_page_item_1_value').html(list[0]);

            jQuery('#sku_log_page_item_2_background').hide();

            jQuery('#sku_log_page_item_3_background').hide();

            jQuery('.page-item.next').addClass('disabled');
            jQuery('.page-item.last').addClass('disabled');
        } else {
            // nothin todo
        }
    }

    buildFooterPageEnd(list) {
        jQuery('#sku_log_page_item_1_background').show();
        jQuery('#sku_log_page_item_2_background').show();
        jQuery('#sku_log_page_item_3_background').show();

        if (list.length === 3) {
            jQuery('.page-item.first').removeClass('disabled');
            jQuery('.page-item.prev').removeClass('disabled');

            jQuery('#sku_log_page_item_1_background').removeClass('active');
            jQuery('#sku_log_page_item_1_value').html(list[0]);

            jQuery('#sku_log_page_item_2_background').removeClass('active');
            jQuery('#sku_log_page_item_2_value').html(list[1]);

            jQuery('#sku_log_page_item_3_background').addClass('active');
            jQuery('#sku_log_page_item_3_value').html(list[2]);

            jQuery('.page-item.next').addClass('disabled');
            jQuery('.page-item.last').addClass('disabled');
        } else if (list.length === 2) {
            jQuery('.page-item.first').removeClass('disabled');
            jQuery('.page-item.prev').removeClass('disabled');

            jQuery('#sku_log_page_item_1_background').removeClass('active');
            jQuery('#sku_log_page_item_1_value').html(list[0]);

            jQuery('#sku_log_page_item_2_background').addClass('active');
            jQuery('#sku_log_page_item_2_value').html(list[1]);

            jQuery('#sku_log_page_item_3_background').hide();

            jQuery('.page-item.next').addClass('disabled');
            jQuery('.page-item.last').addClass('disabled');
        } else {
            // nothin todo
        }
    }

    buildFooterPageMiddle(list) {
        jQuery('.page-item.first').removeClass('disabled');
        jQuery('.page-item.prev').removeClass('disabled');

        jQuery('#sku_log_page_item_1_background').removeClass('active');
        jQuery('#sku_log_page_item_1_value').html(list[0]);

        jQuery('#sku_log_page_item_2_background').addClass('active');
        jQuery('#sku_log_page_item_2_value').html(list[1]);

        jQuery('#sku_log_page_item_3_background').removeClass('active');
        jQuery('#sku_log_page_item_3_value').html(list[2]);

        jQuery('.page-item.next').removeClass('disabled');
        jQuery('.page-item.last').removeClass('disabled');
    }

    idButtonSelected() {
        let result = '';

        for (let index = 1; index <= 3; index++) {
            if (jQuery('#sku_log_page_item_'+ index +'_background').hasClass('active')) {
                result = '#sku_log_page_item_'+ index +'_value';
                break;
            }
        }

        return result;
    }

    listenDownloadCsvClick() {
        jQuery('#download_csv').unbind('click').on('click', () => {
            let searchValue = AbstractInfoSku.getSkuID;
            window.open(`/${myCountry.getCountry()}/csv/skulog/${searchValue}`)
        });
    }
}