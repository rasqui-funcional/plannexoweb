class childrenItemController {
    constructor() {
        this.startElement();
        this.showFooter();
    }

    fetchChildrenData(codItem, codEstab) {
        $.ajax({
            url: `/${myCountry.getCountry()}/infosku/infos/children_item_list`,
            type: 'POST',
            dataType: 'text',
            data: {
                codItem: codItem,
                codEstab: codEstab
            },
            error: () => {
                jQuery('#depend-loader').attr("style", "display:none !important");
                jQuery('.children-table-container').show();
            },
            success: (response) => {
                jQuery('#children-table-loader').attr("style", "display:none !important");
                jQuery('.children-table-container').show();
                jQuery('.children-table-container').html(response);
            }
        });
    }

    startElement() {
        $('#information_tab').on('click', () => {
            const codItem = $('#info_item').html();
            const codEstab = $('#info_cod_estab').html();
            jQuery('.children-table-container').hide();
            jQuery('#children-table-loader').attr("style", "display:flex !important");
            this.fetchChildrenData(codItem, codEstab);
        });
    }

    showFooter() {
        jQuery('#children-table').scroll();
    }
}
