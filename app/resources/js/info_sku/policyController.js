class PolicyController extends AbstractInfoSku {
    constructor() {
        super();
        this.requestData = {};
        this.manualDataUpdate = null;
        this.fetchPoliticsInformationsByTabClick();
        this.fetchPoliticsBySelectChange();
        utilsInstance.onlyNumbers(`
            #info_sku_policy_days_min_input,
            #info_sku_policy_lt_planner_input,
            #id_sku_pkinfo_sku_policy_lt_planner_input,
            #info_sku_policy_lt_buyer_input,
            #info_sku_policy_lt_supplier_input,
            #info_sku_policy_lt_receiving_input,
            #info_sku_policy_lt_others_input, 
            #info_sku_policy_v_order_min, 
            #info_sku_policy_v_order_mult,
            #info_sku_policy_desc_cons_input
        `);

        utilsInstance.biggerThenZero('#info_sku_policy_v_order_min, #info_sku_policy_v_order_mult');
        this.mapSaveClick();
    }

    mapSaveClick() {
        jQuery('#info_sku_save_click').click(() => {

            jQuery('#info_sku_policy_v_order_mult, #info_sku_policy_v_order_min').removeClass('border-danger');
            jQuery('.multiple-batch').addClass('d-none');
            jQuery('.multiple-min').addClass('d-none');
            
            if(!this.verifyBatch()){
              return false
            }

            if (AbstractInfoSku.getEnableSaveButton && Object.keys(this.requestData).length !== 0) {
                jQuery('#info_sku_save_click').addClass('disabled');
                jQuery('#info_sku_save_click').html(`${i18nGlobals[myCountry.getCountry()].saving}...`);

                this.lazzyLoader('#policy_load_part_1', '#policy_data_part_1');
                this.lazzyLoader('#policy_load_part_2', '#policy_data_part_2');

                $.ajax({
                    url: `/${myCountry.getCountry()}/infosku/policy/update/${AbstractInfoSku.getSkuID}`,
                    type: 'POST',
                    datatype: 'json',
                    data: this.requestData,
                    success: (result) => {
                        this.lazzyLoader('#policy_load_part_1', '#policy_data_part_1', false);
                        this.lazzyLoader('#policy_load_part_2', '#policy_data_part_2', false);

                        if (result.hasOwnProperty('data') && result.data === false) {
                          jQuery('#info_sku_policy_v_order_min, #info_sku_policy_v_order_mult').addClass('border-danger')
                          jQuery('.multiple-min, .multiple-batch').removeClass('d-none').addClass('border-danger')
                        }

                        jQuery('#info_sku_save_click').html(`<i class="fa fa-save" aria-hidden="true"></i> ${i18nGlobals[myCountry.getCountry()].saveAlterations}`);
                        jQuery('.save-sku-success').removeClass('d-none');
                        AbstractInfoSku.setEnableSaveButton = false;

                        this.requestData = {};
                        setTimeout(function () {
                            jQuery('.save-sku-success').addClass('d-none');
                        }, 5000);
                        this.fetchPoliticsInformations();
                    }
                });
            }
        });
    }

    verifyBatch(){
      let verifyBatch = true

      if(Number(jQuery('#info_sku_policy_v_order_min').val())  <= 0 || jQuery('#info_sku_policy_v_order_min').val()  == '' )
      {
        jQuery('#info_sku_policy_v_order_min').addClass('border-danger')
        jQuery('.multiple-min').removeClass('d-none').addClass('border-danger')
        
        verifyBatch = false
      }
      
      if(Number(jQuery('#info_sku_policy_v_order_mult').val()) <= 0 || jQuery('#info_sku_policy_v_order_mult').val() == '')
      {
        jQuery('#info_sku_policy_v_order_mult').addClass('border-danger')
        jQuery('.multiple-batch').removeClass('d-none').addClass('border-danger')
        
        verifyBatch = false
      }

      return verifyBatch
    }

    lazzyLoader(id, id_data, openLoader = true) {
        if (openLoader) {
            jQuery(id_data).hide();
            jQuery(id).attr("style", "display:flex !important");
        }

        if (!openLoader) {
            jQuery(id).attr("style", "display:none !important");
            jQuery(id_data).show();
        }
    }

    totalPartial() {
        let total = 0;

        if (!jQuery('#info_sku_policy_lt_planner_check').is(':checked')) {
            total += this.validNaN(jQuery('#info_sku_policy_lt_planner_input input').val());
        }

        if (!jQuery('#info_sku_policy_lt_buyer_check').is(':checked')) {
            total += this.validNaN(jQuery('#info_sku_policy_lt_buyer_input input').val());
        }

        if (!jQuery('#info_sku_policy_lt_supplier_check').is(':checked')) {
            total += this.validNaN(jQuery('#info_sku_policy_lt_supplier_input input').val());
        }

        if (!jQuery('#info_sku_policy_lt_receiving_check').is(':checked')) {
            total += this.validNaN(jQuery('#info_sku_policy_lt_receiving_input input').val());
        }

        if (!jQuery('#info_sku_policy_lt_others_check').is(':checked')) {
            total += this.validNaN(jQuery('#info_sku_policy_lt_others_input input').val());
        }

        jQuery('#info_sku_total_dynamics').html(` ${total}`);
    }

    validNaN(value) {
        return isNaN(parseInt(value)) ? 0 : parseInt(value);
    }

    fetchPoliticsInformationsByTabClick() {
        const eachRow = jQuery('#information_tab');

        eachRow.on('click', () => {
            jQuery('#info_sku_policy_code').empty();
            jQuery('#sku_info_interdependence_policy_of_origin').html('');
            jQuery('#info_sku_unavailable_deposits').empty();

            this.lazzyLoader('#policy_load_part_1', '#policy_data_part_1');
            this.lazzyLoader('#policy_load_part_2', '#policy_data_part_2');

            this.fetchPoliticsInformations();
        });
    }

    fetchPoliticsBySelectChange() {
        const listenSelectChange = jQuery('#info_sku_policy_code');

        listenSelectChange.unbind('change').on('change', () => {
            const policyID = listenSelectChange.val();

            jQuery('#info_sku_policy_deposits_qty').empty();
            jQuery('#info_sku_unavailable_deposits').empty();

            this.lazzyLoader('#policy_load_part_1', '#policy_data_part_1');
            this.lazzyLoader('#policy_load_part_2', '#policy_data_part_2');
            this.fetchPoliticsInformationsByID(policyID);
        });
    }

    fetchPoliticsInformationsByID(policyID) {
        const url = `/${myCountry.getCountry()}/infosku/policy/${AbstractInfoSku.getSkuID}/id/${policyID}`;

        $.ajax({
            url: url,
            type: 'GET',
            datatype: 'json',
            success: (response) => {
                this.mountsPolicyInformationByID(response);
            },
        });
    }

    mountsPolicyInformationByID(response) {
        this.lazzyLoader('#policy_load_part_1', '#policy_data_part_1', false);
        this.lazzyLoader('#policy_load_part_2', '#policy_data_part_2', false);

        const { policyInfo, policyDeposits, unavailableDeposits, manualDataUpdate } = response.data;
        this.manualDataUpdate = manualDataUpdate;

        if (policyInfo != null) { 
            this.buildPlannerField(policyInfo);
            this.buildBuyerField(policyInfo);
            this.buildSupplierField(policyInfo);
            this.buildReceivingField(policyInfo);
            this.buildOthersField(policyInfo);
            this.totalLeadTimePolitic();
    
            this.buildDaysMinField(policyInfo);
            this.buildPoliticDesc(policyInfo.DESC_PROFILE);
            jQuery('#info_sku_policy_min_limit_bottom').html(policyInfo.MIN_LIMIT_BOTTOM);
            jQuery('#info_sku_policy_max_limit_bottom').html(policyInfo.MAX_LIMIT_BOTTOM);
            jQuery('#info_sku_policy_service_level').html(`${policyInfo.SERVICE_LEVEL}%`);
        }

        this.buildPolicyDepositsField(policyDeposits);
        this.buildUnavailableDepositsField(unavailableDeposits);
    }

    fetchPoliticsInformations() {
        $.ajax({
            url: `/${myCountry.getCountry()}/infosku/policy/${AbstractInfoSku.getSkuID}`,
            type: 'GET',
            datatype: 'json',
            success: (response) => {
                this.mountsPolicyInformation(response);
            },
        });
    }

    mountsPolicyInformation(response) {
        this.lazzyLoader('#policy_load_part_1', '#policy_data_part_1', false);
        this.lazzyLoader('#policy_load_part_2', '#policy_data_part_2', false);

        const {
            manualDataUpdate,
            policyCodes,
            policyDeposits,
            policyInfo,
            unavailableDeposits,
            consumerProfiles,
            defaultConsumerProfile,
            defaultPolicyCode
        } = response.data;

        this.manualDataUpdate = manualDataUpdate;

        this.buildPolicyCodeField(policyCodes, defaultPolicyCode);
        this.buildDaysMinField();

        jQuery('#info_sku_policy_min_limit_bottom').html(policyInfo.MIN_LIMIT_BOTTOM);
        jQuery('#info_sku_policy_service_level').html(`${policyInfo.SERVICE_LEVEL}%`);
        jQuery('#info_sku_policy_max_limit_bottom').html(policyInfo.MAX_LIMIT_BOTTOM);

        this.buildPlannerField(policyInfo);
        this.buildBuyerField(policyInfo);
        this.buildSupplierField(policyInfo);
        this.buildReceivingField(policyInfo);
        this.buildOthersField(policyInfo);
        this.buildMinLotField();
        this.buildMultLotField();
        this.buildMonthlyConsumptionField();
        this.buildExpirationDateField();
        this.buildPolicyDepositsField(policyDeposits);
        this.buildUnavailableDepositsField(unavailableDeposits);
        this.buildForecastType();
        this.buildPoliticDesc(policyInfo.DESC_PROFILE);
        this.buildConsumerProfile(defaultConsumerProfile, consumerProfiles);
        this.totalLeadTimePolitic();
        this.totalPartial();
    }

    totalLeadTimePolitic() {
        let cont = 0;
        jQuery('.policy_lt_span').each(function() {
            cont += parseInt($(this).html())
        });

        jQuery('#info_sku_policy_lt_total_days .policy_lt_total').html(cont);
    }

    buildPolicyCodeField(policyCodes, defaultPolicyCode) {
        jQuery('#info_sku_policy_code').empty();
        jQuery("#info_sku_policy_code").append(`<option value="-1">${i18nInfoSku[myCountry.getCountry()].matrix}</option>`);
        policyCodes.forEach((policyCode) => {
            let option = '';

            if (policyCode.ID_PROFILE_PK === defaultPolicyCode.ID_PROFILE_FK) {
                option = '<option value="'+ policyCode.ID_PROFILE_PK +'" selected>' + policyCode.DESC_PROFILE + '</option>';
            } else {
                option = '<option value="'+ policyCode.ID_PROFILE_PK +'">' + policyCode.DESC_PROFILE + '</option>';
            }

            jQuery("#info_sku_policy_code").append(option);
        });

        jQuery('#info_sku_policy_code').change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_code').val();
            this.requestData.v_id_profile = value;
        });
    }

    buildPoliticDesc(descProfile){        
        jQuery('#info_sku_policy_desc_profile').html(descProfile);
    }

    buildDaysMinField() {
        this.buildDaysMinFieldSpecialRule(
            this.manualDataUpdate.V_QTY_MIN_IN,
            this.manualDataUpdate.V_QTY_MIN_IN,
            '#info_sku_policy_days_min'
        );

        jQuery('#info_sku_policy_days_min_input input').on('keyup mouseup', () => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_days_min_input input').val();
            this.requestData.v_qty_min_in = value;
        });

        jQuery('#info_sku_policy_days_min_check').change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = '-1';
            this.requestData.v_qty_min_in = value;
        });
    }

    buildPlannerField(policyInfo) {
        
        if (policyInfo != null) {
            this.buildGenericField(
                policyInfo.LT_PLANNER,
                this.manualDataUpdate.V_LT_PLANNER_IN,
                '#info_sku_policy_lt_planner'
            );
        }        

        jQuery('#info_sku_policy_lt_planner_input input').on('keyup mouseup', () => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_lt_planner_input input').val();
            this.requestData.v_lt_planner_in = value;
            this.totalPartial();
        });

        jQuery('#info_sku_policy_lt_planner_check').change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;

            const value = jQuery('#info_sku_policy_lt_planner_check').is(':checked') ? '-1' : policyInfo.LT_PLANNER;
            this.requestData.v_lt_planner_in = value;
            this.totalPartial();
        });
    }

    buildBuyerField(policyInfo) {
        if (policyInfo != null) {        
            this.buildGenericField(
                policyInfo.LT_BUYER,
                this.manualDataUpdate.V_LT_PURCHASE_IN,
                '#info_sku_policy_lt_buyer'
            );
        }

        jQuery('#info_sku_policy_lt_buyer_input input').on('keyup mouseup', () => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_lt_buyer_input input').val();
            this.requestData.v_lt_purchase_in = value;
            this.totalPartial();
        });

        jQuery('#info_sku_policy_lt_buyer_check').change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = '-1';
            this.requestData.v_lt_purchase_in = value;
            this.totalPartial();
        });
    }

    buildSupplierField(policyInfo) {
        if (policyInfo != null) {        
            this.buildGenericField(
                policyInfo.LT_SUPPLIER,
                this.manualDataUpdate.V_LT_SUPPLIER_IN,
                '#info_sku_policy_lt_supplier'
            );
        }

        jQuery('#info_sku_policy_lt_supplier_input input').on('keyup mouseup', () => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_lt_supplier_input input').val();
            this.requestData.v_lt_supplier_in = value;
            this.totalPartial();
        });

        jQuery('#info_sku_policy_lt_supplier_check').change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = '-1';
            this.requestData.v_lt_supplier_in = value;
            this.totalPartial();
        });
    }

    buildReceivingField(policyInfo) {
        if (policyInfo != null) {
            this.buildGenericField(
                policyInfo.LT_RECEIVING,
                this.manualDataUpdate.V_LT_RECEIVING_IN,
                '#info_sku_policy_lt_receiving'
            );
        }

        jQuery('#info_sku_policy_lt_receiving_input input').on('keyup mouseup', () => {
            jQuery('#info_sku_save_click').removeClass('disabled');
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_lt_receiving_input input').val();
            this.requestData.v_lt_receiving_in = value;
            this.totalPartial();
        });

        jQuery('#info_sku_policy_lt_receiving_check').change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = '-1';
            this.requestData.v_lt_receiving_in = value;
            this.totalPartial();
        });
    }

    buildOthersField(policyInfo) {
        if (policyInfo != null) { 
            this.buildGenericField(
                policyInfo.LT_OTHERS,
                this.manualDataUpdate.V_LT_OTHERS_IN,
                '#info_sku_policy_lt_others'
            );
        }

        jQuery('#info_sku_policy_lt_others_input input').on('keyup mouseup', () => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_lt_others_input input').val();
            this.requestData.v_lt_others_in = value;
            this.totalPartial();
        });

        jQuery('#info_sku_policy_lt_others_check').change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = '-1';
            this.requestData.v_lt_others_in = value;
            this.totalPartial();
        });
    }

    buildMinLotField() {
        jQuery('#info_sku_policy_v_order_min').val(this.transformNegativeValue(this.manualDataUpdate.V_ORDER_MIN));
        jQuery('#info_sku_policy_v_order_min').on('keyup mouseup', () => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
             const value = jQuery('#info_sku_policy_v_order_min').val();
             this.requestData.v_order_min = (value == '' || value == 0 ) ? '1' : value;
        });
    }

    buildMultLotField() {
        jQuery('#info_sku_policy_v_order_mult').val(this.transformNegativeValue(this.manualDataUpdate.V_ORDER_MULT));
        jQuery('#info_sku_policy_v_order_mult').on('keyup mouseup', () => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_v_order_mult').val();
            this.requestData.v_order_mult = (value == '' || value == 0 ) ? '1' : value;;
        });
    }

    buildMonthlyConsumptionField() {
        this.buildConsumerField(
            this.manualDataUpdate.V_QTY_CONS,
            '#info_sku_policy_desc_cons'
        );

        jQuery('#info_sku_policy_desc_cons_input input').on('keyup mouseup', () => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_desc_cons_input input').val();
            this.requestData.v_qty_cons = value;
        });

        jQuery('#info_sku_policy_desc_cons_check').change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;

            const isChecked = jQuery('#info_sku_policy_desc_cons_check').is(':checked');
            let value = null;

            if (isChecked) {
                value = '-1';
                this.requestData.v_date_cons_expiration = 'null';
            } else {
                value = jQuery('#info_sku_policy_desc_cons_input input').val(0);
                jQuery('#info_sku_policy_expiration_date').val('');
            }

            this.requestData.v_qty_cons = value;
        });
    }

    buildExpirationDateField() {
        jQuery('#info_sku_policy_expiration_date').val(this.buildDate(this.manualDataUpdate.V_DATE_CONS_EXPIRATION));

        this.hideExpirationDate(this.manualDataUpdate.V_QTY_CONS);

        jQuery('#info_sku_policy_expiration_date').keyup(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_expiration_date').val();
            this.requestData.v_date_cons_expiration = value;
        });

        jQuery('#info_sku_policy_expiration_date').blur(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_policy_expiration_date').val() || '';
            this.requestData.v_date_cons_expiration = value;
        });
    }

    buildPolicyDepositsField(policyDeposits) {
        jQuery('#info_sku_policy_deposits').empty();
        policyDeposits.items.forEach((policyDeposit) => {
            jQuery('#info_sku_policy_deposits').append('<option>' + policyDeposit.AVAILABLE_DEPOSITS_DESC + '</option>');
        });

        jQuery('#info_sku_policy_deposits_qty').html(policyDeposits.qty);
    }

    buildUnavailableDepositsField(unavailableDeposits) {
        jQuery('#info_sku_unavailable_deposits').empty();
        unavailableDeposits.items.forEach((unavailableDeposit) => {
            jQuery('#info_sku_unavailable_deposits').append('<option>' + unavailableDeposit.UNAVAILABLE_DEPOSIT_DESC + '</option>');
        });

        jQuery('#info_sku_unavailable_deposits_qty').html(unavailableDeposits.qty);
    }

    buildDaysMinFieldSpecialRule(info, type, id) {
        const input = id + '_input';
        const check = id + '_check';
        const currentType = parseInt(type, 10);
        const currentInfo = parseInt(info, 10);
        const base = jQuery(input).data('info');

        jQuery(`${input} input`).val(this.transformNegativeValue(currentInfo));
        jQuery(`#${base} span`).html(this.transformNegativeValue(currentInfo));

        const ZERO = 0;
        if (currentType < ZERO) {
            jQuery(input).hide();
            jQuery(`#${base}`).show();
            jQuery(check).prop('checked', true);
        } else {
            jQuery(input).show();
            jQuery(`#${base}`).hide();
        }

        let show = jQuery(input).is(':hidden');

        const eachRow = jQuery(check);
        eachRow.on('click', () => {
            if (show) {
                jQuery(input).show();
                jQuery(`#${base}`).hide();
            } else {
                jQuery(input).hide();
                jQuery(`#${base}`).show();
            }

            show = !show;
        });
    }

    buildConsumerField(type, id) {
        const input = id + '_input';
        const check = id + '_check';
        const currentType = parseInt(type, 10);
        const currentInfo = parseInt(type, 10);

        jQuery(`${input} input`).val(this.transformNegativeValue(currentInfo));

        const ZERO = 0;
        if (currentType < ZERO) {
            jQuery(input).hide();
            jQuery(check).prop('checked', true);
        } else {
            jQuery(input).show();
            jQuery(check).prop('checked', false);
        }

        const eachRow = jQuery(check);
        let show = jQuery(input).is(':hidden');

        eachRow.on('click', () => {
            if (show) {
                jQuery(input).show();
            } else {
                jQuery(input).hide();
            }

            show = !show;
        });
    }

    hideExpirationDate(type) {
        const id = '#info_sku_policy_desc_cons_check';
        const expirationDate = '#expiration_date';
        const eachRow = jQuery(id);
        let show = true;

        const ZERO = 0;
        if (type < ZERO) {
            show = false;
            jQuery(expirationDate).hide();
        } else {
            show = true;
            jQuery(expirationDate).show();
        }

        eachRow.on('click', () => {
            if (show) {
                jQuery(expirationDate).hide();
            } else {
                jQuery(expirationDate).show();
            }

            show = !show;
        });
    }

    buildDate(date) {
        if (date === null) {
            return '';
        }

        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    buildForecastType() {
        const forecastType = parseInt(this.manualDataUpdate.V_TYPE_OUT_FORECAST, 10);
        jQuery('#info_sku_forecast_type').val(forecastType);

        jQuery('#info_sku_forecast_type').on('change', () => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');
            AbstractInfoSku.setEnableSaveButton = true;

            this.requestData.V_TYPE_OUT_FORECAST = jQuery('#info_sku_forecast_type').val();
        });
    }

    buildConsumerProfile(defaultConsumerProfile, consumerProfiles) {
        jQuery('#info_sku_consumer_profile').empty();
        consumerProfiles.forEach((consumerProfile) => {
            let option = '';

            if (consumerProfile.ID_CONS_PK === defaultConsumerProfile) {
                option = '<option value="'+ consumerProfile.ID_CONS_PK +'" selected>' + consumerProfile.NAME_CONS + '</option>';
            } else {
                option = '<option value="'+ consumerProfile.ID_CONS_PK +'">' + consumerProfile.NAME_CONS + '</option>';
            }

            jQuery("#info_sku_consumer_profile").append(option);
        });

        jQuery('#info_sku_consumer_profile').change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');;
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#info_sku_consumer_profile').val();
            this.requestData.V_TYPE_OUT_FORECAST = value;
        });
    }

    buildGenericField(info, type, id) {        
        const div = id + '_div';
        const input = id + '_input';
        const check = id + '_check';
        const currentType = parseInt(type, 10);
        const currentInfo = parseInt(info, 10);

        jQuery(div).find('span').html(this.transformNegativeValue(currentInfo));
        jQuery(`${input} input`).val(this.transformNegativeValue(currentType));

        const ZERO = 0;
        if (currentType < ZERO) {
            jQuery(div).removeClass('opacity-hide');
            jQuery(input).hide();
            jQuery(check).prop('checked', true);
        } else {
            jQuery(div).addClass('opacity-hide');
            jQuery(input).show();
        }

        const eachRow = jQuery(check);
        let show = jQuery(div).hasClass('opacity-hide');
        eachRow.on('click', () => {
            if (show) {
                jQuery(div).removeClass('opacity-hide');
                jQuery(input).hide();
            } else {
                jQuery(div).addClass('opacity-hide');
                jQuery(input).show();
            }

            show = !show;
        });
    }

    transformNegativeValue(val){
        return (val < 0 ? 0 : val)
    }
}
