class infoSkuController extends AbstractInfoSku {
    constructor() {
        super();
        this.locale = jQuery('#sysLocale').val();
        moment.locale(this.locale);
        this.historyMovtoErp = jQuery("#history_movto_erp");
        this.dashboardTab = jQuery("#dashboard_tab");
        this.othersTab = jQuery("#erp_tab, #information_tab, #log_tab");
        //this.checkOtherTabs = false;
        this.arrRoutesToCall = ['balancing', 'shoppingforecastdaily', 'stockposition_weekly'],
        this.promisse = $.when();
        this.btnStatusSystem = jQuery("#info_btn_status");
        this.today = new Date();
        this.textOptionsChart =  {
            color: '#333333',
            fontSize: 12,
            fontName: 'Lato, sans-serif',
            bold: false,
            italic: false,
        },

        jQuery('#forecastStartDate').val(moment(this.today).format('YYYY-MM-DD'));
        jQuery('#forecastEndDate').val(moment(this.today).add(3, 'M').format('YYYY-MM-DD'));
        this.startDateShoppingForecast = moment(this.today).subtract(1, 'd').format('YYYY-MM-DD');
        this.endDateShoppingForecast = moment(this.today).add(3,'M').format('YYYY-MM-DD');

        this.shoppingForecastDaily = jQuery("#shoppingforecast_day");
        this.shoppingForecastMonth = jQuery("#shoppingforecast_month");

        this.historyDaily = jQuery("#history_day");
        this.historyConsumption = jQuery("#history_consumption");

        this.stockPositionQuarter = jQuery("#stockpos_quarter");
        this.stockPositionYear = jQuery("#stockpos_year");

        jQuery('#historyStartDate').val(moment(this.today).subtract(180, 'd').format('YYYY-MM-DD'));
        jQuery('#historyEndDate').val(moment(this.today).format('YYYY-MM-DD'));
        this.startDateHistory = moment(this.today).subtract(180,'d').format('YYYY-MM-DD');
        this.endDateHistory = moment(this.today).subtract(1,'d').format('YYYY-MM-DD');

        this.modal = jQuery('#modal_info');
        this.init = false;
        this.sku_id;
        this.sku_id_temp;
        this.out6;
        this.leadTime;
        this.jsonStockBalance;
        this.jsonShoppingForecast;
        this.jsonShoppingForecastMonth;
        this.jsonHistory;
        this.jsonHistoryConsumption;
        this.jsonStockPos;
        this.jsonStockPosWeekly;
        this.chart;
        this.days_horizon;
        this.tooltipTotalStock;
        this.listenerBtns();
        google.charts.load('current', {'packages':['corechart']});

        google.charts.load("visualization", "1", {
            packages: ["corechart"]
        });
    }

    initialize(){
        if(this.sku_id){
            this.arrRoutesToCall.reduce((promisse, url) => promisse.then(() => this.genericAjax(url)), Promise.resolve());
            this.sku_id_temp = this.sku_id;
        }
    }

    //set color for the chart column
    setColorStockBalance(totalValue, lowValue, highValue){
        let color='';
        if(totalValue <= lowValue){
            color = "#E63939";
        }else if(totalValue > lowValue && totalValue <= highValue){
            color = "#91D962";
        }else{
            color = "#FFB340";
        }
        return color;
    }

    setCurrency(value){
        return value.toLocaleString(myCountry.getLanguage(), {minimumFractionDigits: 2,style: 'currency',currency: myCountry.getCurrency()});
    }

    setCurrencyCode(value){
        return myCountry.getCurrency().concat(" ",value);
    }

    //set card EXCESS
    setExcess(){
        jQuery("#excess_total").html(this.unitFormatter(this.jsonStockBalance.TOTAL_EXCESS || 0));
        jQuery("#excess_total_val").html(this.setCurrency(this.jsonStockBalance.TOTAL_EXCESS_VALUE || 0));

        jQuery("#excess_available").html(this.unitFormatter(this.jsonStockBalance.AVAILABLE_EXCESS || 0));
        jQuery("#excess_available_value").html(this.setCurrency(this.jsonStockBalance.AVAILABLE_EXCESS_VALUE || 0));
        jQuery("#ideal_prediction").html(this.unitFormatter(this.jsonStockBalance.IDEAL_PREDICTION || 0));

    }

    //Novo Gráfico stockBalanceChart2.0
    stockBalanceChart(){
        var dataTable = new google.visualization.DataTable();
            dataTable.addColumn('string');
            dataTable.addColumn('number',i18nGlobals[myCountry.getCountry()].minimum);
            dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
            dataTable.addColumn({'type': 'string', 'role': 'style'});
            dataTable.addColumn({type: 'string', role: 'annotation'});
            dataTable.addColumn('number', i18nGlobals[myCountry.getCountry()].maximum);
            dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
            dataTable.addColumn({'type': 'string', 'role': 'style'});
            dataTable.addColumn({type: 'string', role: 'annotation'});
            dataTable.addColumn('number');
            dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
            dataTable.addColumn({'type': 'string', 'role': 'style'});

        dataTable.addRows([
            [   i18nGlobals[myCountry.getCountry()].available,

                this.jsonStockBalance.LOW, //valor minimo
                this.prepareTooltipForStockBalanceMinMax(i18nGlobals[myCountry.getCountry()].total, i18nInfoSku[myCountry.getCountry()].column.minimum, this.jsonStockBalance.LOW),
                '#B31212', //cor da linha do valor minimo
                '', //nome que aparece no valor minimo
                this.jsonStockBalance.HIGH, //valor maximo
                this.prepareTooltipForStockBalanceMinMax(i18nGlobals[myCountry.getCountry()].total, i18nInfoSku[myCountry.getCountry()].column.maximum, this.jsonStockBalance.HIGH),
                '#C77700', //cor da linha do valor maximo
                '', //nome que aparece no valor maximo
                this.jsonStockBalance.AVAILABLE, //valor da barra de estoque disponivel
                this.prepareTooltipForstockBalanceChart(this.jsonStockBalance.AVAILABLE, this.setCurrency(this.jsonStockBalance.AVAILABLE_VALUE), this.jsonStockBalance.AVAILABLE_DAYS, i18nGlobals[myCountry.getCountry()].available.toUpperCase()),
                this.setColorStockBalance(this.jsonStockBalance.AVAILABLE_VALUE, this.jsonStockBalance.LOW_VALUE, this.jsonStockBalance.HIGH_VALUE), //cor da barra de estoque disponivel
            ],
            [   i18nGlobals[myCountry.getCountry()].total,

                this.jsonStockBalance.LOW, //valor minimo
                this.prepareTooltipForStockBalanceMinMax(i18nGlobals[myCountry.getCountry()].total, i18nInfoSku[myCountry.getCountry()].column.minimum, this.jsonStockBalance.LOW),
                '#B31212', //cor da linha do valor minimo
                i18nGlobals[myCountry.getCountry()].min, //nome que aparece no valor minimo
                this.jsonStockBalance.HIGH, //valor maximo
                this.prepareTooltipForStockBalanceMinMax(i18nGlobals[myCountry.getCountry()].total, i18nInfoSku[myCountry.getCountry()].column.maximum, this.jsonStockBalance.HIGH),
                '#C77700', //cor da linha do valor maximo
                i18nGlobals[myCountry.getCountry()].max, //nome que aparece no valor maximo
                this.jsonStockBalance.TOTAL, //valor da barra de estoque total
                this.prepareTooltipForstockBalanceChart(this.jsonStockBalance.TOTAL, this.setCurrency(this.jsonStockBalance.TOTAL_VALUE), this.jsonStockBalance.TOTAL_DAYS, i18nGlobals[myCountry.getCountry()].total.toUpperCase()),
                this.setColorStockBalance(this.jsonStockBalance.TOTAL_VALUE, this.jsonStockBalance.LOW_VALUE, this.jsonStockBalance.HIGH_VALUE),
            ],
        ])

        let options = {
            tooltip: {isHtml: true},
            chartArea: {
                width: '100%',
                height: '100%',
                left: 70,
                top: 10,
                bottom: 20,
            },
            bar: {groupWidth: "40%"},
            annotations: {
                alwaysOutside: true,
                textStyle: {...this.textOptionsChart, bold: true},
            },
            vAxis: {
                title: `${i18nGlobals[myCountry.getCountry()].quantity} (${i18nGlobals[myCountry.getCountry()].units})`,
                titleTextStyle: this.textOptionsChart,
                textStyle: {...this.textOptionsChart, color: '#888888'},
                format: 'short',
            },
            seriesType: 'bars',
            legend: { position: "none" },
            series: {
                0: {
                    type: 'line',
                    annotations: {
                        stemColor : 'none',
                        textStyle: {...this.textOptionsChart, bold: true, color: '#000000', auraColor: 'none',},
                    },
                },
                1: {
                    type: 'line',
                    annotations: {
                        stemColor : 'none',
                        textStyle: {...this.textOptionsChart, bold: true, color: '#000000', auraColor: 'none',},
                    },
                },
            },
        };

        this.chart = new google.visualization.ColumnChart(document.getElementById("stockBalance"));
        this.chart.draw(dataTable, options);
        this.stockBalanceTopInfo();
        this.skusStockValueInfo();

    }
    //Fim Gráfico stockBalanceChart2.0
    skusStockValueInfo(){
        jQuery('#sku-stockValue-disp-col, #sku-stockValue-total-col').html();

        const colorClassAvailable = this.setColorTextStockBalance(this.jsonStockBalance.AVAILABLE_VALUE, this.jsonStockBalance.LOW_VALUE, this.jsonStockBalance.HIGH_VALUE),
              colorClassTotal = this.setColorTextStockBalance(this.jsonStockBalance.TOTAL_VALUE, this.jsonStockBalance.LOW_VALUE, this.jsonStockBalance.HIGH_VALUE);

		jQuery('#sku-stockValue-disp-col')
        .html( `<div class="font-weight-bold-700 ${colorClassAvailable}">
                    ${this.unitFormatter(this.jsonStockBalance.AVAILABLE)} ${i18nGlobals[myCountry.getCountry()].unityAbbr}
                </div>
                <div class="pln-modal-text-small text-dark">
                    ${this.setCurrency(this.jsonStockBalance.AVAILABLE_VALUE)}
                </div>
				<div class="pln-text-label-small">${this.unitFormatter(this.jsonStockBalance.AVAILABLE_DAYS)} ${i18nInfoSku[myCountry.getCountry()].daysOfCoverage}</div>
				<div class="pln-text-label-small">${i18nInfoSku[myCountry.getCountry()].spin}: ${this.jsonStockBalance.AVAILABLE_TURNOVER}</div>`);

		jQuery('#sku-stockValue-total-col')
		.html(  `<div class="font-weight-bold-700 ${colorClassTotal}">
                    ${this.unitFormatter(this.jsonStockBalance.TOTAL)} ${i18nGlobals[myCountry.getCountry()].unityAbbr}
                </div>
                <div class="pln-modal-text-small text-dark">
					${this.setCurrency(this.jsonStockBalance.TOTAL_VALUE)}
                </div>
				<div class="pln-text-label-small">${this.unitFormatter(this.jsonStockBalance.TOTAL_DAYS)} ${i18nInfoSku[myCountry.getCountry()].daysOfCoverage}</div>
				<div class="pln-text-label-small">${i18nInfoSku[myCountry.getCountry()].spin}: ${this.jsonStockBalance.TOTAL_TURNOVER}</div>`)
    }

    stockBalanceTopInfo(){
        jQuery(`#stock-balance-minimum-unit,
                #stock-balance-maximum-unit,
                .stock-balance-minimum-cover span,
                .stock-balance-maximum-cover span`)
                .html();

        jQuery("#stock-balance-minimum-unit").html(`${this.unitFormatter(this.jsonStockBalance.LOW)} ${i18nGlobals[myCountry.getCountry()].units}`);
        jQuery("#stock-balance-maximum-unit").html(`${this.unitFormatter(this.jsonStockBalance.HIGH)} ${i18nGlobals[myCountry.getCountry()].units}`);
        jQuery(".stock-balance-minimum-cover span").html(this.unitFormatter(this.jsonStockBalanceCover.minimum.COBERTURA));
        jQuery(".stock-balance-maximum-cover span").html(this.unitFormatter(this.jsonStockBalanceCover.maximum.COBERTURA));
    }

    prepareTooltipForStockBalanceMinMax(title, label, value){
		return `<div class="p-2 pln-tooltip-chart" style="min-width:150px">
					<div class="d-flex flex-column">
						<span class="pln-tooltip-chart-bold text-uppercase">${title}</span>
						<span>${label}</span>
						<span>${this.unitFormatter(value)} ${i18nGlobals[myCountry.getCountry()].units}</span>
					</div>
				</div>`;
	}

    setColorTextStockBalance(totalValue, lowValue, highValue){
        let colorClass="pln-text-max-color";
        if(totalValue <= lowValue){
            colorClass = "pln-text-min-color";
        }else if(totalValue > lowValue && totalValue <= highValue){
            colorClass = "pln-text-ideal-color";
        }
        return colorClass;
    }

    prepareTooltipForstockBalanceChart(unit, value, days, title){
        return `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
                    <div class="d-flex flex-column">
                        <span class="font-weight-bold-700">${title}</span>
                        <span>${this.unitFormatter(unit)} ${i18nGlobals[myCountry.getCountry()].units}</span>
                        <span>${value}</span>
                        <span>${this.unitFormatter(days)} ${i18nInfoSku[myCountry.getCountry()].daysOfCoverage}</span>
                    </div>
                </div>`;
    }

    /**
     * Método que coloca pontos a cada três casas em números de unidade.
     * @param {int} value valor a ser convertido
     * @return {Number} valor convertido
     */
    setCurrencyAndConvert(value) {
        value =  Math.abs(Number(value))
        return Intl.NumberFormat(this.locale, {minimumFractionDigits:2, maximumFractionDigits:2, style: 'decimal'}).format(value)
    }

    shoppingForecastChartDailyGeneritcTooltip(date, title, value, monetaryValue='' ) {
        return `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
            <div class="d-flex flex-column">
                <span class="pln-tooltip-chart-bold d-flex justify-content-between">
                    ${date}
                </span>
                <hr class="pln-tooltip-chart-hr" >
                <span>
                    <span class="pln-tooltip-chart-bold">${title}: </span>
                    ${this.numberFormatter(value)} ${this.verifyMonetaryValue(monetaryValue)}
                </span>
            </div>
        </div>`;
    }

    shoppingForecastChartDailyDetailsTooltip(date, title, value, monetaryValue, id){
        return `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
            <div class="d-flex flex-column">
                <span class="pln-tooltip-chart-bold d-flex justify-content-between">
                    ${date}
                </span>
                <hr class="pln-tooltip-chart-hr" >
                <span>
                    <span class="pln-tooltip-chart-bold">${title}: </span>
                    ${this.unitFormatter(value)} ${this.verifyMonetaryValue(monetaryValue)} ${this.tooltipCheckId(id)}
                </span>
            </div>
        </div>`;
    }

    verifyMonetaryValue(value){
        let monetaryValue = ''
        if(Number(value) > 0){
            monetaryValue = `(${this.setCurrency(value)})`
        }
        return monetaryValue
    }

    tooltipCheckId(displayId){
        return !displayId ? '' : `(Nº ${displayId})`;
    }

    dateFormat(date) {
        date = date.split('/');
        return date[2]+'-'+date[1]+'-'+date[0];
    }

    shoppingForecastChartDaily() {

        let dataTable = new google.visualization.DataTable()
        const result = this.jsonShoppingForecast.chartData
        dataTable.addColumn('string', i18nInfoSku[myCountry.getCountry()].column.month);
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.consumptionForecast);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.othersConsumptions);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.suggestion);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.solicitations);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.orders);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.stockProjections);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

        let rowWithAddedData = [];

        
        jQuery('#forecastStartDate').val(this.dateFormat(result[0].PERIODO));
        jQuery('#forecastEndDate').val(this.dateFormat(result[result.length - 1].PERIODO));

        result.forEach(element => {
            rowWithAddedData.push( [
                element.PERIODO,
                Number(element.PREVISAO_DE_CONSUMO),
                this.shoppingForecastChartDailyGeneritcTooltip(element.PERIODO, i18nInfoSku[myCountry.getCountry()].column.consumptionForecast, element.PREVISAO_DE_CONSUMO),
                Number(element.OUTRAS_SAIDAS),
                this.shoppingForecastChartDailyGeneritcTooltip(element.PERIODO, i18nInfoSku[myCountry.getCountry()].column.othersConsumptions, element.PREVISAO_DE_CONSUMO),
                Number(element.PARCELAS),
                this.shoppingForecastChartDailyDetailsTooltip(element.PERIODO, i18nInfoSku[myCountry.getCountry()].column.suggestion, parseInt(element.PARCELAS), element.INSTALLMENTS_AMOUNT, '' ),
                Number(element.SOLICITACOES),
                this.shoppingForecastChartDailyDetailsTooltip(element.PERIODO, i18nInfoSku[myCountry.getCountry()].column.solicitations, parseInt(element.SOLICITACOES), element.SOLICITACAO_AMOUNT, element.ID_SOLICITACAO),
                Number(element.ORDENS),
                this.shoppingForecastChartDailyDetailsTooltip(element.PERIODO, i18nInfoSku[myCountry.getCountry()].column.orders, parseInt(element.ORDENS), element.ORDENS_AMOUNT, element.ID_ORDEM),
                Number(element.PREVISAO_ESTOQUE_DISPONIVEL),
                this.prepareTooltipForShoppingForecast(element),
            ]);
        });

        dataTable.addRows(rowWithAddedData);

        let options = {
            tooltip: {
                isHtml: true,
            },
            height: '400',
            chartArea: {
                width: '85%',
                left: 120,
                right: 50,
            },
            selectionMode: "multiple",
            legend: {
                position: 'top',
                alignment: 'center',
                textStyle: this.textOptionsChart,
            },
            isStacked:true,
            colors:['#f32938', '#990000', '#30a6ae', '#339900', '#9c0', '#292c39'],
            vAxis: {
                title: i18nInfoSku[myCountry.getCountry()].quantityUnity,
                ticks:
                    [
                        {v: this.jsonShoppingForecast.targetsData[0].BAIXO, f: `${i18nGlobals[myCountry.getCountry()].min.toLowerCase()} ${this.numberFormatter(this.jsonShoppingForecast.targetsData[0].BAIXO)} \n (${this.jsonShoppingForecast.targetsData[0].COVER_MIN} ${i18nGlobals[myCountry.getCountry()].days.toLowerCase()})`},
                        {v: this.jsonShoppingForecast.targetsData[0].ALTO, f: `${i18nGlobals[myCountry.getCountry()].max.toLowerCase()} ${this.numberFormatter(this.jsonShoppingForecast.targetsData[0].ALTO)} \n (${this.jsonShoppingForecast.targetsData[0].COVER_MAX} ${i18nGlobals[myCountry.getCountry()].days.toLowerCase()})`}
                    ],
                    titleTextStyle: this.textOptionsChart,
                    textStyle: {...this.textOptionsChart, color: '#888888'},
            },
            hAxis: {
                title: 'Dia',
                titleTextStyle: this.textOptionsChart,
                textStyle: this.textOptionsChart,
                slantedText: true,
                slantedTextAngle: 45,
                ticks: [5,10,15,20],
            },
            seriesType: 'bars',
            series: {5: {type: 'line'}}
        };

        const chart = new google.visualization.ComboChart(document.getElementById('previsao_compras_dia'));
        chart.draw(dataTable, options);
        this.setLeadTimeOfDaily();
    }

    setLeadTimeOfDaily(){
        jQuery(".lead-time").show();
        jQuery("#lead-time-value").html(this.leadTime);
    }

    //create html for tooltip Shopping Forecast chart
    prepareTooltipForShoppingForecast(obj){
        return `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
            <div class="d-flex flex-column">
                <span class="pln-tooltip-chart-bold d-flex justify-content-between">
                    ${obj.PERIODO}
                </span>
                <hr class="pln-tooltip-chart-hr" >
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.available}:</span>
                    ${this.numberFormatter(obj.PREVISAO_ESTOQUE_DISPONIVEL)}
                </span>
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.coverage}:</span>
                    ${this.numberFormatter(Math.floor(obj.QTY_FORECAST))} ${i18nGlobals[myCountry.getCountry()].days}
                </span>
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.totalStock}:</span>
                    ${this.numberFormatter(obj.ESTOQUE_TOTAL)}
                </span>
                <hr class="pln-tooltip-chart-hr" >
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.ins.toUpperCase()}</span>
                </span>
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.suggestions}:</span>
                    ${this.unitFormatter(obj.PARCELAS)} ${this.verifyMonetaryValue(obj.INSTALLMENTS_AMOUNT)}
                </span>
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.solicitations}:</span>
                    ${this.unitFormatter(obj.SOLICITACOES)} ${this.verifyMonetaryValue(obj.SOLICITACAO_AMOUNT)} ${this.tooltipCheckId(obj.ID_SOLICITACAO)}
                </span>
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.orders}:</span>
                    ${this.unitFormatter(obj.ORDENS)} ${this.verifyMonetaryValue(obj.ORDENS_AMOUNT)} ${this.tooltipCheckId(obj.ID_ORDEM)}
                </span>
                <hr class="pln-tooltip-chart-hr" >
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.outs.toUpperCase()}</span>
                </span>
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.forecasts}:</span>
                    ${this.numberFormatter(obj.PREVISAO_DE_CONSUMO)}
                </span>
                <span>
                    <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.others}:</span>
                    ${this.numberFormatter(obj.OUTRAS_SAIDAS)}
                </span>
            </div>
        </div>`;
    }



    shoppingForecastChartMonth() {

        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('string', i18nInfoSku[myCountry.getCountry()].column.month);
        dataTable.addColumn('number', this.prepareLabelForShoppingForecastMonth(this.jsonShoppingForecastMonth.labels[0]));
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', this.prepareLabelForShoppingForecastMonth(this.jsonShoppingForecastMonth.labels[1]));
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', this.prepareLabelForShoppingForecastMonth(this.jsonShoppingForecastMonth.labels[2]));
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.manualForecast);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', this.methodName(this.jsonShoppingForecastMonth.method_name));
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

        var arr = [], temp;
        if (this.jsonShoppingForecastMonth.chartData) {
            this.jsonShoppingForecastMonth.chartData.forEach(element => {
                temp = jQuery.extend({}, element)
                element[0] = moment().month(element[0]).format('MMMM');

                element.splice(2, 0, this.prepareTooltipForShoppingForecastMonth(element[1]));

                element.splice(4, 0, this.prepareTooltipForShoppingForecastMonth(element[3]));

                element.splice(6, 0, this.prepareTooltipForShoppingForecastMonth(element[5]));

                element.splice(8, 0, this.prepareTooltipForShoppingForecastMonth(element[7]));

                arr.push([...element, this.prepareTooltipForShoppingForecastMonth(temp, 'line')])
            });
        }

        dataTable.addRows(arr);

        let options = {
            tooltip: {isHtml: true},
            height: '350',
            chartArea: {
                width: "90%"
            },
            selectionMode: "multiple",
            legend: {
                position: 'top',
                alignment: 'center',
                textStyle: this.textOptionsChart,
            },
            isStacked:false,
            colors:['#B5E2FA', '#30A5BF', '#0F4C5C', '#E68A00', '#E1410C'],
            vAxis: {
                format: 'short',
                title: i18nInfoSku[myCountry.getCountry()].quantityUnity,
                titleTextStyle: this.textOptionsChart,
                textStyle: {...this.textOptionsChart, color: '#888888'},
            },
            hAxis: {
                textStyle: this.textOptionsChart,
            },
            curveType: 'none',
            seriesType: 'bars',
            series: {3: {lineDashStyle: [10, 2], type: 'line'}, 4: {type: 'line'}}
        };

        const chart = new google.visualization.ComboChart(document.getElementById('previsao_compras_mes'));
        chart.draw(dataTable, options);
        jQuery(".previsaoCompras-spin").attr("style", "display:none !important");
        jQuery("#shoppingforecast_day, #shoppingforecast_month").css("pointer-events", "auto");

    }

    methodName(name) {
        if(name === undefined || name === null) {
            return i18nInfoSku[myCountry.getCountry()].automaticForecastNotFoundMethod;
        }

        return i18nInfoSku[myCountry.getCountry()].automaticForecastMethod + ' ' + name;
    }


    prepareTooltipForShoppingForecastMonth(obj, type){
        if(type === 'line'){
            return `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
                        <div class="d-flex flex-column">
                            <span>${moment().month(obj[0]).format('MMMM').toUpperCase()}</span>

                            <span class="ml-1">${i18nInfoSku[myCountry.getCountry()].automaticForecast}</span>
                            <div class="ml-3 pr-1 d-flex flex-row align-items-center">
                                <div style="width:10px; height:10px; background-color:#F32938"></div>

                                <div class="ml-1">${this.prepareLabelForShoppingForecastMonth(this.jsonShoppingForecastMonth.labels[3])}: ${this.numberFormatter(obj[5])}</div>
                            </div>

                            <span class="ml-1">Previsto (Manual)</span>
                            <div class="ml-3 pr-1 d-flex flex-row align-items-center">
                                <div style="width:10px; height:10px; background-color:#E68A00"></div>

                                <div class="ml-1">${obj[4]}</div>
                            </div>

                            <div class="mt-2 mb-2" style="border:1px dashed white"></div>

                            <div class="ml-3 d-flex flex-row align-items-center">
                                <div style="width:10px; height:10px; background-color:#B5E2FA"></div>
                                <div class="ml-1">${this.prepareLabelForShoppingForecastMonth(this.jsonShoppingForecastMonth.labels[0])}: ${this.numberFormatter(obj[1])}</div>
                            </div>
                            <div class="ml-3 d-flex flex-row align-items-center">
                                <div style="width:10px; height:10px; background-color:#30A5BF"></div>
                                <div class="ml-1">${this.prepareLabelForShoppingForecastMonth(this.jsonShoppingForecastMonth.labels[1])}: ${this.numberFormatter(obj[2])}</div>
                            </div>
                            <div class="ml-3 d-flex flex-row align-items-center">
                                <div style="width:10px; height:10px; background-color:#0F4C5C"></div>
                                <div class="ml-1">${this.prepareLabelForShoppingForecastMonth(this.jsonShoppingForecastMonth.labels[2])}: ${this.numberFormatter(obj[3])}</div>
                            </div>
                        </div>
                    </div>`;
        }else{
            return `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
                    <div class="d-flex flex-column">
                        <span>${i18nGlobals[myCountry.getCountry()].quantity}:</span>
                        <span>${this.numberFormatter(obj)}</span>
                    </div>
                </div>`;
        }
    }

    prepareLabelForShoppingForecastMonth(obj){
        return `${obj.from[1]}/${obj.to[1]}`;
    }

    HistoryChart() {

        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('string', 'Mes');
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.in);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.out);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.minimumStock);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.maximumStock);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.availableStock);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.totalStock);
        dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

        var arr = [];

        if(this.jsonHistory.dataChart){
            this.jsonHistory.dataChart.forEach((element, key) => {

              element.splice(2, 0, this.tooltipTotalStock[element[0]].inputs);
              element.splice(4, 0, this.tooltipTotalStock[element[0]].outputs);
              element.splice(6, 0, this.tooltipTotalStock[element[0]].minimun);
              element.splice(8, 0, this.tooltipTotalStock[element[0]].maximun);
              element.splice(10, 0, this.tooltipTotalStock[element[0]].available);

                arr.push([...element, this.prepareTooltipForHistory(this.jsonHistory, key)])
            });
        }

        dataTable.addRows(arr);

        var options = {
            //focusTarget: 'category',
            tooltip: {isHtml: true},
            height: '350',
            chartArea: {
                width: '85%',
                left: 120,
            },
            selectionMode: "multiple",
            legend: {
                position: 'top',
                alignment: 'center',
                textStyle: this.textOptionsChart,
            },
            isStacked:false,
            colors:['#62A336', '#E61717', '#FF9C9C', '#FFBCA6', '#333333', '#979797'],
            vAxis: {
                format: 'short',
                title: i18nInfoSku[myCountry.getCountry()].quantityUnity,
                titleTextStyle: this.textOptionsChart,
                textStyle: {...this.textOptionsChart, color: '#888888'},
            },
            hAxis: {
                ticks: [5,10,15,20],
                textStyle: this.textOptionsChart,
            },
            seriesType: 'bars',
            series: {
                2: {type: 'line'},
                3: {type: 'line'},
                4: {type: 'line'},
                5: {type: 'line', lineDashStyle: [4, 4]}
            }
        };

        var chart = new google.visualization.ComboChart(document.getElementById('historico'));
        chart.draw(dataTable, options);
    }

    returnZeroIfUndefined(value) {
        return (typeof(value) == 'undefined') ? 0 : value;
    }

    constroiParteVariavelDaTooltip(obj, column, className, title, tootlipDate) {
        let  result = '';
        obj.forEach(item => {
            let informationDateFromJson = (column == 'DATE_CREATED') ? new Date(item.DATE_CREATED) : new Date(item.DATE_APPROVED),
                tooltipDate = new Date(tootlipDate);

            if(informationDateFromJson.getTime() <=tooltipDate.getTime()) {
                result += `<span>
                <span class="pln-tooltip-chart-bold pln-tooltip-chart-${className}">${title} ${item.NUM_PARC_PK}:</span>
                    ${item.QTY_ALTERED} (para ${(column == 'DATE_CREATED') ?  item.DATE_CREATED : item.DATE_APPROVED})
                </span>`
            }
        });

        return result
    }


    //create html for tooltip Shopping Forecast chart
    prepareTooltipForHistory(obj, key){
        let date = obj.dataChart[key][0],
            openOrder = this.returnZeroIfUndefined(obj.dataOpenByDay[date]),
            openRequest = this.returnZeroIfUndefined(obj.dataApprovedByDay[date]),
            invLevel =  this.returnZeroIfUndefined(obj.dataStatistics[date].INV_LEVEL),
            suggestions = '',
            requests = '';
            suggestions = this.constroiParteVariavelDaTooltip(obj.suggestionByDay,  'DATE_APPROVED', 'sug', 'Sugestão de Compra - Parcela', date)
            requests = this.constroiParteVariavelDaTooltip(obj.dataOpenByDay,  'DATE_CREATED', 'apv', 'Ordem em aberto - Parcela', date)

            return `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap" >
                      <div class="d-flex flex-column">
                          <span class="pln-tooltip-chart-bold d-flex justify-content-between">
                              Situação em: ${date}
                              <img src="/resources/img/INV_LEVEL_${invLevel}.png" class="pln-tooltip-chart-sin">
                          </span>

                          <hr class="pln-tooltip-chart-hr" >

                          <span>
                              <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.availableStock}:</span>
                               ${this.numberFormatter(obj.dataChart[key][9])} (${i18nGlobals[myCountry.getCountry()].units.toLowerCase()})
                          </span>

                          <span>
                              <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.totalStock}:</span>
                               ${this.numberFormatter(obj.dataChart[key][11])} (${i18nGlobals[myCountry.getCountry()].units.toLowerCase()})
                          </span>

                          <hr class="pln-tooltip-chart-hr" >

                          <span>
                              <span class="pln-tooltip-chart-bold pln-tooltip-chart-max">${i18nInfoSku[myCountry.getCountry()].column.maximum}:</span>
                               ${this.numberFormatter(obj.dataChart[key][7])} (${i18nGlobals[myCountry.getCountry()].units.toLowerCase()})
                          </span>

                          <span>
                              <span class="pln-tooltip-chart-bold pln-tooltip-chart-min">${i18nInfoSku[myCountry.getCountry()].column.minimum}:</span>
                               ${this.numberFormatter(obj.dataChart[key][5])} (${i18nGlobals[myCountry.getCountry()].units.toLowerCase()})
                          </span>

                          <hr class="pln-tooltip-chart-hr" >

                          <span>
                              <span class="pln-tooltip-chart-bold pln-tooltip-chart-in">${i18nInfoSku[myCountry.getCountry()].column.ins}:</span>
                               ${this.numberFormatter(obj.dataChart[key][1])}
                          </span>

                          <span>
                              <span class="pln-tooltip-chart-bold pln-tooltip-chart-out">${i18nInfoSku[myCountry.getCountry()].column.outs}:</span>
                               ${this.numberFormatter(obj.dataChart[key][3])}
                          </span>

                          <hr class="pln-tooltip-chart-hr" >

                          ${suggestions}

                          <span>
                              <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.openSolicitations}:</span>
                              ${openRequest}
                          </span>

                          <span>
                              <span class="pln-tooltip-chart-bold pln-tooltip-chart-apv">${i18nInfoSku[myCountry.getCountry()].column.approvedAt}:</span>
                               ${openOrder} (${i18nGlobals[myCountry.getCountry()].for} ${date})
                          </span>

                          <hr class="pln-tooltip-chart-hr" >

                          ${requests}
                        </div>
                    </div>`;
    }

    prepareGenericTooltip(obj, key){
      let tooltips = {}
      obj.dataChart.forEach(row => {

        const inputs = `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
                        <div class="d-flex flex-column">
                        <span>
                            <span class="pln-tooltip-chart-bold">${row[0]}</span>
                        </span>
                        <hr class="pln-tooltip-chart-hr" >
                        <span>
                            <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.ins}:</span>
                            ${this.numberFormatter(row[1])}
                        </span>
                        </div>
                    </div>`,

            outputs = `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
                        <div class="d-flex flex-column">
                        <span>
                            <span class="pln-tooltip-chart-bold">${row[0]}</span>
                        </span>
                        <hr class="pln-tooltip-chart-hr" >
                        <span>
                            <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.outs}:</span>
                            ${this.numberFormatter(row[2])}
                        </span>
                        </div>
                    </div>`,

            minimun = `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
                        <div class="d-flex flex-column">
                        <span>
                            <span class="pln-tooltip-chart-bold">${row[0]}</span>
                        </span>
                        <hr class="pln-tooltip-chart-hr" >
                        <span>
                            <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.minimumStock}:</span>
                            ${this.numberFormatter(row[3])}
                        </span>
                        </div>
                    </div>`,

            maximun = `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
                        <div class="d-flex flex-column">
                        <span>
                            <span class="pln-tooltip-chart-bold">${row[0]}</span>
                        </span>
                        <hr class="pln-tooltip-chart-hr" >
                        <span>
                            <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.maximumStock}:</span>
                            ${this.numberFormatter(row[4])}
                        </span>
                        </div>
                    </div>`,

            available = `<div class="p-2 pln-tooltip-chart pln-tooltip-box text-nowrap">
                        <div class="d-flex flex-column">
                        <span>
                            <span class="pln-tooltip-chart-bold">${row[0]}</span>
                        </span>
                        <hr class="pln-tooltip-chart-hr" >
                        <span>
                            <span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.availableStock}:</span>
                            ${this.numberFormatter(row[5])}
                        </span>
                        </div>
                    </div>`;

        tooltips[row[0]] = {
          'inputs' : inputs,
          'outputs' : outputs,
          'minimun' : minimun,
          'maximun' : maximun,
          'available' : available
        }
      })

      return tooltips
    }

    HistoryConsumptionChart(){
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('string', i18nInfoSku[myCountry.getCountry()].column.day);
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.outs);
        var arr = [];

        this.jsonHistoryConsumption.dataChart.forEach(element => {
            arr.push([...element])
        });

        dataTable.addRows(arr);

        var options = {
            //focusTarget: 'category',
            tooltip: {isHtml: true},
            height: '350',
            chartArea: {
                width: "85%"
            },
            selectionMode: "multiple",
            legend: {
                position: 'top',
                alignment: 'center'
            },
            isStacked:false,
            colors:['#f32938'],
            //title : 'Monthly Coffee Production by Country',
            vAxis: {
                title: i18nInfoSku[myCountry.getCountry()].quantityUnity,
                ticks:
                    [
                        0,
                        {v: this.jsonHistoryConsumption.dataMargin[3], f: `${i18nInfoSku[myCountry.getCountry()].minMargin}. ${this.jsonHistoryConsumption.dataMargin[3]}`},
                        {v: this.jsonHistoryConsumption.dataMargin[1], f: `${i18nInfoSku[myCountry.getCountry()].defaultVariation} ${this.jsonHistoryConsumption.dataMargin[1]}`},
                        {v: this.jsonHistoryConsumption.dataMargin[2], f: `${i18nInfoSku[myCountry.getCountry()].maxMargin} ${this.jsonHistoryConsumption.dataMargin[2]}`}
                    ]
            },
            hAxis: {
                textPosition: 'out'
            },
            seriesType: 'bars'
        };

        var chart = new google.visualization.ComboChart(document.getElementById('historico_consumo'));
        chart.draw(dataTable, options);
        jQuery('.historico-spin').attr('style', 'display:none !important');
        jQuery('#history_day, #history_consumption').css('pointer-events', 'auto');
    }

    //Stock Position - Excess chart
    stockPositionChart(periodType) {
        var arrPrepare = [];
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('string', i18nInfoSku[myCountry.getCountry()].column.month);
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.coverage);
        dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.financialExcess);
        //dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
        if(periodType == 'A'){
            arrPrepare = this.prepareDataStockPosition(this.jsonStockPos.stockposA);
        }else{
            arrPrepare = this.prepareDataStockPosition(this.jsonStockPosWeekly.stockposT);
        }

        dataTable.addRows(arrPrepare);

        var options = {
            //focusTarget: 'category',
            tooltip: {isHtml: true},
            height: '350',
            chartArea: {
                width: '85%'
            },
            selectionMode: 'multiple',
            legend: {
                position: 'top',
                alignment: 'center',
                textStyle: this.textOptionsChart,
            },
            isStacked:false,
            colors:['#30a6ae', '#f32938'],
            vAxes: {
                0: {
                    minValue: 0,
                    title: `${i18nInfoSku[myCountry.getCountry()].column.financialExcess} (${i18nGlobals[myCountry.getCountry()].countryCurrency})`,
                    titleTextStyle: this.textOptionsChart,
                    textStyle: {...this.textOptionsChart, color: '#888888'},
                },
                1: {
                    minValue: 0,
                    title: i18nInfoSku[myCountry.getCountry()].quantityUn,
                    titleTextStyle: this.textOptionsChart,
                    textStyle: {...this.textOptionsChart, color: '#888888'},
                },
            },
            hAxis: {
                textPosition: 'out',
                titleTextStyle: this.textOptionsChart,
                textStyle: this.textOptionsChart,
            },
            seriesType: 'bars',
            series: {
                0: {
                    targetAxisIndex:1,
                },
                1: {
                    targetAxisIndex: 0,
                    type: 'line'
                }
            }
        };

        var chart = new google.visualization.ComboChart(document.getElementById('posicao_estoque'));
        chart.draw(dataTable, options);

        //this.disableTabs(false, 'auto', "");
        /*if (this.checkOtherTabs) {
            AbstractInfoSku.setLoadFinished = false;
        } else {
            AbstractInfoSku.setLoadFinished = true;
        }*/

    }

    prepareDataStockPosition(data){
        var arrPrepare = [];

        data.forEach(element => {
            arrPrepare.push(Object.values(element))
        });

        arrPrepare.forEach(el=>{
            el.splice(1,1);
            el.splice(3,1);
            el.splice(3,1);
            el[1] = parseFloat(el[1]);
            el[2] = {v:parseFloat(el[2]), f:this.setCurrency(parseFloat(el[2]))}
        });

        return arrPrepare;
    }

    //adiciona eventos aos componentes que terão interação com o usuário (linha das tabelas, botão de alterar status e etc)
    listenerBtns(){
        jQuery(window).on('throttledresize', ()=> {
            try{
                this.stockBalanceChart();
            }catch(error){
                return false;
            }
        });

        let eachRow = jQuery(".table_tr"),
        nextParcPlus = jQuery("#info_prox_parc_mais"),
        eachRowItem = jQuery(".tableSug_td");

        //gera o click para as linhas da tabela
        eachRow.on('dblclick', (e) => {
            loaderShow();
            this.sku_id = jQuery(e.currentTarget).attr('data-id-sku');
            this.out6 = jQuery(e.currentTarget).attr('data-out6');
            this.leadTime = jQuery(e.currentTarget).attr('data-lt');
            this.days_horizon = jQuery(e.currentTarget).attr('data-days-horizon');
            this.endDateShoppingForecast = moment(this.today).add(this.days_horizon,'days').format('YYYY-MM-DD');
            AbstractInfoSku.setSkuID = this.sku_id;

            //Abre Dashboard como default
            jQuery('.nav-tabs li:nth-child(1) a').tab('show');

            //if (this.sku_id != this.sku_id_temp) {
                //AbstractInfoSku.setLoadFinished = false;

                jQuery(".balanceamentoEstoqueChart-spin, .previsaoCompras-spin, .historico-spin, .posicaoestoque-spin").attr("style", "display:flex !important");
                jQuery("#shoppingforecast_day, #shoppingforecast_month, #history_day, #history_consumption, #stockpos_quarter, #stockpos_year").css("pointer-events", "none");
                //this.disableTabs(true, 'none', "Aguarde o carregamento das informações...");
                this.hiddenModal();
                this.getHeader(this.sku_id);
            //} else {
                //this.openModal();
            //}
        });

        eachRowItem.on("click", (e) => {
            loaderShow();
            this.sku_id = jQuery(e.currentTarget).attr('data-id-sku');
            AbstractInfoSku.setSkuID = this.sku_id;

            //if (this.sku_id != this.sku_id_temp) {
                //this.loadFinished = false;
                this.hiddenModal();
                this.getHeader(this.sku_id);
            //} else {
                //this.openModal();
            //}

            //Abre Movimento ERP como default
            const changeValue = jQuery(e.currentTarget).attr('data-change');
            jQuery('.nav-tabs li:nth-child(3) a').tab('show').trigger('click');
            jQuery('#erp-select').val(changeValue).trigger('change');
        });

        jQuery('#modal_info').on('hide.bs.modal', ()=> {
            setInfoSkuUrl('list');
            jQuery("#modal_info .modal-footer").hide();
            jQuery('.info_modal_header').hide();
            jQuery('.info_modal_header_loading').attr('style', 'display:flex !important');
            loaderHide();
        });

        jQuery('#modal_info').on('shown.bs.modal', () => {
            setInfoSkuUrl('dashboard_tab');

            jQuery(".pln-tab-infosku").click((element) => {
                const activeTab = jQuery(element.currentTarget).attr('id');
                setInfoSkuUrl(activeTab);
                
                jQuery("#modal_info .modal-footer").hide();
                if (activeTab == "information_tab") {
                  jQuery("#modal_info .modal-footer").show();
                }
            });
        });

        //altera o status de sistema para Ativo/Inativo
        this.btnStatusSystem.on("click", (e)=>{
            if(jQuery(e.currentTarget).hasClass("btn-success")) {
                this.confirmInativation(this.sku_id, 0);
            }else{
                this.btnStatusSystem.attr("disabled", true)
                    .find('i').addClass('fa fa-spin fa-spinner');
                this.updateStatusToActive(this.sku_id, 1);
            }
        });

        //abre a aba Movimento do ERP
        nextParcPlus.on("click", (e)=>{
            this.openMovtoErp();
        });

        this.historyMovtoErp.on("click", (e)=>{
            this.openMovtoErp();
        });

        //call soppingforecast chart (daily option)
        this.shoppingForecastDaily.on("click", ()=>{
            this.togglePeriodOption(this.shoppingForecastDaily, 'default');
            this.togglePeriodOption(this.shoppingForecastMonth, 'none');
            jQuery(".previsaoCompras-chart-mes, .out6").hide('fast');
            jQuery(".previsaoCompras-chart-dia, .lead-time").show('fast');
        });

        //call soppingforecast chart (month option )
        this.shoppingForecastMonth.on('click', ()=>{

            this.togglePeriodOption(this.shoppingForecastDaily, 'none');
            this.togglePeriodOption(this.shoppingForecastMonth, 'default');
            jQuery('.previsaoCompras-chart-dia, .lead-time').hide('fast');
            jQuery('.previsaoCompras-chart-mes, .out6').show('fast');
            jQuery('#out6-value').html(`${this.out6} %`);

            if(this.jsonShoppingForecastMonth === null || this.jsonShoppingForecastMonth === undefined || this.jsonShoppingForecastMonth.length === 0){
                jQuery('.previsaoCompras-chart-mes').html('')
                this.getShoppingForecastMonth();
            }

                // jQuery(".out6").show('fast');
        });

        //call history chart (daily option)
        this.historyDaily.on('click', ()=>{
            this.togglePeriodOption(this.historyDaily, 'default');
            this.togglePeriodOption(this.historyConsumption, 'none');
            jQuery('.historico-chart-consumo').hide('fast');
            jQuery('.historico-chart-dia').show('fast');
        });

        //call history consumption chart
        this.historyConsumption.on('click', ()=>{
            this.togglePeriodOption(this.historyDaily, 'none');
            this.togglePeriodOption(this.historyConsumption, 'default');
            jQuery('.historico-chart-dia').hide('fast'); //attr("style", "display:none !important");
            jQuery('.historico-chart-consumo').show('fast'); //attr("style", "display:block !important");

            if(this.jsonHistoryConsumption === null || this.jsonHistoryConsumption === undefined || this.jsonHistoryConsumption.length === 0){
                jQuery('.historico-chart-consumo').html('')
                this.getHistoryConsumption();
            }
        });

        this.stockPositionQuarter.on("click", ()=>{
            this.togglePeriodOption(this.stockPositionQuarter, "default");
            this.togglePeriodOption(this.stockPositionYear, "none");
            this.stockPositionChart("T");
        });

        this.stockPositionYear.on("click", ()=>{
            var $this = this;
            $this.togglePeriodOption($this.stockPositionYear, "default");
            $this.togglePeriodOption($this.stockPositionQuarter, "none");
            if(($this.jsonStockPos != null && $this.jsonStockPos != "")) {
                $this.stockPositionChart("A");
                return true;
            }

            jQuery('#posicao_estoque').attr("style", "display:none !important");
            jQuery('.posicaoestoque-spin').attr("style", "display:flex !important");
            jQuery.ajax({
                url: `/${myCountry.getCountry()}/infosku/stockposition/${this.sku_id}`,
                type: 'GET',		
                datatype: 'text',
                success: function (response) {
                    $this.jsonStockPos = response;
                    jQuery('#posicao_estoque').attr("style", "display:flex !important");
                    $this.stockPositionChart("A");
                    jQuery('.posicaoestoque-spin').attr("style", "display:none !important");
                }
            });
        });

        /*this.othersTab.on("click", ()=>{
            this.checkOtherTabs = true;
        })*/

        //se o usuário alterar a aba da modal enquanto os gráficos são renderizados, teremos erro de js ou os gráficos quebrarão
        //no tamanho. Para evitar isso, vamos recarregar aos gráficos da dashboard APENAS SE o usuário tentar alterar a aba antes do
        //término do carregamento dos gráficos
        this.dashboardTab.on("click", () => {
            //this.checkOtherTabs = false;
            if (!AbstractInfoSku.getLoadFinished) {
                jQuery('.balanceamentoEstoqueChart-spin, .previsaoCompras-spin, .historico-spin, .posicaoestoque-spin').attr("style", "display:flex !important");
                jQuery('#shoppingforecast_day, #shoppingforecast_month, #history_day, #history_consumption, #stockpos_quarter, #stockpos_year').css("pointer-events", "none");

                this.hiddenModal();
                this.initialize();

            }
        });

        jQuery('#forecastApply').unbind('click').on('click', () => {
            this.applyForecastDailyDate();
        })

        jQuery('#historyApply').unbind('click').on('click', () => {
            this.applyHistoryDailyDate();
        })

        jQuery('#recalculate_sku').unbind('click').on('click', () => {
            this.recalculateSKU();
        });

    }

    recalculateSKU() {
        loaderShow();
        jQuery.ajax({
            url: `/${myCountry.getCountry()}/infosku/recalculate/${AbstractInfoSku.getSkuID}`,
            type: 'POST',
            datatype: 'json',
        }).done((response)=> {

            this.sku_id = this.sku_id_temp = null;
            jQuery(`#${AbstractInfoSku.getSkuID}`).trigger('dblclick');
            jQuery('.nav-tabs li:nth-child(2) a').tab('show');
            loaderHide();
            if(!response.data){
                alert(i18nGlobals[myCountry.getCountry()].errRecalculate)
            }

        });
    }

    applyHistoryDailyDate() {
        this.startDateHistory = jQuery('#historyStartDate').val();
        this.endDateHistory = jQuery('#historyEndDate').val();

        //show spinner while the chart is rendering
        jQuery('.historico-spin').attr("style", "display:flex !important");
        jQuery('#history_day, #history_consumption').css("pointer-events", "none");
        jQuery('#historico').hide();

        this.genericAjax('dailyhistory');
    }

    applyForecastDailyDate() {
        this.startDateShoppingForecast = jQuery('#forecastStartDate').val();
        this.endDateShoppingForecast = jQuery('#forecastEndDate').val();

        jQuery('.previsaoCompras-spin').attr("style", "display:flex !important");
        jQuery('.previsaoCompras-chart-dia').hide()

        this.genericAjax('shoppingforecastdaily');
    }

    openMovtoErp(){
        jQuery('#erp_tab').trigger("click");
        jQuery('.nav-tabs li:nth-child(3) a').tab('show');
    }

    hiddenModal(){
        this.jsonShoppingForecastMonth = null;
        this.jsonHistoryConsumption = null;
        this.jsonStockPos = null;

        this.togglePeriodOption(this.shoppingForecastDaily, 'default');
        this.togglePeriodOption(this.historyDaily, 'default');

        this.togglePeriodOption(this.shoppingForecastMonth, 'none');
        this.togglePeriodOption(this.historyConsumption, 'none');

        this.togglePeriodOption(this.stockPositionQuarter, "default");
        this.togglePeriodOption(this.stockPositionYear, "none");

        //clean Average history
        jQuery("#info_average_30").html('');
        jQuery("#info_average_3").html('');
        jQuery("#info_average_6").html('');
        jQuery("#info_average_12").html('');
        jQuery(`.balanceamentoEstoqueChart-chart,
                .stock-balance-cover,
                .previsaoCompras-chart-dia,
                .previsaoCompras-chart-mes,
                .out6,
                .lead-time,
                .historico-chart-dia,
                .historico-chart-consumo,
                .posicaoestoque-chart`)
                .attr("style", "display:none !important");

    };

    disableTabs(disable, pointerEvent, title){
        //this variable contains the number of tabs, excludin the first (dashboard = default tab) and the last tab (STATUS option)
        //we need to disable the other tabs while dashboard is loading
        let iterator = jQuery('.nav-tabs').children().length;
        for(var i=1; i < iterator; i++){
            jQuery(`.nav-tabs li:nth-child(${i})`)
            .prop("title", title)
            .find('a').prop('disabled', disable)
            .css({"pointer-events": pointerEvent});
        }

    }

    //hide/show the chart depending on the type
    togglePeriodOption(id, status){
        if(status == 'default'){
            id.removeClass("pln-link-color").addClass("pln-link-decoration-none")
        }else{
            id.removeClass("pln-link-decoration-none").addClass("pln-link-color");
        }

    }

    //inicializa o plugin para title dos elementos
    initTooltip(){
        jQuery("[data-toggle=tooltip], [data-tooltip=tooltip]").tooltip();
    }

    buildRoute(route){

        switch(route){
            case 'balancing':
                return `balancing/${this.sku_id}`;

            case 'shoppingforecastdaily':
                return `shoppingforecastdaily/${this.sku_id}/${this.startDateShoppingForecast}/${this.endDateShoppingForecast}`;

            case 'dailyhistory':
                return `dailyhistory/${this.sku_id}/${this.startDateHistory}/${this.endDateHistory}`;

            case 'stockposition':
                return `stockposition/${this.sku_id}`;

            case 'stockposition_weekly':
                return `stockposition/weekly/${this.sku_id}`;
        }
    }
    genericAjax(route){
        if(!this.sku_id){
            return false;
        }
        var x = this.buildRoute();
        return $.get( `/${myCountry.getCountry()}/infosku/${this.buildRoute(route)}`)
            .done((response) => {
                switch(route) {
                    case 'balancing':
                        this.getStockBalance(response);
                        break;
                    case 'shoppingforecastdaily':
                        this.getShoppingForecastDay(response);
                        break;
                    case 'dailyhistory':
                        this.getHistory(response);
                        break;
                    default:
                        this.getStockPosition(response);
                }
            })
            .fail(function(fail) {
                alert(i18nInfoSku[myCountry.getCountry()].notPossible + i18nInfoSku[myCountry.getCountry()][route]+'. '+i18nGlobals[myCountry.getCountry()].caseErrPersist)
            });
    }

    //busca no banco as informações da SKU para popular o header da modal
    getHeader(idSku) {
        $.get( `/${myCountry.getCountry()}/infosku/header/${idSku}`)
        .done((response) => {
            this.setInfoHeader(response.header);
            this.setNextParc(response.next_installment);
            this.setLateOrder(response.orders);
            this.initialize();
        });
        
        if (!this.init) {
            this.initTooltip();
            this.init = true;
        }

        this.openModal();
    }

    //seta as informações do banco nos elementos HTML do header
    setInfoHeader(data){
        const sinTooltip = {0: i18nInfoSku[myCountry.getCountry()].headSignal.zero, 1: i18nInfoSku[myCountry.getCountry()].headSignal.veryLow, 2:i18nInfoSku[myCountry.getCountry()].headSignal.low, 3:i18nInfoSku[myCountry.getCountry()].headSignal.ideal, 4:i18nInfoSku[myCountry.getCountry()].headSignal.high, 5: i18nInfoSku[myCountry.getCountry()].headSignal.veryHigh};
        const resource = "/resources/img/INV_LEVEL_";
        AbstractInfoSku.setEstablishmentName = data.estab;
        this.checkSinalizator(data.sin);

        jQuery('#info_manager_in_header').html(data.manager_login);
        jQuery("#info_item").html(`${data.item}`);
        jQuery("#info_description").html(`${data.description}`).attr("data-original-title", data.description);
        jQuery("#info_cod_estab").html(data.cod_estab);
        jQuery("#info_estab").html(` - ${data.estab}`);
        jQuery("#info_curve").html(data.curve);
        jQuery("#info_category").html(data.category);
        jQuery("#info_unit").html(data.unity);
        jQuery("#info_status_erp").html(data.status_erp);
        jQuery("#info_politics").html(data.politics);
        jQuery("#info_sinalizator").prop("src", `${resource}${data.sin}.png`);
        jQuery("#sinalizator_tooltip").attr('data-original-title', `${sinTooltip[data.sin]}`);
        jQuery("#info_days").html(data.days_horizon);
        jQuery('#val_unit_erp').html(parseFloat(data.val_unit_erp).toLocaleString(myCountry.getLanguage(), {minimumFractionDigits: 2,style: 'currency',currency: myCountry.getCurrency()}));
        this.days_horizon = data.days_horizon;
        this.setBtnStatusSystem(data.status);
        jQuery('.info_modal_header').show();
        jQuery(".info_modal_header_loading").attr("style", "display:none !important");

        /**
         * Seta as informações de data de acordo com a política do produto
         */
        this.startDateShoppingForecast = moment(this.today).format('YYYY-MM-DD');
        jQuery('#forecastStartDate').val(this.startDateShoppingForecast);
        this.endDateShoppingForecast = moment(this.today).add(this.days_horizon, 'd').format('YYYY-MM-DD');
        jQuery('#forecastEndDate').val(this.endDateShoppingForecast);
        jQuery('#historyStartDate').val(moment(this.today).subtract(this.days_horizon, 'd').format('YYYY-MM-DD'));
        jQuery('#historyEndDate').val(moment(this.today).format('YYYY-MM-DD'));
        this.applyHistoryDailyDate();
    }

    checkSinalizator(sin){
        if(!sin){
            jQuery("#sinalizator_tooltip").html(`<span>${i18nInfoSku[myCountry.getCountry()].headSignal.abbrNotAvailable}</span>`);
        }else{
            jQuery("#sinalizator_tooltip").html(`<img id="info_sinalizator" src="">`);
        }
    }

    //seta as informações do banco nos elementos HTML de PRÓXIMA PARCELA
    setNextParc(data){
        if (jQuery(".div-nextparc").hasClass("pln-critico")) {
            jQuery(".div-nextparc").removeClass("pln-critico")
        }
        if(data.arrear_days != "null" && data.arrear_days != "0"){
            jQuery(".div-nextparc").addClass("pln-critico");
        }
        jQuery("#info_nextparc_days").html(this.unitFormatter(this.checkDataValue(data.arrear_days)))
        jQuery("#info_nextparc_qty").html(this.unitFormatter(this.checkDataValue(data.quantity)))
        jQuery("#info_nextparc_val").html(this.checkDataValue(data.total))
    }

    //seta as informações do banco nos elementos HTML de ORDENS ATRASADAS
    setLateOrder(data){
        if (jQuery(".div-lateorder").hasClass("pln-critico")) {
            jQuery(".div-lateorder").removeClass("pln-critico")
        }
        if(data.ordens_atrasadas != "null" && data.ordens_atrasadas != "0"){
            jQuery(".div-lateorder").addClass("pln-critico");
        }
        jQuery("#info_order_late_qtd").html(`${this.checkDataValue(data.ordens_atrasadas)} ${(data.ordens_atrasadas == 1 ? i18nInfoSku[myCountry.getCountry()].order : i18nInfoSku[myCountry.getCountry()].orders)}`)

        jQuery("#info_code_item").html(this.checkDataValue(data.codigo))
        jQuery("#info_days_late").html(this.checkDataValue(data.atraso))
        jQuery("#info_order_late_uni").html(this.checkDataValue(data.qtde))
        jQuery("#info_order_late_supplier").html(this.checkDataValue(data.fornecedor))
    }

    setAverageHistory(data) {
        jQuery("#info_average_30").html(this.applyFormatLocale(data.OUT_LAST_30D));
        jQuery("#info_average_3").html(this.applyFormatLocale(data.AVG_3M));
        jQuery("#info_average_6").html(this.applyFormatLocale(data.AVG_6M));
        jQuery("#info_average_12").html(this.applyFormatLocale(data.AVG_12M));
    }

    checkDataValue(value) {
        if(value == 'null' || value == null) {
            return 0;
        }

        return value;
    }

    openModal(){
        loaderHide();
        jQuery("#modal_info").modal("show");
    }

    //seta as classes no botão Ativo/Inativo
    setBtnStatusSystem(status){
        if (status.trim() == `${i18nGlobals[myCountry.getCountry()].active}`){
            this.btnStatusSystem
                .removeClass("btn-danger")
                .addClass("btn-success")
                .html(`${i18nGlobals[myCountry.getCountry()].active}`);
        } else {
            this.btnStatusSystem
                .removeClass("btn-success")
                .addClass("btn-danger")
                .html(`<i class=''></i>${i18nGlobals[myCountry.getCountry()].inactive}`);
        }
    }

    //faz update no status da SKU para 1 (Ativo)
    updateStatusToActive(id_sku, status){
        return $.ajax({
            url: `/${myCountry.getCountry()}/infosku/updatestatus`,
            data: {
                "id":id_sku,
                "status":status
            },
            type: 'POST',
            datatype: 'text',
            success: (response)=> {
                this.btnStatusSystem
                    .removeClass("btn-danger")
                    .addClass("btn-success")
                    .html("Ativo")
                    .attr("disabled", false);
            },
            error: function (error) {
                alert(i18nGlobals[myCountry.getCountry()].updateStatusFail+'. '+i18nGlobals[myCountry.getCountry()].caseErrPersist)
            }
        });
    }

    //request data to compose the stock balance chart
    getStockBalance(response){
        this.jsonStockBalance = (response.stock_balance || 0);
        this.jsonStockBalanceCover = (response.details_stock_balancing || 0);
        google.charts.setOnLoadCallback(()=> { this.stockBalanceChart()});
        jQuery(".balanceamentoEstoqueChart-spin").attr("style", "display:none !important");
        jQuery(".balanceamentoEstoqueChart-chart").attr("style", "display:block !important");
        jQuery(".stock-balance-cover").attr("style", "display:block !important");
        this.setExcess();
    }


    //request data to compose the shoppingforecast chart daily
    getShoppingForecastDay(response){
        jQuery(".previsaoCompras-spin").attr("style", "display:none !important");
        jQuery("#shoppingforecast_day, #shoppingforecast_month").css("pointer-events", "auto");
        jQuery(".previsaoCompras-chart-dia").attr("style", "display:block !important");
        this.jsonShoppingForecast = response;
        google.charts.setOnLoadCallback(()=> {this.shoppingForecastChartDaily()});
    }

    //request data to compose the shoppingforecast chart day month
    getShoppingForecastMonth(){
        jQuery(".previsaoCompras-spin").attr("style", "display:flex !important");
        jQuery("#shoppingforecast_day, #shoppingforecast_month").css("pointer-events", "none");
        $.get( `/${myCountry.getCountry()}/infosku/shoppingforecastmonthly/${this.sku_id}`)
            .done((response)=> {

                this.jsonShoppingForecastMonth = response;
                google.charts.setOnLoadCallback(()=> {this.shoppingForecastChartMonth()});
            })
    }

    //request data to compose the History chart daily
    getHistory(response){
        jQuery('.historico-spin').attr("style", "display:none !important");
        jQuery('#history_day, #history_consumption').css("pointer-events", "auto");
        jQuery('.historico-chart-dia').attr("style", "display:block !important");
        this.jsonHistory = response;
        this.tooltipTotalStock = this.prepareGenericTooltip(this.jsonHistory);
        this.setAverageHistory(response.erpLastDays)
        google.charts.setOnLoadCallback(()=> {this.HistoryChart()});
    }

    //request data to compose the shoppingforecast chart day month
    getHistoryConsumption(){
        jQuery('.historico-spin').attr("style", "display:flex !important");
        jQuery('#history_day, #history_consumption').css("pointer-events", "none");
        $.get( `/${myCountry.getCountry()}/infosku/deviationconsumption/${this.sku_id}`)
            .done((response)=> {

                this.jsonHistoryConsumption = response;
                google.charts.setOnLoadCallback(()=> {this.HistoryConsumptionChart()});
            })
    }

    getStockPosition(response){       
        jQuery(".posicaoestoque-spin").attr("style", "display:none !important");
        jQuery("#stockpos_quarter, #stockpos_year").css("pointer-events", "auto");
        jQuery(".posicaoestoque-chart").attr("style", "display:block !important");
        this.jsonStockPosWeekly = response;
        google.charts.setOnLoadCallback(()=> {this.stockPositionChart("T")});
    }

    //Exibe alerta de confirmação para Inativação da SKU
    confirmInativation(id_sku, status){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: "btn pln-btn-orange ml-2",
            cancelButtonClass: "btn btn-default",
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            reverseButtons: true,
            animation: false,
            title: 'Inativar SKU',
            text: "Você tem certeza que deseja inativar este produto!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Inativar',
            cancelButtonText: 'Cancelar',
            preConfirm: () => {
                return $.ajax({
                    url: `/${myCountry.getCountry()}/infosku/updatestatus`,
                    data: {
                        "id":id_sku,
                        "status":status
                    },
                    type: 'POST',
                    datatype: 'text',
                });
            },
            allowOutsideClick: () => false
        })
            .then((result) => {
                if (result.value) {
                    this.btnStatusSystem
                        .removeClass("btn-success")
                        .addClass("btn-danger")
                        .html("<i class=''></i> Inativo");
                }
            })
    }

    applyFormatLocale(value) {
        return parseFloat(value).toLocaleString(this.locale);
    }

    unitFormatter(value) {
      const transformed = Intl.NumberFormat(this.locale, {maximumFractionDigits:4, style: 'decimal'}).format(Number(value))
        return transformed;
    }

    //Esconde casas decimais quando o valor é igual a 0
    numberFormatter(value){
        let numberValue = Number(value).toFixed(2);        
        if(Number.isInteger(Number(numberValue))){
          return this.unitFormatter(parseInt(numberValue));
        }
        return numberValue.replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
    }
}

function setInfoSkuUrl(activeTab) {
    let infoSkuModalUrl = '';

    switch (activeTab) {
        case 'dashboard_tab':
            infoSkuModalUrl = '#infosku-dashboard';
            break;
        case 'information_tab':
            infoSkuModalUrl = '#infosku-info';
            break;
        case 'erp_tab':
            infoSkuModalUrl = '#infosku-erp';
            break;
        case 'log_tab':
            infoSkuModalUrl = '#infosku-log';
            break;
        case 'list':
            infoSkuModalUrl = '#';
            break;
        default:
            throw new Error('Invalid infosku tab option');
    }

    window.history.replaceState(null, null, infoSkuModalUrl);
}