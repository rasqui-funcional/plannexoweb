let _skuID = null;
let _establishmentName = null;
let _enableSaveButton = null;
let _loadFinished = null;

class AbstractInfoSku {
    constructor() {
        if (this.constructor === AbstractInfoSku) {
            throw new TypeError('Abstract class "AbstractInfoSku" cannot be instantiated directly.');
        }
    }

    static set setSkuID(skuID) {
        _skuID = skuID;
    }

    static get getSkuID() {
        return _skuID;
    }

    static set setEstablishmentName(establishmentName) {
        _establishmentName = establishmentName;
    }

    static get getEstablishmentName() {
        return _establishmentName;
    }

    static set setEnableSaveButton(enableSaveButton) {
        _enableSaveButton = enableSaveButton;
    }

    static get getEnableSaveButton() {
        return _enableSaveButton;
    }

    static set setLoadFinished(loadFinished) {
        _loadFinished = loadFinished;
    }

    static get getLoadFinished() {
        return _loadFinished;
    }
}
