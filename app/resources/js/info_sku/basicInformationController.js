class BasicInformationController extends AbstractInfoSku {
    constructor() {
        super();
        this.base = {};
        this.groupings = {};
        this.globalManagers = [];
        this.observation = '';
        this.existCurrentChange = false;
        this.fetchInformationsByTabClick();
        this.mapSaveClick();
    }

    mapSaveClick() {
        jQuery('#info_sku_save_click').unbind('click').on('click', () => {
            if (AbstractInfoSku.getEnableSaveButton && this.existCurrentChange) {
                const params = this.buildUpdateData();
                jQuery('#info_sku_save_click').addClass('disabled').removeClass('pln-btn-orange');
                loaderShow();
                jQuery('#basic_info_data').hide();
                jQuery('#basic_info_load').attr("style", "display:flex !important");

                $.ajax({
                    url: `/${myCountry.getCountry()}/infosku/informationsUpdate/${AbstractInfoSku.getSkuID}`,
                    type: 'POST',
                    datatype: 'json',
                    data: params,
                    success: () => {
                        jQuery('.save-sku-success').removeClass('d-none');
                        AbstractInfoSku.setEnableSaveButton = false;
                        AbstractInfoSku.setLoadFinished = false;
                        this.existCurrentChange = false;
                        this.setManagerInHeader(params.userId);
                        jQuery('#basic_info_load').attr("style", "display:none !important");
                        jQuery('#basic_info_data').show();
                        setTimeout(function () {
                            jQuery('.save-sku-success').addClass('d-none');
                        }, 5000);
                        loaderHide();
                    }
                });
            }
        });
    }

    buildUpdateData() {
        let data = {
            userId: this.base.USER_ID,
            defaultLocal: this.base.DEFAULT_LOCAL,
            notes: this.observation || '',
            curveXYZ: this.base.CURVE_XYZ
        };

         for (let i = 5; i < 13; i++) {
             data['codGroup'+ i +'FK'] = this.base['COD_GROUP'+ i +'_FK'];
         }

        return data;
    }

    fetchInformationsByTabClick() {
        const eachRow = jQuery('#information_tab');

        eachRow.unbind('click').on('click', () => {
            jQuery('#info_sku_save_click').addClass('disabled');
            jQuery('#info_sku_curve_xyz').empty();
            jQuery('#info_sku_managers').empty();
            jQuery('#info_sku_standard_deposits').empty();
            jQuery('#info_sku_scm_group').empty();
            jQuery('#basic_info_data').hide();
            jQuery('#basic_info_load').attr("style", "display:flex !important");

            this.fetchInformations();
        });
    }

    fetchInformations() {
        $.ajax({
            url: `/${myCountry.getCountry()}/infosku/informations/${AbstractInfoSku.getSkuID}`,
            type: 'GET',
            datatype: 'json',
            success: (response) => {
                jQuery('#basic_info_load').attr("style", "display:none !important");
                jQuery('#basic_info_data').show();

                const { base, observation, managers, standardDeposits, groupings } = response.data;
                this.base = base;
                this.groupings = groupings;
                this.observation = observation;
                this.globalManagers = managers;
                this.buildBasicFields();
                this.buildCurveXYZ();
                this.buildObservation();
                this.buildManagers(managers);
                this.buildStandardDeposits(standardDeposits);
                this.buildScmGroups(groupings);
                const profile = jQuery('#userProfile').val();

                if(profile == 'spdmPharmaceutical' || profile == 'spdmPlanner' )
                    jQuery("#tab_info :input").prop("disabled", true);

            },
        });
    }

    formatCurrentValue(value){
        return value.toLocaleString(myCountry.getLanguage(), {minimumFractionDigits: 2,style: 'currency', currency: myCountry.getCurrency()});
    }

    buildBasicFields() {
        jQuery('#info_sku_cod').html(this.base.ID_SKU_PK);
        jQuery('#info_sku_item').html(this.base.COD_ITEM_PK + ' - ' + this.base.DESC_ITEM);
        jQuery('#info_sku_establishment').html(AbstractInfoSku.getEstablishmentName);
        jQuery('#info_sku_unity').html(this.base.COD_STD_UNIT);
        jQuery('#info_sku_unity_value').html(this.formatCurrentValue(parseFloat(this.base.VAL_UNIT_ERP)));
        jQuery('#info_sku_category').html(this.base.COD_GROUP1_FK);
        jQuery('#info_sku_curve_abc').html(this.base.CURVE_ABC);
        jQuery('#info_sku_curve_pqr').html(this.base.CURVE_PQR);
        jQuery('#info_sku_curve_123').html(this.base.CURVE_123);
    }

    buildCurveXYZ() {
        ['X', 'Y', 'Z'].forEach((curveXYZ) => {
            let option = '';

            if (this.base.CURVE_XYZ === curveXYZ) {
                option = '<div class="radio" style="display: inline-block; margin-left: 10px;">\n' +
                         '  <label><input type="radio" name="optradio" id="info_sku_curve_'+ curveXYZ +'" value="'+ curveXYZ +'" checked>'+ curveXYZ +'</label>\n' +
                         '</div>';
            } else {
                option = '<div class="radio" style="display: inline-block; margin-left: 10px;">\n' +
                         '  <label><input type="radio" name="optradio" id="info_sku_curve_'+ curveXYZ +'" value="'+ curveXYZ +'">'+ curveXYZ +'</label>\n' +
                         '</div>';
            }

            jQuery('#info_sku_curve_xyz').append(option);

            this.listenChangeEvents(
                '#info_sku_curve_'.concat(curveXYZ),
                'CURVE_XYZ'
            );
        });
    }

    buildObservation() {
        if (this.observation === null) {
            jQuery('#info_sku_observation').val('');
        } else {
            jQuery('#info_sku_observation').val(this.observation);
        }

        jQuery('#info_sku_observation').bind('input propertychange',() => {
            jQuery('#info_sku_save_click').removeClass('disabled');
            AbstractInfoSku.setEnableSaveButton = true;

            const observation = jQuery('#info_sku_observation').val();
            this.observation = observation;
            this.existCurrentChange = true;
        });
    }

    buildManagers(managers) {
        managers.forEach((manager) => {
            let option = '';

            if (manager.ID_USER_PK === this.base.USER_ID) {
                option = '<option value="'+ manager.ID_USER_PK +'" selected>' + manager.LOGIN + '</option>';
            } else {
                option = '<option value="'+ manager.ID_USER_PK +'">' + manager.LOGIN + '</option>';
            }

            jQuery("#info_sku_managers").append(option);
        });

        this.listenChangeEvents(
            '#info_sku_managers',
            'USER_ID'
        );
    }

    setManagerInHeader(manager) {
        let selectedManager = this.globalManagers.filter(item => item.ID_USER_PK == manager)[0];

        if(!selectedManager)
        {
            selectedManager = {
                LOGIN: ''
            };
        }

        jQuery('#info_manager_in_header').html(selectedManager.LOGIN);
    }

    buildStandardDeposits(standardDeposits) {
        jQuery('#info_sku_standard_deposits').append('<option></option>');
        standardDeposits.forEach((standardDeposit) => {
            let option = '';

            if (standardDeposit.COD_LOCAL_PK === this.base.DEFAULT_LOCAL) {
                option = '<option value="'+ standardDeposit.COD_LOCAL_PK +'" selected>' + standardDeposit.DESC_LOCAL + '</option>';
            } else {
                option = '<option value="'+ standardDeposit.COD_LOCAL_PK +'">' + standardDeposit.DESC_LOCAL + '</option>';
            }

            jQuery("#info_sku_standard_deposits").append(option);
        });

        this.listenChangeEvents(
            '#info_sku_standard_deposits',
            'DEFAULT_LOCAL'
        );
    }

    buildScmGroups(groupings) {
        groupings.forEach((group, index) => {
            jQuery("#info_sku_scm_group").append(
                '<div class="col-sm-3">\n' +
                '    <div class="row">\n' +
                '        <span class="pln-bold-600">\n' +
                '            ' + group.DESC_GROUP + ':\n' +
                '        </span>\n' +
                '    </div>\n' +
                '    <div class="row">\n' +
                '        <select\n' +
                '            class="form-control form-control-sm"\n' +
                '            style="margin-right: 40px"\n' +
                '            id="info_sku_num_group_pk_' + group.NUM_GROUP_PK + '"\n' +
                '        >\n' +
                '        </select>\n' +
                '    </div>\n' +
                '</div>'
            );

            if (group.NUM_GROUP_PK < 5) {
                jQuery('#info_sku_num_group_pk_' + group.NUM_GROUP_PK).prop('disabled', true);
            }

            jQuery('#info_sku_num_group_pk_' + group.NUM_GROUP_PK).append('<option></option>');

            const options = group.options;
            options.forEach((option) => {
                let result = '';

                if (option.COD_GROUP_PK === this.base['COD_GROUP' + (group.NUM_GROUP_PK) + '_FK']) {
                    result = '<option value="'+ option.COD_GROUP_PK +'"selected>' + option.DESC_GROUP_CONT + '</option>';
                } else {
                    result = '<option value="'+ option.COD_GROUP_PK +'">' + option.DESC_GROUP_CONT + '</option>';
                }

                jQuery('#info_sku_num_group_pk_' + group.NUM_GROUP_PK).append(result);
            });

            this.listenChangeEvents(
                '#info_sku_num_group_pk_' + group.NUM_GROUP_PK,
                'COD_GROUP'+ group.NUM_GROUP_PK +'_FK'
            );
        });
    }

    listenChangeEvents(id, attribute) {
        jQuery(id).change(() => {
            jQuery('#info_sku_save_click').removeClass('disabled').addClass('pln-btn-orange');
            jQuery('.save-sku-success').addClass('d-none');
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery(id).val();
            this.base[attribute] = value;
            this.existCurrentChange = true;
        });
    }
}
