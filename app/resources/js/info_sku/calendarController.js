class CalendarController extends AbstractInfoSku {
    constructor() {
        super();
        this.vCalendar = null;
        this.fetchCalendarInformationsByTabClick();
        this.fetchCalendarBatchOfPurchasesByOptionClick();
        this.listenSaveClick();
    }

    listenSaveClick() {
        jQuery('#info_sku_save_click').click(() => {
            if (AbstractInfoSku.getEnableSaveButton && this.vCalendar !== null) {
                jQuery('#info_sku_save_click').addClass('disabled').removeClass('pln-btn-orange');;
                jQuery('#info_sku_save_click').html(`${i18nGlobals[myCountry.getCountry()].saving}...`);

                jQuery('#calendar_data').hide();
                jQuery('#calendar_load').attr("style", "display:flex !important");

                $.ajax({
                    url: `/${myCountry.getCountry()}/infosku/calendar/update/${AbstractInfoSku.getSkuID}`,
                    type: 'POST',
                    datatype: 'json',
                    data: this.buildUpdateData(),
                    success: () => {
                        jQuery('#info_sku_save_click').html(`<i class="fa fa-save" aria-hidden="true"></i> ${i18nGlobals[myCountry.getCountry()].saveAlterations}`);
                        jQuery('.save-sku-success').removeClass('d-none');
                        AbstractInfoSku.setEnableSaveButton = false;

                        jQuery('#calendar_load').attr("style", "display:none !important");
                        jQuery('#calendar_data').show();
                        this.vCalendar = null;
                        setTimeout(function () {
                            jQuery('.save-sku-success').addClass('d-none');
                        }, 5000);
                    }
                });
            }
        });
    }

    buildUpdateData() {
        let data = {
            vCalendar: this.vCalendar,
        };

        return data;

    }

    fetchCalendarInformationsByTabClick() {
        const eachRow = jQuery('#information_tab');

        eachRow.on('click', () => {
            jQuery('#info_sku_calendar_batch_of_purchases').empty();

            jQuery('#calendar_data').hide();
            jQuery('#calendar_load').attr("style", "display:flex !important");

            this.fetchCalendarInformations();
        });
    }

    fetchCalendarBatchOfPurchasesByOptionClick() {
        const eachRow = jQuery('#info_sku_calendar_batch_of_purchases');

        eachRow.unbind('change').on('change', () => {
            const batchOfPurchase = jQuery('#info_sku_calendar_batch_of_purchases').val();
            this.fetchCalendarBatchOfPurchases(batchOfPurchase);

            jQuery('#calendar_data').hide();
            jQuery('#calendar_load').attr("style", "display:flex !important");
        });
    }

    fetchCalendarBatchOfPurchases(companyCalendarID) {
        $.ajax({
            url: `/${myCountry.getCountry()}/infosku/calendar/dates/${companyCalendarID}`,
            type: 'GET',
            datatype: 'json',
            success: (response) => {
                jQuery('#calendar_data').show();
                jQuery('#calendar_load').attr("style", "display:none !important");

                const data = response.data;
                const { receivingDates, calendarIntervalAverage } = data;

                jQuery('#info_sku_calendar_batch_of_purchases_in_days').html(calendarIntervalAverage);

                Object.keys(receivingDates).forEach((key, index1) => {
                    receivingDates[key].forEach((value, index2) => {
                        const currentDate = new Date(receivingDates[key][index2].value);

                        jQuery('#datepicker'.concat(index1 + 1)).datepicker({
                            dayNames: i18nGlobals[myCountry.getCountry()].calendar.daysOfWeek,
                            dayNamesMin: i18nGlobals[myCountry.getCountry()].calendar.daysOfWeekAbbr,
                            dayNamesShort: i18nGlobals[myCountry.getCountry()].calendar.daysOfWeekShort,
                            monthNames: i18nGlobals[myCountry.getCountry()].calendar.monthNames,
                            monthNamesShort: i18nGlobals[myCountry.getCountry()].calendar.monthNamesShort,
                            format: 'mm/YYYY',
                        });
                        jQuery('#datepicker'.concat(index1 + 1)).datepicker('setDate', currentDate);

                        jQuery('#datepicker'.concat(index1 + 1)).find(".ui-state-active").removeClass('ui-state-active');
                    });

                    this.clearMultipleDates(receivingDates[key], index1);
                    this.setMultipleDates(receivingDates[key], index1);
                });
            },
        });
    }

    fetchCalendarInformations() {
        $.ajax({
            url: `/${myCountry.getCountry()}/infosku/calendar/infos/${AbstractInfoSku.getSkuID}`,
            type: 'GET',
            datatype: 'json',
            success: (response) => {
                jQuery('#calendar_data').show();
                jQuery('#calendar_load').attr("style", "display:none !important");

                const data = response.data;
                const calendarSkuID = data.calendarSkuID;
                const profile = jQuery('#userProfile').val();

                if (calendarSkuID !== null) {
                    jQuery('#sku_info_calendar_with_data').show();
                    jQuery('#sku_info_calendar_without_data').hide();

                    const { companyCalendars, receivingDates, calendarIntervalAvarage } = data;

                    jQuery('#info_sku_calendar_batch_of_purchases_in_days').html(calendarIntervalAvarage);

                    companyCalendars.forEach((companyCalendar) => {
                        const companyCalendarID = companyCalendar[0];
                        const companyCalendarDescription = companyCalendar[1];

                        let option = '';

                        if (companyCalendarID === parseInt(calendarSkuID, 10)) {
                            option = '<option value="' + companyCalendarID + '" selected>' + companyCalendarDescription + '</option>';
                        } else {
                            option = '<option value="' + companyCalendarID + '">' + companyCalendarDescription + '</option>';
                        }

                        jQuery("#info_sku_calendar_batch_of_purchases").append(option);
                    });

                    Object.keys(receivingDates).forEach((key, index1) => {
                        receivingDates[key].forEach((value, index2) => {
                            const currentDate = new Date(receivingDates[key][index2].value);

                            jQuery('#datepicker'.concat(index1 + 1)).datepicker({
                                dayNames: i18nGlobals[myCountry.getCountry()].calendar.daysOfWeek,
                                dayNamesMin: i18nGlobals[myCountry.getCountry()].calendar.daysOfWeekAbbr,
                                dayNamesShort: i18nGlobals[myCountry.getCountry()].calendar.daysOfWeekShort,
                                monthNames: i18nGlobals[myCountry.getCountry()].calendar.monthNames,
                                monthNamesShort: i18nGlobals[myCountry.getCountry()].calendar.monthNamesShort,
                                format: 'mm/YYYY',
                            });

                            jQuery('#datepicker'.concat(index1 + 1)).datepicker('setDate', currentDate);

                            jQuery('#datepicker'.concat(index1 + 1)).find(".ui-state-active").removeClass('ui-state-active');

                            if(profile == 'spdmPharmaceutical' || profile == 'spdmPlanner' )
                                jQuery('#datepicker'.concat(index1  + 1)).datepicker( "option", "disabled", true );

                        });

                        this.clearMultipleDates(receivingDates[key], index1);
                        this.setMultipleDates(receivingDates[key], index1);
                    });

                    jQuery('#info_sku_calendar_batch_of_purchases').change(() => {
                        jQuery('#info_sku_save_click').removeClass('disabled').addClass('pnl-btn-orange');

                        AbstractInfoSku.setEnableSaveButton = true;
                        const value = jQuery('#info_sku_calendar_batch_of_purchases').val();
                        this.vCalendar = value;
                    });
                } else {
                    jQuery('#sku_info_calendar_without_data').show();
                    jQuery('#sku_info_calendar_with_data').hide();
                }
            },
        });
    }

    clearMultipleDates(dates, index) {
        dates.forEach((date) => {
            jQuery('#datepicker'.concat(index + 1)).find("tbody tr").each(function() {
                jQuery(this).find("td").each(function() {
                    jQuery(this).find("a").css(
                        {"background-color":"#ececec", "color": "#363636"}
                    );
                })
            });
        });
    }

    setMultipleDates(dates, index) {
        dates.forEach((date) => {
            const currentDate = new Date(date.value);
            const day = currentDate.getUTCDate();

            jQuery('#datepicker'.concat(index + 1)).find("tbody tr").each(function(){
                jQuery(this).find("td").each(function(){
                    if (jQuery(this).find("a").html() == day && date.haveDay){
                        jQuery(this).find("a").css(
                            {"background-color":"#f8543a", "color": "white"}
                        );
                    }
                })
            });
        });
    }
}
