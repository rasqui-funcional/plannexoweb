class interdependenceController extends AbstractInfoSku {
    constructor() {
        super();
        this.requestData = {};
        this.listenInfoTabClick();
        this.fetchInterdependenceByTabClick();
        this.listenChangeOriginItemCode();
        this.mapSaveClick();
        this.listenRemoveClick();

        utilsInstance.onlyNumbers('#interdep_percetil');

        utilsInstance.biggerThenZero('#interdep_percetil');
        utilsInstance.maxNumber('#interdep_percetil', 100);
    }

    mapSaveClick() {
        jQuery('#info_sku_save_click').click(() => {
            if (this.isSaveAction()) {
                this.mekeUpdateRequest();
            }
        });
    }

    mekeUpdateRequest() {
        if (AbstractInfoSku.getEnableSaveButton &&
            //this.requiredFieldsCompleted() &&
            this.isValidDate() &&
            this.isValidPercent() &&
            Object.keys(this.requestData).length !== 0) {

            jQuery('#info_sku_save_click').addClass('disabled').removeClass('pln-btn-orange');
            jQuery('#info_sku_save_click').html(i18nGlobals[myCountry.getCountry()].saving);

            jQuery('#interdependence-content').hide();
            jQuery('#interdependence_loader').attr("style", "display:flex !important");

            this.prepareRequestData();

            $.ajax({
                url: `/${myCountry.getCountry()}/infosku/interdependence/update/${AbstractInfoSku.getSkuID}`,
                type: 'POST',
                datatype: 'json',
                data: this.requestData,
                success: () => {
                    jQuery('#info_sku_save_click').html(`<i class="fa fa-save" aria-hidden="true"></i> ${i18nGlobals[myCountry.getCountry()].saveAlterations}`);
                    jQuery('.save-sku-success').removeClass('d-none');
                    AbstractInfoSku.setEnableSaveButton = false;

                    jQuery('#interdependence_loader').attr("style", "display:none !important");
                    jQuery('#interdependence-content').show();

                    const ids = [
                        '#sku_info_interdependence_cod_origin',
                        '#sku_info_interdependence_estab_select',
                        '#interdep_expiration',
                        '#interdep_percetil'
                    ];

                    ids.forEach((id) => {
                        jQuery(id).css('border', 'solid 1px #dde2ec');
                    });
                    setTimeout(function () {
                        jQuery('.save-sku-success').addClass('d-none');
                    }, 5000);
                }
            });
        }
    }

    isSaveAction() {
        const ids = [
            '#sku_info_interdependence_cod_origin',
            '#sku_info_interdependence_estab_select',
            '#interdep_expiration',
            '#interdep_percetil'
        ];

        if (this.existOneFieldFilled(ids)) {
            return true;
        }

        return false;
    }

    existOneFieldFilled(ids) {
        let result = false;

        ids.forEach((id) => {
            const value = jQuery(id).val() || '';

            if (value.length > 0) {
                result = true;
                return true;
            }
        });

        return result;
    }

    prepareRequestData() {
        this.requestData['COD_ITEM_PK'] = jQuery('#sku_info_interdependence_cod_origin').val() || '';
        this.requestData['COD_ESTAB_PK'] = jQuery('#sku_info_interdependence_estab_select').val() || '';
        this.requestData['V_DEPEND_DATE_VALID'] = jQuery('#interdep_expiration').val() || '';
        this.requestData['V_DEPEND_PERC_OUT'] = jQuery('#interdep_percetil').val() || '';
        this.requestData['check'] = jQuery('#chk-profile').is(":checked");
        this.requestData['check_stock'] = jQuery('#chk-stock').is(":checked");
    }

    listenInfoTabClick() {
        jQuery('#sku_info_interdependence_estab_select').empty();
        jQuery('#sku_info_interdependence_policy_of_origin').empty();
        jQuery('#interdep_expiration_error_message').hide();

        const eachRow = jQuery('#information_tab');
        eachRow.on('click', () => {
            const ids = [
                '#sku_info_interdependence_cod_origin',
                '#sku_info_interdependence_estab_select',
                '#interdep_expiration',
                '#interdep_percetil'
            ];

            ids.forEach((id) => {
                jQuery(id).css('border', 'solid 1px #dde2ec');
                jQuery(id).val('');
            });
        });
    }


    requiredFieldsCompleted() {
        const ids = [
            '#sku_info_interdependence_cod_origin',
            '#sku_info_interdependence_estab_select',
            '#interdep_expiration',
            '#interdep_percetil'
        ];

        const notExistBlankFields = this.checkIfAllFieldsNotAreBlank(ids)

        if (!notExistBlankFields) {
            ids.forEach((id) => {
                const value = jQuery(id).val() || '';

                if (value.length === 0) {
                    jQuery(id).css('border', 'solid 1px #F32938');

                    const scroll = jQuery('#modal_info');
                    const height = scroll.prop('scrollHeight');
                    jQuery('#modal_info').animate({ scrollTop: height }, 1000);
                } else {
                    jQuery(id).css('border', 'solid 1px #dde2ec');
                }
            });

            const size = jQuery('#sku_info_interdependence_estab_select option').length;
            if (size === 0) {
                jQuery('#sku_info_interdependence_estab_select').css('border', 'solid 1px #F32938');
            } else {
                jQuery('#sku_info_interdependence_estab_select').css('border', 'solid 1px #dde2ec');
            }
        }

        return notExistBlankFields;
    }

    checkIfAllFieldsNotAreBlank(ids) {
        let result = true;

        ids.forEach((id) => {
            const value = jQuery(id).val() || '';
            if (value.length === 0) {
                result = false;
                return false;
            }
        });

        return result;
    }

    fetchInterdependenceByTabClick() {
        const eachRow = jQuery('#information_tab');

        eachRow.on('click', (e) => {
            jQuery('#interdependence-content').hide();
            jQuery('#interdependence_loader').attr("style", "display:flex !important");
            this.getInterdependence();
        });
    }

    getInterdependence() {
        $.ajax({
            url: `/${myCountry.getCountry()}/infosku/interdependence/${AbstractInfoSku.getSkuID}`,
            type: "GET",
            dataType: "json",
            success: (response) => {
                const data = response.data;

                if (data !== null && data !== false) {
                    this.setInterdependenceInfo(data);
                } else {
                    jQuery('#interdep_expiration').val('');
                }

                this.listenInterdependenceInfo();

                jQuery('#interdependence-content').show();
                jQuery('#interdependence_loader').attr("style", "display:none !important");
            }
        });
    }

    setInterdependenceInfo(data) {
        jQuery('#sku_info_interdependence_cod_origin').val(data.COD_ITEM_PK);

        this.buildEstablishmentOfOrigin(data.DESC_ESTAB, data.COD_ITEM_PK);

        jQuery('#interdep_expiration').val(this.buildCustomDate(data.V_DEPEND_DATE_VALID));

        jQuery('#interdep_percetil').val(data.V_DEPEND_PERC_OUT);

        if (data.V_DEPEND_PROFILE_FLAG === '1') {
            jQuery('#chk-profile').attr("checked", "checked");
        }

        if (data.V_DEPEND_SUM_INV === '1') {
            jQuery('#chk-stock').attr("checked", "checked");
        }

        jQuery('#sku_info_interdependence_policy_of_origin').html(data.COD_PROFILE);
    }

    buildEstablishmentOfOrigin(descEstab, codItemPK) {
        $.ajax({
            url: `/${myCountry.getCountry()}/infosku/interdependence/establishment_origin/${codItemPK}`,
            type: 'GET',
            dataType: 'json',
            success: (response) => {
                const establishmentOrigins = response.data;

                jQuery('#sku_info_interdependence_estab_select').empty();
                jQuery('#sku_info_interdependence_estab_select').append('<option></option>');
                establishmentOrigins.forEach((establishmentOrigin) => {
                    let option = null;

                    if (establishmentOrigin.COD_ESTAB_FK + ' - ' + descEstab === establishmentOrigin.DESC_ESTAB) {
                        option = '<option value="'+ establishmentOrigin.COD_ESTAB_FK +'" selected>' + establishmentOrigin.DESC_ESTAB + '</option>';
                    } else {
                        option = '<option value="'+ establishmentOrigin.COD_ESTAB_FK +'">' + establishmentOrigin.DESC_ESTAB + '</option>';
                    }

                    jQuery("#sku_info_interdependence_estab_select").append(option);
                });
            }
        });
    }

    listenInterdependenceInfo() {
        this.listenCodItemProduto();
        this.listenEstablishmentOfOrigin();
        this.listenInterdepExpiration();
        this.listenExpirationDateEvents();
        this.listenInterdepPercetil();
        this.listenInheritPolitics();
        this.listenInheritStock();
    }

    listenInheritPolitics() {
        this.listenChangeEvents('#chk-profile', 'check', 'change');
    }

    listenInheritStock() {
        this.listenChangeEvents('#chk-stock', 'check', 'change');
    }

    listenInterdepPercetil() {
        this.listenChangeEvents('#interdep_percetil', 'V_DEPEND_PERC_OUT', 'keyup');
    }

    listenInterdepExpiration() {
        this.listenChangeEvents(
            '#interdep_expiration',
            'V_DEPEND_DATE_VALID',
            'keyup'
        );
    }

    listenEstablishmentOfOrigin() {
        this.listenChangeEvents(
            '#sku_info_interdependence_estab_select',
            'COD_ESTAB_PK',
            'keyup'
        );
    }

    listenCodItemProduto() {
        this.listenChangeEvents(
            '#sku_info_interdependence_cod_origin',
            'COD_ITEM_PK',
            'keyup'
        );
    }

    listenChangeOriginItemCode() {
        const eachRow = jQuery('#sku_info_interdependence_cod_origin');

        eachRow.keyup(() => {
            const codItemPK = jQuery('#sku_info_interdependence_cod_origin').val();

            $.ajax({
                url: `/${myCountry.getCountry()}/infosku/interdependence/establishment_origin/${codItemPK}`,
                type: 'GET',
                dataType: 'json',
                success: (response) => {
                    const establishmentOrigins = response.data;

                    jQuery("#sku_info_interdependence_estab_select").empty();
                    establishmentOrigins.forEach((establishmentOrigin) => {
                        const option = '<option value="'+ establishmentOrigin.COD_ESTAB_FK +'">' + establishmentOrigin.DESC_ESTAB + '</option>';
                        jQuery("#sku_info_interdependence_estab_select").append(option);
                    });

                    if (establishmentOrigins.length > 0) {
                        jQuery('#sku_info_interdependence_estab_select').css('border', 'solid 1px #dde2ec');
                    }
                }
            });
        });
    }

    listenChangeEvents(id, attribute, listeningActionType) {
        jQuery(id).on(listeningActionType, () => {
            jQuery('#info_sku_save_click').removeClass('disabled');
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery(id).val() || '';
            this.requestData[attribute] = value;

            if (value.length > 0) {
                jQuery(id).css('border', 'solid 1px #dde2ec');
            }
        });
    }

    listenExpirationDateEvents() {
        jQuery('#interdep_expiration').blur(() => {
            jQuery('#info_sku_save_click').removeClass('disabled');
            AbstractInfoSku.setEnableSaveButton = true;
            const value = jQuery('#interdep_expiration').val() || '';
            this.requestData['V_DEPEND_DATE_VALID'] = value;

            this.isValidDate();
        });
    }

    isValidDate() {
        const date = jQuery('#interdep_expiration').val();

        const expirationDate = this.buildDate(date);
        const todayDate = this.currentDate();
        let isValidDate = true;

        if (expirationDate < todayDate) {
            jQuery('#interdep_expiration').css('border', 'solid 1px #F32938');
            jQuery('#interdep_expiration_error_message').show();
            isValidDate = false;

            const scroll = jQuery('#modal_info');
            const height = scroll.prop('scrollHeight');
            jQuery('#modal_info').animate({ scrollTop: height }, 1000);
        } else {
            jQuery('#interdep_expiration').css('border', 'solid 1px #dde2ec');
            jQuery('#interdep_expiration_error_message').hide();
        }

        return isValidDate;
    }

    isValidPercent() {
        const percent = jQuery('#interdep_percetil').val();

        if(percent && percent > 0 && percent <= 100)
        {
            return true;
        }
        return false;
    }

    buildDate(date) {
        const formatedDate = this.formatStringDate(date);
        const currentDate = this.formatDate(formatedDate);

        return currentDate;
    }

    formatStringDate(data) {
        var dia  = data.split("/")[0];
        var mes  = data.split("/")[1];
        var ano  = data.split("/")[2];

        return ano + '/' + ("0"+mes).slice(-2) + '/' + ("0"+dia).slice(-2);
    }

    currentDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        return this.formatDate(yyyy + '/' + mm + '/' + dd);
    }

    formatDate(date) {
        const currentDate = new Date(date);
        return currentDate;
    }

    buildCustomDate(date) {
        var currentDate = new Date(date);

        if (typeof date === 'undefined') {
            currentDate = new Date();
        }

        // console.log(currentDate)

        var dd = currentDate.getDate();
        var mm = currentDate.getMonth() + 1;
        var yyyy = currentDate.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        return dd + '/' + mm + '/' + yyyy;
    }

    listenRemoveClick() {
        jQuery('#sku_info_interdependence_remove_button').on('click', () => {
            $.ajax({
                url: `/${myCountry.getCountry()}/infosku/interdependence/delete/${AbstractInfoSku.getSkuID}`,
                type: 'DELETE',
                dataType: 'json',
                success: () => {
                    jQuery("#sku_info_interdependence_estab_select").empty();

                    const ids = [
                        '#sku_info_interdependence_cod_origin',
                        '#sku_info_interdependence_estab_select',
                        '#interdep_expiration',
                        '#interdep_percetil'
                    ];

                    ids.forEach((id) => {
                        jQuery(id).css('border', 'solid 1px #dde2ec');
                        jQuery(id).val('');
                    });

                    jQuery('#chk-profile').prop('checked', false);
                    jQuery('#chk-stock').prop('checked', false);
                }
            });
        });
    }
}
