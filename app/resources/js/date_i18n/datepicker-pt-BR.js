( function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define( [ "../widgets/datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}( function( datepicker ) {

datepicker.regional[ myCountry.getLanguage() ] = {
	closeText: i18nGlobals[myCountry.getCountry()].close,
	prevText: `&#x3C;${i18nGlobals[myCountry.getCountry()].prev}`,
	nextText: `${i18nGlobals[myCountry.getCountry()].next}&#x3E;`,
	currentText: i18nGlobals[myCountry.getCountry()].today,
	monthNames: i18nGlobals[myCountry.getCountry()].calendar.monthNames,
	monthNamesShort: i18nGlobals[myCountry.getCountry()].calendar.monthNamesShort,
	dayNames: i18nGlobals[myCountry.getCountry()].calendar.daysOfWeek,
	dayNamesShort: i18nGlobals[myCountry.getCountry()].calendar.daysOfWeekShort,
	dayNamesMin: i18nGlobals[myCountry.getCountry()].calendar.daysOfWeekAbbr,
	weekHeader: "Sm",
	dateFormat: "dd/mm/yy",
	firstDay: 0,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: "" };
datepicker.setDefaults( datepicker.regional[ myCountry.getLanguage() ] );

return datepicker.regional[ myCountry.getLanguage() ];

} ) );