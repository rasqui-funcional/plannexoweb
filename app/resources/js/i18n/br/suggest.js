i18nSuggest['br'] = {
  sit_analysed:{
    abbr: "AN.",
    title: "Analisadas",
  },
  sit_saved: {
    abbr: "AP.",
    title: "Aprovadas",
  },
  id_psug_pk: {
    abbr: "ID Nec.",
    title: "ID da Necessidade",
  },
  num_parc_pk: {
    abbr: "Sug.",
    title: "Sugestão",
  },
  ind_excess: {
    abbr: "Disp. Mov.",
    title: "Disponível para Movimentação",
  }, 
  cod_item_pk: {
    abbr: "Cód. Item",
    title: "Código do Item",
  }, 
  desc_item: {
    abbr: "Desc. Item",
    title: "Descrição do item",
  }, 
  cod_estab_fk: {
    abbr: "Estab.",
    title: "Estabelecimento",
  }, 
  desc_std_unit: {
    abbr: "Un.",
    title: "Unidade",
  }, 
  qty_original: {
    abbr: "Qtd.",
    title: "Quantidade",
  }, 
  parc_days: {
    abbr: "Cob. Sug.(D)",
    title: "Cobertura da Sugestão (Dias)",
  }, 
  qty_altered: {
    abbr: "Qtd. Alt.",
    title: "Quantidade Alterado",
  }, 
  qty_mult_order: {
    abbr: "Qtd. Emb.",
    title: "Quantidade da Embalagem",
  }, 
  date_arrival_altered: {
    abbr: "Dt. Alt. Chegada",
    title: "Data Alterada de Chegada",
  }, 
  out_last_30d: {
    abbr: "Cons. Últ. 30 D",
    title: "Consumo Últimos 30 dias",
  }, 
  val_altered: {
    abbr: "Val. Sug.(R$)",
    title: "Valor da Sugestão (R$)",
  }, 
  risk_supplier: {
    abbr: "Dias Atraso",
    title: "Dias de Atraso",
  }, 
  default_local: {
    abbr: "Dep. Padrão",
    title: "Depósito Padrão",
  }, 
  obs: {
    abbr: "Obs.",
    title: "Observação",
  }, 
  inv_level: {
    abbr: "Sin.",
    title: "Sinalizador",
  }, 
  id_sku_pk: {
    abbr: "Cód. SKU",
    title: "Código do SKU",
  }, 
  val_unit_erp: {
    abbr: "Val. Unit. ERP (R$)",
    title: "Valor Unitário no ERP (R$)",
  }, 
  qty_forecast: {
    abbr: "Prev. Cons.",
    title: "Previsão de Consumo",
  }, 
  inv_available: {
    abbr: "Sal. Est. Disp.(UN.)",
    title: "Saldo Estoque Disponível (UN.)",
  }, 
  available_money: {
    abbr: "Sal. Est. Disp.(R$)",
    title: "Saldo Estoque Disponível (R$)",
  },
  qty_min_out: {
    abbr: "Mín.",
    title: "Mínimo",
  },
  qty_target: {
    abbr: "Alvo",
    title: "Alvo",
  },
  qty_max_out: {
    abbr: "Máx.",
    title: "Máximo",
  },
  qty_avg_cons_erp: {
    abbr: "Cons. Méd. (ERP)",
    title: "Consumo Médio (ERP)",
  },
  avg_3m: {
    abbr: "Cons. Méd. 3 M",
    title: "Consumo Médio Mensal 3 meses",
  },
  avg_6m: {
    abbr: "Cons. Méd. 6 M",
    title: "Consumo Médio Mensal 6 meses",
  },
  avg_12m: {
    abbr: "Cons. Méd. 12 M",
    title: "Consumo Médio Mensal 12 meses",
  },
  out_yesterday: {
    abbr: "Cons. ontem",
    title: "Consumo ontem",
  },
  out_yesterday_num_dev: {
    abbr: "Núm.desv. ontem",
    title: "Número de desvios ontem",
  },
  days_no_out: {
    abbr: "Dias sem Cons.",
    title: "Dias sem Consumo",
  },
  reqs_next_in: {
    abbr: "Próx. Sol.",
    title: "Próximas Solicitações (Cód./Qtde/Data)",
  },
  curve_abc: {
    abbr: "ABC",
    title: "ABC",
  },
  curve_pqr: {
    abbr: "PQR",
    title: "PQR",
  },
  curve_xyz: {
    abbr: "XYZ",
    title: "XYZ",
  },
  curve_123: {
    abbr: "123",
    title: "123",
  },
  out1: {
    abbr: "Sug. Total",
    title: "Sugestão Total",
  },
  out2: {
    abbr: "Sug. Prox. Qtd. Ap.",
    title: "Sugestão - Proxima quantidade aprovada",
  },
  out3: {
    abbr: "Qtd. Últ. Ent.",
    title: "Quantidade Última Entrada",
  },
  out4: {
    abbr: "Qtd. Mês",
    title: "Quantidade Mês",
  },
  out5: {
    abbr: "Cob. Ajust.",
    title: "Cobertura Ajustad",
  },
  out6: {
    abbr: "Var. Cons. (%)",
    title: "Variação de Consumo (%)",
  },
  out7: {
    abbr: "Cons. Mês 07",
    title: "Consumo do Mês 07",
  },
  out8: {
    abbr: "Cons. Mês 08",
    title: "Consumo do Mês 08",
  },
  out9: {
    abbr: "Cons. Mês 09",
    title: "Consumo do Mês 09",
  },
  out10: {
    abbr: "Próx. Sug. Ap.",
    title: "Próxima Sugestão Aprovada",
  },
  out11: {
    abbr: "Dt. Últ. Ent.",
    title: "Data da Última Entrada",
  },
  out12: {
    abbr: "Cons. Mês 12",
    title: "Consumo do Mês 12",
  },
  qty_min_out_ori: {
    abbr: "Mín. Orig. (ERP)",
    title: "Mínimo original (ERP)",
  },
  qty_pp: {
    abbr: "Pt. Pedido",
    title: "Ponto de Pedido",
  },
  qty_max_out_ori: {
    abbr: "Máx. Orig. (ERP)",
    title: "Máximo original (ERP)",
  },
  qty_excess: {
    abbr: "Exc. Un. (ERP)",
    title: "Excesso da Unidade (ERP)",
  },
  vl_excess: {
    abbr: "Exc. (ERP) (R$)",
    title: "Excesso (ERP) (R$)",
  },
  qty_max_erp: {
    abbr: "Máximo (ERP)",
    title: "Máx. (ERP)",
  },
  qty_min_erp: {
    abbr: "Min. (ERP)",
    title: "Mínimo (ERP)",
  },
  id_profile: {
    abbr: "Política",
    title: "Política",
  },
  id_calendar_fk: {
    abbr: "Calend.",
    title: "Calendário",
  },
  date_added: {
    abbr: "Dt. Inc. Item",
    title: "Data de Inclusão do Item",
  },
  date_created: {
    abbr: "Dt. Criação Item",
    title: "Data de Criação do Item",
  },
  date_arrival_original: {
    abbr: "Dt. Ent. Orl.",
    title: "Data de entrada original",
  },
  id_user_created: {
    abbr: "Criado por",
    title: "Criado por",
  },
  qty_pp_erp: {
    abbr: "Pt. Ped. (ERP)",
    title: "Ponto de Pedido (ERP)",
  },
  qty_final: {
    abbr: "Qtd Final",
    title: "Quantidade Final",
  },
  date_next_in: {
    abbr: "Dt. Próx. Ent.",
    title: "Data da Próxima Entrada",
  },
  qty_next_in: {
    abbr: "Qtd. Próx. Ent.",
    title: "Quantidade da Próxima Entrada",
  },
  date_last_out: {
    abbr: "Dt. Últ. Cons.",
    title: "Data do último Consumo",
  },
  qty_last_out: {
    abbr: "Qtd. Últ. Cons.",
    title: "Quantidade do Último Consumo",
  },
  date_last_in: {
    abbr: "Dt. Últ. Ent.",
    title: "Data da Última Entrada",
  },
  qty_last_in: {
    abbr: "Qtd Últ. Ent.",
    title: "Quantidade da Última Entrada",
  },
  inv_total: {
    abbr: "Sal. Est. Total Un.",
    title: "Saldo Estoque Total em Unidades",
  },
  inv_days: {
    abbr: "Cob. (D)",
    title: "Cobertura (Dias)",
  },
  total_money: {
    abbr: "Sal. Est. Total (R$)",
    title: "Saldo Estoque Total (R$)",
  },
  purchase_req: {
    abbr: "Qtd Sol.",
    title: "Quantidade da Solicitação",
  },
  orders_next_in: {
    abbr: "Ord. Totais",
    title: "Quantidade Total de Ordens",
  },
  last_purchase_price: {
    abbr: "Vl. Últ. Compra (R$)",
    title: "Valor Últ. Compra (R$)",
  },
  last_purchase_supplier: {
    abbr: "Forn. Últ. Compra",
    title: "Fornecedor da Última Compra",
  },
  last_purchase_date: {
    abbr: "Dt. Últ. Compra",
    title: "Data da Última Compra",
  },
  last_purchase_qty: {
    abbr: "Qtd. Últ. Compra",
    title: "Quantidade da Última Compra",
  },
  lt_supplier_out: {
    abbr: "L. T. Forn. (Auto.)",
    title: "Lead Time Fornecedor (Automático)",
  },
  lt_others_out: {
    abbr: "Outros L. T. (Auto.)",
    title: "Outros Lead Times (Automático)",
  },
  lt_buyer_out: {
    abbr: "L. T. (Auto.)",
    title: "Lead Time (Automático)",
  },
  lt_total_out: {
    abbr: "L. T. Total (Outros)",
    title: "Lead Time Total (Outros",
  },
  qty_min_order: {
    abbr: "Qtd. Fat.",
    title: "Quantidade Faturada",
  },
  service_level_target: {
    abbr: "Nvl. Serv. Alvo",
    title: "Nível de Serviço Alvo",
  },
  service_level_real: {
    abbr: "Nvl. Serv. Atual",
    title: "Nível de Serviço Atual",
  },
  date_mature: {
    abbr: "Dt. Mat",
    title: "Data de Maturidade",
  },
  sku_life_cycle: {
    abbr: "Ciclo de vida",
    title: "Ciclo de vida",
  },
  user_id: {
    abbr: "Gestor",
    title: "Gestor",
  },
  date_last_calc: {
    abbr: "Últ. Cálc.",
    title: "Último Cálculo",
  },
  sit_pending_calc: {
    abbr: "Cálc. pend.",
    title: "Cálculo pendente",
  },
  v_qty_cons: {
    abbr: "Consumo (manual)",
    title: "Consumo (manual)",
  },
  v_date_cons_expiration: {
    abbr: "Dt. Exp. Cons. (Manual)",
    title: "Data de Expiração de Consumo (Manual)",
  },
  v_lt_planner_in: {
    abbr: "L. T. Plan.",
    title: "Lead Time de Planejamento",
  },
  v_lt_purchase_in: {
    abbr: "L. T. Compra",
    title: "Lead Time da Compra",
  },
  v_lt_supplier_in: {
    abbr: "L. T. Forn.",
    title: "Lead Time do Fornecedor",
  },
  v_lt_receiving_in: {
    abbr: "L. T. Chegada",
    title: "Lead Time de Chegada",
  },
  v_lt_others_in: {
    abbr: "L. T. Outros",
    title: "Lead Time Outros",
  },
  v_qty_min_in: {
    abbr: "Mín. (Entrada)",
    title: "Mínimo (Entrada)",
  },
  v_qty_max_in: {
    abbr: "Máx. (Entrada)",
    title: "Máximo (Entrada)",
  },
  v_qty_max_restr: {
    abbr: "Inativo",
    title: "Inativo",
  },
  v_cons_profile: {
    abbr: "Perf. Cons.",
    title: "Perfil de Consumo",
  },
  v_id_depend_sku: {
    abbr: "Item Interdep.",
    title: "Item Interdependente",
  },
  v_order_min: {
    abbr: "Lote Minimo",
    title: "Lote Minimo",
  },
  v_order_mult: {
    abbr: "Lote Múltiplo",
    title: "Lote Múltiplo",
  },
  v_in1: {
    abbr: "Vol. ent. 1",
    title: "Volume de entrada 1",
  },
  v_in2: {
    abbr: "Vol. ent. 2",
    title: "Volume de entrada 2",
  },
  v_in3: {
    abbr: "Vol. ent. 3",
    title: "Volume de entrada 3",
  },
  v_in4: {
    abbr: "Vol. ent. 4",
    title: "Volume de entrada 4",
  },
  v_in5: {
    abbr: "Vol. ent. 5",
    title: "Volume de entrada 5",
  },
  v_in6: {
    abbr: "Vol. ent. 6",
    title: "Volume de entrada 6",
  },
  v_in7: {
    abbr: "Vol. ent. 7",
    title: "Volume de entrada 7",
  },
  v_in8: {
    abbr: "Vol. ent. 8",
    title: "Volume de entrada 8",
  },
  v_in9: {
    abbr: "Vol. ent. 9",
    title: "Volume de entrada 9",
  },
  v_in10: {
    abbr: "Vol. ent. 10",
    title: "Volume de entrada 10",
  },
  v_in11: {
    abbr: "Vol. ent. 11",
    title: "Volume de entrada 11",
  },
  v_in12: {
    abbr: "Vol. ent. 12",
    title: "Volume de entrada 12",
  },
  out_week_month_percent_avg: {
    abbr: "Var. CMD 7 X CMD 30",
    title: "Variação CMD 7 x CMD 30",
  },
  total_out_last_week: {
    abbr: "Cons. Últ. 7 D",
    title: "Consumo Últimos 7 dias",
  },
  no_variation: "Sem Variação",
  cmd30:"CMD 30",
  cmd15: "CMD 15",
  cmd7: "CMD 7",
  no_consumption_variation: "SEM VARIAÇÃO DE CONSUMO",
  increase_consumption: "AUMENTO DE CONSUMO",
  consumption_reduction:"REDUÇÃO DE CONSUMO",
  more_info: 'Saiba mais',
  group_suggests_1_2: 'Agrupar parcelas 1 e 2',
  yes: 'Sim',
  no: 'Não',
  grouped_suggest: "Sugestão agrupada",
}
