i18nManualSuggest['br'] = {
    addedSuccess: 'Sugestões adicionadas com sucesso',
    dateArrivalAlteredExists: 'Não é possível salvar itens iguais com a mesma data de entrega',
    invalidField: 'Campo obrigatório',
    delete: 'Excluir',
    greaterThanZero: 'Maior que 0'
}
