i18nPolice['br'] = {
    modalRemoveItem: {
        title: 'Você tem certeza que deseja deletar essa Política?',
    },
    errorOnDelete: "Não é possível deletar uma Poítica que possui SKU's associadas",
    requiredPolice: 'Por favor, escolha pelo menos uma política.',
    successOnDuplicate: 'Duplicado com sucesso!',
    selectAtLeastOneDeposit: 'Selecione ao menos um depósito',
    newPolicyTitle: 'Nova Política',
    editPolicyTitle: 'Editar Política',
}
