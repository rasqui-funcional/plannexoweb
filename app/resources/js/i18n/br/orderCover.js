i18nOrderCover['br'] = {
    group: {
        selectItems: 'Selecione os itens que deseja agrupar',
        selectError: 'Não é permitido agrupar diferentes estabelecimentos',
    },
    unGroup: {
        title: 'Desagrupar',
        selectItems: 'Selecione o item a ser alterado',
        modalRemoveItem: {
            title: 'Desagrupar os itens selecionados?',
        },
    },
    approve: {
        selectItems: 'Selecione os itens que deseja aprovar',
    },
    sendedToERP: 'Solicitação enviada para o ERP',
    invalidSuggestions: 'Existem sugestões com quantidades zeradas ou datas iguais para mesmo produto que não podem ser aprovadas',
}
