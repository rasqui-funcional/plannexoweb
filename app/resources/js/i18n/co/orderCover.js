i18nOrderCover['co'] = {
    group: {
        selectItems: 'Seleccione los ítems que desea agrupar',
        selectError: 'No está permitido agrupar diferentes establecimientos',
    },
    unGroup: {
        title: 'Desagrupar',
        selectItems: 'Seleccione el ítem a cambiar',
        modalRemoveItem: {
            title: '¿Desagrupar los ítems seleccionados?',
        },
    },
    approve: {
        selectItems: 'Seleccione los ítems que desea aprobar',
    },
    sendedToERP: 'Solicitud enviada al ERP',
    invalidSuggestions: 'Hay Solicitud con cantidades cerradas o fechas iguales para el mismo producto que no pueden aprobarse',
}
