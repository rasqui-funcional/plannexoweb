i18nManualSuggest['ar'] = {
    addedSuccess: '¡Sugerencias agregadas correctamente!',
    dateArrivalAlteredExists: 'No se pueden guardar ítems iguales con la misma fecha de entrega',
    invalidField: 'Campo obligatorio',
    delete: 'Eliminar',
    greaterThanZero: 'Mayor que 0'
}
