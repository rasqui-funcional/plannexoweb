i18nSuggest['ar'] = {
  sit_analysed:{
    abbr: "AN.",
    title: "Analizadas",
  },
  sit_saved: {
    abbr: "AP.",
    title: "Aprobadas",
  },
  id_psug_pk: {
    abbr: "ID Nec.",
    title: "ID de la Necesidad",
  },
  num_parc_pk: {
    abbr: "Sug.",
    title: "Sugerencia",
  },
  ind_excess: {
    abbr: "Disp. Mov.",
    title: "Disponible para mudanza",
  }, 
  cod_item_pk: {
    abbr: "Cód. Item",
    title: "Código del Ítem",
  }, 
  desc_item: {
    abbr: "Desc. Ítem",
    title: "Descripción del ítem",
  }, 
  cod_estab_fk: {
    abbr: "Estab.",
    title: "Establecimiento",
  }, 
  desc_std_unit: {
    abbr: "Un.",
    title: "Unidad",
  }, 
  qty_original: {
    abbr: "Ctd.",
    title: "Cantidad",
  }, 
  parc_days: {
    abbr: "Cob. Sug.(D)",
    title: "Cobertura de la Sugerencia (Días)",
  }, 
  qty_altered: {
    abbr: "Ctd. Alt.",
    title: "Cantidad Alterada",
  }, 
  qty_mult_order: {
    abbr: "Ctd. Emb.",
    title: "Cantidad del Embalaje",
  }, 
  date_arrival_altered: {
    abbr: "Fech. Alt. Llegada",
    title: "Fecha Alterada de Llegada",
  }, 
  out_last_30d: {
    abbr: "Cons. Últ. 30 D",
    title: "Consumo Últimos 30 días",
  }, 
  val_altered: {
    abbr: "Val. Sug.($)",
    title: "Valor de la Sugerencia ($)",
  }, 
  risk_supplier: {
    abbr: "Días Retraso",
    title: "Días de Retraso",
  }, 
  default_local: {
    abbr: "Dep. Estándar",
    title: "Depósito Estándar",
  }, 
  obs: {
    abbr: "Obs.",
    title: "Observación",
  }, 
  inv_level: {
    abbr: "Señ.",
    title: "Señalizador",
  }, 
  id_sku_pk: {
    abbr: "Cód. SKU",
    title: "Código del SKU",
  }, 
  val_unit_erp: {
    abbr: "Val. Unit. ERP ($)",
    title: "Valor Unitario en el ERP ($)",
  }, 
  qty_forecast: {
    abbr: "Prev. Cons.",
    title: "Previsión de Consumo",
  }, 
  inv_available: {
    abbr: "Sal. Est. Disp.(UN.)",
    title: "Saldo Stock Disponible (UN.)",
  }, 
  available_money: {
    abbr: "Sal. Est. Disp.($)",
    title: "Saldo Stock Disponible ($)",
  },
  qty_min_out: {
    abbr: "Mín.",
    title: "Mínimo",
  },
  qty_target: {
    abbr: "Objetivo",
    title: "Objetivo",
  },
  qty_max_out: {
    abbr: "Máx.",
    title: "Máximo",
  },
  qty_avg_cons_erp: {
    abbr: "Cons. Med. (ERP)",
    title: "Consumo Medio (ERP)",
  },
  avg_3m: {
    abbr: "Cons. Med. 3 M",
    title: "Consumo Medio Mensual 3 meses",
  },
  avg_6m: {
    abbr: "Cons. Med. 6 M",
    title: "Consumo Medio Mensual 6 meses",
  },
  avg_12m: {
    abbr: "Cons. Med. 12 M",
    title: "Consumo Medio Mensual 12 meses",
  },
  out_yesterday: {
    abbr: "Cons. ayer",
    title: "Consumo ayer",
  },
  out_yesterday_num_dev: {
    abbr: "Núm.desv. ayer",
    title: "Número de desviaciones ayer",
  },
  days_no_out: {
    abbr: "Días sin Cons.",
    title: "Días sin Consumo",
  },
  reqs_next_in: {
    abbr: "Próx. Sol.",
    title: "Próximas Solicitudes",
  },
  curve_abc: {
    abbr: "ABC",
    title: "ABC",
  },
  curve_pqr: {
    abbr: "PQR",
    title: "PQR",
  },
  curve_xyz: {
    abbr: "XYZ",
    title: "XYZ",
  },
  curve_123: {
    abbr: "123",
    title: "123",
  },
  out1: {
    abbr: "Sug. Total",
    title: "Sugerencia Total",
  },
  out2: {
    abbr: "Sug. Prox. Ctd. Ap.",
    title: "Sugerencia de la Próxima Cantidad Aprobada",
  },
  out3: {
    abbr: "Ctd. Ult. Ent. Alm.",
    title: "Cantidad de la Última Entrada Alm.",
  },
  out4: {
    abbr: "Ctd. M.",
    title: "Cantidad Mes",
  },
  out5: {
    abbr: "Cob. Ajust.",
    title: "Cobertura Ajustada",
  },
  out6: {
    abbr: "Var. Cons. (%)",
    title: "Variable de Consumo (%)",
  },
  out7: {
    abbr: "Cons. Mes 07",
    title: "Consumo del Mes 07",
  },
  out8: {
    abbr: "Cons. Mes 08",
    title: "Consumo del Mes 08",
  },
  out9: {
    abbr: "Cons. Mes 09",
    title: "Consumo del Mes 09",
  },
  out10: {
    abbr: "Próx. Sug. Ap.",
    title: "Próxima Sugerencia Aprobada",
  },
  out11: {
    abbr: "Fech. Últ. Ent.",
    title: "Fecha de la Última Entrada",
  },
  out12: {
    abbr: "Cons. Mes 12",
    title: "Consumo del Mes 12",
  },
  qty_min_out_ori: {
    abbr: "Mín. Orig. (ERP)",
    title: "Mínimo original (ERP)",
  },
  qty_pp: {
    abbr: "Pt. Pedido",
    title: "Punto de Pedido",
  },
  qty_max_out_ori: {
    abbr: "Máx. Orig. (ERP)",
    title: "Máximo original (ERP)",
  },
  qty_excess: {
    abbr: "Exc. Un. (ERP)",
    title: "Exceso de la Unidad (ERP)",
  },
  vl_excess: {
    abbr: "Exc. (ERP) ($)",
    title: "Exceso (ERP) ($)",
  },
  qty_max_erp: {
    abbr: "Máximo (ERP)",
    title: "Máx. (ERP)",
  },
  qty_min_erp: {
    abbr: "Min. (ERP)",
    title: "Mínimo (ERP)",
  },
  id_profile: {
    abbr: "Política",
    title: "Política",
  },
  id_calendar_fk: {
    abbr: "Calend.",
    title: "Calendario",
  },
  date_added: {
    abbr: "Fech. Inc. Ítem",
    title: "Fecha de Inclusión del Ítem",
  },
  date_created: {
    abbr: "Fech. Creación Ítem",
    title: "Fecha de Creación del Ítem",
  },
  date_arrival_original: {
    abbr: "Fech. Ent. Orl.",
    title: "Fecha de entrada original",
  },
  id_user_created: {
    abbr: "Creado por",
    title: "Creado por",
  },
  qty_pp_erp: {
    abbr: "Pt. Ped. (ERP)",
    title: "Punto de Pedido (ERP)",
  },
  qty_final: {
    abbr: "Ctd Final",
    title: "Cantidad Final",
  },
  date_next_in: {
    abbr: "Fech. Próx. Ent.",
    title: "Fecha de la Próxima Entrada",
  },
  qty_next_in: {
    abbr: "Ctd. Próx. Ent.",
    title: "Cantidad de la Próxima Entrada",
  },
  date_last_out: {
    abbr: "Fech. Últ. Cons.",
    title: "Fecha del último Consumo",
  },
  qty_last_out: {
    abbr: "Ctd. Últ. Cons.",
    title: "Cantidad del Último Consumo",
  },
  date_last_in: {
    abbr: "Fech. Últ. Ent.",
    title: "Fecha de la Última Entrada",
  },
  qty_last_in: {
    abbr: "Ctd Últ. Ent.",
    title: "Cantidad de la Última Entrada",
  },
  inv_total: {
    abbr: "Sal. Est. Total Un.",
    title: "Saldo Stock Total en Unidades",
  },
  inv_days: {
    abbr: "Cob. (D)",
    title: "Cobertura (Días)",
  },
  total_money: {
    abbr: "Sal. Est. Total ($)",
    title: "Saldo Stock Total ($)",
  },
  purchase_req: {
    abbr: "Ctd Sol.",
    title: "Cantidad de la Solicitud",
  },
  orders_next_in: {
    abbr: "Órd. total",
    title: "Cantidad total de pedido",
  },
  last_purchase_price: {
    abbr: "Vl. Últ. Compra ($)",
    title: "Valor Últ. Compra ($)",
  },
  last_purchase_supplier: {
    abbr: "Prov. Últ. Compra",
    title: "Proveedor de la Última Compra",
  },
  last_purchase_date: {
    abbr: "Fech. Últ. Compra",
    title: "Fecha de la Última Compra",
  },
  last_purchase_qty: {
    abbr: "Ctd. Últ. Compra",
    title: "Cantidad de la Última Compra",
  },
  lt_supplier_out: {
    abbr: "L. T. Prov. (Auto.)",
    title: "Lead Time Proveedor (Automático)",
  },
  lt_others_out: {
    abbr: "Otros L. T. (Auto.)",
    title: "Otros Lead Times (Automático)",
  },
  lt_buyer_out: {
    abbr: "L. T. (Auto.)",
    title: "Lead Time (Automático)",
  },
  lt_total_out: {
    abbr: "L. T. Total (Otros)",
    title: "Lead Time Total (Otros)",
  },
  qty_min_order: {
    abbr: "Ctd. Fact.",
    title: "Cantidad Facturada",
  },
  service_level_target: {
    abbr: "Nvl. Serv. Objetivo",
    title: "Nivel de Servicio Objetivo",
  },
  service_level_real: {
    abbr: "Nvl. Serv. Actual",
    title: "Nivel de Servicio Actual",
  },
  date_mature: {
    abbr: "Fech. Mat",
    title: "Fecha de Madurez",
  },
  sku_life_cycle: {
    abbr: "Ciclo de vida",
    title: "Ciclo de vida",
  },
  user_id: {
    abbr: "Gestor",
    title: "Gestor",
  },
  date_last_calc: {
    abbr: "Últ. Cálc.",
    title: "Último Cálculo",
  },
  sit_pending_calc: {
    abbr: "Cálc. pend.",
    title: "Cálculo pendiente",
  },
  v_qty_cons: {
    abbr: "Consumo (manual)",
    title: "Consumo (manual)",
  },
  v_date_cons_expiration: {
    abbr: "Fech. Exp. Cons. (Manual)",
    title: "Fecha Expiración Consumo (Manual)",
  },
  v_lt_planner_in: {
    abbr: "L. T. Plan.",
    title: "Lead Time Planificación",
  },
  v_lt_purchase_in: {
    abbr: "L. T. Compra",
    title: "Lead Time Compra",
  },
  v_lt_supplier_in: {
    abbr: "L. T. Prov.",
    title: "Lead Time Proveedor",
  },
  v_lt_receiving_in: {
    abbr: "L. T. Recep",
    title: "Lead Time Recepción",
  },
  v_lt_others_in: {
    abbr: "L. T. Otros",
    title: "Lead Time  Otros",
  },
  v_qty_min_in: {
    abbr: "Mín. (Entrada)",
    title: "Mínimo (Entrada)",
  },
  v_qty_max_in: {
    abbr: "Máx. (Entrada)",
    title: "Máximo (Entrada)",
  },
  v_qty_max_restr: {
    abbr: "Inactivo",
    title: "Inactivo",
  },
  v_cons_profile: {
    abbr: "Perfil Cons.",
    title: "Perfil de Consumo",
  },
  v_id_depend_sku: {
    abbr: "Ítem Interdep.",
    title: "Ítem Interdependiente",
  },
  v_order_min: {
    abbr: "Lote Mínimo",
    title: "Lote Mínimo",
  },
  v_order_mult: {
    abbr: "Lote Múltiple",
    title: "Lote Múltiple",
  },
  v_in1: {
    abbr: "Vol. ent. 1",
    title: "Volumen de entrada 1",
  },
  v_in2: {
    abbr: "Vol. ent. 2",
    title: "Volumen de entrada 2",
  },
  v_in3: {
    abbr: "Vol. ent. 3",
    title: "Volumen de entrada 3",
  },
  v_in4: {
    abbr: "Vol. ent. 4",
    title: "Volumen de entrada 4",
  },
  v_in5: {
    abbr: "Vol. ent. 5",
    title: "Volumen de entrada 5",
  },
  v_in6: {
    abbr: "Vol. ent. 6",
    title: "Volumen de entrada 6",
  },
  v_in7: {
    abbr: "Vol. ent. 7",
    title: "Volumen de entrada 7",
  },
  v_in8: {
    abbr: "Vol. ent. 8",
    title: "Volumen de entrada 8",
  },
  v_in9: {
    abbr: "Vol. ent. 9",
    title: "Volumen de entrada 9",
  },
  v_in10: {
    abbr: "Vol. ent. 10",
    title: "Volumen de entrada 10",
  },
  v_in11: {
    abbr: "Vol. ent. 11",
    title: "Volumen de entrada 11",
  },
  v_in12: {
    abbr: "Vol. ent. 12",
    title: "Volumen de entrada 12",
  },
  out_week_month_percent_avg: {
    abbr: "Var. CMD 7 x CMD 30",
    title: "Variación CMD 7 x CMD 30",
  },
  total_out_last_week: {
    abbr: "Cons. Últ. 7 D",
    title: "Consumo últimos 7 días",
  },
  no_variation: "Sin variación",
  cmd30:"CMD 30",
  cmd15: "CMD 15",
  cmd7: "CMD 7",
  no_consumption_variation: "SIN VARIACIÓN DE CONSUMO",
  increase_consumption: "AUMENTO DEL CONSUMO",
  consumption_reduction:"REDUCCIÓN DE CONSUMO",
  more_info: 'Sepa más',
  group_suggests_1_2: 'Agrupar entregas 1 y 2',
  yes: 'Sí',
  no: 'No',
  grouped_suggest: "Sugerencia agrupada",
}
