i18nPolice['pe'] = {
    modalRemoveItem: {
        title: '¿Está seguro de que desea eliminar esta Política?',
    },
    errorOnDelete: "No se puede eliminar una Política con SKU's asociadas",
    requiredPolice: 'Por favor, elige al menos una política.',
    successOnDuplicate: '¡Duplicado correctamente!',
    selectAtLeastOneDeposit: 'Seleccione al menos un depósito',
    newPolicyTitle: 'Nueva Política',
    editPolicyTitle: 'Editar Política',
}
