class MyCountry {
    constructor(){
        this.country = jQuery("#country").val();
        this.language = jQuery("#sysLocale").val();
        this.currency = jQuery("#currency").val();
    }

    getCountry(){
        return this.country;
    }
    getLanguage(){
        return this.language;
    }
    getCurrency(){
        return this.currency;
    }
}

const myCountry = new MyCountry();