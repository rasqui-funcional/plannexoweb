class editForm extends notificationSweetAlert {
    constructor() {
        super();
        this._form = document.getElementById('formulario')
        // variavel global filter_temp criada na view filters.php
        this._json = filter_temp
        this.arrErrorField = []
        this.formEdit()
        this.daterangepicker()
    }

    setJson(json) {
        // delete json.filter_temp
        this._json = json;
    }

    formEdit() {
        let json = JSON.parse(this._json)

        if (json.filter_id != undefined) {
            jQuery("#select_filtro").val(json.filter_id);
            jQuery("#btn_filtro_text").text("Editar filtro");
            jQuery("#modal_titulo").text("Edição de Filtro");
            jQuery("#btnfiltro_excluir").show();
            jQuery("#filter_nome").val(json.filter_nome);
        }

        for (let name in json) {
            if (typeof (json[name]) == 'object') {
                json[name].forEach(item => {
                    let field = document.getElementById(`${name}_${item}`)

                    try {
                        field.checked = true;
                    } catch (err) {
                        this.arrErrorField.push(`${name}_${item}`)
                        console.log(err)
                    }
                })
            }
            
            let field = this._form.querySelector(`[name="${name}"]`)

            if (field) {
                switch (field.type) {
                    case 'radio':
                        try {
                            field = this._form.querySelector(`[name="${name}"][value="${json[name]}"]`)
                            field.checked = true
                        } catch (e) {
                            this.arrErrorField.push(name)
                        }
                        break
                    default:
                        try {
                            field.value = json[name]
                            if (field.hasAttribute('disabled')) {
                                field.removeAttribute('disabled')
                            }
                        } catch (e) {
                            this.arrErrorField.push(name)
                        }
                }
            }
        }

        //jQuery('.select_date').change();
        //set the daterange on datepicker plugin
        try {
            this.setDatesOnEdit(
                this._json.date_req_order_cond,
                this._json.date_req,
                '#input_daterange_date_req')

            this.setDatesOnEdit(
                this._json.date_order_in_cond,
                this._json.date_order,
                '#input_daterange_date_order')

            this.setDatesOnEdit(
                this._json.date_exp_in_cond,
                this._json.date_exp,
                '#input_daterange_date_exp')
        } catch (e) {
            this.arrErrorField.push('datas')
        }

        if (this.arrErrorField.length > 0) {
            this.execSwal('', `Alguns campos não puderam ser preenchidos: ${this.arrErrorField}`, 'error')
        }
    }

    setDatesOnEdit(dateType, dateRange, inputId) {
        if (dateType == 'INTERVALO' && dateRange !== "") {
            const date = dateRange.split(" ")
            jQuery(inputId).data('daterangepicker').setStartDate(date[0]);
            jQuery(inputId).data('daterangepicker').setEndDate(date[2]);
        }
    }

    daterangepicker(){
        jQuery('.order_daterange').daterangepicker({
            autoUpdateInput: false,
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " até ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Limpar",
                "fromLabel": "De",
                "toLabel": "Até",
                //"customRangeLabel": "Custom",
                "weekLabel": "S",
                "daysOfWeek": [
                    "Do",
                    "Se",
                    "Te",
                    "Qu",
                    "Qu",
                    "Se",
                    "Sa"
                ],
                "monthNames": [
                    "Janeiro",
                    "Fevereiro",
                    "Março",
                    "Abril",
                    "Maio",
                    "Junho",
                    "Julho",
                    "Agosto",
                    "Setembro",
                    "Outubro",
                    "Novembro",
                    "Dezembro"
                ],
                "firstDay": 1
            }
        });

        jQuery('.order_daterange').on('cancel.daterangepicker', function(ev, picker) {
            jQuery(this).val('');
        });

        jQuery('.order_daterange').on('apply.daterangepicker', function(ev, picker) {
            jQuery(this).val(picker.startDate.format('DD/MM/YYYY') + ' até ' + picker.endDate.format('DD/MM/YYYY'));
        });
    }
}

const editFormClass = new editForm();