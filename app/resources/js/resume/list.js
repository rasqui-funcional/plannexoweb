class resumeController {
    constructor() {
        this.country = myCountry.getCountry();
        this.listenerBtns();
    }

    initializeTable() {
        jQuery(".object_table_tr").on("dblclick", (element) => {
            let finalObject,
                dynamicColumns = [];

            jQuery(element.currentTarget).find("[data-column]").each((index, element) => {
                dynamicColumns.push({
                    [jQuery(element).attr("data-column")]: jQuery(element).attr("data-value")
                });
            });

            finalObject = (Object.assign({}, ...dynamicColumns));
            if ( jQuery('[name="cod_estab"]').val() != "") {
                finalObject.cod_estab_fk = jQuery('[name="cod_estab"]').val();
            }
            
            this.sendToSkuList(finalObject);
        });
    }

    listenerBtns() {
        jQuery("#download_csv").on("click", function () {
            window.open(`/${jQuery('#country').val()}/resume/csv`)
        });
    }

    sendToSkuList(params) {
        loaderShow();

        jQuery.ajax({
            url: `/${this.country}/sku/list/setresume/`,
            data: params,
            type: 'POST',
            datatype: 'text',
            success: (resposta) => {
                
                if (resposta.data) {
                    loaderHide();
                    window.open(`/${jQuery("#country").val()}/sku/list/resume/`);
                } else {
                    loaderHide();
                    alert(i18nGlobals[myCountry.getCountry()].unavailableService)
                }
            },
            error: (error) => {
                console.log('response', error)
                loaderHide();
                alert(i18nGlobals[myCountry.getCountry()].unavailableService)
            }
        });
    }

}

const resumeClass = new resumeController()
const routeClass = new RoutesController();
const paginationClass = new paginationController(routeClass.getResumeTable(), routeClass.goToResumeFilters());