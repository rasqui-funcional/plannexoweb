class resumeController{
    constructor(){
        moment.locale('pt-BR');
        this.today = moment(new Date()).format('MM/YYYY');
        this.formSerielized;
        this.getDataFilters;
        this.divOrder = jQuery("#div_order");
        this.periodForExibition();
        this.exibitionInit();
        this.listenerBtns();          
    }

    listenerBtns(){
        jQuery("[data-toggle=tooltip]").tooltip();
        jQuery('[data-toggle="popover"]').popover();
        
        //controlling the label of delete button
        jQuery("#modal_edit").on("show.bs.modal", function(){
            jQuery("#select_filtro").val() == "" ? jQuery("#confirm_delete").addClass("invisible") : jQuery("#confirm_delete").removeClass("invisible");
        });
        
        //controlling the validator field when modal closes
        jQuery("#modal_edit").on("hide.bs.modal", function(){
            jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});
            jQuery("#filter_nome").val("");
        });

        jQuery('.progress-bar').css("background-color","#F96737");
		jQuery('.progress .progress-bar').css("width",function() {
			return jQuery(this).attr("aria-valuenow") + "%";
	      	
        });
       
        jQuery('.chk_check_all').click(function () {    
            jQuery(this).parent().parent().parent().find('input:checkbox').prop('checked', this.checked);    
        });

        jQuery('#chk_check_all_exibicao').trigger('click');
        
        jQuery("#clean_form").on("click", (element)=>{
            this.cleanForm();
        });

        //send to list page
        jQuery("#btn_search").on("click", ()=>{
            this.search();
        });

        //confirm remove item from database
        jQuery("#confirm_delete").on("click", ()=>{
            this.confirmDeleteFilter();
        });
                
        //My filter change event
        jQuery("#select_filtro").on("change", (element)=>{
			this.cleanForm();
						
			if(jQuery(element.currentTarget).val() == ""){
				//$("#save_filter").prop("disabled",true);
				jQuery("#btn_filtro_text").text("Salvar filtro");
				jQuery("#filter_nome").val("");
				jQuery("#modal_titulo").text("Novo Filtro");				
				jQuery("#btnfiltro_excluir").hide();
				jQuery("#filter_perfil_Publico").prop("checked", true);
			}else{
				loaderShow();
				jQuery("#btn_filtro_text").text("Editar filtro");
				jQuery("#filter_nome").val(jQuery("select[name=select_filtro] option:selected").text());
				jQuery("#modal_titulo").text("Edição de Filtro");				
				jQuery("#btnfiltro_excluir").show();

				$.ajax({
					url: `/${myCountry.getCountry()}/ajax/filter/getFilterById`,
					type: 'POST',
					dataType: 'json',
					data: {
						filter_id: jQuery(element.currentTarget).val()
					},
					success: (response)=> {	
                        this.getDataFilters = response.data[0];
						this.editForm(response.data[0]);
						loaderHide();
						//$("#save_filter").prop("disabled",false);
					},
					error: function (error) {
						loaderHide();
						console.log("error", error)	
					}
				});
			}
        });
        
        jQuery("#save_filter").on("click", (element)=>{
            this.saveFilter();
        });
    }

    periodForExibition(){
        let arrMonths=[];
        for(var i = 0; i<=12; i++){
            arrMonths.push({
                label: moment().month(this.today).subtract(i, 'M').format('MMM/YYYY'),
                value: this.setMonthColumn(i)
            })
        }
        arrMonths.forEach((element)=>{
            let exibitionItem = `<div class='d-flex flex-row text-nowrap align-items-center div_exibition_child'>
                                    <input type="checkbox" name='exibition_columns[]' id='exibition_columns_${element.value}' value='${element.value}'>
                                    <label class='text-truncate pl-1'> ${element.label} </label> <i class='fa fa-arrow-right pln-order-blue pln-cur-pointer pln-margin-auto'></i>
                                </div>`;
            jQuery("#div_exibition").append(exibitionItem);            
        })        
    }

    setMonthColumn(i) {
        if(i == 0) {
            return "OUT_CURRENT_MONTH"
        } else {
            return "OUT_MONTH_MINUS" + i
        }
    }

    saveFilter() {
        if (jQuery("#filter_nome").val() == "") {
            jQuery("#filter_nome").css({"border": "1px solid red"});
            return false;
        } else {
            jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});
        }

        jQuery("#save_filter").attr("disabled",true).find('i').removeClass('pln-invi');
        jQuery("#spn_save").html("Salvando");		
        this.setForm();		


        let perfil = 'Publico',
            name = jQuery("#filter_nome").val(),
            view_id = jQuery("#spn_filter_id").attr("data-original-title"),
            filter_id = jQuery("#select_filtro").val();
                
        let objPost = {};
            objPost.url =  ( filter_id == "" ? `/${myCountry.getCountry()}/ajax/filter/saveFilter` : `/${myCountry.getCountry()}/ajax/filter/updateFilter`);
            objPost.verb = ( filter_id == "" ? 'save' : 'update');
            objPost.type = 'POST';
            objPost.data = {
                filter_perfil: perfil,
                filter_nome: name,
                filter_data: this.formSerielized,
                filter_tela_id: view_id,
                filter_id: filter_id
            };

        this.filterService(objPost);
    }

    confirmDeleteFilter(){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: "btn btn-danger mr-2",
            cancelButtonClass: "btn btn-default",
            buttonsStyling: false
        });

        swalWithBootstrapButtons({
            title: "Deletar filtro?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
        }).then((result) => {
            if (result.value) {
                this.deleteFilter();
            } else {
                jQuery("#confirm_delete").find('span').html('Deletar');
                jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
                swal.close();
            }
        });
    }
    
    deleteFilter(){
		jQuery("#confirm_delete").find('span').html('Deletando');
		jQuery("#confirm_delete").find('i').removeClass('fa fa-times pln-danger').addClass('fa fa-spin fa-spinner');
		let name = jQuery("#filter_nome").val(),
			filter_id = jQuery("#select_filtro").val(),
			objPost = {};
				objPost.url = `/${myCountry.getCountry()}/ajax/filter/deleteFilter`;
				objPost.verb = 'delete';
				objPost.type = 'POST';
				objPost.data = {				
					filter_nome: name,				
					filter_id: filter_id
				};
			this.filterService(objPost);	
	}

    setForm(){
        this.formSerielized = JSON.parse(jQuery('#formulario').serializeJSON());
        this.formSerielized.order = this.getOrders();
		jQuery("#filter_temp").val(JSON.stringify(this.formSerielized));
    }
    
    editForm(formulario){

		if (formulario.FILTER_PERFIL == "Privado") {
			jQuery("#filter_perfil_Privado").prop("checked", true);
		} else {
			jQuery("#filter_perfil_Publico").prop("checked", true);
		}

		let form = document.getElementById('formulario');
		let json = JSON.parse(formulario.FILTER_DATA);

		for(let name in json){
			//se for um array, percorre seus valores
			if( typeof(json[name]) == "object"){
                //este item é específio e precisa ser tratado separadamente, o restante será dinâmico
				if(name == "order") {
					let order = json.order;
					order.forEach((item)=>{
						this.addOrder(item.id, item.text, item.order)
					});
				} else {
                    json[name].forEach(function(item) {
                        let field = document.getElementById(name + "_"+ item);
                        
                        try {
                            field.checked = true;
                        } catch(err) {
                            console.log(err)
                        }
                    });
                }
			}

			let field = form.querySelector("[name='" + name+"']");
			
			if(field){
				switch(field.type){
					
					case 'radio':
						field = form.querySelector("[name=" + name + "][value='"+json[name]+"']");
						field.checked = true;
					break;					
					default:					
						field.value = json[name];
				}				
			}
        }
        this.orderExibition();
    }

    orderExibition(){
        let obj = JSON.parse(this.getDataFilters.FILTER_DATA);

        if(obj.exibition){
            let exibition = Object.values(obj.exibition)        
            for(var i = exibition.length-1; i >= 0; i--){            
                jQuery("#div_exibition").prepend(jQuery(`#exibition_${exibition[i]}`).parent().detach())
            }
        }
    }
    
    search(){
        let checkedGroup = 0, checkedExibition = 0;
        jQuery("input[name='group_columns[]']").each((index, element)=>{
            jQuery(element).is(":checked") ? checkedGroup += 1 : '';
        });

        jQuery("input[name='exibition_columns[]']").each((index, element)=>{
            jQuery(element).is(":checked") ? checkedExibition += 1 : '';
        });

        if(checkedGroup == 0 || checkedExibition == 0){
            this.execSwal("",i18nGlobals[myCountry.getCountry()].filters.selectColumnToShow, "warning");
            return false;
        }

        loaderShow();
		document.formulario.submit();
	}

    //responsável por enviar requisições POST ao banco de dados e tratar os retornos dependendo do verbo utilizado
	filterService(postObj) {
        $.ajax({
			url: postObj.url,
			type: postObj.type,
			dataType: 'json',
			data: postObj.data,
			success: (response) => {
				if (!response.error) {
					switch(postObj.verb){
						//se for delete, trato o ícone de deleção, removo o item do <select> e fecho a modal
						case 'delete':						
							jQuery("#confirm_delete").find('span').html('Deletar');
							jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
							jQuery("#select_filtro").find('option[value='+postObj.data.filter_id+']').remove();
							jQuery("#modal_edit").modal("hide");
							jQuery("#btn_filtro_text").text("Salvar filtro");
						break;
						//se for novo registro, trato o ícone de salvamento
						case 'save':							
							//popula o select com o novo filtro criado
							this.fillFilter(response.data);
							this.setBtnSave()
                            jQuery("#modal_edit").modal("hide");                            
						break;
						//se for edição de registro, trato o ícone de salvamento e atualizo o nome no <select>
						case 'update':
							jQuery("#select_filtro").find('option[value='+postObj.data.filter_id+']').text(postObj.data.filter_nome);
							this.setBtnSave();
							jQuery("#modal_edit").modal("hide");
					}					
				}else{
					alert(i18nGlobals[myCountry.getCountry()].unavailableService);
				}
			},
			error: (error)=> {
				this.setBtnSave();				
				alert(i18nGlobals[myCountry.getCountry()].unavailableService)
				console.log("error", error)	
				console.log("url", this.data.data)
			}
		});
    }
    
    fillFilter(dataset){
		var select = jQuery("#select_filtro");
		select.html('');
		select.append("<option value=''></option>");
		dataset.forEach(function(item){
			select.append(`<option value='${item.value}'>${item.name}</option>`);
		})	
    }

    getOrders(){
		var arr = [];
		jQuery("#div_order").find("div").each(function(){
			arr.push({'id': jQuery(this).attr("id").split('-')[0], 
					  'order': jQuery(this).attr("data-order"), 
					  'text': jQuery(this).find('span').eq(1).text().trim()  
					});
			
		});
		return arr;
	}
    
    setBtnSave(){
		jQuery("#save_filter").attr("disabled", false).find('i').addClass('pln-invi');
		jQuery("#spn_save").html(i18nGlobals[myCountry.getCountry()].save);
	}

    cleanForm(){
        document.getElementById("formulario").reset();
        this.divOrder.html("");
    }

    /************************EXIBITION METHODS***************************/

    exibitionInit(){
        jQuery('#div_exibition, #div_order').multisortable({
            items: "div.div_exibition_child",
            selectedClass: "selected"
        });
        
        jQuery("#div_order, #div_exibition").disableSelection();

        jQuery("#div_exibition div").on('dblclick', (element)=> {
            let clicked_el = jQuery(element.currentTarget);
            this.triggerToOrder(clicked_el, false);
        });

        jQuery("#div_exibition").find('.fa-arrow-right').on('click', (element)=> {
            let clicked_el = jQuery(element.currentTarget);
            this.triggerToOrder(clicked_el, parent=true);
        });

        //ENVIA TODAS AS COLUNAS SELECIONADAS PARA QUE SEJAM ORDENADAS
        jQuery("#sendAllToOrder").on('click', ()=> {
            
            jQuery("#div_exibition input[type=checkbox]").each( (index, element)=> {
                if (jQuery(element).is(":checked")) {                    
                    this.triggerToOrder(jQuery(element).parent(), false);
                }
            });
            this.toggleOrder();
        });

        jQuery('.i_multiorder_exibition').on("click", (element)=>{
            let $op = jQuery('#div_exibition div.selected'),
                $this = jQuery(element.currentTarget);
            if($op.length){
                ($this.hasClass("fa-arrow-up")) ? 
                    $op.first().prev().before($op) : 
                    $op.last().next().after($op);
            }
        });

        jQuery('.i_multiorder_order').on("click", (element)=>{
            var $op = jQuery('#div_order div.selected'),
                $this = jQuery(element.currentTarget);
            if($op.length){
                ($this.hasClass("fa-arrow-up")) ? 
                    $op.first().prev().before($op) : 
                    $op.last().next().after($op);
            }
        });

        //REMOVE TODOS OS ITENS DA ORDENAÇÃO
        jQuery("#cleanOrder").on('click', (element)=>{
            this.divOrder.html("");
            this.toggleOrder();
        });
    }
                                    
    /*dispara tanto o evento de CTRL click quanto duplo click e insere a coluna
    dentro do box de ordenação*/
    triggerToOrder(element, elParent) {
        //dependendo da chamada desta função
        elParent ? element = element.parent() : element=element;
    
        let elToOrder = element.find('input').attr('value').trim();
        
        //Insere somente se não existir este elemento no box
        if( this.divOrder.find(`div[id="${elToOrder}-order"]`).length <= 0 ) {
            this.addOrder(elToOrder, element.text(), 'asc');
            
            this.toggleOrder();
        }
    }
                                        
    //MOSTRA AS OPÇÕES DE ORDENAÇÃO QUANDO HOUVER ITENS PARA ORDENAR
    toggleOrder(){
        if( jQuery("#div_order").children().length == 0 || $("#div_order").children().length == undefined){
            jQuery(".hideOrder").hide();
        }else{
            jQuery(".hideOrder").show();
        }
    }
    
    //adiciona uma coluna para que seja ordenada
    addOrder(id, text, order){
        this.divOrder.append(`
                <div class="d-flex flex-row text-nowrap align-items-center div_exibition_child" data-order="${order}" id="${id}-order">
                    <input type="hidden" name="order[${id}]" value="${order}">
                    <span><i class="fa fa-times pln-danger remove pln-cur-pointer"></i></span>
                    <span class="text-truncate pl-1">${text}</span> 
                    <span class="mr-1 pln-margin-auto">
                        <span class="asc_desc  pln-cur-pointer">
                        <img src="../../../resources/img/sort_up.png" title="Crescente" data-order="asc" style="display:${ order == "asc" ? 'block' : 'none' }" class="mr-1 pln-margin-auto"></i>
                        <img src="../../../resources/img/sort_down.png" title="Decrescente" data-order="desc" style="display:${ order == "asc" ? 'none' : 'block' }" class="mr-1 pln-margin-auto"></i>                        
                        </span>							
                    </span>
                    
                </div>
            `);
            
            //GERA EVENTOS PARA REMOVER O ITEM DA ORDENAÇÃO
            jQuery("#" + id+"-order").find('.remove').on('click', function (event) {
                jQuery(this).parent().parent().remove();
                this.toggleOrder();
            });                 
            //GERA EVENTOS PARA REMOVER O ITEM DA ORDENAÇÃO FIM
    
            //TROCA O ÍCONE DE ORDENAÇÃO E SETA O DATA-ORDER DA DIV PARA O VALOR CORREPONDENTE
            jQuery("#" + id+"-order").find('span .asc_desc img').on('click', function (event) {
                var order = jQuery(this).parent().find('img').not(':visible');
                jQuery("#" + id+"-order").attr('data-order', order.attr('data-order'));
                jQuery("#" + id+"-order").find('input').val(order.attr('data-order'));
    
                order.show();
                jQuery(this).hide();
                
            });
    }
    /**********************END EXIBITION METHODS*************************/

    execSwal(header, text, alertType){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            animation: false,
            title: header,
            text: text,
            type: alertType,
            showCancelButton: false,
            confirmButtonText: i18nGlobals[myCountry.getCountry()].modal.confirmButton
        });
    }
}

let classe = new resumeController();