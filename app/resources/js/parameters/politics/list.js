class politicsController {

    constructor() {
        this.dataPolitics;
        this.deposits;
        this.calendars;
        this.modalPolitic = jQuery("#modal_politic");
        this.politicForm = $("#politic_form");
        this.btnSave = jQuery(".btn-post");
        this.idProfile = jQuery("#edit_id_profile_pk");
        this.qtdSku;
        this.canBeZero = [
            'SIT_ONLY_FIRST_PARC',
            'LT_OTHERS_EXCEPTION',
            'LT_OTHERS',
            'LT_RECEIVING_EXCEPTION',
            'LT_RECEIVING',
            'LT_SUPPLIER_EXCEPTION',
            'LT_SUPPLIER',
            'LT_BUYER_EXCEPTION',
            'LT_BUYER',
            'LT_PLANNER_EXCEPTION',
            'LT_PLANNER'
        ];
        this.listeners();
        this.getDeposits();
        this.getCalendars();
        this.country = myCountry.getCountry();
    }

    listeners() {
        jQuery('#select_all').on('click', (element) => {
            jQuery('#div_deposito').find('input:checkbox').prop('checked', element.currentTarget.checked);
        });

        jQuery('#duplicate').on('click', () => {
            this.duplicate();
        });

        jQuery('#confirm_delete').on('click', () => {
            if (parseInt(this.qtdSku) > 0) {
                paginationClass.execSwal("", i18nPolice[myCountry.getCountry()].errorOnDelete, "warning");
            } else {
                this.confirmDelete();
            }
        });

        jQuery('#new_politic').on('click', () => {
            this.newPolitic();
        });

        jQuery('#update').on('click', () => {
            this.update();
        });

        jQuery('#save_new').on('click', () => {
            this.save();
        });

        jQuery('#modal_politic').on('hide.bs.modal', function () {
            jQuery('#div_deposito, #select_all').find('input[type=checkbox]').prop('checked', false);
        });

        jQuery('.auto_erp').on('click', (element) => {
            let automatic = jQuery(element.currentTarget).parent().parent().find('.hide_automatic');
            let manual = jQuery(element.currentTarget).parent().parent().find('.hide_manual');
            jQuery(element.currentTarget).is(':checked') ? (automatic.show(), manual.hide()) : (automatic.hide(), manual.show());
        });

        jQuery('#DAYS_MIN_CHK').on('click', (element) => {
            let automatic = jQuery('#stock_min_div').find('.hide_automatic');
            let manual = jQuery('#stock_min_div').find('.hide_manual');
            jQuery(element.currentTarget).is(':checked') ? (automatic.show(), manual.hide()) : (automatic.hide(), manual.show());
        });

        jQuery('#SIT_ONLY_FIRST_PARC').on('click', (element) => {
            jQuery(element.currentTarget).is(':checked') ? jQuery('#DAYS_HORIZON_DIV').hide() : jQuery('#DAYS_HORIZON_DIV').show();
        });

        jQuery('#SIT_MIN_MAX_ADD_ERP, #M_CALENDAR, #M_LT').on('click', (element) => {
            jQuery(element.currentTarget).is(':checked') ? jQuery(element.currentTarget).val(1) : jQuery(element.currentTarget).val(0);
        });
    }

    initialize() {
        utilsInstance.onlyNumbers('#goto_pagenumber, .onlynumber, .leadtime');

        jQuery('#politcsSearch').unbind('click').on('click', () => {
            paginationClass.search(true);
        });

        jQuery('.object_table_tr').on('dblclick', (element) => {
            jQuery('#politic-modal-action').html(i18nPolice[myCountry.getCountry()].editPolicyTitle);
            this.resetForm();

            jQuery('#save_new').hide();

            jQuery('#update, #confirm_delete').show();

            loaderShow();

            let id = jQuery(element.currentTarget).find('.ID_PROFILE_PK').attr('data-value');

            this.qtdSku = jQuery(element.currentTarget).find('.QUANTIDADE').attr('data-value');

            this.idProfile.val(id);

            this.getPoliticsData(id);
        });
    }

    isEmpty(obj) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }

        return true;
    }

    duplicate() {
        let arrItensToDelete = [];

        jQuery('#tablePoliticsBody').find('.input_chk').each((index, element) => {
            if (jQuery(element).prop('checked')) {
                arrItensToDelete.push(jQuery(element).parent().parent().find('.ID_PROFILE_PK').attr('data-value'));
            }
        });

        if (this.isEmpty(arrItensToDelete)) {
            return paginationClass.execSwal("", i18nPolice[myCountry.getCountry()].requiredPolice, "warning");
        }

        loaderShow();
        jQuery.ajax({
            url: `/${this.country}/parameters/politics/duplicate`,
            type: 'POST',
            dataType: 'json',
            data: {
                ids: arrItensToDelete
            },
            success: (response) => {
                loaderHide();

                if (response.politics) {
                    paginationClass.execSwal("", i18nPolice[myCountry.getCountry()].successOnDuplicate, "success");
                } else {
                    paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
                }

                this.callGetTableService();
            },
            error: function (error) {
                loaderHide();
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
            }
        })
    }

    newPolitic() {
        // troca título da modal
        jQuery('#politic-modal-action').html(i18nPolice[myCountry.getCountry()].newPolicyTitle);

        //toggle button on new or update
        jQuery('#save_new').show();
        jQuery('#update, #confirm_delete').hide();

        this.idProfile.val('');
        this.qtdSku = 0;
        this.resetForm();

        //set ESTOQUE MÍNIMO to default form
        let automatic = jQuery('#stock_min_div').find('.hide_automatic');
        let manual = jQuery('#stock_min_div').find('.hide_manual');

        manual.show();
        automatic.hide();

        //set CALENDÁRIO to default form
        jQuery('#DAYS_HORIZON_DIV').show();
        jQuery('#politcs_calendar').html('');
        //

        //set LEAD TIME to default form
        jQuery('.leadtime_div').find('.hide_automatic').hide();
        jQuery('.leadtime_div').find('.hide_manual').show();

        //set Deposits list to default order
        this.setDeposits();
        this.modalPolitic.modal('show');
    }

    resetForm() {
        this.politicForm[0].reset()
    }

    getPoliticsData(id) {
        jQuery.get(`/${this.country}/parameters/politics/getPoliticsData/${id}`)
            .done((response) => {
                loaderHide();
                this.dataPolitics = response.politics;
                this.buildCalendarUi(this.dataPolitics.receiving_days);
                this.setFormDeposits();
                this.setFormCalendar();
                //this.setFormServicelevel();
                this.setGeneralFields();
                this.modalPolitic.modal('show');
            })
            .fail(() => {
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
            })
    }

    buildCalendarUi(dates) {
        jQuery('#politcs_calendar').html('');

        let months = [...new Set(dates.map(date => date.DT_RECEIVING_PK.substring(3, 10)))];
        let objDates = [];

        //append calendar by month
        for (let i = 0; i < (months.length > 6 ? 6 : months.length); i++) {
            jQuery('#politcs_calendar').append(`<div id='calendar_${i}' class='datepicker mt-2 col-sm-2'></div>`);

            objDates.push(dates.filter(function (date) {
                    return date.DT_RECEIVING_PK.substring(3, 10) === months[i]
                })
            );
        }

        //instance of datepicker
        jQuery('.datepicker').datepicker($.datepicker.regional['pt-BR']);

        //set months on datepicker
        for (let i = 0; i < months.length; i++) {
            jQuery(`#calendar_${i}`).datepicker('setDate', `01/${months[i]}`);
        }

        //remove color of seted day
        this.setDefaultColor(months.length);

        //set days on datepicker
        for (let i = 0; i < objDates.length; i++) {
            for (let j = 0; j < objDates[i].length; j++) {
                let day = objDates[i][j].DT_RECEIVING_PK.split('/')[0];

                jQuery(`#calendar_${i}`).find('tbody tr').each(function () {
                    jQuery(this).find('td').each(function () {
                        if (jQuery(this).find('a').html() == day * 1) {
                            jQuery(this).find('a').css({'background-color': '#f8543a', 'color': 'white'});
                        }
                    })
                });
            }
        }
    }

    getDeposits() {
        jQuery.get(`/${this.country}/parameters/politics/deposits`)
            .done((response) => {
                this.deposits = response.deposits;
                this.setDeposits();
            })
            .fail(() => {
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
            })
    }

    getCalendars() {
        jQuery.get(`/${this.country}/parameters/politics/calendar`)
            .done((response) => {
                this.calendars = response.calendar;
                this.setCalendars();
            })
            .fail(() => {
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
            })
    }

    setDeposits() {
        jQuery('#deposits_count').html(`(${this.deposits.length})`);
        jQuery('#div_deposito').html('');

        for (let i = 0; i < this.deposits.length; i++) {
            jQuery('#div_deposito').append(`
            <div class="d-flex flex-row align-items-center">
                <input type="checkbox" name="cod_local[]" value="${this.deposits[i].COD_LOCAL_PK}" id="${this.deposits[i].COD_LOCAL_PK}">
                <label for="${this.deposits[i].COD_LOCAL_PK}" class="text-truncate pl-1"> ${this.deposits[i].DESCRICAO} </label>
            </div>
            `);
        }
    }

    setCalendars() {
        for (let i = 0; i < this.calendars.length; i++) {
            jQuery('#calendars').append(`<option value="${this.calendars[i].code}">${this.calendars[i].name}</option>`)
        }
    }

    setDefaultColor(index) {
        for (let i = 0; i < index; i++) {
            jQuery(`#calendar_${i}`).find('tbody tr').each(function () {
                jQuery(this).find('td').each(function () {
                    jQuery(this).find('a').css({
                        'background-color': '#ececec',
                        'color': '#363636',
                        'border-color': '#c4c4c4'
                    });
                })
            });
        }
    }

    setFormDeposits() {
        let deposits = this.dataPolitics.cod_local_selected;

        for (let i = 0; i < deposits.length; i++) {
            jQuery('#div_deposito').find('input[type=checkbox]').each((index, element) => {
                if (jQuery(element).attr('id') == deposits[i]) {
                    jQuery(element).prop('checked', true);
                }
            });
        }

        this.orderDepositsExibition();
    }

    orderDepositsExibition() {
        let deposits = this.dataPolitics.cod_local_selected;
        if (deposits) {
            for (let i = deposits.length - 1; i >= 0; i--) {
                jQuery('#div_deposito').prepend(jQuery(`#${deposits[i]}`).parent().detach())
            }
        }
    }

    setFormCalendar() {
        let calendar = this.dataPolitics.calendar_selected;
        jQuery('#calendars').val(calendar[0].ID_CALENDAR_PK)
    }

    setFormServicelevel() {
        let serviceLevel = this.dataPolitics.data.SERVICE_LEVEL;
        jQuery('#service_level').val(serviceLevel)
    }

    setGeneralFields() {
        jQuery('#purchase_count').html(this.dataPolitics.AVG_DAYS_INTERVAL);

        let form = document.getElementById('modal_politic');
        let data = this.dataPolitics.data;
        let fieldsWithError = [];

        for (let key in data) {
            let field = form.querySelector(`[name='${key}']`);
            let fieldLt = key.split('_')[0];

            try {
                field.value = (data[key] < 0 ? 0 : data[key]);
            } catch (err) {
                //fieldsWithError.push(key);
            }

            if (fieldLt == 'LT') {
                //if LT is not EXCEPTION
                if (!key.split('_')[2]) {
                    const automatic = jQuery(`input[name=${key}]`).parent().parent().find('.hide_automatic');
                    const manual = jQuery(`input[name=${key}]`).parent().parent().find('.hide_manual');
                    if (data[key] == -1) {
                        automatic.show();
                        manual.hide();
                        jQuery(`#${key}_CHK`).prop('checked', true);
                    } else {
                        automatic.hide();
                        manual.show();
                        jQuery(`#${key}_CHK`).prop('checked', false);
                    }
                }
            }

            if (key == 'DAYS_MIN') {
                const automatic = jQuery('#stock_min_div').find('.hide_automatic');
                const manual = jQuery('#stock_min_div').find('.hide_manual');

                if (data[key] == -1) {
                    automatic.show();
                    manual.hide();
                    jQuery(`#${key}_CHK`).prop('checked', true);
                } else {
                    automatic.hide();
                    manual.show();
                    jQuery(`#${key}_CHK`).prop('checked', false);
                }
            }

            if (key == 'SIT_ONLY_FIRST_PARC') {
                if (parseInt(data[key]) == 1) {
                    jQuery('#DAYS_HORIZON_DIV').hide();
                } else {
                    jQuery('#DAYS_HORIZON_DIV').show();
                }

                field.checked = parseInt(data[key])
            }

            if (key === 'SIT_MIN_MAX_ADD_ERP' || key === 'M_CALENDAR' || key === 'M_LT') {
                field.checked = parseInt(data[key]);
            }
        }

        if (fieldsWithError.length > 0) {
            paginationClass.execSwal('', `${i18nGlobals[myCountry.getCountry()].errorOnFilledFields}${fieldsWithError}`, 'warning');
        }
    }

    update() {
        const valid = this.formValidate();

        if (!valid) return false;

        this.btnSave.prop('disabled', true);

        this.clearAvailableDepositsFilter();

        const formData = JSON.parse(this.politicForm.serializeJSON({checkboxUncheckedValue: '0'}));
        jQuery.ajax({
            url: `/${this.country}/parameters/politics/update`,
            type: 'POST',
            dataType: 'json',
            data: formData,
            success: (response) => {
                this.btnSave.prop('disabled', false);
                if (response.politics) {
                    this.modalPolitic.modal('hide');
                    this.callGetTableService();
                } else {
                    paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
                    this.btnSave.prop('disabled', false);
                }
            },
            error: function (error) {
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
                console.log('error', error);
            }
        })

    }

    save() {
        const valid = this.formValidate();
        if (!valid) return false;

        this.btnSave.prop('disabled', true);

        const formData = JSON.parse(this.politicForm.serializeJSON());
        jQuery.ajax({
            url: `/${this.country}/parameters/politics/new`,
            type: 'POST',
            dataType: 'json',
            data: formData,
            success: (response) => {
                this.btnSave.prop('disabled', false);
                if (response.politics) {
                    this.modalPolitic.modal('hide');
                    paginationClass.init = false;
                    paginationClass.getTableService(...paginationClass.paginationParams);
                } else {
                    paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
                    this.btnSave.prop('disabled', false);
                }
            },
            error: function (error) {
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
            }
        })
    }

    /**
     * Valida, dentre os valores do array this.canBezero, quais campos podem conter valor igual à zero.
     * @param {string} value
     */
    checkIfCanBeZero(value) {
        return this.canBeZero.some(function (field) {
            return field == value
        });
    }

    /**
     * Valida todos os campos do formulário, indicando quais foram preenchidos incorretamente
     */
    formValidate() {
        let depositCont = 0, invalidField = 0, isValid = false;

        this.politicForm.find('select, input').each((i, e) => {
            //o campo é invisível? se sim, não precisa ser validado
            if (!jQuery(e).is(':visible')) {
                jQuery(e).removeClass('pln-invalid-field');
                return;
            }

            //o campo não precisa ser validado? se sim, não precisa ser validado
            if (jQuery(e).hasClass('no-validate')) {
                jQuery(e).removeClass('pln-invalid-field');
                return;
            }

            //o campo não o campo não está vazio, e não está zerado? se sim, não precisa ser validado
            if (jQuery(e).val() != '' && jQuery(e).val() != 0) {
                jQuery(e).removeClass('pln-invalid-field');
                return;
            }

            //o campo possui valor zero, mas pode ser zero? se sim, não precisa ser validado
            if (jQuery(e).val() == 0 && this.checkIfCanBeZero(jQuery(e).attr('name'))) {
                jQuery(e).removeClass('pln-invalid-field');
                return;
            }

            if (jQuery(e).prop('id') === 'SIT_MIN_MAX_ADD_ERP' || jQuery(e).prop('id') === 'M_CALENDAR' || jQuery(e).prop('id') === 'M_LT') {
                return;
            }

            //não passou em nenhuma validação? então foi preenchido incorretamente.
            invalidField++;
            jQuery(e).addClass('pln-invalid-field');
        });

        //at least one deposit must be checked
        jQuery('#div_deposito').find('input[type=checkbox]').each((index, element) => {
            if (jQuery(element).is(':checked')) {
                depositCont++;
            }
        });

        if (invalidField > 0) {
            paginationClass.execSwal("", i18nGlobals[myCountry.getCountry()].fillRequiredFields, "warning");
        } else if (depositCont <= 0) {
            paginationClass.execSwal("", i18nPolice[myCountry.getCountry()].selectAtLeastOneDeposit, "warning");
        } else {
            isValid = true;
        }

        return isValid;
    }

    //Exibe alerta de confirmação para Inativação da SKU
    confirmDelete() {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-danger mr-2',
            cancelButtonClass: 'btn btn-default',
            buttonsStyling: false
        });

        swalWithBootstrapButtons({
            title: i18nPolice[myCountry.getCountry()].modalRemoveItem.title,
            type: 'warning',
            animation: false,
            showCancelButton: true,
            confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButtonYes,
            cancelButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.cancelButtonNo
        }).then((result) => {
            if (result.value) {
                this.delete();
            } else {
                swal.close();
            }
        });
    }

    delete() {
        jQuery.ajax({
            url: `/${this.country}/parameters/politics/delete`,
            data: {
                'ID_PROFILE_PK': this.idProfile.val(),
            },
            type: 'POST',
            datatype: 'text',
            success: (response) => {
                paginationClass.init = false;
                paginationClass.getTableService(...paginationClass.paginationParams);
                this.modalPolitic.modal('hide');
            },
            error: function (error) {
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'danger');
            }
        });
    }

    callGetTableService() {
        let limit = jQuery("#items_per_page").val(),
            searchValue = jQuery('#search_input').val(),
            searchCol = jQuery('#seach_col').val(),
            recalcFooter = true;

        paginationClass.init = false;
        paginationClass.order = null;
        paginationClass.getTableService(1, limit, searchValue, searchCol, recalcFooter);
    }

    clearAvailableDepositsFilter() {
        jQuery('#ipt_find_deposito').val('');
        search_filter('ipt_find_deposito', 'div_deposito');
    }
}

const classe = new politicsController();
const routeClass = new RoutesController();
const paginationClass = new paginationController(routeClass.getPoliticsTable());