class calendarController {
    constructor() {
        this.descCalendar = jQuery("#desc_calendar");
        this.calendarModal = jQuery('#calendar_modal');
        this.calendarId = jQuery("#calendar_id");
        this.periodInterval = jQuery("#period_interval");
        this.eachWeek = jQuery("#each_week");
        this.dayOrWeek = jQuery("#day_or_week");
        this.repeatWeek = jQuery("#repeat_week");
        this.dayOfMonth = jQuery("#day_of_month");
        this.dayOfWeek = [];
        this.endsIn = jQuery("#ends_in");
        this.calendarUi();
        this.listenDayOfWeekButtons();
        this.country = myCountry.getCountry();
    }

    initialize() {
        utilsInstance.onlyNumbers("#goto_pagenumber");


        jQuery(".input_chk").on("click", (element) => {
            let checked = 0;
            jQuery(".input_chk").each((index, element) => {
                jQuery(element).is(":checked") ? checked += 1 : '';
            });

            if (checked > 0) {
                jQuery("#parameters_calendar_remove, #parameters_calendar_duplicate").prop("disabled", false);
            } else {
                jQuery("#parameters_calendar_remove, #parameters_calendar_duplicate").prop("disabled", true);
            }
        })

        jQuery("#calendarSearch").unbind("click").on("click", () => {
            paginationClass.search();
        });

        jQuery(".list_purchaseorder").on("click", (element) => {
            let finalObject,
                dynamicColumns = [];

            jQuery(element.currentTarget).parent().parent().find("[data-column]").each((index, element) => {
                dynamicColumns.push({[jQuery(element).attr("data-column")]: jQuery(element).attr("data-value")})
            });

            finalObject = (Object.assign({}, ...dynamicColumns));
            this.callOrders(finalObject);
        });

        jQuery("#exec").on("click", () => {
            this.exec();
        });

        jQuery("#politcsSearch").unbind("click").on("click", () => {
            paginationClass.politicsSearch();
        });

        jQuery(".object_table_tr").on("dblclick", (element) => {
            loaderShow();
            const calendarID = jQuery(element.currentTarget).attr("data-id");

            this.calendarId.val(calendarID);

            $.get(`/${this.country}/parameters/calendar/${calendarID}`)
                .done((response) => {
                    loaderHide();
                    this.editCalendar(response);
                })
                .fail(() => {
                    loaderHide();
                });
        });

    }

    /******INITIALIZE FIM******/
    saveValidate() {
        let isInvalid = false;

        if (this.descCalendar.val().trim() == '') {
            this.descCalendar.addClass("pln-invalid-field");
            isInvalid = true;
        } else {
            this.descCalendar.removeClass("pln-invalid-field");
        }

        if (jQuery("#ends_in_period").prop("checked")) {
            if (this.endsIn.val() == '') {
                this.endsIn.addClass('pln-invalid-field');
                isInvalid = true;
            } else {
                this.endsIn.removeClass('pln-invalid-field');
            }
        }

        if (jQuery('#calendar').data('calendar').getDataSource().length <= 0) {
            paginationClass.execSwal("", i18nGlobals[myCountry.getCountry()].calendar.notFilled, "error");
            isInvalid = true;
        } else {
            if (isInvalid) {
                paginationClass.execSwal("", i18nGlobals[myCountry.getCountry()].fillRequiredFields, "error");
                isInvalid = true;
            }
        }

        return isInvalid;
    }

    save() {
        if (this.saveValidate()) {
            return false;
        }

        jQuery("#save").prop("disabled", true);

        $.ajax({
            url: `/${this.country}/parameters/calendar/new`,
            type: 'POST',
            datatype: 'json',
            data: {
                'desc': this.descCalendar.val(),
                'dates': this.prepareDates()
            },
            success: (response) => {
                paginationClass.init = false;
                paginationClass.getTableService(...paginationClass.paginationParams);
                this.calendarModal.modal('hide');
                jQuery("#save").prop("disabled", false);
            }
        });
    }

    update() {
        if (this.saveValidate()) {
            return false;
        }
        jQuery("#update").prop("disabled", true);

        $.ajax({
            url: `/${this.country}/parameters/calendar/update`,
            type: 'POST',
            datatype: 'json',
            data: {
                'calendar_id': this.calendarId.val(),
                'desc': this.descCalendar.val(),
                'dates': this.prepareDates()
            },
            success: (response) => {
                this.callGetTableService();
                this.calendarModal.modal('hide');
                jQuery("#update").prop("disabled", false);
            }
        });
    }

    delete() {
        const codItemsSelected = this.codItemsSelected();

        $.ajax({
            url: `/${this.country}/parameters/calendar/delete`,
            type: 'POST',
            datatype: 'json',
            data: {
                ids: codItemsSelected
            },
            success: (response) => {
                paginationClass.init = false;
                paginationClass.getTableService(...paginationClass.paginationParams);
            }
        });
    }

    duplicate() {
        const codItemsSelected = this.codItemsSelected();

        $.ajax({
            url: `/${this.country}/parameters/calendar/duplicate`,
            type: 'POST',
            datatype: 'json',
            data: {
                'ids': codItemsSelected
            },
            success: (response) => {
                this.callGetTableService();
            }
        });
    }

    callGetTableService() {
        let limit = jQuery("#items_per_page").val(),
            searchValue = jQuery('#search_input').val(),
            searchCol = false,
            recalcFooter = true;

        loaderShow();
        paginationClass.init = false;
        paginationClass.order = null;
        paginationClass.getTableService(1, limit, searchValue, searchCol, recalcFooter);
    }

    codItemsSelected() {
        let codItems = [];

        jQuery("#tableCalendarList").find(".input_chk").each((index, element) => {
            if (jQuery(element).prop("checked")) {
                codItems.push(jQuery(element).parent().parent().parent().attr('data-id'));
            }
        });

        return codItems;
    }

    listenDayOfWeekButtons() {

        this.calendarModal.on("hide.bs.modal", () => {
            this.endsIn.removeClass('pln-invalid-field');
            this.dayOfMonth.removeClass('pln-invalid-field');
            this.descCalendar.removeClass("pln-invalid-field");
        });

        jQuery('#parameters_calendar_remove').on('click', () => {
            this.confirmDelete();
        });

        jQuery('#parameters_calendar_duplicate').on('click', () => {
            this.duplicate();
        });

        jQuery("#new_calendar").on("click", () => {
            this.dayOfWeek = [];
            jQuery('.btn-week').removeClass("btn-selected-week");

            this.descCalendar.val("");
            this.clearCalendar();
            this.calendarModal.modal('show');
            this.calendarId.val("");
            jQuery("#save").show();
            jQuery("#update").hide();
        });

        jQuery("#save").on("click", () => {
            this.save()
        });

        jQuery("#update").on("click", () => {
            this.update()
        });

        this.periodInterval.on("change", (element) => {
            if (jQuery(element.currentTarget).val() == "M") {
                this.dayOrWeek.val("D");
                jQuery(".weekly-each-text, .weekly-numbers, .weekly-week-text, .monthly-repeat-text, .monthly-repeat-buttons").hide();
                jQuery(".monthly-all-text, .monthly-day-week, .monthly-input, .monthly-month-text").show();
            } else {
                jQuery(".weekly-each-text, .weekly-numbers, .weekly-week-text, .monthly-repeat-text, .monthly-repeat-buttons").show();
                jQuery(".monthly-all-text, .monthly-day-week, .monthly-input, .monthly-month-text, .monthly-repeat-select").hide();
            }
        });

        this.dayOrWeek.on("change", (element) => {
            if (jQuery(element.currentTarget).val() == "W") {
                jQuery(".monthly-repeat-text, .monthly-repeat-select, .monthly-repeat-buttons").show();
                jQuery(".monthly-input, .monthly-month-text").hide();
            } else {
                jQuery(".monthly-repeat-text, .monthly-repeat-select, .monthly-repeat-buttons").hide();
                jQuery(".monthly-input, .monthly-month-text").show();
            }
        });

        jQuery("#clear_calendar").on("click", () => {
            this.clearCalendar();
        });

        jQuery('.btn-week').click(element => {

            jQuery(element.currentTarget).toggleClass("btn-selected-week");

            const dayOfWeekNr = jQuery(element.currentTarget).attr("data-day-of-week");

            if (jQuery(element.currentTarget).hasClass("btn-selected-week")) {
                this.dayOfWeek.push(dayOfWeekNr);
            } else {
                this.dayOfWeek.splice(this.dayOfWeek.indexOf(jQuery(element.currentTarget).attr("data-day-of-week")), 1)
            }

        });
    }

    prepareDates() {
        const dates = jQuery('#calendar').data('calendar').getDataSource();

        return dates.map((date) => moment(date.startDate).format('YYYY-MM-DD'))
    }

    getEndDate() {
        //if input type date is not set, calc 1 year
        if (jQuery("#ends_in_period").prop("checked")) {
            return (this.endsIn.val() == "" ? moment(new Date()).add(1, "y").format("YYYY-MM-DD").toString() : this.endsIn.val());
        } else {
            return moment(new Date()).add(10, "y").format("YYYY-MM-DD").toString();
        }

    }

    //get the param forms and build an array to datasource calendar
    calcEndDate(dateParam) {
        //if input type date is not set, calc 1 year
        let validEnd = this.getEndDate();

        let auxInit = new Date();
        let init = moment([auxInit.getFullYear(), auxInit.getMonth(), auxInit.getDate()])
        let end = moment([parseInt(validEnd.split("-")[0]), parseInt(validEnd.split("-")[1]) - 1, parseInt(validEnd.split("-")[2])]);
        let calcEnd = end.diff(init, dateParam);

        return calcEnd;
    }

    //calc if the selected end date is lower then calculed period date of calendarGenerateDatasource
    //precisei validar se o calculo da função calendarGenerateDatasource() nâo ultrapassa a data limite escolhida pelo usuario
    calcCorrectPeriod(date) {
        return (parseInt(date.split("-").join("")) <= parseInt(this.getEndDate().split("-").join("")))
    }

    calendarGenerateDatasource(dayOfWeek) {

        let start = moment();

        let end = moment().add(this.calcEndDate('days'), 'd'); //calcula EM DIAS até aonde temos que marcar as datas de acordo com o que o usuario escolher

        var arr = [];

        let tmp = start.clone().day(parseInt(dayOfWeek)); //this.dayOfWeek = dia da semana que o usuario vai escolher
        if (tmp.isAfter(start, 'd')) {
            arr.push(tmp.format('YYYY-MM-DD'));
        }
        while (tmp.isBefore(end)) {
            tmp.add((this.eachWeek.val() * 7), 'd');
            //este if garante que uma data não seja marcada se for maior do que o usuario escolheu
            if (this.calcCorrectPeriod(tmp.format('YYYY-MM-DD'))) {
                arr.push(tmp.format('YYYY-MM-DD'));
            }

        }

        return arr;
    }

    calendarGenerateDayInMonth(dayOfMonth) {
        const qtyMonth = this.calcEndDate('months'); //calcula EM DIAS até aonde temos que marcar as datas de acordo com o que o usuario escolher
        let dates = [];

        for (let index = 0; index <= qtyMonth; index++) {
            let start = moment();
            start = moment(start).set('date', dayOfMonth);
            const month = start.add(index, 'month');
            dates.push(moment(month).format("YYYY-MM-DD"));
        }

        return dates;
    }

    calendarGenerateWeekInMonth(dayOfWeek) {

        let start = moment();

        let end = moment().add(this.calcEndDate('days'), 'd'); //calcula EM DIAS até aonde temos que marcar as datas de acordo com o que o usuario escolher

        var arr = [];
        // Get "next" monday
        let tmp = start.clone().day(parseInt(dayOfWeek)); //this.dayOfWeek = dia da semana que o cara vai escolher

        while (tmp.isBefore(end)) {
            if (this.weekOfMonth(tmp) == parseInt(this.repeatWeek.val())) {
                arr.push(tmp.format('YYYY-MM-DD'));
            }
            tmp.add(7, 'd');
        }

        return arr;
    }

    weekOfMonth(input) {
        return input.week() - moment(input).startOf('month').week() + 1;
    }

    execValidate() {
        let isInvalid = false;

        if (jQuery("#ends_in_period").prop("checked")) {
            if (this.endsIn.val() == '') {
                paginationClass.execSwal("", i18nGlobals[myCountry.getCountry()].calendar.selectLastDate, "error");
                this.endsIn.addClass('pln-invalid-field');
                isInvalid = true;
            } else {
                this.endsIn.removeClass('pln-invalid-field');
            }
        } else {
            this.endsIn.removeClass('pln-invalid-field');
        }

        //if is weekly
        if (this.periodInterval.val() == 'W') {
            if (this.dayOfWeek.length <= 0) {
                paginationClass.execSwal("", i18nGlobals[myCountry.getCountry()].calendar.selectDayOfWeek, "error");
                isInvalid = true;
            }
        }
        //if is monthly
        else {
            //if is every day
            if (this.dayOrWeek.val() == 'D') {
                if (this.dayOfMonth.val() == '') {
                    paginationClass.execSwal("", i18nGlobals[myCountry.getCountry()].calendar.insertDayOfMonth, "error");
                    this.dayOfMonth.addClass('pln-invalid-field');
                    isInvalid = true
                } else {
                    this.dayOfMonth.removeClass('pln-invalid-field');
                }
            } else {
                ////if is every week
                if (this.dayOfWeek.length <= 0) {
                    paginationClass.execSwal("", i18nGlobals[myCountry.getCountry()].calendar.selectDayOfWeek, "error");
                    isInvalid = true;
                }
            }

        }

        return isInvalid;
    }

    exec() {
        //Weekly

        if (this.execValidate()) return false;

        jQuery('#calendar').data('calendar').setDataSource();

        if (this.periodInterval.val() == 'W') {
            this.execWeek()
        } else {
            //Monthly per day
            if (this.dayOrWeek.val() == 'D') {
                this.execDayInMonth()
                //Monthly per week
            } else {
                this.execWeekInMonth();
            }
        }
    }

    execWeek() {

        let data = [];

        for (let i = 0; i < this.dayOfWeek.length; i++) {
            let generateByDay = this.calendarGenerateDatasource(this.dayOfWeek[i]);
            for (let j = 0; j < generateByDay.length; j++) {
                data.push(generateByDay[j])
            }
        }

        var dataSource = jQuery('#calendar').data('calendar').getDataSource();
        for (let i = 0; i < data.length; i++) {

            let year = data[i].split("-")[0];

            let month = data[i].split("-")[1];
            let day = data[i].split("-")[2];

            this.pushDatasource(dataSource, year, month, day);
        }

        jQuery('#calendar').data('calendar').setDataSource(dataSource);
    }

    execDayInMonth() {

        const dayOfMonth = this.dayOfMonth.val();
        let data = this.calendarGenerateDayInMonth(dayOfMonth);

        var dataSource = jQuery('#calendar').data('calendar').getDataSource();
        for (let i = 0; i < data.length; i++) {
            let year = data[i].split("-")[0];
            let month = data[i].split("-")[1];
            let day = data[i].split("-")[2];

            this.pushDatasource(dataSource, year, month, day);
        }

        jQuery('#calendar').data('calendar').setDataSource(dataSource);
    }

    execWeekInMonth() {
        let data = [];

        for (let i = 0; i < this.dayOfWeek.length; i++) {
            let generateByDay = this.calendarGenerateWeekInMonth(this.dayOfWeek[i]);
            for (let j = 0; j < generateByDay.length; j++) {
                data.push(generateByDay[j])
            }
        }

        var dataSource = jQuery('#calendar').data('calendar').getDataSource();
        for (let i = 0; i < data.length; i++) {

            let year = data[i].split("-")[0];

            let month = data[i].split("-")[1];
            let day = data[i].split("-")[2];

            this.pushDatasource(dataSource, year, month, day);
        }

        jQuery('#calendar').data('calendar').setDataSource(dataSource);
    }


    clearCalendar() {
        let dataSource = jQuery('#calendar').data('calendar').getDataSource();
        dataSource = [];
        jQuery('#calendar').data('calendar').setDataSource(dataSource);
    }

    calendarUi() {

        jQuery('#calendar').calendar({
            customDayRenderer: function (element, date) {
                let feriados = jQuery("#feriados").val().split(",")

                for (let i = 0; i < feriados.length; i++) {

                    let diaFeriado = parseInt(feriados[i].split("-")[0]),
                        mesFeriado = parseInt(feriados[i].split("-")[1]);

                    if ((date.getMonth()) == mesFeriado && date.getDate() == diaFeriado) {
                        $(element).css({
                            'background-color': '#ddd',
                            'color': '#000',
                            'border-bottom': '3px solid #333'
                        });
                        //$(element).css('border-radius', '15px');
                        $(element).attr('title', feriados[i].split("-")[2]);
                    } else {
                        $(element).parent().css({'border': '3px solid #fff'});
                    }
                }

            },
            disabledDays: [
                new Date(new Date().getFullYear(), 11, 25)
            ],
            language: $('#country').eq(0).val(),
            //disabledWeekDays: [6, 0],
            style: 'background',
            enableContextMenu: true,
            enableRangeSelection: false,
            dataSource: [],

        });


        jQuery('#calendar').on("clickDay", (e) => {
            var dataSource = jQuery('#calendar').data('calendar').getDataSource();
            //get clicked date
            let selectedDate = new Date(e.date.getFullYear(), e.date.getMonth(), e.date.getDate());

            //search in datasource if this date already exists
            let alreadyInDatasource = dataSource.map(x => JSON.stringify(x.startDate)).indexOf(JSON.stringify(selectedDate))

            //if not exists, push
            if (alreadyInDatasource === -1) {
                this.pushDatasource(dataSource, e.date.getFullYear(), e.date.getMonth() + 1, e.date.getDate());

                //if exists, remove
            } else {
                dataSource.splice(alreadyInDatasource, 1)
            }

            jQuery('#calendar').data('calendar').setDataSource(dataSource);
        });

    }

    editCalendar(calendar) {
        this.clearCalendar();

        this.descCalendar.val(calendar.calendar_info.DESC_CALENDAR)

        let dataSource = jQuery('#calendar').data('calendar').getDataSource();
        for (let i = 0; i < calendar.calendar_dates.length; i++) {
            let year = calendar.calendar_dates[i].split("-")[0];
            let month = calendar.calendar_dates[i].split("-")[1];
            let day = calendar.calendar_dates[i].split("-")[2];

            this.pushDatasource(dataSource, year, month, day);
        }

        jQuery('#calendar').data('calendar').setDataSource(dataSource);


        jQuery("#save").hide();
        jQuery("#update").show();
        this.calendarModal.modal('show');
    }

    pushDatasource(dataSource, year, month, day) {
        dataSource.push({
            color: "#f96737",
            startDate: new Date(parseInt(year), parseInt(month - 1), parseInt(day)),
            endDate: new Date(parseInt(year), parseInt(month - 1), parseInt(day))
        });
    }

    confirmDelete() {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: "btn btn-danger mr-2",
            cancelButtonClass: "btn btn-default",
            buttonsStyling: false
        });

        swalWithBootstrapButtons({
            title: i18nGlobals[myCountry.getCountry()].calendar.modalRemoveItem.title,
            type: 'warning',
            animation: false,
            showCancelButton: true,
            confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButtonYes,
            cancelButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.cancelButtonNo
        }).then((result) => {
            if (result.value) {
                this.delete();
            } else {
                swal.close();
            }
        })
    }
}

var classe = new calendarController();
const routeClass = new RoutesController();
const paginationClass = new paginationController(routeClass.getCalendarTable());