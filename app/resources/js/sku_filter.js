$(function(){
    //this function check each checkbox by the element <fieldset> as parent of element $this
    $(` #chk_check_all_exibicao, 
        #chk_check_all_cat_sistema, 
        #chk_check_all_grupo,
        #chk_check_all_subgrupo,
        #chk_check_all_classe,
        #chk_check_all_padronizado,
        #chk_check_all_contrato,
        #chk_check_all_alto_custo,
        #chk_check_all_teste`).click(function () {    
            $(this).parent().parent().parent().find('input:checkbox').prop('checked', this.checked);    
        });
});