class Suggests {
    constructor() {
        this.searchInput = jQuery('#search_input');
        this.searchCol = jQuery('#seach_col');
        this.estabs = jQuery('#aux_estabs');
        this.status = jQuery('#aux_status');
        this.utils = "";
        this.country = jQuery("#country").val();
        this.codHeaderPk = jQuery("#cod_header_pk").val();
        this.loadEvents();
    }

    loadEvents() {
        jQuery("#search").on('click', ()=> {
            paginationClass.search();
        });

        jQuery('#search_input').on('keyup', (event)=> {
            event.preventDefault();
            if (event.keyCode === 13) {
                paginationClass.search();
            }
        });

        this.estabs.on('change', (element)=>{
            this.setsearchInput(element.target.value);
        });

        this.status.on('change', (element)=>{
            this.setsearchInput(element.target.value);
        });

        jQuery('#seach_col').on('change', (element)=>{
            this.changeFilterType(element.target.value);
        });

        jQuery("#download").on('click', this.downloadCSV.bind(this));

        jQuery("#duplicate").on('click', this.duplicate.bind(this));

        this.utils = new Utils();
        utilsInstance.onlyNumbers("#goto_pagenumber");        
    }

    changeFilterType(field){
        this.defaultFilters();
        switch(field){
            case 'DATE_ARRIVAL_ORIGINAL': 
            case 'DATE_ARRIVAL_ALTERED':
            case 'DATE_CREATED':
            case 'DATE_APPROVED':
                this.searchInput.prop('type', 'date');
            break;
            case 'SIT_PSUG':
                this.searchInput.hide().val(this.status.val());                
                this.status.removeClass('hide');
            break;
            case 'ID_PSUG_PK':
            case 'NUM_PARC_PK':
            case 'ID_SKU_FK':
            case 'SIT_PSUG':
            case 'QTY_ALTERED':
            case 'QTY_ORIGINAL':
                this.searchInput.prop('type', 'number').val('');
            break;
            case 'COD_ESTAB_FK':
                this.searchInput.hide().val(this.estabs.val());
                this.estabs.removeClass('hide');
            break;
            default:
                this.searchInput.prop('type', 'text').val('');
        }
    }

    defaultFilters(){
        this.searchInput
            .show()
            .val('');
        this.estabs.addClass('hide');
        this.status.addClass('hide');
    }

    setsearchInput(value){
        this.searchInput.val(value);        
    }

    duplicate() {
        jQuery("#modal_duplicate").modal("show");
        $(".dplTableContainer").hide();
        jQuery('#modaltable_load').attr("style", "display:flex !important");
        const dup = new Duplicate;
    }

    downloadCSV() {
        const id = jQuery('#cod_header_pk').val();
        const col = this.searchCol.val();
        const colVal = this.searchInput.val();
        
        if (colVal == '') {
            window.open(`/${myCountry.getCountry()}/csv/approvalssuggest/${id}/0/0`)
        } else {
            window.open(`/${myCountry.getCountry()}/csv/approvalssuggest/${id}/${col}/${colVal}`)
        }
    }
}

const suggests = new Suggests();
const paginationClass = new paginationController(`/${suggests.country}/approvals/suggest/table/${suggests.codHeaderPk}`);