class Duplicate {
    constructor() {
        this.codEstab = jQuery('#cod_estab_pk').val()
        this.dtExpReq = jQuery('#set_dt_exp_req').val()
        this.id = jQuery("#cod_header_pk").val()
        this.limit = 10
        this.pageNumber = 1
        $(".daterangepicker").css("min-width", "auto")
        this.id = jQuery("#cod_header_pk").val()
        this.reason = jQuery("#reason").val()
        this.buyer = jQuery("#buyer").val()
        this.sector = jQuery("#sector").val()
        this.deposit_code = jQuery("#deposit_code").val();
        this.loadData()
        this.load()
        this.searchButtonListener();
        this.init = false
    }

    orderByDuplicate() {
        jQuery(".order-col-duplicate").on('click', (e) => {
            this.ordenateByColumn(e)
        });
    }

    ordenateByColumn(e) {
        var this_icon = jQuery(e.currentTarget).find("i"),
            sort = this_icon.hasClass("fa-sort"),
            sortAsc = this_icon.hasClass("fa-sort-asc"),
            iconEl = this_icon,
            order = "",
            columnName = e.currentTarget.id;

        jQuery(e.currentTarget).parent().parent().find("i").each(function () {
            jQuery(this).removeClass("fa-sort-asc fa-sort-desc").addClass("fa-sort")
        });

        if (sort) {
            this_icon.removeClass("fa-sort").addClass("fa-sort-asc");

            order = "asc";
        } else {
            if (sortAsc) {
                iconEl.removeClass("fa-sort-asc").addClass("fa-sort-desc");
                order = "desc";
            } else {
                iconEl.removeClass("fa-sort-desc").addClass("fa-sort-asc");
                order = "asc";
            }
        }

        var columnOrder = {[columnName]: order};

        this.getDplTable(1, this.limit, false, columnOrder)
    }

    delItem() {
        jQuery(".remover").on('click', function () {
            var id_integration = $(this).closest('tr').find('input[name=id_integration]').val();
            var id_psug = $(this).closest('tr').find('input[name=id_psug_pk]').val();
            var num_parc = $(this).closest('tr').find('input[name=num_parc_pk]').val();

            var lines = jQuery("#modalTable > tr").length

            if (lines != 1) {
                const swalWithBootstrapButtons = swal.mixin({
                    confirmButtonClass: "btn btn-danger mr-2",
                    cancelButtonClass: "btn btn-default",
                    buttonsStyling: false
                });
                swalWithBootstrapButtons({
                    title: i18nGlobals[myCountry.getCountry()].modalRemoveItem.title,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButtonYes,
                    cancelButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.cancelButtonNo
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: `/${myCountry.getCountry()}/approvals/suggest/delete`,
                            data: {
                                id_ps: id_psug,
                                id_int: id_integration,
                                num_par: num_parc,
                                id_company: id_company
                            },
                            type: 'POST',
                            datatype: 'text',
                            error: function (erro) {
                                console.log(erro)
                            }
                        }).done(response => {
                            console.log(response)
                            if (!response.delete) {
                                alert(i18nGlobals[myCountry.getCountry()].modalRemoveItem.errorRemoveItem)
                                console.log()
                            } else {
                                $(this).closest('tr').remove();
                            }
                        })

                    } else {
                        swal.close();
                    }
                })
            } else {
                const swalWithBootstrapButtons = swal.mixin({
                    confirmButtonClass: "btn btn-danger mr-2",
                    buttonsStyling: false
                });
                swalWithBootstrapButtons({
                    title: i18nGlobals[myCountry.getCountry()].cantRemoveDuplicateSuggestion,
                    type: 'warning',
                    confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButton
                })
            }

        })
    }

    load() {
        jQuery("#form").on('submit', (event) => {
            if(!this.dateValidation()){
                return false;
            }else{
                this.save();
                return true;
            }
        });
    }

    dateValidation(){
        let invalidDate = false;
        const allItems = this.setItemsToCompare(),
              transformedItems = allItems.map(item => (`${item.DATE_ARRIVAL_ALTERED}|${item.COD_ITEM_PK}`))
        
        const count = transformedItems => 
        transformedItems.reduce((a, b) => 
            Object.assign(a, {[b]: (a[b] || 0) + 1}), {})
        
        const duplicates = dict => 
            Object.keys(dict).filter((a) => dict[a] > 1)
        
        const checkDuplicatedItems = duplicates(count(transformedItems));
        console.log(checkDuplicatedItems);
        //se houver o mesmo item com a mesma data
        if (checkDuplicatedItems.length) {
            const msg = this.prepareMessageDuplicatedDates(checkDuplicatedItems)
            paginationClass.execSwal(
                "",
                `${i18nGlobals[myCountry.getCountry()].sameDate} <br/><br/> ${msg}</b>`
                , "error"
            );
            return false;
        }                

        //se alguma data estiver vazia
        allItems.forEach(item =>{            
            if(item.DATE_ARRIVAL_ALTERED == "" || item.DATE_ARRIVAL_ALTERED.length < 10){
                invalidDate = true
            }
        });

        if (invalidDate) {
            paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].invalidDate, 'warning');
            return false;
        }


        const checkInvalidQtyItems = [];
        //se alguma quantidade for inválida
        allItems.forEach(item =>{            
            if(item.QTY_ALTERED == "" || item.QTY_ALTERED < 1){
                checkInvalidQtyItems.push(`${item.QTY_ALTERED}|${item.COD_ITEM_PK}`)
            }
        });

        if (checkInvalidQtyItems.length) {
            const msg = this.prepareMessageInvalidQty(checkInvalidQtyItems)
            paginationClass.execSwal(
                "",
                `${i18nGlobals[myCountry.getCountry()].invalidQty} <br/><br/> ${msg}</b>`
                , "error"
            );
            return false;
        }

        return true;
    }

    setItemsToCompare() {
        let objSugg = {}, objArr = [];
        
        jQuery('input[name=id_integration]').each((index, element) => {           
            const parentElement = jQuery(element).parent();
            
            objSugg = {
                'ID': jQuery(element).val(),
                'QTY_ALTERED': parentElement.find('.QTY_ALTERED').val(),
                'DATE_ARRIVAL_ALTERED': parentElement.find('.DATE_ARRIVAL_ALTERED').val(),
                'OBS': parentElement.find('.txtarea_obs').val(),
                'ID_SKU_PK': parentElement.find('.ID_SKU_PK').val(),
                'COD_ITEM_PK': parentElement.find('.COD_ITEM_PK').html()
            };
            objArr.push(objSugg)            
        });
        return objArr;
    }

    prepareMessageDuplicatedDates(arrDates){
        let html = `
                <table class="table">
                    <thead>
                        <tr>
                            <th>${i18nGlobals[myCountry.getCountry()].codItem}</th>
                            <th>${i18nGlobals[myCountry.getCountry()].date}</th>
                        </tr>
                    </thead>
                    <tbody>
            `;
        for(let i = 0; i < arrDates.length; i++){
            html += `
                <tr>
                    <td>${arrDates[i].split('|')[1]}</td>
                    <td>${moment(arrDates[i].split('|')[0]).format("DD/MM/YYYY")}</td>
                </tr>
            `                        
        }
        html += '</tbody></table>'
        return html;
        
    }

    prepareMessageInvalidQty(arrDates){
        let html = `
                <table class="table">
                    <thead>
                        <tr>
                            <th>${i18nGlobals[myCountry.getCountry()].codItem}</th>
                            <th>${i18nGlobals[myCountry.getCountry()].quantity}</th>
                        </tr>
                    </thead>
                    <tbody>
            `;
        for(let i = 0; i < arrDates.length; i++){
            html += `
                <tr>
                    <td>${arrDates[i].split('|')[1]}</td>
                    <td>${arrDates[i].split('|')[0]}</td>
                </tr>
            `                        
        }
        html += '</tbody></table>'
        return html;
        
    }

    save() {
        jQuery("#estab_form").val(jQuery("#estab").val().split(" - ")[0])
        document.getElementById("saveDuplicate").disabled = true;
        document.getElementById("saveDuplicate").innerHTML = i18nGlobals[myCountry.getCountry()].saving;
    }

    close() {
        $('#modal_duplicate').hide();
    }

    searchButtonListener() {
        $('#searchDPLPage').on('click', (e) => {
            $(".dplTableContainer").hide();
            jQuery('#modaltable_load').attr("style", "display:flex !important");
            this.goToPageNumberDPL()
        });
    }


    getDplTable(page, limit, recalcFooter, order = null) {
        this.scrollLeft = jQuery("#controlSize").scrollLeft();
        $.ajax({
            url: `/${myCountry.getCountry()}/approvals/suggest/modaltable`,
            data: {
                page: page,
                limit: limit,
                order: order,
                codHeaderPk: this.id,
                recalcFooter: recalcFooter
            },
            type: 'POST',
            datatype: 'text',
            error: function (error) {
                jQuery('#modaltable_load').attr("style", "display:none !important");
                alert(i18nGlobals[myCountry.getCountry()].unavailableService);
            },
            success: function () {
                jQuery('#modaltable_load').attr("style", "display:none !important");
            }
        }).done(response => {
            jQuery('.dplTableContainer').show();
            jQuery('.dplTableContainer').html(response);
            jQuery('.dplTableContainer').append(`<input type='hidden' name='cod_header_pk' value='${$('#cod_header_pk').val()}'>`);
            jQuery(".input_just_number").keypress((e) => {
                var charCode = (e.which) ? e.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 44)) {
                    return false;
                }
            });

            jQuery(".pln-items-search").show();
            //se as informações do footer forem redefinidas, precisamos destruir a paginação e
            //instanciá-la novamente
            if (recalcFooter) {
                jQuery('#paginationDPL').twbsPagination('destroy');
            }
            this.paginate(page)
            this.paginateLegends();
            //ajusta o rodapé
            jQuery("#controlSize").scroll();
            jQuery("#controlSize").scrollLeft(this.scrollLeft);

            this.orderByDuplicate()
            this.delItem()
            utilsInstance.onlyNumbers(".input_just_number");
            utilsInstance.biggerThenZero(".input_just_number"); 
        });
    }

    paginate(startPg = 1) {
        var objPagination = jQuery('#paginationDPL');
        objPagination.twbsPagination({
            totalPages: jQuery("#total_page_number_dpl").val(),
            startPage: startPg == undefined ? 1 : startPg * 1,
            first: ' <i class="fas fa-angle-double-left"></i>',
            last: '<i class="fas fa-angle-double-right"></i>',
            prev: ' <i class="fas fa-angle-left"></i>',
            next: '<i class="fas  a fa-angle-right"></i>',
            visiblePages: 3,
            onPageClick: (_, page) => {
                if (!this.init) {
                    jQuery(".dplTableContainer").hide();
                    jQuery('#modaltable_load').attr("style", "display:flex !important");
                    this.limit = jQuery('#items_per_page_dpl').val();
                    jQuery("#pln-pagination-footer_dpl").show();
                    this.getDplTable(page, this.limit, false);
                }

                this.init = false;
            }
        })
    }

    execSwal(header, text, alertType) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            animation: false,
            text: text,
            type: alertType,
            showCancelButton: false,
            confirmButtonText: 'Ok'
        });
    }

    goToPageNumberDPL() {
        if (parseInt(jQuery("#goto_pagenumber_dpl").val()) == jQuery('#paginationDPL').twbsPagination("getCurrentPage")) {
            return false;
        } else if (parseInt(jQuery("#goto_pagenumber_dpl").val()) > parseInt(jQuery("#total_page_number_dpl").val())) {
            this.execSwal("", `${i18nGlobals[myCountry.getCountry()].table.insertPage} ${jQuery("#total_page_number_dpl").val()}`, "warning");
            return false;
        } else if (jQuery("#goto_pagenumber_dpl").val() == "" || parseInt(jQuery("#goto_pagenumber_dpl").val()) == "0") {
            this.execSwal("", i18nGlobals[myCountry.getCountry()].table.insertValidPage, "warning");
            return false;
        }
        let limit = jQuery("#items_per_page_dpl").val();
        this.pageNumber = jQuery("#goto_pagenumber_dpl").val();
        this.init = false;
        this.getDplTable(this.pageNumber, 10, true);
    }

    loadData() {
        $.ajax({
            url: `/${myCountry.getCountry()}/approvals/modal/${this.codEstab}`,
            type: 'POST',
            datatype: 'text',
            error: function (error) {
                alert(i18nGlobals[myCountry.getCountry()].unavailableService)
            }
        }).done(response => {
            //verifica se há dados no resultado da pesquisa
            this.cleanDropDown();
            this.fillSelect('#cod_sector', response.sector);
            this.fillSelect('#cod_reason', response.reason);
            this.fillSelect('#cod_buyer', response.buyer);
            this.fillSelect('#deposit', response.deposit);
            this.dtExpReq = moment(this.dtExpReq,'DD/MM/YYYY').format('YYYY-MM-DD');
            jQuery('#dt_exp_req').val(this.dtExpReq);
            jQuery("#cod_reason").val(this.reason);
            jQuery("#cod_buyer").val(this.buyer);
            jQuery("#cod_sector").val(this.sector);
            jQuery("#deposit").val(this.deposit_code);

        });
        $(".dplTableContainer").hide();
        this.getDplTable(1, this.limit, true);
    }

    cleanDropDown() {
        $('#cod_sector').empty();
        $('#cod_reason').empty();
        $('#cod_buyer').empty();
        $('#deposit').empty();
    }

    paginateLegends() {
        var totalPageNumber = jQuery("#total_page_number_dpl").val(),
            currentPage = jQuery("#paginationDPL").twbsPagination("getCurrentPage"),
            itemsPerPage = 10,
            pageItemsTo = itemsPerPage * currentPage,
            pageItemsFrom = (pageItemsTo - itemsPerPage) + 1,
            totalItems = parseInt(jQuery("#hdn_total_items_dpl").val());
        jQuery("#spn_total_page_number_dpl").html(totalPageNumber);
        jQuery("#page_items_from_dpl").html(pageItemsFrom);
        jQuery("#page_items_to_dpl").html(pageItemsTo > totalItems ? totalItems : pageItemsTo);
        jQuery("#page_items_total_dpl").html(totalItems);
        jQuery("#total_items_dpl").html(jQuery("#page_from_to_dpl").html());
    }

    fillSelect(selectId, response) {
        $("#default").remove();
        $.each(response, function (key, value) {
            //$(selectId).html('');
            $(selectId).append(`<option value='${value.ID}'>${value.ID} - ${value.VALUE}</option>`);
        });
    }

}