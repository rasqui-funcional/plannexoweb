class Approvals {
    constructor() {
        
        this.searchInput = jQuery('#search_input');
        this.status = jQuery('#aux_status');
        this.urgent = jQuery('#aux_urgent');
        this.estabs = jQuery('#aux_estabs');
        this.arrIdDates = ['DT_EXP_REQ', 'DATE_APPROVED'];
        this.utils = "";
        this.country = jQuery("#country").val();
        this.loadEvents();
    }

    loadEvents() {

        this.status.on('change', (element)=>{
            this.setsearchInput(element.target.value);
        });

        this.urgent.on('change', (element)=>{
            this.setsearchInput(element.target.value);
        });

        this.estabs.on('change', (element)=>{
            this.setsearchInput(element.target.value);
        });

        jQuery("#search").on('click', ()=> {
            paginationClass.search();
        });

        jQuery('#seach_col').on('change', (element)=>{
            this.changeFilterType(element.target.value);
        });        

        jQuery('#download').on('click', this.downloadCSV.bind(this));

        this.utils = new Utils();
        utilsInstance.onlyNumbers('#goto_pagenumber');
    }

    changeFilterType(field){
        
        this.defaultFilters();
        
        switch(field){
            case 'DT_EXP_REQ':
            case 'DATE_APPROVED':
                this.searchInput.prop('type', 'date');                
            break;
            case 'SIT_HEADER':
                this.searchInput.hide().val(this.status.val());                
                this.status.removeClass('hide');
            break;
            case 'YN_URGENT':
                this.searchInput.hide().val(this.urgent);
                this.urgent.removeClass('hide').val('Y');
            break;
            case 'COD_HEADER_PK':
            case 'COD_HEADER_ERP':
                this.searchInput.prop('type', 'number');
            break;
            case 'COD_ESTAB_FK':
                this.searchInput.hide().val(this.estabs.val());
                this.estabs.removeClass('hide');
            break;
            default:
                this.defaultFilters();
        }
    }

    defaultFilters(){
        this.searchInput
            .show()
            .prop('type', 'text')
            .val('');
        this.status.addClass('hide');
        this.urgent.addClass('hide');
        this.estabs.addClass('hide');
    }

    setsearchInput(value){
        this.searchInput.val(value);        
    }
    
    downloadCSV() {
        let searchValue = jQuery('#search_input').val();
        let searchCollumn = jQuery('#seach_col').val();
        window.open(`/${myCountry.getCountry()}/csv/approvals/${searchCollumn}/${searchValue}`)
    }
}

const approvals = new Approvals();
const paginationClass = new paginationController(`/${approvals.country}/approvals/table`);
