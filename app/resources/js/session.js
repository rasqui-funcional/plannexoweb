(function sessionTime(){
    let timeout,
        interval, 
        countDownTime,
        expired = false,
        sessionCountry = myCountry.getCountry(),
        goToLogin = jQuery('#bioIdUrl').val();

    jQuery('#expiration_confirm').on('click', function(){ 
        window.location.href = goToLogin;        
    })

    jQuery('#session').on('hidden.bs.modal', function(){   
        document.getElementById("session_message").innerHTML = "2m 00s";
    });

    document.onmousemove = function(e){
        //return if the url is formlogin, 
        if (window.location.href.indexOf("formlogin") > -1){
            return false
        };

        if(expired) return false;

        //clear variables
        jQuery('#session').modal('hide');
        clearTimeout(timeout);
        clearInterval(interval);    
        countDownTime = null;            

        //after two hours of inactivity, show a expiration message
        timeout = setTimeout(function(){

            countDownTime = new Date().getTime() + 2*60000;
            
            // Update the count down every 1 second                
            interval = setInterval(function() {

                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down time
                var timeToExpire = countDownTime - now;

                // Time calculations for minutes and seconds

                var minutes = Math.floor((timeToExpire % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((timeToExpire % (1000 * 60)) / 1000);
                
                document.getElementById("session_message").innerHTML = minutes + "m " + seconds + "s ";

                // If the count down is finished, logout and redirect to login
                if (timeToExpire <= 0) {
                    expired = true;
                    jQuery('#session_message').html(i18nGlobals[myCountry.getCountry()].sessionExpired);
                    jQuery('.to-hide').addClass('hide');
                    clearInterval(interval);
                    jQuery.ajax({
                        url: `/${sessionCountry}/expire`,
                        type: 'GET',
                    }).done(()=>{                        
                        jQuery('#expiration_confirm').removeClass('hide').addClass('show');
                    });
                }
            }, 1000);
            jQuery('#session').modal('show')
        }, 7200000);
    }
})();
