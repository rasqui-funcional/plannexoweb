let global_scroll={top:0, left:0},
    global_init  = false,
    global_alteredForm = {},
    //guarda o estado original do formulário (do jeito que é carregado do banco, sem modificações)
    global_defaultForm = {},
    global_itemsToSave = [],
    global_arrItensAnalyzed = [],
    aux_excessAlreadyLoaded,
    orderAlreadyLoaded,
    haveChanges = false,
    columnOrder = {},
    groupping_lang = {},
    haveSuggest
;

const objectClassTable = {
    approved: 'pln-tr-approved',
    analised: 'pln-tr-analised',
    normal: 'pln-tr-normal',
};

function pushInArrAnalyzed(sku_id, checked){
    const index = global_arrItensAnalyzed.findIndex(obj => obj.id==sku_id);
    if (global_arrItensAnalyzed.findIndex(obj => obj.id==sku_id) != -1) {
        global_arrItensAnalyzed[index].value = checked;
    } else {
        global_arrItensAnalyzed.push({'id':sku_id, 'value': checked});
    }
}

function validateSameDate(element, prevDate) {
    loaderShow();
    const data = mySerialize();
    const sug_id = element.parent().parent().find('input[name=id_sug_pk]').val();
    const sku_id = element.parent().parent().find('.input_an').attr('data-id-sku');
    let date = element.val(),
    codItem = element.parent().parent().find('input[name=cod_item_pk]').val();
    let arrInputValues = [];
    let obj = [];
    arrInputValues.push({"cod_item": codItem});
    arrInputValues.push({"delivery_date": moment(date).format("DD/MM/YYYY")});
    arrInputValues.push({"id_sku": sku_id});
    arrInputValues.push({"id_sug": sug_id});
    obj.push(Object.assign({}, ...arrInputValues));

    $.ajax({
        url: `/${myCountry.getCountry()}/suggest/add/checkDate`,
        data: {obj},
        type: 'POST',
        datatype: 'text',
        success: (data)=> {
            loaderHide();
            if(data.data.dateAlreadyExists.length > 0){
                element.val(prevDate)
                execSwal("",`${i18nGlobals[myCountry.getCountry()].sameDate} <br/><br/> <b>${i18nGlobals[myCountry.getCountry()].codItem} ${codItem}</b>`, "error");
            }
        },
        error: function (error) {
            loaderHide();
            console.log(error);
        }
    });
}

function setModalGroupable(params, id_sku_pk) {
    const sinTooltip = {0: i18nInfoSku[myCountry.getCountry()].headSignal.zero, 1: i18nInfoSku[myCountry.getCountry()].headSignal.veryLow, 2:i18nInfoSku[myCountry.getCountry()].headSignal.low, 3:i18nInfoSku[myCountry.getCountry()].headSignal.ideal, 4:i18nInfoSku[myCountry.getCountry()].headSignal.high, 5: i18nInfoSku[myCountry.getCountry()].headSignal.veryHigh};
    const resource = "/resources/img/INV_LEVEL_";
    const {inv_level, desc_item, cod_item_pk, qty_altered, date_arrival_altered, id_psug_pk} = params;
    
    jQuery("#info_sinalizator_groupable").prop("src", `${resource}${inv_level}.png`);
    jQuery("#sinalizator_tooltip_groupable").attr('data-original-title', `${sinTooltip[inv_level]}`);
    jQuery("#info_item_groupable").html(`${cod_item_pk}`);
    jQuery("#info_description_groupable").html(`${desc_item}`).attr("data-original-title", desc_item);
    jQuery("#groupable_qtd").val(qty_altered);
    jQuery("#groupable_date").val(date_arrival_altered);
    jQuery("#groupable_psug").val(id_psug_pk);
    jQuery("#groupable_id_sku_pk").val(id_sku_pk);

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    if(date_arrival_altered >= today) {
      jQuery("#groupable_date").attr({"min" : `${date_arrival_altered}`});
    }
    else {
      jQuery("#groupable_date").attr({"min" : `${today}`});
    }
}

function initialize(){

    jQuery('#tableSug tbody tr.deactivate td').find('textarea, input').attr("disabled", "disabled");
    
    listenStatusChange();

    utilsInstance.onlyNumbers("#goto_pagenumber, .input_just_number");
    utilsInstance.biggerThenZero(".input_just_number");

    const fullScreen = new fullScreenController();

    /*************************FUNÇÕES PARA AGRUPAMENTO DE PARCELAS*************************/
    jQuery('.more_info, #more_info_link').on('click', function(e){
      jQuery('#content-groupable').hide();
      jQuery('#content-info').show();
      jQuery('#back-modal-groupable, #close-more-info').show();
      if(e.target.id != 'more_info_link'){
        jQuery('#back-modal-groupable').hide();
      }else{
        jQuery('#close-more-info').hide();
      }
    })

    jQuery('.groupable' ).on('click', function(e){
      jQuery("#groupable_psug").val('');
      jQuery('#groupable_qtd').val('');
      jQuery('#groupable_date').val('');
      jQuery('#groupable_note').val('');
      jQuery('#groupable_id_sku_pk').val('');
      jQuery('#groupable_date').attr({'min' : ''});

      
      jQuery('#modal-body-groupable').hide();
      jQuery('#content-groupable .loader').show();

      const psug = event.target.getAttribute('data-value')            
      const id_sku_pk = event.target.getAttribute('data-sku')
      $.get( `/${myCountry.getCountry()}/suggest/sugtogroup/${psug}`)
        .done((response) => {
            setModalGroupable(response.sugtogroup, id_sku_pk)
            jQuery('#content-groupable .loader').hide();
            jQuery('#modal-body-groupable').show();
        })

      jQuery('#content-info').hide();
      jQuery('#content-groupable').show();
    })

    jQuery('#back-modal-groupable' ).on('click', function(e){
      jQuery('#content-info').hide();
      jQuery('#content-groupable').show();
    })

    jQuery('#modal_groupable').on('show.bs.modal', function (event) {
      jQuery('#content-groupable').find(".validate").each((index, element)=>{
        jQuery(element).removeClass("is-invalid");
      })
    })

    jQuery('#groupable_btn').unbind('click').on('click', function(){
      let isValid = true;
      jQuery('#content-groupable').find(".validate").each((index, element)=>{
        if( !jQuery(element).val() ){
            jQuery(element).addClass("is-invalid");                    
            isValid = false;
        }else{
            jQuery(element).removeClass("is-invalid");     
        }
      })

      if(isValid){
        const params = {
            "id_psug_pk": jQuery("#groupable_psug").val(),
            "qty_altered": jQuery('#groupable_qtd').val().trim(),
            "date_arrival_altered": jQuery('#groupable_date').val().trim(),
            "obs": jQuery('#groupable_note').val().trim(),
            "id_sku_pk": jQuery('#groupable_id_sku_pk').val().trim(),
        };

        loaderShow();

        jQuery.ajax({
          url: `/${myCountry.getCountry()}/suggest/group-installments`,
          data: params,
          type: 'POST',
          datatype: 'text',
          success: function (response) {
              jQuery('#modal_groupable').modal('hide');
              const searchValue = jQuery('#suggest_search_input').val(),
              searchCol = jQuery('#suggest-seach-col').val(),
              pageNumber = jQuery("#pagination").twbsPagination("getCurrentPage"),
              limit = jQuery("#items_per_page").val(),
              recalcFooter = true;
              global_init = false;
              getTableService(pageNumber, limit, searchValue, searchCol, recalcFooter, columnOrder);
          },
          error: function (error) {
              loaderHide();
              return error;
          }
        });
      }
    })

    /*************************FIM FUNÇÕES PARA AGRUPAMENTO DE PARCELAS*************************/

    /*************************marcadores AP*************************/
    jQuery('#ap_all').on('click', function(){
        jQuery('.input_ap').prop('checked', false)
        jQuery('tbody tr')
            .removeClass(`${objectClassTable.approved} ${objectClassTable.normal}`)
            .addClass(objectClassTable.analised);

        if(this.checked){
            jQuery('.input_ap, .input_an').prop('checked', this.checked)
            jQuery('#an_all').prop('checked', true);
            jQuery('tbody tr')
                .removeClass(`${objectClassTable.analised} ${objectClassTable.normal}`)
                .addClass(objectClassTable.approved);
        }
    });

    //marca/desmarca o checkbox AN e colore o fundo da td
    jQuery('.input_an').on('click', function(){
        //cria um objeto separado do form para enviar ao backand, pois será salvo em uma tabela diferente
        const sku_id = jQuery(this).attr('data-id-sku');
              
        pushInArrAnalyzed(sku_id, this.checked);

        if(jQuery(this).is(':checked')){
            //marca todas as parcelas com o mesmo sku_id
            jQuery(`input:checkbox[data-id-sku=${sku_id}]`).prop('checked', true);
            jQuery(`tr[data-id-sku=${sku_id}]`)
                .removeClass(`${objectClassTable.approved} ${objectClassTable.normal}`)
                .addClass(objectClassTable.analised);
        }else{
            //desmarca todas as parcelas com o mesmo sku_id
            jQuery(`[data-id-sku=${sku_id}]`).prop('checked', false)

            jQuery(`tr[data-id-sku=${sku_id}]`)
                .removeClass(`${objectClassTable.analised} ${objectClassTable.approved}`)
                .addClass(objectClassTable.normal)
                .find('.input_ap').prop('checked', false);
        }
    });

    //marca/desmarca o checkbox AP e colore o fundo da td AP e AN
    jQuery('.input_ap').on('click', function(){

        const tr = jQuery(this).parent().parent().parent();
        tr.removeClass(objectClassTable.approved).addClass(objectClassTable.analised);

        if(jQuery(this).is(':checked')){

            const sku_id = tr.find('.input_an').attr('data-id-sku');
            //marca todas as sugestões para ANALISADAS que possuem o mesmo SKU
            jQuery(`input:checkbox[data-id-sku=${sku_id}]`).prop('checked', true);
            pushInArrAnalyzed(sku_id, true);

            jQuery(`tr[data-id-sku=${sku_id}]`).each(function(){
                if( !jQuery(this).hasClass(objectClassTable.approved) ){
                    jQuery(this).addClass(objectClassTable.analised);
                }
            })

            tr.addClass(objectClassTable.approved);
            tr.removeClass(`${objectClassTable.analised} ${objectClassTable.normal}`);
        }
    });
    /*************************marcadores AN e AP fim *************************/

    //marca linha como selecionada
    jQuery('.table_tr').on('click', function(){
        jQuery(this).parent().find("tr").each(function(){
            if( jQuery(this).hasClass('pln-row-selected') ){
                jQuery(this).removeClass("pln-row-selected")
                jQuery(this).find('td').eq(0).removeClass('pln-row-selected');
                jQuery(this).find('td').eq(1).removeClass('pln-row-selected');
            }
        });
        jQuery(this).find('td').eq(0).addClass('pln-row-selected');
        jQuery(this).find('td').eq(1).addClass('pln-row-selected');
        jQuery(this).addClass('pln-row-selected');
    });

    //ordena a tabela pela coluna selecionada, de forma ASC ou DESC
    jQuery(".order-col").unbind("click").on("click", function(){
        const checkValue = setItemsToSave();

        if(checkValue.length > 0 || global_arrItensAnalyzed.length > 0){
            checkWithoutSave();
        }

        if (fullScreen.isOpen) {
            fullScreen.closeFullscreen();
        }

        const icon = jQuery(this).find("i"),
            sort = icon.hasClass("fa-sort"),
            sortAsc = icon.hasClass("fa-sort-asc"),
            columnName = jQuery(this).attr('data-id'),
            searchValue = jQuery('#suggest_search_input').val(),
            limit = jQuery("#items_per_page").val(),
            searchCol = jQuery('#suggest-seach-col').val(),
            recalcFooter = true
        ;

        let order = '';

        jQuery(this).parent().parent().find("i").each(function(){
            jQuery(this).removeClass("fa-sort-asc fa-sort-desc").addClass("fa-sort")
        });

        order = 'desc';
        
        if (sort || !sortAsc) {
          icon.removeClass('fa-sort').addClass('fa-sort-asc');
          order = 'asc';
        }else{
          icon.removeClass('fa-sort-asc').addClass('fa-sort-desc');
        }
        
        loaderShow();

        global_init = false;
        columnOrder = {[columnName] : order};
        // jQuery('#pagination').twbsPagination('destroy');
        // getTableFooter(false);
        table_footer.paginate();
        getTableService(1, limit, searchValue, searchCol, recalcFooter, columnOrder);
    });    

    //caso o usuário escolha a mesma data com código de item igual, retornamos o campo com a data antiga
    jQuery('.input_just_number, .singledate')
    .on('focusin', function(){
        jQuery(this).data('oldVal', jQuery(this).val())
    })
    //se houver alteração em quantidade alterada ou data chegada, marca campo como analisado
    .on("change", function(e){
        const prevDate = jQuery(this).data('oldVal');        
        setApprovedOnChangeField(jQuery(this));

        const codItem = validateSameDate(jQuery(this), prevDate);
    });
    
    jQuery("input, textarea, .pln-orange-checkbox").on('dblclick',function(e){
        e.stopPropagation();
    });
    
    function setApprovedOnChangeField(element){
        element.parent().parent().find(".input_ap").each(function(){
            if(!jQuery(this).is(":checked")){
                jQuery(this).trigger('click');
            }
        })
    }

    jQuery("#interdep_expiration").daterangepicker({
        singleDatePicker: true,
        "opens": "left",
        "drops": "up",
        "locale": datepickerLocaleOptions()
    });

    function datepickerLocaleOptions(){
        return {
            "format": "DD/MM/YYYY",
            'separator': ` ${i18nGlobals[myCountry.getCountry()].to.toLowerCase()} `,
            'applyLabel': i18nGlobals[myCountry.getCountry()].apply,
            'cancelLabel': i18nGlobals[myCountry.getCountry()].cancel,
            'fromLabel': i18nGlobals[myCountry.getCountry()].from,
            'toLabel': i18nGlobals[myCountry.getCountry()].to,
            'weekLabel': i18nGlobals[myCountry.getCountry()].calendar.weekLabel,
            'daysOfWeek':  i18nGlobals[myCountry.getCountry()].calendar.daysOfWeek,
            'monthNames': i18nGlobals[myCountry.getCountry()].calendar.monthNames,
            "firstDay": 1
        }
    }

    //unbind the click of elements
    jQuery("#shoppingforecast_month, #history_consumption, #stockpos_year, #stockpos_quarter, #dashboard_tab").unbind("click");
    jQuery('#erp-select').unbind("change");
    const classe = new infoSkuController(),
        basicInformationController = new BasicInformationController(),
        calendarController = new CalendarController(),
        erpClass = new erp(),
        childrenItem = new childrenItemController(),
        interdepController = new interdependenceController(),
        policyController = new PolicyController(),
        logClass = new LogController();

    listenInfoSkuBasicInfoBtnSave();
    listenModalClose();
    listenDateChangedArrival();

    jQuery('#download_csv_suggest_list, #download_csv_suggest_list_more_options').on('click', function () {
        window.open(`/${jQuery('#country').val()}/suggest/csv`)
    });
}
/******INITIALIZE FIM******/

function listenInfoSkuBasicInfoBtnSave() {
    const saveButton = jQuery('#info_sku_save_click');

    saveButton.on('click', () => {
        haveChanges = true;
    });
}

function listenStatusChange() {
    jQuery('#info_btn_status').unbind('click').on('click', () => {
        haveChanges = true;
    });
}

function listenModalClose() {
    return false;

    /**
     * FUNÇÃO COMENTADA POR CAUSA DO CARD 1074 QUE PEDIA QUE A PÁGINA NÃO FOSSE RECARREGADA APÓS ATUALIZAÇÃO DO SKU
     * 
     * CASO AJA MUDANÇA, DESCOMENTE O CÓDIGO ABAIXO, E TAMBÉM A CODINÇÃO NO MÉTODO checkWithoutSave()
     * para que recarregue a página após salvar ou não os sku's alterados (análise ou aprovação) 
     */

    // jQuery('#modal_info, #modal_manual_suggest').unbind('hidden.bs.modal').on('hidden.bs.modal', () => {
    //     if (haveChanges || haveSuggest) {
    //         checkValue = setItemsToSave();
    //         if(checkValue.length > 0 || global_arrItensAnalyzed.length > 0){
    //             checkWithoutSave();
    //         }else{
    //             loaderShow();

    //             let limit = jQuery("#items_per_page").val();
    //             let searchValue = jQuery('#suggest_search_input').val();
    //             let searchCol = jQuery('#suggest-seach-col').val();
    //             let page = jQuery('#pagination').twbsPagination("getCurrentPage");
    
    //             haveChanges = false;
    
    //             getTableService(page, limit, searchValue, searchCol, recalcFooter=false, columnOrder);
    //         }
    //     }
    // });
}

function listenDateChangedArrival() {
    const dateChangedArrival = jQuery('.suggest-date-changed-arrival');

    dateChangedArrival.on('change', function(event) {
        const date = event.target.value;
        const currentDate = new Date(date);

        if(currentDate.getDay() === 5 || currentDate.getDay() === 6) {
            jQuery(event.target).addClass('is-invalid');
        } else {
            jQuery(event.target).removeClass('is-invalid');
        }
    });
}

//abre modal de estabelecimentos com itens em excesso
jQuery("#modal_excess").on("show.bs.modal", function(event){
    const triggerElement = jQuery(event.relatedTarget),
        id = triggerElement.attr("data-id");

    if(aux_excessAlreadyLoaded != id){
        jQuery("#include_excess").html("");
        jQuery("#excess_loading").show();
        jQuery.get(`/${myCountry.getCountry()}/modals/excess/sug/${id}`, function(data){
            jQuery("#excess_loading").hide();
            jQuery("#include_excess").html(data);
        }).done(function( data ) {
            aux_excessAlreadyLoaded = id;
        });
    }
});

//abre modal de estabelecimentos com próximas ordens
jQuery("#modal_order").on("show.bs.modal", function(event){
    const triggerElement = jQuery(event.relatedTarget),
        id = triggerElement.attr("data-id");

    if(orderAlreadyLoaded != id){
        jQuery("#include_order").html("");
        jQuery("#order_loading").show();
        jQuery.get(`/${myCountry.getCountry()}/modals/orders/${id}`, function(data){
            jQuery("#order_loading").hide();
            jQuery("#include_order").html(data);
        }).done(function( data ) {
            orderAlreadyLoaded = id;
        });
    }
});

//evento do combo que altera a quantidade de itens por página que o usuário deseja visualizar.
jQuery("#items_per_page").on("change", function(){
    loaderShow();
    let limit = jQuery(this).val(),
        page = jQuery('#pagination').twbsPagination("getCurrentPage"),
        searchValue = jQuery('#suggest_search_input').val(),
        searchCol = jQuery('#suggest-seach-col').val(),
        checkValue = setItemsToSave();
    global_init = false;

    if(checkValue.length > 0 || global_arrItensAnalyzed.length > 0){
        checkWithoutSave();
    }

    table_footer.data_footer = {
        'page': 1,
        'limit': limit,
        'searchValue': searchValue,
        'searchCol': searchCol
    };
    table_footer.post();
    getTableService(1, limit, searchValue, searchCol, recalcFooter=true, columnOrder);
});

//verifica se existem alterações a serem salvas antes de mudar de página

jQuery(".back-to-the-filters, .gotoback").on("click", function(){

    let checkValue = setItemsToSave(),
        route = jQuery(this).attr("data-route");

    if(checkValue.length > 0 || global_arrItensAnalyzed.length > 0){
      checkWithoutSave(route);
    }else{
      loaderShow();
      window.location.href = route;
    }
});

//invoca métodos ao pressionar a tecla setItemsToSaveenter no input de busca ou de paginação
jQuery("#suggest_search_input, #goto_pagenumber").on("keyup", function(event) {
    const target = jQuery(this).prop('id');
    event.preventDefault();
    if (event.keyCode === 13) {
        switch(target){
            case "suggest_search_input":
                suggestSearch();
                break;
            case "goto_pagenumber":
                goToPageNumber();
                break;
        }
    }
});

//função chamada pela paginação. Retorna um dataset de acordo com os parâmetros recebidos: page e limit
function getTableService(page, limit, input, searchCol, recalcFooter, order=null){
    global_scroll.left = jQuery("#controlSize").scrollLeft();
    global_scroll.top = jQuery("#controlSize").scrollTop();

    jQuery.ajax({
        url: `/${myCountry.getCountry()}/suggest-test/table`,
        data: {
            page: page,
            limit: limit,
            searchCol: searchCol,
            searchValue: input,
            recalcFooter: recalcFooter,
            order: order
        },
        type: 'POST',
        datatype: 'text',
        success: function (response) {
            //verifica se há dados no resultado da pesquisa
            if(!checkResultSearch(response.datatable.length, 'filters')) return false;

            jQuery("#tableSug tbody").remove();
            //buildThead(response.cols)
            buildTbody(response)
            //jQuery('#tableSug').append(response);
            loaderHide();
            jQuery(".pln-items-search-sug").show();

            if (order) {
                columnOrder = order;
            }

            // paginateLegends();
            getFormOnLoad();
            jQuery('#tableSug tfoot').attr({'class' : ''});              
            jQuery("#controlSize")  
                .scrollLeft(global_scroll.left)
                .scrollTop(global_scroll.top);
            loaderHide();
            
        },
        error: function (error) {
            loaderHide();
            execSwal("", i18nGlobals[myCountry.getCountry()].unavailableService, "error");
        }
    });
}

/* 
 * Pega as informações de total e paginação para o footer
 * Desenvolvido para solucionar problemas de lentidão
*/
function tableFooterSuggest(table, url) {
    var self = this;

    this.pagination = "#pagination";
    this.start_pg = 1;
    this.page_loaded = false;
    this.url_footer = url;
    this.data_footer = {
        page: 1,
        limit: $('#items_per_page').val(),
    };
    this.response_footer = {
        pages: 0,
        count: "0",
        data: {}
    };
    this.search = {
        input: '[id*="_search_input"]',
        col: '[id*="-seach-col"]'
    };

    this.post = function () {
        $.post( self.url_footer, self.data_footer, function( response ) {
            self.response_footer = response;
            self.paginate();
            self.totals();
        });
    };

    this.totals = function() {
        let th_total = $('#tableSug tfoot tr').find('th').eq(0);
        $(`${table} tfoot tr`).html(th_total[0].outerHTML);

        $.each(self.response_footer.data, function( index, value ) {
            if (value == null || value == 'null' || value == 'NULL') {
                value = '';
            }

            if (index != "SIT_ANALYSED" && index != "SIT_SAVED") {
                $(`${table} tfoot tr`).append(`<th>${value}</th>`);
            }
        });
    };

    this.paginateLabels = function(page) {
        let items_per_page = parseInt(jQuery('#items_per_page').val());
        let current_page = page;
        let items_to = items_per_page * current_page;
        items_to = items_to > self.response_footer.count ? self.response_footer.count : items_to;
        let items_from = (items_to - items_per_page) < 0 ? 1 : (items_to - items_per_page) + 1;
        
        $("#spn_total_page_number").html(self.response_footer.pages);
        $("#page_items_total").html(self.response_footer.count);
        $("#page_items_from").html( items_from );
        $("#page_items_to").html( items_to );
        $("#total_items").html( $("#page_from_to").html() );
    };

    this.paginate = function() {
        if (self.page_loaded) {
            $(`${self.pagination}`).twbsPagination('destroy');
        }
        self.page_loaded = true;

        $(`${self.pagination}`).twbsPagination({
            totalPages: self.response_footer.pages,
            startPage: self.start_pg*1,
            first: ' <i class="fas fa-angle-double-left"></i>',
            last: '<i class="fas fa-angle-double-right"></i>',
            prev: ' <i class="fas fa-angle-left"></i>',
            next: '<i class="fas fa-angle-right"></i>',
            visiblePages: 3,
            onPageClick: function (event, page) {
                let limit = $("#items_per_page").val(),
                    searchValue = $(`${self.search.input}`).val(),
                    searchCol = $(`${self.search.col}`).val();

                self.paginateLabels(page);

                //só executa se a página foi carregada anteriormente
                if (global_init) {
                    const checkValue = setItemsToSave();

                    if(checkValue.length > 0 || global_arrItensAnalyzed.length > 0){
                        checkWithoutSave();
                    }

                    loaderShow();
                    getTableService(page, limit, searchValue, searchCol, false, columnOrder);
                
                } else {
                    global_init = true;
                }
            }
        });
    };
}

var table_footer = new tableFooterSuggest('#tableSug', `/${myCountry.getCountry()}/suggest-test/table/footer`);
table_footer.post();

//realiza a paginação e invoca getTableService() para popular a tabela com
//os dados refentes à pagina e a quantidade de itens escolhida
function paginate(startPg, total, recalcFooter) {
    var objPagination = jQuery('#pagination');
    objPagination.twbsPagination({
        totalPages: total,//jQuery("#total_page_number").val(),
        startPage: startPg == undefined ? 1 : startPg*1,
        first: ' <i class="fas fa-angle-double-left"></i>',
        last: '<i class="fas fa-angle-double-right"></i>',
        prev: ' <i class="fas fa-angle-left"></i>',
        next: '<i class="fas fa-angle-right"></i>',
        visiblePages: 3,
        onPageClick: function (event, page) {
            let limit = jQuery("#items_per_page").val(),
                searchValue = jQuery('#suggest_search_input').val(),
                searchCol = jQuery('#suggest-seach-col').val();

            //só executa se a página foi carregada anteriormente
            if (global_init) {
                const checkValue = setItemsToSave();

                if(checkValue.length > 0 || global_arrItensAnalyzed.length > 0){
                    checkWithoutSave();
                }

                if(recalcFooter) {
                    loaderShow();
                    getTableService(page, limit, searchValue, searchCol, false, columnOrder);
                }
            
            } else {
                global_init = true;
            }
        }
    })
}

//executa somente na primeira vez que a página é carregada
function getTableServiceIni(){
    loaderShow();
    // setListsInLocalStorage();
    getLang()
    
    jQuery.ajax({
        url: `/${myCountry.getCountry()}/suggest-test/table`,
        data: {
            page: 1,
            limit: 20
        },
        type: 'POST',
        datatype: 'html',
        success: function (resposta) {
            //verifica se há dados no resultado da pesquisa
            
            if(!checkResultSearch(resposta.datatable.length, 'filters')) return false;

            // jQuery('#table_insert').html(resposta);
            buildThead(resposta)
            buildTbody(resposta)
            // jQuery('#tableSug').append(resposta);
            loaderHide();
            jQuery(".pln-items-search-sug").show();
            jQuery('#tableSug tfoot').attr({'class' : ''});
            getFormOnLoad();
        },
        error: function (error) {
            loaderHide();
            execSwal("", i18nGlobals[myCountry.getCountry()].unavailableService, "error");
        }
    });
}

getTableServiceIni();

function buildThead(response) {
    let table = document.querySelector('#tableSug')
    let head = document.createElement('thead')
    let tr = document.createElement('tr')
    let columns = response.cols 
    let i18n = i18nSuggest[myCountry.getCountry()]
     
    i18n = Object.assign(i18n, groupping_lang)
    i18n = Object.assign(i18n, getOldMonths())

    for(let column in columns) {
        let th = document.createElement('th')
        let div = `<div class="d-flex flex-row pln-cur-pointer order-col align-items-center" data-id="${column}">
                    <span data-toggle="tooltip" title="${i18n[column.toLowerCase()] ? (i18n[column.toLowerCase()].title ? i18n[column.toLowerCase()].title : column) : column}">
                        ${i18n[column.toLowerCase()] ? (i18n[column.toLowerCase()].abbr ? i18n[column.toLowerCase()].abbr : i18n[column.toLowerCase()].title) : column }
                    </span>
                    <i class="fa fa-sort${columns[column].order == null ? '' : "-"+ columns[column].order} pln-link-color ml-1 align-self-center"></i>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        ${column == 'SIT_SAVED' ? '<label class="pln-container-checkbox mt-3 mb-2"><input id="ap_all" type="checkbox" /><span class="pln-orange-checkbox"></span></label>' : ''}
                    </div>`
        th.innerHTML = div
        tr.appendChild(th)
    }
    
    head.appendChild(tr)
    table.appendChild(head)
}

function buildTbody(response) {
    const {datatable, cols} = response
    let table = document.querySelector('#tableSug')
    let tbody = document.createElement('tbody')
        
    datatable.forEach(row => {
        let suggest_status = 'pln-tr-normal'
        if (row['SIT_ANALYSED']) suggest_status = 'pln-tr-analised'
        if (row['SIT_SAVED']) suggest_status = 'pln-tr-approved'
        let tr = document.createElement('tr')

        tr.classList.add(suggest_status, 'table_tr')
        
        setAttributes(tr, { 
            'id': row['ID_SKU_PK'], 
            'data-id-sku':row['ID_SKU_PK'], 
            'data-lt': row['LT_TOTAL_OUT'], 
            'data-out6': row['OUT6'] 
        })
        
        let cont = 1
        for(let column in cols) {
            let value = formatValues(column, row[column] );
            let td = buildTd(row, column, cont, value)            
            cont ++
            tr.appendChild(td)
        }
        tbody.appendChild(tr)
    })
    table.appendChild(tbody)
    initialize();
}

function buildTd(row, column, cont, value) {
    
    let td = document.createElement('td')
            let input = '', title = '', td_class = '', toggle = ''
            switch(column) {
                case 'ID_CALENDAR_FK':
                    td.className = 'text-nowrap'
                    value = getListInLocalStorage('calendars', value)
                    input = buildSpin('calendars', value)
                break;
                case 'COD_ESTAB_FK':
                   value = getListInLocalStorage('estabs', value)
                   input = buildSpin('estabs', value)
                break;
                case 'SIT_URGENT':
                    // Precisa de tradução?
                    value = value === '1' ? 'Sim' : 'Não';
                    input = `<span>${value}</span>`
                break;
                case 'ID_PROFILE':
                    let pClass = '', spin = false;
                    
                        const temp = getListInLocalStorage('politics', value)                        
                        value = temp.val
                        spin = temp.spin
                        {temp.notExists ? pClass = `politics${temp.val}` : pClass = ''}
                                                                
                    if (!$.isNumeric(value) && value.length > 12) {
                        td_class = 'text-truncate pln-td-mw-100';
                        toggle = 'tooltip';
                        title = value;
                    }

                    td.className = td_class
                    input = `<span data-toggle="${toggle}" class="${pClass}" title="${title}">
                        ${spin != undefined ? spin : value }
                        </span>`
                break;
                case 'COD_GROUP2_FK':
                case 'COD_GROUP3_FK':
                case 'COD_GROUP4_FK':
                case 'COD_GROUP6_FK':
                case 'DESC_SUPPLIER':
                    if (!$.isNumeric(value) && value.length > 12) {
                        td_class = 'text-truncate pln-td-mw-100';
                        toggle = 'tooltip';
                        title = value;
                    }

                    td.className = td_class
                    input = `<span data-toggle="${toggle}" title="${title}">
                        ${value}
                        </span>`
                break;
                case 'QTY_MULT_ORDER':
                    value = (value < 1 && value > 0) ? `0${value}` : value;

                    td.className = 'text-truncate pln-td-mw-200'
                    input = `<span data-toggle="tooltip" title="${value}">${value}</span>`
                break;
                case 'DESC_ITEM':
                    td.className = 'text-truncate pln-td-mw-200'
                    input = `<span data-toggle="tooltip" title="${value}">${value}</span>`
                break;
                case 'DATE_NEXT_IN':
                case 'QTY_NEXT_IN':
                    const diff = getDiffDays(row['DATE_NEXT_IN'])
                    
                    input = value
                    if (row['DATE_NEXT_IN'] && diff ) {
                        input = `<span class="pln-text-red">${value}</span>`
                    }
                break;
                case 'PURCHASE_REQ':
                    title = 'Estoque';
                    input = `<span class="pln-cur-pointer pln-link-color tableSug_td" data-id-sku="${row['ID_SKU_PK']}" data-change="PurchaseRequest" data-toggle="tooltip" data-original-title="${title}">${value}</span>`
                    if (value == 0) {
                      input = `<span data-toggle="tooltip" data-original-title="${title}">${value}</span>`
                    } 
                break;
                case 'INV_AVAILABLE':
                    // Precisa de tradução?
                    title = 'Estoque'; 
                    input = `<span class="pln-cur-pointer pln-link-color tableSug_td" data-id-sku="${row['ID_SKU_PK']}" data-change="OverStock" data-toggle="tooltip" data-original-title="${title}">${value}</span>`
                    if (value == 0) {
                        input = `<span data-toggle="tooltip" data-original-title="${title}">${value}</span>`
                    } 
                break;
                case 'NUM_PARC_PK':
                    td.className = 'pln-custom-tooltip'
                    let highlightClass = (row['SIT_URGENT'] == 1) ? "pln-background-red" : ""
                    input = `<div class="pln-tooltip d-block"><span class="badge badge-secondary td-data ${highlightClass}">${value}</span></div>` 
                break;
                case 'IND_EXCESS':
                    input = `<img src="/resources/img/IND_EXCESS_${value}.png">`
                    if (value) {
                        input = `<img src="/resources/img/IND_EXCESS_${value}.png" data-id="${row['ID_SUG_PK']}" data-toggle="modal" 
                            data-target="#modal_excess" class="pln-cur-pointer ind_excess_${value}">`
                    } 
                break;
                case 'QTY_ALTERED':
                    input = `<input type="text" tabindex="${cont}"
                        class="form-control form-control-sm input_just_number to-serialize"
                        value="${value}" name="${column}">`                     
                break;
                case 'DATE_ARRIVAL_ALTERED':
                    const today = moment(new Date()).format('YYYY-MM-DD')
                    let dateVal = moment(value).format('YYYY-MM-DD')
                    
                    input = `<input type="date" min="${today}"
                        class="suggest-date-changed-arrival
                        form-control form-control-sm singledate to-serialize CLASSEISWEEKEND"
                        value="${dateVal}" name="${column}" onkeydown="return false" required></input>` 
                break;
                case 'OBS':
                    td.className = 'pln-column-obs'
                    input = `
                        <textarea rows="1" class="form-control form-control-sm to-serialize pln-remove-scroll pln-textarea-resize m-0" name="${column}" maxlength="400">${value != null ? value : ''}</textarea>` 
                break;
                case 'INV_LEVEL':
                    const sinTooltip = {0: i18nInfoSku[myCountry.getCountry()].headSignal.zero, 1: i18nInfoSku[myCountry.getCountry()].headSignal.veryLow, 2:i18nInfoSku[myCountry.getCountry()].headSignal.low, 3:i18nInfoSku[myCountry.getCountry()].headSignal.ideal, 4:i18nInfoSku[myCountry.getCountry()].headSignal.high, 5: i18nInfoSku[myCountry.getCountry()].headSignal.veryHigh};
                    const resource = "/resources/img/INV_LEVEL_";                    
                    
                    input =  `<span class='pln-cur-pointer' data-toggle='tooltip' title='${value != null ? sinTooltip[value] : 'N/A'}'>
                                ${ value != null ? `<img src='${resource}${value}.png'>` : 'N/A'}
                            </span>`
                break;
                case 'ORDERS_NEXT_IN':
                    value < 1 ? input = value : ''
                    if(row['DATE_NEXT_IN'] != null) {
                        let late_order = ''

                        const diff = getDiffDays(row['DATE_NEXT_IN'])
                        
                        late_order = !( diff < 0) ? '' : 'pln-icon-color' 
                        input = `<span data-id="${row['ID_SKU_PK']}" data-toggle="modal" data-target="#modal_order" class="pln-cur-pointer ${late_order}">${value}</span>`
                    }
                break;
                case 'SIT_SAVED': 
                    td.className = 'text-center'
                    input = `                        
                        <label class="pln-container-checkbox">
                            <input ${column == 'SIT_ANALYSED' ? `data-id-sku=${row['ID_SKU_PK']}` : 'null'}                            
                                    name="${column.toLowerCase()}"
                                    class="${column == 'SIT_SAVED' ? 'input_ap' : 'input_an'} to-serialize"
                                    type="checkbox" ${value ? 'checked' : null} />
                            <span class="pln-orange-checkbox"></span>
                        </label>`
                break;
                case 'SIT_ANALYSED':                 
                    td.className = 'text-center'
                    input = `                    
                        <input name="id_sug_pk" class="to-serialize" type="hidden" value="${row['ID_SUG_PK']}"/>
                        <input name="id_sku_pk" class="to-serialize" type="hidden" value="${row['ID_SKU_PK']}"/>
                        <input name="cod_item_pk" class="to-serialize" type="hidden" value="${row['COD_ITEM_PK']}"/>    
                        <label class="pln-container-checkbox">
                            <input ${column == 'SIT_ANALYSED' ? `data-id-sku=${row['ID_SKU_PK']}` : 'null'}                            
                                    name="${column.toLowerCase()}"
                                    class="${column == 'SIT_SAVED' ? 'input_ap' : 'input_an'} to-serialize"
                                    type="checkbox" ${value ? 'checked' : null} />
                            <span class="pln-orange-checkbox"></span>
                        </label>`
                break;
                case 'USER_ID':                    
                    value = getListInLocalStorage('users', value)
                    input = buildSpin('estabs', value)
                break;
                default:
                    input = value
                break;
            }
            td.innerHTML = input
            return td
}

function buildSpin(listName, valLocalStorage) {
    
    let spin = false, 
    pClass = '',
    value;

    value = valLocalStorage.val
    spin = valLocalStorage.spin
    valLocalStorage.notExists ? pClass = `${listName}${valLocalStorage.val}` : pClass = ''
    
    input = `<span class="${pClass}">
                ${spin != undefined ? spin : value }
            </span>`
    return input
}

function getListInLocalStorage(listName, id) {
    const listInStorage = JSON.parse(localStorage.getItem('sugHelperLists'));
    let item = listInStorage[listName][id];
    
    if(!item) {
        //setando o id para 'empty' para não repetir requisição caso haja 
        //mais de um item na tabela e que não existe no localstorage
        Object.assign(listInStorage[listName], {[id]: 'empty'});        
        localStorage.setItem('sugHelperLists', JSON.stringify(listInStorage));

        jQuery.get(`/${myCountry.getCountry()}/suggest/helper/${listName}/${id}`)
        .done((response) =>{
            if (response.item) {
                Object.assign(listInStorage[listName], {[id]: response.item});
                localStorage.setItem('sugHelperLists', JSON.stringify(listInStorage));
                jQuery(`.${listName}${id}`).html(response.item);
            } else {                
                Object.assign(listInStorage[listName], response.list)
                localStorage.setItem('sugHelperLists', JSON.stringify(listInStorage));
            }
        })
        return {'val': id, 'notExists': true, 'spin': '<i class="fa fa-spin fa-spinner"></i>'}
    } 
    
    if(item == 'empty'){
        return {'val': id, 'notExists': true, 'spin': '<i class="fa fa-spin fa-spinner"></i>'}
    }
    
    return { val: item }    
}

function setListsInLocalStorage() {    
    const currentCompany = jQuery('#hiddenCompanyId').val();
    const sameCompany = currentCompany == localStorage.getItem('sugCompanyId');
    const lists = JSON.parse(localStorage.getItem('sugHelperLists'));
    //se o objeto sugHelperLists não existir ou algum dos itens (ESTAB, USERS, POLICTS, CALENDARS) estiver ausente,
    //ou se o usuário estiver logando com uma company diferente
    //, vamos no banco e populamos o localStorage novamente
    console.log('lists', lists)
    if(!lists || !sameCompany || Object.keys(lists).length != 4){
        console.log('caiuaqui')
        localStorage.removeItem('sugHelperLists');
        localStorage.setItem('sugCompanyId', currentCompany);
        
        jQuery.get(`/${myCountry.getCountry()}/suggest/helpers`)
            .done((response)=> {
                localStorage.setItem('sugHelperLists', JSON.stringify(response));
                getTableServiceIni();
            })
        return false;
    }
    getTableServiceIni();
}

function setSpecificListInLocalStorage(listItem) {
    const listInStorage = JSON.parse(localStorage.getItem('sugHelperLists'))

    jQuery.get(`/${myCountry.getCountry()}/suggest/helper/${listItem}/false`)
        .done((response) =>{
            listInStorage[listItem] = response.list;
            localStorage.setItem('sugHelperLists', JSON.stringify(listInStorage));
        })
}

function getLang(listItem) {
    jQuery.get(`/${myCountry.getCountry()}/suggest/helper/lang`)
        .done((response) =>{
            groupping_lang = response.item;
        })
}

function formatValues(column, value) {
  let input = ''
    switch(column) {
        case 'PARC_DAYS':
        case 'QTY_TARGET':
        case 'INV_DAYS':
            input = Math.round(value)
        break;
        case 'OUT_YESTERDAY_NUM_DEV':
        case 'SERVICE_LEVEL_REAL':
        case 'SERVICE_LEVEL_TARGET':                
            input = value == null ? '' : parseFloat(value).toLocaleString(myCountry.getLanguage(), {minimumFractionDigits: 2,style: 'currency',currency: myCountry.getCurrency()})
        break;    
        case 'QTY_FORECAST':
        case 'QTY_FORECAST_R3':
        case 'INV_AVAILABLE':
        case 'INV_TOTAL':
        case 'QTY_MIN_OUT':
        case 'QTY_PP':
        case 'QTY_MAX_OUT':
        case 'QTY_MIN_OUT_ORI':
        case 'QTY_MAX_OUT_ORI':
        case 'AVG_3M':
        case 'AVG_6M':
        case 'AVG_12M':
        case 'QTY_MIN_ERP':
        case 'QTY_MAX_ERP':
        case 'QTY_PP_ERP':
        case 'QTY_AVG_CONS_ERP':
        case 'OUT_LAST_30D':
        case 'OUT_MONTH_MINUS12':
        case 'OUT_MONTH_MINUS11':
        case 'OUT_MONTH_MINUS10':
        case 'OUT_MONTH_MINUS9':
        case 'OUT_MONTH_MINUS8':
        case 'OUT_MONTH_MINUS7':
        case 'OUT_MONTH_MINUS6':
        case 'OUT_MONTH_MINUS5':
        case 'OUT_MONTH_MINUS4':
        case 'OUT_MONTH_MINUS3':
        case 'OUT_MONTH_MINUS2':
        case 'OUT_MONTH_MINUS1':
        case 'OUT_CURRENT_MONTH':
	        input = Intl.NumberFormat(myCountry.getLanguage(), {maximumFractionDigits:2, style: 'decimal'}).format(Number(value))
            //ver no php return self::toFloatFormat($Value);                
        break;    
        case 'QTY_NEXT_IN':
        case 'QTY_LAST_IN':
        case 'PURCHASE_REQ':
          input = Intl.NumberFormat(myCountry.getLanguage(), {maximumFractionDigits:2, style: 'decimal'}).format(Math.round(value))
            // return self::Unity($Value);
        break;
        case 'VAL_ALTERED':
        case 'VAL_UNIT_ERP':
        case 'AVAILABLE_MONEY':
        case 'INV_AVAILABLE_MONEY':
        case 'VL_EXCESS':
        case 'TOTAL_MONEY':
        case 'INV_TOTAL_MONEY':
        case 'LAST_PURCHASE_PRICE':
        case 'QTY_INV_VAL':
        case 'VAL_TOTAL' :
        case 'VAL_UNIT':
          input = parseFloat(value).toLocaleString(myCountry.getLanguage(), {minimumFractionDigits: 2,style: 'currency',currency: myCountry.getCurrency()})
            //return self::Currency($Value);
        break;
        case 'RISK_SUPPLIER':
          input = value < 0 ? 0 : value;
            // if ($Value < 0) {
            //     return 0;
            // } else {
            //     return $Value;
            // }
        break;
        case 'DATE_ADDED':
          let day = value.substring(0,2)
          let month = value.substring(2,4)
          let year = value.substring(4,8)

          input = new Date(`${year}-${month}-${day}`).toLocaleDateString(myCountry.getLanguage())
            // $Day = substr($Value, 0, 2);
            // $Month = substr($Value, 2, 2);
            // $Year = substr($Value, 4, 4);
            // return "{$Day}/{$Month}/{$Year}";
        break;
        case 'DATE_ARRIVAL_ORIGINAL':
        case 'DATE_CREATED':
        case 'DATE_NEXT_IN':
        case 'DATE_LAST_OUT':
        case 'DATE_LAST_IN':
        case 'LAST_PURCHASE_DATE':
        case 'DATE_MATURE':
        case 'DT_EXP_REQ':
        case 'V_DATE_CONS_EXPIRATION':
        case 'DATE_TRANS':
        case 'DATE_TRANS_ALT':
        case 'DATE_REQ':
        case 'DATE_ORDER':
        case 'DATE_EXP':
        case 'DATE_LAST_CALC':
          input = new Date(value).toLocaleDateString(myCountry.getLanguage())
            // $new_value = "-";
            // $Datetime = strtotime($Value);
            // if ($Datetime) {
            //     $new_value = date('d/m/Y', $Datetime);
            // }
            // return $new_value;
        break;
        case 'OUT6':
          input = Number(value).toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
            // return number_format($Value, 2, ',', '');
        break;
        default:
            return value
        break;
    }
    return input;
}

function setAttributes(el, attrs) {
    Object.keys(attrs).forEach(key => el.setAttribute(key, attrs[key]));
}

function getDiffDays(dateNextIn) {
    const auxToday = new Date();
    const today = moment([auxToday.getFullYear(), auxToday.getMonth()+1, auxToday.getDate()])
    const date_next_in = moment(dateNextIn).format('YYYY-MM-DD')
    const end = moment([parseInt(date_next_in.split("-")[0]), parseInt(date_next_in.split("-")[1]), parseInt(date_next_in.split("-")[2])]);
    const diff = end.diff(today, 'days')
    return diff
}

function prepareDate(obj){
    for(let i=0; i< obj.length; i++){
        obj[i].DATE_ARRIVAL_ALTERED = moment(obj[i].DATE_ARRIVAL_ALTERED).format("DD/MM/YYYY")
    }

    return obj;
}

function saveData(){
    const data = setItemsToSave();

    if(data.length <= 0 && global_arrItensAnalyzed.length <=0){
        return false;
    }

    loaderShow();

    jQuery.ajax({
        url: `/${myCountry.getCountry()}/suggest/save`,
        data: {
            obj: prepareDate(data),
            analyzed: global_arrItensAnalyzed
        },
        type: 'POST',
        datatype: 'text',
        success: function (resposta) {
            getFormOnLoad();
            global_arrItensAnalyzed=[];
            global_itemsToSave=[];
            const searchValue = jQuery('#suggest_search_input').val(),
            searchCol = jQuery('#suggest-seach-col').val(),
            pageNumber = jQuery("#pagination").twbsPagination("getCurrentPage"),
            limit = jQuery("#items_per_page").val(),
            recalcFooter = true;

            global_init = false;
    
            getTableService(pageNumber, limit, searchValue, searchCol, recalcFooter, columnOrder);
            
        },
        error: function (error) {
            console.log ('response',error)
            loaderHide();
            alert(i18nGlobals[myCountry.getCountry()].unavailableService)
        }
    });
}

function mySerialize(){
    let arrInputValues = []
        , objInputValues = [];
    jQuery("#tableSug tbody").find("tr").each((index, element)=>{
        jQuery(element).find(".to-serialize").each((index, element)=>{
            arrInputValues.push( {
                [prepareInputName(jQuery(element).attr("name"))]:
                ( jQuery(element).prop("type") == "checkbox" ?
                    ( jQuery(element).prop("checked") == true ? 1 : 0)
                    : jQuery(element).val()
                )
            } );
        });

        if(arrInputValues.length > 0){
            objInputValues.push(Object.assign({}, ...arrInputValues));
        }

        arrInputValues=[];
    });

    return objInputValues;
}

function prepareInputName(name){
    return name.split("-")[0];
}

//set os itens que foram alterados para que sejam salvos no banco de dados
function setItemsToSave(){
    global_alteredForm = mySerialize();

    global_itemsToSave=[];
    global_alteredForm.forEach(altered_element => {
        if(JSON.stringify(global_defaultForm).indexOf(JSON.stringify(altered_element)) == -1){
            global_itemsToSave.push(altered_element)
        }
    });

    return global_itemsToSave;
}

//informa ao usuário que ele está tomando alguma ação antes de salvar os dados e que eles podem ser perdidos
function checkWithoutSave(gotoback, reloadSearch){
    let msgText = i18nGlobals[myCountry.getCountry()].table.itemsSaveSuccess,
        msgType = 'success';
    
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn pln-btn-orange ml-2',
        cancelButtonClass: 'btn btn-default',
        buttonsStyling: false,
    });


    let limit = jQuery("#items_per_page").val();
    let searchValue = jQuery('#suggest-search-input').val();
    let searchCol = jQuery('#suggest-seach-col').val();
    let page = jQuery('#pagination').twbsPagination("getCurrentPage");
    haveChanges = false;

    swalWithBootstrapButtons({
        reverseButtons: true,
        animation: false,
        text: i18nGlobals[myCountry.getCountry()].table.saveWarning,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: i18nGlobals[myCountry.getCountry()].save,
        showLoaderOnConfirm: true,
        cancelButtonText: i18nGlobals[myCountry.getCountry()].cancel,
        onClose: function () {
            global_itemsToSave = [];
            global_arrItensAnalyzed = [];
        },
        preConfirm: () => {
            return jQuery.ajax({
                url: `/${myCountry.getCountry()}/suggest/save`,
                data: {
                    obj:  prepareDate(global_itemsToSave),
                    analyzed: global_arrItensAnalyzed
                },
                type: 'POST',
                datatype: 'text',
                success: function (response) {
                    
                    if(response.data !== 'true'){
                        msgText = i18nGlobals[myCountry.getCountry()].unavailableService;
                        msgType = 'error';
                    }
                },
                error: function (error) {
                    console.log('response', error)
                    alert(i18nGlobals[myCountry.getCountry()].unavailableService)
                }
            });
        },
        allowOutsideClick: () => false
    }).then((result) => {
        if (result.value) {
            swalWithBootstrapButtons({
                animation: false,
                text: msgText,
                type: msgType
            })
            if (gotoback) {
                loaderShow();
                window.location.href = gotoback;
            }
            if(reloadSearch){
                loaderShow();
                getTableService(page, limit, searchValue, searchCol, recalcFooter=false, columnOrder);
            }

        } else if (
            result.dismiss === swal.DismissReason.cancel
        ) {
            global_itemsToSave = [];
            global_arrItensAnalyzed = [];
            if (gotoback) {
                loaderShow();
                window.location.href = gotoback;
            }
            if(reloadSearch){
                loaderShow();
                getTableService(page, limit, searchValue, searchCol, recalcFooter=false, columnOrder);
            }
        }
    });
}

/*recebe o resultado da consulta ao banco de dados, se não houver itens,
exibe um alerta e retorna para a página de filtros*/
function checkResultSearch(val, origin){
    if(val == 'false'){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            animation: false,
            text: i18nGlobals[myCountry.getCountry()].table.noItemsToDisplay,
            type: "warning",
            showCancelButton: false,
            confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButton,
            allowOutsideClick: () => false,
            preConfirm: () => {
                if(origin == 'filters'){
                    window.location.href = `/${myCountry.getCountry()}/suggest-test/filters`;
                }else{
                    loaderHide();
                }
            }
        });

        return false;
    }

    return true;
}

//popula o objeto com os dados do formulário carregado, para comparar com outro objeto
//que armazenará os dados das alterações feitas pelo usuário.
function getFormOnLoad(){
    global_defaultForm = mySerialize();
}

/**
 * Buscar sugestão
 */
function suggestSearch(){
    loaderShow();
    let searchValue = jQuery('#suggest_search_input').val();

    const searchCol = jQuery('#suggest-seach-col').val(),
          checkValue = setItemsToSave(),
          limit = jQuery("#items_per_page").val(),
          recalcFooter = true;

    global_init = false;
    columnOrder = null;

    if(searchCol == 'cod_item_pk'){
        searchValue = searchValue.replace(/ /g,'');
    }

    if(checkValue.length > 0 || global_arrItensAnalyzed.length > 0){
        checkWithoutSave(false, true);
    }else{
        getTableService(1, limit, searchValue, searchCol, recalcFooter, columnOrder);
        
        table_footer.data_footer = {
            'page': 1,
            'limit': limit,
            'searchValue': searchValue,
            'searchCol': searchCol
        };
        table_footer.post();
    }
}

//ir para página X (Buscar)
function goToPageNumber(){
    const checkValue = setItemsToSave();
    if(checkValue.length > 0 || global_arrItensAnalyzed.length > 0){
        checkWithoutSave();
    }

    if( parseInt(jQuery("#goto_pagenumber").val()) == jQuery('#pagination').twbsPagination("getCurrentPage") ){
        return false;
    }

    if( parseInt(jQuery("#goto_pagenumber").val()) > parseInt(jQuery("#total_page_number").val()) ){
        execSwal("",`${i18nGlobals[myCountry.getCountry()].table.insertPage} ${jQuery("#total_page_number").val()}`, "warning");
        return false;
    }

    if( jQuery("#goto_pagenumber").val() == "" || parseInt(jQuery("#goto_pagenumber").val()) =="0" ){
        execSwal("", i18nGlobals[myCountry.getCountry()].table.insertValidPage, "warning");
        return false;
    }

    loaderShow();
    const searchValue = jQuery('#suggest_search_input').val(),
          searchCol = jQuery('#suggest-seach-col').val(),
          pageNumber = jQuery("#goto_pagenumber").val(),
          limit = jQuery("#items_per_page").val(),
          recalcFooter = true;

    global_init = false;

    table_footer.start_pg = pageNumber;
    table_footer.paginate();
    getTableService(pageNumber, limit, searchValue, searchCol, recalcFooter, columnOrder);
}

function paginateLegends(data){
    let totalPageNumber = parseInt(data.pages),//jQuery("#total_page_number").val(),
        itemsPerPage = parseInt(jQuery('#items_per_page').val()),
        currentPage = jQuery("#pagination").twbsPagination("getCurrentPage"),
        pageItemsTo = itemsPerPage * currentPage,
        pageItemsFrom = (pageItemsTo - itemsPerPage) + 1,
        totalItems =  parseInt(data.count); //parseInt(jQuery("#hdn_total_items").val());

    jQuery("#spn_total_page_number").html( totalPageNumber );
    jQuery("#page_items_from").html( pageItemsFrom );
    jQuery("#page_items_to").html( pageItemsTo > totalItems ? totalItems : pageItemsTo );
    jQuery("#page_items_total").html( totalItems );

    jQuery("#total_items").html( jQuery("#page_from_to").html() );
}

function execSwal(header, text, alertType){
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn pln-btn-orange',
        buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
        animation: false,
        html: text,
        type: alertType,
        showCancelButton: false,
        confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButton
    });
}

function getOldMonths() {
  let months = {}

  myCountry.getCountry() == 'br' ? moment.locale('pt-BR') : moment.locale('es');
  months[`out_current_month`] = {
    title: moment().format("MMM/YYYY")
  }

  for (let index = 1; index < 13; index++) {
      let date = new Date();
      months[`out_month_minus${index}`] = {
          title: moment(date).subtract(index, 'months').format("MMM/YYYY")
      }
  }

  return months
}
