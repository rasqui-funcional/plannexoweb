class manualSuggestController{
    constructor(){
        this.arrEstabs;
        this.codItemObject = [];
        this.btnAddSuggestion = jQuery("#suggestionValidate");
        this.idIncrement = 0;
        this.table = jQuery('#tableManualSug > tbody');
        this.descItem = "";
        this.listerBtns();
        this.errorMessage = i18nGlobals[myCountry.getCountry()].unavailableService;
    }

    listerBtns(){
        //setting the thead table fixed
        jQuery("#tableManualSug").tableHeadFixer({head: true, foot: true});

        //adding a new suggestion line
        jQuery("#newSuggest").on("click", ()=>{
            this.newSuggestion();
        });

        //save suggestion in database
        this.btnAddSuggestion.on("click", ()=>{
            this.suggestionValidate();
        });

        jQuery('#open_modal_manual_suggest').on('click', ()=>{
            this.openModal();
        });
    }

    openModal() {
        jQuery('#modal_manual_suggest').modal('show');
        if( this.getLengthTableManualSug() == 0) {
            this.newSuggestion();
        }
        if(this.arrEstabs == undefined) this.getEstabs();
    }

    getLengthTableManualSug() {
        return jQuery('#tableManualSug >tbody >tr').length
    }
    
    newSuggestion(){
        let tr = document.createElement("tr");
        tr.innerHTML = `
            <td class="pln-td-w100">
                <input type="text" name="cod_item" id="item_${this.idIncrement}" class="form-control form-control-sm pln-el-maxw-100 validate">
                <div class="invalid-feedback">${i18nManualSuggest[myCountry.getCountry()].invalidField}</div>
                <input type="hidden" name="id_sku">
            </td>
            <td>
                <select id="estab_${this.idIncrement}" name="estab_fk" class="form-control form-control-sm validate">
                </select>
                <div class="invalid-feedback">${i18nManualSuggest[myCountry.getCountry()].invalidField}</div>
            </td>
            <td class="pln-el-maxw-100">
                <div class="d-flex text-nowrap">
                    <span class="text-truncate desc_item" id="desc_tooltip_${this.idIncrement}" data-toggle="tooltip" title=""></span>
                </div>
            </td>
            <td>
                <input type="text" name="qtd_un" id="qtde_${this.idIncrement}" class="form-control form-control-sm pln-el-maxw-100 qtdeun validate">
                <div class="invalid-feedback">${i18nManualSuggest[myCountry.getCountry()].invalidField} ${i18nManualSuggest[myCountry.getCountry()].greaterThanZero}</div>
            </td>
            <td>
                <input type="text" name="delivery_date" class="form-control form-control-sm pln-el-maxw-100 validate" id="delivery_date_${this.idIncrement}">
                <div class="invalid-feedback">${i18nManualSuggest[myCountry.getCountry()].dateArrivalAlteredExists}</div>
            </td>
            <td>
                <div class="d-flex flex-row">
                    <div>
                        <input type="radio" class="validate" id="urgent_s${this.idIncrement}" name="sit_urgent-${this.idIncrement}" value="1"><label for="urgent_s${this.idIncrement}">${i18nGlobals[myCountry.getCountry()].yes}</label>
                    </div>
                    <div class="ml-2">
                        <input type="radio" class="validate" id="urgent_n${this.idIncrement}" name="sit_urgent-${this.idIncrement}" checked value="0"><label for="urgent_n${this.idIncrement}">${i18nGlobals[myCountry.getCountry()].no}</label>
                    </div>
                </div>
            </td>
            <td>
                <div class="d-flex flex-row">
                    <div>
                        <input type="radio" class="validate" id="aprovado_s${this.idIncrement}" name="sit_aproved-${this.idIncrement}" value="1"><label for="aprovado_s${this.idIncrement}">${i18nGlobals[myCountry.getCountry()].yes}</label>
                    </div>
                    <div class="ml-2">
                        <input type="radio" class="validate" id="aprovado_n${this.idIncrement}" name="sit_aproved-${this.idIncrement}" checked value="0"><label for="aprovado_n${this.idIncrement}">${i18nGlobals[myCountry.getCountry()].no}</label>
                    </div>
                </div>
            </td>
            <td>
            <span class="pln-danger pln-cur-pointer delete_row"><i class="fa fa-times pln-danger"></i> <span>${i18nManualSuggest[myCountry.getCountry()].delete}</span></span>
            </td>        
        `;
                
        this.disableAddSuggestion(false);
        this.addEventsTr(tr);
        this.table.append(tr);
        this.applyEventsToTr();
    }

    /**
     * It builds the estab select based on which cod item the user types
     * @param {object} response with cod item information(COD_ESTAB_FK, etc)
     * @param {object} row the row where the user is in the moment 
     */
    setEstabs(response, row) {
        this.setEstabstoEmpty(row)

        const field = row.parent().parent().find("select[name=estab_fk]")

        if(this.arrEstabs){
            const result = response.data.filter(item => Object.keys(this.arrEstabs).includes(item.COD_ESTAB_FK))
            result.forEach((item)=> {
                field.append(`<option value="${item.COD_ESTAB_FK}">${this.arrEstabs[item.COD_ESTAB_FK]}</option>`)
            });

            this.setSkuId(field);
        }
    }

    /**
     * Clears the options on the estab select
     * @param {object} row the row where the user is in the moment 
     */
    setEstabstoEmpty(row) {
        row.parent().parent().find("select[name=estab_fk]").html('')
    }


    suggestionValidate(){
        let isValid = true;
            
        this.table.find("tr").each((index, element)=>{
            jQuery(element).find(".validate").each((index, element)=>{
                if( !jQuery(element).val() ){
                    jQuery(element).addClass("is-invalid");                    
                    isValid = false;                    
                }else{
                    jQuery(element).removeClass("is-invalid");                    
                }
            })
        });

        this.table.find("tr").each((index, element)=>{
            jQuery(element).find('[name=qtd_un]').each((index, element)=>{
                if( !jQuery(element).val() || jQuery(element).val() <= 0){
                    jQuery(element).addClass("is-invalid");                    
                    isValid = false;                    
                }else{
                    jQuery(element).removeClass("is-invalid");                    
                }
            })
        });

        const duplicateDates = this.setItemsToCompare();

        if(duplicateDates.length > 0) {
            this.setDateArrivalAlreadyExists(duplicateDates);
            isValid = false;
        }

        if(isValid) {
            this.addSuggestion();
        }
    }

    setItemsToCompare() {
        let objSugg = {}, objArr = [];
        
        this.table.find('tr').each((index, element) => {
            objSugg = {
                'cod_item': jQuery(element).find('[name=cod_item]').val(),
                'date': jQuery(element).find('[name=delivery_date]').val(),
            };

            objArr.push(objSugg)            
        });

        //aqui eu transformo o objeto em uma string 'data|item' para poder comparar datas duplicadas para o mesmo item
        const transformedItems = objArr.map(item => (`${item.date}|${item.cod_item}`))

        const count = transformedItems => 
        transformedItems.reduce((a, b) => 
            Object.assign(a, {[b]: (a[b] || 0) + 1}), {})
        
        const duplicates = dict => 
            Object.keys(dict).filter((a) => dict[a] > 1)
            
        const checkDuplicatedItems = duplicates(count(transformedItems));
        
        const arrNewObj = [];

        /*Após identificar as datas repetidas, transformo em objeto novamente para poder aproveitar o método setDateArrivalAlreadyExists
        * que recebe um objeto para marcar em vermelho as datas que estão se repetindo e avisar ao usuário que isso não é permitido
        */
        checkDuplicatedItems.forEach(element => {
            arrNewObj.push(
                {
                    cod_item: element.split('|')[1], 
                    date: element.split('|')[0],
                }
            );
        });

        return arrNewObj;
    }

    addSuggestion(){
        let arrInputValues = [];
        let objInputValues = [];

        this.table.find("tr").each((index, element)=>{
            jQuery(element).find("input, select").each((index, element)=>{
                arrInputValues.push( {
                    [this.prepareInputName(jQuery(element).attr("name"))]: 
                    ( jQuery(element).prop("type") == "radio" ? 
                        ( jQuery(element).prop("checked") == true ? 0 : 1) 
                        : jQuery(element).val()
                    )
                } );
            })
            if(arrInputValues.length > 0){
                objInputValues.push(Object.assign({}, ...arrInputValues));
            }
            arrInputValues=[];
        });

        this.sendSuggToInsert(objInputValues);
    }

    prepareInputName(name){
        return name.split("-")[0];
    }

    addEventsTr(tr){
        //add event to delete row
        tr.querySelector(".delete_row").addEventListener("click", (element)=>{
            tr.remove();
            if( this.getLengthTableManualSug() == 0){
                this.disableAddSuggestion(true);
            }
        });

        //add event to find desc item by cod item and estab id, on item_ change
        tr.querySelector(`#item_${this.idIncrement}`).addEventListener("change", (element)=>{
            this.getDescSkuValdate(jQuery(element.currentTarget));
        });

        //add event to find desc item by cod item and estab id, on estab_ change
        tr.querySelector(`#estab_${this.idIncrement}`).addEventListener("change", (element)=>{
            this.setSkuId(jQuery(element.currentTarget));
            this.getDescSkuValdate(jQuery(element.currentTarget).parent().parent().find('[name=cod_item]'), jQuery(element.currentTarget).val())
        });
    }

    applyEventsToTr(){
        utilsInstance.onlyNumbers(`#qtde_${this.idIncrement}`);
        utilsInstance.biggerThenZero(`#qtde_${this.idIncrement}`);
        utilsInstance.alphaNumeric(`#item_${this.idIncrement}`);
        this.setDatepicker(`#delivery_date_${this.idIncrement}`);
        jQuery(`#desc_tooltip_${this.idIncrement}`).tooltip();
        this.idIncrement++;
    }

    disableAddSuggestion(param){
        this.btnAddSuggestion.prop("disabled", param);
    }

    /**
     * Validates if there is an SKU written in the field and calls for the AJAX
     * @param {object} element the element in which the user wrote the cod
     */
    getDescSkuValdate(element, codEstab = null){
        let codItem, obj = {};
    
        codItem = element.val();

        if(codItem){
            obj = {
                'codItem': codItem,
                'codEstab': codEstab
            };

            this.miniLoader(element);
            this.getDescSku(element, obj);
        }
    }

    miniLoader(element){
        jQuery(element).parent().parent()
            .find(".desc_item")
            .html("<div class='mini_loader'></div>");
    }

    getDescSku(element, params){
        this.disableAddSuggestion(true);               
        
        $.ajax({
            url: `/${myCountry.getCountry()}/suggest/add/getskudescription`,
            data: {
                "cod_item": params.codItem,
                "cod_estab_fk": params.codEstab,
            },
            type: 'POST',
            datatype: 'text',
            success: (response)=> {
                this.setDescItem(element, response.data[0]);
                this.disableAddSuggestion(false);
                if(params.codEstab === null){
                    this.setEstabstoEmpty(element);

                    this.codItemObject = response.data;
    
                    if (response.data.length) {
                        this.setEstabs(response, element)    
                    }
                }
            },
            error: function (error) {
                jQuery(element).parent().parent().find(".desc_item").html("");//remove loader
                jQuery(element).val('');//retira o valor
                jQuery(element).parent().parent().find("select[name=estab_fk]").html('')//remove os estabelecimentos
                alert(i18nGlobals[myCountry.getCountry()].insertedDataError);
            }
        });
    }

    getEstabs(){
        const { estabs } = JSON.parse(localStorage.getItem('sugtHelperLists'));
        this.arrEstabs = estabs;
    }

    resetTalbe(){
        jQuery("#tableManualSug > tbody").html("");
        this.newSuggestion();
    }

    changeStateOnSaving(saving){
        if(saving){
            this.btnAddSuggestion.html(`<i class='fa fa-spin fa-spinner'></i> ${i18nGlobals[myCountry.getCountry()].adding}`);
        }else{
            this.btnAddSuggestion.html(i18nGlobals[myCountry.getCountry()].add);
        }
    }

    sendSuggToInsert(obj){
        this.disableAddSuggestion(true);
        this.changeStateOnSaving(true);
        $.ajax({
            url: `/${myCountry.getCountry()}/suggest/add`,
            data: {obj},
            type: 'POST',
            datatype: 'text',
            success: (response)=> {
                
                if(response.data.dateAlreadyExists.length > 0) {
                    this.setDateArrivalAlreadyExists(response.data.dateAlreadyExists);
                    this.disableAddSuggestion(false);
                    this.changeStateOnSaving(false);
                    return false;
                }
                haveSuggest = true;
                jQuery("#modal_manual_suggest").modal("hide");                
                this.resetTalbe();
                this.execSwal("", i18nManualSuggest[myCountry.getCountry()].addedSuccess, "success");
                this.idIncrement = 0;
                this.disableAddSuggestion(false);
                this.changeStateOnSaving(false);
            },
            error: function (error) {
                console.log(error);
                this.disableAddSuggestion(false);
                this.changeStateOnSaving(true);
            }
        });
    }

    setDateArrivalAlreadyExists(objDates) {
        this.table.find('[name=delivery_date]').removeClass('is-invalid');
        objDates.forEach(objItem => {
            this.table.find('tr').each((index, element)=>{
                
                const codItem = jQuery(element).find('[name=cod_item]').val(),
                      dateArrival = jQuery(element).find('[name=delivery_date]');
                      
                if( objItem.cod_item == codItem && objItem.date == dateArrival.val()) {
                    dateArrival.addClass('is-invalid');
                }
            });    
        });
        
    }

    setDescItem(element, response){
        if(!response){
            element.parent().parent().find(".desc_item")
            .attr("data-original-title", i18nGlobals[myCountry.getCountry()].table.itemNotFound)
            .html(`<span class='pln-danger invalidform'>${i18nGlobals[myCountry.getCountry()].table.itemNotFound.toLowerCase()}</span>`);
        }else{
            element.parent().parent().find(".desc_item")
            .attr("data-original-title", response.DESC_ITEM)
            .html(response.DESC_ITEM);

            element.parent().parent().find("[name=cod_item]").removeClass("pln-invalid-field");
        }
    }

    /**
     * Changes the hidden id_sku input after choosing the estab
     * @param {object} estabSelect the estab selected by the user
     */
    setSkuId(estabSelect) {
        const result = this.codItemObject.filter( item =>  item.COD_ESTAB_FK == estabSelect.val())

        estabSelect.parent().parent().find("[name=id_sku]").val(result[0].ID_SKU_PK);
    }

    execSwal(header, text, alertType){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            animation: false,
            text: text,
            type: alertType,
            showCancelButton: false,
            confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButton,
        });
    }

    setDatepicker(element){
        jQuery(element).daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY',
                'separator': ` ${i18nGlobals[myCountry.getCountry()].to.toLowerCase()} `,
                'applyLabel': i18nGlobals[myCountry.getCountry()].apply,
                'cancelLabel': i18nGlobals[myCountry.getCountry()].cancel,
                'fromLabel': i18nGlobals[myCountry.getCountry()].from,
                'toLabel': i18nGlobals[myCountry.getCountry()].to,
                'weekLabel': i18nGlobals[myCountry.getCountry()].calendar.weekLabel,
                'daysOfWeek':  i18nGlobals[myCountry.getCountry()].calendar.daysOfWeek,
                'monthNames': i18nGlobals[myCountry.getCountry()].calendar.monthNames,
                "firstDay": 1
    
            }
        });
    }
}
const classe = new manualSuggestController();