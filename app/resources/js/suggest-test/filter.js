let formSerielized = [];

jQuery(function () {
    let show_date_label = jQuery("#show_date_label"),
        inputDatarangeGroup = jQuery('#inputDatarangeGroup'),
        today = new Date();

    const intervalDiv = jQuery("#interval");

    utilsInstance.onlyNumbers("#num_parc_pk");

    //apply popover to page info helper
    jQuery('[data-toggle="popover"]').popover();

    //apply tooltip to page title
    jQuery('[data-toggle=tooltip]').tooltip();

    //controla botão Deletar quando a modal abre
    jQuery("#modal_edit").on("show.bs.modal", function () {
        jQuery("#select_filtro").val() == "" ? jQuery("#confirm_delete").addClass("invisible") : jQuery("#confirm_delete").removeClass("invisible");
    });
    //controla o validador do campo nome quando a modal fecha
    jQuery("#modal_edit").on("hide.bs.modal", function () {
        jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});
    });

    jQuery('.chk_check_all').click(function () {
        jQuery(this).parent().parent().parent().find('input:checkbox').prop('checked', this.checked);
    });

    jQuery('#data_alterada_chegada').on('change', e => {
        const startDate = moment(today),
            nextWeek = moment(today).add(6, 'd'),
            nextMonth = moment(today).add(1, 'M'),
            startOfMonth = moment(today).startOf('month'),
            endOfMonth = moment(today).endOf('month'),
            startOfNextWeek = moment().startOf('week').add(8, 'd'),
            endOfNextWeek = moment().endOf('week').add(8, 'd'),
            startOfNextMonth = moment(today).add(1, 'M').startOf('month'),
            endOfNextMonth = moment(today).add(1, 'M').endOf('month');

        jQuery("#interval_end, #interval_start").val('');
        switch(e.target.value) {
            case "":
                show_date_label.hide();
                jQuery(".interval").attr("style", "display:none !important");
                jQuery("#interval_select").val('');
                jQuery(".interval-dates").attr("style", "display:none !important");
                break;
            case "PERSONALIZADO":
                jQuery('#interval_select').val('interval').change();
                jQuery('#interval_start, #interval_end').val(startDate.format('YYYY-MM-DD'));
                show_date_label.hide();
                jQuery(".interval").attr("style", "display:flex !important");
                break;
            case "PR_7DIAS":
                updateIntervalDates(startDate, nextWeek);
                break;
            case "PR_30DIAS":
                updateIntervalDates(startDate, nextMonth);
                break;
            case "PR_SEMANA":
                updateIntervalDates(startOfNextWeek, endOfNextWeek);
                break;
            case "MES_ATUAL":
                updateIntervalDates(startOfMonth, endOfMonth);
                break;
            case "PR_MES":
                updateIntervalDates(startOfNextMonth, endOfNextMonth);
                break;
        }
    })

    function updateIntervalDates(startDate, endDate) {
        show_date_label.show();
        show_date_label.html(`${startDate.format('DD/MM/YYYY')} ${i18nGlobals[myCountry.getCountry()].to.toLowerCase()} ${endDate.format('DD/MM/YYYY')}`);
        jQuery("#interval_start").val(startDate.format('YYYY-MM-DD'));
        jQuery("#interval_end").val(endDate.format('YYYY-MM-DD'));
        jQuery("#interval_days").val('');
        jQuery(".interval").attr("style", "display:none !important");
    }

    jQuery("#interval_select").on('change', e => {
        let today = new Date(),
            defaultDate =  moment(today);
        if(jQuery("#data_alterada_chegada").val() === 'PERSONALIZADO'){
            switch (e.target.value) {
                case "interval":
                    jQuery('.interval-dates').attr('style','display:flex !important');
                    jQuery('.interval-days').attr('style','display:none !important');
                    jQuery("#interval_start, #interval_end").val(defaultDate.format('YYYY-MM-DD'));
                    break;
                case "next":
                    jQuery('.interval-days').attr('style','display:flex !important');
                    jQuery('.interval-dates').attr('style','display:none !important');
                    jQuery("#interval_start, #interval_end").val('');
                    break;
            }
        }
    });

    /**************POPULA OS FILTROS SE HOUVER FILTRO SALVO NA SESSION DO PHP***************/
    //filter_temp é uma variável do arquivo filters.php que recebe os dados dos ultimos filtros
    //que foram salvos na SESSION para popular o form automaticamente se o usuário voltar para a tela após uma pesquisa
    if (filter_temp) editForm(filter_temp)
    /**************FIM***************/

    /*********************EXIBIÇÃO E ORDENAÇÃO************************/
    $('#div_exibition, #div_order').multisortable({
        items: "div.div_exibition_child",
        selectedClass: "selected"
    });

    //$('#div_order').sortable();
    $("#div_order, #div_exibition").disableSelection();

    //cria o evento de  duplo click para a lista de Exibição
    jQuery("#div_exibition div").on('dblclick', function name(e) {
        var clicked_el = jQuery(this);
        triggerToOrder(clicked_el, e);
    });

    jQuery("#div_exibition").find('.fa-arrow-right').on('click', function name(e) {
        var clicked_el = jQuery(this);
        triggerToOrder(clicked_el, e, parent = true);
    });
    /*********************EXIBIÇÃO E ORDENAÇÃO FIM************************/

    /*********************ACIONANDO MEUS FILTROS************************/
    jQuery("#select_filtro").on("change", function () {
        cleanForm('formulario');

        if (jQuery(this).val() == "") {
            //$("#btnfiltro_salvar").prop("disabled",true);
            jQuery("#btn_filtro_text").text(i18nGlobals[myCountry.getCountry()].filters.saveFilter);
            jQuery("#filter_nome").val("");
            jQuery("#modal_titulo").text(i18nGlobals[myCountry.getCountry()].filters.newFilter);
            jQuery("#btnfiltro_excluir").hide();
        } else {
            loaderShow();
            jQuery("#btn_filtro_text").text(i18nGlobals[myCountry.getCountry()].filters.editFilter);
            jQuery("#filter_nome").val(jQuery("select[name=select_filtro] option:selected").text());
            jQuery("#modal_titulo").text(i18nGlobals[myCountry.getCountry()].filters.editFilter);
            jQuery("#btnfiltro_excluir").show();

            $.ajax({
                url: `/${myCountry.getCountry()}/ajax/filter/getFilterById`,
                type: 'POST',
                dataType: 'json',
                data: {
                    filter_id: jQuery(this).val()
                },
                success: function (response) {
                    editForm(response.data[0].FILTER_DATA);
                    setApplyedFilter(response.data);
                    loaderHide();
                },
                error: function (error) {
                    loaderHide();
                }
            });
        }
    });

    jQuery("#btn_filtro").on("click", function () {
        jQuery("#filter_nome").val(jQuery("select[name=select_filtro] option:selected").text());
    });

    /*********************ACIONANDO MEUS FILTROS FIM************************/
    $('.progress-bar').css("background-color", "#F96737");
    $('.progress .progress-bar').css("width", function () {
        return $(this).attr("aria-valuenow") + "%";
    });
});

sortable = jQuery("#div_order");

/*dispara tanto o evento de CTRL click quanto duplo click e insere a coluna
dentro do box de ordenação*/
function triggerToOrder(element, event, elParent) {
    //dependendo da chamada desta função
    elParent ? element = element.parent() : element = element;

    let elToOrder = element.find('input').attr('value').trim();

    //Insere somente se não existir este elemento no box
    if (sortable.find(`div[id="${elToOrder}-order"]`).length <= 0) {
        addOrder(elToOrder, element.text(), 'asc');
        toggleOrder();
    }
}

//ENVIA TODAS AS COLUNAS SELECIONADAS PARA QUE SEJAM ORDENADAS
jQuery("#sendAllToOrder").on('click', function () {
    jQuery("#div_exibition input[type=checkbox]").each(function (event) {
        if ($(this).is(":checked")) {
            triggerToOrder($(this).parent(), event);
        }
    });

    toggleOrder();
});

$('.i_multiorder_exibition').click(function () {
    var $op = $('#div_exibition div.selected'),
        $this = $(this)

    if ($op.length) {
        ($this.hasClass("fa-arrow-up")) ?
            $op.first().prev().before($op) :
            $op.last().next().after($op);
    }
});

$('.i_multiorder_order').click(function () {
    var $op = $('#div_order div.selected'),
        $this = $(this);

    if ($op.length) {
        ($this.hasClass("fa-arrow-up")) ?
            $op.first().prev().before($op) :
            $op.last().next().after($op);
    }
});


//REMOVE TODOS OS ITENS DA ORDENAÇÃO
jQuery("#cleanOrder").on('click', function () {
    sortable.html("");
    toggleOrder();
});

//MOSTRA AS OPÇÕES DE ORDENAÇÃO QUANDO HOUVER ITENS PARA ORDENAR
function toggleOrder() {
    if (jQuery("#div_order").children().length == 0 || $("#div_order").children().length == undefined) {
        jQuery(".hideOrder").hide();
    } else {
        jQuery(".hideOrder").show();
    }
}

//adiciona uma coluna para que seja ordenada
function addOrder(id, text, order) {
    sortable.append(`
				<div class="d-flex flex-row text-nowrap align-items-center div_exibition_child" data-order="${order}" id="${id}-order">
					<input type="hidden" name="order[${id}]" value="${order}">
					<span><i class="fa fa-times pln-danger remove pln-cur-pointer"></i></span>
					<span class="text-truncate pl-1">${text}</span> 
					<span class="mr-1 pln-margin-auto">
						<span class="asc_desc  pln-cur-pointer">
						<img src="../../../resources/img/sort_up.png" title="${i18nGlobals[myCountry.getCountry()].ascending}" data-order="asc" style="display:${order == "asc" ? 'block' : 'none'}" class="mr-1 pln-margin-auto"></i>
						<img src="../../../resources/img/sort_down.png" title="${i18nGlobals[myCountry.getCountry()].descending}" data-order="desc" style="display:${order == "asc" ? 'none' : 'block'}" class="mr-1 pln-margin-auto"></i>	
						</span>							
					</span>					
				</div>
			`);

    //GERA EVENTOS PARA REMOVER O ITEM DA ORDENAÇÃO
    jQuery("#" + id + "-order").find('.remove').on('click', function (event) {
        jQuery(this).parent().parent().remove();
        toggleOrder();
    });

    //GERA EVENTOS PARA REMOVER O ITEM DA ORDENAÇÃO FIM

    //TROCA O ÍCONE DE ORDENAÇÃO E SETA O DATA-ORDER DA DIV PARA O VALOR CORREPONDENTE
    jQuery("#" + id + "-order").find('span .asc_desc img').on('click', function (event) {
        var order = jQuery(this).parent().find('img').not(':visible');
        jQuery("#" + id + "-order").attr('data-order', order.attr('data-order'));
        jQuery("#" + id + "-order").find('input').val(order.attr('data-order'));

        order.show();
        jQuery(this).hide();
    });
}

function cleanForm(form) {
    document.getElementById(form).reset();
    sortable.html("");
}

function pesquisar() {
    setForm();
    document.formulario.submit();
    loaderShow();
}

function execSwal(header, text, alertType){
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn pln-btn-orange',
        buttonsStyling: false,
    });

    swalWithBootstrapButtons({
        animation: false,
        text: text,
        type: alertType,
        showCancelButton: false,
        confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButton
    });
}

//abre janela de confirmação de exclusão
function confirmDeleteFilter() {
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: "btn btn-danger mr-2",
        cancelButtonClass: "btn btn-default",
        buttonsStyling: false
    });

    swalWithBootstrapButtons({
        animation: false,
        text: i18nGlobals[myCountry.getCountry()].filters.deleteFilter,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButtonYes,
        cancelButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.cancelButtonNo
    }).then((result) => {
        if (result.value) {
            deleteFilter();
        } else {
            jQuery("#confirm_delete").find('span').html(i18nGlobals[myCountry.getCountry()].delete);
            jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
            swal.close();
        }
    })
};

function setBtnSave() {
    jQuery("#btnfiltro_salvar").find('i').addClass('pln-invi');
    jQuery("#spn_save").html(i18nGlobals[myCountry.getCountry()].save);
};

function fillFilter(dataset) {
    var select = jQuery("#select_filtro");
    select.html('');
    select.append("<option value=''></option>");
    dataset.forEach(function (item) {
        select.append(`<option value='${item.value}'>${item.name}</option>`);
    })
};

//instancia os valores para enviar ao banco de dados
function saveFilter() {
    jQuery('#filter_temp').val('');
    if (jQuery("#filter_nome").val() == "") {
        jQuery("#filter_nome").css({"border": "1px solid red"});
        return false;
    }

    jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});

    jQuery("#btnfiltro_salvar").find('i').removeClass('pln-invi');
    jQuery("#spn_save").html(i18nGlobals[myCountry.getCountry()].saving);
    setForm();

    const perfil = "Publico",
        name = jQuery("#filter_nome").val(),
        view_id = "SCM420G",
        filter_id = jQuery("#select_filtro").val();

    let objPost = {};
    objPost.url = (filter_id == "" ? `/${myCountry.getCountry()}/ajax/filter/saveFilter` : `/${myCountry.getCountry()}/ajax/filter/updateFilter`);
    objPost.verb = (filter_id == "" ? 'save' : 'update');
    objPost.type = 'POST';
    objPost.data = {
        filter_perfil: perfil,
        filter_nome: name,
        filter_data: formSerielized,
        filter_tela_id: view_id,
        filter_id: filter_id,
    };
    filterService(objPost);
}

function deleteFilter() {
    jQuery("#confirm_delete").find('span').html(i18nGlobals[myCountry.getCountry()].deleting);
    jQuery("#confirm_delete").find('i').removeClass('fa fa-times pln-danger').addClass('fa fa-spin fa-spinner');

    let name = jQuery("#filter_nome").val(),
        filter_id = jQuery("#select_filtro").val(),
        objPost = {};

    objPost.url = `/${myCountry.getCountry()}/ajax/filter/deleteFilter`;
    objPost.verb = 'delete';
    objPost.type = 'POST';
    objPost.data = {
        filter_nome: name,
        filter_id: filter_id
    };

    filterService(objPost);
}

//responsável por enviar requisições POST ao banco de dados e tratar os retornos dependendo do verbo utilizado
function filterService(postObj) {
    $.ajax({
        url: postObj.url,
        type: postObj.type,
        dataType: 'json',
        data: postObj.data,
        success: function (response) {
            if (!response.error) {
                switch (postObj.verb) {
                    //se for delete, trato o ícone de deleção, removo o item do <select> e fecho a modal
                    case 'delete':
                        jQuery("#confirm_delete").find('span').html(i18nGlobals[myCountry.getCountry()].delete);
                        jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
                        jQuery("#select_filtro").find('option[value=' + postObj.data.filter_id + ']').remove();
                        jQuery("#modal_edit").modal("hide");
                        jQuery("#btn_filtro_text").text(i18nGlobals[myCountry.getCountry()].filters.saveFilter);
                        break;
                    //se for novo registro, trato o ícone de salvamento
                    case 'save':
                        //popula o select com o novo filtro criado
                        fillFilter(response.data);
                        setBtnSave();

                        const arrFilter = response.data.map(el => el.value), filter = {};
                        filter.filter_id = Math.max(...arrFilter);
                        setApplyedFilter(filter);
                        jQuery("#modal_edit").modal("hide");
                        break;
                    //se for edição de registro, trato o ícone de salvamento e atualizo o nome no <select>
                    case 'update':
                        jQuery("#select_filtro").find('option[value=' + postObj.data.filter_id + ']').text(postObj.data.filter_nome);
                        setBtnSave();
                        jQuery("#modal_edit").modal("hide");
                        break;
                }
            } else {
                execSwal("", i18nGlobals[myCountry.getCountry()].unavailableService, "error");
            }
        },
        error: function (error) {
            setBtnSave();
            execSwal("", i18nGlobals[myCountry.getCountry()].unavailableService, "error");
        }
    });
}

function setForm() {
    jQuery('#filter_temp').val('');
    formSerielized = JSON.parse(jQuery('#formulario').serializeJSON());
    formSerielized.order = getOrders();

    //check if any saved filter was selected
    if(jQuery('#select_filtro').val() != ""){
        formSerielized.filter_id = jQuery('#select_filtro').val();
        formSerielized.filter_nome = jQuery('#select_filtro option:selected').text();
    }

    jQuery("#filter_temp").val(JSON.stringify(formSerielized));
}

function setApplyedFilter(filter) {
    if (filter.filter_id != undefined) {
        jQuery("#select_filtro").val(filter.filter_id);
        jQuery("#btn_filtro_text").text("Editar filtro");
        jQuery("#modal_titulo").text("Edição de Filtro");
        jQuery("#btnfiltro_excluir").show();
        jQuery("#filter_nome").val(filter.filter_nome);
    }
}

function editForm(formulario) {
    let form = document.getElementById('formulario');
    let json = JSON.parse(formulario);
    delete json.filter_temp;

    for (let name in json) {
        //se for um array, percorre seus valores
        if (typeof (json[name]) == "object") {
            //este item é específio e precisa ser tratado separadamente, o restante será dinâmico
            if (name == "order") {
                let order = json.order;
                order.forEach(function (item) {
                    addOrder(item.id, item.text, item.order)
                });
            } else {
                json[name].forEach(function (item) {
                    let field = document.getElementById(name + "_" + item);
                    try{
                        field.checked = true;
                    }catch(e){
                        console.log("err check: "+name + "_" + item);
                        console.log(e)
                    }

                });
            }
        }

        let field = form.querySelector("[name='" + name + "']");
        if (field) {
            switch (field.type) {
                case 'radio':
                    try{
                        field = form.querySelector("[name=" + name + "][value='" + json[name] + "']");
                        field.checked = true;
                    }catch(e){
                        console.log(e)
                    }
                    break;
                default:
                    try{
                        field.value = json[name];
                    }catch(e){
                        console.log(e)
                    }
            }
        }
    }

    jQuery("#data_alterada_chegada").change();
    if (json.data_alterada_chegada != undefined && json.data_alterada_chegada != null && json.data_alterada_chegada != false) {
        if (json.data_alterada_chegada == "PERSONALIZADO" && json.interval_select == "interval") {
            jQuery('#interval_select').val('interval').change();
            jQuery(".interval-days").attr("style", "display:none !important");
            jQuery(".interval-dates").attr("style", "display:flex !important");
            jQuery('#interval_start').val(json.interval_start);
            jQuery('#interval_end').val(json.interval_end);
        }else{
            intervalDays = Number(json.interval_days);
            jQuery('#interval_select').val('next').change();
            jQuery(".interval-dates").attr("style", "display:none !important");
            jQuery(".interval-days").attr("style", "display:flex !important");
            jQuery('#interval_days').val(intervalDays);
        }
    }

    orderExibition(json);
}

function orderExibition(formdata) {
    let obj = formdata;
    if (obj.chkvetor) {
        let chkvetor = Object.values(obj.chkvetor)
        for (var i = chkvetor.length - 1; i >= 0; i--) {
            jQuery("#div_exibition").prepend(jQuery(`#chkvetor_${chkvetor[i]}`).parent().detach())
        }
    }
}

function getOrders() {
    var arr = [];
    jQuery("#div_order").find("div").each(function () {
        arr.push({
            'id': jQuery(this).attr("id").split('-')[0],
            'order': jQuery(this).attr("data-order"),
            'text': jQuery(this).find('span').eq(1).text().trim()
        });
    });

    return arr;
}