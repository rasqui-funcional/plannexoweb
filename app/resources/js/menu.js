/****************FUNÇÃO DO TEMPLATE PARA MONTAR O MENU********************/
function os_init_sub_menus() {

    // INIT MENU TO ACTIVATE ON HOVER
    var menu_timer;
    $('.menu-activated-on-hover').on('mouseenter', 'ul.main-menu > li.has-sub-menu', function () {
      var $elem = $(this);
      clearTimeout(menu_timer);
      $elem.closest('ul').addClass('has-active').find('> li').removeClass('active');
      $elem.addClass('active');
    });
  
    $('.menu-activated-on-hover').on('mouseleave', 'ul.main-menu > li.has-sub-menu', function () {
      var $elem = $(this);
      menu_timer = setTimeout(function () {
        $elem.removeClass('active').closest('ul').removeClass('has-active');
      }, 30);
    });
  
    // INIT MENU TO ACTIVATE ON CLICK
    $('.menu-activated-on-click').on('click', 'li.has-sub-menu > a', function (event) {
      var $elem = $(this).closest('li');
      if ($elem.hasClass('active')) {
        $elem.removeClass('active');
      } else {
        $elem.closest('ul').find('li.active').removeClass('active');
        $elem.addClass('active');
      }
      return false;
    });
  }

  $('.mobile-menu-trigger').on('click', function () {
    $('.menu-mobile .menu-and-user').slideToggle(200, 'swing');
    return false;
  });

  $('#dropdownMenuLink').dropdown();

  os_init_sub_menus();
  /************************************/

$( document ).ready(function() {

    var menuControllSlide = 1,
        marginLeft = 0;

    const toggleMenu = () => {
      if (menuControllSlide) {
        $('.overlay').addClass('active');
        $('.content-i').addClass('filter-blur');
        marginLeft = 0;
        menuControllSlide = 0;
      } else {
        $('.overlay').removeClass('active');
        $('.content-i').removeClass('filter-blur');
        marginLeft = -300;
        menuControllSlide = 1;
      };

      return $("#menu-retratil").animate({marginLeft: marginLeft+"px"}, 500)
    };
    
    // se a página NÃO tiver o campo hidden #checkFormWithoutSaving, o menu redireciona direto para o link, senão, verifica se existe formulário a ser salvo 
    jQuery("#menu-retratil .gotoback").on("click", function(){
      let route = jQuery(this).attr("data-route");
      if (!jQuery("#checkFormWithoutSaving").val()) {
        window.location.href = route;
        toggleMenu();
        loaderShow();
      }
      else{
        loaderShow();
        toggleMenu();
      }
    });

    jQuery(".top-menu-controls .gotoback").on("click", function(){
      let route = jQuery(this).attr("data-route");
      if (!jQuery("#checkFormWithoutSaving").val() && jQuery("#checkFormWithoutSaving").val() != undefined) {
        window.location.href = route;
        toggleMenu();
        loaderShow();
      }else{
        loaderShow();
        window.location.href = route;
      }
    });  

    // esconde o menu lateral e chama a função que ajusta a largura da tabela
    $("#open_menu, .overlay").on("click", function(){
      return toggleMenu();
    });

});