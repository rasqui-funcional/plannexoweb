const routeClass = new RoutesController();
const paginationClass = new paginationController(routeClass.getPurchaseOrderTable(), routeClass.goToPurchaseOrderFilters());

class purchaseOrderController{

    constructor(){
        this.initialize();
    }   

    initialize(){
        utilsInstance.onlyNumbers("#goto_pagenumber");
        jQuery(".list_purchaseorder").on("click", (element)=>{
            let finalObject,            
            dynamicColumns = [];

            jQuery(element.currentTarget).parent().parent().find("[data-column]").each((index,element)=>{
                dynamicColumns.push({[jQuery(element).attr("data-column")]: jQuery(element).attr("data-value")})
            });

            finalObject =(Object.assign({}, ...dynamicColumns));
            this.callOrders(finalObject);

        });
    }
    /******INITIALIZE FIM******/        
    //send params and redirect to suggestions list
    callOrders(params){
        loaderShow();
        $.ajax({
            url: `/${myCountry.getCountry()}/purchaseorder/orders/preparelist`,
            data: {
                obj: params
            },
            type: 'POST',
            datatype: 'text',
            success: (resposta)=> {
                if(resposta.data) {
                    window.location.href= `/${myCountry.getCountry()}/purchaseorder/orders/list`;
                } else {
                    console.log('response',resposta)
                    loaderHide();
                    alert(i18nGlobals[myCountry.getCountry()].unavailableService)
                }
            },
            error: (error)=> {
                console.log('response',error)
                loaderHide();
                alert(i18nGlobals[myCountry.getCountry()].unavailableService)
            }
        });
    }

    execSwal(header, text, alertType){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            animation: false,
            text: text,
            type: alertType,
            showCancelButton: false,
            confirmButtonText: i18nGlobals[myCountry.getCountry()].modal.confirmButton
        });
    }
}

var classe = new purchaseOrderController();