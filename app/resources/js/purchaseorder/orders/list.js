const routeClass = new RoutesController();
const paginationClass = new paginationController(routeClass.getPurchaseOrderOrdersTable());

class PurchaseListController{

    constructor(){
        this.country = myCountry.getCountry();
        this.listenerBnts();        
    }   

    initialize(){
        jQuery("#shoppingforecast_month, #history_consumption, #stockpos_year, #stockpos_quarter, #dashboard_tab").unbind("click");
        jQuery('#erp-select').unbind("change");
        utilsInstance.onlyNumbers("#goto_pagenumber");
        var fullScreen = new fullScreenController();
        new infoSkuController();
        new BasicInformationController();
        new CalendarController();
        new childrenItemController();
        new interdependenceController();
        new PolicyController();
        new erp();
        new LogController();
    }

    listenerBnts(){
        jQuery("#download-csv").on("click", function() {
            window.open(`/${this.country}/csv/purchaseorder/orders`)
        });
    }
}

var classe = new PurchaseListController();