class purchaseOrderController
{
    constructor(){
        /************INPUTS TO CHANTE RANGE DATE**********/
        this.formSerielized;
        this.inputDaterangeDateReq = jQuery("#input_daterange_date_req");
        this.labelDaterangeDateReq = jQuery("#lbl_daterange_date_req");

        this.inputDaterangeDateOrder = jQuery("#input_daterange_date_order");
        this.labelDaterangeDateOrder = jQuery("#lbl_daterange_date_order");

        this.inputDaterangeDateExp = jQuery("#input_daterange_date_exp");
        this.labelDaterangeDateExp = jQuery("#lbl_daterange_date_exp");

        this.ddDateReq = jQuery("#date_req_order_cond");
        this.ddDateOrder = jQuery("#date_order_in_cond");
        this.ddDateExp = jQuery("#date_exp_in_cond");
        /************************/

        this.searchbtn = jQuery('#btn_pesquisa');
        this.daysOfDelay = jQuery("#dias_atraso");
        this.qtdDaysOfDelay = jQuery("#qtde_dias_atraso");
		this.today = new Date();
        this.listenerBtns();
        this.daterangepicker();
    }

    listenerBtns(){

        //controlling the label of delete button
        jQuery("#modal_edit").on("show.bs.modal", function(){
            jQuery("#select_filtro").val() == "" ? jQuery("#confirm_delete").addClass("invisible") : jQuery("#confirm_delete").removeClass("invisible");
        });

        //controlling the validator field when modal closes
        jQuery("#modal_edit").on("hide.bs.modal", function(){
            jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});
            jQuery("#filter_nome").val("");
        });

        jQuery('.progress-bar').css("background-color","#F96737");
		jQuery('.progress .progress-bar').css("width",function() {
			return jQuery(this).attr("aria-valuenow") + "%";
        });

        jQuery("[data-toggle=tooltip]").tooltip();

        utilsInstance.onlyNumbers("#dias_atraso_in");

        //binding change event for Filtros Especiais
        this.ddDateReq.on('change', (element)=>{
            this.filterDate(element.currentTarget, this.inputDaterangeDateReq, this.labelDaterangeDateReq)
        });

        this.ddDateOrder.on('change', (element)=>{
            this.filterDate(element.currentTarget, this.inputDaterangeDateOrder, this.labelDaterangeDateOrder)
        });

        this.ddDateExp.on('change', (element)=>{
            this.filterDate(element.currentTarget, this.inputDaterangeDateExp, this.labelDaterangeDateExp)
        });
        //end binding

        //disable field Qtde. Dias when order == "sem atraso"
        jQuery(".ord_atraso").on("click", (element)=>{
            if(jQuery(element.currentTarget).attr("id") == "ord_atraso_com"){
                this.daysOfDelay.prop("disabled", false)
                this.qtdDaysOfDelay.prop("disabled", false)
            }else{
                this.daysOfDelay.prop("disabled", true).val("")
                this.qtdDaysOfDelay.prop("disabled", true).val("")
            }
        });

        jQuery('.chk_check_all').click(function () {
            jQuery(this).parent().parent().parent().find('input:checkbox').prop('checked', this.checked);
        });

        jQuery("#clean_form").on("click", (element)=>{
            this.cleanForm();
        });

        //send to list page
        jQuery("#btn_search").on("click", ()=>{
            this.search();
        });

        //confirm remove item from database
        jQuery("#confirm_delete").on("click", ()=>{
            this.confirmDeleteFilter();
        });

        //My filter change event
        jQuery("#select_filtro").on("change", (element)=>{
			this.cleanForm();

			if(jQuery(element.currentTarget).val() == ""){
				//$("#save_filter").prop("disabled",true);
				jQuery("#btn_filtro_text").text(i18nGlobals[myCountry.getCountry()].filters.saveFilter);
				jQuery("#filter_nome").val("");
				jQuery("#modal_titulo").text(i18nGlobals[myCountry.getCountry()].filters.newFilter);
				jQuery("#btnfiltro_excluir").hide();
				jQuery("#filter_perfil_Publico").prop("checked", true);
			}else{
				loaderShow();
				jQuery("#btn_filtro_text").text(i18nGlobals[myCountry.getCountry()].filters.editFilter);
				jQuery("#filter_nome").val(jQuery("select[name=select_filtro] option:selected").text());
				jQuery("#modal_titulo").text(i18nGlobals[myCountry.getCountry()].filters.editFilter);
				jQuery("#btnfiltro_excluir").show();

				$.ajax({
					url: `/${myCountry.getCountry()}/ajax/filter/getFilterById`,
					type: 'POST',
					dataType: 'json',
					data: {
						filter_id: jQuery(element.currentTarget).val()
					},
					success: (response)=> {
                        editFormClass.setJson(response.data[0].FILTER_DATA)
                        editFormClass.formEdit()
						loaderHide();
					},
					error: function (error) {
						loaderHide();
						console.log("error", error)
					}
				});
			}
        });

        jQuery("#save_filter").on("click", (element)=>{
            this.saveFilter();
        });
    }

    saveFilter() {
        if (jQuery("#filter_nome").val() == "") {
            jQuery("#filter_nome").css({"border": "1px solid red"});
            return false;
        } else {
            jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});
        }

        jQuery("#save_filter").attr("disabled",true).find('i').removeClass('pln-invi');
        jQuery("#spn_save").html(i18nGlobals[myCountry.getCountry()].saving);
        this.getSerializedForm();

        let perfil = jQuery("#filter_perfil").val(),
            name = jQuery("#filter_nome").val(),
            view_id = jQuery("#spn_filter_id").attr("data-original-title"),
            filter_id = jQuery("#select_filtro").val();

        let objPost = {};
            objPost.url =  ( filter_id == "" ? `/${myCountry.getCountry()}/ajax/filter/saveFilter` : `/${myCountry.getCountry()}/ajax/filter/updateFilter`);
            objPost.verb = ( filter_id == "" ? 'save' : 'update');
            objPost.type = 'POST';
            objPost.data = {
                filter_perfil: perfil,
                filter_nome: name,
                filter_data: this.formSerielized,
                filter_tela_id: view_id,
                filter_id: filter_id
            }

            this.filterService(objPost);
    }

    confirmDeleteFilter(){
		const swalWithBootstrapButtons = swal.mixin({
			confirmButtonClass: "btn btn-danger mr-2",
			cancelButtonClass: "btn btn-default",
			buttonsStyling: false
		  });
		swalWithBootstrapButtons({
			title: i18nGlobals[myCountry.getCountry()].filters.deleteFilter,
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButtonYes,
			cancelButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.cancelButtonNo
		  }).then((result) => {
			if (result.value) {
				this.deleteFilter();
			}else{
				jQuery("#confirm_delete").find('span').html(i18nGlobals[myCountry.getCountry()].deleting);
				jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
				swal.close();
			}
		  })
    }

    deleteFilter(){
		jQuery("#confirm_delete").find('span').html(i18nGlobals[myCountry.getCountry()].deleting);
		jQuery("#confirm_delete").find('i').removeClass('fa fa-times pln-danger').addClass('fa fa-spin fa-spinner');
		let name = jQuery("#filter_nome").val(),
			filter_id = jQuery("#select_filtro").val(),
			objPost = {};
				objPost.url = `/${myCountry.getCountry()}/ajax/filter/deleteFilter`;
				objPost.verb = 'delete';
				objPost.type = 'POST';
				objPost.data = {
					filter_nome: name,
					filter_id: filter_id
				};
			this.filterService(objPost);
	}

    getSerializedForm(){
		this.formSerielized = JSON.parse(jQuery('#formulario').serializeJSON());
		jQuery("#filter_temp").val(JSON.stringify(this.formSerielized));
    }

    search(){
        let filters = JSON.parse(jQuery('#formulario').serializeJSON());
        if(jQuery('#select_filtro').val() != ""){
            filters.filter_id = jQuery('#select_filtro').val()
            filters.filter_nome = jQuery('#select_filtro option:selected').text()
        }

        jQuery("#input_daterange_date_exp").val(filters.date_exp.replace(i18nGlobals[myCountry.getCountry()].to.toLowerCase(),"-"));
        jQuery("#input_daterange_date_order").val(filters.date_order.replace(i18nGlobals[myCountry.getCountry()].to.toLowerCase(),"-"));
        jQuery("#input_daterange_date_req").val(filters.date_req.replace(i18nGlobals[myCountry.getCountry()].to.toLowerCase(),"-"));

        jQuery("#filter_temp").val(JSON.stringify(filters));
        loaderShow();
        document.formulario.submit();
	}

    //responsável por enviar requisições POST ao banco de dados e tratar os retornos dependendo do verbo utilizado
	filterService(postObj){
		$.ajax({
			url: postObj.url,
			type: postObj.type,
			dataType: 'json',
			data: postObj.data,
			success: (response)=> {
				if(!response.error){
					switch(postObj.verb){
						//se for delete, trato o ícone de deleção, removo o item do <select> e fecho a modal
						case 'delete':
							jQuery("#confirm_delete").find('span').html(i18nGlobals[myCountry.getCountry()].delete);
							jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
							jQuery("#select_filtro").find('option[value='+postObj.data.filter_id+']').remove();
							jQuery("#modal_edit").modal("hide");
							jQuery("#btn_filtro_text").text(i18nGlobals[myCountry.getCountry()].filters.saveFilter);
						break;
						//se for novo registro, trato o ícone de salvamento
						case 'save':
							//popula o select com o novo filtro criado
							this.fillFilter(response.data);
							this.setBtnSave()

                            const arrFilter = response.data.map(el => el.value), filter = {};
                            filter.filter_id = Math.max(...arrFilter);
                            setApplyedFilter(filter);
                            jQuery("#modal_edit").modal("hide");
						break;
						//se for edição de registro, trato o ícone de salvamento e atualizo o nome no <select>
						case 'update':
                            jQuery("#select_filtro").find('option[value='+postObj.data.filter_id+']').text(postObj.data.filter_nome);
							this.setBtnSave();
							jQuery("#modal_edit").modal("hide");
						break;
					}
				}else{
                    this.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'warning')
				}
			},
			error: function (error) {
				this.setBtnSave();
				this.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'warning')
				console.log("error", error)
				console.log("url", this.data.data)
			}
		});
    }

    fillFilter(dataset){
		let select = jQuery("#select_filtro");
		select.html('');
		select.append("<option value=''></option>");
		dataset.forEach(function(item){
			select.append(`<option value='${item.value}'>${item.name}</option>`);
		})
    }

    setBtnSave(){
		jQuery("#save_filter").attr("disabled", false).find('i').addClass('pln-invi');
		jQuery("#spn_save").html(i18nGlobals[myCountry.getCountry()].save);
	}

    cleanForm(){
        document.getElementById("formulario").reset();
    }

    filterDate(currentTarget, inputDateRange, label){
        switch( jQuery(currentTarget).val() ){
            case "INTERVALO":
                inputDateRange.show();
                label.hide();
            break;
            case "HOJE":
                this.toggleDate(inputDateRange, label);
                label.html(moment(this.today).format('DD/MM/YYYY'));
                inputDateRange.val( `${moment(this.today).format('DD/MM/YYYY')} - ${moment(this.today).format('DD/MM/YYYY')}`);
            break;
            case "PR_7DIAS":
                this.toggleDate(inputDateRange, label);
                label.html(`${ moment(this.today).format('DD/MM/YYYY') } até ${ moment(this.today).add(6,'d').format('DD/MM/YYYY') }`);
                inputDateRange.val(`${ moment(this.today).format('DD/MM/YYYY') } - ${ moment(this.today).add(6,'d').format('DD/MM/YYYY') }`);
            break;
            case "PR_30DIAS":
                this.toggleDate(inputDateRange, label);
                label.html(`${ moment(this.today).format('DD/MM/YYYY') } até ${ moment(this.today).add(1,'M').format('DD/MM/YYYY') }`);
                inputDateRange.val(`${ moment(this.today).format('DD/MM/YYYY') } - ${ moment(this.today).add(1,'M').format('DD/MM/YYYY') }`);
            break;
            case "PR_SEMANA":
                this.toggleDate(inputDateRange, label);
                label.html(`${ moment(this.today).add(1, 'w').startOf('isoWeek').format('DD/MM/YYYY') } até ${ moment(this.today).add(1, 'w').startOf('isoWeek').add(6,'d').format('DD/MM/YYYY') }`);
                inputDateRange.val(`${ moment(this.today).add(1, 'w').startOf('isoWeek').format('DD/MM/YYYY') } - ${ moment(this.today).add(1, 'w').startOf('isoWeek').add(6,'d').format('DD/MM/YYYY') }`);
            break;
            case "MES_ATUAL":
                this.toggleDate(inputDateRange, label);
                label.html(`${ moment(this.today).startOf('month').format('DD/MM/YYYY') } até ${ moment(this.today).endOf('month').format('DD/MM/YYYY') }`);
                inputDateRange.val(`${ moment(this.today).startOf('month').format('DD/MM/YYYY') } - ${ moment(this.today).endOf('month').format('DD/MM/YYYY') }`);
            break;
            case "PR_MES":
                this.toggleDate(inputDateRange, label);
                label.html(`${ moment(this.today).add(1, 'months').startOf('month').format('DD/MM/YYYY') } até ${ moment(this.today).add(1, 'months').endOf('month').format('DD/MM/YYYY') }`);
                inputDateRange.val(`${ moment(this.today).add(1, 'months').startOf('month').format('DD/MM/YYYY') } - ${ moment(this.today).add(1, 'months').endOf('month').format('DD/MM/YYYY') }`);
            break;
        }
    }

    toggleDate(input, label){
		input.hide();
		label.show();
    }

    daterangepicker(){
        jQuery('.order_daterange').daterangepicker({
            autoUpdateInput: false,
            "locale": {
                "format": "DD/MM/YYYY",
                'separator': ` ${i18nGlobals[myCountry.getCountry()].to.toLowerCase()} `,
                'applyLabel': i18nGlobals[myCountry.getCountry()].apply,
                'cancelLabel': i18nGlobals[myCountry.getCountry()].cancel,
                'fromLabel': i18nGlobals[myCountry.getCountry()].from,
                'toLabel': i18nGlobals[myCountry.getCountry()].to,
                'weekLabel': i18nGlobals[myCountry.getCountry()].calendar.weekLabel,
                'daysOfWeek':  i18nGlobals[myCountry.getCountry()].calendar.daysOfWeek,
                'monthNames': i18nGlobals[myCountry.getCountry()].calendar.monthNames,
                "firstDay": 1
            }
        });

        jQuery('.order_daterange').on('cancel.daterangepicker', function(ev, picker) {
            jQuery(this).val('');
        });
        $('.order_daterange').on('apply.daterangepicker', function(ev, picker) {
            jQuery(this).val(`${picker.startDate.format('DD/MM/YYYY')} ${i18nGlobals[myCountry.getCountry()].to.toLowerCase()} ${picker.endDate.format('DD/MM/YYYY')}`);
        });
    }

    execSwal(header, text, alertType){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            animation: false,
            text: text,
            type: alertType,
            showCancelButton: false,
            confirmButtonText: i18nGlobals[myCountry.getCountry()].confirmButton
        });
    }
}

let classe = new purchaseOrderController();