class orderCoverSuggestController {
    constructor() {
        this.country = myCountry.getCountry();
        
        this.message_error = i18nGlobals[myCountry.getCountry()].unavailableService;
        //guarda o estado original do formulário (do jeito que é carregado do banco, sem modificações)
        this.global_defaultForm = {};
        this.itemsToRemove;
        this.listenerBtns();
    }

    listenerBtns() {
        jQuery('#confirm_delete').on('click', () => {
            this.confirmDeleteFilter();
        });

        jQuery('#saveData').on('click', () => {
            this.formValidation();
        });

        jQuery('#toDisallow').on('click', () => {
            this.formValidationToDelete();
        });

        jQuery('#download_csv').on('click', function () {
            let searchValue = '';
            window.open(`/${this.country}/csv/suggestions/${searchValue}`)
        });
    }

    initialize() {
        utilsInstance.createRegex('.txtarea_obs', RegExp(/[A-Za-zÀ-ÿ0-9\.\-\_ ]/));
        utilsInstance.onlyNumbers('#goto_pagenumber, .qtd_alterado');
        utilsInstance.biggerThenZero(".qtd_alterado");

        jQuery('.input_chk').on('click', () => {
            this.itemsToRemove = [];

            let objSugg = {};
            jQuery('.input_chk').each((index, element) => {
                if (jQuery(element).prop('checked')) {
                    objSugg = {
                        'NUM_PARC': jQuery(element).attr('data-num-parc'),
                        'ID_PSUG_PK': jQuery(element).attr('data-id-psug'),
                        'PSUG_GROUPED_PK': jQuery(element).attr('data-id-psug-grouped')
                    };

                    this.itemsToRemove.push(objSugg)
                }
            });

            this.hideDeleteBtn();
        });

        jQuery('#chk_all').on('click', (element) => {
            this.itemsToRemove = [];
            let objSugg = {};

            jQuery('.input_chk').prop('checked', element.currentTarget.checked);
            jQuery('.input_chk').each((index, element) => {
                if (jQuery(element).prop('checked')) {
                    objSugg = {
                        'NUM_PARC': jQuery(element).attr('data-num-parc'),
                        'ID_PSUG_PK': jQuery(element).attr('data-id-psug'),
                        'PSUG_GROUPED_PK': jQuery(element).attr('data-id-psug-grouped')
                    };

                    this.itemsToRemove.push(objSugg)
                }
            });

            this.hideDeleteBtn();
        });

        jQuery('.tableOrderCoverSuggestions_tr').on('click', function () {
            jQuery(this).parent().find('tr').each(function () {
                if (jQuery(this).hasClass('pln-row-selected')) {
                    jQuery(this).removeClass('pln-row-selected')
                }
            });

            jQuery(this).addClass('pln-row-selected');
        });

        jQuery('.DATE_ARRIVAL_ALTERED').on('focusin', (e) =>{
            jQuery(e.currentTarget).data('oldVal', jQuery(e.currentTarget).val())
        }).on('change', (e) =>{
            this.changeDate(jQuery(e.currentTarget));
        });

        jQuery('.get-changed').on('change', (e) =>{
            this.checkOnChange(e);
        })
    }
    /******INITIALIZE FIM******/

    prepareMessageDuplicatedDates(arrDates){
        let html = `
                <table class="table">
                    <thead>
                        <tr>
                            <th>${i18nGlobals[myCountry.getCountry()].codItem}</th>
                            <th>${i18nGlobals[myCountry.getCountry()].date}</th>
                        </tr>
                    </thead>
                    <tbody>
            `;
        for(let i = 0; i < arrDates.length; i++){
            html += `
                <tr>
                    <td>${arrDates[i].split('|')[1]}</td>
                    <td>${moment(arrDates[i].split('|')[0]).format("DD/MM/YYYY")}</td>
                </tr>
            `                        
        }
        html += '</tbody></table>'
        return html;
        
    }

    dateValidation(){
        let invalidDate = false;
        const allItems = this.setItemsToCompare(),
              transformedItems = allItems.map(item => (`${item.DATE_ARRIVAL_ALTERED}|${item.COD_ITEM_PK}`))
        
        const count = transformedItems => 
        transformedItems.reduce((a, b) => 
            Object.assign(a, {[b]: (a[b] || 0) + 1}), {})
        
        const duplicates = dict => 
            Object.keys(dict).filter((a) => dict[a] > 1)
        
        const checkDuplicatedItems = duplicates(count(transformedItems));
        
        //se houver o mesmo item com a mesma data
        if (checkDuplicatedItems.length) {
            const msg = this.prepareMessageDuplicatedDates(checkDuplicatedItems)
            paginationClass.execSwal(
                "",
                `${i18nGlobals[myCountry.getCountry()].sameDate} <br/><br/> ${msg}</b>`
                , "error"
            );
            return false;
        }                

        //se alguma data estiver vazia
        allItems.forEach(item =>{            
            if(item.DATE_ARRIVAL_ALTERED == "" || item.DATE_ARRIVAL_ALTERED.length < 10){
                invalidDate = true
            }
        });
        
        if (invalidDate) {
            paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].invalidDate, 'warning');
            return false;
        }
        return true;
    }

    formValidation() {
        //se nenhum item estiver selecionado
        const checkedItems = this.setItemsToSave();
        if (checkedItems.length == 0) {
            paginationClass.execSwal('', i18nOrderCover[myCountry.getCountry()].unGroup.selectItems, 'warning');
            return false;
        }
        
        const isValid = this.dateValidation();
        if(!isValid) return false;

        this.saveData(checkedItems);

    }

    formValidationToDelete() {
        const checkedItems = this.setItemsToRemove();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-danger mr-2',
            cancelButtonClass: 'btn btn-default',
            buttonsStyling: false
        });

        swalWithBootstrapButtons({
            text: i18nGlobals[myCountry.getCountry()].suggestion.confirmDelete,
            type: 'warning',
            animation: false,
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                jQuery.ajax({
                    url: `/${this.country}/ordercover/suggestions/remove`,
                    type: 'POST',
                    dataType: 'text',
                    data: {obj: checkedItems},
                    success: function(response) {
                        paginationClass.init = false;
                        paginationClass.getTableService(...paginationClass.paginationParams);
                        paginationClass.execSwal('', '<b>'+i18nGlobals[myCountry.getCountry()].suggestion.successOnDelete+'</b>', 'success');
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        paginationClass.execSwal('', this.message_error, 'warning');
                    }
                });
                jQuery('#toDisallow').prop("disabled", true);
            } else {
                jQuery('#confirm_delete').find('span').html(i18nGlobals[myCountry.getCountry()].modalRemoveItem.title);
                jQuery('#confirm_delete').find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
                swal.close();
            }
        })
    }

    saveData(checkedItems) {

        jQuery.ajax({
            url: `/${this.country}/ordercover/suggestions/update`,
            data: {
                obj: this.prepareDate(checkedItems)
            },
            type: 'POST',
            datatype: 'text',
            success: (response) => {
                paginationClass.init = false;
                paginationClass.getTableService(...paginationClass.paginationParams);                
            },
            error: (error) => {
                console.log('response', error);
                paginationClass.execSwal('', this.message_error, 'warning');
            }
        });
    }

    changeDate(element){
        const prevDate = element.data('oldVal'),
              codItem = this.sameDateValidation(element, prevDate);

        if(codItem){
            element.val(prevDate)
            paginationClass.execSwal("",`${i18nGlobals[myCountry.getCountry()].sameDate} <br/><br/> <b>${i18nGlobals[myCountry.getCountry()].codItem} ${codItem}</b>`, "error");
        }
    }

    sameDateValidation(element, prevDate) {
        loaderShow();
        const sku_id = element.parent().parent().attr('data-id-sku');
        const sug_id = element.parent().parent().find('.input_chk').val();
        let data = this.setItemsToCompare(),
            date = element.val(),
            codItem = element.parent().parent().find('.COD_ITEM_PK').html().trim();

        let arrInputValues = [];
        let obj = [];
        arrInputValues.push({"cod_item": codItem});
        arrInputValues.push({"delivery_date": moment(date).format("DD/MM/YYYY")});
        arrInputValues.push({"id_sku": sku_id});
        arrInputValues.push({"id_sug": sug_id});
        obj.push(Object.assign({}, ...arrInputValues));
        $.ajax({
            url: `/${myCountry.getCountry()}/suggest/add/checkDate`,
            data: {obj},
            type: 'POST',
            datatype: 'text',
            success: (data)=> {
                loaderHide();
                if(data.data.dateAlreadyExists.length > 0){
                    element.val(prevDate)
                    paginationClass.execSwal("",`${i18nGlobals[myCountry.getCountry()].sameDate} <br/><br/> <b>${i18nGlobals[myCountry.getCountry()].codItem} ${codItem}</b>`, "error");
                }
            },
            error: function (error) {
                loaderHide();
                console.log(error);
            }
        });
        
        return false;
    }

    prepareDate(obj){
        for(let i=0; i< obj.length; i++){
            obj[i].DATE_ARRIVAL_ALTERED = moment(obj[i].DATE_ARRIVAL_ALTERED).format("DD/MM/YYYY")
        }
    
        return obj;
    }

    deleteItems() {
        jQuery.ajax({
            url: `/${this.country}/ordercover/suggestions/delete`,
            type: 'POST',
            dataType: 'json',
            data: {
                obj: this.itemsToRemove
            },
            success: (response) => {
                if (!response.error) {
                    paginationClass.init = false;
                    paginationClass.getTableService(...paginationClass.paginationParams);
                    return;
                }
                paginationClass.execSwal('', this.message_error, 'warning');
            },
            error: function (error) {
                console.log('error', error);
                console.log('url', this.data.data);
                setBtnSave();
                paginationClass.execSwal('', this.message_error, 'warning');
            }
        });
    }

    hideDeleteBtn() {
        if (this.itemsToRemove.length > 0) {
            jQuery('#toAgroup, #toDisallow').prop("disabled", false);
        } else {
            jQuery('#toAgroup, #toDisallow').prop("disabled", true);
        }
    }

    confirmDeleteFilter() {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-danger mr-2',
            cancelButtonClass: 'btn btn-default',
            buttonsStyling: false
        });

        swalWithBootstrapButtons({
            title: i18nOrderCover[myCountry.getCountry()].unGroup.modalRemoveItem.title,
            type: 'warning',
            animation: false,
            showCancelButton: true,
            confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButtonYes,
            cancelButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.cancelButtonNo
        }).then((result) => {
            if (result.value) {
                this.deleteItems();
                jQuery('#toAgroup').prop("disabled", true);
            } else {
                jQuery('#confirm_delete').find('span').html(i18nOrderCover[myCountry.getCountry()].unGroup.title);
                jQuery('#confirm_delete').find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
                swal.close();
            }
        })
    };

    //set os itens que foram alterados para que sejam salvos no banco de dados
    setItemsToSave() {
        let objSugg = {}, objArr = [];

        jQuery('.input_chk').each((index, element) => {
            const parentElement = jQuery(element).parent().parent();
            if (jQuery(element).prop('checked')) {
                objSugg = {
                    'ID': jQuery(element).val(),
                    'QTY_ALTERED': parentElement.find('.QTY_ALTERED').val(),
                    'DATE_ARRIVAL_ALTERED': parentElement.find('.DATE_ARRIVAL_ALTERED').val(),
                    'OBS': parentElement.find('.txtarea_obs').val(),
                    'ID_SKU_PK': parentElement.attr('data-id-sku'),
                    'COD_ITEM_PK': parentElement.find('.COD_ITEM_PK').html().trim()
                };

                objArr.push(objSugg) 
            }
        });

        return objArr;
    }

    //set os itens que foram alterados para que sejam salvos no banco de dados
    setItemsToRemove() {
        let objSugg = {}, objArr = [];

        jQuery('.input_chk').each((index, element) => {
            const parentElement = jQuery(element).parent().parent();
            if (jQuery(element).prop('checked')) {
                objSugg = {
                    'ID_SUG': jQuery(element).val(),
                    'ID_PSUG_PK': jQuery(element).attr('data-id-psug'),
                    'PSUG_GROUPED_PK': jQuery(element).attr('data-id-psug-grouped'),
                    'NUM_PARC': jQuery(element).attr('data-num-parc')
                };

                objArr.push(objSugg) 
            }
        });

        return objArr;
    }

    setItemsToCompare() {
        let objSugg = {}, objArr = [];
        
        jQuery('.input_chk').each((index, element) => {            
            const parentElement = jQuery(element).parent().parent();
            objSugg = {
                'ID': jQuery(element).val(),
                'QTY_ALTERED': parentElement.find('.QTY_ALTERED').val(),
                'DATE_ARRIVAL_ALTERED': parentElement.find('.DATE_ARRIVAL_ALTERED').val(),
                'OBS': parentElement.find('.txtarea_obs').val(),
                'ID_SKU_PK': parentElement.attr('data-id-sku'),
                'COD_ITEM_PK': parentElement.find('.COD_ITEM_PK').html().trim()
            };

            objArr.push(objSugg)            
        });
        return objArr;
    }

    checkOnChange(element) {        
        jQuery(element.currentTarget).parent().parent().find('.input_chk').prop('checked', true)
    }
    
}

const orderCoverSuggest = new orderCoverSuggestController();
const routeClass = new RoutesController();
const paginationClass = new paginationController(routeClass.getOrderCoverSuggTable(), routeClass.goToOrderCoverList());