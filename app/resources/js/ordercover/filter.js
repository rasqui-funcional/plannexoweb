jQuery(function () {

    utilsInstance.createRegex("#filter_nome", RegExp(/[A-Za-zÀ-ÿ0-9\.\-\_ ]/));
    //apply tooltip to page title
    jQuery('[data-toggle=tooltip]').tooltip();

    //controla botão Deletar quando a modal abre
    jQuery("#modal_edit").on("show.bs.modal", function () {
        jQuery("#select_filtro").val() == "" ? jQuery("#confirm_delete").addClass("invisible") : jQuery("#confirm_delete").removeClass("invisible");
    });
    //controla o validador do campo nome quando a modal fecha
    jQuery("#modal_edit").on("hide.bs.modal", function () {
        jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});
    });

    jQuery('.chk_check_all').click(function () {
        jQuery(this).parent().parent().parent().find('input:checkbox').prop('checked', this.checked);
    });

    /**************POPULA OS FILTROS SE HOUVER FILTRO SALVO NA SESSION DO PHP***************/
    //filter_temp é uma variável do arquivo filters.php que recebe os dados dos ultimos filtros
    //que foram salvos na SESSION para popular o form automaticamente se o usuário voltar para a tela após uma pesquisa
    //if(filter_temp)editForm(filter_temp)
    /**************FIM***************/


    /*********************ACIONANDO MEUS FILTROS************************/
    jQuery("#select_filtro").on("change", function () {
        cleanForm('formulario');

        if (jQuery(this).val() == "") {
            //$("#btnfiltro_salvar").prop("disabled",true);
            jQuery("#btn_filtro_text").text(i18nGlobals[myCountry.getCountry()].filters.saveFilter);
            jQuery("#filter_nome").val("");
            jQuery("#modal_titulo").text(i18nGlobals[myCountry.getCountry()].filters.newFilter);
            jQuery("#btnfiltro_excluir").hide();
            jQuery("#filter_perfil_Publico").prop("checked", true);
        } else {
            loaderShow();
            jQuery("#btn_filtro_text").text(i18nGlobals[myCountry.getCountry()].filters.editFilter);
            jQuery("#filter_nome").val(jQuery("select[name=select_filtro] option:selected").text());
            jQuery("#modal_titulo").text(i18nGlobals[myCountry.getCountry()].filters.editFilter);
            jQuery("#btnfiltro_excluir").show();

            $.ajax({
                url: `/${myCountry.getCountry()}/ajax/filter/getFilterById`,
                type: 'POST',
                dataType: 'json',
                data: {
                    filter_id: jQuery(this).val()
                },
                success: function (response) {
                    editForm(response.data[0].FILTER_DATA);
                    loaderHide();
                    //$("#btnfiltro_salvar").prop("disabled",false);
                },
                error: function (error) {
                    loaderHide();
                    console.log("error", error)
                    console.log("url", this.data.data)
                }
            });
        }
    });

    jQuery("#btn_filtro").on("click", function () {
        jQuery("#filter_nome").val(jQuery("select[name=select_filtro] option:selected").text());
    });
    /*********************ACIONANDO MEUS FILTROS FIM************************/

    $('.progress-bar').css("background-color", "#F96737");
    $('.progress .progress-bar').css("width", function () {
        return $(this).attr("aria-valuenow") + "%";

    });
});


function cleanForm(form) {
    document.getElementById(form).reset();
}

function pesquisar() {
    var filtros = JSON.parse(jQuery('#formulario').serializeJSON());
    loaderShow();
    document.formulario.submit();
}

//abre janela de confirmação de exclusão
function confirmDeleteFilter() {
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: "btn btn-danger mr-2",
        cancelButtonClass: "btn btn-default",
        buttonsStyling: false
    });

    swalWithBootstrapButtons({
        title: i18nGlobals[myCountry.getCountry()].filters.deleteFilter,
        type: 'warning',
        animation: false,
        showCancelButton: true,
        confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButtonYes,
        cancelButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.cancelButtonNo
    }).then((result) => {
        if (result.value) {
            deleteFilter();
        } else {
            jQuery("#confirm_delete").find('span').html(i18nGlobals[myCountry.getCountry()].delete);
            jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
            swal.close();
        }
    })
};

function setBtnSave() {
    jQuery("#btnfiltro_salvar").find('i').addClass('pln-invi');
    jQuery("#spn_save").html(i18nGlobals[myCountry.getCountry()].saving);
};

function fillFilter(dataset) {
    var select = jQuery("#select_filtro");
    select.html('');
    select.append("<option value=''></option>");
    dataset.forEach(function (item) {
        select.append(`<option value='${item.value}'>${item.name}</option>`);
    })
};

//instancia os valores para enviar ao banco de dados
function saveFilter() {
    jQuery('#filter_temp').val('');
    if (jQuery("#filter_nome").val() == "") {
        jQuery("#filter_nome").css({"border": "1px solid red"});
        return false;
    }

    jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});

    jQuery("#btnfiltro_salvar").find('i').removeClass('pln-invi');
    jQuery("#spn_save").html("Salvando");
    setForm();

    let perfil = jQuery("#filter_perfil").val(),
        name = jQuery("#filter_nome").val(),
        view_id = "SCM532G",
        filter_id = jQuery("#select_filtro").val();

    let objPost = {};
    objPost.url = (filter_id == "" ? `/${myCountry.getCountry()}/ajax/filter/saveFilter` : `/${myCountry.getCountry()}/ajax/filter/updateFilter`);
    objPost.verb = (filter_id == "" ? 'save' : 'update');
    objPost.type = 'POST';
    objPost.data = {
        filter_perfil: perfil,
        filter_nome: name,
        filter_data: formSerielized,
        filter_tela_id: view_id,
        filter_id: filter_id
    };
    filterService(objPost);
}

function deleteFilter() {
    jQuery("#confirm_delete").find('span').html(i18nGlobals[myCountry.getCountry()].deleting);
    jQuery("#confirm_delete").find('i').removeClass('fa fa-times pln-danger').addClass('fa fa-spin fa-spinner');
    let name = jQuery("#filter_nome").val(),
        filter_id = jQuery("#select_filtro").val(),
        objPost = {};
    objPost.url = `/${myCountry.getCountry()}/ajax/filter/deleteFilter`;
    objPost.verb = 'delete';
    objPost.type = 'POST';
    objPost.data = {
        filter_nome: name,
        filter_id: filter_id
    };
    filterService(objPost);
}

//responsável por enviar requisições POST ao banco de dados e tratar os retornos dependendo do verbo utilizado
function filterService(postObj) {
    $.ajax({
        url: postObj.url,
        type: postObj.type,
        dataType: 'json',
        data: postObj.data,
        success: function (response) {
            if (!response.error) {
                switch (postObj.verb) {
                    //se for delete, trato o ícone de deleção, removo o item do <select> e fecho a modal
                    case 'delete':
                        jQuery("#confirm_delete").find('span').html('Deletar');
                        jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
                        jQuery("#select_filtro").find('option[value=' + postObj.data.filter_id + ']').remove();
                        jQuery("#modal_edit").modal("hide");
                        jQuery("#btn_filtro_text").text(i18nGlobals[myCountry.getCountry()].filters.saveFilter);
                        break;
                    //se for novo registro, trato o ícone de salvamento
                    case 'save':
                        //popula o select com o novo filtro criado
                        fillFilter(response.data);
                        setBtnSave()
                        jQuery("#modal_edit").modal("hide");
                        break;
                    //se for edição de registro, trato o ícone de salvamento e atualizo o nome no <select>
                    case 'update':
                        jQuery("#select_filtro").find('option[value=' + postObj.data.filter_id + ']').text(postObj.data.filter_nome);
                        setBtnSave();
                        jQuery("#modal_edit").modal("hide");
                        break;
                }
            } else {
                alert(i18nGlobals[myCountry.getCountry()].unavailableService);
            }
        },
        error: function (error) {
            setBtnSave();
            alert(i18nGlobals[myCountry.getCountry()].unavailableService)
        }
    });
}

var formSerielized;

function setForm() {
    jQuery('#filter_temp').val('');
    formSerielized = JSON.parse(jQuery('#formulario').serializeJSON());
    jQuery("#filter_temp").val(JSON.stringify(formSerielized));
}

function editForm(formulario) {    

    let form = document.getElementById('formulario');
    let json = JSON.parse(formulario);
    delete json.filter_temp;

    for (let name in json) {        
        //se for um array, percorre seus valores
        if (typeof (json[name]) == "object") {
            json[name].forEach(function (item) {
                let field = document.getElementById(name + "_" + item);
                try{
                    field.checked = true;
                }catch(e){
                    console.log('name',name)
                }
            });
        }
    }
}