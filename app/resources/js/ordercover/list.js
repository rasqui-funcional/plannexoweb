class orderCoverController {
    constructor() {
        this.country = jQuery('#country').val();
        this.checkHasDateArrival;
        this.agroupModal = jQuery('#agroup_fields');
        this.alreadyCloned = false;
        this.definitions;
        this.objRows;
        this.limit = 20;
        this.agrupCodComprador = jQuery('.agrup_cod_comprador');
        this.agrupCodSetor = jQuery('.agrup_cod_setor');
        this.agrupMotivo = jQuery('.agrup_motivo');
        this.agrupDeposito = jQuery('.agrup_deposito');
        this.fieldsToAgroup = jQuery('.fields-to-agroup');
        this.listenerBtns();
        this.initialize();
    }

    listenerBtns() {
        jQuery('#toApprove').on('click', () => {
            this.processOrderCoverValidate();
        });

        jQuery('#toAgroup').on('click', () => {
            this.agroupOrderCoverValidate();
        });

        jQuery('#agroup_confirm').on('click', () => {
            this.agroupConfirm();
        });

        this.agroupModal.on('hidden.bs.modal', () => {
            this.clearInvalidAgroupFields();
        });
    }

    countDiffEstabs() {
        let arrSelectedEstabs = [];
        jQuery('.input_chk').each(function () {
            if (jQuery(this).prop('checked')) {
                arrSelectedEstabs.push(jQuery(this).parent().parent().find('.estab').attr('data-estab-id'));
            }
        });

        return [...new Set(arrSelectedEstabs)];
    }

    //get min date of selecteds suggestions
    getMinDate(arrOfDates) {
        let today = moment(new Date()).format('DD/MM/YYYY');
        let convertToMoment = arrOfDates.map((value) => moment((value == false ? today : value)));
        let moments = convertToMoment.map(d => moment(d)),
            minDate = moment.min(moments);
        
            return moment(minDate._i).format('DD/MM/YYYY');
    }

    agroupOrderCover() {
        let urgent = [], dateExp = [], estabId, hasDateArrival, dateArrival;
        this.objRows = [];
        jQuery('.input_chk').each((index, element) => {
            //id of estab

            //each checked Row
            if (jQuery(element).prop('checked')) {
                //get estabId to agroup
                estabId = jQuery(element).parent().parent().find('.estab').attr('data-estab-id');
                dateArrival = jQuery(element).parent().parent().find('.dt_exp_req_label').val();
                hasDateArrival =jQuery(element).parent().parent().find('[data-column]').attr('data-column');                                                
 
                let objPsug = {
                    psug: jQuery(element).attr('data-psgug'),
                    default_local: jQuery(element).attr('data-default-local')                    
                };                            

                if (hasDateArrival == 'date_arrival_altered') { 
                    objPsug.DATE_ARRIVAL_ALTERED = moment(dateArrival).format("DD/MM/YYYY");
                }

                //get each value of inputs row
                jQuery(element).parent().parent().each((idx, el) => {
                    //if any line is checked like urgent, the agroup will be urgent
                    urgent.push(jQuery(el).find('.sit_urgent').is(':checked') ? 'Y' : 'N');
                    dateExp.push(jQuery(el).find('.dt_exp_req_label').val());

                    let columns = [];
                    jQuery(el).find('[data-column]').each((index, element) => {
                        //para if para não repetir atributo date_arrival_altered que já é inserido em objPsug.DATE_ARRIVAL_ALTERED
                        if(jQuery(element).attr('data-column') != 'date_arrival_altered'){
                            columns.push({[jQuery(element).attr('data-column')]: jQuery(element).attr('data-value')})
                        }
                    });

                    this.objRows.push(Object.assign({}, objPsug, ...columns));
                });
            }
            //end each checked Row
        });

        this.definitions = {
            cod_buyer: this.agrupCodComprador.val(),
            cod_sector: this.agrupCodSetor.val(),
            cod_req_reason: this.agrupMotivo.val(),
            default_local: this.agrupDeposito.val(),
            sit_urgent: (urgent.indexOf(1) > -1 ? 'Y' : 'N'),
            dt_exp_req: this.getMinDate(dateExp),
            cod_estab_fk: estabId
        };
 
        this.sendParamsToAgroup();
    }

    sendParamsToAgroup() {
        loaderShow();

        $.ajax({
            url: `/${myCountry.getCountry()}/ordercover/setgroup`,
            data: {
                obj: this.objRows,
                definitions: this.definitions
            },
            type: 'POST',
            datatype: 'text',
            success: (response) => {
                this.agroupModal.modal('hide');
                paginationClass.init = false;
                paginationClass.getTableService(...paginationClass.paginationParams);
            },
            error: (error) => {
                this.agroupModal.modal('hide');
                console.log('response', error);
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'warning');
            }
        });
    }

    agroupOrderCoverValidate() {
        const arrEstabs = this.countDiffEstabs();

        if (arrEstabs.length == 0) {
            paginationClass.execSwal('', i18nOrderCover[myCountry.getCountry()].group.selectItems, 'warning');
        } else if (arrEstabs.length > 1) {
            paginationClass.execSwal('', i18nOrderCover[myCountry.getCountry()].group.selectError, 'warning');
        } else {
            let checkedItem = false;
            jQuery('.input_chk').each((i, element) => {
                if (jQuery(element).prop('checked')) {
                    checkedItem = element;
                    return false
                }
            });

            this.cloneOptions(jQuery(checkedItem), 'select_cod_comprador', this.agrupCodComprador);
            this.cloneOptions(jQuery(checkedItem), 'select_deposito', this.agrupDeposito);
            this.cloneOptions(jQuery(checkedItem), 'select_motivo', this.agrupMotivo);
            this.cloneOptions(jQuery(checkedItem), 'select_cod_setor', this.agrupCodSetor);
            this.agroupModal.modal('show');
        }
    }

    clearInvalidAgroupFields() {
        this.fieldsToAgroup.find('select').each((index, element) => {
            jQuery(element).removeClass('is-invalid');
        });
    }

    agroupConfirm() {
        let invalid = false;

        this.fieldsToAgroup.find('select').each((index, element) => {
            if (jQuery(element).val() == '') {
                invalid = true;
                jQuery(element).addClass('is-invalid');
            } else {
                jQuery(element).removeClass('is-invalid');
            }
        });

        if (!invalid) this.agroupOrderCover();
    }

    disableFieldsToAgroup(act) {
        this.fieldsToAgroup.find('select').prop('disabled', act);
    }

    setFieldsToAgroupToDefault() {
        this.agrupCodComprador.html('').append(`<option value=''>${i18nGlobals[myCountry.getCountry()].select}</option>`);
        this.agrupCodSetor.html('').append(`<option value=''>${i18nGlobals[myCountry.getCountry()].select}</option>`);
        this.agrupMotivo.html('').append(`<option value=''>${i18nGlobals[myCountry.getCountry()].select}</option>`);
        this.agrupDeposito.html('').append(`<option value=''>${i18nGlobals[myCountry.getCountry()].select}</option>`);
    }

    cloneOptions(curLine, cloneElement, appendToElement) {
        let options = curLine.parent().parent().find(`.${cloneElement} > option`).clone();

        //when we copy the options, the attr "selected" cannot be copied, so we remove this
        Object.entries(options).map((element) => {
            (element[1]['defaultSelected'] != undefined ? element[1]['defaultSelected'] = false : '')
        });

        appendToElement.html('');
        appendToElement.append(options);
    }

    processOrderCoverValidate() {
        let arrInputValues = [], objInputValues = [], anyCheckedRow = false, isValid = true, dateArrival, hasDateArrival;

        jQuery('.input_chk').each((index, element) => {
            //each checked Row
            if (jQuery(element).prop('checked')) {
                anyCheckedRow = true;
                dateArrival = jQuery(element).parent().parent().find('.dt_exp_req_label').val();
                hasDateArrival =jQuery(element).parent().parent().find('[data-column]').attr('data-column');

                let objDefaultLocal = {
                    original_cod_local_fk: jQuery(element).attr('data-default-local'),
                    psug: jQuery(element).attr('data-psgug'),
                };

                if (hasDateArrival == 'date_arrival_altered') {
                    objDefaultLocal.DATE_ARRIVAL_ALTERED;
                }

                //get each value of inputs row
                jQuery(element).parent().parent().find('select, input, textarea').each((i, e) => {
                    if (!jQuery(e).hasClass('no-validate') && (jQuery(e).val() == '0' || jQuery(e).val() == '')) {
                        jQuery(e).parent().parent().addClass('pln-invalid-row');
                        jQuery(e).addClass('is-invalid');

                        paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].fillRequiredFields, 'error');
                        isValid = false;
                        return false;
                    } else {
                        jQuery(e).parent().parent().removeClass('pln-invalid-row');
                        jQuery(e).removeClass('is-invalid');
                        if (jQuery(e).attr('name') != undefined && jQuery(e).attr('name') != null) {
                            arrInputValues.push({
                                [jQuery(e).attr('name')]:
                                    (jQuery(e).attr('name') == 'yn_urgent' ?
                                            (jQuery(e).prop('checked') === true ? 'Y' : 'N')
                                            : jQuery(e).val()
                                    )
                            });
                        }
                    }
                });

                let columns = [];
                jQuery(element).parent().parent().find('[data-column]').each((index, element) => {
                    columns.push({[jQuery(element).attr('data-column')]: jQuery(element).attr('data-value')})
                });

                let dynamic = {};
                dynamic.dynamicFields = Object.assign({}, ...columns);
                //end each value of inputs row

                if (arrInputValues.length > 0) {
                    objInputValues.push(Object.assign({}, dynamic, objDefaultLocal, ...arrInputValues));
                }

                arrInputValues = [];
            }
            //end each checked Row
        });

        if (!anyCheckedRow) {
            paginationClass.execSwal('', i18nOrderCover[myCountry.getCountry()].approve.selectItems, 'warning');
        } else if (isValid && anyCheckedRow) {
            this.sendParamsToProcess(objInputValues);
        }
    }

    prepareDate(obj){
        for(let i=0; i< obj.length; i++){
            try{ 
                if(obj[i].DATE_ARRIVAL_ALTERED) {
                    obj[i].DATE_ARRIVAL_ALTERED = moment(obj[i].DATE_ARRIVAL_ALTERED).format("DD/MM/YYYY")
                }
                obj[i].dt_exp_req = moment(obj[i].dt_exp_req).format("DD/MM/YYYY")
                
            }catch(e){
                console.log(e)
            }
        }
    
        return obj;
    }

    sendParamsToProcess(params) {        
        loaderShow();

        $.ajax({
            url: `/${myCountry.getCountry()}/ordercover/process`,
            data: {
                obj: this.prepareDate(params)
            },
            type: 'POST',
            datatype: 'json',
            success: (response) => {
                loaderHide();
                paginationClass.init = false;
                if(response.data.validate.length > 0)
                {
                    this.refreshTableAfterValidate();   
                }else{
                    this.refreshTable();
                }
            },
            error: (error) => {
                console.log('response', error);
                loaderHide();
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'warning');
            }
        });
    }

    refreshTableAfterValidate(){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            animation: false,
            text: i18nOrderCover[myCountry.getCountry()].invalidSuggestions,
            type: "warning",
            showCancelButton: false,
            confirmButtonText: 'Ok',
            allowOutsideClick: () => false,
            preConfirm: () => {
                paginationClass.getTableService(...paginationClass.paginationParams);
            }
        });
    }

    refreshTable(){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            animation: false,
            text: i18nOrderCover[myCountry.getCountry()].sendedToERP,
            type: "success",
            showCancelButton: false,
            confirmButtonText: 'Ok',
            allowOutsideClick: () => false,
            preConfirm: () => {
                paginationClass.getTableService(...paginationClass.paginationParams);
            }
        });
    }

    initialize() {
        utilsInstance.createRegex('.txtarea_obs', RegExp(/[A-Za-zÀ-ÿ0-9\.\-\_ ]/));
        utilsInstance.onlyNumbers('#goto_pagenumber');

        /******************************************/
        //this block will set the page to option "Processar" as default, as if it had loaded now
        this.setFieldsToAgroupToDefault();

        this.alreadyCloned = false;
        /******************************************/

        //call suggestions of ordercover
        jQuery('.suggestion_ordercover').on('click', (element) => {
            let finalObject,
                simpleObj = {
                    psug_grouped_pk: jQuery(element.currentTarget).parent().parent().find('.input_chk').attr('data-psgug'),
                    cod_estab_fk: jQuery(element.currentTarget).parent().parent().find('.estab').attr('data-estab-id'),
                    default_local: jQuery(element.currentTarget).parent().parent().find('.input_chk').attr('data-default-local'),
                },
                dynamicColumns = [];

            jQuery(element.currentTarget).parent().parent().find('[data-column]').each((index, element) => {
                dynamicColumns.push({[jQuery(element).attr('data-column')]: jQuery(element).attr('data-value')})
            });

            finalObject = (Object.assign({}, simpleObj, ...dynamicColumns));
            this.callSuggestions(finalObject);
        });
    }

    /******INITIALIZE FIM******/

    //send params and redirect to suggestions list
    callSuggestions(params) {
        loaderShow();
        
        $.ajax({
            url: `/${myCountry.getCountry()}/ordercover/suggestions/preparelist`,
            data: {
                obj: params
            },
            type: 'POST',
            datatype: 'text',
            success: (resposta) => {
                window.location.href = `/${myCountry.getCountry()}/ordercover/suggestions/list`;
            },
            error: (error) => {
                console.log('response', error);
                loaderHide();
                paginationClass.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'warning');
            }
        });
    }
}

const orderCoverClass = new orderCoverController();
const routeClass = new RoutesController();
const paginationClass = new paginationController(routeClass.getOrderCoverTable(), routeClass.goToOrderCoverFilters());