class Utils{

    constructor(){
        this.regexOnlyNumbers = RegExp(/[0-9]/);        
        this.regexAlphaNumeric = RegExp(/[A-Za-zÀ-ÿ0-9 ]/);
        this.contains = false;
    }

    /**
     * a generic method to allow/disable chars of input
     * @param {String} element one or more input ID like this ("#el1, #el2")
     * @param {String} regex  regular expression to allow/disable chars of input
     * @param {Boolean} contains indicates whether the input value entered corresponds to the regular expression     
     */
    generic(element, regex, contains){
       jQuery(element).on({
            "keypress": function (e) {
                if (e.which === 8 || e.which === 0) return;
                var s = String.fromCharCode(e.which);
                if ( regex.test(s) == contains)return false;
            }
       });
   
       jQuery(element).change(function(e){
   
           var spt = jQuery(this).val().split(''),    
           that = this;
           
           spt.forEach(el => {
               if ( regex.test(el) == contains){
                   jQuery(that).val("")
               }
           });           
       });
    }

    /**
     * allows only numeric input
     * @param {String} element one or more input ID like this ("#el1, #el2")     
     */
    onlyNumbers(element){
        this.generic(element, this.regexOnlyNumbers, this.contains = false);
    }
    
    /**
     * allows alphanumerics and some special chars
     * @param {String} element one or more input ID like this ("#el1, #el2")     
     */
    alphaNumeric(element){
        this.generic(element, this.regexAlphaNumeric, this.contains = false);
    }

    /**
     * create a filter with {reg} param 
     * @param {String} element one or more input ID like this ("#el1, #el2")
     * @param {RegExp} reg a valid regular expression     
     */
    createRegex(element, reg){
        this.generic(element, reg, this.contains = false);
    }

    /**
     * return number bigger then 0 and 1 if the field is empty
     * @param {String} element one or more input ID like this ("#el1, #el2")
     */
    biggerThenZero(element){
        jQuery(element).on({
            "keyup": (el)=> {
                if (jQuery(el.currentTarget).val() <= 0 && jQuery(el.currentTarget).val() != "" ) jQuery(el.currentTarget).val(1) ;                
            }
       });

       jQuery(element).on({
            "change": (el)=> {
                if (jQuery(el.currentTarget).val() == ""  ) jQuery(el.currentTarget).val(1) ;                
            }
        });
    }


    maxNumber(element, maxVal){
        jQuery(element).on({
            "keyup": (el)=> {
                if (jQuery(el.currentTarget).val() > maxVal && jQuery(el.currentTarget).val() != "" ) jQuery(el.currentTarget).val(maxVal) ;                
            }
       });
    }

}
var utilsInstance = new Utils();
