class paginationController{
    
    constructor(route, goBack){
        this.route = route;
        this.goBack = goBack;
        this.scrollLeft='';
        this.order = null;
        this.progressBar();
        this.listenerBtns();
        this.paginationParams = [1, 20, null, null, true, null];
        this.getTableService(...this.paginationParams);
        this.init  = false;
    }
    
    progressBar(){
        jQuery('.progress-bar').css('background-color','#F96737');
        jQuery('.progress .progress-bar').css('width',function() {
            return $(this).attr('aria-valuenow') + '%';
        });
    }

    listenerBtns(){
        //evento do combo que altera a quantidade de itens por página que o usuário deseja visualizar.
        jQuery('#items_per_page').on('change', (element)=>{
            let limit = jQuery(element.currentTarget).val(),
                searchValue = jQuery('#search_input').val(),
                recalcFooter = true,
                searchCol = jQuery('#seach_col').val();

            this.init=false;
            this.getTableService(1, limit, searchValue, searchCol, recalcFooter, this.order);
        });

        //invoca métodos ao pressionar a tecla enter no input de busca ou de paginação
        jQuery('#goto_pagenumber').on('keyup', (event)=> {
            event.preventDefault();
            if (event.keyCode === 13) {
                jQuery('#goToPageNumber').trigger('click');
            }
        });

        //ir para página X (Buscar)
        jQuery('#goToPageNumber').on('click', ()=>{
            if( parseInt(jQuery('#goto_pagenumber').val()) == jQuery('#pagination').twbsPagination('getCurrentPage') ){
                return false;
            }

            if( parseInt(jQuery('#goto_pagenumber').val()) > parseInt(jQuery('#total_page_number').val()) ){
                this.execSwal('',`${i18nGlobals[myCountry.getCountry()].table.insertPage} ${jQuery('#total_page_number').val()}`, 'warning');
                return false;
            }

            if( jQuery('#goto_pagenumber').val() == '' || parseInt(jQuery('#goto_pagenumber').val()) =='0' ){
                this.execSwal('', i18nGlobals[myCountry.getCountry()].table.insertValidPage , 'warning');
                return false;
            }

            let limit = jQuery('#items_per_page').val(),
                searchValue = jQuery('#search_input').val(),
                searchCol = jQuery('#seach_col').val(),
                recalcFooter = true,
                pageNumber = jQuery('#goto_pagenumber').val();

            this.init=false;
            this.getTableService(pageNumber, limit, searchValue, searchCol, recalcFooter, this.order);
        })
    }            

    initialize(){
        jQuery('#chk_all').on('click', (element)=>{
            jQuery('.input_chk').prop('checked', element.currentTarget.checked);
        });
        
        jQuery('.object_table_tr').on('click', function(){
            jQuery(this).parent().find('tr').each(function(){
                if( jQuery(this).hasClass('pln-row-selected') ){
                    jQuery(this).removeClass('pln-row-selected')
                }
            });
            jQuery(this).addClass('pln-row-selected');
        });

        //ordena a tabela pela coluna selecionada, de forma ASC ou DESC
        jQuery('.order-col').on('click', (element)=>{
            const icon = jQuery(element.currentTarget).find('i'),
                sort = icon.hasClass('fa-sort'),
                sortAsc = icon.hasClass('fa-sort-asc'),
                columnName = jQuery(element.currentTarget).attr('data-id'),
                searchValue = jQuery('#search_input').val(),
                limit = jQuery('#items_per_page').val(),
                recalcFooter = true,
                searchCol = jQuery('#seach_col').val();
            let order = '';

            jQuery(element.currentTarget).parent().parent().find('i').each(function(){
                jQuery(this).removeClass('fa-sort-asc fa-sort-desc').addClass('fa-sort')
            });

            order = 'desc';
            icon.removeClass('fa-sort-asc').addClass('fa-sort-desc');

            if (sort || !sortAsc) {
                icon.removeClass('fa-sort').addClass('fa-sort-asc');
                order = 'asc';
            }

            const columnOrder = {[columnName] : order};

            this.init = false;
            this.getTableService(1, limit, searchValue, searchCol, recalcFooter, columnOrder);
        });        

        window.addEventListener('beforeunload', function (event) {
            jQuery('.progress-bar').show();
        });
    }
    /******INITIALIZE FIM******/

    //função chamada pela paginação. Retorna um dataset de acordo com os parâmetros recebidos: page e limit
    getTableService(page, limit, input, searchCol, recalcFooter, order=null){
        loaderShow();
        this.scrollLeft = jQuery('#controlSize').scrollLeft();

        jQuery.ajax({
            url: this.route,
            data: {
                page: page,
                limit: limit,
                searchCol: searchCol,
                searchValue: input,
                recalcFooter: recalcFooter,
                order: order
            },
            type: 'POST',
            datatype: 'text',
            error: function (error) {
                this.execSwal('', i18nGlobals[myCountry.getCountry()].unavailableService, 'error');
            }
        }).done(response => {
            //verifica se há dados no resultado da pesquisa
            if(!this.checkResultSearch(response.trim())) return false;

            jQuery('#table_insert').html(response);
            jQuery('.pln-items-search').show();

            //se as informações do footer forem redefinidas, precisamos destruir a paginação e
            //instanciá-la novamente
            if (recalcFooter) {
                jQuery('#pagination').twbsPagination('destroy');
                this.paginate(page);
            }

            if (order) {
                this.order = order;
            }

            this.paginateLegends();

            jQuery('#controlSize').scrollLeft(this.scrollLeft);
            loaderHide();
        });
    }

     /*recebe o resultado da consulta ao banco de dados, se não houver itens,
    exibe um alerta e retorna para a página de filtros*/
    checkResultSearch(val){
        if(val == 'false'){
            const swalWithBootstrapButtons = swal.mixin({
                confirmButtonClass: 'btn pln-btn-orange',
                buttonsStyling: false,
            });

            swalWithBootstrapButtons({
                animation: false,
                text: i18nGlobals[myCountry.getCountry()].table.noItemsToDisplay,
                type: 'warning',
                showCancelButton: false,
                confirmButtonText: i18nGlobals[myCountry.getCountry()].modal.confirmButton,
                allowOutsideClick: () => false,
                preConfirm: () => {
                    if(this.goBack){
                        window.location.href = this.goBack;
                    }else{
                        loaderHide();
                    }
                }
            });

            return false;
        }

        return true;
    }

    //realiza a paginação e invoca getTableService() para popular a tabela com
    //os dados refentes à pagina e a quantidade de itens escolhida
    paginate(startPg) {
        const objPagination = jQuery('#pagination'),
              recalcFooter = false;

        objPagination.twbsPagination({
            totalPages: jQuery('#total_page_number').val(),
            startPage: startPg == undefined ? 1 : startPg*1,
            first: ' <i class="fas fa-angle-double-left"></i>',
            last: '<i class="fas fa-angle-double-right"></i>',
            prev: ' <i class="fas fa-angle-left"></i>',
            next: '<i class="fas fa-angle-right"></i>',
            visiblePages: 3,
            onPageClick: (event, page)=> {
                let limit = jQuery('#items_per_page').val(),
                    searchValue = jQuery('#search_input').val(),
                    searchCol = jQuery('#seach_col').val();

                //só executa se a página foi carregada anteriormente
                if (this.init) {
                    this.getTableService(page, limit, searchValue, searchCol, recalcFooter, this.order);
                }

                this.init = true;
                jQuery('#spn_total_page_number').html( jQuery('#total_page_number').val() );
            }
        })
    }

    paginateLegends(){
        const totalPageNumber = jQuery('#total_page_number').val(),
              itemsPerPage = parseInt(jQuery('#items_per_page').val()),
              currentPage = jQuery('#pagination').twbsPagination('getCurrentPage'),
              pageItemsTo = itemsPerPage * currentPage,
              pageItemsFrom = (pageItemsTo - itemsPerPage) + 1,
              totalItems = parseInt(jQuery('#hdn_total_items').val());

        jQuery('#spn_total_page_number').html( totalPageNumber );
        jQuery('#page_items_from').html( pageItemsFrom );
        jQuery('#page_items_to').html( pageItemsTo > totalItems ? totalItems : pageItemsTo );
        jQuery('#page_items_total').html( totalItems );
        jQuery('#total_items').html( jQuery('#page_from_to').html() );
    }

    search(toLower = false){
        let searchValue = (jQuery('#search_input').val().trim()),
            searchCol = jQuery('#seach_col').val(),
            limit = jQuery('#items_per_page').val();
        
        toLower ? searchValue = searchValue.toLowerCase() : searchValue

        this.init=false;
        this.order = null;
        this.getTableService(1, limit, searchValue, searchCol, true, this.order);
    }
    
    execSwal(header, text, alertType){
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons.fire({
            animation: false,
            html: text,
            type: alertType,
            showCancelButton: false,
            confirmButtonText: 'Ok'
        });
    }
}