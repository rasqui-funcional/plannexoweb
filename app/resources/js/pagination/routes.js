class RoutesController{
    constructor(){
        this.country = jQuery('#country').val()
        this.table = 'table'
        this.filters = 'filters'
        this.purchaseorder = 'purchaseorder'
        this.parameters = 'parameters'
        this.resume = 'resume'
        this.ordercover = 'ordercover'
        this.suggestions = 'suggestions'
    }

    getPurchaseOrderTable(){
        return `/${this.country}/${this.purchaseorder}/${this.table}`
    }
    goToPurchaseOrderFilters(){
        return `/${this.country}/${this.purchaseorder}/${this.filters}`
    }
    getPurchaseOrderOrdersTable(){
        return `/${this.country}/${this.purchaseorder}/orders/${this.table}`
    }
    getCalendarTable(){
        return `/${this.country}/${this.parameters}/calendar/${this.table}`
    }
    getPoliticsTable(){
        return `/${this.country}/${this.parameters}/politics/${this.table}`
    }
    getResumeTable(){
        return `/${this.country}/${this.resume}/${this.table}`
    }
    goToResumeFilters(){
        return `/${this.country}/${this.resume}/${this.filters}`
    }
    getOrderCoverTable(){
        return `/${this.country}/${this.ordercover}/${this.table}`
    }
    goToOrderCoverFilters(){
        return `/${this.country}/${this.ordercover}/${this.filters}`
    }
    getOrderCoverSuggTable(){
        return `/${this.country}/${this.ordercover}/${this.suggestions}/${this.table}`
    }
    goToOrderCoverList(){
        return `/${this.country}/${this.ordercover}/list`
    }
}    