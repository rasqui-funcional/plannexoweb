utilsInstance.createRegex("#filter_nome", RegExp(/[A-Za-zÀ-ÿ0-9\.\-\_ ]/));
var filterData,
	formSerielized;

function fillFilterToEdit(){
	if(filterData != null && filterData != undefined){
		jQuery("#filter_nome").val(filterData.FILTER_NOME);
		(filterData.FILTER_PERFIL == "Publico" ? $("#filter_perfil_Publico").prop("checked", true) : $("#filter_perfil_Privado").prop("checked", true));
	}
}
fillFilterToEdit()

jQuery(function(){
	//controla botão Deletar quando a modal abre
	jQuery("#modal_edit").on("show.bs.modal", function(){
		jQuery("#selected_filter").val() == "" ? jQuery("#confirm_delete").addClass("invisible") : jQuery("#confirm_delete").removeClass("invisible");
	});

	jQuery("#modal_edit").on("hide.bs.modal", function(){
		jQuery("#btnfiltro_salvar").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-save');
		jQuery("#btnfiltro_salvar").find(".saving_spn").html(i18nGlobals[myCountry.getCountry()].save);
		jQuery("#filter_nome").val("").css({"border": "2px solid #dde2ec"});
		jQuery("#btnfiltro_salvar").prop("disabled", false);
	});

	
	
	//limpa formulário sempre que a modal de filtro se abre
	jQuery(".modal-filters-dashboard").on("show.bs.modal", function(){
		cleanForm('formulario');
		jQuery("#selected_filter").val("");
		jQuery("#filter_nome").val("");
		

	});	

		/*********************ACIONANDO MEUS FILTROS************************/
		
		jQuery("#selected_filter").on("change", function(){			
			removeFromLocalStorage();
			
			cleanForm('formulario');
			loaderShow();
						
			if($("#selected_filter").val() == ""){
				jQuery("#filter_nome").val("");
				jQuery("#modal_titulo").text(i18nGlobals[myCountry.getCountry()].filters.newFilter);
							
				jQuery("#btnfiltro_excluir").hide();
				jQuery("#filter_perfil_Publico").prop("checked", true);
				$("#formulario").submit();
			}else{				
				jQuery("#filter_nome").val(jQuery("select[name=selected_filter] option:selected").text());
				jQuery("#modal_titulo").text(i18nGlobals[myCountry.getCountry()].filters.editFilter);				
				jQuery("#btnfiltro_excluir").show();				
				$("#myfilter_form").submit();				
			}
		});

		jQuery("#openFilterModal").on("click", function(){			
			cleanForm('formulario');
						
			if($("#selected_filter").val() != ""){
				jQuery("#modal_titulo").text(i18nGlobals[myCountry.getCountry()].filters.editFilter);
				jQuery("#btnfiltro_excluir").show();
			}
		});
		/*********************ACIONANDO MEUS FILTROS FIM************************/
		
		$('.progress-bar').css("background-color","#F96737");
		$('.progress .progress-bar').css("width",function() {
			return $(this).attr("aria-valuenow") + "%";
	      	
		});
		window.addEventListener("beforeunload", function (event) {
			jQuery('.progress-bar').show();
		});
		if($("#selected_filter").val() == ""){
			jQuery("#openFilterModal").hide();
		}else{
			jQuery("#openFilterModal").show();
		}
});

	function cleanForm(form){
		document.getElementById(form).reset();		
	}	

	function fillFilter(dataset){
		var select = jQuery("#selected_filter");
		select.html('');
		select.append("<option value=''></option>");
		dataset.forEach(function(item){
			select.append(`<option value='${item.value}'>${item.name}</option>`);
		})	
	};
			
	//instancia os valores para enviar ao banco de dados 
	function goToSaveFilter() {
		
		if(jQuery("#filter_nome").val() == ""){
			jQuery("#filter_nome").css({"border": "1px solid red"});
			return false;
		}else{
			jQuery("#filter_nome").css({"border": "2px solid #dde2ec"});
			
		}
		jQuery("#btnfiltro_salvar").find('i').removeClass('fa fa-save').addClass('fa fa-spin fa-spinner');
		jQuery("#btnfiltro_salvar").find(".saving_spn").html(i18nGlobals[myCountry.getCountry()].saving);
		
		
		let perfil =jQuery("#filter_perfil").val(),
		 	name = jQuery("#filter_nome").val(),
			view_id = jQuery("#filter_tela_id").val(),
			filter_id = jQuery("#selected_filter").val();
				

		let objPost = {};
			objPost.url =  ( filter_id == "" ? `/${myCountry.getCountry()}/ajax/filter/saveFilter` : `/${myCountry.getCountry()}/ajax/filter/updateFilter`);
			objPost.verb = ( filter_id == "" ? 'save' : 'update');
			objPost.type = 'POST';
			objPost.data = {
				filter_perfil: perfil,
				filter_nome: name,
				filter_tela_id: view_id,
				filter_id: filter_id,
				filter_data: formSerielized
			};
			filterService(objPost);		
	}

	function deleteFilter(){
		jQuery("#confirm_delete").find('span').html(i18nGlobals[myCountry.getCountry()].deleting);
		jQuery("#confirm_delete").find('i').removeClass('fa fa-times pln-danger').addClass('fa fa-spin fa-spinner');
		let name = jQuery("#filter_nome").val(),
			filter_id = jQuery("#selected_filter").val(),
			objPost = {};
				objPost.url = `/${myCountry.getCountry()}/ajax/filter/deleteFilter`;
				objPost.verb = 'delete';
				objPost.type = 'POST';
				objPost.data = {				
					filter_nome: name,				
					filter_id: filter_id
				};
			filterService(objPost);	
	}

	function removeFromLocalStorage() {
		localStorage.setItem('dashboardAppliedFilter', 'true');
		const arrRoutesToCall = ['balancing', 'skus', 'stock', 'insVsOuts', 'adhesion', 'urgency'];
		arrRoutesToCall.forEach(graph => {
			localStorage.removeItem(graph);
		});
	}

	//responsável por enviar requisições POST ao banco de dados e tratar os retornos dependendo do verbo utilizado
	function filterService(postObj){
		removeFromLocalStorage();

		$.ajax({
			url: postObj.url,
			type: postObj.type,
			dataType: 'json',
			data: postObj.data,
			success: function (response) {
				if(!response.error){
					switch(postObj.verb){
						//se for delete, trato o ícone de deleção, removo o item do <select> e fecho a modal
						case 'delete':						
							jQuery("#confirm_delete").find('span').html('Deletar');
							jQuery("#confirm_delete").find('i').removeClass('fa fa-spin fa-spinner').addClass('fa fa-times pln-danger');
							jQuery("#selected_filter").find('option[value='+postObj.data.filter_id+']').remove();
							jQuery("#modal_edit").modal("hide");
							jQuery("#openFilterModal").hide();
						break;
						//se for novo registro, trato o ícone de salvamento
						case 'save':							
							//popula o select com o novo filtro criado
							fillFilter(response.data);							
							jQuery("#modal_edit").modal("hide");
							jQuery("#selected_filter").val( jQuery(`option:contains(${postObj.data.filter_nome})`).val()).trigger('change');
						break;
						//se for edição de registro, trato o ícone de salvamento e atualizo o nome no <select>
						case 'update':
							jQuery("#selected_filter").find('option[value='+postObj.data.filter_id+']').text(postObj.data.filter_nome);							
							jQuery("#modal_edit").modal("hide");
						break;
					}					
				}else{
					alert(i18nGlobals[myCountry.getCountry()].unavailableService)
				}
			},
			error: function (error) {				
				jQuery("#modal_edit").modal("hide");
				alert(i18nGlobals[myCountry.getCountry()].unavailableService);
			}
		});
	}
	
	//first exec serialize, them post to backend
	function execSerialization(){
		setForm(goToSaveFilter)
	}
		
	function setForm(callback){
		jQuery("#btnfiltro_salvar").prop("disabled", true);
		formSerielized = JSON.parse(jQuery('#formulario').serializeJSON());
		callback();
	}
	