class notificationSweetAlert {
    constructor() {
        this.swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });
    }

    execSwal(header, text, alertType){
        this.swalWithBootstrapButtons({
            animation: false,
            text: text,
            type: alertType,
            showCancelButton: false,
            confirmButtonText: 'Ok'
        });
    }
}