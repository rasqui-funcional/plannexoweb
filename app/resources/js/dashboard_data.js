

function getJsonData (dataType) {
	$.ajax({
	    url: `/${myCountry.getCountry()}/dashboard/getjsondata`,
	    type: 'GET',
	    datatype: 'text',
	    success: function (resposta) {
	        var data = JSON.parse(resposta)
    		return data.dataType
	    },
	    error: function (error) {                    
	        console.log('response',error)
	        alert(i18nGlobals[myCountry.getCountry()].unavailableService)
	    }
	});
}

