$( document ).ready(function() {
  const specificCompanies = ['46', '19'];
  const companyId = jQuery('#hiddenCompanyId').val();
  const currentRoute = window.location.pathname.indexOf('dashboard')
  
  if(currentRoute == -1) {
    return false
  }

    function updateJobInfo(){
      const arrRoutesToCall = ['balancing', 'skus', 'stock', 'insVsOuts', 'adhesion', 'urgency', 'stock/weekly'];      
      
      let url = `/${myCountry.getCountry()}/jobnight/${companyId}`
      //const url = 'http://localhost/MOCK_DATA_STATUS.json'; 

      let checkCompany = companyId == localStorage.getItem('companyId');

      if( !checkCompany ) {
        arrRoutesToCall.forEach(graph => {
          localStorage.removeItem(graph);
        }); 
      }
 
      jQuery.ajax({
        url: url,
        async: true,
        dataType: 'json',
        crossDomain: true,
        type: 'GET',
        success: function(data){
          /************DASHBOARD************/
          //esse trecho de código é para a tela de dashboard, para não repetir essa requisição
          const dashboardAppliedFilter = localStorage.getItem('dashboardAppliedFilter');
          const jobDate = data.jobNightData[0].jobNightLastRunTime;
          const localStorageJobDate = localStorage.getItem('jobNightTime');          

          localStorage.setItem('plannexoPath', window.location.pathname);          

          //para garantir que os dados dos gráficos da Dashboard se atualizem após aplicar um filtro, ao sair da tela e voltar,
          //vamos exluir os dados de localStorage
          if( localStorage.getItem('plannexoPath').indexOf('dashboard') == -1 && dashboardAppliedFilter != null) {
            
            arrRoutesToCall.forEach(graph => {
              localStorage.removeItem(graph);
            });
            localStorage.removeItem('dashboardAppliedFilter'); 
          }

          //se o jobNight rodou OU se mudou login da company, exlui localStorage para buscar os dados atualizados
          if(jobDate !== localStorageJobDate) { 
            
            arrRoutesToCall.forEach(graph => {
              localStorage.removeItem(graph);
            });
            
            localStorage.setItem('jobNightTime', jobDate);
            localStorage.setItem('companyId', companyId);
            if( currentRoute != -1 ) {
              window.location.reload();
            }            
          }
          /************DASHBOARD END************/

          jQuery('#updateJobStatus').html(data.jobNightData[0].isJobNightRunning == false ? i18nGlobals[myCountry.getCountry()].updated : `${i18nGlobals[myCountry.getCountry()].updating} (NÃO PLANEJAR)`);
          const lastUpdate = new Date(data.jobNightData[0].jobNightLastRunTime);
          const locale = jQuery('#sysLocale').val();
          moment.locale(locale);
          jQuery('#updateJobLastUpdate').html(`${moment(lastUpdate).add(3, 'hours').format('L')} às ${moment(lastUpdate).add(3, 'hours').format('LT')}`);
        },
        error: function(){
          jQuery('#updateJobStatus, #updateJobLastUpdate').html(i18nGlobals[myCountry.getCountry()].errorOnLoad);
        }
      });
    };
    updateJobInfo();

    setInterval(
      function(){
        
        if(specificCompanies.includes(companyId) ) {
          updateJobInfo(); 
        }
        //a cada 30 minutos 1800000
    }, 1800000);
});
