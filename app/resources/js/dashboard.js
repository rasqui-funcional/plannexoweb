let globalStockValue,
	globalStockBalance,
	globalStockPosition,
	globalStockPositionWeekly = null,
	globalMountInsVsOutsChart,
	globalAdhesionChart,
	globalUrgencyChart,
	globalUrgencyObj ={
		type:'financialVolume',
		period: 'monthly_urgency'
	},
	globalAnimation =  {
		duration: 500,
		easing: 'out',
		startup: true
	},
	globalTextOptions = {
		color: '#333333',
		fontSize: 12,
		fontName: 'Lato, sans-serif',
		bold: false,
		italic: false,
	},
	globalStockPositionOptions = {
		animation: globalAnimation,
		tooltip: {isHtml: true},
		displayAnnotations: true,
		annotations: {
			textStyle: {...globalTextOptions, color: '#000000', auraColor: 'none',},
		},
		height: '350',
		chartArea: {
				width: "85%"
		},
		selectionMode: "multiple",
		legend: {
				position: 'top',
				alignment: 'center',
				textStyle: globalTextOptions,
		},
		isStacked:false,
		colors:['#4CBFD9', '#2a2d39'],
		hAxis: {
			textPosition: 'out',
			textStyle: globalTextOptions,
		},
		vAxis: {
			format: 'short',
			titleTextStyle: globalTextOptions,
			textStyle: {...globalTextOptions, color: '#888888'},
		},
		seriesType: 'bars',
		series: {
			0: {
				targetAxisIndex:1,
			},
			1: {
				targetAxisIndex: 0,
				type: 'line'
			}
		}
	};

jQuery(window).on("throttledresize", ()=> {
		try{
			stockValue()
			insVsOutsChart()
			stockBalanceChart()
			resizeStockPositionChart()
			adhensionIndexChart()
			financialVolume()
			urgencyPercent()
		}catch(error){
			return false;
		}
});

/**
 * Redimenciona o gráfico de posição de estoque que está visível atualmente
 */
function resizeStockPositionChart() {
	switch (jQuery("#filtroChartSelecionado").val()) {
		case 'levelService':
			serviceLevel()
			break;
		case 'excesso':
			excess()
			break;
		case 'estoqueFinanceiro':
			financialStock()
			break;
		case 'levelService':
			serviceLevel()
			break;
	}
}

const arrRoutesToCall = ['balancing', 'skus', 'stock', 'insVsOuts', 'adhesion', 'urgency'];
const p = $.when();

// google.charts.load('current', {'packages':['corechart']});
google.charts.load("visualization", "1", {
	callback: () => {
		// call each graphic route in order of page	
		buildGraphics();
  	},
	packages: ["corechart"]
});

//removed
utilsInstance.alphaNumeric("#desc_item");
utilsInstance.onlyNumbers("#cod_item_pk, #id_sku_pk");
var obj_stock_pos_labels,
	  obj_stock_balance,
	  obj_stock_balance_labels,
	  obj_locale,
	  obj_input_labels;

function setCurrency(value, localeString, minFractionDigit, localeCurrency) {
	return value.toLocaleString(localeString, { minimumFractionDigits: minFractionDigit, style: 'currency', currency: localeCurrency })
}

function setCurrencyAndConvert(value) {
	value =  Math.abs(Number(value))

		// Nine Zeroes for Billions
		return value >= 1.0e+9

		? setCurrency(value / 1.0e+9, obj_locale.string, 2 , obj_locale.currency) + "B"
		// Six Zeroes for Millions
		: value >= 1.0e+6

		? setCurrency(value / 1.0e+6, obj_locale.string, 2 , obj_locale.currency) + "M"
		// Three Zeroes for Thousands
		: value >= 1.0e+3

		? setCurrency(value / 1.0e+3, obj_locale.string, 2 , obj_locale.currency) + "K"

		: setCurrency(value, obj_locale.string, 2 , obj_locale.currency);
}

function genericAjax(route, resolve){
	return jQuery.ajax({
		url: `/${myCountry.getCountry()}/dashboard/${route}`,
		type: 'GET',
		//async: async,
		datatype: 'text',
		success: function (response) {
			switch (route) {
				case 'stock':
				case 'stock/true':
				case 'stock/false':
					globalStockPosition = response;
					localStorage.setItem('stock', JSON.stringify(globalStockPosition));
					serviceLevel();
					removeSpin(".posicaoEstoqueChart-spin");
				break;

				case 'skus':
					globalStockBalance = response.skus;
					localStorage.setItem('skus', JSON.stringify(globalStockBalance));
					stockBalanceChart();
					removeSpin(".stockBalanceChart-spin");
				break;

				case 'insVsOuts':
					globalMountInsVsOutsChart = response;
					localStorage.setItem('insVsOuts', JSON.stringify(globalMountInsVsOutsChart));
					insVsOutsChart();
					removeSpin(".plnDoubleBarLineChart-spin");
				break;

				case 'balancing':
					globalStockValue = response.stockbalance;
					localStorage.setItem('balancing', JSON.stringify(globalStockValue));
					stockValue();
					removeSpin(".stockValue-spin");
				break;

				case 'adhesion':
					if(!jQuery('#can_supplier').val()){
						globalAdhesionChart = response.adhesion;
						localStorage.setItem('adhesion', JSON.stringify(globalAdhesionChart));
						removeSpin(".plnAdhensionIndexChart-spin");
						adhensionIndexChartMonthly(globalAdhesionChart.monthly);
						adhensionPlannexo();
					}
				break;

				case 'urgency':
					globalUrgencyChart = response.urgency;
					localStorage.setItem('urgency', JSON.stringify(globalUrgencyChart));
					removeSpin(".plnUrgencyIndexChart-spin");
					financialVolume(globalUrgencyObj.period);
				break;
			}
			resolve();
		}
	});
}

function removeFromLocalStorage() {
	localStorage.setItem('dashboardAppliedFilter', 'true');
	
	arrRoutesToCall.forEach(graph => {
		localStorage.removeItem(graph);
	});
	localStorage.removeItem('stockcheckStorage/weekly');
}

function buildGraphics() {		
	arrRoutesToCall.reduce((p, url) => p.then(() => checkLocalStorage(url)), Promise.resolve());
}

function checkLocalStorage(route) {
	const promise = new Promise((resolve, reject) => {
	let checkStorage = false;

		switch (route) {
			case 'stock':
				globalStockPosition = JSON.parse(localStorage.getItem('stock'));
				if(globalStockPosition != null) {
					checkStorage = true;
					serviceLevel();
					removeSpin(".posicaoEstoqueChart-spin");
					resolve();
				}
				route += `/${checkStorage}`
				break;
	
			case 'skus':
				globalStockBalance = JSON.parse(localStorage.getItem('skus'));
				if(globalStockBalance != null) {
					checkStorage = true;
					stockBalanceChart();
					removeSpin(".stockBalanceChart-spin");
					resolve();
				}
			break;
	
			case 'insVsOuts':
				globalMountInsVsOutsChart = JSON.parse(localStorage.getItem('insVsOuts'));
				if(globalMountInsVsOutsChart != null) {
					checkStorage = true;
					insVsOutsChart();
					removeSpin(".plnDoubleBarLineChart-spin");
					resolve();
				}
			break;
	
			case 'balancing':
				globalStockValue = JSON.parse(localStorage.getItem('balancing'));
				if(globalStockValue != null) {
					checkStorage = true;
					stockValue();
					removeSpin(".stockValue-spin");
					resolve();
				}			
			break;
	
			case 'adhesion':
				globalAdhesionChart = JSON.parse(localStorage.getItem('adhesion'));
				if(!jQuery('#can_supplier').val()){
					if(globalAdhesionChart != null) {
						checkStorage = true;
						removeSpin(".plnAdhensionIndexChart-spin");
						adhensionIndexChartMonthly(globalAdhesionChart.monthly)
						adhensionPlannexo();
						resolve();
					}
				}
			break;
	
			case 'urgency':
				globalUrgencyChart = JSON.parse(localStorage.getItem('urgency'));
				if(globalUrgencyChart != null) {
					checkStorage = true;
					removeSpin(".plnUrgencyIndexChart-spin");
					financialVolume(globalUrgencyObj.period);
					resolve();
				}
			break;
		}

		if( !checkStorage ) {
			genericAjax(route, resolve);
		}	
	})
	return promise;
}

function removeSpin(el) {
	jQuery(el).attr('style', 'display:none !important');
}

/**
 * Método que coloca pontos a cada três casas em números de unidade.
 * @param {int} value valor a ser convertido
 * @return {Number} valor convertido
 */
function formatToUnity(value) {
	value =  Math.abs(Number(value))

	return Intl.NumberFormat(myCountry.getLanguage(), {maximumFractionDigits:2, style: 'decimal'}).format(value)
}


/**
 * Monta o gráfico de Balanceamento de estoque.
 */
function stockBalanceChart() {
	const data = google.visualization.arrayToDataTable([
							[i18nInfoSku[myCountry.getCountry()].headSignal.level, i18nInfoSku[myCountry.getCountry()].skus, { role: "style" }, { role: "tooltip", p: {html: true} }, {role: 'annotation'}],
							[i18nInfoSku[myCountry.getCountry()].headSignal.zero, globalStockBalance[0], "#F32938", stockBalanceTooltip(i18nInfoSku[myCountry.getCountry()].headSignal.zero, globalStockBalance[0]), formatToUnity(globalStockBalance[0])],
							[i18nInfoSku[myCountry.getCountry()].headSignal.veryLow, globalStockBalance[1], "#ff4747", stockBalanceTooltip(i18nInfoSku[myCountry.getCountry()].headSignal.veryLow, globalStockBalance[1]), formatToUnity(globalStockBalance[1])],
							[i18nInfoSku[myCountry.getCountry()].headSignal.low, globalStockBalance[2], "#30A6AE", stockBalanceTooltip(i18nInfoSku[myCountry.getCountry()].headSignal.low, globalStockBalance[2]), formatToUnity(globalStockBalance[2])],
							[i18nInfoSku[myCountry.getCountry()].headSignal.ideal, globalStockBalance[3], "#66BD2B", stockBalanceTooltip(i18nInfoSku[myCountry.getCountry()].headSignal.ideal, globalStockBalance[3]), formatToUnity(globalStockBalance[3])],
							[i18nInfoSku[myCountry.getCountry()].headSignal.high, globalStockBalance[4], "#FF9900", stockBalanceTooltip(i18nInfoSku[myCountry.getCountry()].headSignal.high, globalStockBalance[4]), formatToUnity(globalStockBalance[4])],
							[i18nInfoSku[myCountry.getCountry()].headSignal.veryHigh, globalStockBalance[5], '#F56516', stockBalanceTooltip(i18nInfoSku[myCountry.getCountry()].headSignal.veryHigh, globalStockBalance[5]), formatToUnity(globalStockBalance[5])],
						]),
			view = new google.visualization.DataView(data),
			options = {
				animation: globalAnimation,
				legend: { position: "none" },
				height: 500,
				chartArea: {
					width: '100%',
					left: 50,
					right: 30,
				},
				hAxis: {
					textStyle: globalTextOptions,
				},
				vAxis: {
					format: 'short',
					titleTextStyle: globalTextOptions,
					textStyle: {...globalTextOptions, color: '#888888'},
				},
				tooltip: { isHtml: true }
			},
			chart = new google.visualization.ColumnChart(document.getElementById("stockBalanceChart"));

	chart.draw(view, options)

	google.visualization.events.addListener(chart, 'select', function(e) {
		const invLevel = chart.getSelection()[0].row;
		redirectToSku(invLevel)
	});
}

/**
 * Monta a tooltip para o gráfico de balanceamento de estoque.
 * @param {string} label título da label(Muito Baixo, Baixo, Ideal, etc.)
 * @param {int} value número de SKUs relativos à label
 * @return {string}
 */
function stockBalanceTooltip(label, value) {
	return `<div class="p-2 pln-tooltip-chart" style="min-width:220px">
						<div class="d-flex flex-column">
								<span class="pln-tooltip-chart-bold">${label}</span>
								<span>${i18nInfoSku[myCountry.getCountry()].skus}: ${formatToUnity(value)}</span>
						</div>
				</div>`
}

/**
 * Redireciona o usuário para a lista de SKUs, filtrado por sinalizador(Zero, Muito Baixo, Baixo, Ideal, etc.)
 * @param {int} invLevel sinalizador escolhido(0: Zero, 1: Muito Baixo, 2: Baixo, 3: Ideal, 4: Alto, 5: Muito Alto)
 */
function redirectToSku(invLevel) {
	const country = myCountry.getCountry();
	let formData = JSON.parse(appliedFilters) || {};

	formData.inv_level = [invLevel];

	loaderShow();

	jQuery.ajax({
		url: '/' + country + '/sku/list/setlevelfilters/',
		type: 'POST',
		dataType: 'json',
		data: formData,
		success: function () {
			loaderHide();
			location.assign('/' + country + '/sku/list/level/')
		},
		error: function (error) {
			loaderHide();
		}
	})
}

function organizeInsAndOutsData(response) {
	let ins = [],
		outs = []
		stock = []
		date = []
	for (let i = 0; i <= response.length - 1; i++) {
		ins.push(response[i].ENTRADA > 0 ? response[i].ENTRADA : 0)
		outs.push(response[i].SAIDA > 0 ? response[i].SAIDA : 0)
		stock.push(response[i].ESTOQUE > 0 ? response[i].ESTOQUE : 0)
		date.push(response[i].PERIOD_DATE)
	}

	return {
		'ins': ins,
		'outs': outs,
		'stock': stock,
		'date': date
	}
}

function barInsAndOuts(response) {
	const dataChart = organizeInsAndOutsData(response)
	return {
		labels: dataChart.date,
		datasets: [
			{
				label: obj_input_labels.pos_estoque,
				type: "line",
				lineTension: 0,
				datalabels: {
					align: 'top',
					display: 1,
					color: 'black'
				},
				borderColor: '#292C39',
				pointBorderColor: '#000',
				pointBackgroundColor: '#fff',
				pointHoverBackgroundColor: '#fff',
				pointHoverBorderColor: '#EC932F',
				data: dataChart.stock,
				fill: false
			},
			{
				label: obj_input_labels.entrada,
				type: "bar",
				datalabels: {
					display: false,
					color: 'black'
				},
				backgroundColor: "#62A336",
				backgroundColorHover: "#3e95cd",
				data: dataChart.ins,
				fill: false
			},
			{
				label: obj_input_labels.saida,
				type: "bar",
				datalabels: {
					anchor: 'end',
					display: false
				},
				backgroundColor: "#E61717",
				backgroundColorHover: "#3e95cd",
				data: dataChart.outs
			},
		]
	}
}

function insVsOutsChart(period = 'A') {
	const response = this.retrievePeriodDataFromInsVsOut(period, globalMountInsVsOutsChart);
  const dataTable = new google.visualization.DataTable();
  let rowWithAddedData = [];
	const options = {
		animation: globalAnimation,
		annotations: {
			textStyle: {...globalTextOptions, color: '#000000', auraColor: 'none',},
		},
		tooltip: {isHtml: true},
		title: "",
		height: '350',
		chartArea: {
			width: '95%'
		},
		selectionMode: "multiple",
		legend: {
			position: 'top',
			alignment: 'center',
			textStyle: globalTextOptions,
		},
		isStacked:false,
		colors:['#5DB324', '#E63945', '#292C39'],
		hAxis: {
			title: i18nInfoSku[myCountry.getCountry()].column.period.toUpperCase(),
			titleTextStyle: globalTextOptions,
			textStyle: globalTextOptions,
		},
		vAxis: {
			format: 'short',
			textStyle: {...globalTextOptions, color: '#888888'},
		},
		seriesType: 'bars',
		series: {2: {type: 'line'}}
	};
  
  response.forEach(element => {
    rowWithAddedData.push( [
      (element.PERIOD_DATE)? element.PERIOD_DATE : element.PERIOD_DATE_WEEK,
      Number(element.ENTRADA),
      this.tooltipInVsOut(element),
      Math.abs(Number(element.SAIDA)),
      this.tooltipInVsOut(element),
      Number(element.ESTOQUE),
      this.tooltipInVsOut(element),
      setCurrencyAndConvert(element.ESTOQUE)
    ]);
  });

  dataTable.addColumn('string', i18nInfoSku[myCountry.getCountry()].column.month);
	dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.ins);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.outs);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn('number', i18nInfoSku[myCountry.getCountry()].column.stockPosition);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn({type: 'string', role: 'annotation'})

	dataTable.addRows(rowWithAddedData);

	this.chart = new google.visualization.ComboChart(document.getElementById("plnDoubleBarLineChart"));
	this.chart.draw(dataTable, options);
}

function tooltipInVsOut(obj){
	return `<div class="p-2 pln-tooltip-chart" style="min-width:220px">
						<div class="d-flex flex-column">
								<span class="pln-tooltip-chart-bold">${obj.PERIOD_DATE || obj.PERIOD_DATE_WEEK}</span>
								<span>${i18nInfoSku[myCountry.getCountry()].column.ins}: ${setCurrency(parseFloat(obj.ENTRADA), obj_locale.string, 2 , obj_locale.currency)}</span>
								<span>${i18nInfoSku[myCountry.getCountry()].column.outs}: ${setCurrency(Math.abs(parseFloat(obj.SAIDA)), obj_locale.string, 2 , obj_locale.currency)}</span>
								<span>${i18nInfoSku[myCountry.getCountry()].column.stockPosition}: ${setCurrency(parseFloat(obj.ESTOQUE), obj_locale.string, 2 , obj_locale.currency)}</span>
						</div>
				</div>`
}

/**
 * Cria o gráfico de Posição de Estoque com as três colunas padrão(data, valor da barra e valor da linha)
 * @param {string} columns As três colunas padrão
 * @return {object}
 */
function buildDataTable(columns) {
	const dataTable = new google.visualization.DataTable();
	dataTable.addColumn('string', columns[0]);
	dataTable.addColumn('number', columns[1]);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn('number', columns[2]);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn({type: 'string', role: 'annotation'});

	return dataTable;
}

/**
 * Cria a tooltip para o gráfico de Nível de Serviço
 * @param {object} element Objeto que contém os valores para o preenchimento da tooltip
 */
function serviceLevelTooltip(element) {
	const tooltipData = {
		title: element.DATE_HIST,
		row: {
			0: {
				title: i18nInfoSku[myCountry.getCountry()].column.levelOfService,
				value: `${element.SERVICE_LEVEL}%`
			},
			1: {
				title: i18nInfoSku[myCountry.getCountry()].column.coverage,
				value: `${Math.abs(element.COVER)}`
			}
		}
	}
	return stockPositionTooltip(tooltipData)
}

/**
 * Cria a tooltip para o gráfico de Excesso
 * @param {object} element Objeto que contém os valores para o preenchimento da tooltip
 */
function excessTooltip(element) {
	const tooltipData = {
		  title: element.DATE_HIST,
		  row: {
			0: {
				title: i18nInfoSku[myCountry.getCountry()].column.over,
				value: setCurrency(parseFloat(element.EXCESS), obj_locale.string, 2 , obj_locale.currency)
			},
			1: {
				title: i18nInfoSku[myCountry.getCountry()].skus,
				value: formatToUnity(element.AVAILABLE_ITEMS)
			}
		  }
	}
	return stockPositionTooltip(tooltipData)
}

/**
 * Cria a tooltip para o Estoque Financeiro
 * @param {object} element Objeto que contém os valores para o preenchimento da tooltip
 */
function financialStockTooltip(element) {
	const tooltipData = {
		title: element.DATE_HIST,
		row: {
			0: {
				title: i18nGlobals[myCountry.getCountry()].total,
				value: setCurrency(parseFloat(element.AVAILABLE_ITEMS_VALUE), obj_locale.string, 2 , obj_locale.currency)
			},
			1: {
				title: i18nInfoSku[myCountry.getCountry()].quantitySkus,
				value: element.AVAILABLE_ITEMS
			}
		}
	}
	return stockPositionTooltip(tooltipData)
}


/**
 * Cria a tooltip genérico para os gráficos de Estoque Financeiro
 * @param {object} element Objeto que contém os valores para o preenchimento da tooltip
 */
function stockPositionTooltip(element) {
	return `<div class="p-2 pln-tooltip-chart" style="min-width:150px">
				<div class="d-flex flex-column">
					<span class="pln-tooltip-chart-bold">${element.title}</span>
					<span>${element.row[0].title}: ${element.row[0].value}</span>
					<span>${element.row[1].title}: ${element.row[1].value}</span>
				</div>
			</div>`
}

function retrievePeriodDataFromInsVsOut(period, obj) {
	if (period === 'A') {
		return obj.insVsOuts
	}
	return obj.insVsOutsPerWeek
}

function retrievePeriodData(period) {
	if (period == 'A') {
		return globalStockPosition.stockposA
	}
	return globalStockPositionWeekly.stockposT
}

/**
 * Cria uma instância do gráfico de Nível de Serviço
 * @param {string} period período escolhido pelo usuário(Anual ou Trimestral)
 */
function serviceLevel(period = 'A') {
	const columns = [
    i18nGlobals[myCountry.getCountry()].month,
    i18nGlobals[myCountry.getCountry()].coverage,
    i18nInfoSku[myCountry.getCountry()].column.levelOfService
  ];
	const dataTable = buildDataTable(columns);
	const response = this.retrievePeriodData(period);
	let rowWithAddedData = [];

	globalStockPositionOptions.vAxes = {
		0: {
			viewWindow: {
				min: 0,
				max: 100
			},
			title: i18nInfoSku[myCountry.getCountry()].column.levelOfService.toUpperCase()
		},
		1: {
			minValue: 0,
			title: i18nInfoSku[myCountry.getCountry()].column.coverageDays
		}
	}

	globalStockPositionOptions.hAxis = {
		slantedText:true,
		slantedTextAngle:45,
		textStyle: globalTextOptions,
	}

	//Removendo ultimo elemento do array temporariamente
	if(response.length == 13 && period == 'A'){
		let resp_date = new Date(response[12]);
		let now = new Date();
		
		if(resp_date.getFullYear()+'/'+resp_date.getUTCMonth() == now.getFullYear()+'/'+now.getUTCMonth()) {
			response.pop();
		}
	}

	response.forEach(element => {
		rowWithAddedData.push([
			element.DATE_HIST,
			Math.abs(Number(element.COVER)),
			serviceLevelTooltip(element),
			{v: Number(element.SERVICE_LEVEL), f: `${element.SERVICE_LEVEL}%`},
			serviceLevelTooltip(element),
			`${element.SERVICE_LEVEL}%`
		])
	})

	dataTable.addRows(rowWithAddedData);

	stockPosition(dataTable, globalStockPositionOptions)
}

/**
 * Cria uma instância do gráfico de Excesso
 * @param {string} period período escolhido pelo usuário(Anual ou Trimestral)
 */
function excess(period = 'A') {
	const columns = [
    i18nGlobals[myCountry.getCountry()].month,
    i18nGlobals[myCountry.getCountry()].quantity,
    i18nInfoSku[myCountry.getCountry()].column.financialExcess
  ];
	const dataTable = buildDataTable(columns);
	const response = this.retrievePeriodData(period, globalStockPosition);
	let rowWithAddedData = [];

	response.forEach(element => {
		const formattedNumber = setCurrency(parseFloat(element.EXCESS), obj_locale.string, 2 , obj_locale.currency)
		rowWithAddedData.push([
			element.DATE_HIST,
			Number(element.AVAILABLE_ITEMS),
			excessTooltip(element),
			{v: Number(element.EXCESS), f: formattedNumber},
			excessTooltip(element),
			setCurrencyAndConvert(Number(element.EXCESS))
		])
	})

	dataTable.addRows(rowWithAddedData);

	globalStockPositionOptions.vAxes = {
		0: {
			title: i18nInfoSku[myCountry.getCountry()].column.financialExcess.toUpperCase(),
		},
		1: {
			minValue: 0,
			title: i18nInfoSku[myCountry.getCountry()].quantitySkus
		}
	};

	stockPosition(dataTable, globalStockPositionOptions)
}

/**
 * Cria uma instância do gráfico de Excesso
 * @param {string} period período escolhido pelo usuário(Anual ou Trimestral)
 */
function financialStock(period = 'A') {
	const columns = [
    i18nGlobals[myCountry.getCountry()].month,
    i18nGlobals[myCountry.getCountry()].quantity,
    i18nGlobals[myCountry.getCountry()].total
  ];
	const dataTable = buildDataTable(columns);
	const response = this.retrievePeriodData(period, globalStockPosition);
	let rowWithAddedData = [];

	response.forEach(element => {
		const formattedNumber = setCurrency(parseFloat(element.AVAILABLE_ITEMS_VALUE), obj_locale.string, 2 , obj_locale.currency)
		rowWithAddedData.push([
			element.DATE_HIST,
			Number(element.AVAILABLE_ITEMS),
			financialStockTooltip(element),
			{v: Number(element.AVAILABLE_ITEMS_VALUE), f: formattedNumber},
			financialStockTooltip(element),
			setCurrencyAndConvert(Number(element.AVAILABLE_ITEMS_VALUE))
		])
	})

	dataTable.addRows(rowWithAddedData);

	globalStockPositionOptions.vAxes = {
		0: {
			minValue: 0,
			title: i18nInfoSku[myCountry.getCountry()].column.financialTotal.toUpperCase(),
		},
		1: {
			minValue: 0,
			title: i18nInfoSku[myCountry.getCountry()].quantitySkus.toUpperCase(),
		}
	};

	stockPosition(dataTable, globalStockPositionOptions)
}

/**
 * Cria os gráficos de Posição de Estoque.
 * @param dataTable instância do google charts com os dados do gráfico selecionado
 * @param options opções de estilo para o gráfico selecionado.
 */
function stockPosition(dataTable, options) {
	const chart = new google.visualization.ComboChart(document.getElementById('posicaoEstoqueChart'));
	chart.draw(dataTable, options);
}

function getStockWeekly() {
	jQuery(".pln-pos-estoque-periodo-row").find("a").each(function () {
		jQuery(this).addClass("pln-link-color").removeClass("pln-link-decoration-none");
	});
	jQuery('#pde_trimestral').removeClass("pln-link-color").addClass("pln-link-decoration-none");

	globalStockPositionWeekly = JSON.parse(localStorage.getItem('stock/weekly'));
	if(globalStockPositionWeekly != null) {
		auxStockPeriod();
		return false;
	}
	
	jQuery('#posicaoEstoqueChart').hide();
	jQuery('.posicaoEstoqueChart-spin').attr('style', 'display:flex !important');

	jQuery.ajax({
		url: `/${myCountry.getCountry()}/dashboard/stock/weekly/true`,
		type: 'GET',		
		datatype: 'text',
		success: function (response) {
			jQuery('#posicaoEstoqueChart').show();
			globalStockPositionWeekly = response;
			localStorage.setItem('stock/weekly', JSON.stringify(globalStockPositionWeekly));
			auxStockPeriod();
			
		}
	});
	
}

function auxStockPeriod() {
	jQuery("#filtroPeriodoSelecionado").val('T');
	const chartSel = jQuery("#filtroChartSelecionado").val();
	if (chartSel == "levelService") {
		serviceLevel('T')
	}
	else if (chartSel == "excesso") {
		excess('T')
	}
	else if (chartSel == "estoqueFinanceiro") {
		financialStock('T')
	}			
	removeSpin(".posicaoEstoqueChart-spin");
}

function getStockMonthly() {
		
	jQuery(".pln-pos-estoque-periodo-row").find("a").each(function () {
		jQuery(this).addClass("pln-link-color").removeClass("pln-link-decoration-none");
	});

	jQuery('#pde_anual').removeClass("pln-link-color").addClass("pln-link-decoration-none");

	jQuery("#filtroPeriodoSelecionado").val('A');
	const chartSel = jQuery("#filtroChartSelecionado").val();

	if (chartSel == "levelService") {
		serviceLevel('A')
	}
	else if (chartSel == "excesso") {
		excess('A')
	}
	else if (chartSel == "estoqueFinanceiro") {
		financialStock('A')
	}
}

function trocaPeriodoEntradaSaida(periodo, elem) {
	jQuery(".pln-pos-entrada-saida-row").find("a").each(function () {
		jQuery(this).addClass("pln-link-color").removeClass("pln-link-decoration-none");
	})
	jQuery("#" + elem).removeClass("pln-link-color").addClass("pln-link-decoration-none");

	insVsOutsChart(periodo);
}

function trocaFiltroChart(chart) {
	//Destaca o link ativo para que o usuário saiba em qual tipo de gráfico está
	jQuery(".pln-pos-estoque-row").find("a").each(function () {
		jQuery(this).addClass("pln-link-color").removeClass("pln-link-decoration-none");

	})
	jQuery("#" + chart).removeClass("pln-link-color").addClass("pln-link-decoration-none")
	//fim

	jQuery("#filtroChartSelecionado").val(chart);
	const periodo = jQuery("#filtroPeriodoSelecionado").val(),
		  chartSel = jQuery("#filtroChartSelecionado").val();

	if (chart == "levelService") {
		serviceLevel(periodo)
	}
	else if (chartSel == "excesso") {
		excess(periodo)
	}
	else if (chartSel == "estoqueFinanceiro") {
		financialStock(periodo)
	}
}

function stockValue(){
	const response = globalStockValue
  const dataTable = new google.visualization.DataTable();
  let options = {
    tooltip: {isHtml: true},
    chartArea: {
      width: '100%',
      height: '100%',
      left: 70,
      top: 10,
      bottom: 20,
    },
    bar: {groupWidth: "40%"},
    annotations: {
      textStyle: {...globalTextOptions, bold: true, color: '#000000', auraColor: 'none',},
      stemColor : 'none',
    },
    vAxis: {
      title: `${i18nGlobals[myCountry.getCountry()].quantity} (${i18nGlobals[myCountry.getCountry()].countryCurrency})`,
      titleTextStyle: globalTextOptions,
      textStyle: {...globalTextOptions, color: '#888888'},
      format: 'short',
    },
    seriesType: 'bars',
    legend: { position: "none" },
    series: {
      0: {type: 'line'},
      1: {type: 'line'}
    },
    
  };
  let container = document.getElementById('stockValue');
  var chart = new google.visualization.ComboChart(container);

	dataTable.addColumn('string');
	dataTable.addColumn('number', i18nGlobals[myCountry.getCountry()].minimum);
	dataTable.addColumn({'type': 'string', 'role': 'style'});
	dataTable.addColumn({type: 'string', role: 'annotation'});
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn('number', i18nGlobals[myCountry.getCountry()].maximum);
	dataTable.addColumn({'type': 'string', 'role': 'style'});
	dataTable.addColumn({type: 'string', role: 'annotation'});
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});         
	dataTable.addColumn('number');
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
  dataTable.addColumn({'type': 'string', 'role': 'style'});

	dataTable.addRows([
		[   i18nGlobals[myCountry.getCountry()].available,
			parseFloat(obj_stock_min), //valor minimo
			'#B31212', //cor da linha do valor minimo
			'', //nome que aparece no valor minimo      
			prepareTooltipForstockValueMinMax(i18nGlobals[myCountry.getCountry()].available, i18nGlobals[myCountry.getCountry()].minimum,  setCurrency(parseFloat(obj_stock_min), obj_locale.string, 2 , obj_locale.currency)),
			
			parseFloat(obj_stock_max), //valor maximo
			'#C77700', //cor da linha do valor maximo 
			'', //nome que aparece no valor maximo
			prepareTooltipForstockValueMinMax(i18nGlobals[myCountry.getCountry()].available, i18nGlobals[myCountry.getCountry()].maximum,  setCurrency(parseFloat(obj_stock_max), obj_locale.string, 2 , obj_locale.currency)),
			
			parseFloat(response.AVAILABLE_VALUE), //valor da barra de estoque disponivel
			prepareTooltipForstockValue(response.QTY_DISPONIBLE, response.AVAILABLE_LABEL, response.AVAILABLE_COVER, i18nGlobals[myCountry.getCountry()].available.toUpperCase()),
			setColorStockBalance(response.AVAILABLE_VALUE) //cor da barra de estoque disponivel
		],
		[   i18nGlobals[myCountry.getCountry()].total,
			parseFloat(obj_stock_min), //valor minimo
			'#B31212', //cor da linha do valor minimo
			i18nGlobals[myCountry.getCountry()].min, //nome que aparece no valor minimo
			prepareTooltipForstockValueMinMax(i18nGlobals[myCountry.getCountry()].total, i18nGlobals[myCountry.getCountry()].minimum,  setCurrency(parseFloat(obj_stock_min), obj_locale.string, 2 , obj_locale.currency)),
			
			parseFloat(obj_stock_max), //valor maximo
			'#C77700', //cor da linha do valor maximo 
			i18nGlobals[myCountry.getCountry()].max, //nome que aparece no valor maximo
			prepareTooltipForstockValueMinMax(i18nGlobals[myCountry.getCountry()].total, i18nGlobals[myCountry.getCountry()].maximum,  setCurrency(parseFloat(obj_stock_max), obj_locale.string, 2 , obj_locale.currency)),

			parseFloat(response.TOTAL_VALUE), //valor da barra de estoque total
			prepareTooltipForstockValue(response.QTY_TOTAL, response.TOTAL_LABEL, response.TOTAL_COVER, i18nGlobals[myCountry.getCountry()].total.toUpperCase()),
			setColorStockBalance(response.TOTAL_VALUE), //cor da barra de estoque total
		],
	])

		jQuery('#stockValueChart').show();
		chart.draw(dataTable, options);

		stockValueInfo(response);

}
//FIM stockValue 2.0

function stockValueInfo(response){
	jQuery('#stockValue-disp-col')
	.html( `<div class="font-weight-bold ${setColorTextStockBalance(response.AVAILABLE_VALUE)}">
				${i18nGlobals[myCountry.getCountry()].countryCurrency} ${response.AVAILABLE_LABEL}
			</div>
			<div class="pln-text-label-small">${response.AVAILABLE_COVER} ${obj_stock_balance_labels.dias_cob}</div>
      <div class="pln-text-label-small">${i18nInfoSku[myCountry.getCountry()].spin}: ${response.AVAILABLE_TURNOVER}</div>`);

	jQuery('#stockValue-total-col')
	.html(` <div class="font-weight-bold ${setColorTextStockBalance(response.TOTAL_VALUE)}">
				${i18nGlobals[myCountry.getCountry()].countryCurrency} ${response.TOTAL_LABEL}
			</div>
			<div class="pln-text-label-small">${response.TOTAL_COVER} ${obj_stock_balance_labels.dias_cob}</div>
			<div class="pln-text-label-small">${i18nInfoSku[myCountry.getCountry()].spin}: ${response.TOTAL_TURNOVER}</div>`)
}

function setColorTextStockBalance(value){
	if(parseInt(value) <= parseInt(obj_stock_min)){
		return 'pln-text-min-color';
	}
	if(parseInt(value) > parseInt(obj_stock_min) && parseInt(value) <= parseInt(obj_stock_max)){
		return 'pln-text-ideal-color';
	}
	return 'pln-text-max-color';
}

function prepareTooltipForstockValue(unit, value, days, title){
	return `<div class="p-2 pln-tooltip-chart" style="min-width:150px">
				<div class="d-flex flex-column">
					<span class="pln-tooltip-chart-bold">${title}</span>
					<span>${numberFormatter(unit)} ${i18nGlobals[myCountry.getCountry()].unityAbbr}</span>
					<span>${i18nGlobals[myCountry.getCountry()].countryCurrency} ${value}</span>
					<span>${days} ${i18nInfoSku[myCountry.getCountry()].daysOfCoverage}</span>
				</div>
			</div>`;
}

function prepareTooltipForstockValueMinMax(title, label,  value){
	return `<div class="p-2 pln-tooltip-chart" style="min-width:150px">
				<div class="d-flex flex-column">
					<span class="pln-tooltip-chart-bold text-uppercase">${title}</span>
					<span>${label}</span>
					<span>${value}</span>
				</div>
			</div>`;
}

function numberFormatter(value){
  let numberValue = Number(value);
  if(Number.isInteger(numberValue) == true){
    return numberValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  return numberValue.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
}

function setColorStockBalance(value){
	if(parseInt(value) <= parseInt(obj_stock_min)){
		return '#E63939';
	}
	if(parseInt(value) > parseInt(obj_stock_min) && parseInt(value) <= parseInt(obj_stock_max)){
		return '#91D962';
	}
	return '#FFB340';
}

function financialVolume(period) {
	globalUrgencyObj.type = 'financialVolume'
	const dataTable = new google.visualization.DataTable();
  let rowWithAddedData = [];
  const options = {
		tooltip: {isHtml: true},
		animation: globalAnimation,
		height: 300,
		colors:['#DDDDDD', '#FF7F7D', '#E62F2C'],
		chartArea: {
			width: '100%',
			left: 120,
			right: 50,
		},
		legend: { 
			position: 'top', 
			alignment: 'center',
			textStyle: globalTextOptions,
		},
		hAxis: {
			textStyle: globalTextOptions,
		},
		vAxis: {
			title: `${i18nGlobals[myCountry.getCountry()].quantity} (${i18nGlobals[myCountry.getCountry()].countryCurrency}%)`,
			titleTextStyle: globalTextOptions,
			textStyle: {...globalTextOptions, color: '#888888'},
			viewWindow: {
				min: 0,
			},
			format: 'short',
		},
	}

	dataTable.addColumn('string', i18nGlobals[myCountry.getCountry()].date);
	dataTable.addColumn('number', `${i18nInfoSku[myCountry.getCountry()].column.approvedTotal} (${i18nGlobals[myCountry.getCountry()].countryCurrency})`);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn('number', `${i18nInfoSku[myCountry.getCountry()].classifiedAsUrgent} (${i18nGlobals[myCountry.getCountry()].countryCurrency})`);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn('number', `${i18nInfoSku[myCountry.getCountry()].urgentLeadTime} (${i18nGlobals[myCountry.getCountry()].countryCurrency})`);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

	dataChartUrgency = globalUrgencyChart.weekly

	if(period == 'monthly_urgency'){
		dataChartUrgency = globalUrgencyChart.monthly
	}

	dataChartUrgency.forEach(element => {
		rowWithAddedData.push( [
			element.DATE,
			Number(element.TOTAL),
			this.urgencyChartTooltip(element),
			Number(element.TOTAL_URGENT),
			this.urgencyChartTooltip(element),
			Number(element.TOTAL_URGENT_LT),
			this.urgencyChartTooltip(element),
		]);
	});

	dataTable.addRows(rowWithAddedData);

	this.chart = new google.visualization.ColumnChart(document.getElementById("financialVolumeChart"));
	this.chart.draw(dataTable, options);
}

function urgencyPercent(period) {
	globalUrgencyObj.type = 'urgencyPercent'
	const dataTable = new google.visualization.DataTable();
  let rowWithAddedData = [];
  const options = {
		isStacked: 'true',
		displayAnnotations: true,
		annotations: {
			textStyle: globalTextOptions,
		},
		tooltip: {isHtml: true},
		animation: globalAnimation,
		height: 300,
		pointSize: 5,
		dataOpacity: 0.5,
		colors:['#FF7F7D', '#A61922'],
		chartArea: {
			width: '100%',
			left: 120,
			right: 50,
		},
		legend: { 
			position: 'top', 
			alignment: 'center',
			textStyle: globalTextOptions,
		},
		hAxis: {
			textStyle: globalTextOptions,
		},
		vAxis: {
			title: i18nInfoSku[myCountry.getCountry()].percentOfUrgency.toUpperCase(),
			titleTextStyle: globalTextOptions,
			textStyle: {...globalTextOptions, color: '#888888'},
			viewWindow: {
				min: 0,
				max: 100
			},
		},
	}

	dataTable.addColumn('string', i18nGlobals[myCountry.getCountry()].date);
	dataTable.addColumn('number', `${i18nInfoSku[myCountry.getCountry()].classifiedAsUrgent} (${i18nGlobals[myCountry.getCountry()].countryCurrency})`);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn({type: 'string', role: 'annotation'})
	dataTable.addColumn('number', `${i18nInfoSku[myCountry.getCountry()].urgentLeadTime} (${i18nGlobals[myCountry.getCountry()].countryCurrency})`);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn({type: 'string', role: 'annotation'});
	
	dataChartUrgency = globalUrgencyChart.weekly
	
	if(period == 'monthly_urgency'){
		dataChartUrgency = globalUrgencyChart.monthly
	}

	dataChartUrgency.forEach(element => {
		rowWithAddedData.push( [
			element.DATE,
			Number(element.PERC_TOTAL_URGENT),
			this.urgencyChartTooltip(element),
			String(`${element.PERC_TOTAL_URGENT}%`),
			Number(element.PERC_TOTAL_URGENT_LT),
			this.urgencyChartTooltip(element),
			String(`${element.PERC_TOTAL_URGENT_LT}%`),
		]);
	});

	dataTable.addRows(rowWithAddedData);

	this.chart = new google.visualization.LineChart(document.getElementById("urgencyPercentChart"));
	this.chart.draw(dataTable, options);
}

function urgencyChartTooltip(obj) {
	return `<div class="p-2 pln-tooltip-chart pln-urgency-tooltip-chart">
				<div class="d-flex flex-column">
					<span class="pln-tooltip-chart-bold">${obj.DATE}</span>
					<hr class="pln-tooltip-chart-hr" >
					<span>
						<span class="pln-tooltip-chart-bold pln-urgency-tooltip-color">${i18nInfoSku[myCountry.getCountry()].urgentLeadTime}:</span> ${obj.PERC_TOTAL_URGENT_LT}% (${setCurrency(parseFloat(obj.TOTAL_URGENT_LT), obj_locale.string, 2 , obj_locale.currency)})
					</span>
					<span>
						<span class="pln-tooltip-chart-bold pln-urgency-tooltip-color">${i18nInfoSku[myCountry.getCountry()].classifiedAsUrgent}:</span> ${obj.PERC_TOTAL_URGENT}% (${setCurrency(parseFloat(obj.TOTAL_URGENT), obj_locale.string, 2 , obj_locale.currency)})
					</span>
						<hr class="pln-tooltip-chart-hr" >
					<span>
						<span class="pln-tooltip-chart-bold">${i18nGlobals[myCountry.getCountry()].planned}:</span> ${calcPlanned(obj.TOTAL, obj.TOTAL_URGENT)} 
					</span>
					<span>
						<span class="pln-tooltip-chart-bold">${i18nInfoSku[myCountry.getCountry()].column.approvedTotal}:</span> ${setCurrency(parseFloat(obj.TOTAL), obj_locale.string, 2 , obj_locale.currency)}
					</span>
				</div>
			</div>`
}

function urgencyChartType(idType, divClass) {
	globalUrgencyObj.type = idType
	dateFilter(idType, divClass)
	buildUrgencyChart()
}

function urgencyChartPeriod(idPeriod, divClass) {
	globalUrgencyObj.period = idPeriod
	dateFilter(idPeriod, divClass)
	buildUrgencyChart()
}

function buildUrgencyChart(){
	if(globalUrgencyObj.type == 'financialVolume'){
		jQuery('#urgencyPercentChart').attr('style', 'display:none');
		jQuery('#financialVolumeChart').attr('style', 'display:block');
		financialVolume(globalUrgencyObj.period)
	}else{
		jQuery('#financialVolumeChart').attr('style', 'display:none');
		jQuery('#urgencyPercentChart').attr('style', 'display:block');
		urgencyPercent(globalUrgencyObj.period)
	}
}

function calcPlanned(totalValue, classUrgValue ){
	totalPlanned = totalValue - classUrgValue
	totalPlannedPercent = 0
	if(totalValue > 0){
		totalPlannedPercent = totalPlanned/(totalValue/100)
	}
	
	return `${parseFloat(totalPlannedPercent).toFixed(2)}% (${setCurrency(totalPlanned, obj_locale.string, 2 , obj_locale.currency)})`
}

/**
 * Cria o gráfico de ÍNDICE DE ADERÊNCIA
 */
function adhensionIndexChart(result) {
	const dataTable = new google.visualization.DataTable();
  let rowWithAddedData = [];
  const options = {
		tooltip: {isHtml: true},
		animation: globalAnimation,
		height: '350',
		pointSize: 5,
		dataOpacity: 0.5,
		colors:['#29859A', '#F96737'],
		chartArea: {
			width: '100%',
			left: 120,
			right: 50,
		},
		legend: { 
			position: 'top', 
			alignment: 'center',
			textStyle: globalTextOptions,
		},
		hAxis: {
			textStyle: globalTextOptions,
		},
		vAxis: {
			title: `${i18nGlobals[myCountry.getCountry()].adhesion.toUpperCase()} (%)`,
			titleTextStyle: globalTextOptions,
			textStyle: {...globalTextOptions, color: '#888888'},
			viewWindow: {
				min: 0,
				max: 100
			},
		},
		seriesType: 'area',
		series: {1: {type: 'line'}}
	};

	dataTable.addColumn('string', i18nGlobals[myCountry.getCountry()].date);
	dataTable.addColumn('number', `${i18nGlobals[myCountry.getCountry()].approvedInPlannexo} (%)`);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	dataTable.addColumn('number', `${i18nGlobals[myCountry.getCountry()].approvedInErp} (%)`);
	dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	
	result.forEach(element => {
		rowWithAddedData.push( [
			element.DATE,
			Number(element.PLANNEXO),
			this.adhensionTooltip(element),
			Number(element.ERP),
			this.adhensionTooltip(element),
		]);
	});

	dataTable.addRows(rowWithAddedData);

	this.chart = new google.visualization.ComboChart(document.getElementById("adhensionIndexChart"));
	this.chart.draw(dataTable, options);
}

function adhensionTooltip(obj) {
	return `<div class="p-2 pln-tooltip-chart" style="min-width:220px">
				<div class="d-flex flex-column">
					<span class="pln-tooltip-chart-bold">${obj.DATE}</span>
					<hr class="pln-tooltip-chart-hr" >
					<span>${i18nGlobals[myCountry.getCountry()].approvedInPlannexo}: ${obj.PLANNEXO}%</span>
					<span>${i18nGlobals[myCountry.getCountry()].approvedInErp}: ${obj.ERP}%</span>
				</div>
			</div>`
}

function dateFilter(elementId, divClass) {
	jQuery(divClass).addClass("pln-link-color").removeClass("pln-link-decoration-none");	
	jQuery("#" + elementId).removeClass("pln-link-color").addClass("pln-link-decoration-none");
}

function adhensionIndexChartMonthly(){
	dateFilter('monthly_adhension', '.pln-link-adhension-period')
	adhensionIndexChart(globalAdhesionChart.monthly)
}

function adhensionIndexChartWeekly(){
	dateFilter('weekly_adhension', '.pln-link-adhension-period')
	adhensionIndexChart(globalAdhesionChart.weekly)
}

function adhensionPlannexo(){
	const plannexo = globalAdhesionChart.total
	jQuery('.pln-adhension-date').html(`${i18nGlobals[myCountry.getCountry()].adhesionIn} ${getCurrentDate()}`);
	jQuery('.pln-adhension-index-value').html(`${plannexo.PLANNEXO}%`);
	jQuery('.pln-adhension-vertical-graph').attr("style", `height:${plannexo.PLANNEXO}%`);
}

function getCurrentDate(){
	let dateObj = new Date()
	let dateString = dateObj.toLocaleString('pt-BR', {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit',
	}).replace(/\//g, '/')

	return dateString;
}