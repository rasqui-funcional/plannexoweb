class fullScreenController{
    constructor(){
        this.btnOpenFullscreen = jQuery("#openFullscreen, #openFullscreenMoreOptions");
        this.elemToOpenFullscreen = document.getElementById("controlSize");
        this.eachRow = jQuery(".table_tr");
        this.isOpen = false;
        this.listener();
        this.openFullscreen();
    }

    listener(){
      this.eachRow.on("dblclick",(e)=> {
        //verify if the window already is fullscreen
        if (this.isOpen) {
          this.closeFullscreen();
        }        
      })

      document.addEventListener("fullscreenchange", ()=> {          
        if (!document.fullscreenElement) {
          this.isOpen = false
        } 
      });

      document.addEventListener("mozfullscreenchange", ()=> {
        if (!document.fullscreenElement) {
          this.isOpen = false
        } 
      });

      document.addEventListener("webkitfullscreenchange", ()=> {
        if (!document.fullscreenElement) {
          this.isOpen = false
        } 
      });

      document.addEventListener("msfullscreenchange", ()=> {
        if (!document.fullscreenElement) {
          this.isOpen = false
        } 
      });
    }
    
    openFullscreen(){
      
        this.btnOpenFullscreen.on("click", ()=>{
            
          try{
            this.isOpen = true; 
            if (this.elemToOpenFullscreen.requestFullscreen) {
                this.elemToOpenFullscreen.requestFullscreen();
                } else if (this.elemToOpenFullscreen.mozRequestFullScreen) { /* Firefox */
                this.elemToOpenFullscreen.mozRequestFullScreen();
                } else if (this.elemToOpenFullscreen.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
                this.elemToOpenFullscreen.webkitRequestFullscreen();
                } else if (this.elemToOpenFullscreen.msRequestFullscreen) { /* IE/Edge */
                this.elemToOpenFullscreen.msRequestFullscreen();
            }     
          }catch(e){
            return;
          }
                
        })
    }

    closeFullscreen() {
        this.isOpen = false;
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) { /* Firefox */
          document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
          document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { /* IE/Edge */
          document.msExitFullscreen();
        }
      
    }
}