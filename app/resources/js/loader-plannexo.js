function loaderShow(){
    jQuery('#pln-loader').fadeIn();
    jQuery('body').addClass('overflow-hidden');
    jQuery('.content-w').addClass('filter-blur');
};

function loaderHide(){
    jQuery('#pln-loader').fadeOut();
    jQuery('body').removeClass('overflow-hidden');
    jQuery('.content-w').removeClass('filter-blur');
}