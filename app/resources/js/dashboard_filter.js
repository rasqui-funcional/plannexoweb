$(function(){
    let dashboard = $(".modal-dashboard");
    //this function check each checkbox by the element <fieldset> as parent of element $this
    $(`[id^="chk_check_all_"]`).click(function () {    
            $(this).parent().parent().parent().find('input:checkbox').prop('checked', this.checked);    
        });
    //end

    //clear all inputs
    $("#limpar_filtros").on("click", function(){
        dashboard.find("input:checkbox").prop("checked", false);
        dashboard.find("input:text, select").val("");
    });
    //clear all inputs end
});