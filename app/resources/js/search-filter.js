//essa função filtra a lista de valores para o usuario selecionar uma opção facilmente,
	//sem necessidade de rolar o scroll
	function search_filter(el_input, el_div) {
		var input, filter, div_parent, div_child, a, i;
		input = document.getElementById(el_input);
		filter = input.value.toUpperCase();
		div_parent = document.getElementById(el_div);
		
		div_child = div_parent.getElementsByTagName("div");
		
		for (i = 0; i < div_child.length; i++) {        
			a = div_child[i].getElementsByTagName("label")[0];
			
			if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {				
				div_child[i].setAttribute('style', 'display:flex !important');
			} else {			
				div_child[i].setAttribute('style', 'display:none !important');
			}
		}
	}
	//esconde ou mostra o filtro para aproveitar melhor o espaço da tela
	function hide_filter(field){		
		jQuery(`#${field}`).toggle('fast').focus();
	}