jQuery('.progress-bar').css("background-color","#F96737");
jQuery('.progress .progress-bar').css("width",function() {
    return jQuery(this).attr("aria-valuenow") + "%";
});

let aux_excessAlreadyLoaded;
let init = false;
let haveChanges = false;
let columnOrder = {};
let orderAlreadyLoaded;
let global_scroll = {
    top: 0,
    left: 0,
};

function initialize() {
    listenStatusChange();
    /*window.onbeforeunload = confirmExit;
    function confirmExit()
    {
      return "text";
    }*/

    utilsInstance.onlyNumbers("#goto_pagenumber");

    const fullScreen = new fullScreenController();

        jQuery(".input_just_number").keypress(function(e){
        const charCode = (e.which) ? e.which : event.keyCode
        
         if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 44)){            
            return false;
         }
    });


    /*************************marcadores AN e AP*************************/
    jQuery("#an_all").on("click", function(){
        jQuery(".input_an").prop('checked', this.checked)
        .parent()
        .css({"background-color" : this.checked ? "#30a6ae" : "#cccccc"});

        if(!this.checked){
            jQuery(".input_ap").prop('checked', false)
            .parent()
            .css({"background-color" : "#cccccc"});
        }
    });

    //permite apenas números
    jQuery(".input_just_number").keypress(function(e){
        const charCode = (e.which) ? e.which : event.keyCode
        
         if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 44)){            
            return false;
         }
    });

    jQuery("#ap_all").on("click", function(){
        if(this.checked){
            jQuery(".input_ap, .input_an").prop('checked', this.checked)
            .parent()
            .css({"background-color" : this.checked ? "#30a6ae" : "#cccccc"});
            jQuery("#an_all").prop('checked', true);
        }else{
            if(!this.checked){
                jQuery(".input_ap").prop('checked', false)
                .parent()
                .css({"background-color" : "#cccccc"});
            }
        }
    });

    //marca/desmarca o checkbox AN e colore o fundo da td
    jQuery(".input_an").on('click', function(){
        if(jQuery(this).is(":checked")){
            jQuery(this).parent().css({"background-color" : "#30a6ae"});
        }else{
            jQuery(this).parent().css({"background-color" : "#cccccc"});
            jQuery(this).parent().parent().find('.input_ap').prop('checked', false)
            .parent().css({"background-color" : "#cccccc"});

        }
    });


    jQuery(".table_tr").on("click", function(){
        jQuery(this).parent().find("tr").each(function(){
            if( jQuery(this).hasClass("pln-row-selected") ){
                jQuery(this).removeClass("pln-row-selected")
            }
        });
        jQuery(this).addClass("pln-row-selected");
    });

    //ordena a tabela pela coluna selecionada, de forma ASC ou DESC
    jQuery(".order-col").on("click", function() {
        if (fullScreen.isOpen) {
            fullScreen.closeFullscreen();
        }

        let this_icon = jQuery(this).find("i"),
            sort = this_icon.hasClass("fa-sort"),
            sortAsc = this_icon.hasClass("fa-sort-asc"),
            iconEl = this_icon,
            order = "",
            columnName = jQuery(this).attr('data-id'),
            searchValue = jQuery('#suggest-search-input').val(),
            limit = jQuery("#items_per_page").val(),
            searchCol = jQuery('#suggest-seach-col').val(),
            recalcFooter = true;
            
        
        jQuery(this).parent().parent().find("i").each(function(){
            jQuery(this).removeClass("fa-sort-asc fa-sort-desc").addClass("fa-sort")
        });

        if(sort){
            this_icon.removeClass("fa-sort").addClass("fa-sort-asc");
            order="asc";
        }else{
            if(sortAsc){
                iconEl.removeClass("fa-sort-asc").addClass("fa-sort-desc");
                order="desc";
            }else{
                iconEl.removeClass("fa-sort-desc").addClass("fa-sort-asc");
                order="asc";
            }
        }

        columnOrder = {[columnName] : order};
        loaderShow();
        init = false;

        table_footer.paginate();
        getTableService(1, limit, searchValue, searchCol, recalcFooter, columnOrder);
    });
    
    window.addEventListener("beforeunload", function (event) {
        jQuery('.progress-bar').show();
    });

    $(".singledate").daterangepicker({
        singleDatePicker: true,
        // minDate: "2018-10-22",
        locale: {
            format: 'DD/MM/YYYY',
            'separator': ` ${i18nGlobals[myCountry.getCountry()].to.toLowerCase()} `,
            'applyLabel': i18nGlobals[myCountry.getCountry()].apply,
            'cancelLabel': i18nGlobals[myCountry.getCountry()].cancel,
            'fromLabel': i18nGlobals[myCountry.getCountry()].from,
            'toLabel': i18nGlobals[myCountry.getCountry()].to,
            'weekLabel': i18nGlobals[myCountry.getCountry()].calendar.weekLabel,
            'daysOfWeek':  i18nGlobals[myCountry.getCountry()].calendar.daysOfWeek,
            'monthNames': i18nGlobals[myCountry.getCountry()].calendar.monthNames,
            "firstDay": 1

        }
    });


    jQuery("#info_shoppingforecast").daterangepicker({
        "minDate": moment(this.today).subtract(1, 'd'),
        "startDate": moment(this.today).subtract(1, 'd'),
        "endDate": moment(this.today).add(3,'M'),
        "opens": "left",
        "drops": "up",
        "locale": datepickerLocaleOptions()
    });

    jQuery("#info_history").daterangepicker({

        "maxDate": moment(this.today).subtract(1, 'd'),
        "startDate": moment(this.today).subtract(3, 'M').subtract(1, 'd'),
        "endDate": moment(this.today).subtract(1, 'd'),
        "opens": "left",
        "drops": "up",
        "locale": datepickerLocaleOptions()
    });
            
    jQuery("#interdep_expiration").daterangepicker({
        singleDatePicker: true,
        "opens": "left",
        "drops": "up",
        "locale": datepickerLocaleOptions()
    });



function datepickerLocaleOptions(){
    return {
        "format": "DD/MM/YYYY",
        'separator': ` ${i18nGlobals[myCountry.getCountry()].to.toLowerCase()} `,
        'applyLabel': i18nGlobals[myCountry.getCountry()].apply,
        'cancelLabel': i18nGlobals[myCountry.getCountry()].cancel,
        'fromLabel': i18nGlobals[myCountry.getCountry()].from,
        'toLabel': i18nGlobals[myCountry.getCountry()].to,
        'weekLabel': i18nGlobals[myCountry.getCountry()].calendar.weekLabel,
        'daysOfWeek':  i18nGlobals[myCountry.getCountry()].calendar.daysOfWeek,
        'monthNames': i18nGlobals[myCountry.getCountry()].calendar.monthNames,
        "firstDay": 1
    }
}

jQuery("#shoppingforecast_month, #history_consumption, #stockpos_year, #stockpos_quarter, #dashboard_tab").unbind("click");
    jQuery('#erp-select').unbind("change");
    /* Não altere a ordem os items abaixo. */
    const classe = new infoSkuController();
    const basicInformationController = new BasicInformationController();
    const calendarController = new CalendarController();
    const erpclass = new erp();
    const childrenItem = new childrenItemController();
    const interdepController = new interdependenceController();
    const policyController = new PolicyController();
    const logClass = new LogController();

    listenInfoSkuBasicInfoBtnSave();
    listenModalClose();

    jQuery('#download_csv_sku_list').on('click', function () {
        window.open(`/${jQuery('#country').val()}/sku-test/csv`)
    });
    
    const pathname = window.location.pathname;
    if( pathname.indexOf('/getsku/') > -1) {
        jQuery("#tableSku > tbody > tr:first-child").trigger('dblclick');
    }
    
}

function listenInfoSkuBasicInfoBtnSave() {
    const saveButton = jQuery('#info_sku_save_click');

    saveButton.on('click', () => {
        haveChanges = true;
    });
}

function listenStatusChange() {
    jQuery('#info_btn_status').unbind('click').on('click', () => {
        haveChanges = true;
    });
}

function listenModalClose() {
    jQuery('#modal_info').unbind('hidden.bs.modal').on('hidden.bs.modal', () => {
        if (haveChanges) {
            loaderShow();

            let limit = jQuery('#items_per_page').val();
            let searchValue = jQuery('#suggest-search-input').val();
            let searchCol = jQuery('#suggest-seach-col').val();
            let page = jQuery('#pagination').twbsPagination('getCurrentPage');

            haveChanges = false;

            getTableService(page, limit, searchValue, searchCol, recalcFooter=false, columnOrder);
        }
    });
}

/******INITIALIZE FIM******/
window.addEventListener("beforeunload", function (event) {
    jQuery('.progress-bar').show();
});

//evento do combo que altera a quantidade de itens por página que o usuário deseja visualizar.
jQuery("#items_per_page").on("change", function(){
    loaderShow();

    let limit = jQuery(this).val();
    let searchValue = jQuery('#suggest-search-input').val();
    let searchCol = jQuery('#suggest-seach-col').val();

    init = false;

    table_footer.data_footer = {
        'page': 1,
        'limit': limit,
        'searchValue': searchValue,
        'searchCol': searchCol
    };
    table_footer.post();

    getTableService(1, limit, searchValue, searchCol, recalcFooter=true, columnOrder);
});

//função chamada pela paginação. Retorna um dataset de acordo com os parâmetros recebidos: page e limit
function getTableService(page, limit, input, searchCol, recalcFooter, order=null) {
    global_scroll.left = jQuery("#controlSize").scrollLeft();
    global_scroll.top = jQuery("#controlSize").scrollTop();

    jQuery.ajax({
        url: `/${myCountry.getCountry()}/sku-test/table`,
        data: {
            page: page,
            limit: limit,
            searchCol: searchCol,
            searchValue: input,
            recalcFooter: recalcFooter,
            order: order
        },
        type: 'POST',
        datatype: 'text',
        success: function (resposta) {
            if (! checkResultSearch(resposta.trim(), 'list')) return false;

            // jQuery('#table_insert').html(resposta);
            $("#tableSku thead, #tableSku tbody").remove();
            jQuery('#tableSku').append(resposta);
            loaderHide();
            jQuery(".pln-items-search").show();
            $('#tableSku tfoot').attr({'class' : ''});
            jQuery("#controlSize")
                .scrollLeft(global_scroll.left)
                .scrollTop(global_scroll.top);
        },
        error: function (error) {
            loaderHide();
            execSwal("", i18nGlobals[myCountry.getCountry()].unavailableService, "error");
        }
    });
}

/*recebe o resultado da consulta ao banco de dados, se não houver itens,
exibe um alerta e retorna para a página de filtros*/
function checkResultSearch(val, origin) {
    if (val == 'false') {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn pln-btn-orange',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            animation: false,
            text: i18nGlobals[myCountry.getCountry()].table.noItemsToDisplay,
            type: "warning",
            showCancelButton: false,
            confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButton,
            allowOutsideClick: () => false,
            preConfirm: () => {
                if (origin == 'filters') {
                    window.location.href = `/${myCountry.getCountry()}/sku-test/filters`;
                } else {
                    loaderHide();
                }
            }
        });

        return false;
    }

    return true;
}


//realiza a paginação e invoca getTableService() para popular a tabela com
//os dados refentes à pagina e a quantidade de itens escolhida
function paginate(startPg) {    
    const objPagination = jQuery('#pagination');
    objPagination.twbsPagination({
        totalPages: jQuery("#total_page_number").val(),
        startPage: startPg == undefined ? 1 : startPg*1,
        first: ' <i class="fas fa-angle-double-left"></i>',
        last: '<i class="fas fa-angle-double-right"></i>',
        prev: ' <i class="fas fa-angle-left"></i>',
        next: '<i class="fas fa-angle-right"></i>',
        visiblePages: 3,
        onPageClick: function (event, page) {
            let limit = jQuery("#items_per_page").val();
            let searchValue = jQuery('#suggest-search-input').val();
            let searchCol = jQuery('#suggest-seach-col').val();

            if (init) {
                loaderShow();
                getTableService(page, limit, searchValue, searchCol, recalcFooter = false, columnOrder);
            } else {
                init = true;
            }

        }
    })
} 

/* 
 * Pega as informações de total e paginação para o footer
 * Desenvolvido para solucionar problemas de lentidão
*/
function tableFooterSku(table, url) {
    var self = this;

    this.pagination = "#pagination";
    this.start_pg = 1;
    this.page_loaded = false;
    this.url_footer = url;
    this.data_footer = {
        page: 1,
        limit: $('#items_per_page').val(),
    };
    this.response_footer = {
        pages: 0,
        count: "0",
        data: {}
    };
    this.search = {
        input: '[id*="_search_input"]',
        col: '[id*="-seach-col"]'
    };

    this.post = function () {
        $.post( self.url_footer, self.data_footer, function( response ) {
            self.response_footer = response;
            self.paginate();
            self.totals();
        });
    };

    this.totals = function() {
        $.each(self.response_footer.data, function( index, value ) {
            if (value == null || value == 'null' || value == 'NULL') {
                value = '';
            }

            if (index != "SIT_ANALYSED" && index != "SIT_SAVED") {
                $(`${table} tfoot tr`).append(`<th>${value}</th>`);
            }
        });
    };

    this.paginateLabels = function(page) {
        let items_per_page = parseInt(jQuery('#items_per_page').val());
        let current_page = page;
        let items_to = items_per_page * current_page;
        items_to = items_to > self.response_footer.count ? self.response_footer.count : items_to;
        let items_from = (items_to - items_per_page) < 0 ? 1 : (items_to - items_per_page) + 1;
        
        $("#spn_total_page_number").html(self.response_footer.pages);
        $("#page_items_total").html(self.response_footer.count);
        $("#page_items_from").html( items_from );
        $("#page_items_to").html( items_to );
        $("#total_items").html( $("#page_from_to").html() );
    };

    this.paginate = function() {
        if (self.page_loaded) {
            $(`${self.pagination}`).twbsPagination('destroy');
        }
        self.page_loaded = true;

        $(`${self.pagination}`).twbsPagination({
            totalPages: self.response_footer.pages,
            startPage: self.start_pg*1,
            first: ' <i class="fas fa-angle-double-left"></i>',
            last: '<i class="fas fa-angle-double-right"></i>',
            prev: ' <i class="fas fa-angle-left"></i>',
            next: '<i class="fas fa-angle-right"></i>',
            visiblePages: 3,
            onPageClick: function (event, page) {
                let limit = $("#items_per_page").val(),
                    searchValue = $(`${self.search.input}`).val(),
                    searchCol = $(`${self.search.col}`).val();

                self.paginateLabels(page);

                if(init) {
                    loaderShow();
                    getTableService(page, limit, searchValue, searchCol, recalcFooter = false, columnOrder);
                } else {
                    init = true;
                }
            }
        });
    };
}

var table_footer = new tableFooterSku('#tableSku', `/${myCountry.getCountry()}/sku-test/table/footer`);
table_footer.post();

//executa somente na primeira vez que a página é carregada
function getTableServiceIni() {
    loaderShow();
    jQuery.ajax({
        url: `/${myCountry.getCountry()}/sku-test/table`,
        data: {
            page: 1,
            limit: 20
        },
        type: 'POST',
        datatype: 'text',
        success: function (resposta) {

            if(!checkResultSearch(resposta.trim(), 'filters')) return false;
            jQuery('#tableSku').append(resposta);
            loaderHide();
            jQuery(".pln-items-search").show();
            $('#tableSku tfoot').attr({'class' : ''});
            getFormOnLoad();
        },
        error: function (error) {
            loaderHide();
            execSwal("", i18nGlobals[myCountry.getCountry()].unavailableService, "error");
        }
    });
}
getTableServiceIni();

let serializeOnPageLoad = {};
function getFormOnLoad(){
    serializeOnPageLoad = jQuery("#formulario").serializeJSON();
    serializeOnPageLoad = JSON.parse(serializeOnPageLoad);
}

/**
 * Buscar sugestão
 */
function suggestSearch() {
    loaderShow();

    let searchValue = jQuery('#suggest-search-input').val();
    let searchCol = jQuery('#suggest-seach-col').val();
    let limit = jQuery("#items_per_page").val();

    init = false;

    table_footer.data_footer = {
        'page': 1,
        'limit': limit,
        'searchValue': searchValue,
        'searchCol': searchCol
    };
    table_footer.post();
    getTableService(1, limit, searchValue, searchCol, recalcFooter=true);
}

//ir para página X (Buscar)
function goToPageNumber(){
    if( parseInt(jQuery("#goto_pagenumber").val()) == jQuery('#pagination').twbsPagination("getCurrentPage") ){
        return false;
    } else if ( parseInt(jQuery("#goto_pagenumber").val()) > parseInt(jQuery("#total_page_number").val()) ){
        execSwal("",`${i18nGlobals[myCountry.getCountry()].table.insertPage} ${jQuery("#total_page_number").val()}`, "warning");
        return false;
    } else if ( jQuery("#goto_pagenumber").val() == "" || parseInt(jQuery("#goto_pagenumber").val()) =="0" ){
        execSwal("", i18nGlobals[myCountry.getCountry()].table.insertValidPage, "warning");
        return false;
    }

    loaderShow();
    const searchValue = jQuery('#suggest-search-input').val(),
        searchCol = jQuery('#suggest-seach-col').val(),
        limit = jQuery("#items_per_page").val(),
        pageNumber = jQuery("#goto_pagenumber").val();

    init = false;

    table_footer.data_footer = {
        page: pageNumber
    };
    table_footer.paginate();
    getTableService(pageNumber, limit, searchValue, searchCol, recalcFooter=true, columnOrder);
}

function paginateLegends(){
    const totalPageNumber = jQuery("#total_page_number").val(),
        itemsPerPage = parseInt(jQuery('#items_per_page').val()),        
        currentPage =jQuery("#pagination").twbsPagination("getCurrentPage"),
        pageItemsTo = itemsPerPage * currentPage,
        pageItemsFrom = (pageItemsTo - itemsPerPage) + 1,
        totalItems = parseInt(jQuery("#hdn_total_items").val());

    jQuery("#spn_total_page_number").html( totalPageNumber );
    jQuery("#page_items_from").html( pageItemsFrom );
    jQuery("#page_items_to").html( pageItemsTo > totalItems ? totalItems : pageItemsTo );
    jQuery("#page_items_total").html( totalItems );

    jQuery("#total_items").html( jQuery("#page_from_to").html() );
}

function execSwal(header, text, alertType){
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn pln-btn-orange',
        buttonsStyling: false,
      });
      swalWithBootstrapButtons({
        animation: false,
        text: text,
        type: alertType,
        showCancelButton: false,
        confirmButtonText: i18nGlobals[myCountry.getCountry()].modalRemoveItem.confirmButton
      });
}

//abre modal de estabelecimentos com próximas ordens
jQuery("#modal_order").on("show.bs.modal", function(event){
    const id = jQuery(event.relatedTarget).attr("data-id");

    if(orderAlreadyLoaded != id){
        jQuery("#include_order").html("");
        jQuery("#order_loading").show();
        jQuery.get(`/${myCountry.getCountry()}/modals/orders/${id}`, function(data){
            jQuery("#order_loading").hide();
            jQuery("#include_order").html(data);
        }).done(function( data ) {
            orderAlreadyLoaded = id;
        });
    }
});

//abre modal de estabelecimentos com itens em excesso
jQuery("#modal_excess").on("show.bs.modal", function(event){
    const triggerElement = jQuery(event.relatedTarget),
        id = triggerElement.attr("data-id");
    if(aux_excessAlreadyLoaded != id){
        jQuery("#include_excess").html("");
        jQuery("#excess_loading").show();
        jQuery.get(`/${myCountry.getCountry()}/modals/excess/sku/${id}`, function(data){
            jQuery("#excess_loading").hide();
            jQuery("#include_excess").html(data);
        }).done(function( data ) {
            aux_excessAlreadyLoaded = id;
        });
    }
});