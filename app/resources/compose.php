<?php 

if (isset($ViewData['ajax'])) {
    unset($ViewData['ajax']);    
    require('view.php');
} elseif (isset($ViewData['json'])) {
    unset($ViewData['json']);
    header("Content-type: application/json");
    echo json_encode($ViewData);
} elseif ($ViewData['csv']) {
    require('csv.php');
} else {
    require('head.php');
    require('view.php');
    require('footer.php');
}