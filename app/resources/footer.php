<!--
  - Carregamento de Javascrips Globais -
 obs: Passe o atributo 'async' para não blockar no render do navegador os js.
 -->

</body>
<?php
    printf('<script src="/resources/js/uglify/all-commons-min.js?v=%s"></script>', VERSION);
?>
<!--
<script src='/resources/js/loader.js'></script>
<script src='/resources/js/jquery/jquery.min.js'></script>
<script src='/resources/js/popper.js/popper.min.js'></script>
<script src='/resources/js/sweetalert2.js'></script>
<script src='/resources/js/moment/moment.js'></script>
<script src='/resources/js/dragdrop/jqueryUI.js'></script>
<script src='/resources/js/dragdrop/cookie.js'></script>
<script src='/resources/js/dragdrop/jsfunctions.js'></script>
<script src='/resources/js/bootstrap-daterangepicker/daterangepicker.js'></script>
<script src='/resources/js/bootstrap/util.js'></script>
<script src='/resources/js/bootstrap/alert.js'></script>
<script src='/resources/js/bootstrap/collapse.js'></script>
<script src='/resources/js/bootstrap/modal.js'></script>
<script src='/resources/js/bootstrap/tab.js'></script>
<script src='/resources/js/bootstrap/tooltip.js'></script>
<script src='/resources/js/bootstrap/popover.js'></script>
<script src='/resources/js/menu.js'></script>

<script src="/resources/js/moment.locales.js"></script>
<script src="/resources/js/jquery.multisortable.js"></script>
<script src="/resources/js/session.js"></script> -->
<!--
  - Autoload dos arquivos Javascript passados na View.
 -->
<?php
if (isset($JS)) {
    foreach ($JS as $javascript) {
        printf("<script src='/resources/js/%s.js?v=%s'></script>", $javascript, VERSION);
    }
}
?>
</html>