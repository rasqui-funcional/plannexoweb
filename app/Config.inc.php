<?php

/**
 * Load Kernel
 */
if ($handle = opendir('model/Kernel')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            require_once('model/Kernel/' . $entry);
        }
    }

    closedir($handle);
}

/**
 * Set up classes autoload
 */
function autoload($Class)
{
    $cDir = [
        'controllers',
        'model/Database',
        'model/Helpers',
        'model/Http',
        'model/Classes',
        'model/Redis',
        'model/Graphics',
        'services',
        'services/Graphics',
        'services/InfoSKU',
        'services/Approvals',
        'services/OrderCover',
        'services/PurchaseOrder',
        'services/Parameters/Calendar',
        'services/Audit',
        'services/Parameters/Politics',
        'services/Resume'
    ];

    $iDir = null;
    foreach ($cDir as $dirName) {
        $filename = __DIR__ . "//{$dirName}//{$Class}.class.php";
        if (!$iDir && file_exists($filename) && !is_dir($filename)) {
            include_once($filename);
            $iDir = true;
        }
    }

    if (!$iDir) {
        WSErro("Não foi possível incluir {$Class}.class.php", WS_ERROR);
        exit;
    }
}

spl_autoload_register('autoload');

/**
 * Set the enviroment
 */
new SetEnviroment();

/**
 * Set the parameters
 */
new SetParameter();

/**
 * Set locale and timezone
 */
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ERROR);
ini_set('xdebug.var_display_max_depth', 5);
ini_set('xdebug.var_display_max_children', 256);
ini_set('xdebug.var_display_max_data', 1024);

/**
 * Error handling
 */
define('WS_ACCEPT', 'accept');
define('WS_INFOR', 'infor');
define('WS_ALERT', 'alert');
define('WS_ERROR', 'error');
$version = time();
if(getenv('ENV') == 'PROD'){
    $version ='1.6.11';
}
define('VERSION', $version);

//WSErro :: Exibe erros lançados :: Front
function WSErro($ErrMsg, $ErrNo = null, $ErrDie = null)
{
    $CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));
    echo "<p class=\"trigger {$CssClass}\">{$ErrMsg}<span class=\"ajax_close\"></span></p>";

    if ($ErrDie) {
        exit;
    }
}

//PHPErro :: personaliza o gatilho do PHP
function PHPErro($ErrNo, $ErrMsg, $ErrFile, $ErrLine)
{
    $CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR
        : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));

    printf(
        '<p class="trigger %s">
                        <b>Erro na linha: %s ::</b> %s<br>
                        <small>%s</small>
                        <span class="ajax_close"></span>
                   </p>',
        $CssClass,
        $ErrLine,
        $ErrMsg,
        $ErrFile
    );

    if ($ErrNo == E_USER_ERROR) {
        exit;
    }
}
