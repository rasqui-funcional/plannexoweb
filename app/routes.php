<?php

/**
 * <b>Routes</b>
 * Rotas e suas respectivas Controllers com a função a ser executada;
 *
 * Para criar uma nova rota passe a rota como key(index) e o Controller e a função.
 * Desta forma: 'caminho/rota' => 'controllerName@funcao'
 *
 * Se essa rota precisa receber parâmetros informe junto a rota entre chaves.
 * Desta forma: 'caminho/rota/{parametro_1}/{parametro_2}' => 'controllerName@funcao'
 *
 * OBSERVAÇÕES:
 * - Não inicie o caminho da rota com "/";
 * - É obrigatorio informar um Controller;
 * - É obrigatorio passar a função depois do Controller com o @;
 * - A primeira rota é exclusiva para funções em Ajax.
 */
$Routes = [];
/**
 * Analise de perfomace
 */
$Routes += [
    'sku-test/filters' => 'TestSkuController@getFormData',
    'sku-test/list' => 'TestSkuController@getList',
    'sku-test/list/{action}/' => 'TestSkuController@getDynamicList',
    'sku-test/table' => 'TestSkuController@getTable',
    'sku-test/table/footer' => 'TestSkuController@getTableFooter',
    'suggest-test/filters' => 'TestSuggestController@getFormData',
    'suggest-test/list' => 'TestSuggestController@getList',
    'suggest-test/list/{indicators}' => 'TestSuggestController@getCustomSuggest',
    'suggest-test/table' => 'TestSuggestController@getTable',
    'suggest-test/table/footer' => 'TestSuggestController@getTableFooter',
];

/**
 * Core Routes
 */

//Geral
$Routes += [
    'test' => 'TestController@methodTest',
    'test/{param}' => 'TestController@getParam',
    'test/audit/{method}' => 'TestController@getMethod',
    'test/indicators/{indicator}' => 'TestController@getIndicators',
    'login' => 'LoginController@execLogin',
    'formlogin' => 'LoginController@loginError',
    'getlogin' => 'LoginController@getlogin',
    'logout' => 'LoginController@execLogout',
    'expire' => 'LoginController@expireSession',
    'estabs' => 'EstabsController@getEstabs',
    'home' => 'HomeController@getHome',
    'checksession' => 'LoginController@checkSession',
    'setsession/{session_id}' => 'LoginController@setSessionId',
    'health' => 'LoginController@health',
    'jobnight/{companyId}' => 'LoginController@jobnight',
    'error/{number}' => 'ErrorController@getMessage',
    'ajax/filter/{function}' => 'FiltersController@exec',
];
if (cannot('spdmBuyer')) {

}
/**
 * CSV Routes
 */
$Routes += [
    'csv/approvals/{collumn}/{search}' => 'CSVController@getApprovalsCSV',
    'csv/{table}/{skuItem}/{codEstabPk}/{orderCol}/{orderType}/{search}' => 'CSVController@getCsvByTable',
    'csv/approvalssuggest/{id}/{column}/{value}' => 'CSVController@getApprovalsSuggestCSV',
    'csv/suggestions/{search}' => 'CSVController@getSuggestionsCSV',
    'csv/purchaseorder' => 'CSVController@getPurchaseOrderCSV',
    'csv/purchaseorder/orders' => 'CSVController@getPurchaseOrderCSV',
    'csv/skulog/{skuid}' => 'CSVController@getSkuLogCSV'
];


if (cannot('spdmBuyer')) {
    //Relatórios
    include_once 'routes/reports.php';

}
//Planejamento
include_once 'routes/planning.php';
//Parametros
if (cannot('spdmBuyer') && cannot('spdmPharmaceutical') && cannot('spdmPlanner')) {
    include_once 'routes/parameters.php';
}


