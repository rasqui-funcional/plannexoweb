<?php

return [
    'roles' => [
        'allow' => [
            'buyer' => '*',
            'sysadmin' => '*'
        ],
        'denny' => [
            'supplier' => [
                'health',
                'csv',
                'purchaseorder',
                'resume',
                'parameters',
                'test'
            ]
        ]
    ]
];
