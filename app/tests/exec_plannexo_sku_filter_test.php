<?php

require_once('vendor/autoload.php');
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;

require_once ('tests/plannexoWebPages/skuFiltersPage.php');

class PlannexoSkuFilterTest extends PHPUnit_Framework_TestCase {
    /**
     * @var \RemoteWebDriver
     */
    protected $webDriver;
    
	public function setUp()
    {
        $capabilities = array("platform"=>"LINUX", "browserName" => "chrome");
        $this->webDriver = RemoteWebDriver::create('http://172.17.0.1:4444/wd/hub', $capabilities);
    }

    protected function tearDown()
        {
            $this->webDriver->close();
            $this->webDriver->quit();
        }
    }
    ?>
