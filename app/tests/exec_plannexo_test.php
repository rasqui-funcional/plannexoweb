<?php

require_once('vendor/autoload.php');
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;

require_once ('tests/plannexoWebPages/dashboardPage.php');
require_once ('tests/plannexoWebPages/suggBuyersFiltersPage.php');

class PlannexoSuggFilterTest extends PHPUnit_Framework_TestCase {
    /**
     * @var \RemoteWebDriver
     */
    protected $webDriver;
    #protected $basePage;
    #protected $dashboard;
	public function setUp()
    {
        $capabilities = array("platform"=>"LINUX", "browserName" => "chrome");
        $this->webDriver = RemoteWebDriver::create('http://172.17.0.1:4444/wd/hub', $capabilities);
    }

    protected function tearDown()
        {
            $this->webDriver->close();
            $this->webDriver->quit();
        }
        
        public function testSuggBuyersSearchAllItems()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSearch();
        }
        public function testSuggBuyersSearchAByCodeEstab()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectItemsInEstab();
            $filters->doSearch();
        }
        
        public function testSuggBuyersSearchByStatusSku()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectStatusSkuByErp();
            $filters->doSelectStatusSkuBySystem();
            $filters->doSelectStatusSkuByAnalyze();
            $filters->doSearch();
        }

        public function testSuggBuyersSearchByCurve()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectCurveABCSku();
            $filters->doSelectCurveXYZSku();
            $filters->doSelectCurvePQRSku();
            $filters->doSelectCurve123Sku();
            $filters->doSearch();
        }

        public function testSuggBuyersSearchBySkuItem()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSearchSkuByCodItemOrDescItems();
            $filters->doSearch();
        }

        public function testSuggBuyersSearchByExbitionSku()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectExhibitionItems();
            $filters->doSearch();
        }
        
        public function testSuggBuyersSearchBySpecialFilters()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectSpecialFiltersByCoverWithDays();
            $filters->doSelectSpecialFiltersByCoverWithMinimumDays();
            $filters->doSelectSpecialFiltersByPurchaseOrder();
            $filters->doSelectSpecialFiltersBySkuUrgent();
            $filters->doSelectSpecialFiltersByPurchaseRequistion();
            $filters->doSelectSpecialFiltesByAnalizeDelayed();
            $filters->doSearch();
        }

        public function testSuggBuyersSearchByParametersSku()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectParametersSignalized();
            $filters->doSelectParametersLifeCycle();
            $filters->doSelectParametersPolitics();
            $filters->doSearch();
        }

        public function testSuggBuyersSearchBySystemCategory()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectCategorySystemCheckByDiv();
            $filters->doSelectCategorySystemCheckByMat();
            $filters->doSelectCategorySystemByMed();
            $filters->doSearch();
        }

        public function testSuggBuyersSearchByGroup()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectGroupItem();
            $filters->doSearch();
        }

        public function testSuggBuyersSearchBySubGroup()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectSubGroup();
            $filters->doSearch();

        }
        public function testSuggBuyersSearchByManagersProfile()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectManagerProfile();
            $filters->doSearch();

        }

        public function testSuggBuyersSearchByClasses()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectClasses();
            $filters->doSearch();

        }

        public function testSuggBuyersSearchByContracts()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectContract();
            $filters->doSearch();

        }

        public function testSuggBuyersSearchByStandardItems()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectStandardItems();
            $filters->doSearch();

        }
        public function testSuggBuyersSearchByHighCostItems()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectHighCost();
            $filters->doSearch();

        }

        public function testSuggBuyersSearchByTestItems()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectTestItems();
            $filters->doSearch();

        }

        public function testSuggBuyersSearchDate()
        {
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doSelectDateInterval();
            $filters->doSearch();

        }
        public function testSuggBuyersSearchCreateNewFilters()
        {
            $filterName = "plannexo 5";
            $filters = new SuggBuyersFiltersPage($this->webDriver);
            $filters->open();
            $filters->doCreateNewFilter($filterName);
            $filters->doSearch();
        }
        
}
?>
