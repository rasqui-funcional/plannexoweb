<?php
/**
 * <b>TestValidation.class:</b>
 * Classe responsável por testar a Classe <b>Validation.class</b>.
 *
 */

use PHPUnit\Framework\TestCase;

require_once('Validation.class.php');

class TestValidation extends TestCase
{

    public function testFilterWithArrayAndValidField(): void
    {
        $data = array(
            'Volvo' => 'caminhao',
            'BMW' => 'carro de passeio',
            'Saab' => 'esportivo',
            'Land Rover' => 'utilitario de luxo'
        );

        $validation = new Validation;
        $this->assertEquals($validation->filter($data), $data);
    }

    public function testFilterWithArrayAndInvalidField(): void
    {
        $invalid_data = array(
            'Volvo' => '<b>caminhao</b>',
            'BMW' => 'carro de passeio',
            'Saab' => 'SELECT esportivo',
            'Land Rover' => 'utilitario de luxo'
        );

        $valid_data = array(
            'Volvo' => 'caminhao',
            'BMW' => 'carro de passeio',
            'Saab' => 'esportivo',
            'Land Rover' => 'utilitario de luxo'
        );

        $validation = new Validation;
        $this->assertEquals($validation->filter($invalid_data), $valid_data);
    }

    public function testFilterWithInvalidData(): void
    {
        $data = '<h1>Hello World!</h1>';
        $validation = new Validation;
        $this->assertEquals($validation->filter($data), 'Hello World!');
    }

    public function testFilterWithValidData(): void
    {
        $data = 'Hello World!';
        $validation = new Validation;
        $this->assertEquals($validation->filter($data), 'Hello World!');
    }


}