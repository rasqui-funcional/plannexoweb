<?php

class SkuFiltersLocators {
    //locator estabelecimento
    const COD_ESTAB = "cod_estab_fk_1";

    //locator button pesquisar
    const BTN_SEARCH = 'btn_pesquisa';

    //locator link limpar filtros
    const LINK_CLEAN_FILTERS = "//span[@onclick=\"cleanForm('formulario')\"]";

    //locators situacao sku
    const SIT_SKU_ERP_ACTIVE= "sit_sku_erp_1";
    const SIT_SKU_SYSTEM_ACTIVE= "sit_sku_1";
    const SIT_SKU_ANALYZED_ACTIVE = "sit_saved_1";
    const SIT_SKU_ERP_INACTIVE= "sit_sku_erp_0";
    const SIT_SKU_SYSTEM_INACTIVE= "sit_sku_0";
    const SIT_SKU_ANALYZED_INACTIVE= "sit_saved_0";

    //locators curvas ABC
    const CURVE_A = "curva_abc_A";
    const CURVE_B = "curva_abc_B";
    const CURVE_C = "curva_abc_C";

    //locators curvas PQR
    const CURVE_P = "curva_pqr_P";
    const CURVE_Q = "curva_pqr_Q";
    const CURVE_R = "curva_pqr_R";

    //locators curvas XYZ
    const CURVE_X = "curva_xyz_X";
    const CURVE_Y = "curva_xyz_Y";
    const CURVE_Z = "curva_xyz_Z";

    //locators curvas 123
    const CURVE_1 = "curva_123_1";
    const CURVE_2 = "curva_123_2";
    const CURVE_3 = "curva_123_3";

    //locators 
    const id_sku_pk = "id_sku_pk";
    const cod_item_pk = "cod_item_pk";
    const DESC_ITEM = "desc_item";
    const NETWORK = "network";

    const chkvetor_cod_profile = "chkvetor_cod_profile";
    const chkvetor_ID_CALENDAR_FK = "chkvetor_ID_CALENDAR_FK";
    const chkvetor_inv_available = "chkvetor_inv_available";
    const chkvetor_qty_inv_origin = "chkvetor_qty_inv_origin";

    //locators exibição
    const EXHIBITION_ID_NEC = "//div[@id='div_exibition']/div[15]/i";
    const EXHIBITION_PARCELA = "//div[@id='div_exibition']/div[16]/i";
    const EXHIBITION_COD_ITEM = "//div[@id='div_exibition']/div[17]/i";
    const EXHIBITION_COD_ITEM_PK = "//div[@id='div_exibition']/div[18]/i";
    const chk_check_all_exibicao = "chk_check_all_exibicao";
    
    const sendAllToOrder = "sendAllToOrder";
    const cleanOrder = "cleanOrder";
    const qty_excess_next_in = "qty_excess_next_in";
    const qty_inv_origin = "qty_inv_origin";
    const purchase_req = "purchase_req";
    const purchase_order = "purchase_order";
    const last_purchase_supplier_autocomp = "last_purchase_supplier_autocomp";
    const inv_days_min = "inv_days_min";
    const maior_que_zero = "//form[@id='formulario']/div[3]/div/div/fieldset/div[3]/div/div/select";
    const inv_days = "inv_days";
    const f_out_last_30d = "f_out_last_30d";
    const date_last_in = "date_last_in";
    const date_next_in = "date_next_in";
    const inv_level_1 = "inv_level_1";
    const inv_level_2 = "inv_level_2";
    const inv_level_3 = "inv_level_3";
    const inv_level_4 = "inv_level_4";
    const inv_level_5 = "inv_level_5";
    const inv_level_6 = "inv_level_6";
    const sku_life_cycle_1 = "sku_life_cycle_1";
    const sku_life_cycle_2 = "sku_life_cycle_2";
    const sku_life_cycle_3 = "sku_life_cycle_3";
    const id_profile_8 = "id_profile_8";
    const id_profile_3 = "id_profile_3";
    const id_profile_6 = "id_profile_6";
    const cod_group1_fk_MED = "cod_group1_fk_MED";
    const chk_check_all_cat_sistema = "chk_check_all_cat_sistema";
    const cod_group2_fk_008 = "cod_group2_fk_008";
    const cod_group2_fk_003 = "cod_group2_fk_003";
    const chk_check_all_grupo = "chk_check_all_grupo";
    const cod_group3_fk_10_78 = "cod_group3_fk_10-78";
    const cod_group3_fk_115 = "cod_group3_fk_115";
    const chk_check_all_subgrupo = "chk_check_all_subgrupo";
    const user_id_1 = "user_id_1";
    const cod_group4_fk_1_15_39 = "cod_group4_fk_1-15-39";
    const chk_check_all_classe = "chk_check_all_classe";
    const cod_group5_fk_Nao = "cod_group5_fk_Nao";
    const cod_group5_fk_Sim = "cod_group5_fk_Sim";
    const chk_check_all_padronizado = "chk_check_all_padronizado";
    const cod_group6_fk_B_BRAUN = "cod_group6_fk_B.BRAUN";
    const cod_group6_fk_BIOMEDICAL = "cod_group6_fk_BIOMEDICAL";
    const chk_check_all_contrato = "chk_check_all_contrato";
    const cod_group7_fk_A = "cod_group7_fk_A";
}
?>