<?php
use Facebook\WebDriver\WebDriverBy;

class SuggBuyersFiltersLocators {

    //locator estabelecimento
    const COD_ESTAB = "cod_estab_fk_1";

    //locator button pesquisar
    const BTN_SEARCH = 'btn_pesquisa';

    //locator link limpar filtros
    const LINK_CLEAN_FILTERS = "//span[@onclick=\"cleanForm('formulario')\"]";

    //locators situacao sku
    const SIT_SKU_ERP_ACTIVE= "sit_sku_erp_1";
    const SIT_SKU_SYSTEM_ACTIVE= "sit_sku_1";
    const SIT_SKU_ANALYZED_ACTIVE = "sit_saved_1";
    const SIT_SKU_ERP_INACTIVE= "sit_sku_erp_0";
    const SIT_SKU_SYSTEM_INACTIVE= "sit_sku_0";
    const SIT_SKU_ANALYZED_INACTIVE= "sit_saved_0";

    //locators curvas ABC
    const CURVE_A = "curva_abc_A";
    const CURVE_B = "curva_abc_B";
    const CURVE_C = "curva_abc_C";

    //locators curvas PQR
    const CURVE_P = "curva_pqr_P";
    const CURVE_Q = "curva_pqr_Q";
    const CURVE_R = "curva_pqr_R";

    //locators curvas XYZ
    const CURVE_X = "curva_xyz_X";
    const CURVE_Y = "curva_xyz_Y";
    const CURVE_Z = "curva_xyz_Z";

    //locators curvas 123
    const CURVE_1 = "curva_123_1";
    const CURVE_2 = "curva_123_2";
    const CURVE_3 = "curva_123_3";

    //locators 
    const ID_PSUG_PK = "chkvetor_id_psug_pk";
    const NUM_PARC_PK = "chkvetor_num_parc_pk";
    const COD_ITEM_PK = "cod_item_pk";
    const DESC_ITEM = "desc_item";

    //locators exibição
    const EXHIBITION_ID_NEC = "(.//*[normalize-space(text()) and normalize-space(.)='ID Nec.'])[1]/following::i[1]";
    const EXHIBITION_PARCELA = "(.//*[normalize-space(text()) and normalize-space(.)='Parcela'])[1]/following::i[1]";
    const EXHIBITION_COD_ITEM = "(.//*[normalize-space(text()) and normalize-space(.)='Cód. Item'])[2]/following::i[1]";
    const EXHIBITION_COD_ITEM_PK = "chkvetor_cod_item_pk";

    const SPECIAL_FILTERS_MINIMUM_DAYS = "inv_days_min_1";
    const SPECIAL_FILTERS_COVER_DAYS = "inv_days";
    const SPECIAL_FILTERS_PURCHASE_ORDER_ACTIVE =  "purchase_order_1";
    const SPECIAL_FILTERS_SKU_SITUATION_URGENT_ACTIVE = "sit_urgent_1";
    const SPECIAL_FILTERS_SKU_SITUATION_URGENT_INACTIVE = "sit_urgent_0";
    
    const SPECIAL_FILTERS_PURCHASE_ORDER_INACTIVE = "purchase_order_0";
    const SPECIAL_FILTERS_PURCHASE_REQUISTION_ACTIVE = "purchase_req_1";
    const SPECIAL_FILTERS_PURCHASE_REQUISTION_INACTIVE = "purchase_req_0";
    const SPECIAL_FILTERS_ANALIZE_DELAYED = "risk_supplier_1";

    const PARAMETER_SIGNAL_ZERO = "inv_level_1";
    const PARAMETER_SIGNAL_TARGET = "inv_level_4";
    const PARAMETER_SIGNAL_MINIMUM_2 = "inv_level_2";

    const PARAMETER_SIGNAL_MAXIMUM = "inv_level_5";
    const PARAMETER_SIGNAL_MAXIMUM_2 = "inv_level_6";
    const PARAMETER_SIGNAL_MINIMUM = "inv_level_3";
    
    const PARAMETER_POLICY_A = "id_profile_1";
    const PARAMETER_POLICY_B = "id_profile_2";
    const PARAMETER_POLICY_C = "id_profile_3";
    const PARAMETER_POLICY_TOTAL_STOCK = "id_profile_4";
    
    const PARAMETER_SKU_LIFE_CYCLE_NEW = "sku_life_cycle_1";
    const PARAMETER_SKU_LIFE_CYCLE_MATURE = "sku_life_cycle_2";
    const PARAMETER_SKU_LIFE_CYCLE_DESCONTINUED = "sku_life_cycle_3";

    const link_click_btn_pesquisa = "(.//*[normalize-space(text()) and normalize-space(.)='Pesquisar'])[1]/following::span[1]";
    const data_alterada_chegada = "data_alterada_chegada";
    const prox_parcelas = "(.//*[normalize-space(text()) and normalize-space(.)='Próximas parcelas'])[1]/following::input[1]";
    //const politicas = "(.//*[normalize-space(text()) and normalize-space(.)='Política'])[2]/following::div[7]";
    
    const data_range = "daterange";
    const btn_salve_filter = "(.//*[normalize-space(text()) and normalize-space(.)='Salvar'])[1]/following::i[4]";
    const btn_do = "(.//*[normalize-space(text()) and normalize-space(.)='Do'])[2]/following::button[1]";
    
    const click_all_cat_sistema = "chk_check_all_cat_sistema";
    const CATEGORY_SYSTEM_BY_DIV = "cod_group1_fk_DIV";
    const CATEGORY_SYSTEM_BY_MAT = "cod_group1_fk_MAT";
    const CATEGORY_SYSTEM_BY_MED = "cod_group1_fk_MED";
    
    const GROUP_SELECT_ITEM = "cod_group2_fk_0024";

    const gestor_1 = "user_id_1";
    const gestor_2 = "user_id_2";
    const gestor_3 = "user_id_3";
    const gestor_4 = "user_id_4";
    
    const select_classe = "#div_filter_classe > div.d-flex.mt-2.flex-row.text-nowrap.align-items-center";
    const cod_group4_fk_1_133_516 = "cod_group4_fk_1-133-516";
    const cod_group4_fk_1_15_40 = "cod_group4_fk_1-15-40";
    const cod_group4_fk_1_15_39 = "cod_group4_fk_1-15-39";
    
    const cod_group6_fk_Nao = "cod_group6_fk_Não";
    const cod_group6_fk_Sim = "cod_group6_fk_Sim";
    const cod_group6_fk_hashtag = "cod_group6_fk_#";
    
    const HIGH_COST_UP = "cod_group7_fk_A";
    const HIGH_COST_DOWN = "cod_group7_fk_B";
    const HIGH_COST_MEDIAN = "cod_group7_fk_M";
    
    const cod_group5_fk_hashtag = "cod_group5_fk_#";
    const cod_group8_fk_hashtag = "cod_group8_fk_#";
    
    const select_filtro = "(.//*[normalize-space(text()) and normalize-space(.)='Meus filtros:'])[1]/following::select[1]";//select_filtro";
    const btn_filtro = "btn_filtro_text";
    const filter_name = "filter_nome";
    const salve_filter = "spn_save";
    const select_type_filter_private = "filter_perfil_Privado";
    const modal_load = "modal_loading";
    
    const BTN_DELETE_FILTER = "#confirm_delete > span";
    const BTN_CANCEL_FILTER = "button.swal-button.swal-button--cancel";
    const LINK_EDIT_FILTER = "//div[@id='modal_edit']/div/div/div/button/span";
}