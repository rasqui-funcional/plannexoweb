<?php

class DashboardLocators {
    const link_more_details_urgent_items = "Mais detalhes";
    const link_more_detail_next_purchase = "//a[contains(text(),'Mais detalhes')])[2]";
    const link_more_detail_order_delayed = "//a[contains(text(),'Mais detalhes')])[3]";
    const link_more_detail_purchase_approved = "//a[contains(text(),'Mais detalhes')])[4]";
    const click_stock_balance_chart= "balanceamentoEstoqueChart";
    const click_sku_chart = "skuChart";
    const click_position_stock_chart = "posicaoEstoqueChart";
    const link_excess_stock_chart = "excesso";
    const link_stock_financial_stock_chart ="estoqueFinanceiro";
    const link_more_details_sku_chart ="";
    const link_level_service_stock_chart = "levelService";
    const link_trimestral_stock_chart = "pde_trimestral";
    const link_year_stock_chart = "pde_anual"; 
    const info_general_level_service = "div.pln-analise-geral-nivel-servico.d-flex.justify-content-center.flex-column.element-content";
    const info_general_excess = "div.pln-analise-geral-excesso.d-flex.justify-content-center.flex-column.element-content";
    const info_general_total_available = "div.pln-analise-geral-total-disp.d-flex.justify-content-center.flex-column.element-content";
    const click_in_out_chart = "plnDoubleBarLineChart";
    const link_more_filters = "span.pln-icon-color.pln-cur-pointer.float-right";
    const cod_item_pk = "cod_item_pk";
    const id_sku_pk = "id_sku_pk";
    const desc_item = "desc_item";
    const network = "network";
    const cod_group1_fk_MED = "cod_group1_fk_MED";
    const chk_check_all_categorias = "chk_check_all_categorias";
    const checkbox_5 = "//input[@type='checkbox'])[5]";
    const checkbox_6 = "//input[@type='checkbox'])[6]";
    const checkbox_7 = "//input[@type='checkbox'])[7]";
    const checkbox_8 = "//input[@type='checkbox'])[8]";
    const checkbox_9 = "//input[@type='checkbox'])[9]";
    const checkbox_10 = "//input[@type='checkbox'])[10]";

     //locators curvas ABC
    const CURVE_A = "curva_abc_A";
    const CURVE_B = "curva_abc_B";
    const CURVE_C = "curva_abc_C";

    //locators curvas PQR
    const CURVE_P = "curva_pqr_P";
    const CURVE_Q = "curva_pqr_Q";
    const CURVE_R = "curva_pqr_R";

    //locators curvas XYZ
    const CURVE_X = "curva_xyz_X";
    const CURVE_Y = "curva_xyz_Y";
    const CURVE_Z = "curva_xyz_Z";

    //locators curvas 123
    const CURVE_1 = "curva_123_1";
    const CURVE_2 = "curva_123_2";
    const CURVE_3 = "curva_123_3";

    const chk_check_all_estabelecimento = "chk_check_all_estabelecimento";
    const chk_check_all_contrato = "chk_check_all_contrato";
    const chk_check_all_grupo = "chk_check_all_grupo";
    const chk_check_all_classe = "chk_check_all_classe";
    const chk_check_all_subgrupo = "chk_check_all_subgrupo";
    const chk_check_all_politica = "chk_check_all_politica";
    const btn_apply_filter = "//form[@id='formulario']/div/div[2]/div[2]/button[2]";
    const link_clean_filters = "Limpar filtros";

}
?>