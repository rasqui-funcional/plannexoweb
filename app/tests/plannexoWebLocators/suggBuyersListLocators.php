<?php

class SuggBuyersListLocators {
    const suggest_search_col = "suggest-seach-col";
    const suggest_search_input = "suggest-search-input";
    const btn_search = "//button";
    const goto_pagenumber = "goto_pagenumber";
    const link_back = "<< Voltar";
    const link_pagination = "//div[@id='pln-pagination-footer']/div[2]/span[3]/button";
    const click_table_result = "//table[@id='tableSug']/tbody/tr/td";
    const input_qtd = "";
    const input_date_arrived = "";
    const click_button_save_change = "";

}