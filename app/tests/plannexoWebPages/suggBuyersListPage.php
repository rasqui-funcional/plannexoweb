<?php 
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverSelect;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverActions;


require_once ('tests/plannexoWebPages/BasePage.class.php');
require_once ('tests/plannexoWebLocators/suggBuyersListLocators.php');

class SuggBuyersListPage extends BasePageObject{

    public function open()
    {
        $this->webDriver->get(("http://172.17.0.1/br/suggest/filters"));
        $this->webDriver->navigate()->refresh();
    }

    public function doSelectColumnView(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doInsertValueToSearch(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doClickSearchButton(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doNavigatePages(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doGoToSpecificPage(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doSelectResultTable(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doSaveChanges(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doChangeQuantityItems(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doChangeArrivalDate(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doClickColumnsAnalysed(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doClickColumnsApproved(){
        $this->webDriver->findElement(WebDriverBy::id(SuggBuyersListLocators::cod_group4_fk_1_133_516))->click();
    }
}
?>