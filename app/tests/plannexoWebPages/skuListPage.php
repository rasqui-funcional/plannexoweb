<?php
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverSelect;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverActions;


require_once ('tests/plannexoWebPages/BasePage.class.php');
require_once ('tests/plannexoWebLocators/skuListLocators.php');

class SkuListPage extends BasePageObject{

    public function open()
    {
        $this->webDriver->get(("http://172.17.0.1/br/sku/filters"));
        $this->webDriver->navigate()->refresh();
    }

    public function doSelectColumnView(){
        $this->webDriver->findElement(WebDriverBy::id(SkuListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doInsertValueToSearch(){
        $this->webDriver->findElement(WebDriverBy::id(SkuListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doClickSearchButton(){
        $this->webDriver->findElement(WebDriverBy::id(SkuListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doNavigatePages(){
        $this->webDriver->findElement(WebDriverBy::id(SkuListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doGoToSpecificPage(){
        $this->webDriver->findElement(WebDriverBy::id(SkuListLocators::cod_group4_fk_1_133_516))->click();
    }

    public function doSelectResultTable(){
        $this->webDriver->findElement(WebDriverBy::id(SkuListLocators::cod_group4_fk_1_133_516))->click();
    }
}
?>