<?php
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverSelect;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverActions;


require_once ('tests/plannexoWebPages/BasePage.class.php');
require_once ('tests/plannexoWebLocators/dashboardLocators.php');

class DashboardPage extends BasePageObject{

    public function open()
    {
        $this->webDriver->get(("http://172.17.0.1/br/dashboard"));
        $this->webDriver->navigate()->refresh();
    }

    public function checkBalanceStockChart(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::click_stock_balance_chart));
    }

    public function checkSkuChart(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::click_sku_chart));
    }

    public function checkPositionStockChart(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::click_position_stock_chart));
    }

    public function checkInOutChart(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::click_in_out_chart));
    }

    public function doClickMoreDetailsUrgentsItems(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::link_more_details_urgent_items));
    }

    public function doClickMoreDetailsNextPurchase(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::link_more_detail_next_purchase));
    }

    public function doClickMoreDetailsOrderDelayed(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::link_more_detail_order_delayed));
    }

    public function doClickMoreDetailsPurchaseApproved(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::link_more_detail_purchase_approved));
    }

    public function doClickMoreDetailsSkuChart(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::link_more_details_sku_chart));
    }

    public function validateLevelService(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::info_general_level_service));
    }

    public function validateFinancialExcess(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::info_general_excess));
    }
    public function validateAvailable(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::info_general_total_available));
    }

    public function doClickInLinkMoreFilters(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::link_more_filters));
    }

    public function doApplyFilters(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::btn_apply_filter))->click();
    }

    public function doCleanFilters(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::))->click();
    }

    public function doDeleteFilters(){
        $this->webDriver->findElement(WebDriverBy::cssSelector(DashboardLocators::))->click();
    }

    public function doCancelActionFilters(){
        $this->webDriver->findElement(WebDriverBy::cssSelector(DashboardLocators::))->click();
    }

    public function doSelectCurveABCSku(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::CURVE_A))->click();
    }

    public function doSelectCurveXYZSku(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::CURVE_X))->click();
    }

    public function doSelectCurvePQRSku(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::CURVE_P))->click();
    }

    public function doSelectCurve123Sku(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::CURVE_1))->click();
    }

    public function doFilterByCodeItem(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::cod_item_pk))->click();
    }

    public function doFilterByIdSku(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::id_sku_pk))->click();
    }

    public function doFilterByDescItem(){
        $this->webDriver->findElement(WebDriverBy::id(DashboardLocators::desc_item))->click();
    }

    public function doFilterByNetwork(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::network))->click();
    }

    public function doFilterByCategoryMED(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::cod_group1_fk_MED))->click();
    }

    public function doFilterByAllCategories(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::chk_check_all_categorias))->click();
    }

    public function doFilterByCheckAllEstab(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::chk_check_all_estabelecimento))->click();
    }

    public function doFilterByCheckAllContract(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::chk_check_all_contrato))->click();
    }

    public function doFilterByAllGroup(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::chk_check_all_grupo))->click();
    }

    public function doFilterByCheckAllClasses(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::chk_check_all_classe))->click();
    }

    public function doFilterByCheckAllSubGroup(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::chk_check_all_subgrupo))->click();
    }

    public function doFilterByCheckAllPolitics(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::chk_check_all_politica))->click();
    }

    public function doFiltersByCheckFlags(){
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::checkbox_5))->click();
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::checkbox_6))->click();
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::checkbox_7))->click();
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::checkbox_8))->click();
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::checkbox_9))->click();
        $this->webDriver->findElement(WebDriverBy::xpath(DashboardLocators::checkbox_10))->click();
    }
}
?>