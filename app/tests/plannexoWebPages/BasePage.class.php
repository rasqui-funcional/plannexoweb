<?php

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;

abstract class BasePageObject
{
    /** @var RemoteWebDriver */
    protected $webDriver;
    /**
     * @param RemoteWebDriver $driver
     */
    public function __construct(RemoteWebDriver $driver)
    {
        $this->webDriver = $driver;
        $this->webDriver->manage()->timeouts()->implicitlyWait(15);
    }
}
