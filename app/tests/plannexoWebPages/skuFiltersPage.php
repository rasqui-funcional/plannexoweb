<?php
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverSelect;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverActions;


require_once ('tests/plannexoWebPages/BasePage.class.php');
require_once ('tests/plannexoWebLocators/skuFiltersLocators.php');

class SkuFilterPage extends BasePageObject{

    public function open()
    {
        $this->webDriver->get(("http://172.17.0.1/br/sku/filters"));
        $this->webDriver->navigate()->refresh();
    }

    public function doSearch(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::BTN_SEARCH))->click();
    }

    public function doCleanFilters(){
        $this->webDriver->findElement(WebDriverBy::xpath(SkuFiltersLocators::LINK_CLEAN_FILTERS))->click();
    }

    public function doEditFilters(){
        $this->webDriver->findElement(WebDriverBy::xpath(SkuFiltersLocators::LINK_EDIT_FILTER))->click();
    }

    public function doSaveFilters(){
        $element = $this->webDriver->switchTo()->activeElement()->findElement(WebDriverBy::xpath("//span[@id='spn_save']"));
        $element->click();
    }

    public function doDeleteFilters(){
        $this->webDriver->findElement(WebDriverBy::cssSelector(SkuFiltersLocators::BTN_DELETE_FILTER))->click();
    }

    public function doCancelActionFilters(){
        $this->webDriver->findElement(WebDriverBy::cssSelector(SkuFiltersLocators::BTN_CANCEL_FILTER))->click();
    }

    public  function doSelectItemsInEstab(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::COD_ESTAB))->click();
    }

    public function doSelectStatusSkuByErp(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SIT_SKU_ERP_ACTIVE))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SIT_SKU_ERP_INACTIVE))->click();
    }

    public function doSelectStatusSkuBySystem(){
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SIT_SKU_SYSTEM_ACTIVE))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SIT_SKU_SYSTEM_INACTIVE))->click(); 
    }

    public function doSelectStatusSkuByAnalyze(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SIT_SKU_ANALYZED_ACTIVE))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SIT_SKU_ANALYZED_INACTIVE))->click();
    }

    public function doSelectCurveABCSku(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::CURVE_A))->click();
    }

    public function doSelectCurveXYZSku(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::CURVE_X))->click();
    }

    public function doSelectCurvePQRSku(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::CURVE_P))->click();
    }

    public function doSelectCurve123Sku(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::CURVE_1))->click();
    }
    
    public function doSearchSkuByCodItemOrDescItems(){  
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::COD_ITEM_PK))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::COD_ITEM_PK))->clear();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::COD_ITEM_PK))->sendKeys("1234");
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::DESC_ITEM))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::DESC_ITEM))->clear();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::DESC_ITEM))->sendKeys("teste");
    }
    
    public function doSelectExhibitionItems(){
        $this->webDriver->findElement(WebDriverBy::xpath(SkuFiltersLocators::EXHIBITION_ID_NEC))->click();
        $this->webDriver->findElement(WebDriverBy::xpath(SkuFiltersLocators::EXHIBITION_PARCELA))->click();
        $this->webDriver->findElement(WebDriverBy::xpath(SkuFiltersLocators::EXHIBITION_COD_ITEM))->click();
    }
   
   public function doSelectSpecialFiltersByCoverWithDays(){
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_COVER_DAYS))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_COVER_DAYS))->clear();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_COVER_DAYS))->sendKeys("2");
   }
    
   public function doSelectSpecialFiltersByCoverWithMinimumDays(){
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_MINIMUM_DAYS))->click();
   } 

   public function doSelectSpecialFiltersByPurchaseOrder(){
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_PURCHASE_ORDER_ACTIVE))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_PURCHASE_ORDER_INACTIVE))->click();
   } 
    
   public function doSelectSpecialFiltersBySkuUrgent(){

    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_SKU_SITUATION_URGENT_ACTIVE))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_SKU_SITUATION_URGENT_INACTIVE))->click();
   }

   public function doSelectSpecialFiltersByPurchaseRequistion(){
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_PURCHASE_REQUISTION_ACTIVE))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_PURCHASE_REQUISTION_INACTIVE))->click();
   }

   public function doSelectSpecialFiltesByAnalizeDelayed(){
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::SPECIAL_FILTERS_ANALIZE_DELAYED))->click();
   }
    
   public function doSelectParametersSignalized(){
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_SIGNAL_ZERO))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_SIGNAL_TARGET))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_SIGNAL_MINIMUM_2))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_SIGNAL_MAXIMUM))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_SIGNAL_MAXIMUM_2))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_SIGNAL_MINIMUM))->click();
   }

   public function doSelectParametersLifeCycle(){
    //$this->webDriver->findElement(WebDriverBy::xpath(SkuFiltersLocators::ciclo_de_vida))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_SKU_LIFE_CYCLE_NEW))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_SKU_LIFE_CYCLE_MATURE))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_SKU_LIFE_CYCLE_DESCONTINUED))->click();
   }

   public function doSelectParametersPolitics(){
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_POLICY_A))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_POLICY_B))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_POLICY_C))->click();
    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::PARAMETER_POLICY_TOTAL_STOCK))->click();
   }
    public function doSelectDateInterval(){
    $selectElement = $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::data_alterada_chegada));
    $select = new WebDriverSelect($selectElement);
    $select->selectByValue('PR_30DIAS');

    $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::data_alterada_chegada))->click();
    $this->webDriver->findElement(WebDriverBy::xpath(SkuFiltersLocators::prox_parcelas))->click();
    $this->webDriver->findElement(WebDriverBy::xpath(SkuFiltersLocators::prox_parcelas))->clear();
    $this->webDriver->findElement(WebDriverBy::xpath(SkuFiltersLocators::prox_parcelas))->sendKeys("2");
    }

    public function doSelectCategorySystemCheckByDiv(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::CATEGORY_SYSTEM_BY_DIV))->click();
    }
    public function doSelectCategorySystemCheckByMat(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::CATEGORY_SYSTEM_BY_MAT))->click();
    } 
    public function doSelectCategorySystemByMed(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::CATEGORY_SYSTEM_BY_MED))->click();
    }
    
    public function doSelectGroupItem(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::GROUP_SELECT_ITEM))->click();
    }
    public function doSelectManagerProfile(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::gestor_1))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::gestor_2))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::gestor_3))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::gestor_4))->click();
    }
   
    public function doSelectClasses(){
        $this->webDriver->findElement(WebDriverBy::cssSelector(SkuFiltersLocators::select_classe))->click();
    }

    public function doSelectSubGroup(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::cod_group4_fk_1_133_516))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::cod_group4_fk_1_15_40))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::cod_group4_fk_1_15_39))->click();
    }
    
    public function doSelectContract(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::cod_group6_fk_Sim))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::cod_group6_fk_hashtag))->click();
    }

    public function doSelectStandardItems(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::cod_group5_fk_hashtag))->click();
    }

    public function doSelectHighCost(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::HIGH_COST_DOWN))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::HIGH_COST_UP))->click();
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::HIGH_COST_MEDIAN))->click();
    }

    public function doSelectTestItems(){
        $this->webDriver->findElement(WebDriverBy::id(SkuFiltersLocators::cod_group8_fk_hashtag))->click();

    }

    public function doSelectTypeFilterPrivate(){
        $element = $this->webDriver->switchTo()->activeElement()->findElement(WebDriverBy::id("filter_perfil_Privado"));
        $element->click();
    }

    public function doSelectTypeFilterPublic(){
        $element=$this->webDriver->findElement(WebDriverBy::xpath("//input[@id='filter_perfil_Privado']"));
        $element->click();
    }

    public function doSelectAndChooseFilterAlreadySaved($filterName){
        $selectElement2 = $this->webDriver->findElement(WebDriverBy::cssSelector("#select_filtro"));
        $s = new WebDriverSelect($selectElement2);
        $s->selectByVisibleText("plannexo 3");
    }
    
    public function doCreateNewFilter($filterName){
        $this->webDriver->findElement(WebDriverBy::xpath("//span[@id='btn_filtro_text']"))->click();
        sleep(5);
        $element = $this->webDriver->switchTo()->activeElement()->findElement(WebDriverBy::id("filter_nome"));
        $element->sendKeys($filterName);
        sleep(5);
        $element2 = $this->webDriver->switchTo()->activeElement()->findElement(WebDriverBy::xpath("//div[5]/div/div/div[2]/div[2]/input[2]"));
        $element2->click();
        sleep(5);
        $element3 = $this->webDriver->switchTo()->activeElement()->findElement(WebDriverBy::xpath("//div[3]/button/span"));
        $element3->click();
        sleep(5);
        $selectElement4 = $this->webDriver->findElement(WebDriverBy::cssSelector("#select_filtro"));
        $s2 = new WebDriverSelect($selectElement4);
        $s2->selectByVisibleText("plannexo 5");
        sleep(5);
        $this->webDriver->findElement(WebDriverBy::xpath("//span[@id='btn_filtro_text']"))->click();
        sleep(5);
        $element5 = $this->webDriver->switchTo()->activeElement()->findElement(WebDriverBy::id("filter_nome"));
        $element5->clear();
        $element5->sendKeys("funcionou");
        sleep(5);
        $element6 = $this->webDriver->switchTo()->activeElement()->findElement(WebDriverBy::xpath("//div[3]/button/span"));
        $element6->click();
        sleep(5);
        $selectElement7 = $this->webDriver->findElement(WebDriverBy::cssSelector("#select_filtro"));
        $s3 = new WebDriverSelect($selectElement7);
        $s3->selectByVisibleText("funcionou");
        sleep(5);
        $this->webDriver->findElement(WebDriverBy::xpath("//span[@id='btn_filtro_text']"))->click();
        sleep(5);
        $element8 = $this->webDriver->switchTo()->activeElement()->findElement(WebDriverBy::xpath("//div[3]/span/span"));
        $element8->click();
        sleep(5);
        $element9 = $this->webDriver->switchTo()->activeElement()->findElement(WebDriverBy::xpath("//div[3]/div[2]/button"));
        $element9->click();
        sleep(3);
    }

}

?>