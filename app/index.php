<?php
//ATIVA O OUTPUT BUFFERING
ob_start();
header('Access-Control-Allow-Origin: *');
// SESSION START
session_start();

ini_set('memory_limit', '-1');

//Include de todas as partes do compose
require('vendor/autoload.php');
require('Config.inc.php');

//DEFINE AS ROTAS ##################
require('model/Http/execRoutes.php');

require('resources/compose.php');

//Manter sempre como a última linha para não quebrar o output buffering
ob_flush();
