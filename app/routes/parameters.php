<?php

/**
 * Parameters  Routes Politics
 */
$Routes += [
    'parameters/politics/list' => 'Parameters/Politics/PoliticsListController@getList',
    'parameters/politics/table' => 'Parameters/Politics/PoliticsListController@getTable',
    'parameters/politics/getPoliticsData/{politicsId}' => 'Parameters/Politics/PoliticsListController@show',
    'parameters/politics/update' => 'Parameters/Politics/PoliticsListController@update',
    'parameters/politics/new' => 'Parameters/Politics/PoliticsListController@new',
    'parameters/politics/deposits' => 'Parameters/Politics/PoliticsListController@deposits',
    'parameters/politics/calendar' => 'Parameters/Politics/PoliticsListController@calendar',
    'parameters/politics/delete' => 'Parameters/Politics/PoliticsListController@delete',
    'parameters/politics/duplicate' => 'Parameters/Politics/PoliticsListController@duplicate'
];
/**
 * Parameters Routes Calendar
 */
$Routes += [
    'parameters/calendar/list' => 'Parameters/Calendar/CalendarListController@showList',
    'parameters/calendar/table' => 'Parameters/Calendar/CalendarListController@getTable',
    'parameters/calendar/new' => 'Parameters/Calendar/CalendarController@insertCalendars',
    'parameters/calendar/delete' => 'Parameters/Calendar/CalendarController@deleteCalendar',
    'parameters/calendar/duplicate' => 'Parameters/Calendar/CalendarController@duplicateCalendars',
    'parameters/calendar/update' => 'Parameters/Calendar/CalendarController@updateCalendars',
    'parameters/calendar/{id}' => 'Parameters/Calendar/CalendarController@getCalendarData'
];
