<?php
if (cannot('spdmBuyer')) {
    $Routes += [
        'sku/filters' => 'SkuController@getFormData',
        'sku/list' => 'SkuController@getList',
        'sku/list/{action}/' => 'SkuController@getDynamicList',
        'sku/table' => 'SkuController@getTable',
        'sku/table/footer' => 'SkuController@getTableFooter',
        'sku/getAllFilters' => 'SkuController@getFiltersSelected',
        'sku/csv' => 'SkuController@getCsvSkuTable',
    ];

    $Routes += [
        'infosku/infos/children_item_list' => 'InfoSKU/InformationController@getChildrenData',
        'infosku/informations/{skuID}' => 'InfoSKU/InformationController@basicInformations',
        'infosku/informationsUpdate/{skuID}' => 'InfoSKU/InformationController@basicInformationsUpdate',
        'infosku/recalculate/{skuID}' => 'InfoSKU/InformationController@recalculate',
        'infosku/erp' => 'ERPActivityController@getActivity',
        'infosku/erp/OverStock' => 'ERPActivityController@getStockOverBalance',
        'infosku/erp/PurchaseOrder' => 'ERPActivityController@getPurchaseOrder',
        'infosku/erp/PurchaseRequest' => 'ERPActivityController@getPurchaseRequest',
        'infosku/erp/InTransaction' => 'ERPActivityController@getInTransaction',
        'infosku/erp/OutTransaction' => 'ERPActivityController@getOutTransaction',
        'infosku/erp/MonthlyConsumption' => 'ERPActivityController@getMonthlyConsumption',
        'infosku/header/{skuId}' => 'InfoSKU/DashboardInfoController@getHeader',
        'infosku/updatestatus' => 'InfoSKU/DashboardInfoController@updateSkuStatus',
        'infosku/nextinstallment' => 'InfoSKU/DashboardInfoController@getNextInstallment',
        'infosku/shoppingforecastdaily/{skuId}/{dateFrom}/{dateTo}' => 'Graphics/ShoppingForecastController@getDataDaily',
        'infosku/shoppingforecastmonthly/{skuId}' => 'Graphics/ShoppingForecastController@getDataMonthly',
        'infosku/balancing/{skuId}' => 'Graphics/StockBalanceController@getInfoSkuBalancing',
        'infosku/calendar/infos/{skuID}' => 'InfoSKU/InformationController@getCalendar',
        'infosku/calendar/dates/{calendarID}' => 'InfoSKU/InformationController@getDates',
        'infosku/calendar/update/{skuID}' => 'InfoSKU/InformationController@updateCalendar',
        'infosku/policy/{skuID}' => 'InfoSKU/InformationController@getPolicy',
        'infosku/policy/{skuID}/id/{policyID}' => 'InfoSKU/InformationController@getPolicyByID',
        'infosku/policy/update/{skuID}' => 'InfoSKU/InformationController@updatePolicy',
        'infosku/stockposition/weekly/{skuID}' => 'Graphics/StockPositionController@getStockWeeklybySku',
        'infosku/stockposition/{skuID}' => 'Graphics/StockPositionController@getStockbySku',
        'infosku/deviationconsumption/{skuId}' => 'Graphics/DeviationConsumptionController@getData',
        'infosku/dailyhistory/{skuId}/{dateFrom}/{dateTo}' => 'Graphics/DailyHistoryController@getData',
        'infosku/interdependence/establishment_origin/{codItemPK}' => 'InfoSKU/InformationController@establishmentOrigin',
        'infosku/interdependence/{skuId}' => 'InfoSKU/InformationController@getInterdependence',
        'infosku/interdependence/update/{skuID}' => 'InfoSKU/InformationController@updateInterdependence',
        'infosku/interdependence/delete/{skuID}' => 'InfoSKU/InformationController@deleteInterdependence',
    ];

    /**
     * Log  Routes
     */
    $Routes += [
        'infosku/log/log_list' => 'InfoSKU/LogController@getLogData',
        'infosku/log/fetch' => 'InfoSKU/LogController@fetch',
    ];

    /**
     * Suggest (Sugestões de Compra) Routes
     */
    $Routes += [
        'suggest/filters' => 'SuggestController@getFormData',
        'suggest/list' => 'SuggestController@getList',
        'suggest/list/{indicators}' => 'SuggestController@getCustomSuggest',
        'suggest/helpers' => 'SuggestController@getAllHelpers',
        'suggest/helper/lang' => 'SuggestController@getLang',
        'suggest/table' => 'SuggestController@getTable',
        'suggest/table/footer' => 'SuggestController@getTableFooter',
        'suggest/save' => 'SuggestController@saveChanges',
        'suggest/csv' => 'SuggestController@getCsvSuggestTable',
        'suggest/add' => 'SuggestAddController@addSuggest',
        'suggest/add/getskudescription' => 'SuggestAddController@getSKUDescription',
        'suggest/add/getestabs' => 'SuggestAddController@getEstabs',
        'suggest/add/checkDate' => 'SuggestAddController@checkDateForSug',
        'suggest/sugtogroup/{id_psug_pk}' => 'SuggestController@sugToGroup',
        'suggest/group-installments' => 'SuggestController@groupInstallments',
    ];

    /**
     * Order Cover (Capa de Pedido) Routes
     */
    $Routes += [
        'ordercover/filters' => 'OrderCover/OrderCoverFiltersController@getFilters',
        'ordercover/list' => 'OrderCover/OrderCoverListController@getList',
        'ordercover/table' => 'OrderCover/OrderCoverListController@getTable',
        'ordercover/setgroup' => 'OrderCover/OrderCoverListController@setGroup',
        'ordercover/suggestions/preparelist' => 'OrderCover/OrderCoverSuggestionsController@prepareList',
        'ordercover/suggestions/list' => 'OrderCover/OrderCoverSuggestionsController@getList',
        'ordercover/suggestions/table' => 'OrderCover/OrderCoverSuggestionsController@getTable',
        'ordercover/suggestions/update' => 'OrderCover/OrderCoverSuggestionsController@updateSuggestions',
        'ordercover/suggestions/delete' => 'OrderCover/OrderCoverSuggestionsController@deleteSuggestions',
        'ordercover/suggestions/remove' => 'OrderCover/OrderCoverSuggestionsController@removeSuggestions',
        'ordercover/process' => 'OrderCover/OrderCoverListController@processOrderCover'
    ];

    /**
     * Approvals (Histórico de Aprovações) Routes
     */
    $Routes += [
        'approvals/list' => 'Approvals/ApprovalsController@getList',
        'approvals/list/{order}' => 'Approvals/ApprovalsController@getList',
        'approvals/table' => 'Approvals/ApprovalsController@getTable',
        'approvals/duplicate/save' => 'Approvals/ApprovalsDuplicateController@save',
        'approvals/suggest/list/{id}' => 'Approvals/ApprovalsSuggestController@getList',
        'approvals/suggest/modaltable' => 'Approvals/ApprovalsDuplicateController@getTable',
        'approvals/modal/{id}' => 'Approvals/ApprovalsSuggestController@getData',
        'approvals/suggest/table/{codHeaderPk}' => 'Approvals/ApprovalsSuggestController@getTable',
        'approvals/suggest/delete' => 'Approvals/ApprovalsSuggestController@delData'
    ];
}
/**
 * Purchase Order (Ordem de Compras) Routes
 */
$Routes += [
    'purchaseorder/filters' => 'PurchaseOrder/PurchaseOrderFiltersController@getFilters',
    'purchaseorder/list' => 'PurchaseOrder/PurchaseOrderGroupedController@getList',
    'purchaseorder/table' => 'PurchaseOrder/PurchaseOrderGroupedController@getTable',
    'purchaseorder/orders/preparelist' => 'PurchaseOrder/PurchaseOrderListController@prepareList',
    'purchaseorder/orders/list' => 'PurchaseOrder/PurchaseOrderListController@getList',
    'purchaseorder/orders/table' => 'PurchaseOrder/PurchaseOrderListController@getTable'
];


