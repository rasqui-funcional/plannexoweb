<?php

$Routes += [
    'modals/excess/{from}/{id}' => 'ExcessController@getExcess',
    'modals/orders/{sku_id}' => 'OrdersController@getOrders',
    'dashboard/skus' => 'Graphics/ServiceLevelController@getServiceLevel',
    'dashboard/insVsOuts' => 'Graphics/InsVsOutsController@getInsVsOuts',
    'dashboard/stock/weekly/{check}' => 'Graphics/StockPositionController@getStockWeekly',
    'dashboard/stock/{check}' => 'Graphics/StockPositionController@getStock',
    'dashboard/stock' => 'Graphics/StockPositionController@getStock',
    'dashboard/balancing' => 'Graphics/StockBalanceController@getStockBalance',
    'dashboard/adhesion' => 'Graphics/AdhesionController@getAdhesion',
    'dashboard/urgency' => 'Graphics/UrgencyController@getUrgency',
    'dashboard' => 'DashboardController@getDashboardInfo',
    'dashboard/table' => 'DashboardController@getFilterDashboard ',
    'dashboard/getjsondata' => 'DashboardController@getJsonData',
];

/**
 * Resume (Resumo) Routes
 */
$Routes += [
    'resume/filters' => 'Resume/ResumeFiltersController@getFilters',
    'resume/list' => 'Resume/ResumeListController@prepareResumeTable',
    'resume/table' => 'Resume/ResumeListController@getResumeTable',
    'resume/csv' => 'Resume/ResumeListController@getResumeCSV',
];