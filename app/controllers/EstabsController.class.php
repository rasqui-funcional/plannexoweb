<?php

class EstabsController
{

    public function __construct()
    {
        $this->session = Login::getUserSession();
        $this->serviceEstabs = new EstabsService($this->session['id_company_pk']);
    }

    public function getEstabs()
    {
        $cache_estabs = $this->serviceEstabs->getCacheEstabs();

        if ($cache_estabs) {
            $view_data['estabs'] = $cache_estabs;
        } else {
            $view_data['estabs'] = $this->serviceEstabs->updateEstabs();
        }

        $view_data['json'] = true;

        return $view_data;
    }

}
