<?php

class TestSkuController extends Fields
{
    private $ViewData;
    private $Query;
    private $post;
    private $user;
    private $skuService;
    private $dynamicListWhere = [];
    private $groups = [];

    public function __construct()
    {
        parent::__construct();
        $this->groups = $this->getNewDinamicFieldsGrouping(2);
        $this->skuService = new SkuService($this->groups['inactive']);
        $this->user = Login::getUserSession();
        $this->dynamicListWhere = [
            'sit_sku' => 1
        ];
    }

    public function getFormData()
    {
        Redis::Set(USERSESSIONID . ":session:dashboard:manager-users", '');
        $ViewData = parent::getFields('vw_scr_scm405g_sku_list');

        $ViewData['select_filtro'] = Fields::getSelectFilter('SCM405G');

        $ViewData['Cols'] = $this->removeInactiveGroups(parent::getTableColumns('vw_scr_scm405g_sku_list'));
        foreach ($ViewData['Cols'] as $field) {
            $ViewData['post'][$field] = Request::post($field);
        }

        $ViewData['chkvetor'] = array_pop($ViewData['post']);
        if (is_array($ViewData['chkvetor'])) {
            foreach ($ViewData['chkvetor'] as $chave => $valor) {
                $ViewData['chkvetor'][$valor] = "checked";
            }
        }

        $ViewData['filter_temp'] = $this->recoveryFilter();
        return $ViewData;
    }

    /**
     * <b>GetList</b>
     * Retrieve filter data to be applied in SKU LIST queries.
     *
     *
     * @param null $sku
     * @return ARRAY
     */
    public function getList($sku = null)
    {
        $this->post = Request::post();
        if (!$this->post) {
            if(!$sku){
                Request::redirect('sku/filters');
            }

            $this->post = $sku;
        }

        $display_columns = $this->skuService->prepareDisplayColumns($this->post['chkvetor']);
        Redis::Set(USERSESSIONID . ':session:sku:list:cols', json_encode($this->prepareCols($display_columns)));

        Redis::Set(USERSESSIONID . ':session:sku:post', json_encode($this->post));

        /**
         * Returns filters to <options/> on search <select/>
         */
        $ViewData['SearchFields'] = $this->groups['active'];
        return $ViewData;
    }

    /**
     * <b>GetTable</b>
     * Return LIST SKU table data.
     *
     *
     * @return ARRAY
     */
    public function getTable()
    {
        $this->ViewData['ajax'] = true;
        $this->post['table'] = Request::post();
        $this->post['list'] = json_decode(Redis::Get(USERSESSIONID . ':session:sku:post'), true);


        $this->ViewData['Cols'] = json_decode(Redis::Get(USERSESSIONID . ':session:sku:list:cols'), true);

        $query_columns = $this->skuService->prepareQueryColumns(null);
        Redis::Set(USERSESSIONID . ':session:sku:list:query-cols', $query_columns);
        Redis::Set(USERSESSIONID . ':session:sku:list:filter-temp', json_encode($this->post['list']['filter_temp']));

        $this->prepareQuery($this->post['list']);

        // table
        $Offset = $this->prepareSetOffset($this->post['table']['page'], $this->post['table']['limit']);
        $Columns = $this->prepareColumns();

        $Order = (isset($this->post['table']['order']) && is_array($this->post['table']['order'])
            ? $this->prepareOrder($this->post['table']['order'])
            : Redis::Get(USERSESSIONID . ':session:sku:list:query-order'));

        Redis::Set(':session:sku:list:query-order', json_encode($Order));

        /*
        *os termos mudarão caso o usuário selecione um filtro dentro da lista de sugestão de compra, ou seja, se ele selecionou curva A na tela de filtro,
        quando estiver na lista e mudar o filtro para curva B, os filtros anteriores deverão ser esquecidos. Antes ele acumulava 'and where' na consulta
        *
        */
        $objTerms = json_decode(Redis::Get(USERSESSIONID . ':session:sku:list:query-basic'));
        $search_value = strtolower($this->post['table']['searchValue']);

        $Terms =  $this->prepareTerms($objTerms);
        if (!empty($search_value)) {
            $Terms =  $objTerms->where . $objTerms->company . $objTerms->isSupplier . $this->prepareSearch($this->post['table']);
        }
        Redis::Set(USERSESSIONID . ':session:sku:list:csv:terms', $Terms);

        $this->ViewData['dataTable'] = $this->skuService->getDataTableOci($Offset, $this->post['table']['limit'], $Columns, $Terms, $Order);
        $this->ViewData['dataTable'] = $this->skuService->avoidJoinOci($this->ViewData['dataTable']);

        $this->unsetCols();

        return $this->ViewData;
    }

    public function getTableFooter()
    {
        $this->prepareQuery( json_decode(Redis::Get(USERSESSIONID . ':session:sku:post'), true));
        
        $request = Request::post();
        if(isset($request['searchValue']) || isset($request['searchCol'])) {
            $request["recalcFooter"] = true;
            $this->prepareSearch($request);
        }
        $this->prepareDataFooter(json_decode(Redis::Get(USERSESSIONID . ':session:sku:list:cols'), true));

        $data = [];
        $data['data'] = $this->skuService->prepareData(
            json_decode(Redis::Get(USERSESSIONID . ':session:sku:list:footer-data'), true)
        );
        $data['count'] = Redis::Get(USERSESSIONID . ':session:sku:list:footer:count');
        $data['pages'] = Utilities::calculatePages(
            $data['count'],
            $request['limit']
        );

        header('Content-Type: application/json');
        echo json_encode($data); 
        die();
    }

    private function unsetCols()
    {
        unset(
            $this->ViewData['Cols']['TOTAL_OUT_LAST_MONTH_AVG'], 
            $this->ViewData['Cols']['TOTAL_OUT_LAST_HALF_MONTH_AVG'], 
            $this->ViewData['Cols']['TOTAL_OUT_LAST_WEEK_AVG'], 
            $this->ViewData['Cols']['TOTAL_OUT_LAST_WEEK'] 
        );
    }

    private function prepareColumns()
    {
        $columnsList = Redis::Get(USERSESSIONID . ":session:sku:list:query-cols");
        $columnsList = $this->addSpecificFields($columnsList);
        if($this->isQueryingForDateLastCalc($columnsList) !== false) {
            $columnsList = $this->castDateLastCalcToDateTime($columnsList);
        }

        return $columnsList;
    }

    private function isQueryingForDateLastCalc($columnsList)
    {
        return strpos($columnsList, 'date_last_calc');
    }

    private function castDateLastCalcToDateTime($columnsList)
    {
        return str_replace(
            'date_last_calc',
            "TO_CHAR(date_last_calc, 'DD/MM/YYYY HH24:MI:SS') AS date_last_calc",
            strtolower($columnsList)
        );
    }

    private function addSpecificFields($columnsList)
    {
        $columns = array_map('trim', explode(',', $columnsList));
        array_push($columns, 'DAYS_HORIZON');

        return implode(',', $columns);
    }

    public function getDynamicList()
    {
        $post = Request::post();
        switch (Request::get('action')) {
            case 'setlevelfilters':
                return $this->getPrepareFilters($post);
            case 'setresume':
                $post['sit_sku'] = 1;
                return $this->getPrepareFilters($post);
            case 'level':
            case 'resume':
                return $this->getViewData();
            case 'getsku':
                $cod_sku['cod_item_pk'] = filter_input(INPUT_GET, 'cod_sku');
                return $this->getList($cod_sku);
            default:
                Request::redirect('sku/filters');
                break;
        }
    }

    public function getCsvSkuTable()
    {
        $this->ViewData['ajax'] = true;
        $this->ViewData['error'] = [];
        $this->ViewData['collumns_exib'] = json_decode(Redis::Get(USERSESSIONID . ':session:sku:list:cols'), true);

        $table = 'vw_scr_scm405g_sku_list';
        $columns = implode(",", $this->removeInactiveGroups($this->prepareColumns()));
        $columns = $this->removeColumns($columns);

        /**
         * Ajusta tambem as colunas de resultado.
         */
        $this->ViewData['collumns_exib'] =  $this->removeColumns($this->ViewData['collumns_exib']);

        $strCodGroupJoin = "";
        for($i = 2; $i <= 12; $i++){
            if (strpos(strtolower($columns), "scm_group{$i}.desc_group_cont as cod_group{$i}_fk")) {
                $strCodGroupJoin = $strCodGroupJoin . "
                LEFT JOIN scm_group_cont scm_group{$i}
                    ON {$table}.id_company_fk = scm_group{$i}.id_company_fk
                        AND scm_group{$i}.COD_GROUP_PK = {$table}.cod_group{$i}_fk
                        AND scm_group{$i}.NUM_GROUP_FK = {$i} ";
                continue;
            } elseif (strpos(strtolower($columns),"cod_group{$i}_fk,")) {
                $columns = str_replace("cod_group{$i}_fk", "scm_group{$i}.desc_group_cont as cod_group{$i}_fk", strtolower($columns));

                $strCodGroupJoin = $strCodGroupJoin . "
                LEFT JOIN scm_group_cont scm_group{$i}
                    ON {$table}.id_company_fk = scm_group{$i}.id_company_fk
                        AND scm_group{$i}.COD_GROUP_PK = {$table}.cod_group{$i}_fk
                        AND scm_group{$i}.NUM_GROUP_FK = {$i} ";
            }
        }
        $columns = "* FROM (SELECT DISTINCT " . $columns;
        $objTerms = json_decode(Redis::Get(USERSESSIONID . ':session:sku:list:query-basic'));
        $columns =  str_replace(
            'cod_item_pk',
            "vw_scr_scm405g_sku_list.cod_item_pk",
            strtolower($columns)
        );

        $columns =  str_replace(
            'cod_estab_fk',
            "vw_scr_scm405g_sku_list.cod_estab_fk",
            strtolower($columns)
        );

        $columns =  str_replace(
            'qty_excess',
            "vw_scr_scm405g_sku_list.qty_excess",
            strtolower($columns)
        );

        $columns =  str_replace(
            's.supplier_name last_purchase_supplier',
            "s.supplier_name as last_purchase_supplier",
            strtolower($columns)
        );

        if(array_search('ind_excess', $this->ViewData['collumns_exib']) != false){
            $columns = str_replace(
                ',ind_excess,',
                ",case
                    when e.qty_excess > 0
                        then concat(e.desc_estab, concat(' - ', to_char(e.qty_excess)))
                    else '0'
                end as ind_excess,",
                strtolower($columns)
            );
        }

        $left_joins = "
                LEFT JOIN erp_local l
                    ON {$table}.id_company_fk = l.id_company_fk
                    AND {$table}.default_local = l.cod_local_pk
                    AND {$table}.cod_estab_fk= l.cod_estab_pk
                {$strCodGroupJoin}
                LEFT JOIN scm_supplier s
                    ON {$table}.last_purchase_supplier = s.id_supplier_pk
                    AND {$table}.id_company_fk = s.id_company_fk
                LEFT JOIN
                (SELECT
                    ESTB.ID_COMPANY_FK,
                    ESTB.COD_ESTAB_FK,
                    ESTB.COD_ITEM_PK,
                    estb.QTY_EXCESS,
                    ESTBDESC.DESC_ESTAB
                FROM vw_scr_scm475g_excess_estab estb
                LEFT JOIN SCM_ESTAB estbdesc
                    ON ESTB.COD_ESTAB_FK = ESTBDESC.COD_ESTAB_PK
                    AND estb.ID_COMPANY_FK = ESTBDESC.ID_COMPANY_FK
                WHERE ESTB.id_company_fk = {$objTerms->company}) e
                ON e.id_company_fk = vw_scr_scm405g_sku_list.id_company_fk
                AND e.cod_estab_fk != vw_scr_scm405g_sku_list.COD_ESTAB_FK
                AND e.cod_item_pk = vw_scr_scm405g_sku_list.COD_ITEM_PK
        ";

        $terms = Redis::Get(USERSESSIONID . ':session:sku:list:csv:terms');
        $orderByIndex = strpos($terms, 'ORDER BY', 0);
        $where = substr($terms, 0, $orderByIndex) . ') a ORDER BY ';
        $orderBy = substr($terms, ($orderByIndex + 9));

        $terms = $where . $orderBy;
        $this->ViewData['dataTable'] = DB::Select($table . $left_joins, $columns, $terms);
        $this->prepareOBS();

        return $this->ViewData;
    }

    private function prepareFilters($filters)
    {
        $columns = $this->removeInactiveGroups(parent::getTableColumns('vw_scr_scm405g_sku_list'));
        $this->prepareQuery($filters);
        $this->prepareDataFooter($columns);

        Redis::Set(USERSESSIONID . ':session:sku:list:cols', json_encode($columns));
        Redis::Set(USERSESSIONID . ':session:sku:list:query-cols', implode(',', $columns));
    }

    private function removeInactiveGroups($columns)
    {
        if (!is_array($columns)) {
            $columns = array_map("trim", explode(",", $columns));
        }

        $inactive_groups = $this->groups['inactive'];
        foreach ($inactive_groups as $inactive_group) {
            $index = array_search("cod_group{$inactive_group['NUM_GROUP_PK']}_fk", $columns);
            if($index !== false){
                unset($columns[$index]);
            }
        }
        return $columns;
    }

    private function getViewData()
    {
        $view_data['Cols'] = $this->prepareCols(parent::getTableColumns('vw_scr_scm405g_sku_list'));
        $view_data['SearchFields'] = $this->groups['active'];

        return $view_data;
    }

    private function prepareOBS()
    {
        if (isset($this->ViewData['dataTable'][0]['OBS'])) {
            foreach ($this->ViewData['dataTable'] as $Key => $Data) {
                $this->ViewData[$Key]['OBS'] = stream_get_contents($Data['OBS']);
            }
        }
    }

    private function prepareSearch($post)
    {
        $objTerms = json_decode(Redis::Get(USERSESSIONID . ':session:sku:list:query-basic'));
        $search_query = null;
        $terms = null;
        $search_value = strtolower($post['searchValue']);

        if (!empty($search_value)) {
            $clause = 'LIKE';
            $value = "'%{$search_value}%'";

            if ($post['searchCol'] == 'cod_item_pk') {
                $checkValue = strpos($search_value, ',');
                $clause = 'LIKE';
                $value = "'%{$search_value}%'";
                if ($checkValue!=false) {
                    $clause = 'IN';
                    $value = "({$search_value})";
                }
            }

            if (array_key_exists('searchCol', $post) || $post['searchValue']) {
                $search_query = " AND LOWER (vw_scr_scm405g_sku_list.{$post['searchCol']}) {$clause} {$value}";
            }
            $terms =  $objTerms->where . $objTerms->company . $objTerms->isSupplier;
        } else {
            $terms =  $objTerms->where . $objTerms->company . $objTerms->isSupplier . $objTerms->fields . $objTerms->specialFilters . $objTerms->date;
        }

        /**
         * Recalcula o footer caso seja necessário
         */
        if ($post['recalcFooter']) {
            $this->Query = $terms . $search_query;
//            $cols = $this->prepareCols(Redis::HGet(USERSESSIONID . ':session:sku:list:cols'));
            $cols = json_decode(Redis::Get(USERSESSIONID . ':session:sku:list:cols'), true);
            $this->prepareDataFooter($cols);
        }
        $manager_users = Redis::Get(USERSESSIONID . ":session:dashboard:manager-users");
        return $search_query . $manager_users;
    }

    private function prepareQuery($Post)
    {
        /*cod_item_pk sempre é enviado ao backend, mesmo que vazio, por isso foi necessário o if abaixo.
        Se o usuario não digitar nada neste campo, ignoramos o if e o cod_item_pk nem fará parte da query,
        caso contrário, transformamos cod_item_pk em array para que seja tratado pela lógica existente no método DB::setFields() do arquivo DB.class.php
        */
        if ($Post['cod_item_pk'] != '') {
            $Post['cod_item_pk'] = explode(',', $Post['cod_item_pk']);
        }

        $Order = (isset($Post['order']) ? $this->prepareOrder($Post['order']) : $this->defaultOrder());
        $Date = (isset($Post['date_last_in']) ? $this->prepareDate($Post['date_last_in']) : null);
        $SpecialFilters = $this->applySpecialFilters($Post);
        if (!is_null($Post['order'])) {
            Redis::HSet(USERSESSIONID . ':session:sku:list:order-data', $Post['order']);
        }

        $Post = $this->unsetDefaults($Post);

        $supplier = null;
        if (can('supplier')) {
            $supplier = $this->setSupplier();
        }

        $basicTerms = new stdClass();
        $basicTerms->where = "WHERE ";
        $basicTerms->where .= array_key_exists('cod_estab_fk', $Post) ? "" : $this->setEstabs();

        $basicTerms->where .= " vw_scr_scm405g_sku_list.id_company_fk = ";
        $basicTerms->company = $this->user['id_company_pk'];
        $basicTerms->isSupplier = $supplier;

        $whereFields = [];
        foreach($Post as $key => $value)
        {
            $whereFields['vw_scr_scm405g_sku_list.'.$key] = $value;
            if($key === 'desc_item'){
                if (count($value) === 1 && !empty($value)) {
                    unset($whereFields['vw_scr_scm405g_sku_list.'.$key]);
                    $whereFields['LOWER(vw_scr_scm405g_sku_list.'.$key.')'] = mb_strtolower("%$value%");
                }
            } else {
                $Post = array_merge($Post, $this->dynamicListWhere);
            }

            if ($key === 'cod_item_pk') {
                if (count($value) === 1 && !empty($value)) {
                    unset($whereFields['vw_scr_scm405g_sku_list.'.$key]);
                    $whereFields['vw_scr_scm405g_sku_list.'.$key] = "%$value[0]%";
                }
            }
        }

        $basicTerms->fields = DB::setFields($whereFields);
        $basicTerms->specialFilters = $SpecialFilters;

        $basicTerms->date = $Date;

        Redis::Set(USERSESSIONID . ':session:sku:list:query-basic', json_encode($basicTerms));

        Redis::Set(USERSESSIONID . ':session:sku:list:query-order', $Order);
        $manager_users = Redis::Get(USERSESSIONID . ":session:dashboard:manager-users");

        $this->Query = $basicTerms->where
            .$basicTerms->company
            .$basicTerms->isSupplier
            .$basicTerms->fields
            .$basicTerms->specialFilters
            .$basicTerms->date
            .$manager_users
            .$Order;
        return $this->Query;
    }

    private function setEstabs()
    {
        return !empty($this->user['estab']) ? "vw_scr_scm405g_sku_list.cod_estab_fk IN({$this->user['estab']}) AND" : '';
    }

    private function setSupplier()
    {
        //return "AND vw_scr_scm405g_sku_list.user_id = {$this->user['user_id']} ";
        return "AND vw_scr_scm405g_sku_list.cod_group11_fk = '1' ";
    }

    private function prepareDataFooter($columns)
    {
        $table = 'vw_scr_scm405g_sku_list
        left join erp_local l
        on vw_scr_scm405g_sku_list.id_company_fk = l.id_company_fk
        and vw_scr_scm405g_sku_list.default_local = l.cod_local_pk
        and vw_scr_scm405g_sku_list.cod_estab_fk= l.cod_estab_pk';
        $query_footer = DB::SelectRow($table, $this->getFooterCols(), $this->Query);
        Redis::Set(USERSESSIONID . ':session:sku:list:footer:count', $query_footer['COUNT(*)']);
        $data_footer = [];
        $column_names = array_keys($columns);
        $upper_column_names = array_map('strtoupper', $column_names);
        foreach ($upper_column_names as $column) {
            $data_footer[$column] = $query_footer[$column];
        }

        Redis::Set(USERSESSIONID . ':session:sku:list:footer-data', json_encode($data_footer));
    }

    private function prepareDate($DateStr)
    {
        $DateQuery = '';
        if ($DateStr != "") {
            $DateStr = trim($DateStr);
            $DateFrom = str_replace('/', '-', $DateStr);
            $DateFrom = (date('Y-m-d', strtotime($DateFrom)));

            $DateQuery = " AND date_last_in between to_date('{$DateFrom} 00:00:00', 'yyyy-mm-dd HH24:mi:ss') and to_date('{$DateFrom} 23:59:59', 'yyyy-mm-dd HH24:mi:ss') ";

            return $DateQuery;
        }
        return $DateQuery;
    }

    private function applySpecialFilters($specialFilters)
    {
        $querySpecialFilters = "";

        if (isset($specialFilters['purchase_req'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldsPurchaseReq($specialFilters);
        }
        if (isset($specialFilters['purchase_order'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldsPurchaseOrder($specialFilters);
        }
        if (isset($specialFilters['qty_inv_origin'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterInvOrigin($specialFilters);
        }
        if (isset($specialFilters['inv_days'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterInvDays($specialFilters);
        }
        if (isset($specialFilters['qty_excess_next_in'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterExcessNextIn($specialFilters);
        }
        if (isset($specialFilters['inv_available'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldCover($specialFilters);
        }
        if (is_numeric($specialFilters['out_last_30d'])) {
            $querySpecialFilters .= $this->applyRangeFilter(
                'out_last_30d',
                (int)$specialFilters['out_last_30d'],
                $specialFilters['range_out_last_30d']
            );
        }
        if (is_numeric($specialFilters['qty_forecast'])) {
            $querySpecialFilters .= $this->applyRangeFilter(
                'qty_forecast',
                (int)$specialFilters['qty_forecast'],
                $specialFilters['range_qty_forecast']
            );
        }
        if (isset($specialFilters['last_purchase_supplier'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldFornUltCompra($specialFilters);
        }
        return $querySpecialFilters;
    }

    /**Sku filter field Cobertura
     * 1 - abaixo do mínimo; 2 - abaixo ou igual ao minimo; 3 - abaixo do leadtime; 4 - abaixo do ponto de pedido
     */
    private function applyFilterFieldCover($specialFilters)
    {
        $applyFilterFieldCover = "";
        if ($specialFilters['inv_available'] != "") {
            if ($specialFilters['inv_available'] === "1") {
                $applyFilterFieldCover = $applyFilterFieldCover . " AND inv_available < qty_min_out";
            } elseif ($specialFilters['inv_available'] === "2") {
                $applyFilterFieldCover = $applyFilterFieldCover . " AND inv_available < qty_min_out";
            } elseif ($specialFilters['inv_available'] === "4") {
                $applyFilterFieldCover = $applyFilterFieldCover . " AND inv_available < qty_pp";
            } elseif ($specialFilters['inv_available'] === "3") {
                $applyFilterFieldCover = $applyFilterFieldCover . " AND inv_days <= lt_total_out";
            }
        }
        return $applyFilterFieldCover;
    }

    private function applyFilterInvOrigin($specialFilters)
    {
        $querySpecialFilters = "";
        if ($specialFilters['qty_inv_origin'] === "on") {
            $querySpecialFilters = $querySpecialFilters . " AND qty_inv_origin > 0";
        }
        return $querySpecialFilters;
    }

    private function applyFilterInvDays($specialFilters)
    {
        $querySpecialFilters = "";
        if ($specialFilters['inv_days'] != "") {
            $querySpecialFilters = $querySpecialFilters . " AND inv_days < '{$specialFilters['inv_days']}'";
        }
        return $querySpecialFilters;
    }

    private function applyFilterExcessNextIn($specialFilters)
    {
        $querySpecialFilters = "";
        if ($specialFilters['qty_excess_next_in'] === "on") {
            $querySpecialFilters = $querySpecialFilters . " AND qty_excess_next_in NOT IN (0) ";
        }
        return $querySpecialFilters;
    }

    private function applyFilterFieldsPurchaseOrder($specialFilters)
    {
        $applyFilterFieldsPurchaseOrder = "";
        if (is_array($specialFilters['purchase_order'])) {
            if (($specialFilters['purchase_order'][0] === "1") && ($specialFilters['purchase_order'][1] === "0")) {
                $applyFilterFieldsPurchaseOrder = $applyFilterFieldsPurchaseOrder . " AND purchase_order IN ('{$specialFilters['purchase_order'][0]}','{$specialFilters['purchase_order'][1]}')";
            } elseif ($specialFilters['purchase_order'][0] === "1") {
                $applyFilterFieldsPurchaseOrder = $applyFilterFieldsPurchaseOrder . " AND purchase_order IN ('{$specialFilters['purchase_order'][0]}')";
            } elseif ($specialFilters['purchase_order'][0] === "0") {
                $applyFilterFieldsPurchaseOrder = $applyFilterFieldsPurchaseOrder . " AND purchase_order IN ('{$specialFilters['purchase_order'][0]}')";
            }
        }
        return $applyFilterFieldsPurchaseOrder;
    }

    private function applyFilterFieldsPurchaseReq($specialFilters)
    {
        $applyFilterFieldsPurchaseReq = "";
        if (is_array($specialFilters['purchase_req'])) {
            if (($specialFilters['purchase_req'][0] === "1") && ($specialFilters['purchase_req'][1] === "0")) {
                $applyFilterFieldsPurchaseReq = $applyFilterFieldsPurchaseReq . " AND purchase_req IN ('{$specialFilters['purchase_req'][0]}','{$specialFilters['purchase_req'][1]}')";
            } elseif ($specialFilters['purchase_req'][0] === "1") {
                $applyFilterFieldsPurchaseReq = $applyFilterFieldsPurchaseReq . " AND purchase_req IN ('{$specialFilters['purchase_req'][0]}')";
            } elseif ($specialFilters['purchase_req'][0] === "0") {
                $applyFilterFieldsPurchaseReq = $applyFilterFieldsPurchaseReq . " AND purchase_req IN ('{$specialFilters['purchase_req'][0]}')";
            }
        }
        return $applyFilterFieldsPurchaseReq;
    }

    /**
     * @param string $column
     * @param int $value
     * @param string $operator
     * @return string
     */
    private function applyRangeFilter(string $column, int $value, string $operator) : string
    {
        return " AND {$column} {$operator} {$value} ";
    }

    /**
     * SKU filter Forneedor da Ultima Compra
     *
     */
    private function applyFilterFieldFornUltCompra($specialFilters)
    {
        $applyFilterFieldFornUltCompra = "";
        if ($specialFilters['last_purchase_supplier'] != "") {
            $applyFilterFieldFornUltCompra = $applyFilterFieldFornUltCompra . " AND last_purchase_supplier LIKE '%{$specialFilters['last_purchase_supplier']}%'";
        }

        return $applyFilterFieldFornUltCompra;
    }

    private function prepareOrder($Orders)
    {
        if(!array_key_exists('id_sku_pk', $Orders)){
            $Orders['id_sku_pk'] = 'asc'; 
        }
        end($Orders);
        $End = key($Orders);
        $OrderQuery = " ORDER BY ";
        foreach ($Orders as $Columns => $ColOrder) {
            if (strtolower($Columns) == 'cod_item_pk') {
                $OrderQuery .= "length({$Columns}) {$ColOrder}, {$Columns} {$ColOrder}";
                if ($End != $Columns) {
                    $OrderQuery .= ", ";
                }
                continue;
            }

            $OrderQuery .= "{$Columns} {$ColOrder}";
            if ($End != $Columns) {
                $OrderQuery .= ", ";
            }
        }
        return $OrderQuery;
    }

    private function defaultOrder()
    {
        return " ORDER BY sit_sku desc, id_sku_pk";
    }

    private function prepareCols($Columns)
    {
        $ColsArray = array();
        foreach ($Columns as $key => $Name) {
            $PrintName = strtoupper($Name);
            switch (strtolower($Name)) {
                case 'qty_altered':
                    $type = 'input-text';
                    break;
                case 'date_arrival_altered':
                    $type = 'input-date';
                    break;
                case 'obs':
                    $type = 'input-textarea';
                    break;
                case 'ind_excess':
                    $type = 'ind_excess';
                    break;
                case 'inv_level':
                    $type = 'img';
                    break;
                case 'inv_total':
                    $type = 'text-modal';
                    break;
                case 'purchase_order':
                    $type = 'order';
                    break;
                case 'desc_item':
                    $type = 'desc_item';
                    break;
                case 'cod_estab_fk':
                case 'default_local':
                    $type = 'max_width_100';
                    break;
//                case 'out_week_month_percent_avg':
//                    $type = 'variacao_consumo';
//                    break;
                default:
                    $type = 'text';
                    break;
            }

            Redis::Set(USERSESSIONID ."session:sku:list:cols:type:$key", $type);
            Redis::Set(USERSESSIONID ."session:sku:list:cols:order:$key", $this->getColOrder($PrintName));

            $ColsArray[$PrintName] = [
                'type' => $type,
                'order' => $this->getColOrder($PrintName)
            ];
        }

        return $ColsArray;
    }

    private function getColOrder($ColName)
    {
        if (isset($this->post['order']) && $this->post['order']) {
            $Orders = $this->post['order'];
        } elseif (Redis::HGet(USERSESSIONID . ':session:sku:list:order-data')) {
            $Orders = Redis::HGet(USERSESSIONID . ':session:sku:list:order-data');
            $ColName = strtolower($ColName);
        } else {
            return null;
        }

        $KeySearch = key_exists($ColName, $Orders);
        if ($KeySearch) {
            return $Orders[$ColName];
        }
    }

    private function unsetDefaults($Post)
    {
        unset($Post['chkvetor'], $Post['chk_check_all_cat_sistema'], $Post['filter_temp'], $Post['radio-exibicao'], $Post['date_last_in'], $Post['data_alterada_chegada'], $Post['order'],
            $Post['inv_available'], $Post['inv_days'], $Post['purchase_req'], $Post['purchase_order'], $Post['qty_inv_origin'],
            $Post['qty_excess_next_in'], $Post['out_last_30d'], $Post['range_out_last_30d'], $Post['range_qty_forecast'], $Post['qty_forecast'], $Post['last_purchase_supplier']);

        if (isset($Post['chk_check_all_exibicao'])) {
            unset($Post['chk_check_all_exibicao']);
        }

        return $Post;
    }

    private function getFooterCols()
    {
        return "
            COUNT(*),
            sum(inv_available) as inv_available,
            sum(inv_available_money) as inv_available_money,
            sum(inv_total) as inv_total,
            sum(inv_total_money) as inv_total_money,
            sum(qty_forecast) as qty_forecast,
            sum(QTY_FORECAST_R3) as qty_forecast_r3,
            sum(qty_min_out) as qty_min_out,
            sum(QTY_MIN_OUT_ORI) as qty_min_out_ori,
            sum(qty_pp) as qty_pp,
            sum(qty_target) as qty_target,
            sum(qty_max_out) as qty_max_out,
            sum(QTY_MAX_OUT_ORI) as qty_max_out_ori,
            sum(qty_excess) as qty_excess,
            sum(VL_EXCESS) as vl_excess,
            sum(QTY_EXCESS_NEXT_IN) as qty_excess_next_in,
            avg(avg_3m) as avg_3m,
            avg(avg_6m) as avg_6m,
            avg(avg_12m) as avg_12m,
            sum(qty_min_erp) as qty_min_erp,
            sum(qty_max_erp) as qty_max_erp,
            sum(qty_pp_erp) as qty_pp_erp,
            sum(qty_avg_cons_erp) as qty_avg_cons_erp,
            sum(out_last_30d) as out_last_30d,
            sum(out_yesterday) as out_yesterday,
            sum(out_yesterday_num_dev) as out_yesterday_num_dev,
            sum(out_month_minus12) as out_month_minus12,
            sum(out_month_minus11) as out_month_minus11,
            sum(out_month_minus10) as out_month_minus10,
            sum(out_month_minus9) as out_month_minus9,
            sum(out_month_minus8) as out_month_minus8,
            sum(out_month_minus7) as out_month_minus7,
            sum(out_month_minus6) as out_month_minus6,
            sum(out_month_minus5) as out_month_minus5,
            sum(out_month_minus4) as out_month_minus4,
            sum(out_month_minus3) as out_month_minus3,
            sum(out_month_minus2) as out_month_minus2,
            sum(out_month_minus1) as out_month_minus1,
            sum(out_current_month) as out_current_month,
            sum(qty_next_in) as qty_next_in,
            sum(qty_last_in) as qty_last_in,
            sum(qty_last_out) as qty_last_out,
            sum(days_no_out) as days_no_out,
            sum(last_purchase_price) as last_purchase_price,
            sum(last_purchase_qty) as last_purchase_qty,
            sum(purchase_req) as purchase_req,
            sum(purchase_order) as purchase_order,
            sum(purchase_sugg) as purchase_sugg,
            avg(service_level_real) as service_level_real,
            avg(service_level_target) as service_level_target
        ";
    }

    private function prepareSetOffset($Page, $Limit)
    {
        return ($Page - 1) * $Limit;
    }

    /**
     * @return string
     */
    private function recoveryFilter()
    {
        return Redis::Get(USERSESSIONID . ':session:sku:list:filter-temp') && Request::verifyReferer('sku/list') ? Redis::Get(USERSESSIONID . ':session:sku:list:filter-temp') : 'false';
    }

    private function prepareTerms($objTerms)
    {
        return $objTerms->where . $objTerms->company . $objTerms->isSupplier . $objTerms->fields . $objTerms->specialFilters . $objTerms->date . $this->prepareSearch($this->Post);
    }

    private function getPrepareFilters($post)
    {
        $this->prepareFilters($post);

        return [
            'data' => true,
            'json' => true
        ];
    }

    // private function removeColumns($columns)
    // {
    //     $replaces = [
    //         "out_week_month_percent_avg", 
    //         "total_out_last_month_avg", 
    //         "total_out_last_half_month_avg", 
    //         "total_out_last_week_avg", 
    //         "total_out_last_week" 
    //     ];
        
    //     if (!is_array($columns)) {
    //         $columns = array_map("trim", explode(",", $columns));
    //     }

    //     foreach ($replaces as $replace) {
    //         dd($columns);
    //         dd($columns[$replace]);
    //         unset($columns[$replace]);
    //     }

    //     return implode(", ", $columns);
    // }

    private function removeColumns($columns)
    {
        $replaces = ["out_week_month_percent_avg", "total_out_last_month_avg", "total_out_last_half_month_avg", "total_out_last_week_avg", "total_out_last_week" ];
        $is_array = true;

        if (!is_array($columns)) {
            $columns = array_map("trim", explode(",", $columns));
            $is_array = false;
        }
        
        foreach ($replaces as $replace) {
            if ( in_array($replace, $columns) ) {
                $key = array_search($replace, $columns);
                unset($columns[$key]);
            }
        }

        return !$is_array ? implode(", ", $columns) : $columns;
    }
}
