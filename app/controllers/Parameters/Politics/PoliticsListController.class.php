<?php

class PoliticsListController
{
    private $view_data;
    private $post;
    private $user;
    private $politicsListService;
    private $checkBoxes;

    public function __construct()
    {
        $this->user = Login::getUserSession();
        $this->politicsListService = new PoliticsListService($this->user);
        $this->post = Request::post();
        $this->checkBoxes = [
            'LT_PLANNER',
            'LT_BUYER',
            'LT_SUPPLIER',
            'LT_RECEIVING',
            'LT_OTHERS',
            'DAYS_MIN',
        ];
    }

    /**
     * <b>getList</b>
     *
     * @return ARRAY $view_data all data used in view.
     */
    public function getList()
    {
        return $this->view_data;
    }

    /**
     * <b>getTable</b>
     * Build the politics table array.
     *
     * @return array
     */
    public function getTable()
    {
        $this->post = Request::post();
        $this->view_data['ajax'] = true;
        $order = (isset($this->post['order']) && $this->post['order'] ? $this->prepareOrder($this->post['order']) : "");
        $offset = $this->prepareSetOffset($this->post['page'], $this->post['limit']);
        $search = $this->prepareSearch($this->post);

        $this->view_data['table'] = $this->politicsListService->paginate(
            $offset,
            $this->post['limit'],
            $order,
            $search
        );

        $this->formatRows();

        $this->view_data['raw'] = $this->politicsListService->select(
            $order,
            $search
        )['data'];

        $this->view_data['footer']['skus'] = $this->sumQuantity($this->view_data['raw']);


        $this->view_data['table']['columns'] = $this->prepareCols($this->view_data['table']['columns']);
        Redis::Set(USERSESSIONID . ':session:politics:list:cols', json_encode($this->view_data['table']['columns']));

        $this->view_data['footer']['count'] = count($this->view_data['raw']);

        $this->view_data['user'] = $this->user;

        $this->view_data['footer']['pages'] = Utilities::calculatePages(
            $this->view_data['footer']['count'],
            $this->post['limit']
        );

        return $this->view_data;
    }


    /**
     * <b>show</b>
     * Retorna dados de uma política existente.
     * @return ARRAY $view_data all data used in view.
     */
    public function show()
    {
        $id = Request::get('politicsId');
        if (!$this->checkIfPoliticsIdIsValid($id)) {
            return [
                'json' => true,
                'politics' => false,
            ];
        }

        return [
            'json' => true,
            'politics' => $this->politicsListService->show($id, $this->user)
        ];
    }

    /**
     * <b>new</b>
     * Salva uma nova política.
     * @return ARRAY $view_data all data used in view.
     */
    public function new()
    {
        $this->formatData();
        $this->formatUpdate();
        return [
            'json' => true,
            'politics' => $this->politicsListService->save($this->post, $this->user)
        ];
    }

    /**
     * <b>delete</b>
     * Deleta uma ou mais políticas e seus respectivos depósitos.
     * @return ARRAY $view_data all data used in view.
     */
    public function delete()
    {
        return [
            'json' => true,
            'politics' => $this->politicsListService->delete($this->post['ID_PROFILE_PK'], $this->user)
        ];
    }

    /**
     * <b>duplicate</b>
     * Deleta uma ou mais políticas e seus respectivos depósitos.
     * @return ARRAY $view_data all data used in view.
     */
    public function duplicate()
    {
        $result = [];
        foreach ($this->post['ids'] as $id) {
            $row = $this->politicsListService->show($id, $this->user);
            $row['data']['DESC_PROFILE'] = $row['data']['DESC_PROFILE'].' (1)';
            $row['data']['cod_local'] = $row['cod_local_selected'];
            array_push($result, $this->formatUpdate($row['data']));
        }

        $result = $this->politicsListService->duplicate($result, $this->user);
        if (in_array('false', $result, true)) {
            return [
                'json' => true,
                'politics' => false
            ];
        }

        return [
            'json' => true,
            'politics' => true
        ];
    }


    /**
     * <b>deposits</b>
     * Retorna os depósitos disponíveis
     * @return ARRAY $view_data all data used in view.
     */
    public function deposits()
    {
        $fields = new Fields();
        return [
            'json' => true,
            'deposits' => $fields->getFields()['Deposit']
        ];
    }

        /**
     * <b>deposits</b>
     * Retorna os depósitos disponíveis
     * @return ARRAY $view_data all data used in view.
     */
    public function calendar()
    {
        $fields = new Fields();
        return [
            'json' => true,
            'calendar' => $fields->getFields()['Calendar']
        ];
    }

    /**
     * <b>update</b>
     * Edita uma política existente.
     * @return ARRAY $view_data all data used in view.
     */
    public function update()
    {
        $this->formatData();
        $this->post['SIT_ONLY_FIRST_PARC'] = isset($this->post['SIT_ONLY_FIRST_PARC']) ? 1 : 0;
        $this->post['SIT_MIN_MAX_ADD_ERP'] = isset($this->post['SIT_MIN_MAX_ADD_ERP']) ? 1 : 0;
        $this->post['M_CALENDAR'] = isset($this->post['M_CALENDAR']) ? 1 : 0;
        $this->post['M_LT'] = isset($this->post['M_LT']) ? 1 : 0;

        return [
            'json' => true,
            'politics' => $this->politicsListService->update($this->post, $this->user)
        ];
    }

    private function formatUpdate($data = '')
    {
        if ($data == '') {
            $this->post = $this->formatUpdateRow($this->post);
            return;
        }

        return $this->formatUpdateRow($data);
    }

    private function formatUpdateRow($row)
    {
        $row['COD_PROFILE'] = isset($row['COD_PROFILE']) ? "'{$row['COD_PROFILE']}'" : "''";
        $row['SCHEDULE'] = isset($row['SCHEDULE']) ? "'{$row['SCHEDULE']}'" : "''";
        $row['DESC_PROFILE'] = isset($row['DESC_PROFILE']) ? "'{$row['DESC_PROFILE']}'" : "''";
        $row['SIT_ONLY_FIRST_PARC'] = isset($row['SIT_ONLY_FIRST_PARC']) ? 1 : 0;
        $row['SIT_MIN_MAX_ADD_ERP'] = isset($row['SIT_MIN_MAX_ADD_ERP']) ? "'{$row['SIT_MIN_MAX_ADD_ERP']}'" : "'0'";

        foreach ($row as $key => $value) {
            $row[$key] = (is_null($value) || empty($value)) ? "''" : $value;
        }

        return $row;
    }

    /**
     * MÉTODO ESSENCIAL!! O formulário manda diversos campos checkboxes. Primeiro é preciso verificar se o checkbox existe.
     * Se ele existe, ele modifica o campo correspondente.
     * Os campos que sofrem essa alteração são os campos contidos em $this->checkBoxes.
     **/
    private function formatData()
    {
        foreach ($this->checkBoxes as $field) {
            if (isset($this->post[$field.'_CHK'])) {
                $this->post[$field] = -1;
                unset($this->post[$field.'_CHK']);
            }
        }
    }

    //campos que precisam checar se existem DAYS_MIN_CHK, TODOS QUE TIVEREM LT_ e terminarem com _CHK, SIT_ONLY_FIRST_PARC_CHK,

    private function checkIfPoliticsIdIsValid($id)
    {
        if (is_null($id) || !is_numeric($id)) {
            return false;
        }

        return true;
    }

    /**
     * <b>formatRows</b>
     * Formata os dados para contruir a tabela. Note que para construir a coluna "Mínimo", é necessário concatenar 4 informações: DAYS_MIN, SERVICE_LEVEL, MAX_LIMIT_BOTTOM e  MIN_LIMIT_BOTTOM
     */
    private function formatRows()
    {
        foreach ($this->view_data['table']['data'] as $key => $row) {
            unset($this->view_data['table']['data'][$key]['RNUM']);
            $this->view_data['table']['data'][$key]['LAST_DATE'] = Date::translateDate($this->view_data['table']['data'][$key]['LAST_DATE']);
            $this->view_data['table']['data'][$key]['NEXT_DATE'] = Date::translateDate($this->view_data['table']['data'][$key]['NEXT_DATE']);
            $this->view_data['table']['data'][$key]['SIT_MIN_MAX_ADD_ERP'] = $this->getTextLang(
                'add_min_max_erp',
                (int) $this->view_data['table']['data'][$key]['SIT_MIN_MAX_ADD_ERP']
            );
        }
    }

    private function prepareSearch($Post)
    {
        if (!isset($Post['searchCol']) || !$Post['searchValue']) {
            $SearchQuery = null;
        } else {
            $SearchQuery = " AND LOWER({$Post['searchCol']}) LIKE '%{$Post['searchValue']}%'";
        }

        /**
         * Recalcula o footer caso seja necessário
         */
        if ($Post['recalcFooter']) {
            $Cols = $this->prepareCols(json_decode(Redis::Get(USERSESSIONID . ':session:politics:list:cols'), true));
        }

        return $SearchQuery;
    }

    private function sumQuantity($raw)
    {
        $result = 0;
        foreach ($raw as $row) {
            foreach ($row as $key => $value) {
                if ($key != 'QUANTIDADE') {
                    continue;
                }

                $result = $result + (float)$value;
            }
        }

        return $result;
    }

    private function prepareOrder($Orders)
    {
        end($Orders);
        $End = key($Orders);
        $OrderQuery = "ORDER BY ";
        foreach ($Orders as $Columns => $ColOrder) {
            $OrderQuery .= "{$Columns} {$ColOrder}";
            if ($End != $Columns) {
                $OrderQuery .= ", ";
            }
        }
        return $OrderQuery;
    }

    private function prepareSetOffset($Page, $Limit)
    {
        return ($Page - 1) * $Limit;
    }

    private function prepareCols($columns)
    {
        $result = array();
        foreach ($columns as $name) {
            $printname = strtoupper($name);
            if ($printname == 'RNUM') {
                continue;
            }

            $result[$printname] = [
                'order' => $this->getColOrder($printname)
            ];
        }

        return $result;
    }

    private function getColOrder($colName)
    {
        if (isset($this->post['order']) && $this->post['order']) {
            $orders = $this->post['order'];
        } elseif (Redis::Get(USERSESSIONID . ':session:approvals:list:order-data')) {
            $orders = (array)json_decode(Redis::Get(USERSESSIONID . ':session:approvals:list:order-data'));
            $colName = strtolower($colName);
        } else {
            return null;
        }

        if ($orders) {
            $keySearch = key_exists($colName, $orders);
        }

        if ($keySearch) {
            return $orders[$colName];
        }
    }

    private function getTextLang($context, $field)
    {
        global $lang;
        return $lang['politics'][$context][$field];
    }
}