<?php

class CalendarController extends AbstractController
{
    private $session;
    private $service;

    public function __construct()
    {
        $this->session = Login::getUserSession();
        $this->service = new CalendarService($this->session['id_company_pk']);
    }

    public function getCalendarData()
    {
        $calendar_id = Request::get('id');

        return [
            'calendar_info' => $this->service->getCalendarInfo($calendar_id),
            'calendar_dates' => $this->service->getCalendarDates($calendar_id),
            'json' => true
        ];
    }

    public function insertCalendars()
    {
        $post = Request::post();

        if (!$this->newCalendar($post)) {
            return $this->returnJson(false);
        }

        return $this->returnJson(true);
    }

    public function deleteCalendar()
    {
        $post = Request::post();

        $calendars = $post['ids'];

        foreach ($calendars as $calendar_id) {
            if (!$this->service->deleteCalendar($calendar_id)) {
                return $this->returnJson(false);
            }
        }

        return $this->returnJson(true);
    }

    public function duplicateCalendars()
    {
        $post = Request::post();

        $calendars = $post['ids'];

        foreach ($calendars as $calendar_id) {
            if (!$this->duplicateCalendar($calendar_id)) {
                return $this->returnJson(false);
            }
        }

        return $this->returnJson(true);
    }

    public function updateCalendars()
    {
        $post = Request::post();

        if (!$this->service->deleteCalendarDates($post['calendar_id'])) {
            return $this->returnJson(false);
        }

        if (!$this->service->insertDates($post['calendar_id'], $post['dates'])) {
            return $this->returnJson(false);
        }

        if (!$this->service->updateInfos($post['calendar_id'], $post['desc'])) {
            return $this->returnJson(false);
        }

        return $this->returnJson(true);
    }

    private function newCalendar($calendar_data)
    {
        $new_calendar_id = $this->service->newCalendarID();

        if (!$new_calendar_id) {
            return false;
        }

        if (!$this->service->insertDates($new_calendar_id, $calendar_data['dates'])) {
            return false;
        }

        if (!$this->service->insertInfos($new_calendar_id, $calendar_data['desc'])) {
            return false;
        }

        return true;
    }

    private function duplicateCalendar($calendar_id)
    {
        $calendar_info = $this->service->getCalendarInfo($calendar_id);

        $duplicated_calendar_data['dates'] = $this->service->getCalendarDates($calendar_id);
        $duplicated_calendar_data['desc'] = $calendar_info['DESC_CALENDAR'] . ' - 2';

        return $this->newCalendar($duplicated_calendar_data);
    }

}
