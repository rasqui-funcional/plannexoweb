<?php

class CalendarListController
{
    private $post;
    private $session;
    private $service;

    public function __construct()
    {
        $this->session = Login::getUserSession();
        $this->service = new CalendarListService($this->session['id_company_pk']);
        $this->session_key = USERSESSIONID . ':calendars:';
        $this->post = Request::post();
    }

    /**
     * <b>getList</b>
     *
     * @return ARRAY $view_data all data used in view.
     */
    public function showList()
    {
        return true;
    }

    /**
     * <b>getTable</b>
     * Build the politics table array.
     *
     * @return array
     */
    public function getTable()
    {
        $order = $this->getOrder($this->post['order']);
        $header_cols = $this->getHeaderCols();
        $total_itens = $this->service->getTotalItems();

        return [
            'data_table' => $this->service->getData($this->post['page'], $this->post['limit'], $order, $this->post['searchValue']),
            'header_cols' => $header_cols,
            'order' => $this->post['order'],
            'footer' => [
                'data' => false,
                'count' => $total_itens,
                'pages' => Utilities::calculatePages($total_itens, $this->post['limit']),
            ],
            'ajax' => true,
        ];
    }

    private function getHeaderCols()
    {
        return [
            'id_calendar_pk',
            'desc_calendar',
            'avg_days_interval',
            'total_skus',
            'dt_receiving_pk'
        ];
    }

    private function getOrder($order)
    {
        if($order) {
            return $this->service->setOrder($order);
        } else {
            return null;
        }        
    }

}
