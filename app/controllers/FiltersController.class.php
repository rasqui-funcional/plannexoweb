<?php

/**
 * Controller criada para administrar os Filtros.
 */
class FiltersController extends Fields
{
    private $ViewData;
    private $FilterService;

    public function __construct()
    {
        parent::__construct();
        $this->FilterService = new FilterService();
    }

    /**
     * Executa a função informada pela URL
     */
    public function exec()
    {
        $Function = Request::get('function');
        $this->$Function();

        $this->ViewData['json'] = true;
        return $this->ViewData;
    }

    private function deleteFilter()
    {
        $Post = Request::post();
        $this->ViewData['error'] = $this->lang['filtro']['error_deteling_filter'];

        $deleteFilter = $this->FilterService->delete($Post['filter_id']);
        if ($deleteFilter) {
            $this->ViewData['data'] = "Filtro " . $Post['filter_nome'] . " foi deletado.";
            unset($this->ViewData['error']);
        }
    }

    private function updateFilter()
    {
        $Post = Request::post();
        $this->ViewData['error'] = $this->lang['filtro']['error_update_filter'];

        $updateFilter = $this->FilterService->update($Post);
        if ($updateFilter) {
            $this->ViewData['data'] = sprintf( $this->lang['filtro']['filter_updated'], $Post['filter_nome'] );
            unset($this->ViewData['error']);
        }
    }

    private function getFilterById()
    {
        $Post = Request::post();
        $this->ViewData['error'] = $this->lang['filtro']['filter_not_found'];

        $Select = $this->FilterService->get($Post["filter_id"]);
        if ($Select) {
            $this->ViewData['data'] = $Select;
            unset($this->ViewData['error']);
        }
    }

    private function saveFilter()
    {
        $post = Request::post();
        $this->ViewData['error'] = $this->lang['filtro']['error_saving_filter'];

        $saveFilter = $this->FilterService->save($post);
        if ($saveFilter) {
            $this->ViewData['message'] = sprintf( $this->lang['filtro']['filter_updated'], $post['filter_nome'] );
            $this->ViewData['data'] = Fields::getSelectFilter($post['filter_tela_id']);
            unset($this->ViewData['error']);
        }
    }
}
