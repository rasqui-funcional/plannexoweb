<?php

/**
 * <b>InfoSKUController.class:</b>
 * Classe responsável por entregar os dados necessários para o preenchimento da tela de Informação do SKU.
 *
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class InfoSKUController
{
    private $ViewData;

    /**
     * <b>getHeader</b>
     * Recupera os dados necessários para o preenchimento do cabeçalho. Parâmetros necessários: id(int) do sku.
     * @return JSON
     */
    public function getHeader()
    {
        $id = Request::get('skuId');

        $this->ViewData['json'] = true;

        if (!$this->validateMethod($id, $_SESSION)) {
            return $this->ViewData;
        }

        $result = $this->getHeaderQuery($id);

        $this->ViewData['header'] = [
            'item' => $result['COD_ITEM_PK'],
            'description' => $result['DESC_ITEM'],$result['COD_ESTAB_PK'],//usado na aba ERP
            'estab' => $result['DESC_ESTAB'],
            'curve' => $result['CURVE_ABC'].$result['CURVE_PQR'].$result['CURVE_XYZ'].$result['CURVE_123'],
            'unity' => $result['DESC_STD_UNIT'],
            'politics' => $result['COD_PROFILE'],
            'category' => $result['CATEGORY'],
            'sin' => $result['INV_LEVEL'],
            'status' => $this->checkStatus($result['SIT_SKU']),
            'status_erp' => $this->checkStatus($result['SIT_SKU_ERP']),
        ];

        $this->ViewData['next_installment'] = [
            'arrear_days' => $this->checkArrearDays($result['PP_ATRASO']),
            'quantity' => $result['PP_QTY_ALTERED'],
            'total' => $this->formatField($result['PP_VAL_QTY_ALTERED'], 'Currency'),
        ];

        $this->ViewData['orders'] = [
            'ordens_atrasadas' => $result['ORDENS_ATRASADAS'],
            'fornecedor' => $result['fornecedor'],
            'qtde' => $result['QTDE'],
            'codigo' => $result['CODIGO'],
            'atraso' => $result['ATRASO'],
        ];

        return $this->ViewData;
    }

    /**
     * <b>formatField</b>
     * Formata o valor recebido com a ajuda da classe Utilities
     * @param int value
     * @param int method
     * @return string
     */
    public function formatField($value, $method)
    {
        return Utilities::$method($value);
    }


    /**
     * <b>checkArrearDays</b>
     * Checa a validade do campo 'dias de atraso', e formata caso ele seja menor que zero
     * @param int value
     * @return int
     */
    public function checkArrearDays($value)
    {
        if (!is_numeric($value) || $value < 0) {
            return 0;
        }

        return $value;
    }

    /**
     * <b>getHeaderQuery</b>
     * Retorna o json com a query que preenche o cabeçalho
     * @param int skuId
     * @return array
     */
    private function getHeaderQuery($skuId)
    {
        if (empty($skuId) || is_null($skuId)) {
            return [];
        }
        $session= Login::getUserSession();
        $columns =' id_company_fk,
                    cod_item_pk,
                    desc_item,
                    cod_estab_pk,
                    desc_estab,
                    curve_abc,
                    curve_pqr,
                    curve_xyz,
                    curve_123,
                    desc_std_unit,
                    cod_profile,
                    category,
                    inv_level,
                    sit_sku,
                    sit_sku_erp,
                    pp_qty_altered,
                    pp_val_qty_altered,
                    pp_atraso,
                    ordens_atrasadas,
                    oa_desc_supplier as "fornecedor",
                    oa_qty_pending as "qtde",
                    oa_num_order "codigo",
                    oa_atraso as "atraso"';
        $table =' vw_scm_info_sku';
        $terms = "WHERE id_company_fk={$session['id_company_pk']} AND id_sku_pk={$skuId}";

        $result = DB::Select($table, $columns, $terms);
        return $result[0];
    }


    /**
     * <b>checkStatus</b>
     * Formata o campo de status do SKU, que no banco é boolean.
     * @param bool status
     * @return string
     */
    private function checkStatus($status)
    {
        if (!isset($status) || is_null($status)) {
            return '';
        }
        return $status == 0 ? $this->lang['info_sku']['status_inativo'] : $this->lang['info_sku']['status_ativo'];
    }
}
