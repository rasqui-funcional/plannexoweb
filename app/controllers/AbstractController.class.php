<?php

abstract class AbstractController
{
     /**
     * returnJson
     * A lazzy function to return json and checker data. 
     * 
     * @param  BOOLEAN $data_check
     * @return ARRAY
     */
    protected function returnJson($data_check)
    {
        return [
            'data' => $data_check,
            'json' => true
        ];
    }
}
