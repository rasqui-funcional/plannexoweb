<?php

/**
 * <b>SuggestService</b>
 * Controller responsible for managing Suggest methods
 *
 * @copyright (c) 2018 | Bionexo
 */
class TestSuggestController extends Fields
{
    const SUGGEST_LIST_KEY = USERSESSIONID . ':log:suggest:result';

    private $ViewData;
    private $Query;
    private $Post;
    private $Order;
    private $session;
    private $suggestService;
    private $auditPrepareDataService;
    private $indicators = [];
    private $columnsToUnset = ['sit_urgent', 'sit_analysed', 'sit_saved'];
    private $updColsToUnset = ['sit_analysed', 'id_sug_pk', 'id_sku_pk', 'DATE_ARRIVAL_ALTERED', 'OBS', 'cod_item_pk'];
    private $groups = [];

    public function __construct()
    {
        parent::__construct();
        $this->session = Login::getUserSession();
        $this->groups = $this->getNewDinamicFieldsGrouping(2);
        $this->suggestService = new SuggestService($this->groups['inactive']);
        $this->auditPrepareDataService = new AuditPrepareData();
        $this->indicators = [
            'urgent',
            'upcoming',
        ];
    }

    /**
     * <b>getFormData</b>
     * Get the data from database to fill suggest/filters.
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function getFormData()
    {
        $ViewData = parent::getFields();
        $this->suggestService->unsetColumnsInactive(parent::getTableColumns('vw_scr_scm420g_sug'));

        $ViewData['Cols'] = $this->unsetFilterColumns($this->suggestService->getColumns(), $this->columnsToUnset);
        $ViewData['select_filtro'] = Fields::getSelectFilter('SCM420G');
        $ViewData['filter_temp'] = $this->recoveryFilter();
        return $ViewData;
    }

    /**
     * <b>getList</b>
     * Receive the post from suggest/filters and prepares query for save in cache(Redis).
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function getList()
    {
        $Post = Request::post();
        if (!$Post) {
            Request::redirect('suggest/filters');
        }

        if (array_key_exists('sit_urgent', $Post) && !array_key_exists('num_parc_pk', $Post)) {
            $Post['num_parc_pk'] = 1;
        }
        Redis::Set(USERSESSIONID . ':session:suggest:post', json_encode($Post));

        $display_columns = $this->suggestService->prepareDisplayColumns($Post['chkvetor']);
        Redis::Set(USERSESSIONID . ':session:suggest:viewdata:cols', json_encode($this->prepareCols($display_columns)));

        $ViewData['SearchFields'] = $this->groups['active'];
        return $ViewData;
    }

    /**
     * Receive the get/param from suggest/filters and prepares query for save in cache(Redis).
     *
     * @return ARRAY $ViewData all data used in view.
     * @author Celso Lopes <caleixo.mooven@bionexo.com>
     */
    public function getCustomSuggest()
    {
        $indicator = strtolower(Request::get('indicators'));
        if (!in_array($indicator, $this->indicators)) {
            Request::home();
        }

        $data = [];
        $data['order'] = null;
        $data['num_parc_pk'] = 1;
        $data['sit_sku'] = ["1"];

        if ($indicator == 'urgent') {
            $data['sit_urgent'] = ["1"];
        }

        if ($indicator == 'upcoming') {
            $data['daterange'] = Date::createRangeDate(7, false, 'd-m-Y');
        }

        $filters = Redis::HGet(USERSESSIONID . ':session:suggest:filters-post');
        if (array_key_exists('post-fields', $filters)) {
            $fields = json_decode($filters['post-fields'], true);
            foreach ($fields as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                $data[$key] = $value;
            }
        }

        $columns = $this->prepareQueryCols(null);
        Redis::Set(USERSESSIONID . ':session:suggest:list:query-cols', $columns);
        Redis::Set(USERSESSIONID . ':session:suggest:post', json_encode($data));

        $ViewData['SearchFields'] = $this->getNewDinamicFieldsGrouping();
        return $ViewData;
    }

    /**
     * <b>getTable</b>
     * Build the suggest table array.
     *
     * @return array
     */
    public function getTable()
    {
        $this->ViewData['json'] = true;
        $this->Post['table'] = Request::post();
        $this->Post['list'] = json_decode(Redis::Get(USERSESSIONID . ':session:suggest:post'), true);
        $this->ViewData['cols'] = json_decode(Redis::Get(USERSESSIONID . ':session:suggest:viewdata:cols'), true);

        $query_columns = $this->suggestService->prepareQueryColumns(null);

        Redis::Set(USERSESSIONID . ':session:suggest:table:post', json_encode($this->Post['table']));
        Redis::Set(USERSESSIONID . ':session:suggest:list:query-cols', $query_columns);
        Redis::Set(USERSESSIONID . ':session:suggest:list:filter-temp', json_encode($this->Post['list']['filter_temp']));

        $this->prepareQuery($this->Post["list"]);

        $Offset = $this->prepareSetOffset($this->Post['table']['page'], $this->Post['table']['limit']);

        $Order = (
        isset($this->Post['table']['order']) && $this->Post['table']['order'] ?
            $this->prepareOrder($this->Post['table']['order']) :
            Redis::Get(USERSESSIONID . ':session:suggest:list:query:order')
        );

        /*
        * Os termos mudarão caso o usuário selecione um filtro dentro da lista de sugestão de compra, ou seja, se ele selecionou curva A na tela de filtro,
        quando estiver na lista e mudar o filtro para curva B, os filtros anteriores deverão ser esquecidos. Antes ele acumulava 'and where' na consulta
        *
        */
        $objTerms = json_decode(Redis::Get(USERSESSIONID . ':session:suggest:list:query-basic'));
        $search_value = strtolower($this->Post['table']['searchValue']);

        $Terms = $this->prepareTerms($objTerms);
        if (!empty($search_value)) {
            $Terms = $objTerms->where . $objTerms->company . $objTerms->isSupplier . $this->prepareSearch($this->Post['table']);
        }
        Redis::Set(':session:suggest:list:csv:terms', $Terms . $Order);

        $this->ViewData['datatable'] = $this->suggestService->getTableOci($Offset, $this->Post['table']['limit'], $query_columns, $Terms, $Order);
        $this->ViewData['datatable'] = $this->suggestService->avoidJoinOci($this->ViewData['datatable']);

        Redis::HSet(
            self::SUGGEST_LIST_KEY,
            ['data' => serialize($this->auditPrepareDataService->suggest($this->ViewData['datatable']))]
        );

        $this->unsetCols();
                
        /*FIM*/
        
        return $this->ViewData;
    }

    /**
     * <b>getHelperList</b>
     * Build the list of estabs/politcs/users/calendars.
     *
     * @return array
     */
    public function getHelperList() {
        $ViewData['json'] = true;
        $type = strtolower(Request::get('typeList'));
        $id = strtolower(Request::get('id'));
        
        $function = 'update'.ucfirst($type);
        /*Pega a descrição dos calendários para mostrar na tela*/
        $item = Redis::HGet("company:{$this->session['id_company_pk']}:{$type}", $id);
        $ViewData['item'] = $item;
        if(!$item) {
            Utilities::$function($this->session['id_company_pk']);
            $ViewData['list'] = Redis::HGet("company:{$this->session['id_company_pk']}:{$type}");
        }
        return $ViewData;
    }

    /**
     * <b>getHelperList</b>
     * Build the list of estabs/politcs/users/calendars.
     *
     * @return array
     */
    public function getLang() {
        $ViewData['json'] = true;

        $ViewData['item'] = json_decode(Redis::Get("company:{$this->session['id_company_pk']}:groupping"), true);
        if(!$ViewData['item']) {
            Utilities::updateGroupping($this->session['id_company_pk']);
            $ViewData['item'] = json_decode(Redis::Get("company:{$this->session['id_company_pk']}:groupping"), true);
        }
        return $ViewData;
    }

    /**
     * <b>getAllHelpers</b>
     * Build the list of estabs/politcs/users/calendars.
     *
     * @return array
     */
    public function getAllHelpers()
    {
        $this->ViewData['json'] = true;

        /*Pega a descrição dos estabs para mostrar na tela*/
        $estabs = Redis::HGet("company:{$this->session['id_company_pk']}:estabs");
        if (empty($estabs)) {
            Utilities::updateEstabs($this->session['id_company_pk'], null);
            $estabs = Redis::HGet("company:{$this->session['id_company_pk']}:estabs");
        }
        $this->ViewData['estabs'] = $estabs;

        /*Pega a descrição das políticas para mostrar na tela*/
        $politics = Redis::HGet("company:{$this->session['id_company_pk']}:politics");
        if(empty($politics)) {
            Utilities::updatePolitics($this->session['id_company_pk'], null);
            $politics = Redis::HGet("company:{$this->session['id_company_pk']}:politics");
        }
        $this->ViewData['politics'] = $politics;
        

        /*Pega a descrição das políticas para mostrar na tela*/
        $users = Redis::HGet("company:{$this->session['id_company_pk']}:users");
        if(!$users) {
            Utilities::updateUsers($this->session['id_company_pk'], null);
            $users = Redis::HGet("company:{$this->session['id_company_pk']}:users");
        }
        $this->ViewData['users'] = $users;
        
        /*Pega a descrição dos calendários para mostrar na tela*/
        $calendars = Redis::HGet("company:{$this->session['id_company_pk']}:calendars");
        if(!$calendars) {
            Utilities::updateCalendars($this->session['id_company_pk']);
            $calendars = Redis::HGet("company:{$this->session['id_company_pk']}:calendars");
        }
        $this->ViewData['calendars'] = $calendars;

        return $this->ViewData;
    }

    public function getTableFooter()
    {
        $this->prepareQuery( json_decode(Redis::Get(USERSESSIONID . ':session:suggest:post'), true));
        
        $request = Request::post();
        if(isset($request['searchValue']) || isset($request['searchCol'])) {
            $request["recalcFooter"] = true;
            $this->prepareSearch($request);
        }
        $this->prepareDataFooter(json_decode(Redis::Get(USERSESSIONID . ':session:suggest:viewdata:cols'), true));

        $data = [];
        $data['data'] = $this->suggestService->prepareData(
            json_decode(Redis::Get(USERSESSIONID . ':session:suggest:list:footer-data'), true)
        );
        $data['count'] = Redis::Get(USERSESSIONID . ':session:suggest:list:footer-count');
        $data['pages'] = Utilities::calculatePages(
            $data['count'],
            $request['limit']
        );

        header('Content-Type: application/json');
        echo json_encode($data); 
        die();
    }

    private function unsetCols()
    {
        if (isset($this->ViewData['cols']['TOTAL_OUT_LAST_MONTH_AVG'])) {
            unset($this->ViewData['cols']['TOTAL_OUT_LAST_MONTH_AVG']);
        }

        if (isset($this->ViewData['cols']['TOTAL_OUT_LAST_HALF_MONTH_AVG'])) {
            unset($this->ViewData['cols']['TOTAL_OUT_LAST_HALF_MONTH_AVG']);
        }

        if (isset($this->ViewData['cols']['TOTAL_OUT_LAST_WEEK_AVG'])) {
            unset($this->ViewData['cols']['TOTAL_OUT_LAST_WEEK_AVG']);
        }

        if (isset($this->ViewData['cols']['TOTAL_OUT_LAST_WEEK'])) {
            unset($this->ViewData['cols']['TOTAL_OUT_LAST_WEEK']);
        }
    }

    /**
     * <b>saveChanges</b>
     * Save all the changes send by suggest/filters.
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function saveChanges()
    {
        $post = Request::post();
        Logger::debug(__CLASS__ . '/' . __METHOD__ . '::salvarDados', $post);

        $ViewData['json'] = true;

        if (isset($post['obj']) && !$this->updateSug($post['obj'])) {
            $ViewData['error'] = 'Erro ao atualizar a coluna analisada.';
            return $ViewData;
        }

        if (isset($post['analyzed']) && !$this->updateAnalized($post['analyzed'])) {
            $ViewData['error'] = 'Erro ao atualizar a coluna analisada.';
            return $ViewData;
        }

        //NÃO mudar o true abaixo para boolean
        $ViewData['data'] = 'true';
        return $ViewData;
    }

    public function getCsvSuggestTable()
    {
        $this->ViewData['ajax'] = true;
        $this->ViewData['error'] = [];

        $objTerms = json_decode(Redis::Get(USERSESSIONID . ':session:suggest:list:query-basic'));

        $table_alias = "g";

        $columns = Redis::Get(USERSESSIONID . ':session:suggest:list:query-cols');
        $columns = $this->removeColumns($columns);

        $columns = "* FROM (SELECT DISTINCT " . $columns;

        $replace_cols = [
            'cod_item_pk' => "{$table_alias}.cod_item_pk",
            'default_local' => "l.cod_local_pk || '-' || l.desc_local AS default_local",
            'cod_estab_fk' => "{$table_alias}.cod_estab_fk",
            'qty_excess' => "{$table_alias}.qty_excess",
            'date_last_calc' => "TO_CHAR(date_last_calc, 'DD/MM/YYYY HH24:MI:SS') AS date_last_calc",
            'ind_excess' => "case
                                when e.qty_excess > 0
                                    then concat(e.desc_estab, concat(' - ', to_char(e.qty_excess)))
                                else '0'
                            end as ind_excess"
        ];

        foreach ($replace_cols as $key => $alter) {
            $columns = str_replace($key, $alter, strtolower($columns));
        }

        $table = "
            vw_scr_scm420g_sug g
                LEFT JOIN erp_supplier s
                       ON last_purchase_supplier = id_supplier_pk
                       AND {$table_alias}.id_company_fk = s.id_company_fk
                LEFT JOIN erp_local l
                       ON g.id_company_fk = l.id_company_fk
                       AND g.default_local = l.cod_local_pk
                       AND g.cod_estab_fk= l.cod_estab_pk
        ";

        $strCodGroupJoin = "";
        for ($i = 2; $i <= 12; $i++) {
            if (strpos($columns, "cod_group{$i}_fk,")) {
                $columns = str_replace("cod_group{$i}_fk", "scm_group{$i}.DESC_GROUP_CONT as cod_group{$i}_fk", $columns);

                $strCodGroupJoin = $strCodGroupJoin . " LEFT JOIN scm_group_cont scm_group{$i}
                ON {$table_alias}.id_company_fk = scm_group{$i}.id_company_fk
                AND scm_group{$i}.COD_GROUP_PK = {$table_alias}.cod_group{$i}_fk
                AND scm_group{$i}.NUM_GROUP_FK = {$i} ";
            }
        }

        $left_joins = "
        LEFT JOIN
        (SELECT
            ESTB.ID_COMPANY_FK,
            ESTB.COD_ESTAB_FK,
            ESTB.COD_ITEM_PK,
            estb.QTY_EXCESS,
            ESTBDESC.DESC_ESTAB
        FROM vw_scr_scm475g_excess_estab estb
        LEFT JOIN SCM_ESTAB estbdesc
            ON ESTB.COD_ESTAB_FK = ESTBDESC.COD_ESTAB_PK
            AND estb.ID_COMPANY_FK = ESTBDESC.ID_COMPANY_FK
        WHERE ESTB.id_company_fk = {$objTerms->company}) e
        ON e.id_company_fk = {$table_alias}.id_company_fk
                AND e.cod_estab_fk != {$table_alias}.COD_ESTAB_FK
                AND e.cod_item_pk = {$table_alias}.COD_ITEM_PK
        {$strCodGroupJoin}";

        $terms = Redis::Get(':session:suggest:list:csv:terms');

        $orderByIndex = strpos($terms, 'ORDER BY', 0);

        $where = substr($terms, 0, $orderByIndex) . ') a ORDER BY ';

        $orderBy = substr($terms, ($orderByIndex + 9));
        $orderBy = explode(',', $orderBy);
        foreach ($orderBy as $key => $value) {
            $orderBy[$key] = 'a.' . trim($value);
        }
        $orderBy = implode(',', $orderBy);

        $terms = $where . $orderBy;

        $this->ViewData['data'] = strpos($terms, 'g.id_company_fk') ? DB::Select($table . $left_joins, $columns, $terms) : '';
        return $this->ViewData;
    }

    private function updateAnalized($analizedData)
    {
        foreach ($analizedData as $item) {
            unset($item['cod_item_pk']);

            $id_sku = $item['id'];
            $data['sit_analysed'] = 0;

            if ($item['value'] == 'true') {
                $data['sit_analysed'] = 1;
            }

            Logger::info(__CLASS__ . 'suggest:preupdate:' . __METHOD__);
            $update = DB::Update('scm_sku', $data, "WHERE id_sku_pk = :id", "id={$id_sku}");
            Logger::info(__CLASS__ . 'suggest:update:' . __METHOD__);

            if (!$update) {
                return false;
            }
        }

        return true;
    }

    private function updateSug($suggestData)
    {
        foreach ($suggestData as $suggest) {
            Logger::debug(__CLASS__ . '/' . __METHOD__ . ':suggest:data', [$suggest]);

            $copyArrayData = $this->copyArrayData($suggest);

            $id_suggest = $suggest['id_sug_pk'];
            Logger::debug(__CLASS__ . ':suggest:id_sug_pk', [$id_suggest]);

            $date = null;
            if ($suggest['DATE_ARRIVAL_ALTERED'] != "") {
                $dt_replace = str_replace("/", "-", $suggest['DATE_ARRIVAL_ALTERED']);
                $date = ", DATE_ARRIVAL_ALTERED = TO_DATE('" . date('d/m/Y', strtotime($dt_replace)) . "','DD/MM/YYYY')";
            }
            Logger::debug(__CLASS__ . ':suggest:prepare-data', [$date]);

            if (isset($suggest['sit_saved']) && $suggest['sit_saved'] == 1) {
                if (!$this->updateAnalizedForce($suggest['id_sku_pk'])) {
                    return false;
                }
            } else {
                $suggest['sit_saved'] = 0;
            }

            Logger::debug(__CLASS__ . ':suggest:sit_saved', [$suggest['sit_saved']]);

            $obs = $this->prepareSaveOBS($suggest['OBS']);

            //$suggest = $this->unsetFilterColumns($suggest, $this->updColsToUnset);
            unset($suggest['sit_analysed'], $suggest['id_sug_pk'], $suggest['id_sku_pk'], $suggest['DATE_ARRIVAL_ALTERED'], $suggest['OBS'], $suggest['cod_item_pk']);

            try {
                Logger::info(__CLASS__ . 'suggest:preupdate:' . __METHOD__);
                $update = DB::Update('SCM_SKU_PURCHASE_SUG', $suggest, "{$date} {$obs} WHERE id_sug_pk = :id", "id={$id_suggest}");
                Logger::info(__CLASS__ . ':suggest:update' . __METHOD__, [$update]);

                Log::new(
                    self::SUGGEST_LIST_KEY,
                    'id_sug_pk',
                    'suggestions:list',
                    'update',
                    (int)$copyArrayData['ID_SUG_PK'],
                    $copyArrayData
                );
            } catch (Exception $exception) {
                Logger::error(__CLASS__ . '::suggest:update', [
                    "Error when try update SUGGEST {$id_suggest} with following response code:{$exception->getCode()} message:{$exception->getMessage()}",
                    $exception->getFile(), $exception->getTrace()
                ]);

                return false;
            }
        }

        return true;
    }

    private function copyArrayData($data)
    {
        $arr = [];

        $arrayCopy = (new ArrayObject($data))->getArrayCopy();
        foreach ($arrayCopy as $key => $value) {
            $arr[strtoupper($key)] = $this->prepareData($key, $value);
        }

        return $arr;
    }

    private function prepareData($item, $value)
    {
        switch (strtolower($item)) {
            case 'date_arrival_altered':
                $date = DateTime::createFromFormat('d/m/Y', $value);
                return $date->format('d-m-y');
                break;

            default:
                return $value;
                break;
        }
    }

    private function updateAnalizedForce($id_sku)
    {
        $update_analized_data[] = [
            'id' => $id_sku,
            'value' => 'true'
        ];

        Logger::info(__CLASS__ . ':suggest:update_analized_force', [$update_analized_data]);

        if (!$this->updateAnalized($update_analized_data)) {
            return false;
        }

        return true;
    }

    private function prepareSaveOBS($OBS)
    {
        if (!$OBS) {
            return null;
        }

        $OBS = trim(strip_tags($OBS));
        $OBS = str_replace("'", '"', $OBS);

        return ", OBS = '" . $OBS . "'";
    }

    private function prepareQueryCols($Columns)
    {
        if (!is_null($Columns)) {
            $OBSKey = array_search('obs', $Columns);
            if ($OBSKey) {
                $Columns[$OBSKey] = 'dbms_lob.substr(obs, 4000, 1 ) as obs';
            }

            if (!array_search('id_sku_pk', $Columns)) {
                $Columns[] = 'id_sku_pk';
            }

            if ((in_array('purchase_order', $Columns) || in_array('date_next_in', $Columns) || in_array(
                        'qty_next_in',
                        $Columns
                    )) && !in_array('risk_supplier', $Columns)) {
                $Columns[] = 'risk_supplier';
            }

            if ((in_array('num_parc_pk', $Columns) && !in_array('purchase_order', $Columns))) {
                $Columns[] = 'purchase_order';
            }

            if (in_array('qty_next_in', $Columns) && !in_array('date_next_in', $Columns)) {
                $Columns[] = 'date_next_in';
            }

            $Columns[] = 'id_sug_pk';
            if (array_search('sit_urgent', $Columns) === false) {
                $Columns[] = 'sit_urgent';
            }

            return DB::setColumns($Columns);
        } else {
            $Columns = DB::getTableCols('vw_scr_scm420g_sug');
            $Columns = str_replace(', obs', ', dbms_lob.substr(obs, 4000, 1 ) as obs', $Columns);
            $Columns .= ', id_sug_pk';

            return $Columns;
        }
    }

    private function prepareSearch($post)
    {
        $objTerms = json_decode(Redis::Get(USERSESSIONID . ':session:suggest:list:query-basic'));
        $search_query = null;
        $terms = null;
        Redis::Set(USERSESSIONID . ':session:suggest:list:header-search', '');
        $search_value = strtolower($post['searchValue']);
        if (!empty($search_value)) {
            $clause = 'LIKE';

            $value = "'%{$search_value}%'";

            if ($post['searchCol'] == 'cod_item_pk') {
                $checkValue = strpos($search_value, ',');
                $clause = 'LIKE';
                $value = "'%{$search_value}%'";
                if ($checkValue != false) {
                    $clause = 'IN';
                    $value = "({$search_value})";
                }
            }

            if (array_key_exists('searchCol', $post) || $post['searchValue']) {
                $search_query = " AND LOWER (g.{$post['searchCol']}) {$clause} {$value}";
                Redis::Set(USERSESSIONID . ':session:suggest:list:header-search', $search_query);
            }
            $terms = $objTerms->where . $objTerms->company . $objTerms->isSupplier;
        } else {
            $terms = $objTerms->where . $objTerms->company . $objTerms->isSupplier . $objTerms->fields . $objTerms->filters . $objTerms->date;
        }

        /**
         * Recalcula o footer caso seja necessário
         */
        if ($post['recalcFooter']) {
            $this->Query = $terms . $search_query;

            $cols = json_decode(Redis::Get(USERSESSIONID . ':session:suggest:list:cols'), true);
            $this->prepareDataFooter($cols);
        }

        return $search_query;
    }

    private function prepareDataFooter($Cols)
    {        
        $table = "vw_scr_scm420g_sug g";
        $QueryFooter = DB::Select($table, $this->getFooterCols(), $this->Query);
        if (!$QueryFooter) {
            return false;
        }

        Redis::Set(USERSESSIONID . ':session:suggest:list:footer-count', $QueryFooter[0]['COUNT(*)']);

        $datafooter = [];
        foreach ($Cols as $ColName => $ColType) {
            $ColName = strtoupper($ColName);
            $datafooter[$ColName] = null;
            if (key_exists($ColName, $QueryFooter[0])) {
                $datafooter[$ColName] = (is_numeric($QueryFooter[0][$ColName])
                    ? round($QueryFooter[0][$ColName], 2) : $QueryFooter[0][$ColName]);
            }
        }

        Redis::Set(USERSESSIONID . ':session:suggest:list:footer-data', json_encode($datafooter));

        return $datafooter;
    }

    private function getFooterCols()
    {
        return "count(*), sum(QTY_ORIGINAL) as qty_original, sum(VAL_ALTERED) as val_altered,
        sum(QTY_FORECAST) as qty_forecast, sum(INV_AVAILABLE) as inv_available, sum(AVAILABLE_MONEY) as available_money,
        sum(QTY_MIN_OUT) as qty_min_out, sum(QTY_TARGET) as qty_target, sum(QTY_MAX_OUT) as qty_max_out,
        sum(OUT_LAST_30D) as out_last_30d, sum(QTY_AVG_CONS_ERP) as qty_avg_cons_erp, sum(AVG_3M) as avg_3m,
        sum(AVG_6M) as avg_6m, sum(AVG_12M) as avg_12m, sum(OUT_YESTERDAY) as out_yesterday,
        sum(OUT_YESTERDAY_NUM_DEV) as out_yesterday_num_dev, avg(DAYS_NO_OUT) as days_no_out,
        sum(OUT_MONTH_MINUS12) as out_month_minus12, sum(OUT_MONTH_MINUS11) as out_month_minus11,
        sum(OUT_MONTH_MINUS10) as out_month_minus10, sum(OUT_MONTH_MINUS9) as out_month_minus9,
        sum(OUT_MONTH_MINUS8) as out_month_minus8, sum(OUT_MONTH_MINUS7) as out_month_minus7,
        sum(OUT_MONTH_MINUS6) as out_month_minus6, sum(OUT_MONTH_MINUS5) as out_month_minus5,
        sum(OUT_MONTH_MINUS4) as out_month_minus4, sum(OUT_MONTH_MINUS3) as out_month_minus3,
        sum(OUT_MONTH_MINUS2) as out_month_minus2, sum(OUT_MONTH_MINUS1) as out_month_minus1,
        sum(OUT_CURRENT_MONTH) as out_current_month, sum(OUT1) as out1, sum(OUT2) as out2, sum(OUT3) as out3,
        sum(OUT4) as out4, sum(OUT5) as out5, sum(OUT6) as out6, sum(QTY_MIN_OUT_ORI) as qty_min_out_ori,
        sum(QTY_PP) as qty_pp, sum(QTY_MAX_OUT_ORI) as qty_max_out_ori, sum(QTY_EXCESS) as qty_excess,
        sum(VL_EXCESS) as vl_excess, sum(QTY_MAX_ERP) as qty_max_erp, sum(QTY_MIN_ERP) as qty_min_erp,
        sum(QTY_PP_ERP) as qty_pp_erp, sum(QTY_FINAL) as qty_final, sum(QTY_NEXT_IN) as qty_next_in,
        sum(QTY_LAST_OUT) as qty_last_out, sum(QTY_LAST_IN) as qty_last_in, sum(INV_TOTAL) as inv_total,
        avg(INV_DAYS) as avg_inv_days, sum(TOTAL_MONEY) as total_money, sum(LAST_PURCHASE_PRICE) as last_purchase_price,
        sum(LAST_PURCHASE_QTY) as last_purchase_qty, avg(SERVICE_LEVEL_TARGET) as service_level_target,
        avg(SERVICE_LEVEL_REAL) as service_level_real, sum(V_IN1) as v_in1, sum(V_IN2) as v_in2, sum(V_IN3) as v_in3,
        sum(V_IN4) as v_in4, sum(V_IN5) as v_in5, sum(V_IN6) as v_in6, sum(INV_REQ) as inv_req,
        sum(PURCHASE_SUGG) as purchase_sugg";
    }

    private function prepareSetOffset($Page, $Limit)
    {
        return ($Page - 1) * $Limit;
    }

    private function prepareQuery($Post)
    {
        /*cod_item_pk sempre é enviado ao backend, mesmo que vazio, por isso foi necessário o if abaixo.
        Se o usuario não digitar nada neste campo, ignoramos o if e o cod_item_pk nem fará parte da query,
        caso contrário, transformamos cod_item_pk em array para que seja tratado pela lógica existente no método DB::setFields() do arquivo DB.class.php
        */
        if ($Post['cod_item_pk'] != '') {
            $Post['cod_item_pk'] = explode(',', $Post['cod_item_pk']);
        }

        $Order = (isset($Post['order']) ? $this->prepareOrder($Post['order']) : $this->defaultOrder());

        if (isset($Post['interval_days']) && !empty($Post['interval_days'])) {
            $endDate = strtotime("+{$Post['interval_days']} day");
            $Post['interval_start'] = date("d-m-Y");
            $Post['interval_end'] = date('d-m-Y', $endDate);
        }

        $Date = '';
        $Date = (isset($Post['interval_start']) && !empty($Post['interval_start'])
            ? $this->prepareDate($Post['interval_start'], $Post['interval_end']) : null);

        if (key_exists('daterange', $Post)) {
            $Date = $this->prepareDate($Post['daterange']['start'], $Post['daterange']['end']);
        }

        $SpecialFilters = $this->applySpecialFilters($Post);

        if (!is_null($Post['order'])) {
            Redis::HSet(USERSESSIONID . ':session:suggest:list:query:order-data', $Post['order']);
        }

        $Post = $this->unsetDefaults($Post);
        $whereFields = [];
        foreach ($Post as $key => $value) {
            $whereFields['g.' . $key] = $value;
            if ($key === 'desc_item' && !empty($value)) {
                unset($whereFields['g.' . $key]);
                $whereFields['LOWER(g.' . $key . ')'] = mb_strtolower("%$value%");
            }

            if ($key === 'cod_item_pk') {
                if (count($value) === 1 && !empty($value)) {
                    unset($whereFields['g.' . $key]);
                    $whereFields['g.' . $key] = "%$value[0]%";
                }
            }
        }

        $supplier = null;
        if (can('supplier')) {
            $supplier = $this->setSupplier();
        }

        $basicTerms = new stdClass();
        $basicTerms->where = "WHERE ";
        $basicTerms->where .= array_key_exists('g.cod_estab_fk', $whereFields) ? "" : $this->setEstabs();
        $basicTerms->where .= " g.id_company_fk = ";
        $basicTerms->company = $this->session['id_company_pk'];
        $basicTerms->isSupplier = $supplier;
        $basicTerms->fields = DB::setFields($whereFields);
        $basicTerms->filters = $this->applyFilterFields($SpecialFilters, $whereFields);
        $basicTerms->date = $Date;

        Redis::Set(USERSESSIONID . ':session:suggest:list:query-basic', json_encode($basicTerms));

        Redis::Set(USERSESSIONID . ':session:suggest:list:query:order', $Order);

        $this->Query = $basicTerms->where
            . $basicTerms->company
            . $basicTerms->isSupplier
            . $basicTerms->fields
            . $basicTerms->filters
            . $basicTerms->date
            . $Order;

        return $this->Query;
    }

    private function setEstabs()
    {
        return !empty($this->session['estab']) ? " g.COD_ESTAB_FK IN({$this->session['estab']}) AND " : '';
    }

    private function setSupplier()
    {
        //return "AND user_id = {$this->session['user_id']} ";
        return "AND cod_group11_fk = '1' ";
    }

    private function prepareDate($startDate, $endDate)
    {
        $dateFrom = Date::translateDate($startDate, '00:00:00');
        $dateTo = Date::translateDate($endDate, '23:59:59');

        $dateQuery = " AND date_arrival_altered between  to_date('{$dateFrom}', 'DD/MM/YYYY HH24:mi:ss') and to_date('{$dateTo}','DD/MM/YYYY HH24:mi:ss') ";
        return $dateQuery;
    }

    private function prepareOrder($Orders)
    {
        end($Orders);
        $End = key($Orders);
        $OrderQuery = "ORDER BY ";
        foreach ($Orders as $Columns => $ColOrder) {
            $OrderQuery .= "{$Columns} {$ColOrder}";
            if ($End != $Columns) {
                $OrderQuery .= ", ";
            }
        }

        return $OrderQuery;
    }

    private function defaultOrder()
    {
        return " ORDER BY RISK_SUPPLIER DESC, id_sku_pk";
    }

    /**
     * <b>prepareCols</b>
     *
     * @param ARRAY
     * @return ARRAY
     */
    private function prepareCols($Columns)
    {
        return array_change_key_case(array_flip($this->setFilterColumns($Columns)), CASE_UPPER);
    }

    /**
     * <b>setFilterColumns</b>
     *
     * @return ARRAY
     */
    private function setFilterColumns($data)
    {
        $data = $this->unsetFilterColumns($data, ['sit_urgent']);
        array_unshift($data, 'sit_analysed', 'sit_saved');

        return $data;
    }

    private function unsetDefaults($Post)
    {
        unset($Post['chkvetor'], $Post['filter_temp'], $Post['radio-exibicao'], $Post['daterange'], $Post['interval_select'], $Post['interval_start'], $Post['interval_end'],
            $Post['data_alterada_chegada'], $Post['order'],
            $Post['page'], $Post['limit'], $Post['inv_days_min'], $Post['inv_days'], $Post['purchase_req'], $Post['num_parc_pk'],
            $Post['purchase_order'], $Post['risk_supplier'], $Post['interval_days']
        );

        if (isset($Post['chk_check_all_exibicao'])) {
            unset($Post['chk_check_all_exibicao']);
        }

        if (array_key_exists('sit_urgent', $Post)) {
            unset($Post['sit_urgent']);
        }

        return $Post;
    }

    private function applySpecialFilters($specialFilters)
    {
        $querySpecialFilters = "";

        //inv_days_min
        if (isset($specialFilters['inv_days'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldsCoverbyDays($specialFilters);
        }

        //sit_urgent
        if (isset($specialFilters['sit_urgent'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldsUrgent($specialFilters);
        }

        //risk_supplier
        if (isset($specialFilters['risk_supplier'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldsRiskSupplier($specialFilters);
        }

        //inv_days
        if (isset($specialFilters['inv_days_min'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldsCoverMinDays($specialFilters);
        }

        //purchase_order
        if (isset($specialFilters['purchase_order'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldsPurchaseOrder($specialFilters);
        }

        //purchase_req
        if (isset($specialFilters['purchase_req'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldsPurchaseReq($specialFilters);
        }

        //num_parc_pk
        if (isset($specialFilters['num_parc_pk'])) {
            $querySpecialFilters = $querySpecialFilters . $this->applyFilterFieldsNumParcPk($specialFilters);
        }

        return $querySpecialFilters;
    }

    private function applyFilterFieldsCoverbyDays($specialFilters)
    {
        $applyFilterFieldsCoverbyDays = "";
        if ($specialFilters['inv_days'] != "") {
            $applyFilterFieldsCoverbyDays = $applyFilterFieldsCoverbyDays . " AND inv_days < '{$specialFilters['inv_days']}'";
        }

        return $applyFilterFieldsCoverbyDays;
    }

    private function applyFilterFieldsCoverMinDays($specialFilters)
    {
        $applyFilterFieldsCoverMinDays = "";
        if (is_array($specialFilters['inv_days_min'])) {
            if ($specialFilters['inv_days_min'][0] === "1") {
                $applyFilterFieldsCoverMinDays = $applyFilterFieldsCoverMinDays . " AND inv_available < qty_min_out";
            }
        }

        return $applyFilterFieldsCoverMinDays;
    }

    private function applyFilterFieldsPurchaseOrder($specialFilters)
    {
        $applyFilterFieldsPurchaseOrder = "";
        if (is_array($specialFilters['purchase_order'])) {
            if (($specialFilters['purchase_order'][0] === "1") && ($specialFilters['purchase_order'][1] === "0")) {
                $applyFilterFieldsPurchaseOrder = $applyFilterFieldsPurchaseOrder . " AND purchase_order IN ('{$specialFilters['purchase_order'][0]}','{$specialFilters['purchase_order'][1]}')";
            } elseif ($specialFilters['purchase_order'][0] === "1") {
                $applyFilterFieldsPurchaseOrder = $applyFilterFieldsPurchaseOrder . " AND purchase_order IN ('{$specialFilters['purchase_order'][0]}')";
            } elseif ($specialFilters['purchase_order'][0] === "0") {
                $applyFilterFieldsPurchaseOrder = $applyFilterFieldsPurchaseOrder . " AND purchase_order IN ('{$specialFilters['purchase_order'][0]}')";
            }
        }

        return $applyFilterFieldsPurchaseOrder;
    }

    private function applyFilterFieldsPurchaseReq($specialFilters)
    {
        $applyFilterFieldsPurchaseReq = "";
        if (is_array($specialFilters['purchase_req'])) {
            if (($specialFilters['purchase_req'][0] === "1") && ($specialFilters['purchase_req'][1] === "0")) {
                $applyFilterFieldsPurchaseReq = $applyFilterFieldsPurchaseReq . " AND purchase_req IN ('{$specialFilters['purchase_req'][0]}','{$specialFilters['purchase_req'][1]}')";
            } elseif ($specialFilters['purchase_req'][0] === "1") {
                $applyFilterFieldsPurchaseReq = $applyFilterFieldsPurchaseReq . " AND purchase_req IN ('{$specialFilters['purchase_req'][0]}')";
            } elseif ($specialFilters['purchase_req'][0] === "0") {
                $applyFilterFieldsPurchaseReq = $applyFilterFieldsPurchaseReq . " AND purchase_req IN ('{$specialFilters['purchase_req'][0]}')";
            }
        }

        return $applyFilterFieldsPurchaseReq;
    }

    private function applyFilterFieldsUrgent($specialFilters)
    {
        $applyFilterFieldsUrgent = "";
        if (is_array($specialFilters['sit_urgent'])) {
            if (($specialFilters['sit_urgent'][0] === "1") && ($specialFilters['sit_urgent'][1] === "0")) {
                $applyFilterFieldsUrgent = $applyFilterFieldsUrgent . " AND sit_urgent IN ('{$specialFilters['sit_urgent'][0]}','{$specialFilters['sit_urgent'][1]}')";
            } elseif ($specialFilters['sit_urgent'][0] === "1") {
                $applyFilterFieldsUrgent = $applyFilterFieldsUrgent . " AND sit_urgent IN ('{$specialFilters['sit_urgent'][0]}')";
            } elseif ($specialFilters['sit_urgent'][0] === "0") {
                $applyFilterFieldsUrgent = $applyFilterFieldsUrgent . " AND sit_urgent IN ('{$specialFilters['sit_urgent'][0]}')";
            }
        }

        return $applyFilterFieldsUrgent;
    }

    private function applyFilterFieldsRiskSupplier($specialFilters)
    {
        $applyFilterFieldsRiskSupplier = "";
        if (is_array($specialFilters['risk_supplier'])) {
            if ($specialFilters['risk_supplier'][0] === "1") {
                $applyFilterFieldsRiskSupplier = $applyFilterFieldsRiskSupplier . " AND risk_supplier IN ('1')";
            }
        }

        return $applyFilterFieldsRiskSupplier;
    }

    private function applyFilterFieldsNumParcPk($specialFilters)
    {
        $applyFilterFieldsNumParcPk = "";

        return (is_numeric($specialFilters['num_parc_pk'])) ? $applyFilterFieldsNumParcPk . " AND num_parc_pk <= {$specialFilters['num_parc_pk']}" : '';
    }

    private function applyFilterFields($specialFilters, array $fields = [])
    {
        /*verifica se a aplicação veio da tela de filtro ou da tela de dashboard. Dependendo de onde vier, os filtros aplicados são diferentes*/
        if (!Request::verifyReferer('dashboard')) {
            return $specialFilters;
        }

        if (key_exists('cod_item_pk', $fields) && empty($fields['cod_item_pk'])) {
            unset($fields['cod_item_pk']);
        }

        if (key_exists('desc_item', $fields) && empty($fields['desc_item'])) {
            unset($fields['desc_item']);
        }

        $new_fields = [];
        foreach ($fields as $field => $value) {
            $new_fields[] = sprintf("AND %s IN('%s')", $field, array_pop($value));
        }

        return $specialFilters . ' ' . implode(' ', $new_fields);
    }

    /**
     * @return string
     */
    private function recoveryFilter()
    {
        return Redis::Get(USERSESSIONID . ':session:suggest:list:filter-temp') && Request::verifyReferer('suggest/list')
            ? Redis::Get(USERSESSIONID . ':session:suggest:list:filter-temp') : 'false';
    }

    private function prepareTerms($objTerms)
    {
        return $objTerms->where . $objTerms->company . $objTerms->isSupplier . $objTerms->fields . $objTerms->filters . $objTerms->date . $this->prepareSearch($this->Post);
    }

    private function removeColumns($columns)
    {
        $replaces = ["out_week_month_percent_avg", "total_out_last_month_avg", "total_out_last_half_month_avg", "total_out_last_week_avg", "total_out_last_week" ];
        $is_array = true;

        if (!is_array($columns)) {
            $columns = array_map("trim", explode(",", $columns));
            $is_array = false;
        }
        
        foreach ($replaces as $replace) {
            if ( in_array($replace, $columns) ) {
                $key = array_search($replace, $columns);
                unset($columns[$key]);
            }
        }

        return !$is_array ? implode(", ", $columns) : $columns;
    }
    /**
     * @return mixed
     * @throws Exception
     */
    public function sugToGroup()
    {
        $ViewData['json'] = true;
        $id_psug_pk = (int)Request::get('id_psug_pk');

        $ViewData['sugtogroup'] = $this->suggestService->getSuggestToGroup($id_psug_pk);

        return $ViewData;
    }

    public function groupInstallments()
    {
        $ViewData['json'] = true;
        $post = Request::post();
        $fields = $this->prepareFieldsGroupInstallments($post);

        if (empty($fields)) {
            $ViewData["group-installments"] = false;
        }

        $id_suggest = (int) $post['id_psug_pk'];

        try {
            Logger::info(__CLASS__ . 'suggest:preupdate:' . __METHOD__);
            $terms = json_decode(Redis::Get(USERSESSIONID . ':session:suggest:list:query-basic'));
            $update = [];

            /** Update num_parc_pk 1 and 2 */
            $update[0] = $this->updateSugParc($id_suggest, $terms, $fields, $post);

            /** Update analised */
            $update[1] = $this->updateSkuAnalised($post, $terms);
            
            $ViewData["group-installments"] = true;
            Logger::info(__CLASS__ . ':suggest:update' . __METHOD__, [$update]);

            return $ViewData;
        
        } catch (Exception $exception) {
            Logger::error(__CLASS__ . '::suggest:update', [
                "Error when try update SUGGEST {$id_suggest} with following response code:{$exception->getCode()} message:{$exception->getMessage()}",
                $exception->getFile(), $exception->getTrace()
            ]);

            $ViewData["group-installments"] = $exception->getMessage();
            return $ViewData;
        }

    }

    private function updateSugParc($id_suggest, $terms, $fields, $post) 
    {
        global $lang;

        $obs = $this->prepareSaveOBS($post['obs']);
        $where = "WHERE id_psug_pk = :id_psug_pk AND id_company_fk = :id_company_fk";
        $parc_string = "id_psug_pk={$id_suggest}&id_company_fk={$terms->company}";

        /** mounth where for log audithor **/
        $whereLog = str_replace(':id_psug_pk', $id_suggest, $where);
        $whereLog = str_replace(':id_company_fk', $terms->company, $whereLog);

        /** Update num_parc_pk 1 */
        $update1 = DB::Update("scm_sku_purchase_sug", $fields, "{$obs} {$where} AND num_parc_pk = :num_parc_pk", $parc_string . "&num_parc_pk=1");

        /** Add log audit infosku num_parc 1 */
        $this->insertAuditLogGroupedInstallment($fields, $whereLog, $post['obs'], $id_suggest, 1);

        /** Update num_parc_pk 2 */
        $obs2 = $lang["suggest"]["grouped_suggestion"];
        $update2 = DB::Update("scm_sku_purchase_sug", ["grouped_installments" => 1, "sit_saved" => 0, "obs" => $obs2],  "{$where} AND num_parc_pk = :num_parc_pk", $parc_string  . "&num_parc_pk=2");

        /** Add log audit infosku num_parc 2 */
        $this->insertAuditLogGroupedInstallment($fields, $whereLog, $obs2, $id_suggest, 2);
        if (!$update1 || !$update2) {
            return false;
        }

        return true;
    }

    private function updateSkuAnalised($post, $terms) 
    {
        return DB::Update(
            "scm_sku", 
            ["sit_analysed" => 1], 
            "WHERE id_company_fk = :id_company_fk AND id_sku_pk = :id_sku_pk", 
            "id_company_fk={$terms->company}&id_sku_pk={$post['id_sku_pk']}"
        );
    }

    private function prepareFieldsGroupInstallments($post)
    {
        $fields = [];
        
        if (!isset($post["id_psug_pk"]) || empty($post["id_psug_pk"])) {
            return "";
        }
        
        if (!isset($post["qty_altered"]) || empty($post["qty_altered"])) {
            return "";
        }
        
        $fields["QTY_ALTERED"] = (int) $post["qty_altered"];
        $fields["DATE_ARRIVAL_ALTERED"] = $this->formatUpdateDate(trim($post["date_arrival_altered"]));
        $fields["GROUPED_INSTALLMENTS"] = 1;
        $fields["SIT_SAVED"] = 1;

        return $fields;
    }

    private function formatUpdateDate($date = "", $format = "d-M-Y")
    {
        return date($format, strtotime($date));
    }

    private function insertAuditLogGroupedInstallment($fields, $where, $obs, $id_suggest, $num_parc)
    {
        $info = DB::SelectRow('SCM_SKU_PURCHASE_SUG','ID_SUG_PK, NUM_PARC_PK, ID_SKU_FK AS ID_SKU_PK', "{$where} AND NUM_PARC_PK = {$num_parc}");
        $data = array_merge($fields, $info, ['OBS' => "$obs", 'ID_PSUG_PK' => $id_suggest]);

        Log::new(
            self::SUGGEST_LIST_KEY,
            'id_sug_pk',
            'suggestions:list',
            'update',
            (int) $info['ID_SUG_PK'],
            $data
        );
    }
}
