<?php

/**
 * ErrorController.class
 * Class abstract to controll the error route.
 *
 * @copyright (c) 2018, Alef Alves | Bionexo
 */
class ErrorController
{
    private $ErrNumber;
    private $ErrUrl;

    /**
      * <b>getMessage</b>
      * Sets url, message and status for error page.
      *
      * @return ARRAY $ViewData all data used in view.
      */
    public function getMessage()
    {
        $this->ErrNumber = Request::get('number');
        $ViewData['error_number'] = $this->ErrNumber;
        $ViewData['url'] = $this->setErrUrl();

        /** This is for don`t load head.php and footer.php */
        $ViewData['ajax'] = true;
        return $ViewData;
    }

    /**
     * Used in view for sets the button URL.
     */
    private function setErrUrl()
    {
        switch ($this->ErrNumber) {
            case '440':
            case '401':
                return sprintf('%slogout', BIOVIEWURL);

            case '404':
            case '403':
                if (can('spdmBuyer')) {
                    return $_SERVER['REQUEST_SCHEME'] . "://" . HOME . "/" . COUNTRY . "/purchaseorder/filters";
                }

                return $_SERVER['REQUEST_SCHEME'] . "://" . HOME . "/" . COUNTRY . "/dashboard";

            case '503':
            default:
                return "#";
        }
    }
}
