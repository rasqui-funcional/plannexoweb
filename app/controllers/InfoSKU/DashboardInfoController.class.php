<?php

/**
 * <b>DashboardController.class: on InfoSKU</b>
 * Classe responsável por entregar os dados necessários para o preenchimento da tela de Informação do SKU.
 *
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class DashboardInfoController
{
    private $ViewData;
    private $serviceAuditPrepareData;
    CONST BASIC_INFORMATIONS = USERSESSIONID . ':log:sku:informations:result';

    public function __construct()
    {
        $this->userSession = Login::getUserSession();
        $this->DashboardInfoService = new DashboardInfoService($this->userSession['id_company_pk'],
            Request::get('skuId'));
        $this->serviceAuditPrepareData = new AuditPrepareData();
    }

    /**
     * <b>getHeader</b>
     * Recupera os dados necessários para o preenchimento do cabeçalho. Parâmetros necessários: id(int) do sku.
     * @return JSON
     */
    public function getHeader()
    {
        $viewData = $this->DashboardInfoService->getHeader();

        $log_data = ['base'=> array_merge(['ID_SKU_PK' => Request::get('skuId')], $this->DashboardInfoService->getHeaderData())];
        Redis::HSet(
            self::BASIC_INFORMATIONS,
            $this->serviceAuditPrepareData->basicInformations($log_data)
        );

        $viewData['json'] = true;

        return $viewData;
    }

    /**
     * <b>updateSkuStatus</b>
     * Atualiza o status(sit_sku na tabela SCM_SKU) do SKU. Parâmetros necessários: id(int) do sku, status desejado(bool).
     * @return boolean
     */
    public function updateSkuStatus()
    {
        $post = Request::post();
        $this->ViewData['json'] = true;

        if (!is_numeric($post['status'])) {
            return $this->ViewData;
        }

        $data = [
            'SIT_SKU' => $post['status'],
            'SIT_SKU_MANUAL' => 1,
        ];

        $result = DB::Update(
            'SCM_SKU',
            $data,
            "WHERE ID_SKU_PK = :id",
            "id={$post['id']}"
        );

        Log::new(
            self::BASIC_INFORMATIONS,
            'id_sku_pk',
            'sku:list:modal:informations:basic',
            'update',
            (int)$post['id'],
            $data
        );

        // SET NEW LOG DATA TO REDIS
        $data_log = Redis::HGet(self::BASIC_INFORMATIONS);
        $data_log['SIT_SKU'] = $post['status'];
        Redis::HSet(self::BASIC_INFORMATIONS, $data_log);
        $this->ViewData['result'] = $result;

        return $this->ViewData;
    }
}
