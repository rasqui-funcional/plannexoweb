<?php

/**
 * <b>InformationController: on InfoSKU</b>
 * Classe responsável por entregar os dados necessários para o preenchimento da tela de Informação do SKU.
 *
 * @copyright (c) 2018, Alef Alves | Bionexo
 */
class InformationController
{
    CONST BASIC_INFORMATIONS = USERSESSIONID . ':log:sku:informations:result';
    CONST CALENDAR_SKU = USERSESSIONID . ':log:sku:calendar:result';
    CONST POLICY_SKU = USERSESSIONID . ':log:sku:policy:result';
    CONST INTERDEPENDENCE_SKU = USERSESSIONID . ':log:sku:interdependence:result';

    private $serviceAuditPrepareData;

    public function __construct()
    {
        $this->session = Login::getUserSession();
        $this->service = new InformationService((int) $this->session['id_company_pk']);
        $this->serviceAuditPrepareData = new AuditPrepareData();
    }

    public function basicInformations()
    {
        $data = [];
        $data['json'] = true;
        $data['data'] = $this->service->buildBasicInformations();
        
        $log_data = array_merge_recursive($data['data'], ['base' => ['OBSERVATION' => $data['data']['observation']]]);

        Redis::HSet(
            self::BASIC_INFORMATIONS,
            $this->serviceAuditPrepareData->basicInformations($log_data)
        );
        return $data;
    }

    public function getCalendar()
    {
        $data = [];
        $data['json'] = true;
        $data['data'] = $this->service->getCalendar();

        Redis::HSet(self::CALENDAR_SKU, $this->serviceAuditPrepareData->calendar($data['data']));

        return $data;
    }

    public function updateCalendar()
    {
        $data = [];
        $data['json'] = true;
        $post = Request::post();
        $data['data'] = $this->service->updateCalendar($post);
        if ($data['data']) {
            Log::new(
                self::CALENDAR_SKU,
                'id_sku_pk',
                'sku:list:modal:informations:calendar',
                'update',
                (int)Request::get('skuID'),
                [CALENDAR_SKU_PARAM => $post['vCalendar']]
            );
        }

        return $data;
    }

    public function getDates()
    {
        $this->service->sku_calendar_id = (int) Request::get('calendarID');

        $data['json'] = true;
        $data['data'] = $this->service->getDates();

        return $data;
    }

    public function basicInformationsUpdate()
    {
        $data['json'] = true;
        $post = Request::post();

        $data['data'] = $this->service->basicInformationsUpdate($post);
        if ($data['data'] === true) {
           Log::new(
                self::BASIC_INFORMATIONS,
                'id_sku_pk',
                'sku:list:modal:informations:basic',
                'update',
                (int) Request::get('skuID'),
                $this->service->buildSkuData($post)
           );
        }

        return $data;
    }

    public function getPolicy()
    {
        $skuID = Request::get('skuID');
        $data['json'] = true;
        $data['data'] = $this->service->getPolicy($skuID);
        Redis::HSet(self::POLICY_SKU, $this->serviceAuditPrepareData->policy($data['data']));

        return $data;
    }

    public function getPolicyByID()
    {
        $data['json'] = true;
        $policy_id = (int) Request::get('policyID');
        $data['data'] = $this->service->getPolicyByID($policy_id);

        Redis::HSet(self::POLICY_SKU, $this->serviceAuditPrepareData->policy($data['data']));

        return $data;
    }


    public function getInterdependence()
    {
        $data['json'] = true;
        $data['data'] = $this->service->getInterdependenceData();

        Redis::HSet(
            self::INTERDEPENDENCE_SKU,
            $this->serviceAuditPrepareData->interdependence($data)
        );

        return $data;
    }

    public function getChildrenData()
    {
        $data['ajax'] = true;
        $post = Request::post();
        $idCompany = $this->session['id_company_pk'];
        $codItem = $post['codItem'];
        $codEstab = $post['codEstab'];
        $rawData = $this->service->getChildrenData($codItem,$idCompany,$codEstab);
        $data['data']['children'] = $rawData['data'];
        $data['data']['error'] = $rawData['error'];
        if(isset($rawData['parent']))
            $data['data']['parent'] = array_pop($rawData['parent']);
        if(isset($rawData['footer']))
            $data['data']['footer'] = array_pop($rawData['footer']);

        return $data;
    }

    public function establishmentOrigin()
    {
        $data['json'] = true;
        $params = [ 'codItemPK' => Request::get('codItemPK') ];
        $data['data'] = $this->service->establishmentOrigin($params);

        return $data;
    }

    public function updateInterdependence()
    {
        $data['json'] = true;
        $post = Request::post();

        $data['data'] = $this->service->updateInterdependence($post);
        if ($data['data']) {
            Log::new(
                self::INTERDEPENDENCE_SKU,
                'id_sku_pk',
                'sku:list:modal:informations:interdependence',
                'update',
                (int)Request::get('skuID'),
                $post
            );
        }

        return $data;
    }

    public function updatePolicy()
    {
        $data['json'] = true;
        $post = Request::post();
        $data['data'] = $this->service->updatePolicy($post);

        if ($data['data']) {
            Log::new(
                self::POLICY_SKU,
                'id_sku_pk',
                'sku:list:modal:informations:policy',
                'update',
                (int)Request::get('skuID'),
                $this->service->buildPolicyData($post)
            );
        }

        return $data;
    }

    public function deleteInterdependence()
    {
        $data['json'] = true;
        $data['data'] = $this->service->deleteInterdependence();
        if ($data['data']) {
            Log::new(
                self::INTERDEPENDENCE_SKU,
                'id_sku_pk',
                'sku:list:modal:informations:interdependence',
                'delete',
                (int)Request::get('skuID'),
                $this->service->buildDeleteInterdependence()
            );
        }

        return $data;
    }

    public function recalculate()
    {
        $data['json'] = true;
        $sku_id = Request::get('skuID');
        $user_id = Login::getUserSession()['user_id'];

        Logger::info(__CLASS__ . "::recalculate", ['message'=> "User {$user_id} is sending SKU {$sku_id} of company {$this->company_id} to recalculate"]);
        $data['data'] = $this->service->recalculateSku();
        Logger::info(__CLASS__ . "::recalculate", ['message'=> "Receive the following response '{$data['data']}' when try recalculate the SKU {$sku_id} of company {$this->company_id}"]);

        return $data;
    }
}
