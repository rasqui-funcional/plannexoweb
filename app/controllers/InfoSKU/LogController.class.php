<?php

/**
 * <b>LogController: on InfoSKU</b>
 * Classe responsável por entregar os dados necessários para o preenchimento da aba Log do SKU.
 *
 * @copyright (c) 2019, Gabriel G. Antunes | Bionexo
 */
class LogController
{
    private $ViewData;
    private $service;

    public function __construct()
    {
        $this->session = Login::getUserSession();
    }

    public function fetch()
    {
        $post = Request::post();
        $sku_id = (int)$post['id'];
        $page = (int)$post['page'];
        $limit = (int)$post['limit'];
        $orderColumns = $post['orderColumns'];
        $order = ($orderColumns ? $this->prepareOrder($orderColumns) : " ");

        $this->service = new LogService($this->session['id_company_pk'], $sku_id);

        $data = [];
        $data['json'] = true;

        $offset = $this->prepareSetOffset($page, $limit);
        $data['data']['items'] = $this->formatData($this->service->fetch($offset, $order, $limit), $sku_id);

        $count_sku_log = $this->service->countSkuLog();
        $quantity_pages = Utilities::calculatePages($count_sku_log, $limit);

        $data['data']['quantityPages'] = $quantity_pages;
        $data['data']['quantitySkuLog'] = $count_sku_log;
        $data['data']['startItem'] = $offset + 1;
        $data['data']['endItem'] = $offset + $limit;
        $data['data']['listFooterPages'] = $this->service->listFooterPages($quantity_pages, $page);

        return $data;
    }

    public function getLogData()
    {
        $this->ViewData['ajax'] = true;

        $this->post = Request::post();

        $offset = $this->prepareSetOffset($this->post['page'], $this->post['limit']);


        $this->service = new LogService($this->session['id_company_pk'], $this->post['sku']);
        $order = (isset($this->Post['order']) && $this->Post['order'] ? $this->prepareOrder($this->Post['order']) : " ");
        $this->ViewData['dataTable'] = $this->service->getTableData($offset, $order, $this->post['limit']);
        $this->ViewData['dataTable'] = $this->formatRows($this->ViewData['dataTable']);
        $this->ViewData['Cols'] = $this->prepareCols([
            'ID, INSERT_DATE',
            'USERNAME',
            'IP_USER',
            'APPLICATION',
            'SC_FIELD_0',
            'ALTERED_COLUMN',
            'OLD_VALUE',
            'NEW_VALUE'
        ]);
        $this->ViewData['footer']['count'] = count($this->ViewData['dataTable']);
        $this->ViewData['footer']['pages'] = Utilities::calculatePages($this->ViewData['footer']['count'],
            $this->post['limit']);
        $this->ViewData['skuId'] = $this->post['sku'];
        return $this->ViewData;
    }

    private function formatRows($rows)
    {
        foreach ($rows as $key => $value) {
            $methodName = (string)$value['SC_FIELD_0'];
            $newRows[$key] = $this->$methodName($value);
        }

        return $newRows;
    }

    private function update($row)
    {
        $newRow = [];
        foreach ($row as $key => $value) {
            if ($key == 'DESCRIPTION') {
                $method = 'format' . ucfirst($key);
                $newRow[$key] = $this->$method($value);
                continue;
            }

            if ($key == 'USERNAME') {
                $newRow[$key] = Utilities::getUser(explode('.', $value)[1]);
                continue;
            }

            $newRow[$key] = $value;
        }

        return $newRow;
    }

    private function formatDescription($data)
    {
        $newRow = [];
        $description = stream_get_contents($data);
        $afterFields = explode('fields <--', $description);

        $oldVsNew = explode('||', $afterFields[1]);

        $old = [];
        $new = [];
        $alteredColumn = [];

        foreach ($oldVsNew as $key => $value) {
            $value = explode(':', $value);
            if ($key % 2 == 0) {
                $alteredColumn[] = explode(' (old)', $value[0])[0];
                $old[] = $value[1];
            } else {
                $new[] = $value[1];
            }
        }

        $newRow['ALTERED_COLUMN'] = $alteredColumn;
        $newRow['OLD_VALUE'] = $old;
        $newRow['NEW_VALUE'] = $new;

        return $newRow;
    }

    private function prepareSetOffset($Page, $limit)
    {
        return ($Page - 1) * $limit;
    }

    private function getColOrder($ColName)
    {
        if (isset($this->Post['order']) && $this->Post['order']) {
            $Orders = $this->Post['order'];
        } else {
            return null;
        }

        if ($Orders) {
            $KeySearch = key_exists($ColName, $Orders);
        }

        if ($KeySearch) {
            return $Orders[$ColName];
        }
    }

    private function prepareCols($Columns)
    {
        $ColsArray = array();
        foreach ($Columns as $Name) {
            $PrintName = strtoupper($Name);
            $ColsArray[$PrintName] = [
                'type' => 'text',
                'order' => $this->getColOrder($PrintName)
            ];
        }

        return $ColsArray;
    }

    private function prepareOrder($Orders)
    {
        end($Orders);
        $End = key($Orders);
        $OrderQuery = "ORDER BY ";

        foreach ($Orders as $Columns => $ColOrder) {
            $OrderQuery .= "{$Columns} {$ColOrder}";
            if ($End != $Columns) {
                $OrderQuery .= ", ";
            }
        }

        return $OrderQuery;
    }

    /**
     * @param $data
     * @param $sku_id
     * @return mixed
     */
    private function formatData($data, $sku_id)
    {
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i]['CREATOR'] == 'Plannexo 1.0') {
                $data[$i]['DESCRIPTION'] = $this->formatLogDescription($data[$i]['DESCRIPTION'], $sku_id);
            }
        }

        return $data;
    }

    /**
     * @param $description
     * @param $sku_id
     * @return false|string
     */
    private function formatLogDescription($description, $sku_id)
    {
        $fields = explode('fields <--', $description);
        $logs = explode('||', $fields[1]);
        $old = $new = $column = [];
        foreach ($logs as $key => $value) {
            $value = explode(':', $value);
            if ($key % 2 == 0) {
                $column[] = explode(' (old)', $value[0])[0];
                $old[] = trim($value[1]);
            } else {
                $new[] = trim($value[1]);
            }
        }

        return json_encode(
            [
                'id' => $sku_id,
                'type' => 'id_sku_pk',
                'id_sku' => $sku_id,
                'fields' => [[
                    'label' => implode("<br/>", $column),
                    'old' => implode("<br/>", $old),
                    'new' => implode("<br/>", $new)
                ]]
            ]
        );
    }
}
