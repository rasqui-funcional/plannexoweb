<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 01/03/19
 * Time: 17:07
 */

class AuditController extends Fields
{
    private $dataOld;
    private $auditService;

    public function __construct()
    {
        parent::__construct();
        $this->auditService = new AuditService();
    }

    public function create($key, $type, $application, $action, $id, array $dataNew)
    {
        $this->dataOld = Redis::HGet($key);
        $params = Login::getUserSessionByKeys(['ip', 'user_id', 'id_company_pk']);

        try {
            $this->isValid();
            $extra['id_sku'] = (array_key_exists('ID_SKU_PK', $dataNew))
                ? (int)$dataNew['ID_SKU_PK'] : (int)$id;

            $logSave = $this->auditService
                ->params($params)
                ->dataOld($this->dataOld)
                ->dataNew($dataNew)
                ->application($application)
                ->action($action)
                ->type($type)
                ->id($id)
                ->extra($extra)
                ->generate()
                ->save()
            ;

            return $logSave;
        } catch (Exception $e) {
            // gravar log de erro para monitoria
            Logger::error(__CLASS__ . ":auditController:create", [
                $e->getCode(), $e->getFile(), $e->getMessage(), $e->getTrace()
            ]);
            return false;
        }
    }

    protected function isValid()
    {
        if (!$this->dataOld) {
            throw new Exception($this->lang['global']['error_on_retrieve_data']);
        }
    }
}
