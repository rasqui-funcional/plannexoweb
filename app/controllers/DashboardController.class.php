<?php

class DashboardController extends Fields
{
    private $ViewData;
    private $where;
    private $user_session;
    private $select_filter;
    private $applied_filters;
    private $post;
    private $manager_users = null;

    public function __construct()
    {
        parent::__construct();
        $this->post = Request::post();
        $this->user_session = Login::getUserSession();
        $this->service = new DashboardService($this->user_session);
    }

    public function getDashboardInfo()
    {
        $benchmarking = (new Benchmarking())->setClassName(__CLASS__)->setTimeStart(microtime(true));

        $this->where = $this->createSpecialFilterDashboard();

        $ViewData = parent::getFields();
        $ViewData['selected_filter'] = $this->select_filter;
        $ViewData['detalhes'] = $this->getDetailsData(); //Dados da consulta da modal +detalhes
        $ViewData['totais'] = $this->getTotal($ViewData['detalhes']); //Totais da modal +detalhes
        $ViewData['percentage'] = $this->getPercentage($ViewData['detalhes'], $ViewData['totais']); //Porcentagens da modal +detalhes
        $ViewData['linhaData'] = $this->getDetailsRows($ViewData['detalhes'], $ViewData['percentage']); //Linhas da tabela da modal +detalhes
        $ViewData['indicadores'] = $this->getIndicators(); //Indicadores
        $ViewData['lospercentage'] = $this->getLevelOfServicePercentage(); //Porcentagens da modal detalhes
        $ViewData['filters'] = Fields::getSelectFilter('SCM453C'); //Dados do select de presets de filtros
        $ViewData['unified'] = $this->getUnifiedQuery();//Consulta unificada
        $ViewData['detailStockBalancing'] = $this->service->detailStockBalancing($this->where, $this->manager_users);
        $ViewData['applied_filters'] = $this->applied_filters;//Consulta unificada
        $ViewData['datepercent'] = $this->getDateLevelOfServicePercentage(); //Data da Porcentagem

        $benchmarking->setTimeEnd(microtime(true))->benchmark();
        return $ViewData;
    }

    // PRIVATE METHODS

    private function createJsonAppliedFilters($filters)
    {
        $this->applied_filters = 'null';
        if ($filters) {
            $this->applied_filters = json_encode($filters);
        }
    }

    private function getEstabByUser($alias = '')
    {
        if (!empty($alias)) {
            $alias .= '.'; 
        }

        if(!empty($this->user_session['estab'])) {
            return " AND {$alias}COD_ESTAB_FK IN({$this->user_session['estab']}) ";
        }
        return '';
    }

    private function getUnifiedQuery()
    {
        $is_supplier = Gate::setSupplier('', 1);
        $estabIn = $this->getEstabByUser('a');

        $fields = "
			SUM(qty_excess)                                                                                                                                 AS excess,
			SUM(inv_total)                                                                                                                                  AS total,
			ROUND((SUM(qty_excess)*100)/CASE WHEN SUM(inv_total)=0 THEN 1 ELSE SUM(inv_total) END,1)                                                        AS perc_excess,
			100-ROUND((SUM(qty_excess)*100)/CASE WHEN SUM(inv_total)=0 THEN 1 ELSE SUM(inv_total) END,1)                                                    AS perc_diff,
			ROUND(SUM(qty_excess*val_unit_erp),0)                                                                                                           AS excess_val,
			ROUND(SUM(qty_excess*val_unit_erp)/ CASE WHEN SUM(qty_forecast*val_unit_erp)=0 THEN 1 ELSE SUM(qty_forecast*val_unit_erp) END,0)                AS days,
			ROUND(SUM(inv_level_very_low*val_unit_erp),0)                                                                                                   AS m_baixo,
			ROUND(SUM(inv_level_very_high*val_unit_erp),0)                                                                                                  AS m_alto,
			ROUND(SUM(inv_total*val_unit_erp),0)                                                                                                            AS total_val,
			ROUND(SUM(inv_available*val_unit_erp),0)                                                                                                        AS available_val,
			ROUND(SUM(qty_target*val_unit_erp),0)                                                                                                           AS rep,
			ROUND(SUM(inv_total*val_unit_erp) ,0)                                                                                                           AS upper_limit,
			100-ROUND((SUM(qty_final+qty_next_in)*100)/CASE WHEN SUM(inv_total)=0 THEN 1 ELSE SUM(inv_total) END,1)                                         AS perc_diff,
			ROUND(SUM((qty_final)*val_unit_erp),0)                                                                                                          AS rep_val,
			ROUND(SUM((qty_final)*val_unit_erp)/CASE WHEN SUM(qty_forecast*val_unit_erp)=0 THEN 1 ELSE SUM(qty_forecast*val_unit_erp) END,0)                AS rep_days,
			ROUND(SUM((qty_next_in+purchase_req)*val_unit_erp),0)                                                                                           AS rep_val_order,
			ROUND(SUM((qty_next_in+purchase_req)*val_unit_erp)/CASE WHEN SUM(qty_forecast*val_unit_erp)=0 THEN 1 ELSE SUM(qty_forecast*val_unit_erp) END,0) AS rep_days_order,
			CASE WHEN SUM(qty_forecast*val_unit_erp) = 0 THEN 0 ELSE NVL(ROUND(SUM(inv_total*val_unit_erp) / SUM(qty_forecast*val_unit_erp),0),0) END       AS days_total,
			CASE WHEN SUM(qty_forecast*val_unit_erp) = 0 THEN 0 ELSE NVL(ROUND(SUM(inv_available*val_unit_erp) / SUM(qty_forecast*val_unit_erp),0),0) END       AS available_days_total
		";

        $table = "vw_scm_aux_sku a";
        $handled_manager_users = $this->service->handleManagerUsers('a.', $this->manager_users);
        $terms = "WHERE sit_sku = 1 AND ID_COMPANY_FK = {$this->user_session['id_company_pk']} {$this->where} {$handled_manager_users} {$is_supplier} {$estabIn}";

        $unificado = DB::Select($table, $fields, $terms);
        $unificado = array_pop($unificado);

        Redis::HSet(USERSESSIONID . ':session:dashboard:unified:result', $unificado);

        return $unificado;
    }

    private function getDateLevelOfServicePercentage()
    {
        return $this->service->buildDetailsDataServicePercentage();
    }

    private function getLevelOfServicePercentage()
    {
        return $this->service->buildDetailsServicePercentage($this->where, $this->manager_users);
    }

    private function createSpecialFilterDashboard()
    {
        if (!$this->post) {
            $this->select_filter = null;
            $this->applied_filters = 'null';
            Redis::Set(USERSESSIONID . ":session:dashboard:filters-query", '');
            Redis::Set(USERSESSIONID . ":session:dashboard:manager-users", '');
            Redis::HSet(USERSESSIONID . ":session:suggest:filters-post", ['post-fields' => '']);
            return null;
        }

        return $this->setFilterData($this->post);
    }

    private function setFilterData($post)
    {
        if (isset($post['selected_filter'])) {
            $this->select_filter = $this->service->filter($post['selected_filter'], $this->user_session);
            $decode = json_decode($this->select_filter['FILTER_DATA'], true);
            $decode = $this->handlePost($decode);
            Redis::Set(USERSESSIONID . ":session:dashboard:manager-users", $this->manager_users);

            $filter_fields = DB::setFields($decode);
            $this->createJsonAppliedFilters($decode);
            $post_fields = $this->select_filter['FILTER_DATA'];
        } else {
            $this->select_filter = null;
            $post = $this->handlePost($post);
            Redis::Set(USERSESSIONID . ":session:dashboard:manager-users", $this->manager_users);
            $this->createJsonAppliedFilters($post);
            $filter_fields = DB::setFields($post);
            $post_fields = json_encode($post);
        }

        Redis::Set(USERSESSIONID . ":session:dashboard:filters-query", $filter_fields);
        Redis::HSet(USERSESSIONID . ":session:suggest:filters-post", ['post-fields' => $post_fields]);

        return $filter_fields;
    }

    /**
     * <b>getIndicators</b>
     * Retorna os valores de itens urgentes, próximas parcelas, ordens atrasadas e parcelas aprovadas.
     * @return Array.
     * @var where
     */
    private function getIndicators()
    {
        return $this->service->buildIndicators($this->where, $this->select_filter, $this->manager_users);
    }

    private function getDetailsData()
    {
        return $this->service->buildDetails($this->where, $this->manager_users);
    }

    private function getDetailsRows($detalhes, $percentual_arr)
    {
        $linha = 0;
        $linhaData = array();
        $cell = "<td><img class=\"mr-2\" width=\"20px\" src='../../resources/img/";
        foreach ($detalhes as $key => $value) {
            $linhaData[$key] .= "<tr>";
            $linhaData[$key] .=  "{$cell}INV_LEVEL_{$value['INV_LEVEL']}.png'> {$this->lang['inv_level'][$value['INV_LEVEL']]}</td>";

            $linhaData[$key] .= "<td>" . number_format($value['SKUS'], 0, "", ".") . "</td>
				<td>" . str_replace('.', ',', round($percentual_arr[$linha], 2)) . "%</td>
				<td>" . Utilities::Currency($value['SALDO']) . "</td>
				<td>" . Utilities::Currency($value['QTY_EXCESS']) . "</td>
				<td>" . Utilities::Currency($value['REQ_PURCH']) . "</td>
				<td>" . Utilities::Currency($value['PURCH_ORDER']) . "</td>
				</tr>";
            $linha++;
        }

        return $linhaData;
    }

    private function getTotal($array)
    {
        $totais = array();
        if (count($array) > 0) {
            foreach ($array as $key => $value) {
                $totais['SKUS'] += $value['SKUS'];
                $totais['SALDO'] += round($value['SALDO'], 2);
                $totais['QTY_EXCESS'] += round($value['QTY_EXCESS'], 2);
                $totais['REQ_PURCH'] += round($value['REQ_PURCH'], 2);
                $totais['PURCH_ORDER'] += round($value['PURCH_ORDER'], 2);
            }
        }
        return $totais;
    }

    private function getPercentage($detalhes, $totais)
    {
        $percentual_arr = array();
        if (is_array($detalhes)) {
            foreach ($detalhes as $key => $value) {
                if ($totais['SKUS'] != 0) {
                    array_push($percentual_arr, ($value['SKUS'] / $totais['SKUS']) * 100);
                }
            }
        }

        return $percentual_arr;
    }

    private function handlePost($post)
    {
        $manager_users = $post['user_id'];
        if (!is_null($manager_users)) {
            $data = implode(',', $manager_users);
            $this->manager_users = "AND user_id IN ({$data})";
        }

        unset($post['user_id']);

        return $post;
    }
}
