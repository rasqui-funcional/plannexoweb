<?php

class TestController
{
    private $ViewData;
    private $Name;

    /**
     * Função declarada no Controller
     */
    public function methodTest()
    {
        $data = [
            'db' => DBHOST,
            'redis' => REDISHOST,
            'session_id' => Login::getUserSession(),
            'user_session' => USERSESSIONID,
        ];

        debug($data);

        return true;
    }

    public function getParam()
    {
        return Request::get('param');
    }

    public function getMethod()
    {
        $items = [
            USERSESSIONID .':log:sku:informations:result',
            USERSESSIONID .':log:sku:calendar:result',
            USERSESSIONID .':log:sku:policy:result',
            USERSESSIONID .':log:sku:interdependence:result',
            USERSESSIONID .':log:suggest:result',
            USERSESSIONID . ':log:order_cover:suggest:result'
        ];

        Redis::HSet(USERSESSIONID . ':test:123', [
            'USER_ID' => '1',
            'DEFAULT_LOCAL' => '38',
            'CURVE_XYZ' => 'Z',
            'COD_GROUP1_FK' => 'MED',
            'COD_GROUP2_FK' => '001',
            'COD_GROUP3_FK' => '1-13',
            'COD_GROUP4_FK' => '1-13-27',
            'COD_GROUP5_FK' => '1',
            'COD_GROUP6_FK' => 'Johnson & Johnson do Brasil Ind E Com de Prod Para Saude Ltda',
            'COD_GROUP7_FK' => '#',
            'COD_GROUP8_FK' => 'CME',
            'COD_GROUP9_FK' => '#',
            'COD_GROUP10_FK' => '#',
            'COD_GROUP11_FK' => '#',
            'COD_GROUP12_FK' => '#',
        ]);

        switch (Request::get('method')) {
            case 'log':
                $new = [
                    'cod_group1_fk' => 'MED',
                    'cod_group2_fk' => '001',
                    'cod_group3_fk' => '1-13',
                    'cod_group4_fk' => '1-13-27',
                    'cod_group5_fk' => '#',
                    'cod_group6_fk' => 'Multirad Com de Mat Hosp Ltda',
                    'cod_group7_fk' => '#',
                    'cod_group8_fk' => '#',
                    'cod_group9_fk' => '#',
                    'cod_group10_fk' => '#',
                    'cod_group11_fk' => '#',
                    'cod_group12_fk' => '#',
                    'user_id' => '998',
                    'default_local' => '20',
                    'notes' => '987654321',
                    'usu_notes' => '998',
                    'curve_xyz' => 'Y',
                ];

                $logSave = Log::new(
                    USERSESSIONID . ':test:123',
                    'id_sku_fk',
                    'sku:list:info',
                    'update',
                    1768423640,
                    $new
                );

                exit(0);
                break;

            case 'redis':
                foreach ($items as $item) {
                    $HGet = Redis::HGet($item);
                    if (array_reverse(explode(':', $item))[1] === "suggest") {
                        debug(unserialize($HGet['data']));
                    } else {
                        debug(Redis::HGet($item));
                    }
                }
                break;

            case 'redis-clear':
                foreach ($items as $item) {
                    Redis::HSet($item, ['data' => null]);
                }
                break;

            case 'mask':
                debug(MaskHelper::cnpjCpf('12345678901234'));
                break;

            case 'suggest-logs':
                $logs = unserialize( Redis::HGet(USERSESSIONID . ':log:suggest:result')['data'] );
                debug( $logs );
                break;

            default:
                echo "Ops! Comando inválido...";
                break;
        }
    }

    public function getIndicators()
    {
        switch (Request::get('indicator')) {
            case 'sugestaoaprovadas':
                $columns = 'COUNT(*) AS TOTAL, SUM(HI.QTY_ALTERED) AS QTY, SUM(HI.QTY_ALTERED * S.VAL_UNIT_ERP) AS QTY_VAL';
                $table = 'VW_SCM_AUX_SKU S';
                $terms = "INNER JOIN SCM_SKU_PURCHASE_SUG_HIST HI
                    ON S.ID_COMPANY_FK = HI.ID_COMPANY_FK AND S.ID_SKU_PK = HI.ID_SKU_FK
                  INNER JOIN SCM_SKU_PURCHASE_SUG_HEADER HE
                    ON HI.ID_COMPANY_FK = HE.ID_COMPANY_FK AND HI.ID_INTEGRATION_GTP = HE.COD_HEADER_PK
                  WHERE S.ID_COMPANY_FK = 2 
                    AND TRUNC(HI.DATE_INTEGRATION) BETWEEN TRUNC(CURRENT_DATE) AND TRUNC(CURRENT_DATE - 7)
                    AND HE.DATE_APPROVED IS NOT NULL";

                $query = DB::SelectRow($table, $columns, $terms);
                break;

            default:
                break;
        }

        return true;
    }
}
