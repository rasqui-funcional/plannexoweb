<?php 

/**
 * Class responsable for controll the view VW_SCR_SCM528G_ORDERS_SKU
 */
class OrdersController {

    /**
     * <b>getOrders</b>
     * Return all the orders by company and id_sky
    */
    public function getOrders($SkuId = null){

        if(!$SkuId){
            $SkuId = Request::get('sku_id');
        }
        $session= Login::getUserSession();
        $Terms = "WHERE ID_COMPANY_FK = {$session['id_company_pk']} AND ID_SKU_PK = {$SkuId}";
        $Total = DB::Select('VW_SCR_SCM528G_ORDERS_SKU', 'count(*), sum(QTY_PARC) as qty_parc', $Terms);
        $Orders = DB::Select('VW_SCR_SCM528G_ORDERS_SKU', 'NUM_ORDER, DESC_SUPPLIER, QTY_PARC, QTY_REC, QTY_ORIG, DATE_ORDER, DATE_EXP, SIT_AUTO_SUG', $Terms);
        $ViewData['total'] = $Total;
        $ViewData['orders'] = $Orders;

        $ViewData['ajax'] = true;
        return $ViewData;
    }
    /** ------------------------------ PRIVATE ------------------ */

    /**
     * <b>prepareData</b>
     * Prepare the data of Column
     */
    private function prepareData($ColName, $Value){
        switch ($ColName) {
            case 'INV_DAYS':
                $Value = round($Value, 1);
                break;                
            case 'COD_ESTAB_FK':
                $Value = Utilities::getEstab($Value);
                break;
        }
        return $Value;
    }

}