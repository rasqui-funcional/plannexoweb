<?php

/**
 * <b>ApprovalsController</b>
 * Controller responsible for managing Suggest methods
 *
 * @copyright (c) 2018 | Bionexo
 */


class ApprovalsController extends Fields
{
    private $ViewData;
    private $Query;
    private $post;
    private $Order;
    private $user;
    private $approvalsService;
    private $indicator;
    private $filteringByField = ['COD_HEADER_PK', 'COD_HEADER_ERP', 'SIT_HEADER', 'COD_ESTAB_FK', 'DESC_LOCAL', 'YN_URGENT', 'DESC_REQ_REASON', 'DESC_BUYER', 'DESC_SECTOR', 'DESC_USER', 'DT_EXP_REQ', 'DATE_APPROVED', 'COD_ITEM'];

    public function __construct()
    {
        parent::__construct();
        $this->indicator = 'lastdays';
        $this->user = Login::getUserSession();
        $this->approvalsService = new ApprovalsService($this->user);
    }

    public function getList()
    {
        $this->ViewData['estabs'] = parent::getFields();
        $this->ViewData['filters'] = $this->filteringByField;
        return $this->ViewData;
    }

    public function getTable()
    {
        $this->post = Request::post();
        $this->ViewData['ajax'] = true;

        $extra = null;
        if (array_key_exists('indicator', $this->post) && !empty($this->post['indicator']) && $this->post['indicator'] == $this->indicator) {
            $extra = Date::createRangeDate(7, true);
        }

        $order = 'order by COD_HEADER_PK desc';

        if (!is_null($this->post['order']) || !is_null(Request::get('order'))) {
            $order_to_redis = $this->post['order'] ? json_encode($this->post['order']) : urldecode(Request::get('order'));
            Redis::Set(USERSESSIONID . ':session:approvals:list:order-data', $order_to_redis);

            if ($order_to_redis != '') {
                $order = $this->prepareOrder(json_decode($order_to_redis));

                if ($this->post['order'] == '') {
                    $this->post['order'] = json_decode($order_to_redis, true);
                }
            }
        }

        $offset = $this->prepareSetOffset($this->post['page'], $this->post['limit']);

        $this->ViewData['table'] = $this->approvalsService->paginate(
            $offset,
            $this->post['limit'],
            $order,
            $this->user['id_company_pk'],
            $this->post['searchValue'],
            $this->post['searchCol'],
            $extra
        );

        $this->ViewData['qtd'] = $this->approvalsService->countRecords(
            $order,
            $this->user['user_id'],
            $this->user['id_company_pk'],
            $this->post['searchValue'],
            $this->post['searchCol'],
            $extra
        );

        $this->ViewData['table']['columns'] = $this->prepareCols($this->ViewData['table']['columns']);
        $this->ViewData['footer']['count'] = $this->ViewData['qtd'];

        $this->ViewData['footer']['pages'] = Utilities::calculatePages(
            $this->ViewData['footer']['count'],
            $this->post['limit']
        );

        $this->ViewData['user'] = $this->user;

        return $this->ViewData;
    }

    public function getCustomList()
    {
        $this->ViewData['indicator'] = Request::get('indicator');
        return $this->ViewData;
    }

    private function getFooterCols()
    {
        return "sum(TOTAL_PARCELAS) as TOTAL_PARCELAS";
    }

    private function prepareCols($columns)
    {
        $result = array();
        foreach ($columns as $name) {
            $printname = strtoupper($name);
            $result[$printname] = [
                'order' => $this->getColOrder($printname)
            ];
        }

        return $result;
    }

    private function getColOrder($colName)
    {
        if (isset($this->post['order']) && $this->post['order']) {
            $orders = $this->post['order'];
        } elseif (Redis::Get(USERSESSIONID . ':session:approvals:list:order-data')) {
            $orders = (array)json_decode(Redis::Get(USERSESSIONID . ':session:approvals:list:order-data'));
            $colName = strtolower($colName);
        } else {
            return null;
        }

        if ($orders) {
            $keySearch = key_exists($colName, $orders);
        }

        if ($keySearch) {
            return $orders[$colName];
        }
    }

    private function prepareSetOffset($Page, $Limit)
    {
        return ($Page - 1) * $Limit;
    }

    private function prepareOrder($Orders)
    {
        end($Orders);
        $End = key($Orders);

        $OrderQuery = "ORDER BY ";
        foreach ($Orders as $Columns => $ColOrder) {
            $OrderQuery .= "{$Columns} {$ColOrder}";
            if ($End != $Columns) {
                $OrderQuery .= ", ";
            }
        }

        return $OrderQuery;
    }
}
