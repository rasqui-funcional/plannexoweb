<?php

/**
 * <b>ApprovalsSuggestController</b>
 * Controller responsible for managing Suggest methods
 *
 * @copyright (c) 2018 | Bionexo
 */
class ApprovalsSuggestController extends Fields
{
    const COD_SECTOR = 'cod_sector';
    const COD_REASON = 'cod_reason';
    const COD_BUYER = 'cod_buyer';
    private $ViewData;
    private $user;
    private $approvalsSuggestService;
    private $codHeaderPk;
    private $filteringByField = ['ID_PSUG_PK', 'NUM_PARC_PK', 'ID_SKU_FK', 'COD_ITEM_PK', 'COD_ESTAB_FK', 'SIT_PSUG', 'QTY_ORIGINAL', 'QTY_ALTERED', 'DATE_ARRIVAL_ORIGINAL', 'DATE_ARRIVAL_ALTERED', 'DATE_CREATED', 'LOGIN', 'DATE_APPROVED'];

    public function __construct()
    {
        parent::__construct();
        $this->user = Login::getUserSession();
        $this->approvalsSuggestService = new ApprovalsSuggestService();
    }

    public function getList($id = null)
    {
        $this->ViewData['estabs'] =  parent::getFields();
        $this->ViewData['filters'] = $this->filteringByField;
        $this->ViewData['user'] = $this->user;
        $this->ViewData['header'] = $this->buildHeader($id ? $id : Request::get('id'));
        if(Redis::Get(USERSESSIONID . ':session:approvals:list:order-data')!= '""')
            $this->ViewData['approvals_order'] = Redis::Get(USERSESSIONID . ':session:approvals:list:order-data');
            
        
        return $this->ViewData;
    }


    public function getTable()
    {
        $this->post = Request::post();
        $this->codHeaderPk = Request::get('codHeaderPk');
        $this->ViewData['ajax'] = true;
        if (!is_null($this->post['order'])) {
            Redis::Set(USERSESSIONID . ':session:approvals:suggest:list:order-data', json_encode($this->post['order']));
        }

        $data_footer = $this->approvalsSuggestService->getTotalFooter($this->user['id_company_pk'], $this->codHeaderPk, $this->post['searchCol'], $this->post['searchValue']);
        $order = (isset($this->post['order']) && $this->post['order'] ? $this->prepareOrder($this->post['order']) : "");
        $offset = $this->prepareSetOffset($this->post['page'], $this->post['limit']);

        $data_table = $this->approvalsSuggestService->paginate(
            $offset,
            $this->post['limit'],
            $order,
            $this->user['user_id'],
            $this->user['id_company_pk'],
            $this->codHeaderPk,
            $this->post['searchCol'],
            $this->post['searchValue']
        );

        $footer_count = (int) $this->approvalsSuggestService->count(
            $this->user['user_id'],
            $this->user['id_company_pk'],
            $this->codHeaderPk,
            $this->post['searchCol'],
            $this->post['searchValue']
        );

        $this->ViewData['table'] = $this->approvalsSuggestService->orderValues($data_table, $this->post['order']);
        $this->ViewData['table']['columns'] = $this->prepareCols($data_table['columns']);
        $this->ViewData['footer']['count'] = $footer_count;
        $this->ViewData['user'] = $this->user;
        $this->ViewData['footer']['pages'] = Utilities::calculatePages($this->ViewData['footer']['count'], $this->post['limit']);
        $this->ViewData['footer']['data'] = $this->prepareDataFooter($data_footer, $this->ViewData['table']['columns']);
        return $this->ViewData;
    }

    public function getDuplicationTable()
    {
        $this->post = Request::post();
        $this->ViewData['ajax'] = true;
        if (!is_null($this->post['order'])) {
            Redis::Set(USERSESSIONID . ':session:approvals:duplicate:list:order-data', json_encode($this->post['order']));
        }
        $order = (isset($this->post['order']) && is_array($this->post['order']) ? $this->prepareOrder($this->post['order']) : "");
        $offset = $this->prepareSetOffset($this->post['page'], $this->post['limit']);
        $this->ViewData['table'] = $this->approvalsSuggestService->DuplicateTableData($offset, $this->post['limit'], $order, $this->user['user_id'], $this->user['id_company_pk'], $this->post['codHeaderPk'], $this->post['searchCol'], $this->post['searchValue']);
        $this->ViewData['raw'] = $this->approvalsSuggestService->rawDuplicateTableData($this->user['user_id'], $this->user['id_company_pk'], $this->post['codHeaderPk'])['data'];
        $this->ViewData['table']['columns'] = $this->prepareCols($this->ViewData['table']['columns']);
        $this->ViewData['footer']['count'] = count($this->ViewData['raw']);
        $this->ViewData['user'] = $this->user;
        $this->ViewData['footer']['pages'] = Utilities::calculatePages($this->ViewData['footer']['count'], $this->post['limit']);


        return $this->ViewData;
    }

    private function buildHeader($id)
    {
        return $this->approvalsSuggestService->getHeader($id, $this->user['id_company_pk']);
    }

    private function prepareCols($columns)
    {
        $result = array();
        foreach ($columns as $name) {
            $printname = strtoupper($name);
            $result[$printname] = [
                'order' => $this->getColOrder($printname)
            ];
        }

        return $result;
    }

    private function getColOrder($colName)
    {
        if (isset($this->post['order']) && $this->post['order']) {
            $orders = $this->post['order'];
        } elseif (Redis::Get(USERSESSIONID . ':session:approvals:suggest:list:order-data')) {
            $orders = (array)json_decode(Redis::Get(USERSESSIONID . ':session:approvals:suggest:list:order-data'));
            $colName = strtolower($colName);
        } else {
            return null;
        }

        if ($orders) {
            $keySearch = key_exists($colName, $orders);
        }

        if ($keySearch) {
            return $orders[$colName];
        }
    }

    private function prepareSetOffset($Page, $Limit)
    {
        return ($Page - 1) * $Limit;
    }

    private function prepareOrder($Orders)
    {
        if (key_exists('QTY_ORIGINAL_VAL', $Orders)
            || key_exists('QTY_ALTERED_VAL', $Orders)
            || key_exists('LOGIN', $Orders)
        ) {
            return "";
        }

        $arr = [];
        foreach ($Orders as $Columns => $ColOrder) {
            array_push($arr, "{$Columns} {$ColOrder}");
        }

        $OrderQuery = "ORDER BY ";
        $OrderQuery .= implode(', ', $arr);
        return $OrderQuery;
    }

    public function getData()
    {
        $codEstab = Request::get('id');                

        $this->ViewData['json'] = true;
        $this->ViewData['sector'] =  $this->approvalsSuggestService->getByCodParam($this->user['id_company_pk'], $codEstab, self::COD_SECTOR);
        $this->ViewData['reason'] =  $this->approvalsSuggestService->getByCodParam($this->user['id_company_pk'], $codEstab, self::COD_REASON);
        $this->ViewData['buyer'] =  $this->approvalsSuggestService->getByCodParam($this->user['id_company_pk'], $codEstab, self::COD_BUYER);
        $this->ViewData['deposit'] =  $this->approvalsSuggestService->getDeposit($this->user['id_company_pk'], $codEstab);

        return $this->ViewData;
    }

    public function delData()
    {
        $this->post = Request::post();

        $id_company = $this->user['id_company_pk'];
        $id_ps = $this->post['id_ps'];
        $id_int = $this->post['id_int'];
        $num_par = $this->post['num_par'];
        
        $this->ViewData['delete'] = $this->approvalsSuggestService->deleteItem($id_company, $id_ps, $num_par, $id_int);

        $this->ViewData['json'] = true;

        return $this->ViewData;
    }

    /**
     * @param $data
     * @param $cols
     * @return mixed
     */
    private function prepareDataFooter(array $data, array $cols) : array
    {
        return array_reduce(array_keys($cols), function ($total_footer, $col) use ($data) {
            $total_footer[$col] = null;

            if (key_exists($col, $data)) {
                $total_footer[$col] = $data[$col];
            }

            return $total_footer;
        }, []);
    }
}
