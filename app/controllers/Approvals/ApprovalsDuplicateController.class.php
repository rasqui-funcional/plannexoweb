<?php
/**
 * <b>ApprovalsDuplicateController</b>
 * Controller responsible for managing Suggest methods
 *
 * @copyright (c) 2018 | Bionexo
 */


class ApprovalsDuplicateController extends Fields
{

    private $ViewData;
    private $post;
    private $user;
    private $order;
    private $approvalsDuplicateService;

    public function __construct()
    {
        parent::__construct();
        $this->user = Login::getUserSession();
        $this->approvalsDuplicateService = new ApprovalsDuplicateService();
        $this->approvalSuggestService = new ApprovalsSuggestService();
    }

    public function save()
    {
        try{
            $this->ViewData['json'] = true;
            $this->post = Request::post();
            $this->ViewData['OLD_COD_HEADER_PK'] = $this->post['cod_header_pk'];
            $this->ViewData['COD_ESTAB_FK'] = $this->post['estab'];
    
            $this->ViewData['purchase'] = [
                "COD_BUYER" => $this->post["COD_BUYER"],
                "COD_LOCAL_FK" => $this->post["COD_LOCAL_FK"],
                "COD_SECTOR" => $this->post["COD_SECTOR"],
                "DT_EXP_REQ" => $this->post["DT_EXP_REQ"],
                "COD_REQ_REASON" => $this->post["COD_REQ_REASON"],
                "OBS_REQ" => $this->post["OBS_REQ"],
                "YN_URGENT" => isset($this->post["YN_URGENT"]) ? "Y" : "N"
            ];

            $validate = $this->approvalsDuplicateService->validatePurchase($this->post['purchase_suggestions']);
            if($validate['duplicated_date'] || $validate['qty_zero'])
            {
                throw new Exception('Não foi possível duplicar. Pedido com quantidades zeradas ou datas iguais para mesmo produto');
            }
    
            $this->ViewData['COD_HEADER_PK'] = $this->approvalsDuplicateService->insertPurchase()[0]['COD_HEADER_PK'];
    
            $this->approvalsDuplicateService->updatePurchase(
                $this->ViewData['OLD_COD_HEADER_PK'],
                $this->ViewData['COD_HEADER_PK'],
                $this->user['id_company_pk'],
                $this->ViewData['COD_ESTAB_FK'],
                $this->ViewData['purchase']
            );
    
            $this->approvalsDuplicateService->insertPurchaseHistory(
                $this->user,
                $this->ViewData['COD_HEADER_PK'],
                $this->ViewData['OLD_COD_HEADER_PK'],
                $this->post['purchase_suggestions']
            );
    
            $this->approvalsDuplicateService->updatePurchaseStatus(
                $this->user['id_company_pk'],
                $this->ViewData['COD_HEADER_PK']
            );
    
            $url = "/" . $this->user['lang'] . "/approvals/list";
            header("Location: $url");
        }catch (\Exception | Throwable $thrw)
        {
            $url = "/" . $this->user['lang'] . "/approvals/list";
            $Message = urlencode("Some error occured please try after some time ");
            header("Location: $url");
        }
    }

    public function getTable()
    {
        $this->post = Request::post();
        $this->ViewData['ajax'] = true;

        if (!is_null($this->post['order'])) {
            Redis::Set(USERSESSIONID . ':session:approvals:duplicate:order-data', json_encode($this->post['order']));
        }

        $this->order = (isset($this->post['order']) && $this->post['order'] ? $this->prepareOrder($this->post['order']) : "");
        $offset = $this->prepareSetOffset($this->post['page'], $this->post['limit']);
        $this->ViewData['table'] = $this->approvalsDuplicateService->selectDuplicateItems($this->user['id_company_pk'], $this->post['codHeaderPk'], $this->order);
        $this->ViewData['raw'] = $this->approvalsDuplicateService->select($this->user['user_id'], $this->user['id_company_pk'], $this->post['codHeaderPk'])['data'];
        $this->ViewData['table']['columns'] = $this->prepareCols($this->ViewData['table']['columns']);
        $this->ViewData['footer']['count'] = count($this->ViewData['raw']);
        $this->ViewData['user'] = $this->user;
        $this->ViewData['footer']['pages'] = Utilities::calculatePages($this->ViewData['footer']['count'], $this->post['limit']);

        return $this->ViewData;
    }

    private function formatPost($post)
    {
        $return = [];
        $i = 0;
        foreach ($post['QTY_ALTERED'] as $rows) {
            foreach ($post as $key => $value) {
                $return[$i][$key] = $value[$i];
            }
            $i++;
        }
        return $return;
    }

    private function prepareCols($columns)
    {
        $result = array();
        foreach ($columns as $name) {
            $printname = strtoupper($name);

            if ($printname == 'RNUM' || $printname == 'ID_INTEGRATION_GTP') {
                continue;
            }

            $result[$printname] = [
                'order' => $this->getColOrder($printname)
            ];
        }

        return $result;
    }

    private function getColOrder($colName)
    {
        if (isset($this->post['order']) && $this->post['order']) {
            $orders = $this->post['order'];
        } elseif (Redis::Get(USERSESSIONID . ':session:approvals:duplicate:order-data')) {
            $orders = Redis::Get(USERSESSIONID . ':session:approvals:duplicate:order-data');
            $colName = strtolower($colName);
        } else {
            return null;
        }


        if ($orders) {
            $keySearch = key_exists($colName, $orders);
        }

        if ($keySearch) {
            return $orders[$colName];
        }
    }

    private function prepareSetOffset($Page, $Limit)
    {
        return ($Page - 1) * $Limit;
    }

    private function prepareOrder($Orders)
    {

        end($Orders);
        $End = key($Orders);
        $OrderQuery = "ORDER BY ";
        foreach ($Orders as $Columns => $ColOrder) {
            $OrderQuery .= "{$Columns} {$ColOrder}";
            if ($End != $Columns) {
                $OrderQuery .= ", ";
            }
        }
        return $OrderQuery;
    }

}
