<?php

/**
 * <b>CSVController</b>
 * Controller responsible for managing CSV methods
 *
 * @copyright (c) 2018 Wallace | Bionexo
 */
class CSVController extends Fields
{
    private $user;
    private $ViewData;

    public function __construct()
    {
        parent::__construct();
        $this->user = Login::getUserSession();
        $this->allowedTables = [
            'overstock',
            'PurchaseOrder',
            'PurchaseRequest',
            'InTransaction',
            'OutTransaction',
            'MonthlyConsumption',
        ];
    }

    public function getPurchaseOrderGroupedCSV()
    {
        $this->ViewData['ajax'] = true;
        $purchaseOrder = new PurchaseOrderGroupedService($this->user['id_company_pk']);

        $this->ViewData['table'] = $purchaseOrder->csvData();

        if ($this->ViewData['table'] == null) {
            $this->ViewData['error'] = $this->lang['csv']['error_create'];
            return $this->ViewData;
        }

        return $this->ViewData;
    }

    public function getPurchaseOrderCSV()
    {
        $this->ViewData['ajax'] = true;
        $purchaseOrder = new PurchaseOrderListService($this->user['id_company_pk'], $this->getNewDinamicFieldsGrouping(0));

        $this->ViewData['table'] = $purchaseOrder->getPurchaseOrderCSV();

        if ($this->ViewData['table'] == null) {
            $this->ViewData['error'] = $this->lang['csv']['error_create'];
            return $this->ViewData;
        }

        return $this->ViewData;
    }

    /**
     * Função para pegar o cabeçalho da tabela de acordo com os campos usados
     * @param array $dataTitles
     * @param array $titles
     * @return array
     */
    public function filterTitles($dataTitles, $titles)
    {
        $filteredTitles = [];
        foreach($dataTitles as $dataTitle)
        {
            $filteredTitles[$dataTitle] = $titles[$dataTitle];
        }
        return $filteredTitles;
    }

    public function getCsvByTable()
    {
        $this->ViewData['ajax'] = true;

        $table = Request::get('table');
        $skuItem = Request::get('skuItem');
        $codEstabPk = Request::get('codEstabPk');
        $orderBy = '';

        if(Request::get('orderCol')){
            $orderBy = 'ORDER BY '.Request::get('orderCol').' '.Request::get('orderType');
        }
        $this->ViewData['table'] = $this->$table($skuItem, $this->user['id_company_pk'], $codEstabPk, $orderBy);
        $dataTitles = array_keys($this->ViewData['table']['data'][0]);
        $this->ViewData['table']['filteredTitles'] = $this->filterTitles($dataTitles, $this->ViewData['table']['titles']);

        if ($this->ViewData['table'] == null) {
            $this->ViewData['error'] = $this->lang['csv']['error_create'];
            return $this->ViewData;
        }

        Request::generateCSVHeaders($table);
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        fputcsv($output, mb_convert_encoding($this->ViewData['table']['filteredTitles'], 'ISO-8859-1'), ';');

        // fetch the data
        $rows = $this->ViewData['table'];
        // loop over the rows, outputting them
        foreach ($this->ViewData['table']['data'] as $row) {
            foreach ($dataTitles as $title) {
                $row[$title] = Utilities::formatCsvFields($title, $row[$title]);
            }
            fputcsv($output, mb_convert_encoding($row, 'ISO-8859-1'), ';');
        }

        return $this->ViewData;
    }

    private function prepareOrderByCsv($Orders)
    {
        end($Orders);
        $End = key($Orders);

        $OrderQuery = "ORDER BY ";
        foreach ($Orders as $Columns => $ColOrder) {
            $OrderQuery .= "{$Columns} {$ColOrder}";
            if ($End != $Columns) {
                $OrderQuery .= ", ";
            }
        }

        return $OrderQuery;
    }

    public function getApprovalsCSV()
    {
        $order = 'order by COD_HEADER_PK desc';
        $Orders = json_decode(Redis::Get(USERSESSIONID . ':session:approvals:list:order-data'));
        if($Orders)
        {
            $order = $this->prepareOrderByCsv($Orders);
        }
        $this->ViewData['ajax'] = true;
        $collumn = Request::get('collumn');
        $value = Request::get('search');
        $approvalsService = new ApprovalsService($this->user);

        $this->ViewData['table'] = $approvalsService->select($order, $this->user['user_id'], $this->user['id_company_pk'], $value, $collumn);

        if ($this->ViewData['table'] == null) {
            $this->ViewData['error'] = $this->lang['csv']['error_create'];
            return $this->ViewData;
        }

        return $this->ViewData;
    }

    /**
     * <b>getListCsv</b>
     * Receive the post from ordercover/suggestions/list and prepares query for csv.
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function getSuggestionsCSV()
    {
        $this->ViewData['ajax'] = true;
        $serviceOrderCoverSuggest = new OrderCoverSuggestionsService($this->user['id_company_pk']);
        $order = Redis::Get(USERSESSIONID . ":session:ordercover:suggestions:order");
        $where = Redis::Get(USERSESSIONID . ":session:ordercover:suggestions:where");
        $terms = $where . json_decode($order);

        $this->ViewData['table'] = $serviceOrderCoverSuggest->getSuggestionsToCsv($terms, $this->user['user_id'],
            $this->user['id_company_pk'], $search);

        if ($this->ViewData['table'] == null) {
            $this->ViewData['error'] = $this->lang['csv']['error_create'];
            return $this->ViewData;
        }

        return $this->ViewData;
    }

    public function getApprovalsSuggestCSV()
    {
        $this->ViewData['ajax'] = true;
        $id = Request::get('id');
        $column = (Request::get('column') == '0') ? '' : Request::get('column');
        $value = Request::get('value') == 0 ? '' : Request::get('value');

        $approvalsSuggestService = new ApprovalsSuggestService();

        $this->ViewData['table'] = $approvalsSuggestService->select("", $this->user['user_id'],
            $this->user['id_company_pk'], $id, $column, $value);

        if ($this->ViewData['table'] == null) {
            $this->ViewData['error'] = $this->lang['csv']['error_create'];
            return $this->ViewData;
        }

        return $this->ViewData;
    }

    public function getSkuLogCSV()
    {
        $this->ViewData['ajax'] = true;
        $skuId = Request::get('skuid');

        $this->ViewData['table'] = (new LogService($this->user['id_company_pk'], null))
            ->getTableFullDataBySkuId($skuId);

        if ($this->ViewData['table'] == null) {
            $this->ViewData['error'] = $this->lang['csv']['error_create'];
            return $this->ViewData;
        }

        return $this->ViewData;
    }

    /**
     * Retorna a string que complementa a busca no banco com a ordenação
     * @param string $field
     * @return string
     */
    private function prepareSearch(string $field)
    {
        $search = '';
        $reqSearch = Request::get('search');
        if (isset($reqSearch) && !empty($reqSearch)) {
            $search = " AND {$field} LIKE'%{$reqSearch}%'";
        }
        
        return $search;
    }

    private function overstock($skuItem, $idCompanyPk, $codEstabPk, $order = '')
    {
        $stockBalance = new StockBalanceService();

        $result = $stockBalance->select($skuItem, $idCompanyPk, $codEstabPk, $order);

        $result['data'] = $this->prepareOverStock($result['data']);

        return $result;
    }

    private function prepareOverStock($rows)
    {
        foreach ($rows as $rowKey => $rowValues) {
            $rows[$rowKey]['DESC_LOCAL'] = $rows[$rowKey]['COD_LOCAL'] . ' - ' . $rows[$rowKey]['DESC_LOCAL'];
            unset($rows[$rowKey]['COD_LOCAL']);
        }
        return $rows;
    }

    private function PurchaseOrder($skuItem, $idCompanyPk, $codEstabPk, $order = '')
    {
        $purchaseOrderService = new PurchaseOrderService();
        $search = $this->prepareSearch('NUM_ORDER');
        $order = ($order === '') ? 'ORDER BY DATE_EXP DESC' : $order;
        return $purchaseOrderService->select($skuItem, $idCompanyPk, $codEstabPk, '', $order, '', $search);
    }

    private function purchaserequest($skuItem, $idCompanyPk, $codEstabPk, $order = '')
    {
        $purchaseRequest = new PurchaseRequestService();
        $search = $this->prepareSearch('p.COD_LOCAL');
        $order = ($order === '') ? 'ORDER BY DATE_EXP DESC' : $order;
        return $purchaseRequest->select($skuItem, $idCompanyPk, $codEstabPk, $order, $search);
    }

    private function intransaction($skuItem, $idCompanyPk, $codEstabPk, $order = 'ORDER BY DATE_TRANS DESC')
    {
        $order = ($order !== '')?$order: 'ORDER BY DATE_TRANS DESC';
        $intransaction = new InTransactionService();
        return $intransaction->select($skuItem, $idCompanyPk, $codEstabPk, Request::get('search'), $order);
    }

    /**
     * Seguindo a regra de ERPActivityController
     * outtransaction (transacao de saida) só pode ser ordenado por DATE_TRANS_ALT
     * se for diferente, ele é alterado para DATE_TRANS_ALT DESC
     */
    private function outtransaction($skuItem, $idCompanyPk, $codEstabPk, $order = 'ORDER BY DATE_TRANS_ALT DESC')
    {
        $order = ($order !== '')?$order: 'ORDER BY DATE_TRANS_ALT DESC';
        $outtransaction = new OutTransactionService();
        return $outtransaction->select($skuItem, $idCompanyPk, $codEstabPk, $order);
    }

    private function monthlyconsumption($skuItem, $idCompanyPk, $codEstabPk, $order = '')
    {
        $monthlyconsumption = new MonthlyConsumptionService();
        return $monthlyconsumption->select($skuItem, $idCompanyPk, $codEstabPk, $order);
    }
}
