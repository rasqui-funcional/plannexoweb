<?php

class OrderCoverSuggestionsController
{
    const ORDER_SUGGEST = USERSESSIONID . ':log:order_cover:suggest:result';

    private $view_data;
    private $query;
    private $post;
    private $order;
    private $user;
    private $serviceAuditPrepareData;

    public function __construct()
    {
        $this->user = Login::getUserSession();
        $this->serviceOrderCoverSuggest = new OrderCoverSuggestionsService($this->user['id_company_pk']);
        $this->serviceOrderFooter = new OrderCoverFooterService();
        $this->serviceAuditPrepareData = new AuditPrepareData();
    }

    public function getList()
    {
        return true;
    }

    /**
     * <b>getList</b>
     * Receive the post from ordercover/filters and prepares query for save in cache(Redis).
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function prepareList()
    {
        $post = Request::post();
        Logger::info(__CLASS__ . "::prepareList", ['data-post'=> $post]);
        $where = $this->getMainWhere();
        if ($post['obj']['psug_grouped_pk'] == '-1') {
            $where .= DB::setFields($post['obj']);
        } else {
            $where .= " AND psug_grouped_pk = {$post['obj']['psug_grouped_pk']}";
        }

        return Redis::Set(USERSESSIONID . ":session:ordercover:suggestions:where", $where);
    }

    /**
     * <b>getTable</b>
     * Build the ordercover table array.
     *
     * @return array
     */
    public function getTable()
    {
        $post = Request::post();
        Logger::info(__CLASS__ . "::getTableOrderCover", ['data-post'=> $post]);

        $view_data['data_table'] = $this->serviceOrderCoverSuggest->getSuggestionsData($post);
        if ($view_data['data_table']) {
            Redis::HSet(
                self::ORDER_SUGGEST,
                ['data' => serialize($this->serviceAuditPrepareData->orderCoverSuggestions($view_data['data_table']))]
            );
        }

        $view_data['order'] = $post['order'];
        $view_data['footer']['count'] = $this->serviceOrderCoverSuggest->getQtyTotalSuggestions();
        $view_data['footer']['pages'] = Utilities::calculatePages($view_data['footer']['count'], $post['limit']);
        $view_data['footer']['data'] = $this->serviceOrderFooter->getSuggestionsFooterData();

        $view_data['ajax'] = true;

        return $view_data;
    }

    public function updateSuggestions()
    {
        $post = Request::post();
        Logger::info(__CLASS__ . "::updateSuggestions", ['data-post'=> $post]);

        $view_data['json'] = true;

        foreach ($post['obj'] as $row => $data_row) {
            unset($data_row['COD_ITEM_PK']);
            $suggestion_id = $data_row['ID'];
            $delivery_date = $data_row['DATE_ARRIVAL_ALTERED'];
            $obs = $data_row['OBS'];
            $data = $this->prepareSuggestionsUpdateData($data_row);

            if (!$this->serviceOrderCoverSuggest->updateSuggestions($data, $suggestion_id, $delivery_date, $obs)) {
                $view_data['data'] = false;
                return $view_data;
            }

            Log::new(
                self::ORDER_SUGGEST,
                'id_sug_pk',
                'ordercover:suggestions:list',
                'update',
                (int)$suggestion_id,
                $this->serviceOrderCoverSuggest->buildOrderCoverSuggest($data_row)
            );
        }

        $view_data['data'] = true;
        return $view_data;
    }

    public function removeSuggestions()
    {
        $post = Request::post();
        Logger::info(__CLASS__ . "::removeSuggestions", ['data-post'=> $post]);

        $view_data['json'] = true;

        foreach ($post['obj'] as $row => $data_row) {
            unset($data_row['COD_ITEM_PK']);
            $id_psug = $data_row['ID_PSUG_PK'];
            $id_sug = $data_row['ID_SUG'];
            $data = [
                'SIT_SAVED' => 0,
            ];
            //remove do agrupamento
            $this->serviceOrderCoverSuggest->deleteSuggestions($data_row);
            //desaprova a sugestão
            if (!$this->serviceOrderCoverSuggest->removeSuggestions($data, $id_psug, $id_sug)) {
                $view_data['data'] = false;
                return $view_data;
            }

            Log::new(
                self::ORDER_SUGGEST,
                'id_sug_pk',
                'ordercover:suggestions:list',
                'update',
                (int)$id_psug,
                $this->serviceOrderCoverSuggest->buildOrderCoverSuggest($data_row)
            );
        }

        $view_data['data'] = true;
        return $view_data;
    }

    public function deleteSuggestions()
    {
        $post = Request::post();

        $view_data['json'] = true;

        foreach ($post['obj'] as $row => $data_row) {
            if (!$this->serviceOrderCoverSuggest->deleteSuggestions($data_row)) {
                $view_data['data'] = false;
                return $view_data;
            }
        }

        $view_data['data'] = true;

        return $view_data;
    }

    private function prepareSuggestionsUpdateData($data_row)
    {
        unset($data_row['ID'], $data_row['DATE_ARRIVAL_ALTERED'], $data_row['OBS'], $data_row['ID_SKU_PK']);

        return $data_row;
    }

    /**
     * @return mixed
     */
    private function getMainWhere()
    {

        return str_replace(['a.', '_filtro'], ['', ''], Redis::Get(USERSESSIONID . ':session:ordercover:list:query-basic'));
    }
}
