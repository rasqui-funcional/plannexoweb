<?php

class OrderCoverFiltersController extends Fields
{
    public function __construct()
    {
        parent::__construct();
        $this->session = Login::getUserSession();
        $this->serviceFilter = new OrderCoverFilterService($this->session['id_company_pk']);
        $this->serviceCalendar = new CalendarService($this->session['id_company_pk']);
    }

    public function getFilters()
    {
        $viewData = $this->serviceFilter->getFiltersFields();
        $viewData['select_filtro'] = $this->serviceFilter->getSavedFilters();
        $viewData['calendars'] = $this->serviceCalendar->getCalendarsbyCompany();
        $viewData['SearchFields'] = $this->getNewDinamicFieldsGrouping();

        return $viewData;
    }
}
