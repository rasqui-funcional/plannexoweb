<?php

class OrderCoverListController
{
    private $view_data;
    private $query;
    private $post;
    private $order;
    private $user;
    private $cols_header_fixed = [];

    public function __construct()
    {
        $this->user = Login::getUserSession();
        $this->post = Request::post();
        $this->serviceOrderList = new OrderCoverListService($this->user);
        $this->serviceOrderFooter = new OrderCoverFooterService();

        $this->cols_header_fixed = [
            'cod_estab_fk' => ['name' => null]
            ,'TOTAL_SUGS' => [
                'name' => null
                ,'total' => 'TOTAL_SUGS'
            ]
            ,'comprador' => ['name' => 'comprador']
            ,'setor' => ['name' => 'setor']
            ,'col_deposito' => ['name' => 'col_deposito']
            ,'col_motivo' => ['name' => 'col_motivo']
            ,'total_qty' => [
                'name' => null
                ,'total' => 'TOTAL_QTY'
            ]
            ,'TOTAL_PRICE' => [
                'name' => null
                ,'total' => 'TOTAL_PRICE'
            ]
            ,'DATE_ARRIVAL_ALTERED' => ['name' => null]
            ,'col_urgente' => ['name' => 'col_urgente']
            ,'col_obs' => ['name' => 'col_obs']
        ];
    }

    public function processOrderCover()
    {
        $view_data['data'] = $this->serviceOrderList->processSuggestions($this->post['obj']);
        $view_data['json'] = true;

        return $view_data;
    }

    public function setGroup()
    {
        $view_data['data'] = $this->serviceOrderList->agroupSuggestions($this->post['definitions'], $this->post['obj']);
        $view_data['json'] = true;

        return $view_data;
    }

    /**
     * <b>getList</b>
     * Receive the post from ordercover/filters and prepares query for save in cache(Redis).
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function getList()
    {
        $cache_check = $this->serviceOrderList->checkOrderCoverCache();
        if (!$this->post && !$cache_check) {
            return Request::redirect('ordercover/filters');
        }

        $view_data['header_cols'] = $this->checkColsFrom($cache_check);
        $this->serviceOrderList->preparePagination($this->post);

        return $view_data;
    }

    /**
     * <b>getTable</b>
     * Build the ordercover table array.
     *
     * @return array
     */
    public function getTable()
    {
        $view_data['data_table'] = $this->serviceOrderList->getGroupedSuggestions($this->post);
        
        $total_suggestions = $this->serviceOrderList->getQtyTotalSuggestions();
        $view_data['header_cols_fixed'] = array_merge($this->serviceOrderList->getHeaderCols(), $this->cols_header_fixed);
        $view_data['header_cols'] = $this->serviceOrderList->getHeaderCols();
        $view_data['order'] = $this->post['order'];
        $view_data['has_date'] = $this->serviceOrderList->hasDate();
        $view_data['footer']['pages'] = Utilities::calculatePages($total_suggestions, $this->post['limit']);
        $view_data['footer']['count'] = $total_suggestions;
        $view_data['footer']['data'] = $this->serviceOrderFooter->getGroupedFooterData();

        $view_data['ajax'] = true;        
        return $view_data;
    }

    protected function checkColsFrom($cache)
    {
        return (!$this->post && $cache)
            ? $this->serviceOrderList->getHeaderColsFromCache()
            : $this->serviceOrderList->prepareHeaderCols($this->post);
    }
}
