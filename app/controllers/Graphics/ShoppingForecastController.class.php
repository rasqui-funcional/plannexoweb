<?php

class ShoppingForecastController {

    private $user_session;

	public function __construct(){
		$this->user_session = Login::getUserSession();
		$this->serviceDaily = new ShoppingForecastServiceDaily($this->user_session['id_company_pk']);
		$this->serviceMonthly = new ShoppingForecastServiceMonthly($this->user_session['id_company_pk']);
    }
    
    /**
     * getDataDaily
     * Get the data used on Shopping Forecast (Previsão de Compras) chart 
     * @return JSON
     */
    public function getDataDaily()
    {
        try{
            $Data = $this->serviceDaily->buildChartData();
            $Data['json'] = true;
    
            return $Data;
        } catch (\Exception | Throwable $thrw)
        {
            header('HTTP/1.1 '.$thrw->getCode().' Internal Server Error');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => $thrw->getMessage())));
        }
    }

    /**
     * getDataMonthly
     * Get the data used on Shopping Forecast (Previsão de Compras) chart 
     * @return JSON
     */
    public function getDataMonthly()
    {
        try{
            $Data = $this->serviceMonthly->buildChartData();
            $Data['json'] = true;
    
            return $Data;
        } catch (\Exception | Throwable $thrw)
        {
            header('HTTP/1.1 '.$thrw->getCode().' Internal Server Error');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => $thrw->getMessage())));
        }
    }
}
