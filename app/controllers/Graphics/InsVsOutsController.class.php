<?php

class InsVsOutsController
{
    private $ins_vs_outs_service;
    private $user_session;

    public function __construct()
    {
        $this->ins_vs_outs_service = new InsVsOutsService;
        $this->user_session = Login::getUserSession();
    }

    /**
     * <b>getInsVsOutsjsondata</b>
     * Recupera os dados necessários para o preenchimento de posição de entradas e saídas.
     * @return JSON
     */
    public function getInsVsOuts()
    {
        $ViewData['json'] = true;
        $this->where = Redis::Get(USERSESSIONID . ":session:dashboard:filters-query");
        $manager_users = Redis::Get(USERSESSIONID . ':session:dashboard:manager-users');

        $insVsOutsService = new InsVsOutsService;        
        $ViewData['insVsOuts'] = $insVsOutsService->buildInsVsOuts($this->user_session, $manager_users);
        $ViewData['insVsOutsPerWeek'] = $insVsOutsService->buildInsVsOutsPerWeek(
            $this->user_session,
            $manager_users
        );

        return $ViewData;
    }

    private function getInsAndOutsData()
    {
        $fields = "to_char(trunc(cal.date_calendar_pk,'MM'), 'DD/MM/YYYY') AS period_date,
		SUM(m.qty_inv_total * m.val_unit) AS estoque,
		SUM(m.qty_in * m.val_unit) AS entrada,
		SUM(m.qty_out * m.val_unit) AS saida";

        $table = "aux_calendar cal
		LEFT JOIN (
			SELECT
				m.date_trans,
				m.qty_inv_total,
				m.qty_in,
				m.qty_out,
				m.val_unit,
				m.id_company_fk
			FROM vw_scm_aux_sku a 
			INNER JOIN scm_sku_month_by_month m 
			  ON m.id_company_fk = a.id_company_fk
			  AND m.id_sku_fk = a.id_sku_pk
			WHERE
				a.sit_sku = 1
		) m
		ON trunc(cal.date_calendar_pk,'MM') = m.date_trans";

        $terms = "WHERE cal.date_calendar_pk BETWEEN current_date - 365 AND trunc(current_date,'MM') - 1 {$this->where}
		GROUP BY
			trunc(cal.date_calendar_pk,'MM')
		ORDER BY
			trunc(cal.date_calendar_pk,'MM') ASC";


        return DB::Select($table, $fields, $terms);
    }
}
