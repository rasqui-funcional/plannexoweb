<?php

/**
 * Class AdhesionController
 */
class AdhesionController
{
    /**
     * @var AdhesionService
     */
    private $adhesion;

    /**
     * @var string
     */
    private $user_session;

    /**
     * AdhesionController constructor.
     */
    public function __construct()
    {
        $this->user_session = Login::getUserSession();
        $service = new AdhesionService($this->user_session['id_company_pk']);
        $this->adhesion = new AdhesionGraphic($service);
    }


    /**
     * @return mixed
     */
    public function getAdhesion()
    {
        return [
            'json' => true,
            'adhesion' => $this->adhesion->buildAdhesion()
        ];
    }

}
