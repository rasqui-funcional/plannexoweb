<?php
/**
 * <b>ServiceLevelController</b>
 * Controller responsible for managing the Service Level Graph data
 * 
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class ServiceLevelController {
	private $user;

	public function __construct(){
		$this->user = Login::getUserSession();
		$this->serviceLevelService = new ServiceLevelService();
	}

	/**
     * <b>getServiceLevel</b>
     * Retrieve the serviceLevelService level data to fill the graph. 
     * 
     * @return JSON
     */
	public function getServiceLevel(){		
		$where = Redis::Get(USERSESSIONID . ":session:dashboard:filters-query");
		$ViewData['json'] = true;
		$ViewData['skus'] = $this->serviceLevelService->getServiceLevel($this->user, $where);
		return $ViewData;		
    }
}