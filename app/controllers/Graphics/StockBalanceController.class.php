<?php
/**
 * <b>StockBalanceController</b>
 * Controller responsible for managing the Stock Balance Graph data
 * 
 * @copyright (c) 2018, Wallace Randal | Bionexo
 */
class StockBalanceController {
	private $stockBalanceService;
	private $user;
    private $manager_users = null;

    /**
     * <b>construct</b>
     * initializes the var stockBalanceService
     */
	public function __construct()
    {
		$this->user = Login::getUserSession();
		$this->stockBalanceService = new StockBalanceService();
	}
	
	/**
     * <b>getStockBalance</b>
     * Retrieve the stock balance data to fill the graph. 
     * 
     * @return JSON
     */
	public function getStockBalance()
    {
		try{
			$where = Redis::Get(USERSESSIONID . ":session:dashboard:filters-query");
			$ViewData['json'] = true;
			$ViewData['stockbalance'] = $this->stockBalanceService->getStockBalance($this->user, $where);
	
			return $ViewData;
        } catch (\Exception | Throwable $thrw)
        {
            header('HTTP/1.1 '.$thrw->getCode().' Internal Server Error');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => $thrw->getMessage())));
        }
	}

	/**
     * <b>getInfoSkuBalancing</b>
     * Retrieve the stock balance data to fill the graph on the Info SKU modal. 
     * 
     * @return JSON
     */
	public function getInfoSkuBalancing()
    {
		$id = Request::get('skuId');
		$ViewData['json'] = true;

		if(!is_numeric($id) || empty($this->user)) {
			$ViewData['stock_balance'] = [
				'HIGH' => 0,
				'HIGH_VALUE' => 0,
				'LOW' => 0,
				'LOW_VALUE' => 0,
				'TOTAL' => 0,
				'TOTAL_VALUE' => 0,
				'TOTAL_DAYS' => 0,
				'AVAILABLE' => 0,
				'AVAILABLE_VALUE' => 0,
				'AVAILABLE_DAYS' => 0,
				'TOTAL_EXCESS' => 0,
				'TOTAL_EXCESS_VALUE' => 0,
				'AVAILABLE_EXCESS' => 0,
				'AVAILABLE_EXCESS_VALUE' => 0,
				'IDEAL_PREDICTION' => 0,
			];

			$ViewData['details_stock_balancing'] = [
				'minimum' => [
					'INV_LEVEL' => "0",
					'SALDO' => "0",
					'COBERTURA' => "0",
				],
				'maximum' => [
					'INV_LEVEL' => "0",
					'SALDO' => "0",
					'COBERTURA' => "0",
				]
			];

			return $ViewData;
		}

		$where = "AND ID_SKU_PK = $id";
		$ViewData['stock_balance'] = $this->stockBalanceService->getStockBalanceLabelInfo($this->user['id_company_pk'], $where);
		$ViewData['details_stock_balancing'] = $this->stockBalanceService->detailStockBalancing($this->user['id_company_pk'], $where);
		
		return $ViewData;
	}
}