<?php

/**
 * Class UrgencyController
 */
class UrgencyController
{
    /**
     * @var UrgencyService
     */
    private $urgency;

    /**
     * @var string
     */
    private $user_session;

    /**
     * UrgencyController constructor.
     */
    public function __construct()
    {
        $this->user_session = Login::getUserSession();
        $service = new UrgencyService($this->user_session);
        $this->urgency = new UrgencyGraphic($service);
    }


    /**
     * @return mixed
     */
    public function getUrgency()
    {
        return [
            'json' => true,
            'urgency' => $this->urgency->buildUrgency()
        ];
    }

}
