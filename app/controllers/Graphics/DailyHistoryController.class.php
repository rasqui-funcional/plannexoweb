<?php

class DailyHistoryController
{
    private $user_session;

    public function __construct()
    {
        $this->user_session = Login::getUserSession();
        $this->service = new DailyHistoryService($this->user_session['id_company_pk'], $this->user_session['lang']);
    }

    public function getData()
    {
        $data = $this->service->buildData();
        $data['json'] = true;
        
        return $data;
    }
}
