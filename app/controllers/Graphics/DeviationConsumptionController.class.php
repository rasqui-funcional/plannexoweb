<?php

class DeviationConsumptionController
{
	private $ins_vs_outs_service;
	private $user_session;

	public function __construct()
    {
		$this->user_session = Login::getUserSession();
		$this->service = new DeviationConsumptionService($this->user_session['id_company_pk']);
    }

    public function getData(){
		$data['dataChart'] = $this->service->buildChartData();
		$data['dataMargin'] = $this->service->buildMarginDeviation();
		$data['json'] = true;
		
		return $data;
	}
}

?>
