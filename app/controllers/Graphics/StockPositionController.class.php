<?php

class StockPositionController
{

    const SESSION_TIME_LIFE = 3600*4;
    private $stockPositionService;
    private $sku_id;
    private $user;

    /**
     * <b>construct</b>
     * initializes the var graphicService
     */
    public function __construct()
    {
        $this->stockPositionService = new StockPositionService();
        $this->user = Login::getUserSession();
    }

    /**
     * <b>getBalancingJsondata</b>
     * Retrieve data for Balancing Graphic
     * @return JSON
     */
    public function getStock()
    {

        $where = Redis::Get(USERSESSIONID . ":session:dashboard:filters-query");
        $check = Request::get('check');
        $ViewData['json'] = true;

        $stockposA = $this->generateKey($where);
        $ViewData['stockposA'] = json_decode(Redis::Get($stockposA));

        if (!$check || $check == 'false' || !$ViewData['stockposA']) {
            $ViewData['stockposA'] = $this->stockPositionService->getStock($this->user, $where, 'annual');
            Redis::Set($stockposA, json_encode($ViewData['stockposA']), self::SESSION_TIME_LIFE);
        }

        return $ViewData;
    }

    public function getStockWeekly()
    {

        $where = Redis::Get(USERSESSIONID . ":session:dashboard:filters-query");
        $check = Request::get('check');
        $ViewData['json'] = true;

        $stockposT = $this->generateKey($where, 'stockposT');

        $ViewData['stockposT'] = json_decode(Redis::Get($stockposT));

        if (!$check || $check == 'false' || !$ViewData['stockposT']) {
            $ViewData['stockposT'] = $this->stockPositionService->getStock($this->user, $where, 'weekly');
            Redis::Set($stockposT, json_encode($ViewData['stockposT']), self::SESSION_TIME_LIFE);
        }

        return $ViewData;
    }

    public function getStockbySku()
    {
        $where = $this->getSkuWhere();

        if ($where) {
            $stockposA = $this->generateKey($where);
            $ViewData['stockposA'] = json_decode(Redis::Get($stockposA));

            if (!$ViewData['stockposA']) {
                $ViewData['stockposA'] = $this->stockPositionService->getStock($this->user, $where, 'annual');
                Redis::Set($stockposA, json_encode($ViewData['stockposA']), self::SESSION_TIME_LIFE);
            }
        } else {
            $ViewData = $this->stockPositionService->mockNullData();
        }

        $ViewData['json'] = true;

        return $ViewData;

    }

    public function getStockWeeklybySku()
    {
        $where = $this->getSkuWhere();

        if ($where) {
            $stockposT = $this->generateKey($where, 'stockposT');
            $ViewData['stockposT'] = json_decode(Redis::Get($stockposT));

            if (!$ViewData['stockposT']) {
                $ViewData['stockposT'] = $this->stockPositionService->getStock($this->user, $where, 'weekly');
                Redis::Set($stockposT, json_encode($ViewData['stockposT']), self::SESSION_TIME_LIFE);
            }
        } else {
            $ViewData = $this->stockPositionService->mockNullData();
        }

        $ViewData['json'] = true;

        return $ViewData;

    }

    /**
     * *******************************************
     * ****************PRIVATE********************
     * *******************************************
     */


    private function getSkuWhere()
    {

        $this->sku_id = (int)Request::get('skuID');

        if ($this->sku_id) {
            return "AND id_sku_pk = {$this->sku_id}";
        } else {
            return false;
        }
    }


    private function generateKey($key, $period = 'stockposA')
    {
        $key = preg_replace('/\s(?=\s)/', '', ($key));
        $key = preg_replace("/[[:punct:]]/", "", $key);
        $key = strtr(($key), (("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ")), "aaaaeeiooouucAAAAEEIOOOUUC");
        $key = preg_replace("/[^a-zA-Z0-9]/", "-", $key);

        $key = strtolower((trim(($key))));
        return Login::getUserSessionByKey('id_company_pk') . '-' . $period . $key;
    }

}