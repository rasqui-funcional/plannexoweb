<?php

class PurchaseOrderGroupedController
{
    private $post;
    private $user;
    const SESS_NAME_DATA_FILTERS = USERSESSIONID . ':session:purchaseorder:list:filters';

    public function __construct()
    {
        $this->user = Login::getUserSession();
        $this->post = Request::post();
        $this->serviceOrderList = new PurchaseOrderGroupedService($this->user['id_company_pk']);
    }

    /**
     * <b>getList</b>
     * Receive the post from ordercover/filters and prepares query for save in cache(Redis).
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function getList()
    {
        $cache_check = $this->serviceOrderList->checkOrderCoverCache();
        if (!$this->post && !$cache_check) {
            return Request::redirect('purchaseorder/filters');
        }

        $header_cols = $this->getHeaderColumns($cache_check);
        if ($this->post) {
            $this->serviceOrderList->preparePagination($this->post);
        }

        $this->prepareSessPost();
        return [
            'header_cols' => $header_cols
        ];
    }

    /**
     * Para finalizar esse processo de listar somente as ordem em atraso
     * Necessario o processo completo de Order está pronto
     *
     * @return array
     */
    public function getListOrderLate()
    {
        $indicator = strtolower(Request::get('indicator'));
        if ($indicator != "late") {
            Request::home();
        }

        $date = Date::createRangeDate(0);

        $data = [];
        $data['ord_atraso'] = 'com';
        $data['date_req'] = "{$date['start']} até {$date['end']}";
        $data['date_order'] = "{$date['start']} até {$date['end']}";
        $data['date_exp'] = "{$date['start']} até {$date['end']}";

        // $view_data['header_cols'] = $this->serviceOrderList->prepareHeaderCols($data);
        // $this->serviceOrderList->preparePagination($data);

        return ['json' => true, 'data' => $indicator];
    }

    /**
     * <b>getTable</b>
     * Build the ordercover table array.
     *
     * @return array
     */
    public function getTable()
    {
        return [
            'ajax' => true,
            'header_cols' => $this->serviceOrderList->getHeaderCols(),
            'data_table' => $this->serviceOrderList->getGroupedOrders($this->post),
            'order' => $this->post['order'],
            'footer' => [
                'pages' => Utilities::calculatePages($this->serviceOrderList->getQtyTotalOrders(), $this->post['limit']),
                'count' => $this->serviceOrderList->getQtyTotalOrders()
            ]
        ];
    }

    /**
     * @param $cache
     * @return array|mixed|STRING
     */
    private function getHeaderColumns($cache)
    {
        return (!$this->post && $cache)
            ? $this->serviceOrderList->getHeaderColsFromCache()
            : $this->serviceOrderList->prepareHeaderCols($this->post);
    }

    private function prepareSessPost(): void
    {
        if ($this->post) {
            $filter = json_decode($this->post['filter_temp'], JSON_UNESCAPED_UNICODE);
            unset($filter['filter_temp']);

            $this->post['filter_temp'] = json_encode($filter, JSON_UNESCAPED_UNICODE);

            Redis::Set(self::SESS_NAME_DATA_FILTERS, json_encode($this->post['filter_temp'], JSON_UNESCAPED_UNICODE));
        }
    }
}
