<?php

class PurchaseOrderFiltersController
{
    private $verify_referer = false;

    public function __construct()
    {
        $this->verify_referer = Request::verifyReferer('purchaseorder/list');
        $this->session = Login::getUserSession();
        $this->serviceFilter = new PurchaseOrderFiltersService($this->session['id_company_pk']);
        $this->serviceCalendar = new CalendarService($this->session['id_company_pk']);
    }

    public function getFilters()
    {
        $viewData = $this->serviceFilter->getFiltersFields();
        $viewData['select_filtro'] = $this->serviceFilter->getSavedFilters();
        $viewData['SearchFields'] = $this->serviceFilter->getDinamicFieldsGrouping();
        $viewData['calendars'] = $this->serviceCalendar->getCalendarsbyCompany();
        $viewData['filter_temp'] = $this->serviceFilter->getFilterTemp($this->verify_referer);

        return $viewData;
    }
}
