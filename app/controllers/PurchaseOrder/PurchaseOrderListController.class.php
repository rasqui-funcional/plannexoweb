<?php

class PurchaseOrderListController extends Fields
{
    private $session;
    private $service;
    private $serviceFooter;

    public function __construct()
    {
        parent::__construct();
        $this->session = Login::getUserSession();
        $this->service = new PurchaseOrderListService(
            $this->session['id_company_pk'],
            $this->getNewDinamicFieldsGrouping(0)
        );
        $this->serviceFooter = new PurchaseOrderFooterService($this->session['id_company_pk']);
    }

    /**
     * <b>getList</b>
     * Receive the post from ordercover/filters and prepares query for save in cache(Redis).
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function getList()
    {
        return true;
    }

    /**
     * <b>getList</b>
     * Receive the post from ordercover/filters and prepares query for save in cache(Redis).
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function prepareList()
    {
        $post = Request::post();

        $view_data = [
            'json' => true,
            'data' => true
        ];

        $where = Redis::Get(USERSESSIONID . ':session:purchaseorder:grouped:query-basic');
        $where = str_replace('a.', '', $where);
        $where .= DB::setFields($post['obj']);

        if (!Redis::Set(USERSESSIONID . ":session:purchaseorder:list:where", $where)) {
            $view_data['data'] = false;
            return $view_data;
        }

        $total_orders = $this->service->getQtyTotalOrders($where);

        if (!Redis::Set(USERSESSIONID . ":session:purchaseorder:footer:total", $total_orders)) {
            $view_data['data'] = false;
        }

        return $view_data;
    }

    /**
     * <b>getTable</b>
     * Build the ordercover table array.
     *
     * @return array
     */
    public function getTable()
    {
        $post = Request::post();

        $total_itens = Redis::Get(USERSESSIONID . ":session:purchaseorder:footer:total");
        return [
            'header_cols' => $this->unsetFilterColumns($this->service->getHeaderCols(), ['LT_TOTAL_OUT', 'OUT6']),
            'data_table' => $this->service->getOrdersData($post),
            'order' => $post['order'],
            'footer' => [
                'data' => $this->serviceFooter->getOrdersFooterData(),
                'count' => $total_itens,
                'pages' => Utilities::calculatePages($total_itens, $post['limit'])
            ],
            'ajax' => true
        ];
    }
}
