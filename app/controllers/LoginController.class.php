<?php

/**
 * <b>LoginController.class:</b>
 * Class to handle with the login.
 *
 * @copyright (c) 2018, Alef Lima | Bionexo
 */
class LoginController
{
    private $serviceLogin;
    private $post;
    private $message;

    public function __construct()
    {
        $this->serviceLogin = new LoginService();
        $this->post = Request::post();
        $this->message = __CLASS__;
        Logger::debug("{$this->message}:post:login", $this->encryptPass($this->post));
    }

    public function execLogin()
    {
        //Login is now performed on VueJS.
        $this->execLogout();
//        if (!isset($this->post['login'])) {
//            return Request::redirect('error/401');
//        }
//
//        $loginData = $this->serviceLogin->setLogin($this->post);
//        Logger::debug("{$this->message}:response:login", [$loginData]);
//
//        if ($loginData === false) {
//            return Request::redirect('error/401');
//        }
//
//        Request::home();
    }

    public function loginError()
    {
        $this->execLogout();

    }

    public function getLogin()
    {
        $this->execLogout();
    }

    public function execLogout()
    {
        $this->serviceLogin->logout();
        header('Location: ' . BIOVIEWURL . 'logout');
    }

    public function expireSession()
    {
        $this->serviceLogin->logout();
    }

    public function checkSession()
    {
        $check = Login::getUserSession() ? true : false;

        return [
            'json' => true,
            'check' => $check
        ];
    }

    public function setSessionId()
    {
        $session_id = Request::get('session_id');
        Login::setUserSessionId($session_id);

        if (can('spdmBuyer')) {
            return Request::redirect('purchaseorder/filters');
        }

        return Request::redirect('dashboard');
    }

    public function health()
    {
        $status = [
            'database' => false,
            'redis' => false
        ];

        if (DB::Procedure('SELECT * FROM UAC_USER WHERE ACTIVE = 1')) {
            $status['database'] = true;
        }

        if (Redis::Set(USERSESSIONID . ':login:health', 'health check redis')) {
            $status['redis'] = true;
        }

        Logger::info(__CLASS__ . ' ' . __METHOD__, $status);
    }

    public function jobnight()
    {
        $companyId = Request::get('companyId');
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://status-carga.planning.bio/api/v1/plannexo/jobnight?companyid={$companyId}",
            CURLOPT_HTTPHEADER => array('Content-type: application/json'),
        ]);

        $result = curl_exec($curl);

        $jobnight = [
            'json' => true
        ];

        return array_merge($jobnight, json_decode($result, true));
    }

    private function encryptPass($post)
    {
        if (key_exists('password', $post)) {
            $post['password'] = base64_encode("{$post['password']}-plannexoweb");
        }

        return $post;
    }
}
