<?php

class ResumeFiltersController
{
    public function __construct()
    {
        $this->session = Login::getUserSession();
        $this->serviceFilter = new ResumeFiltersService($this->session['id_company_pk']);
    }

    public function getFilters()
    {
        $viewData = $this->serviceFilter->getFiltersFields();
        $viewData['select_filtro'] = $this->serviceFilter->getSavedFilters();
        $viewData['SearchFields'] = $this->serviceFilter->getDinamicFieldsGrouping();

        return $viewData;
    }
}
