<?php

class ResumeListController extends Fields
{
    private $post;
    private $session;
    private $terms;
    private $query_columns;
    private $redirect_columns;

    public function __construct()
    {
        parent::__construct();
        $this->session = Login::getUserSession();
        $this->post = Request::post();
        $this->service = new ResumeListService($this->session['id_company_pk']);
        $this->session_key = USERSESSIONID . ':session:resume:';
    }

    public function prepareResumeTable()
    {
        $cache_set = $this->setQueryCache();
        if (!$cache_set) {
            Request::redirect('resume/filters');
        }

        return true;
    }

    public function getResumeTable()
    {
        $query_columns = Redis::Get($this->session_key . 'columns');
        $redirect_columns = Redis::HGet($this->session_key . 'redirect:columns');
        $terms = Redis::Get($this->session_key . 'terms');
        $cod_estab = Redis::Get($this->session_key . 'cod_estab');
        $order = $this->getOrder($this->post['order']);
        $header_cols = Redis::HGet($this->session_key . 'header:cols');
        $total_itens = Redis::Get($this->session_key . 'footer:count');
        $exibitions_columns = $this->service->setExibitionsColumns($redirect_columns, $header_cols);

        $data = $this->service->getData(
            $this->post['page'],
            $this->post['limit'],
            $query_columns,
            $terms,
            $order,
            $header_cols
        );

        return [
            'cod_estab' => $cod_estab,
            'data_table' => $data,
            'header_cols' =>  $header_cols,
            'redirect_columns' => $redirect_columns,
            'exibitions_columns' => $exibitions_columns,
            'order' => $this->post['order'],
            'footer' => [
                'data' => $this->getFooterData(),
                'count' => $total_itens,
                'pages' => Utilities::calculatePages($total_itens, $this->post['limit']),
            ],
            'ajax' => true
        ];
    }

    public function getResumeCSV()
    {
        $columns = Redis::Get($this->session_key . 'columns');
        $terms = Redis::Get($this->session_key . 'terms');
        $order = $this->getOrder($this->post['order']);
        $header_cols = Redis::HGet($this->session_key . 'header:cols');
        $data = $this->service->getCSVData($columns, $terms, $order, $header_cols);

        return [
            'ajax' => true,
            'data' => $data,
            'error' => $data ? false : $this->lang['csv']['error_create']
        ];
    }

    private function getOrder($order)
    {
        if($order) {
            return $this->service->setOrder($order);
        } else {
            return Redis::Get($this->session_key . 'order');
        }
    }

    private function setQueryCache()
    {
        if (!$this->addFixedColumn()) {
            return false;
        }

        $this->terms = $this->service->setTerms($this->post);
        $columns = $this->service->setColumns($this->post['exibition_columns'], $this->post['group_columns']);
        $this->query_columns = $columns['query_columns'];
        $this->redirect_columns = $columns['redirect_columns'];
        $this->order = $this->service->setOrder($this->post['order']);
        $total_itens = $this->service->getTotalItems($this->terms, $this->query_columns);

        Redis::Set($this->session_key . 'cod_estab', isset($this->post['cod_estab_fk']) ? implode(",", $this->post['cod_estab_fk']) : '');

        if (!Redis::Set($this->session_key . 'terms', $this->terms)) {
            return false;
        }

        if ($this->query_columns && !Redis::Set($this->session_key . 'columns', $this->query_columns)) {
            return false;
        }

        if ($this->redirect_columns && !Redis::HSet($this->session_key . 'redirect:columns', $this->redirect_columns)) {
            return false;
        }

        if (!Redis::Set($this->session_key . 'order', $this->order)) {
            return false;
        }

        if (!Redis::Set($this->session_key . 'footer:count', $total_itens)) {
            return false;
        }

        if(!$this->setFooterData()){
            return false;
        }

        if(!$this->setHeaderCols($this->post['exibition_columns'])){
            return false;
        }

        return true;
    }

    /**
	 * <b>addFixedColumn</b>
	 * Adiciona ao grupo de colunas selecionadas as colunas pelos quais o usuário gostaria de agrupar.
	 *
	 * @return Bool
	 */
    private function addFixedColumn()
    {
        foreach (array_reverse($this->post['group_columns']) as $column) {
            if (!array_unshift($this->post['exibition_columns'], $column)) {
                return false;
            }
        }

        return true;
    }

    private function getFooterData()
    {
        return Redis::HGet($this->session_key . 'footer:data');
    }

    public function getHeaderCols()
    {
        $header_cols = Redis::HGet($this->session_key . 'header:cols');

        return $this->setTitleonHeaderCols($header_cols);
    }

    private function setHeaderCols($post_columns)
    {
        $group_data = $this->service->getDynamicHeaderColsTitle();

        $header_cols = array();
        foreach ($post_columns as $col) {
            if(isset($group_data[$col])) {
                $header_cols[strtolower($col)] = $group_data[$col];
            } else {
                $header_cols[strtolower($col)] = false;
            }
        }

        if ($header_cols && !Redis::HSet($this->session_key . 'header:cols', $header_cols)) {
            return false;
        }

        return true;
    }

    private function setFooterData()
    {
        $footer_data = $this->service->setFooterData($this->terms, $this->query_columns);

        return Redis::HSet($this->session_key . 'footer:data', $footer_data);
    }
}
