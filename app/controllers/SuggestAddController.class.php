<?php

class SuggestAddController
{
    private $session;
    private $serviceSuggestAdd;
    private $post;

    public function __construct()
    {
        $this->session = Login::getUserSession();
        $this->serviceSuggestAdd = new SuggestAddService($this->session['id_company_pk']);
        $this->post = Request::post();
    }

    /**
     * <b>getSKUDescription</b>
     * Get the sku description from database to manual suggestion.
     *
     * @return ARRAY $ViewData all data used in view.
     */
    public function getSKUDescription()
    {
        try{
            $cod_estab_fk = ($this->post['cod_estab_fk'])? $this->post['cod_estab_fk']: null;
            $view_data['data'] = $this->serviceSuggestAdd->getSkuDescription($this->post['cod_item'], $this->post['cod_estab_fk']);
            $view_data['json'] = true;
    
            return $view_data;
        }
        catch (\Exception | Throwable $thrw)
        {
            header('HTTP/1.1 '.$thrw->getCode().' Internal Server Error');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => $thrw->getMessage())));
        }
    }

    public function addSuggest()
    {
        $dateAlreadyExists = $this->verifySugForThisDate(($this->post['obj']));
        $invalidQuantiy = $this->verifyInvalidQty(($this->post['obj']));

        $view_data['data']['dateAlreadyExists'] = $dateAlreadyExists;
        $view_data['data']['invalidQty'] = $invalidQuantiy;
        $view_data['json'] = true;

        if($dateAlreadyExists || $invalidQuantiy) {            
            return $view_data;
        }
        
        $data = $this->prepareInsertData($this->post['obj']);
        $this->serviceSuggestAdd->insertSuggest($data);        

        return $view_data;
    }

    public function verifyInvalidQty($data) {
        foreach ($data as $suggest) {
            if($suggest['qtd_un'] <= 0)
            {
                return true;
            }
        }
        return false;
    }

    public function checkDateForSug()
    {
        try{
            $dateAlreadyExists = $this->verifySugForThisDate(($this->post['obj']));
            // $dateExistsOnAprovals = $this->verifyDateOnSuggestAproval(($this->post['obj']));

            $view_data['data']['dateAlreadyExists'] = $dateAlreadyExists;
            // $view_data['data']['dateExistsOnAprovals'] = $dateExistsOnAprovals;
            $view_data['json'] = true;

            return $view_data;
        }catch (\Exception | Throwable $thrw)
        {
            header('HTTP/1.1 '.$thrw->getCode().' Internal Server Error');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => $thrw->getMessage())));
        }
    }

    public function verifySugForThisDate($sug) {
        
        $table = 'scm_sku_purchase_sug';
        $columns = 'date_arrival_altered';
        $checkDate = [];
        foreach ($sug as $key => $value) {
            $whereSug = '';
            if($value['id_sug'])
            {
                $whereSug = "and id_sug_pk != {$value['id_sug']}";
            }
            $terms = "
                where id_company_fk = {$this->session['id_company_pk']}
                    and id_sku_fk = {$value['id_sku']}
                    and date_arrival_altered = to_date('{$value['delivery_date']}', 'DD/MM/YYYY')
                    $whereSug
            ";
            $dates =  DB::Select($table, $columns, $terms);
            array_push($checkDate, $this->compareDates($dates, $value['delivery_date'], $value['cod_item']));
        }
        
        /*o array_filter vai me retornar apenas indices que não sejam nulos, mas ele mantem o indice do array
        o arra_values faz com que o array tenha seus indices corretos, desde o indice 0, assim no javascript não terei problemas
        */
        return array_values(array_filter($checkDate));
    }

    public function verifyDateOnSuggestAproval($sug) {
        $table = 'scm_sku_purchase_sug_hist';
        $columns = 'date_arrival_altered';
        $checkDate = [];
        foreach ($sug as $key => $value) {
            $terms = "
                where id_company_fk = {$this->session['id_company_pk']}
                    and id_sku_fk = {$value['id_sku']}
                    and date_arrival_altered = to_date('{$value['delivery_date']}', 'DD/MM/YYYY')
            ";
            $dates =  DB::Select($table, $columns, $terms);
            array_push($checkDate, $this->compareDates($dates, $value['delivery_date'], $value['cod_item']));
        }
        
        /*o array_filter vai me retornar apenas indices que não sejam nulos, mas ele mantem o indice do array
        o arra_values faz com que o array tenha seus indices corretos, desde o indice 0, assim no javascript não terei problemas
        */
        return array_values(array_filter($checkDate));
    }

    public function compareDates($dateAlreadyInDb, $originalDate, $codItem) {
        
        $newDate = DateTime::createFromFormat('d/m/Y', $originalDate);
        $newDate = $newDate->format('d-m-y');

        foreach ($dateAlreadyInDb as $key => $value) {
            $date = DateTime::createFromFormat('j-M-Y', $value['DATE_ARRIVAL_ALTERED']);
            if($date->format('d-m-y') === $newDate) {
                $matchDate['date'] = $originalDate;
                $matchDate['cod_item'] = $codItem;
                break;
            }
        }
        
        return $matchDate;
    }

    private function prepareInsertData($data)
    {
        foreach ($data as $suggest) {
            $sku_id = $suggest['id_sku'];
            $company_id = $this->session['id_company_pk'];
            $today = date('d/m/Y');
            $portion = $this->buildNeedIDAndNumPortion($sku_id, $company_id);

            $data = [
                'id_company_fk' => $company_id,
                'id_psug_pk' => $portion['ID_PSUG_PK'],
                'num_parc_pk' => $portion['NUM_PARC_PK'],
                'id_sku_fk' => $sku_id,
                'date_arrival_original' => "to_date('{$suggest['delivery_date']}', 'DD/MM/YYYY')",
                'date_arrival_altered' => "to_date('{$suggest['delivery_date']}', 'DD/MM/YYYY')",
                'qty_original' => $suggest['qtd_un'],
                'qty_altered' => $suggest['qtd_un'],
                'id_user_created' => $this->session['user_id'],
                'sit_auto_sug' => 0,
                'date_created' => "to_date('$today', 'DD/MM/YYYY')",
                'sit_altered_qty' => 1,
                'sit_psug' => 1,
                'sit_urgent' => $suggest['sit_urgent'],
                'sit_saved' => $suggest['sit_aproved'],
                'id_sug_pk' => 'seq_scm_sku_purchase_sug.NEXTVAL'
            ];

            if (!DB::Insert('scm_sku_purchase_sug', $data)) {
                return false;
            }
        }
        
        return true;
    }

    private function buildNeedIDAndNumPortion($sku_id, $company_id)
    {
        $portion = $this->buildPortion($sku_id, $company_id);

        if (empty($portion)) {
            $portion = $this->buildFirstPortion($company_id);
        }

        return $portion;
    }

    private function buildPortion($sku_id, $company_id)
    {
        $table = 'scm_sku_purchase_sug';
        $columns = 'id_psug_pk, max(num_parc_pk)+1 as num_parc_pk';
        $terms = "
            where id_company_fk = {$company_id}
                and id_sku_fk = {$sku_id} group by id_psug_pk
        ";

        return DB::SelectRow($table, $columns, $terms);
    }

    private function buildFirstPortion($company_id)
    {
        $table = 'scm_sku_purchase_sug';
        $columns = 'max(id_psug_pk) + 1 as id_psug_pk, 1 as num_parc_pk';
        $terms = "where id_company_fk = {$company_id}";

        return DB::SelectRow($table, $columns, $terms);
    }
}