<?php 

/**
 * Class responsable for controll the view vw_scr_scm475g_excess_estab
 */
class ExcessController {

    /**
     * <b>getExcess</b>
     * Return all different estabs from the estab of the item of an suggest
    */
    public function getExcess(){
        $id = Request::get('id');
        $view = 'vw_scr_scm420g_sug';
        $key = 'id_sug_pk';        

        if(Request::get('from') == 'sku'){
            $view = 'vw_scr_scm405g_sku_list';
            $key = 'id_sku_pk';
        }                        

        $session= Login::getUserSession();

        $Estab = DB::Select($view, 'cod_estab_fk, cod_item_pk', "WHERE {$key} = {$id}");
        if($Estab){
            $Termos = "WHERE id_company_fk = {$session['id_company_pk']} AND cod_item_pk = '{$Estab[0]['COD_ITEM_PK']}' AND cod_estab_fk != '{$Estab[0]['COD_ESTAB_FK']}'";
            $ExcessEstab = DB::Select('vw_scr_scm475g_excess_estab', 'COD_ESTAB_FK, QTY_EXCESS, INV_DAYS', $Termos);
        }else{
            $ExcessEstab = false;
        }

        if($ExcessEstab){
            foreach($ExcessEstab as $Key => $EstabData){
                foreach($EstabData as $ColName => $Value){             
                    $ViewData['estab_excess'][$Key][$ColName] = $this->prepareData($ColName, $Value);
                }      
            } 
        } else {
            $ViewData['estab_excess'] = false;
        }       
        
        $ViewData['ajax'] = true;
        return $ViewData;

    }
    /** ------------------------------ PRIVATE ------------------ */

    /**
     * <b>prepareData</b>
     * Prepare the data of Column
     */
    private function prepareData($ColName, $Value){
        switch ($ColName) {
            case 'INV_DAYS':
                $Value = round($Value, 1);
                break;                
            case 'COD_ESTAB_FK':
                $Value = Utilities::getEstab($Value);
                break;
        }
        return $Value;
    }

}