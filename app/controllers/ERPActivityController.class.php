<?php

class ERPActivityController
{
    private $user;
    private $post;
    const LIMIT = 15;

    public function __construct()
    {
        $this->user = Login::getUserSession();
    }

    public function getActivity()
    {
        return ['json' => true];
    }

    /**
     * Retorna as informações da tabela de Saldo em estoque
     *
     */
    public function getStockOverBalance()
    {
        $this->post = Request::post();
        $offset = $this->prepareSetOffset((int)$this->post['page']);
        $order = $this->post['order'] ? $this->prepareOrder($this->post['order']): '';
        $data = $this->dynamicGetTableData('StockBalanceService', $offset, $order);

        return [
            'ajax' => true,
            'table' => [
                'data' => $data['result'],
                'cols' => $this->prepareCols(['DESC_LOCAL', 'DAYS_QTY_INV', 'QTY_INV_VAL', 'QTY_INV']),
            ],
            'footer' => [
                'count' => $data['number_of_rows'],
                'pages' => Utilities::calculatePages($data['number_of_rows'], self::LIMIT),
                'data' => $data['result']['countInfo']
            ]
        ];
    }

    /**
     * Retorna as informações da tabela de Ordem de compra
     *
     */
    public function getPurchaseOrder()
    {
        $this->post = Request::post();
        $search = $this->prepareSearch('NUM_ORDER');
        $offset = $this->prepareSetOffset((int)$this->post['page']);
        $order = $this->post['order'] ? $this->prepareOrder($this->post['order']): 'ORDER BY DATE_EXP DESC';
        $data = $this->dynamicGetTableData('PurchaseOrderService', $offset, $order, $search);

        return [
            'ajax' => true,
            'table' => [
                'data' => $data['result'],
                'cols' => $this->prepareCols(['NUM_ORDER', 'ID_SUPPLIER', 'DATE_ORDER', 'DATE_EXP', 'QTY_PENDING', 'QTY_REC', 'QTY_PARC', 'VAL_UNIT', 'VAL_TOTAL']),
            ],
            'footer' => [
                'count' => $data['number_of_rows'],
                'pages' => Utilities::calculatePages($data['number_of_rows'], self::LIMIT),
                'data' => $data['result']['countInfo']
            ]
        ];
    }

    /**
     * Retorna as informações da tabela de Solicitação de compra
     *
     */
    public function getPurchaseRequest()
    {
        $this->post = Request::post();
        $search = $this->prepareSearch('p.COD_LOCAL');
        $offset = $this->prepareSetOffset((int)$this->post['page']);
        $order = $this->post['order'] ? $this->prepareOrder($this->post['order']): 'ORDER BY DATE_EXP DESC';
        $data = $this->dynamicGetTableData('PurchaseRequestService', $offset, $order, $search);

        return [
            'ajax' => true,
            'table' => [
                'data' => $data['result'],
                'cols' => $this->prepareCols([
                    'COD_REQ',
                    'ORIGEM',
                    'NAME',
                    'DESC_LOCAL',
                    'DATE_REQ',
                    'DATE_EXP',
                    'QTY_REQ',
                    'QTY_COVERAGE'
                ]),
            ],
            'footer' => [
                'count' => $data['number_of_rows'],
                'pages' => Utilities::calculatePages($data['number_of_rows'], self::LIMIT),
                'data' => $data['result']['countInfo']
            ]
        ];
    }

    /**
     * Retorna as informações da tabela de transação de entrada
     *
     */
    public function getInTransaction()
    {
        $this->post = Request::post();
        $search = $this->prepareSearch('COD_TRANS_PK');
        $offset = $this->prepareSetOffset((int)$this->post['page']);
        $order = $this->post['order'] ? $this->prepareOrder($this->post['order']): 'ORDER BY DATE_TRANS DESC';
        $data = $this->dynamicGetTableData('InTransactionService', $offset, $order, $search);

        return [
            'ajax' => true,
            'table' => [
                'data' => $data['result'],
                'cols' => $this->prepareCols(['COD_TRANS_PK', 'QTY_TRANS', 'DATE_TRANS']),
            ],
            'footer' => [
                'count' => $data['number_of_rows'],
                'pages' => Utilities::calculatePages($data['number_of_rows'], self::LIMIT),
                'data' => $data['result']['countInfo']
            ]
        ];
    }

    /**
     * Retorna as informações da tabela de transação de saída
     *
     */
    public function getOutTransaction()
    {
        $this->post = Request::post();
        $offset = $this->prepareSetOffset((int)$this->post['page']);
        $reset_order = false;

        $order = $this->post['order'] ? $this->prepareOrder($this->post['order']): 'ORDER BY DATE_TRANS_ALT DESC';
        $data = $this->dynamicGetTableData('OutTransactionService', $offset, $order);

        if ($reset_order) {
            $this->post['order'] = NULL;
        }

        return [
            'ajax' => true,
            'table' => [
                'data' => $data['result'],
                'cols' => $this->prepareCols(['DATE_TRANS_ALT', 'COD_LOCAL', 'COD_LOCAL_DEST', 'COD_SECTOR', 'QTY_TRANS', 'TYPE_TRANS', 'YN_CONS']),
            ],
            'footer' => [
                'count' => $data['number_of_rows'],
                'pages' => Utilities::calculatePages($data['number_of_rows'], self::LIMIT),
                'data' => $data['result']['countInfo']
            ]
        ];
    }


    /**
     * Retorna as informações da tabela de Consumo Mensal
     *
     * @return array
     */
    public function getMonthlyConsumption()
    {
        $this->post = Request::post();
        $offset = $this->prepareSetOffset((int)$this->post['page']);
        $order = $this->post['order'] ? $this->prepareOrder($this->post['order']): '';
        $data = $this->dynamicGetTableData('MonthlyConsumptionService', $offset, $order);

        return [
            'ajax' => true,
            'table' => [
                'data' => $data['result'],
                'cols' => $this->prepareCols(['MONTH_TRANS_PK', 'QTY_TRANS']),
            ],
            'footer' => [
                'count' => $data['number_of_rows'],
                'pages' => Utilities::calculatePages($data['number_of_rows'], self::LIMIT),
                'data' => $data['result']['countInfo']
            ]
        ];
    }

    /**
     * Retorna um array de colunas com suas respectivas ordenações
     * @param array $columns
     * @return array
     */
    private function prepareCols(array $columns)
    {
        $cols = [];
        foreach ($columns as $name) {
            $print_name = strtoupper($name);
            
            if ($print_name == 'RNUM') {
                continue;
            }
            
            $cols[$print_name] = [
                'type' =>  'text',
                'order' => $this->getColOrder($print_name)
            ];
        }
        
        return $cols;
    }

    /**
     * Retorna a string que complementa a ordem no banco com a ordenação
     * @param array $orders
     * @return string
     */
    private function prepareOrder(array $orders)
    {
        end($orders);
        $end = key($orders);
        $order_query = "ORDER BY ";
        foreach ($orders as $columns => $col_order) {
            $order_query .= "{$columns} {$col_order}";
            if ($end != $columns) {
                $order_query .= ", ";
            }
        }
        return $order_query;
    }

    /**
     * Retorna a string que complementa a busca no banco com a ordenação
     * @param string $field
     * @return string
     */
    private function prepareSearch(string $field)
    {
        $search = '';
        if (isset($this->post['search']) && !empty($this->post['search'])) {
            $search = " AND {$field} LIKE'%{$this->post['search']}%'";
        }
        
        return $search;
    }

    /**
     * Retorna o número da posição da paginação
     * @param int $page
     * @return int
     */
    private function prepareSetOffset(int $page)
    {
        return ($this->post['page'] - 1) * self::LIMIT;
    }
    
    /**
     * Retorna a ordenação de determinado campo
     *
     * @param string $column_name nome da coluna
     * @return string resultado(asc/desc/null)
     */
    private function getColOrder(string $column_name)
    {
        $result = null;

        if (!isset($this->post['order'])) {
            return $result;
        }

        if (!$this->post['order']) {
            return $result;
        }

        return (key_exists($column_name, $this->post['order'])) ? $this->post['order'][$column_name] : $result;
    }
        
    private function dynamicGetTableData($serviceName, $offset, $order, $search = null)
    {
        $this->service = new $serviceName();
        if (is_null($search)) {
            $data = $this->service->getTableData($this->post, $this->user['id_company_pk'], $offset, self::LIMIT, $order);
        } else {
            $data = $this->service->getTableData($this->post, $this->user['id_company_pk'], $offset, self::LIMIT, $search, $order);
        }

        return [
            'result' => $data,
            'number_of_rows' => (int) $data['rows']['TOTAL']
        ];
    }
}
