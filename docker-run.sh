#!/bin/bash
echo "Subindo o docker........."
if [ $1 ]
then
  docker-compose -f docker-compose-dev.yml up -d

  echo "Baixando as libs do API ........."
  docker-compose -f docker-compose-dev.yml exec php-fpm composer install

  echo "Configurando o API........."
  docker-compose -f docker-compose-dev.yml exec php-fpm php artisan config:clear

  docker-compose -f docker-compose-dev.yml exec php-fpm cp .env.example .env

  docker-compose -f docker-compose-dev.yml exec php-fpm php artisan key:generate
else
  docker-compose -f docker-compose-dev.yml up -d
fi

echo "############################"
echo "#  Isto é tudo pessoal :D  #"
echo "############################"
