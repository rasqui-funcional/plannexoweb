#!/usr/bin/env bash

# git hook to run a command after `git pull` if a specified file was changed
# Run `chmod +x post-merge` to make it executable then put it into `.git/hooks/`.

changed_files="$(git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD)"

extension=".js"
if [[ $changed_files= =~ $extension ]]; then
  cd app/resources
  if [ ! -d "node_modules" ]; then
    echo "Installing dependencies"
    npm install
  fi
  ./node_modules/gulp/bin/gulp.js
fi
