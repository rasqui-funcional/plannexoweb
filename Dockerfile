FROM php:7.2.12-apache

ENV TZ=America/Sao_Paulo
RUN echo $TZ > /etc/timezone

ENV ORACLE_HOME /opt/oracle/instantclient
ENV LD_LIBRARY_PATH=$ORACLE_HOME/lib

ENV COMPOSER_ALLOW_SUPERUSER=1

ARG ambiente=prod

#download composer 
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

# Add nodejs Repo
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -

RUN apt-get update && apt-get install --no-install-recommends -y && apt-get install -y wget \
    unzip \
    lsb-release \
    apt-transport-https \
    libxslt1-dev \
    zlib1g-dev \
    libpng-dev \
    libjpeg-dev \
    build-essential \
    libaio-dev \
    nodejs && \
    apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install intl gettext
RUN docker-php-ext-enable intl gettext


# Install Oracle Instant Client Basic and SDK
ADD install_oracle/instantclient-basic-linux.x64-19.6.0.0.0dbru.zip /tmp/basic.zip
ADD install_oracle/instantclient-sdk-linux.x64-19.6.0.0.0dbru.zip /tmp/sdk.zip

RUN mkdir -p /opt/oracle/instantclient && \
    unzip -q /tmp/basic.zip -d /opt/oracle && \
    mv /opt/oracle/instantclient_19_6 /opt/oracle/instantclient/lib && \
    unzip -q /tmp/sdk.zip -d /opt/oracle && \
    mv /opt/oracle/instantclient_19_6/sdk/include /opt/oracle/instantclient/include && \
#    ln -s /opt/oracle/instantclient/lib/libclntsh.so.19.1 /opt/oracle/instantclient/lib/libclntsh.so && \
#    ln -s /opt/oracle/instantclient/lib/libocci.so.19.1 /opt/oracle/instantclient/lib/libocci.so && \
    echo /opt/oracle/instantclient/lib >> /etc/ld.so.conf && \
    ldconfig

# Install PHP OCI8 extension
#RUN echo 'instantclient,/opt/oracle/instantclient/lib' | pecl install oci8-2.1.4

RUN docker-php-ext-configure pdo_oci --with-pdo-oci=/opt/oracle/instantclient && \
    echo 'instantclient,/opt/oracle/instantclient/lib' | pecl install oci8 && \
    docker-php-ext-install pdo_oci xsl zip xmlrpc gd && \
    docker-php-ext-enable pdo_oci oci8 xsl zip xmlrpc gd

#Copy apache2.conf customized
COPY infra/$ambiente/apache/apache2.conf /etc/apache2/

#Configuration ssl 

#RUN mkdir /etc/apache2/crt
#COPY ssl/ /etc/apache2/crt
COPY infra/$ambiente/apache/00-plannexo.conf /etc/apache2/sites-enabled/

#Enable rewrite/ssl module apache
RUN a2enmod rewrite
RUN a2enmod ssl

COPY infra/$ambiente/apache/php.ini /usr/local/etc/php/php.ini
COPY ./app/ /var/www/html/

#Generate min files
RUN cd /var/www/html/resources && npm install && npm install --global gulp-cli && gulp

#install composer dependencies  
RUN cd /var/www/html && composer install --optimize-autoloader --no-scripts --no-plugins --no-dev --prefer-dist

EXPOSE 80 443

