# Plannexo Rebuild
Instalação do plannexo rebuild e suas respectivas correspondências.

### Sumário ###
 - Pré requisitos
 - Instalação
 - Estrutura do projeto
 - Workflow
 - Kubernets
 - Links Uteís

### Pré-Requisitos ###
- PHP = 7.2
- Apache = 2.4
- Docker >= 18.03.1-ce
- Docker-Compose >= 1.21.2
- Git >= 2.17.1

### Instalação ###

**Git**
```
#!terminal

git clone git@bitbucket.org:bionexo/plannexoweb.git

cd plannexoweb
```
### Create hook
1. **Set up git hooks in `.git/hooks`
    - `cp -p infra/scripts/post-merge.sh .git/hooks/post-merge`

**Docker e Docker Compose**
 - Procure pelo tutorial de instalação respectivo ao seu sistema operacional 
    - Ubuntu (https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-18-04-pt)
    - Windows (https://docs.docker.com/v17.09/docker-for-windows/install/)
 - Procure pelo tutorial de instalação respectivo ao seu sistema operacional 
    - Ubuntu: (https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04)
    - Windows: (https://docs.docker.com/compose/install/)

**Subindo o projeto**
* Caso você ja tenha baixado o projeto e em algum momento ja ter subido o docker, então, execute os passos abaixo:
   ```
  docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q) && docker rmi $(docker images -q)
  
  // see: https://github.com/chadoe/docker-cleanup-volumes
  docker volume rm $(docker volume ls -qf dangling=true)
  docker volume ls -qf dangling=true | xargs -r docker volume rm
  
  // see: http://stackoverflow.com/questions/32723111/how-to-remove-old-and-unused-docker-images
  docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
  docker rmi $(docker images | grep "none" | awk '/ / { print $3 }')
  
  
  // see: http://stackoverflow.com/questions/32723111/how-to-remove-old-and-unused-docker-images
  docker rm $(docker ps -qa --no-trunc --filter "status=exited")
  
  docker network rm $(docker network ls | grep "bridge" | awk '/ / { print $1 }')
  
  bash docker-run.sh start      
  ``` 

* Caso contrário, apenas execute:
  ```
  bash docker-run.sh
  ```

* Mas se quiser subir manualmente: (use sudo na frente dos comando se for o seu caso)
    ```
    docker-compose -f docker-compose-dev.yml up -d
    
    // Os comandos abaixo, devem ser rodados se caso for a primeira instalação do projeto
    docker-compose -f docker-compose-dev.yml exec php-fpm composer install
    
    docker-compose -f docker-compose-dev.yml exec php-fpm php artisan config:clear
    
    docker-compose -f docker-compose-dev.yml exec php-fpm cp .env.example .env
    
    docker-compose -f docker-compose-dev.yml exec php-fpm php artisan key:generate
    
    // Demais comandos
    docker ps
    
    docker exec -it <CONTAINER ID> bash
    
    exit
    
    ```

## Estrutura do projeto
 - app -- pasta que contém as classes do projeto.
 - docker-compose.yml -- arquivo docker para  subir a aplicação localmente.
 - Dockerfile -- arquivo docker para criar uma imagem base da aplicação.
 - infra -- pasta que contém arquivos relacionado as váriaveis de ambiente/arquivos do kubernetes
 - Jenkinsfile -- arquivo onde é definido a pipeline do Jenkins para construção do build do projeto.
 - GitModule -- Foi adicionado ao projeto dois submodulos (API/Client)

## WorkFlow
Neste projeto trabalharemos com Feature branch e Pull Resquest.
Teremos 2 branches principais:
 - master -- onde armazenará os códigos que estarão em produção.
 - QA -- onde armazenará os códigos/testes, para que seja validado as novas features desenvolvidas.

Estando os desenvolvedores com o clone do projeto plannexoweb:
Nova feature
## 1ª - o desenvolvedor deverá realizar o pull (baixar novas atualizações) do projeto, para isso fazer o checkout do masters:
 - git checkout master
 - git pull

## 2ª - o desenvolvedor deverá criar uma nova branch a partir do master:
 - git checkout -b [nome da feature]

## 3º - O desenvolvedor implementa os códigos e disponibiliza a branch (lembrar de adicionar informações ao CHANGELOG):
 - git add (para adicionar as modificações)
 - git commit -m "[informaçõe do commit]"
 - git push origin <funcionalidade_x> 

## 4º - Após terminar o desenvolvimento deverá criar um pull request para o time avaliar o código:
 - Na descrição do pull request deverá conter uma breve descrição do que foi desenvolvido/alterado e o comportamento esperado. Em caso de bugs, adicionar a solução adotada.

## 5º - Após aprove do time o branch deverá ser enviado(mergeado) para o branch de QA:
 - git checkout QA
 - git pull (no branch QA para atualizar)
 - git merge <branch>
 - jenkins irá gerar o pacote de qa de acordo com a atualização feita no branch

## 6º - Após finalização dos testes deve ser realizado o merge com o master
 - git checkout master
 - git pull
 - git merge <branch>

## Deploy em QA
 - 1º realizar o merge com a branch de QA. Ex: git merge --no-ff PNR-X
 - 2º Realizar o push para o remoto.  EX: git commit -m "PNR-X" && git push
 - 3º Jenkins realizar o build automaticamente após identificar o commit realizado na branch de QA. Isso está definido no Jenksfile.
 - 3.1º O jenkins faz o download do código e gera uma nova imagem do docker e fazer push para o dockerhub desta imagem com a tag qa. Ex: bionexo/plannexoweb:qa
 - 3.2º O jenkins acessar o kubernetes e executa o comando de deletar a pod (docker) a atual e executa o comando de create para criar uma nova pod com o novo código que se encontra na imagem docker já buildada anteriormente.
 - 4º Após o build realizado com sucesso, no slack de qa será emitido uma notificação do build realizado no plannexo.
 - 5º Espere alguns minutos e acesse a url de qa do plannexo.
 Caso o build não acontece automáticamente será necessário:
 - 1º Acessar o jenkins (http://jenkins.cloud.bionexo.com.br) e logar com sua conta.
 - 2º Procurar pelo por: Bitbucket Automatic Tests -> plannexoweb -> qa -> Construir Agora (para fazer um novo deploy em QA).
  - senão verificar o build que deu erro selecionando o build que esta marcado em vermelho e ir em "Saída do Console" ou "Exibir com texto puro"

## Deploy em produção
- 1ª Realizar o merge da branch de qa na branch master
- 2º Gerar na master uma tag manualmente (pode ser automatica via script no Jenkinsfile) e realizar o commit e push
- 3º Acessar o Jenkins (http://jenkins.cloud.bionexo.com.br) e logar com sua conta.
- 4º Procurar o projeto: Jenkins -> Plannexo-PROD.
- 5º Para gerar um deploy clicar em "Construir com Parâmetros"
- 6º Inserir como valor a próxima tag apartir do último deploy gerado. Ex: últim deploy foi a tag 1.0.62 - pŕoximo deploy será a tag 1.0.63. OBS: Esta tag não tem relação com a tag feita anterior na branch mastet, porém o ideal fosse seguir a mesma tag da master.
- 7º Quando o pipeline chegarna fase de "Promote: Deploy Sandbox" passe o mouse em cima dessa fase e terá 2 opções prosseguir ou abortar o deploy em sandbox. Nesta caso vamos clicar em prosseguir.
- 8º Na fase "Promote: Deploy PROD" teremos as opções de "Prosseguir ou abortar". Lembrando que ao prosseguir o Jenkins irá deletar a pod do plannexo em PROD e depois criará um novo POD em produção.

## Acessando Kubernetes cluster DEV (onde nossa ambiente de QA)
Caso não tenha o kubernetes instalado e deseja instalar:
https://bionexo.atlassian.net/wiki/spaces/ENG/pages/616398950/Instala+o+do+Kubectl+e+configura+o
Porém não é pré-requisito para acessar o dashboard do Kubernetes.

Para acessar o dashboard do Kubernetes cluster DEV :
dashboard URL: https://dashboard.regene.k8s.dev.cloud.bionexo.com.br/#!/login
Usar acesso por token:
eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLWw4aHZrIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiIwNTkxZTYxZi0yNTVhLTExZTktOTNhMi0wNmRmYzM3MzBjN2EiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06YWRtaW4tdXNlciJ9.hz-wzFhyy8I34SuEiaR8L5Ggs8leO00hrGno4EZwU-V7seV9qRaP6_SdXpBi-PkPtRM9VDHH6brFysDOC6zrfDXPDMrM634wBkKrZuR6Jp_k1Js6xb-IByLhJ8zCyZ0cFeHeIdbgIfIX-0tAk0Ln2VjU7aibUVt9_QXpzHIHTH3YUj95S4kV8T5dbigorqeZzum5A7S6DE2nTxyUvxqTjGjnQeNHMdkcyBE8UT_pYzAW49SHOtivYB7yHofRszQjWlNnGiOhK13dzP_XueU9BnOwp6_8m-_wCw9w9l6ybJ1GwCR7sRPUy8xZQTCm-sS4DGPR9v8qxBeanEANVg6fIQ

Quando acessar o dashboard escolher o Namespace: Plannexoweb
Para fazer qualquer alteração diretamente no código em QA:
Com o namespace 
  
## Execução dos Testes
Para execução dos testes é necessário:

## Links Úteis:
 - https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04
 - http://rogerdudler.github.io/git-guide/index.pt_BR.html
 - http://marklodato.github.io/visual-git-guide/index-en.html
 - https://git-scm.com/book/pt-br/v2
 - https://stackoverflow.com/questions/35008713/bitbucket-git-credentials-if-signed-up-with-google
 
